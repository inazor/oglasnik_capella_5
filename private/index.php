#!/usr/bin/env/php
<?php
/**
 * CLI Bootstrap
 */
error_reporting(E_ALL);
try {
    if (!defined('ROOT_PATH')) {
        define('ROOT_PATH', dirname(__DIR__));
    }
    if (!defined('APP_DIR')) {
        define('APP_DIR', ROOT_PATH . '/app');
    }

    require_once APP_DIR . '/Bootstrap.php';
    require_once APP_DIR . '/Console.php';
    require_once ROOT_PATH . '/vendor/autoload.php';

    $console = new \Baseapp\Console(new \Phalcon\Di\FactoryDefault\Cli());
    $console->handle($argv);
} catch (\Exception $e) {
    \Baseapp\Console::exception($e);
}
