-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 11, 2016 at 05:09 PM
-- Server version: 10.1.16-MariaDB-1~wily
-- PHP Version: 5.6.11-1ubuntu3.4

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `oglasnik_facelift`
--

-- --------------------------------------------------------

--
-- Table structure for table `users_shops_featured_homepage`
--

CREATE TABLE IF NOT EXISTS `users_shops_featured_homepage` (
  `id` bigint(20) unsigned NOT NULL,
  `users_shops_featured_id` bigint(20) unsigned NOT NULL,
  `expires_at` int(10) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `modified_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- RELATIONS FOR TABLE `users_shops_featured_homepage`:
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users_shops_featured_homepage`
--
ALTER TABLE `users_shops_featured_homepage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `active_featured_shop_expiry` (`users_shops_featured_id`,`expires_at`,`active`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users_shops_featured_homepage`
--
ALTER TABLE `users_shops_featured_homepage`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;SET FOREIGN_KEY_CHECKS=1;
COMMIT;
