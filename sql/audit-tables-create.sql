-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.0.12-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oglasnik_prod.audit
CREATE TABLE IF NOT EXISTS `audit` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `username` varchar(64) COLLATE utf8_croatian_ci NOT NULL,
  `model_name` varchar(64) COLLATE utf8_croatian_ci DEFAULT NULL,
  `model_pk_val` bigint(20) unsigned DEFAULT NULL,
  `controller` varchar(64) COLLATE utf8_croatian_ci DEFAULT NULL,
  `action` varchar(64) COLLATE utf8_croatian_ci DEFAULT NULL,
  `params` text COLLATE utf8_croatian_ci,
  `message` text COLLATE utf8_croatian_ci NOT NULL,
  `ip` varchar(45) COLLATE utf8_croatian_ci NOT NULL,
  `type` char(1) COLLATE utf8_croatian_ci NOT NULL COMMENT 'C=Create, D=Delete, U=Update',
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `audit_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- Data exporting was unselected.


-- Dumping structure for table oglasnik_prod.audit_detail
CREATE TABLE IF NOT EXISTS `audit_detail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `audit_id` int(11) unsigned NOT NULL,
  `field_name` varchar(32) COLLATE utf8_croatian_ci NOT NULL,
  `old_value` text COLLATE utf8_croatian_ci,
  `new_value` text COLLATE utf8_croatian_ci,
  PRIMARY KEY (`id`),
  KEY `audit_id` (`audit_id`),
  CONSTRAINT `audit_detail_ibfk_1` FOREIGN KEY (`audit_id`) REFERENCES `audit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
