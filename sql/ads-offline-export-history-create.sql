--
-- Table structure for table `ads_offline_export_history`
--

CREATE TABLE IF NOT EXISTS `ads_offline_export_history` (
  `id` bigint(20) unsigned NOT NULL,
  `ad_id` bigint(20) unsigned NOT NULL,
  `appearance_date` varchar(255) COLLATE utf8_croatian_ci NOT NULL,
  `product` text COLLATE utf8_croatian_ci NOT NULL,
  `exported_id` bigint(20) DEFAULT NULL,
  `processed` tinyint(1) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- RELATIONS FOR TABLE `ads_offline_export_history`:
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ads_offline_export_history`
--
ALTER TABLE `ads_offline_export_history`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ad_appearance_date` (`ad_id`,`appearance_date`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ads_offline_export_history`
--
ALTER TABLE `ads_offline_export_history`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;SET FOREIGN_KEY_CHECKS=1;
COMMIT;
