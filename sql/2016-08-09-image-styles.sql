INSERT INTO `image_styles` (`id`, `slug`, `width`, `height`, `crop`, `watermark`) 
VALUES 
(NULL, 'CMS-1500x400', '1500', '400', '1', '0'), 
(NULL, 'CMS-800x450', '800', '450', '0', '0'), 
(NULL, 'CMS-300x200', '300', '200', '1', '0'), 
(NULL, 'CMS-70x70', '70', '70', '1', '0');
