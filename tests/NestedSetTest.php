<?php

// TODO: test all the methods with examples from the README:
// https://github.com/braska/incubator/tree/master/Library/Phalcon/Mvc/Model/Behavior

use TestCategories as Categories;
use TestCategoriesManyRoots as CatMR;
use TestDictionaries as Dictionaries;
use TestParameters as Parameters;
use TestParametersTypes as ParametersTypes;

class NestedSetTest extends ModelsTestCase
{

    private function insertSingleRootFixtures()
    {
        $model = new Categories();
        $table = $model->getSource();
        $sql = <<<SQL
INSERT INTO `{$table}` (`id`, `parent_id`, `lft`, `rght`, `root_id`, `level`, `name`)
VALUES
	(1, NULL, 1, 12, NULL, 1, 'Root'),
	(2, 1, 2, 11, NULL, 2, 'Child 1'),
	(3, 2, 3, 4, NULL, 3, 'Grandchild 1'),
	(4, 2, 5, 8, NULL, 3, 'Grandchild 2'),
	(5, 2, 9, 10, NULL, 3, 'Grandchild 3'),
	(6, 4, 6, 7, NULL, 4, 'Even deeper');
SQL;
        $db = self::$di->get('db');
        $db->execute($sql);
    }

    private function insertMultiRootFixtures()
    {
        $cat_mr = new CatMR();
        $table = $cat_mr->getSource();
        $sql = <<<SQL
INSERT INTO `{$table}` (`id`, `parent_id`, `lft`, `rght`, `root_id`, `level`, `name`)
VALUES
	(1, null, 1, 12, 1, 1, 'Mobile Phones'),
	(2, null, 1, 8, 2, 1, 'Cars'),
	(3, 2, 4, 5, 2, 2, 'Ford'),
	(4, 2, 6, 7, 2, 2, 'Mercedes'),
	(5, 2, 2, 3, 2, 2, 'Audi'),
	(6, 1, 4, 9, 1, 2, 'Samsung'),
	(7, 1, 10, 11, 1, 2, 'Motorola'),
	(8, 1, 2, 3, 1, 2, 'iPhone'),
	(9, 6, 7, 8, 1, 3, 'X100'),
	(10, 6, 5, 6, 1, 3, 'C200');
SQL;
        $db = self::$di->get('db');
        $db->execute($sql);
    }

    private function insertDictionariesFixtures()
    {
        $model = new Dictionaries();
        $table = $model->getSource();
        $sql = <<<SQL
INSERT INTO `{$table}` (`id`, `parent_id`, `root_id`, `lft`, `rght`, `level`, `name`, `active`) VALUES
	(1, NULL, 1, 1, 18, 1, 'Auti - Oblik karoserije', 1),
	(2, NULL, 2, 1, 22, 1, 'Auti - Marka i model', 1),
	(3, NULL, 3, 1, 18, 1, 'HIFI - Proizvođači', 1),
	(6, NULL, 2, 2, 13, 2, 'Audi', 0),
	(7, NULL, 2, 14, 21, 2, 'Mercedes', 1),
	(8, NULL, 2, 3, 4, 3, 'A2', 1),
	(9, NULL, 2, 5, 6, 3, 'A3', 1),
	(10, NULL, 2, 15, 16, 3, 'C500', 1),
	(11, NULL, 2, 7, 8, 3, 'A4', 1),
	(12, NULL, 2, 9, 10, 3, 'A5', 0),
	(13, NULL, 2, 11, 12, 3, 'A6', 1),
	(14, NULL, 2, 19, 20, 3, 'CLK 500', 1),
	(15, NULL, 2, 17, 18, 3, 'SLR', 1),
	(16, NULL, 3, 2, 15, 2, 'LG', 1),
	(17, NULL, 3, 16, 17, 2, 'Zračni jastuci', 1),
	(18, NULL, 3, 3, 8, 3, 'Televizori', 1),
	(19, NULL, 3, 9, 14, 3, 'Zvučnici', 1),
	(20, NULL, 3, 4, 5, 4, '32"', 1),
	(21, NULL, 3, 6, 7, 4, '40"', 1),
	(22, NULL, 3, 10, 11, 4, '100W', 1),
	(23, NULL, 3, 12, 13, 4, '200W', 1),
	(24, NULL, 1, 2, 3, 2, 'limuzina', 1),
	(25, NULL, 1, 4, 5, 2, 'hatchback', 1),
	(26, NULL, 26, 1, 36, 1, 'Auti - Dodatna oprema', 1),
	(27, NULL, 26, 2, 3, 2, '3. stop svjetlo', 1),
	(28, NULL, 26, 4, 5, 2, '4x4', 1),
	(29, NULL, 26, 6, 7, 2, 'aluminijski naplatci', 1),
	(30, NULL, 26, 8, 9, 2, 'svjetla za maglu', 1),
	(31, NULL, 26, 10, 11, 2, 'sportsko podvozje', 1),
	(32, NULL, 26, 12, 13, 2, 'nadzor pritiska u pneumaticima', 1),
	(33, NULL, 26, 14, 15, 2, 'ksenonska svjetla', 1),
	(34, NULL, 26, 16, 17, 2, 'bi-ksenonska svjetla', 1),
	(35, NULL, 26, 18, 19, 2, 'navigacija', 1),
	(36, NULL, 26, 20, 21, 2, 'navigacija + TV', 1),
	(37, NULL, 26, 22, 23, 2, 'putno računalo', 1),
	(38, NULL, 26, 24, 25, 2, 'mrežasta pregrada prtljažnika', 1),
	(39, NULL, 26, 26, 27, 2, 'krovni nosači', 1),
	(40, NULL, 26, 28, 29, 2, 'kuka za vuču', 1),
	(41, NULL, 26, 30, 31, 2, 'zatamnjena stakla', 1),
	(42, NULL, 26, 32, 33, 2, 'upravljač presvučen kožom', 1),
	(43, NULL, 26, 34, 35, 2, 'krovni prozor', 1),
	(44, NULL, 44, 1, 24, 1, 'Auti - Sigurnost', 1),
	(45, NULL, 44, 2, 3, 2, 'ABS', 1),
	(46, NULL, 44, 4, 5, 2, 'ESP', 1),
	(47, NULL, 44, 6, 7, 2, 'EDC', 1),
	(48, NULL, 44, 8, 9, 2, 'ETS', 1),
	(49, NULL, 44, 10, 11, 2, 'ASR', 1),
	(50, NULL, 44, 12, 13, 2, 'ASD', 1),
	(51, NULL, 44, 14, 15, 2, 'samozatezajući sigurnosni pojasevi', 1),
	(52, NULL, 44, 16, 17, 2, 'isofix (sustav vezanja sjedalice za dijete)', 1),
	(53, NULL, 44, 18, 19, 2, 'vozački zračni jastuk', 1),
	(54, NULL, 44, 20, 21, 2, 'vozački i suvozački zračni jastuk', 1),
	(55, NULL, 44, 22, 23, 2, 'vozački, suvozački i bočni zračni jastuci', 1),
	(56, NULL, 1, 6, 7, 2, 'karavan', 1),
	(57, NULL, 1, 8, 9, 2, 'monovolumen', 1),
	(58, NULL, 1, 10, 11, 2, 'coupe', 1),
	(59, NULL, 1, 12, 13, 2, 'kabriolet', 1),
	(60, NULL, 1, 14, 15, 2, 'terenac/SUV', 1),
	(61, NULL, 1, 16, 17, 2, 'pick-up', 1),
	(62, NULL, 62, 1, 12, 1, 'Auti - Sigurnost od krađe', 1),
	(63, NULL, 62, 2, 3, 2, 'alarm', 1),
	(64, NULL, 62, 4, 5, 2, 'blokada motora', 1),
	(65, NULL, 62, 6, 7, 2, 'centralno zaključavanje', 1),
	(66, NULL, 62, 8, 9, 2, 'centralno zaključavanje s daljinskim', 1),
	(67, NULL, 62, 10, 11, 2, 'multilock', 1),
	(68, NULL, 68, 1, 12, 1, 'Auti - Vrsta goriva', 1),
	(69, NULL, 68, 2, 3, 2, 'benzin', 1),
	(70, NULL, 68, 4, 5, 2, 'diesel', 1),
	(71, NULL, 68, 6, 7, 2, 'benzin + plin', 1),
	(72, NULL, 68, 8, 9, 2, 'diesel + plin', 1),
	(73, NULL, 68, 10, 11, 2, 'hibrid', 1),
	(74, NULL, 74, 1, 8, 1, 'Auti - Pogon', 1),
	(75, NULL, 74, 2, 3, 2, 'prednji', 1),
	(76, NULL, 74, 4, 5, 2, 'zadnji', 1),
	(77, NULL, 74, 6, 7, 2, '4x4', 1),
	(78, NULL, 78, 1, 10, 1, 'Auti - Vrsta mjenjača', 1),
	(79, NULL, 78, 2, 3, 2, 'mehanički', 1),
	(80, NULL, 78, 4, 5, 2, 'automatski', 1),
	(81, NULL, 78, 6, 7, 2, 'automatski sekvencijski', 1),
	(82, NULL, 78, 8, 9, 2, 'sekvencijski', 1),
	(83, NULL, 83, 1, 10, 1, 'Auti - Broj brzina', 1),
	(84, NULL, 83, 2, 3, 2, '4', 1),
	(85, NULL, 83, 4, 5, 2, '5', 1),
	(86, NULL, 83, 6, 7, 2, '6', 1),
	(87, NULL, 83, 8, 9, 2, '7', 1),
	(88, NULL, 88, 1, 12, 1, 'Auti - Broj vrata', 1),
	(89, NULL, 88, 2, 3, 2, '2', 1),
	(90, NULL, 88, 4, 5, 2, '3', 1),
	(91, NULL, 88, 6, 7, 2, '4', 1),
	(92, NULL, 88, 8, 9, 2, '5', 1),
	(93, NULL, 88, 10, 11, 2, '6', 1),
	(94, NULL, 94, 1, 50, 1, 'Auti - Udobnost', 1),
	(95, NULL, 94, 2, 3, 2, 'servo upravljač', 1),
	(96, NULL, 94, 4, 5, 2, 'upravljač podesiv po visini', 1),
	(97, NULL, 94, 6, 7, 2, 'komande na upravljaču', 1),
	(98, NULL, 94, 8, 9, 2, 'el. podizanje prednjih stakala', 1),
	(99, NULL, 94, 10, 11, 2, 'el. podizanje stražnjih stakala', 1),
	(100, NULL, 94, 12, 13, 2, 'grijanje ogledala', 1),
	(101, NULL, 94, 14, 15, 2, 'el. podešavanje ogledala', 1),
	(102, NULL, 94, 16, 17, 2, 'el. sklapanje ogledala', 1),
	(103, NULL, 94, 18, 19, 2, 'el. podešavanje sjedala', 1),
	(104, NULL, 94, 20, 21, 2, 'djeljiva stražnja klupa', 1),
	(105, NULL, 94, 22, 23, 2, 'središnji naslon za ruku', 1),
	(106, NULL, 94, 24, 25, 2, 'kožna sjedala', 1),
	(107, NULL, 94, 26, 27, 2, 'sportska sjedala', 1),
	(108, NULL, 94, 28, 29, 2, 'parkirni senzori', 1),
	(109, NULL, 94, 30, 31, 2, 'tempomat', 1),
	(110, NULL, 94, 32, 33, 2, 'priprema za mobilni aparat', 1),
	(111, NULL, 94, 34, 35, 2, 'handsfree', 1),
	(112, NULL, 94, 36, 37, 2, 'CD izmjenjivač', 1),
	(113, NULL, 94, 38, 39, 2, 'ručna klima', 1),
	(114, NULL, 94, 40, 41, 2, 'automatska klima', 1),
	(115, NULL, 94, 42, 43, 2, 'dvozonska klima', 1),
	(116, NULL, 94, 44, 45, 2, 'autoradio/CD', 1),
	(117, NULL, 94, 46, 47, 2, 'autoradio/CD/mp3', 1),
	(118, NULL, 94, 48, 49, 2, 'autoradio/CD/mp3/DVD', 1),
	(119, NULL, 119, 1, 6, 1, 'Auti - Novo/Rabljeno', 1),
	(120, 119, 119, 2, 3, 2, 'Novo', 1),
	(121, 119, 119, 4, 5, 2, 'Rabljeno', 1),
	(122, NULL, 122, 1, 12, 1, 'Auti - Stanje', 1),
	(123, NULL, 122, 2, 3, 2, 'odlično', 1),
	(124, NULL, 122, 4, 5, 2, 'dobro', 1),
	(125, NULL, 122, 6, 7, 2, 'osrednje', 1),
	(126, NULL, 122, 8, 9, 2, 'slabo', 1),
	(127, NULL, 122, 10, 11, 2, 'karambol/za dijelove', 1),
	(128, NULL, 128, 1, 8, 1, 'Auti - Vlasnik', 1),
	(129, NULL, 128, 2, 3, 2, 'prvi', 1),
	(130, NULL, 128, 4, 5, 2, 'drugi', 1),
	(131, NULL, 128, 6, 7, 2, 'treći i više', 1),
	(132, NULL, 132, 1, 42, 1, 'Auti - Boja', 1),
	(133, NULL, 132, 2, 3, 2, 'bež', 1),
	(134, NULL, 132, 4, 5, 2, 'bijela', 1),
	(135, NULL, 132, 6, 7, 2, 'crna', 1),
	(136, NULL, 132, 8, 9, 2, 'crvena', 1),
	(137, NULL, 132, 10, 11, 2, 'bordo crvena', 1),
	(138, NULL, 132, 12, 13, 2, 'ljubičasta', 1),
	(139, NULL, 132, 14, 15, 2, 'narančasta', 1),
	(140, NULL, 132, 16, 17, 2, 'plava', 1),
	(141, NULL, 132, 18, 19, 2, 'svijetlo plava', 1),
	(142, NULL, 132, 20, 21, 2, 'tamno plava', 1),
	(143, NULL, 132, 22, 23, 2, 'siva', 1),
	(144, NULL, 132, 24, 25, 2, 'svijetlo siva', 1),
	(145, NULL, 132, 26, 27, 2, 'tamno siva', 1),
	(146, NULL, 132, 28, 29, 2, 'smeđa', 1),
	(147, NULL, 132, 30, 31, 2, 'srebrna', 1),
	(148, NULL, 132, 32, 33, 2, 'zelena', 1),
	(149, NULL, 132, 34, 35, 2, 'svijetlo zelena', 1),
	(150, NULL, 132, 36, 37, 2, 'tamno zelena', 1),
	(151, NULL, 132, 38, 39, 2, 'zlatna', 1),
	(152, NULL, 132, 40, 41, 2, 'žuta', 1),
	(154, NULL, 154, 1, 4, 1, 'one more new dict', 1),
	(155, 154, 154, 2, 3, 2, 'test child node', 1);
SQL;
        $db = self::$di->get('db');
        $db->execute($sql);
    }

    private function insertParametersFixtures()
    {
        $model = new Parameters();
        $table = $model->getSource();
        $sql = <<<SQL
INSERT INTO `{$table}` (`id`, `name`, `type_id`, `dictionary_id`) VALUES
	(1, 'Auti - Oblik karoserije', 'DROPDOWN', 1),
	(2, 'HIFI - Proizvođači', 'DEPENDABLE_DROPDOWN', 3),
	(3, 'Auti - Marka i model vozila', 'DEPENDABLE_DROPDOWN', 2),
	(5, 'Auti - Dodatna oprema', 'CHECKBOXGROUP', 26),
	(6, 'Auti - Sigurnost', 'CHECKBOXGROUP', 44),
	(7, 'Auti - Sigurnost od krađe', 'CHECKBOXGROUP', 62),
	(8, 'Auti - Vrsta goriva', 'DROPDOWN', 68),
	(9, 'Auti - Tip vozila', 'TEXT', NULL),
	(10, 'Auti - Snaga motora (kW)', 'NUMBER', NULL),
	(11, 'Auti - Radni obujam (cm3)', 'NUMBER', NULL),
	(12, 'Auti - Pogon', 'DROPDOWN', 74),
	(13, 'Auti - Vrsta mjenjača', 'DROPDOWN', 78),
	(14, 'Auti - Broj brzina', 'DROPDOWN', 83),
	(15, 'Auti - Broj vrata', 'DROPDOWN', 88),
	(16, 'Auti - Udobnost', 'CHECKBOXGROUP', 94),
	(17, 'Auti - Novo/Rabljeno', 'RADIO', 119),
	(18, 'Auti - Oldtimer', 'CHECKBOX', NULL),
	(19, 'Auti - Stanje', 'DROPDOWN', 122),
	(20, 'Auti - Boja', 'DROPDOWN', 132),
	(21, 'Auti - Vlasnik', 'DROPDOWN', 128),
	(22, 'Auti - Servisna knjižica', 'CHECKBOX', NULL),
	(23, 'Auti - Garažiran', 'CHECKBOX', NULL),
	(24, 'Youtube video', 'URL', NULL),
	(25, 'Ad title', 'TITLE', NULL),
	(27, 'Ad price', 'CURRENCY', NULL),
	(28, 'Ad description', 'DESCRIPTION', NULL),
	(29, 'Datum rođenja', 'DATE', NULL),
	(30, 'Slobodni termin', 'DATEINTERVAL', NULL),
	(31, 'Ad location', 'LOCATION', NULL),
	(32, 'Ad pictures', 'PICTURE', NULL),
	(33, 'Auti - Godina proizvodnje', 'YEAR', NULL),
	(34, 'Auti - Registriran do', 'MONTHYEAR', NULL),
	(35, 'Test param', 'DROPDOWN', 154);
SQL;
        $db = self::$di->get('db');
        $db->execute($sql);
    }

    // An "invalid-on-purpose" multi-root fixture in which
    // nodes need to be moved around in order to test node move operations etc.
    private function insertWrongMultiRootFixtures()
    {
        $cat_mr = new CatMR();
        $table = $cat_mr->getSource();
        $sql = <<<SQL
INSERT INTO `{$table}` (`id`, `parent_id`, `lft`, `rght`, `root_id`, `level`, `name`)
VALUES
	(1, null, 1, 12, 1, 1, 'Mobile Phones'),
	(2, null, 1, 8, 2, 1, 'Cars'),
	(3, null, 8, 9, 1, 2, 'Ford'),
	(4, null, 10, 11, 1, 2, 'Mercedes'),
	(5, null, 2, 7, 1, 2, 'Audi'),
	(6, null, 4, 5, 2, 2, 'Samsung'),
	(7, null, 6, 7, 2, 2, 'Motorola'),
	(8, null, 2, 3, 2, 2, 'iPhone'),
	(9, null, 5, 6, 1, 3, 'X100'),
	(10, null, 3, 4, 1, 3, 'C200');
SQL;
        $db = self::$di->get('db');
        $db->execute($sql);
    }

    public function testNestedSetBehaviorBasicsWithSingleRoot()
    {
        $category = new Categories();
        $category->name = 'Test category';

        $this->assertTrue($category->saveNode());

        $this->assertEquals(1, $category->lft);
        $this->assertEquals(2, $category->rght);
        $this->assertEquals(1, $category->level);
        $this->assertEquals('Test category', $category->name);
        // $this->assertEquals(null, $category->root_id);

        // TODO: add more tests maybe? :)

        // deleteNode deletes the node + descendants
        $this->assertTrue($category->deleteNode());
    }

    public function testThrowsOnNonBehaviorMethodUsageOfUpdate()
    {
        $test = new Categories();
        $test->name = 'Something';
        $test->saveNode();

        $test->name = 'Something (edit)';
        $test->level = 10;
        $test->lft = 11;
        $test->rght = 100;
        try {
            $this->assertFalse($test->update());
        } catch (\Phalcon\Mvc\Model\Exception $e) {
            return;
        }

        $this->fail('Expected Exception was not caught');
    }

    public function testThrowsOnNonBehaviorMethodUsageOfDelete()
    {
        $test = new Categories();
        $test->name = 'Something To Delete';
        $test->saveNode();

        try {
            $this->assertNull($test->delete());
        } catch (\Phalcon\Mvc\Model\Exception $e) {
            return;
        }

        $this->fail('Expected Exception was not caught');
    }

    /**
     * @expectedException \Phalcon\Mvc\Model\Exception
     * @expectedExceptionMessage You should not use `TestCategories::beforeCreate` when `Baseapp\Library\Behavior\NestedSet` is attached. Use the methods of behavior.
     */
    public function testThrowsOnNonBehaviorMethodUsageOfCreate()
    {
        $test = new Categories();
        $test->name = 'Something to create';
        // $test->setDirtyState(\Phalcon\Mvc\Model::DIRTY_STATE_DETACHED);
        // $test->setDirtyState(\Phalcon\Mvc\Model::DIRTY_STATE_TRANSIENT);
        $test->setDirtyState(\Phalcon\Mvc\Model::DIRTY_STATE_PERSISTENT);
        $test->setSnapshotData($test->toArray());
        $this->assertFalse($test->create());
    }

    /**
     * @expectedException \Phalcon\Mvc\Model\Exception
     */
    public function testThrowsOnMultipleRootsWithoutHasManyRoots()
    {
        $root1 = new Categories();
        $root1->name = 'Mobile Phones';

        $root1->saveNode();

        $root2 = new Categories();
        $root2->name = 'Cars';
        $root2->saveNode();
    }

    public function testAllowsManyRootsWhenConfigured()
    {
        $root1 = new CatMR();
        $root1->name = 'Mobile Phones';

        $this->assertTrue($root1->saveNode());

        $root2 = new CatMR();
        $root2->name = 'Cars';
        $this->assertTrue($root2->saveNode());
    }

    public function testSingleRootBasicNodeOperations()
    {
        $root = new Categories();
        $root->name = 'Root';
        $root->saveNode();

        $child = new Categories();
        $child->name = 'Child 1';
        $child->appendTo($root);

        $grandchild1 = new Categories();
        $grandchild1->name = 'Grandchild 1';
        $grandchild1->appendTo($child);

        $grandchild2 = new Categories();
        $grandchild2->name = 'Grandchild 2';
        $grandchild2->insertAfter($grandchild1);

        $grandchild3 = new Categories();
        $grandchild3->name = 'Grandchild 3';
        $grandchild3->insertAfter($grandchild2);

        $more = new Categories();
        $more->name = 'Even deeper';
        $more->appendTo($grandchild2);

        $this->assertEquals(5, $grandchild2->lft);
        $this->assertEquals(3, $grandchild1->lft);
        $this->assertEquals(7, $grandchild3->lft);
    }
/*
    public function disabledtestGetTreeWithChildrenWithSingleRoot()
    {
        $this->insertSingleRootFixtures();
        $categories = new Categories();
        var_dump($categories->getTreeWithChildren());
    }

    public function disabledtestGetTreeWithChildrenWithManyRoots()
    {
        $this->insertMultiRootFixtures();
        $cat_mr = new CatMR();
        var_dump($cat_mr->getTreeWithChildren(3));
    }
*/
    public function testMoveNodesInSingleRootMode()
    {
        $root = new Categories();
        $root->name = 'Root';
        $root->saveNode();

        $phones = new Categories();
        $phones->name = 'Mobile Phones';
        $root->append($phones);

        $cars = new Categories();
        $cars->name = 'Cars';
        $root->append($cars);

        $child = new Categories();
        $child->name = 'Child';
        $child->appendTo($cars);

        $child->refresh();;
        $phones->refresh();
        $cars->refresh();

        $this->assertEquals(false, $child->isDescendantOf($phones), '"Child" is not a descendant of "Mobile PHones"');
        $this->assertEquals(true, $child->isDescendantOf($cars), '"Child" is a descendant of "Cars"');

        $child->moveAsFirst($phones);

        $child->refresh();
        $phones->refresh();
        $cars->refresh();

        $this->assertEquals(true, $child->isDescendantOf($phones), '"Child" is now a descendant of "Mobile PHones"');
        $this->assertEquals(false, $child->isDescendantOf($cars), '"Child" is not a descendant of "Cars" any more');
    }

    public function testMoveNodeBetweenManyRoots()
    {
        $phones = new CatMR();
        $phones->name = 'Phones';
        $phones->saveNode();

        $cars = new CatMR();
        $cars->name = 'Cars';
        $cars->saveNode();

        $child = new CatMR();
        $child->name = 'Child';
        $child->appendTo($cars);

        // without refresh we get stale values since the behavior
        // internally updates database columns and those changes aren't
        // propagated to existing instances (for that we'd need a manager object which of some kind)
        $child->refresh();
        $phones->refresh();
        $cars->refresh();

        $this->assertEquals('Phones', $phones->name, '"Phones" is properly saved');
        $this->assertEquals('Child', $child->name, '"Child" is properly saved');
        $this->assertEquals('Cars', $cars->name, '"Cars" is properly saved');

        $this->assertEquals(false, $child->isDescendantOf($phones), '"Child" is not a descendant of "PHones"');
        $this->assertEquals(true, $child->isDescendantOf($cars), '"Child" is a descendant of "Cars"');

        $child->moveAsFirst($phones);

        $phones->refresh();
        $cars->refresh();
        $child->refresh();

        $this->assertEquals('Phones', $phones->name, '"Phones" is properly saved');
        $this->assertEquals('Child', $child->name, '"Child" is properly saved');
        $this->assertEquals('Cars', $cars->name, '"Cars" is properly saved');

        $this->assertEquals(false, $child->isDescendantOf($cars), '"Child" should not be a descendant of "Cars" after the move');
        $this->assertEquals(true, $child->isDescendantOf($phones), '"Child" should be a descendant of "Phones" after the move');
    }

    public function testTreeManipulationsWithManyRoots()
    {
        $root1 = new CatMR();
        $root1->name = 'Mobile Phones';
        $root1->saveNode();

        $root2 = new CatMR();
        $root2->name = 'Cars';
        $root2->saveNode();

        $category1 = new CatMR();
        $category1->name = 'Ford';

        $category2 = new CatMR();
        $category2->name = 'Mercedes';

        $category3 = new CatMR();
        $category3->name = 'Audi';

        $category1->appendTo($root1);
        $category2->insertAfter($category1);
        $category3->insertBefore($category1);

        $root1->refresh();
        $root2->refresh();

        // order of above is wrong on purpose, we'll fix it later

        $category1 = new CatMR();
        $category1->name = 'Samsung';
        $category2 = new CatMR();
        $category2->name = 'Motorola';
        $category3 = new CatMR();
        $category3->name = 'iPhone';

        $category1->appendTo($root2);
        $category2->insertAfter($category1);
        $category3->prependTo($root2);

        $category1 = new CatMR();
        $category1->name = 'X100';
        $category2 = new CatMR();
        $category2->name = 'C200';
        $audi = CatMR::findFirst("name = 'Audi'");
        $category1->appendTo($audi);
        $category2->prependTo($audi);

//        - 1. Mobile phones
//            - 5. Audi
//                - 10. С200
//                - 9. X100
//            - 3. Ford
//            - 4. Mercedes
//        - 2. Cars
//            - 8. Iphone
//            - 6. Samsung
//            - 7. Motorola

        $root1->refresh();
        $root2->refresh();

        // now lets make our tree as it should be

        // move phones to the proper place
        $samsung = CatMR::findFirst("name = 'Samsung'");
        $x100 = CatMR::findFirst("name = 'X100'");
        $x100->moveAsFirst($samsung);

        // WARN: if we don't re-fetch x100 it doesn't get moved
        // properly since the moveAsFirst() above doesn't really update
        // all the model/owner properties on move operations at all!!!1
        $x100 = CatMR::findFirst("name = 'X100'");
        $c200 = CatMR::findFirst("name = 'C200'");
        $c200->moveBefore($x100);

        // now move all Samsung phones branch
        $samsung = CatMR::findFirst("name = 'Samsung'");
        $mobile_phones = CatMR::findFirst("name = 'Mobile Phones'");
        $samsung->moveAsFirst($mobile_phones);

        // move the rest of phone models
        $iphone = CatMR::findFirst("name = 'Iphone'");
        $iphone->moveAsFirst($mobile_phones);

        $samsung = CatMR::findFirst("name = 'Samsung'");
        $motorola = CatMR::findFirst("name = 'Motorola'");
        $motorola->moveAfter($samsung);

        // move car models to appropriate place
        $cars = CatMR::findFirst("name = 'Cars'");
        $audi = CatMR::findFirst("name = 'Audi'");
        $audi->moveAsLast($cars);

        $cars = CatMR::findFirst("name = 'Cars'");
        $ford = CatMR::findFirst("name = 'Ford'");
        $ford->moveAsLast($cars);

        $cars = CatMR::findFirst("name = 'Cars'");
        $mercedes = CatMR::findFirst("name = 'Mercedes'");
        $mercedes->moveAsLast($cars);

        // WARN: We cannot instantiate and reuse the required nodes only once
        // and then use them... if we do,
        // all hell breaks loose, since each node sees the old left/right
        // values before the various sql updates took place... (mainly related to node move operations)

        // This might be because of transactions? or just that we're using $owner
        // inside the behavior, and $owner sees old values... in theory, every
        // direct db query should refresh the $owner, but not sure what other
        // problems that might cause... or introduce more queries aka performance loss
/*
        $cars = CatMR::findFirst("name = 'Cars'");
        $mercedes = CatMR::findFirst("name = 'Mercedes'");
        $ford = CatMR::findFirst("name = 'Ford'");
        $audi = CatMR::findFirst("name = 'Audi'");

        //foreach (array($mercedes, $ford, $audi) as $cat) { // All 3 present, but reversed order, WTF?
        //foreach (array($mercedes, $audi, $ford) as $cat) { // Ford missing, what the fuck is going on?
        //foreach (array($audi, $ford, $mercedes) as $cat) { // Ford missing, WHAT THE ACTUAL FUCK?
        //foreach (array($audi, $mercedes, $ford) as $cat) { // Ford is lost, why?
        //foreach (array($ford, $audi, $mercedes) as $cat) { // Mercedes is now gone, why?
        //foreach (array($ford, $mercedes, $audi) as $cat) { // Mercedes missing...

        foreach (array($audi, $mercedes, $ford) as $cat) {
            // var_dump($cat->toArray());
            // $cars = CatMR::findFirst("name = 'Cars'");
            $cat->moveAsLast($cars);
        }
*/

        // new tree should now look like this:
/*

1. Mobile Phones
  8. iPhone
  6. Samsung
    10. C200
    9.  X100
  7. Motorola
2. Cars
  5. Audi
  3. Ford
  4. Mercedes
*/

        $root1->refresh();
        $root2->refresh();

        // finally, asserting that things are as they should be

        $this->assertEquals(2, count($root1->roots()), 'There are 2 separate roots in the tree');
        $this->assertEquals(5, count($root1->descendants()->toArray()), 'There are 5 descendants in the Mobile Phones tree');
        $this->assertEquals(3, count($root2->descendants()->toArray()), 'There are 3 descendants in the Cars tree');

        $samsung = CatMR::findFirst("name='Samsung'");
        $this->assertEquals(2, count($samsung->children()->toArray()), 'Samsung node has 2 child nodes');
        $c200 = CatMR::findFirst("name='C200'");
        $this->assertTrue($c200->isDescendantOf($samsung), 'C200 is a descendant of Samsung');

        $this->assertNotEquals(false, $c200->next(), 'C200 has a next sibling');
        $this->assertEquals(false, $c200->prev(), 'C200 has no previous sibling');

        $ford = CatMR::findFirst("name='Ford'");
        $this->assertEquals($ford->parent()->toArray(), $root2->toArray(), 'Ford\'s parent node equals root node of the tree');
        $this->assertNotEquals(false, $ford->prev(), 'Ford has a previous sibling');
        $this->assertNotEquals(false, $ford->next(), 'Ford has a next sibling');
    }

    public function testTreeManipulationsWithSingleRoot()
    {
        $root = new Categories();
        $root->name = 'Root';
        $root->saveNode();

        $mobiles = new Categories();
        $mobiles->name = 'Mobile Phones';
        $root->append($mobiles);

        $cars = new Categories();
        $cars->name = 'Cars';
        $root->append($cars);

        $category1 = new Categories();
        $category1->name = 'Ford';

        $category2 = new Categories();
        $category2->name = 'Mercedes';

        $category3 = new Categories();
        $category3->name = 'Audi';

        $category1->appendTo($mobiles);
        $category2->insertAfter($category1);
        $category3->insertBefore($category1);

        $root->refresh();
        $mobiles->refresh();
        $cars->refresh();

        // order of above is wrong on purpose, we'll fix it later

        $category1 = new Categories();
        $category1->name = 'Samsung';
        $category2 = new Categories();
        $category2->name = 'Motorola';
        $category3 = new Categories();
        $category3->name = 'iPhone';

        $category1->appendTo($cars);
        $category2->insertAfter($category1);
        $category3->prependTo($cars);

        $category1 = new Categories();
        $category1->name = 'X100';
        $category2 = new Categories();
        $category2->name = 'C200';
        $audi = Categories::findFirst("name = 'Audi'");
        $category1->appendTo($audi);
        $category2->prependTo($audi);

//        - 1. Root
//            - 3. Cars
//                - 9. Iphone
//                - 7. Samsung
//                - 8. Motorola
//            - 2. Mobile Phones
//                - 6. Audi
//                    - 11. C200
//                    - 10. X100
//                - 4. Ford
//                - 5. Mercedes

        $root->refresh();
        $cars->refresh();
        $mobiles->refresh();

        // now lets make our tree as it should be

        // move phones to the proper place
        $samsung = Categories::findFirst("name = 'Samsung'");
        $x100 = Categories::findFirst("name = 'X100'");
        $x100->moveAsFirst($samsung);

        // WARN: if we don't re-fetch x100 it doesn't get moved
        // properly since the moveAsFirst() above doesn't really update
        // all the model/owner properties on move operations at all!!!1
        $x100 = Categories::findFirst("name = 'X100'");
        $c200 = Categories::findFirst("name = 'C200'");
        $c200->moveBefore($x100);

        // now move all Samsung phones branch
        $samsung = Categories::findFirst("name = 'Samsung'");
        $mobile_phones = Categories::findFirst("name = 'Mobile Phones'");
        $samsung->moveAsFirst($mobile_phones);

        // move the rest of phone models
        $iphone = Categories::findFirst("name = 'Iphone'");
        $iphone->moveAsFirst($mobile_phones);

        $samsung = Categories::findFirst("name = 'Samsung'");
        $motorola = Categories::findFirst("name = 'Motorola'");
        $motorola->moveAfter($samsung);

        // move car models to appropriate place
        $cars = Categories::findFirst("name = 'Cars'");
        $audi = Categories::findFirst("name = 'Audi'");
        $audi->moveAsLast($cars);

        $cars = Categories::findFirst("name = 'Cars'");
        $ford = Categories::findFirst("name = 'Ford'");
        $ford->moveAsLast($cars);

        $cars = Categories::findFirst("name = 'Cars'");
        $mercedes = Categories::findFirst("name = 'Mercedes'");
        $mercedes->moveAsLast($cars);

        // WARN: We cannot instantiate and reuse the required nodes only once
        // and then use them... if we do,
        // all hell breaks loose, since each node sees the old left/right
        // values before the various sql updates took place... (mainly related to node move operations)

        // new tree should now look like this:
        /*
        1. Root
          3. Cars
            6. Audi
            4. Ford
            5. Mercedes
          2. Mobile Phones
            9. iPhone
            7. Samsung
              11. C200
              10. X100
            8. Motorola
        */

        $root->refresh();
        $cars->refresh();
        $mobiles->refresh();

        // finally, asserting that things are as they should be

        $this->assertEquals(1, count($root->roots()), 'There is only one root in the tree');
        $this->assertEquals(10, count($root->descendants()->toArray()), 'There are 5 descendants in the Mobile Phones tree');
        $this->assertEquals(5, count($mobiles->descendants()->toArray()), 'There are 5 descendants in the Mobile Phones subtree');
        $this->assertEquals(3, count($cars->descendants()->toArray()), 'There are 3 descendants in the Cars subtree');

        $samsung = Categories::findFirst("name='Samsung'");
        $this->assertEquals(2, count($samsung->children()->toArray()), 'Samsung node has 2 child nodes');
        $c200 = Categories::findFirst("name='C200'");
        $this->assertTrue($c200->isDescendantOf($samsung), 'C200 is a descendant of Samsung');

        $this->assertNotEquals(false, $c200->next(), 'C200 has a next sibling');
        $this->assertEquals(false, $c200->prev(), 'C200 has no previous sibling');

        $ford = Categories::findFirst("name='Ford'");
        $this->assertEquals($ford->parent()->toArray(), $cars->toArray(), 'Ford\'s parent node equals the $cars node');
        $this->assertNotEquals(false, $ford->prev(), 'Ford has a previous sibling');
        $this->assertNotEquals(false, $ford->next(), 'Ford has a next sibling');
    }

    public function testGetTreeSingleRoot()
    {
        $this->insertSingleRootFixtures();

        // Testing unscoped (should return entire tree) with hasManyRoots=false
        $model = new Categories();
        $tree = $model->getTree();

        $this->assertNotEmpty($tree, 'Tree structure is not empty');
        $this->assertEquals(7, count($tree), 'Tree contains 7 elements: 6 actual nodes + 1 "root" node which is always added');
        $this->assertInstanceOf('stdClass', $tree[1], 'Tree items are objects');
        $this->assertInstanceOf('stdClass', current($tree[1]->children), 'Tree node children nodes are objects');
        $this->assertEquals(1, count($tree[1]->children), '"Root" contains only 1 direct children node');
        $this->assertEquals(3, count($tree[2]->children), '"Child 1" contains 3 direct children');
        $this->assertEquals(1, count($tree[4]->children), '"Grandchild 2" has 1 child node');

        // Testing scoped getTreeWithChildren() with and without the scoped node itself
        $node = Categories::findFirst("name='Child 1'");
        $scoped_tree_with_self = $node->getTree();
        $this->assertNotEmpty($scoped_tree_with_self, 'Scoped tree (with self) is not empty');
        $this->assertEquals(6, count($scoped_tree_with_self), 'Scoped tree (with self) contains 6 elements: 5 actual nodes + 1 "root" node');
        // FIXME: Figure out a better way to have $node->getTree() but not having to do $node->getTree(null, false) for excluding self!
        $scoped_tree_without_self = $node->getTree(false);
        $this->assertNotEmpty($scoped_tree_without_self, 'Scoped tree (without self) is not empty');
        $this->assertEquals(5, count($scoped_tree_without_self), 'Scoped tree (without self) contains 5 elements: 5 actual nodes + 1 "root" node');
    }

    public function testGetTreeManyRoots()
    {
        $this->insertMultiRootFixtures();

        // Testing unscoped (all/multiple trees when hasManyRoots=true)
        $model = new CatMR();
        // These should return identical results in this setup
        $tree = $model->getTree();
        $trees = $model->getTrees();

        $this->assertNotEmpty($trees, 'Tree structure is not empty');
        $this->assertEquals(11, count($trees), 'Tree contains 11 elements: 10 actual nodes + 1 "root" fake node');
        $this->assertInstanceOf('stdClass', $trees[1], 'Tree items are objects');
        $this->assertInstanceOf('stdClass', current($trees[1]->children), 'Tree node children nodes are objects');
        $this->assertEquals($tree, $trees, 'getTree() and getTrees() return identical results');

        // Grab only a specific tree ("Cars" have id=2 in the multi root fixtures)
        $cars_tree = $model->getTree(2);
        $this->assertEquals(5, count($cars_tree), 'Cars tree contains 5 elements: 4 actual nodes + 1 "root" fake node');

        //var_dump($cars_tree);
        //exit;
    }

    public function testGetSelectablesFromTreeArray()
    {
        // Testing unscoped (entire tree) with hasManyRoots=false
        $this->insertSingleRootFixtures();
        $model = new Categories();
        $tree = $model->getTree();

        // "Selectables" are arrays keyed with primary key values currently

        $selectables1 = $model->getSelectablesFromTreeArray($tree, '--', $with_root = true, $depth = null);
        $this->assertEquals('Root', $selectables1[1], 'Selectables including root');
        $selectables2 = $model->getSelectablesFromTreeArray($tree, '--', $with_root = false, $depth = null);
        $this->assertEquals('-- Child 1', $selectables2[2], 'Selectables without root');
        $this->assertArrayNotHasKey(1, $selectables2, 'Selectables without root do not have the "Root" element.');

        // Should return only the root since we want only 1 level of depth and including root
        $selectables3 = $model->getSelectablesFromTreeArray($tree, '--', $with_root = true, $depth = 1);
        $this->assertEquals('Root', $selectables3[1], 'Selectables with root, limited depth');
        $this->assertEquals(1, count($selectables3), 'Selectables with root, depth=1 contains only one element');

        // Selectables without root and depth=1 should return nothing
        $selectables4 = $model->getSelectablesFromTreeArray($tree, '--', $with_root = false, $depth = 1);
        $this->assertEmpty($selectables4);

        // Selectables with depth=2 and without root should only contain "Child 1"
        $selectables5 = $model->getSelectablesFromTreeArray($tree, '--', $with_root = false, $depth = 2);
        $this->assertEquals(1, count($selectables5), 'Selectables without root, depth=2 contains only one element');
        $this->assertEquals('-- Child 1', $selectables5[2]);

        // Selectables with depth=2 with root included
        $selectables6 = $model->getSelectablesFromTreeArray($tree, '--', $with_root = true, $depth = 2);
        $this->assertEquals(2, count($selectables6), 'Selectables with root, depth=2 contains 2 elements');
        $this->assertEquals('Root', $selectables6[1], 'Selectables with root, depth=2, "Root" is present');
        $this->assertEquals('-- Child 1', $selectables6[2], 'Selectables with root, depth=2, "Child" is present');

        // Now lets see if multi root is behaving as expected...
        $this->insertMultiRootFixtures();
        $model = new CatMR();
        $tree = $model->getTree();

        $selectables1 = $model->getSelectablesFromTreeArray($tree, '--', $with_root = true, $depth = null);
        $this->assertEquals(10, count($selectables1), 'Selectables with multiple roots contain the entire tree');

        $selectables2 = $model->getSelectablesFromTreeArray($tree, '--', $with_root = false, $depth = null);
        $this->assertEquals(8, count($selectables2), 'Selectables with multiple roots but without root nodes contain 2 less elements');

        // Should return only the roots since we want only 1 level of depth and including root
        $selectables3 = $model->getSelectablesFromTreeArray($tree, '--', $with_root = true, $depth = 1);
        $this->assertEquals(2, count($selectables3), 'Selectables with root, depth=1 contains only 2 elements');
        $this->assertEquals('Mobile Phones', $selectables3[1]);
        $this->assertEquals('Cars', $selectables3[2]);

        // Selectables without root and depth=1 should return nothing
        $selectables4 = $model->getSelectablesFromTreeArray($tree, '--', $with_root = false, $depth = 1);
        $this->assertEmpty($selectables4);

        // Selectables with depth=2 and without roots should only contain: Iphone, Samsung, Motorola, Audi, Ford, Mercedes
        $selectables5 = $model->getSelectablesFromTreeArray($tree, '--', $with_root = false, $depth = 2);
        $this->assertEquals(6, count($selectables5), 'Selectables without root, depth=2 contains 6 elements');
        $this->assertEquals('-- iPhone', $selectables5[8]);
        $this->assertEquals('-- Samsung', $selectables5[6]);
        $this->assertEquals('-- Motorola', $selectables5[7]);
        $this->assertEquals('-- Audi', $selectables5[5]);
        $this->assertEquals('-- Ford', $selectables5[3]);
        $this->assertEquals('-- Mercedes', $selectables5[4]);

        // Selectables with depth=2 with roots included
        $selectables6 = $model->getSelectablesFromTreeArray($tree, '--', $with_root = true, $depth = 2);
        $this->assertEquals(8, count($selectables6), 'Selectables with root, depth=2 contains 8 elements');
        $this->assertEquals('Mobile Phones', $selectables6[1]);
        $this->assertEquals('-- iPhone', $selectables6[8]);
        $this->assertEquals('-- Samsung', $selectables6[6]);
        $this->assertEquals('-- Motorola', $selectables6[7]);
        $this->assertEquals('Cars', $selectables6[2]);
        $this->assertEquals('-- Audi', $selectables6[5]);
        $this->assertEquals('-- Ford', $selectables6[3]);
        $this->assertEquals('-- Mercedes', $selectables6[4]);
    }

    public function testGetSelectables()
    {
        // Testing unscoped (entire tree) with hasManyRoots=false
        $this->insertSingleRootFixtures();
        $model = new Categories();
        $tree = $model->getTree();

        $selectables1 = $model->getSelectablesFromTreeArray($tree, '--', $with_root = true, $depth = null);
        $selectables2 = $model->getSelectables(array(
            'source' => $tree,
            'prefix' => '--',
            'with_root' => true,
            'depth' => null
        ));

        $this->assertEquals($selectables1, $selectables2, 'getSelectables() returns identical results as getSelectablesFromTreeArray()');

        $node = $model::findFirst('name="Child 1"');

        $node_selectables = $node->getSelectables();
        $this->assertEquals(5, count($node_selectables), 'getSelectables() on a node');
        $this->assertEquals('-- Child 1', $node_selectables[2]);
        $this->assertEquals('---- Grandchild 1', $node_selectables[3]);
        $this->assertEquals('---- Grandchild 2', $node_selectables[4]);
        $this->assertEquals('------ Even deeper', $node_selectables[6]);
        $this->assertEquals('---- Grandchild 3', $node_selectables[5]);

        $node_selectables = $node->getSelectables(array('with_root' => false, 'prefix' => '-'));
        $this->assertEquals(4, count($node_selectables), 'getSelectables() on a node with custom parameters');
        $this->assertEquals('-- Grandchild 1', $node_selectables[3]);
        $this->assertEquals('-- Grandchild 2', $node_selectables[4]);
        $this->assertEquals('--- Even deeper', $node_selectables[6]);
        $this->assertEquals('-- Grandchild 3', $node_selectables[5]);

        $node_selectables = $node->getSelectables(array('with_root' => false, 'prefix' => ''));
        $this->assertEquals(4, count($node_selectables), 'getSelectables() on a node with custom parameters');
        $this->assertEquals('Grandchild 1', $node_selectables[3]);
        $this->assertEquals('Grandchild 2', $node_selectables[4]);
        $this->assertEquals('Even deeper', $node_selectables[6]);
        $this->assertEquals('Grandchild 3', $node_selectables[5]);

        $node_selectables = $node->getSelectables(array('with_root' => false, 'prefix' => '-', 'depth' => 1));
        $this->assertEquals(3, count($node_selectables), 'getSelectables() on a node, without root, depth=1');
        $this->assertEquals('-- Grandchild 1', $node_selectables[3]);
        $this->assertEquals('-- Grandchild 2', $node_selectables[4]);
        // $this->assertEquals('Even deeper', $node_selectables[6]);
        $this->assertEquals('-- Grandchild 3', $node_selectables[5]);
    }

    public function testGetSelectablesWithBreadcrumbsInSingleRootMode()
    {
        // Testing unscoped (entire tree) with hasManyRoots=false
        $this->insertSingleRootFixtures();
        $model = new Categories();
        $tree = $model->getTree();

        $selectables1 = $model->getSelectablesFromTreeArray($tree, '--', $with_root = true, $depth = null, $breadcrumbs = true);
        $selectables2 = $model->getSelectables(array(
            'source' => $tree,
            'prefix' => '--',
            'with_root' => true,
            'depth' => null,
            'breadcrumbs' => true
        ));

        $this->assertEquals($selectables1, $selectables2, 'getSelectables() returns identical results as getSelectablesFromTreeArray() with $breadcrumbs=true');
    }
/*
    public function testUpdateTreeOrderSingleRoot()
    {
        // TODO: implement test
    }

    public function testUpdateTreeOrderMultiRoot()
    {
        // TODO: implement test
    }
*/
    public function testTreeMovesAndParentIdIsUpdatedMultiRoot()
    {
        $this->insertWrongMultiRootFixtures();

        // Moving stuff around in the tree as it's supposed to be

        // Move phones to the proper place
        $samsung = CatMR::findFirst("name = 'Samsung'");
        $x100 = CatMR::findFirst("name = 'X100'");
        $x100->moveAsFirst($samsung);

        // WARN: if we don't re-fetch x100 it doesn't get moved
        // properly since the moveAsFirst() above doesn't really update
        // all the model/owner properties on move operations at all!!!1
        $x100 = CatMR::findFirst("name = 'X100'");
        $c200 = CatMR::findFirst("name = 'C200'");
        $c200->moveBefore($x100);

        // Now move all Samsung phones branch
        $samsung = CatMR::findFirst("name = 'Samsung'");
        $mobile_phones = CatMR::findFirst("name = 'Mobile Phones'");
        $samsung->moveAsFirst($mobile_phones);

        // Move the rest of phone models
        $iphone = CatMR::findFirst("name = 'Iphone'");
        $iphone->moveAsFirst($mobile_phones);

        $samsung = CatMR::findFirst("name = 'Samsung'");
        $motorola = CatMR::findFirst("name = 'Motorola'");
        $motorola->moveAfter($samsung);

        // Move car models to appropriate place
        $cars = CatMR::findFirst("name = 'Cars'");
        $audi = CatMR::findFirst("name = 'Audi'");
        $audi->moveAsLast($cars);

        $cars = CatMR::findFirst("name = 'Cars'");
        $ford = CatMR::findFirst("name = 'Ford'");
        $ford->moveAsLast($cars);

        $cars = CatMR::findFirst("name = 'Cars'");
        $mercedes = CatMR::findFirst("name = 'Mercedes'");
        $mercedes->moveAsLast($cars);

        $cars = CatMR::findFirst("name = 'Cars'");
        $cars_selectables = $cars->getSelectables();
        $cars_tree = $cars->getTree();
        $phones = CatMR::findFirst("name = 'Mobile Phones'");
        $phones_selectables = $phones->getSelectables();
        $phones_tree = $phones->getTree();

        // Empty the table
        $this->emptyTable($phones->getSource());

        // Now insert the real fixtures
        $this->insertMultiRootFixtures();

        $cars = CatMR::findFirst("name = 'Cars'");
        $cars_selectables_real = $cars->getSelectables();
        $cars_tree_real = $cars->getTree();

        $phones = CatMR::findFirst("name = 'Mobile Phones'");
        $phones_selectables_real = $phones->getSelectables();
        $phones_tree_real = $phones->getTree();

        // Assert that the trees and selectables from wrong-then-fixed fixtures
        // are identical to the proper fixtures
        $this->assertEquals($cars_tree_real, $cars_tree);
        $this->assertEquals($phones_tree_real, $phones_tree);
        $this->assertEquals($cars_selectables_real, $cars_selectables);
        $this->assertEquals($phones_selectables_real, $phones_selectables);
    }

    public function testDeleteNodeWithChildrenInMultiRootTree()
    {
        // Ensure deleting nodes with children works properly in a multi root setup
        $this->insertMultiRootFixtures();

        // Delete a node containing child nodes
        $cars = CatMR::findFirst("name = 'Cars'");
        $result = $cars->deleteNode();
        $this->assertEquals(true, $result, 'deleteNode() returns true');

        // Query again after delete and make sure the delete nodes don't exist any more
        $cars = CatMR::findFirst("name = 'Cars'");
        $this->assertEquals(false, $cars, 'Cars node does not exist any more');

        $mercedes = CatMR::findFirst("name = 'Mercedes'");
        $this->assertEquals(false, $mercedes, '"Mercedes" node also not found (was a child of Cars)');
    }

    public function testDeleteNodeWithChildrenInDictionaries()
    {
        // Ensure deleting nodes with children works properly in a multi root setup
        $this->insertDictionariesFixtures();
        $this->insertParametersFixtures();

        // Delete a node containing child nodes
        $test = Dictionaries::findFirst(154);
        $dict_params = $test->Parameters->toArray();

        $result = $test->deleteNode();
        $this->assertEquals(true, $result, 'deleteNode() returns true');

        // Query again after delete and make sure the deleted nodes don't exist any more
        $node = Dictionaries::findFirst("name = 'one more new dict'");
        $this->assertEquals(false, $node, 'Dictionary node does not exist any more');

        $child = Dictionaries::findFirst("name = 'test child node'");
        $this->assertEquals(false, $child, '"test child node" node also not found (was a child of "one more new dict")');
    }

    // TODO: test with reduced staging data dump somehow because this passes for me!
    // TODO: it could be that it fails/deadlocks when db has invalid values from earlier
    public function testDictionaryNodeMoveToNewParent()
    {
        $root = new Dictionaries();
        $root->name = 'Root';
        $root->saveNode();

        $child = new Dictionaries();
        $child->name = 'Child 1';
        $child->appendTo($root);

        $grandchild1 = new Dictionaries();
        $grandchild1->name = 'Grandchild 1';
        $grandchild1->appendTo($child);

        $grandchild2 = new Dictionaries();
        $grandchild2->name = 'Grandchild 2';
        $grandchild2->insertAfter($grandchild1);

        $grandchild3 = new Dictionaries();
        $grandchild3->name = 'Grandchild 3';
        $grandchild3->insertAfter($grandchild2);

        $more = new Dictionaries();
        $more->name = 'Even deeper';
        $more->appendTo($grandchild2);

        $this->assertSame(5, $grandchild2->lft);
        $this->assertSame(3, $grandchild1->lft);
        $this->assertSame(7, $grandchild3->lft);

        // create another new root
        $root2 = new Dictionaries();
        $root2->name = 'Root 2';
        $root2->saveNode();

        $root2_child = new Dictionaries();
        $root2_child->name = 'Below root 2';
        $root2_child->appendTo($root2);

        $model = new Dictionaries();
        $tree = $model->getTree();
        $tree_before_move = $model->getSelectablesFromTreeArray($tree, '--', $with_root = true, $depth = null);

        $expected_before_move = array(
            1 => 'Root',
            2 => '-- Child 1',
            3 => '---- Grandchild 1',
            4 => '---- Grandchild 2',
            6 => '------ Even deeper',
            5 => '---- Grandchild 3',
            7 => 'Root 2',
            8 => '-- Below root 2'
        );
        $this->assertSame($expected_before_move, $tree_before_move, 'Tree is looking good before the move');

        // move Grandchild 2 and all its children below a new node with a different root
        $node = Dictionaries::findFirst('name="Grandchild 2"');
        $current_parent = $node->parent();
        $new_parent = Dictionaries::findFirst('name="Below root 2"');

        $this->assertNotSame($current_parent->id, $new_parent->id, 'Current parent and new parent are different');
        $node_root_id = $node->root()->id;
        $new_root_id = $new_parent->root()->id;

        $save_result = $node->moveAsLast($new_parent);
        $this->assertTrue($save_result, 'Move was successful');

        // fetch the newly moved node again
        $node = Dictionaries::findFirst('name="Grandchild 2"');
        $this->assertSame($new_root_id, $node->root_id, 'Node root id changed as expected');

        $expected_after_move = array(
            1 => 'Root',
            2 => '-- Child 1',
            3 => '---- Grandchild 1',
            5 => '---- Grandchild 3',
            7 => 'Root 2',
            8 => '-- Below root 2',
            4 => '---- Grandchild 2',
            6 => '------ Even deeper'
        );
        // Makes sure what we're after is actually different
        $this->assertNotSame($expected_after_move, $expected_before_move);

        // Now check the new tree
        $model = new Dictionaries();
        $tree = $model->getTree();
        $tree_after_move = $model->getSelectablesFromTreeArray($tree, '--', $with_root = true, $depth = null);
        $this->assertSame($expected_after_move, $tree_after_move, 'Tree is changed after move and looks ok');
    }

    public function testDictionaryNodeChangeParent()
    {
        $root = new Dictionaries();
        $root->name = 'Dict';
        $root->saveNode();

        $child1 = new Dictionaries();
        $child1->name = 'Test 1';
        $child1->appendTo($root);

        $test = new Dictionaries();
        $test->name = 'Move me';
        $test->appendTo($child1);

        // Grab root again since we don't update references automatically (otherwise it uses old values)
        $root = Dictionaries::findFirst('name="Dict"');
        $child2 = new Dictionaries();
        $child2->name = 'Test 2';
        $child2->appendTo($root);

        $expected = array(
            1 => 'Dict',
            2 => '-- Test 1',
            3 => '---- Move me',
            4 => '-- Test 2'
        );
        $model = new Dictionaries();
        $tree = $model->getTree();
        $structure = $model->getSelectablesFromTreeArray($tree, '--', $with_root = true, $depth = null);
        $this->assertSame($expected, $structure, 'Tree structure is as expected before the move');

        // Now grab the 'Move me' node and move it to a new parent
        $node_to_move = Dictionaries::findFirst('name="Move me"');
        $new_parent = Dictionaries::findFirst('name="Test 2"');
        $save_result = $node_to_move->moveAsLast($new_parent);
        $this->assertTrue($save_result, 'Move was ok');

        $expected_after = array(
            1 => 'Dict',
            2 => '-- Test 1',
            4 => '-- Test 2',
            3 => '---- Move me',
        );
        $this->assertNotSame($expected, $expected_after, 'Expected tree structures are really different');
        $model = new Dictionaries();
        $tree = $model->getTree();
        $structure = $model->getSelectablesFromTreeArray($tree, '--', $with_root = true, $depth = null);
        $this->assertSame($expected_after, $structure, 'Tree structure is as expected after the move');
    }

/*
    public function testGetPathWithAncestors()
    {
        $this->insertSingleRootFixtures();
        $model = new Categories();

        $node = $model::findFirst("name='Grandchild 2'");
        $path = $node->getNodePath();
        var_dump($path);

        $path_without_self = $node->getNodePath(false, 1);
        var_dump($path_without_self);
        exit;

    }
*/

}
