<?php

use \Phalcon\Validation\Validator\Regex as RegexValidator;

class ValidatorsTest extends PHPUnit_Framework_TestCase
{
    public function testRegexValidator()
    {
        $_POST = array();

        $field = 'username';
        $values = array(
            // value => count of expected messages triggered (0 = pass, 1 = fail)
            'Jasnajakupović' => 1,
            'Jasnajakupovic' => 0,
        );

        $pattern = \Baseapp\Library\Validations\UsersFrontend::USERNAME_REGEX;
        $message = 'Korisničko ime smije sadržavati samo slova, brojke, - (minus) i _ (donju crtu) te mora početi slovom';

        $options = array(
            'pattern' => $pattern,
            'message' => $message
        );
        $validator = new RegexValidator($options);

        $validation = new Phalcon\Validation();
        $validation->add($field, $validator);

        // Empty/non-present value should fail the regex check
        $messages = $validation->validate($_POST);
        $expectedMessages = Phalcon\Validation\Message\Group::__set_state(array(
            '_messages' => array(
                0 => Phalcon\Validation\Message::__set_state(array(
                    '_type' => 'Regex',
                    '_message' => $message,
                    '_field' => $field,
                    '_code' => '0',
                ))
            )
        ));
        $this->assertEquals($expectedMessages, $messages);

        // Now check with the actual values to test, which should also pass/fail
        foreach ($values as $value => $expected_count) {
            $_POST[$field] = $value;
            $messages = $validation->validate($_POST);
            $this->assertEquals($expected_count, count($messages));
        }
    }
}
