# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 10.0.12-MariaDB)
# Database: oglasnik_tests
# Generation Time: 2014-08-21 17:39:50 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `lft` int(10) unsigned DEFAULT NULL,
  `rght` int(10) unsigned DEFAULT NULL,
  `root_id` int(10) unsigned DEFAULT NULL,
  `level` int(10) unsigned NOT NULL,
  `name` varchar(64) COLLATE utf8_croatian_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `I_root_id` (`root_id`),
  KEY `lft_rght_lvl` (`lft`,`rght`,`level`),
  KEY `I_parent_id` (`parent_id`),
  CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`),
  CONSTRAINT `categories_ibfk2` FOREIGN KEY (`root_id`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

# Dump of table categories_many_roots
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories_many_roots`;

CREATE TABLE `categories_many_roots` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `lft` int(10) unsigned DEFAULT NULL,
  `rght` int(10) unsigned DEFAULT NULL,
  `root_id` int(10) unsigned DEFAULT NULL,
  `level` int(10) unsigned NOT NULL,
  `name` varchar(64) COLLATE utf8_croatian_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `I_root_id` (`root_id`),
  KEY `lft_rght_lvl` (`lft`,`rght`,`level`),
  KEY `I_parent_id` (`parent_id`),
  CONSTRAINT `categories_many_roots_ibfk_2` FOREIGN KEY (`parent_id`) REFERENCES `categories_many_roots` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `categories_many_roots_ibfk_1` FOREIGN KEY (`root_id`) REFERENCES `categories_many_roots` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

# Dump of table dictionaries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dictionaries`;

CREATE TABLE `dictionaries` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`parent_id` INT(10) UNSIGNED NULL DEFAULT NULL,
	`root_id` INT(10) UNSIGNED NULL DEFAULT NULL,
	`lft` INT(10) UNSIGNED NULL DEFAULT NULL,
	`rght` INT(10) UNSIGNED NULL DEFAULT NULL,
	`level` INT(10) UNSIGNED NOT NULL,
	`name` VARCHAR(64) NOT NULL COLLATE 'utf8_croatian_ci',
	`active` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
	PRIMARY KEY (`id`),
	INDEX `root_id` (`root_id`),
	INDEX `lft_rght_lvl` (`lft`, `rght`, `level`),
	INDEX `I_parent_id` (`parent_id`),
	CONSTRAINT `dictionaries_ibfk_1` FOREIGN KEY (`root_id`) REFERENCES `dictionaries` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `dictionaries_ibfk_2` FOREIGN KEY (`parent_id`) REFERENCES `dictionaries` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE='utf8_croatian_ci';

# Dump of table parameters_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parameters_types`;
CREATE TABLE IF NOT EXISTS `parameters_types` (
  `id` varchar(32) COLLATE utf8_croatian_ci NOT NULL,
  `name` varchar(64) COLLATE utf8_croatian_ci NOT NULL,
  `class` varchar(64) COLLATE utf8_croatian_ci DEFAULT NULL,
  `group_type` enum('standard','custom') COLLATE utf8_croatian_ci NOT NULL DEFAULT 'custom',
  `accept_dictionary` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `can_default` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `can_default_type` enum('text','checkbox','dropdown','date') COLLATE utf8_croatian_ci DEFAULT NULL,
  `can_other` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `can_prefix_suffix` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_dependable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_searchable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

INSERT INTO `parameters_types` (`id`, `name`, `class`, `group_type`, `accept_dictionary`, `can_default`, `can_default_type`, `can_other`, `can_prefix_suffix`, `is_dependable`, `is_required`, `is_searchable`) VALUES
	('CHECKBOX', 'Checkbox', 'fa-check-square-o', 'custom', 0, 1, 'checkbox', 0, 0, 0, 0, 0),
	('CHECKBOXGROUP', 'Checkbox group', 'fa-check-square-o', 'custom', 1, 0, NULL, 0, 0, 0, 0, 0),
	('CURRENCY', 'Currency', 'fa-money', 'standard', 0, 0, NULL, 0, 0, 0, 1, 1),
	('DATE', 'Date', 'fa-calendar', 'custom', 0, 1, 'date', 0, 0, 0, 0, 0),
	('DATEINTERVAL', 'Date interval', 'fa-calendar', 'custom', 0, 0, NULL, 0, 0, 0, 0, 0),
	('DEPENDABLE_DROPDOWN', 'Dependable Dropdown menu', 'fa-caret-square-o-down', 'custom', 1, 0, 'text', 1, 0, 1, 0, 0),
	('DESCRIPTION', 'Ad description', 'fa-font', 'standard', 0, 0, NULL, 0, 0, 0, 1, 1),
	('DROPDOWN', 'Dropdown menu', 'fa-caret-square-o-down', 'custom', 1, 1, 'dropdown', 1, 0, 0, 0, 0),
	('LOCATION', 'Ad location', 'fa-flag', 'standard', 0, 1, 'dropdown', 0, 0, 1, 1, 1),
	('MONTHYEAR', 'Month/Year', 'fa-caret-square-o-down', 'custom', 0, 1, 'dropdown', 0, 0, 0, 0, 0),
	('NUMBER', 'Number', 'fa-sort-numeric-asc', 'custom', 0, 1, 'text', 0, 1, 0, 0, 0),
	('NUMBERINTERVAL', 'Number interval', 'fa-sort-numeric-asc', 'custom', 0, 0, NULL, 0, 0, 0, 0, 0),
	('PICTURE', 'Ad pictures', 'fa-image', 'standard', 0, 0, NULL, 0, 0, 0, 0, 0),
	('RADIO', 'Radio buttons', 'fa-dot-circle-o', 'custom', 1, 1, 'dropdown', 1, 0, 0, 0, 0),
	('TEXT', 'Text input', 'fa-font', 'custom', 0, 1, 'text', 0, 1, 0, 0, 0),
	('TEXTAREA', 'Textarea input', 'fa-font', 'custom', 0, 1, 'text', 0, 0, 0, 0, 0),
	('TITLE', 'Ad Title', 'fa-font', 'standard', 0, 0, NULL, 0, 0, 0, 1, 1),
	('URL', 'URL field', 'fa-link', 'custom', 0, 1, 'text', 0, 1, 0, 0, 0),
	('YEAR', 'Year', 'fa-caret-square-o-down', 'custom', 0, 1, 'dropdown', 0, 0, 0, 0, 0);

# Dump of table parameters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parameters`;
CREATE TABLE `parameters` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(128) NOT NULL COLLATE 'utf8_croatian_ci',
	`type_id` VARCHAR(32) NOT NULL DEFAULT 'TEXT' COLLATE 'utf8_croatian_ci',
	`dictionary_id` INT(10) UNSIGNED NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `name` (`name`),
	INDEX `type_id` (`type_id`),
	INDEX `dictionary_id` (`dictionary_id`),
    CONSTRAINT `parameters_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `parameters_types` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT `parameters_ibfk_2` FOREIGN KEY (`dictionary_id`) REFERENCES `dictionaries` (`id`) ON UPDATE CASCADE ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE='utf8_croatian_ci';

INSERT INTO `parameters` (`id`, `name`, `type_id`, `dictionary_id`) VALUES
	(1, 'Auti - Oblik karoserije', 'DROPDOWN', 1),
	(2, 'HIFI - Proizvođači', 'DEPENDABLE_DROPDOWN', 3),
	(3, 'Auti - Marka i model vozila', 'DEPENDABLE_DROPDOWN', 2),
	(5, 'Auti - Dodatna oprema', 'CHECKBOXGROUP', 26),
	(6, 'Auti - Sigurnost', 'CHECKBOXGROUP', 44),
	(7, 'Auti - Sigurnost od krađe', 'CHECKBOXGROUP', 62),
	(8, 'Auti - Vrsta goriva', 'DROPDOWN', 68),
	(9, 'Auti - Tip vozila', 'TEXT', NULL),
	(10, 'Auti - Snaga motora (kW)', 'NUMBER', NULL),
	(11, 'Auti - Radni obujam (cm3)', 'NUMBER', NULL),
	(12, 'Auti - Pogon', 'DROPDOWN', 74),
	(13, 'Auti - Vrsta mjenjača', 'DROPDOWN', 78),
	(14, 'Auti - Broj brzina', 'DROPDOWN', 83),
	(15, 'Auti - Broj vrata', 'DROPDOWN', 88),
	(16, 'Auti - Udobnost', 'CHECKBOXGROUP', 94),
	(17, 'Auti - Novo/Rabljeno', 'RADIO', 119),
	(18, 'Auti - Oldtimer', 'CHECKBOX', NULL),
	(19, 'Auti - Stanje', 'DROPDOWN', 122),
	(20, 'Auti - Boja', 'DROPDOWN', 132),
	(21, 'Auti - Vlasnik', 'DROPDOWN', 128),
	(22, 'Auti - Servisna knjižica', 'CHECKBOX', NULL),
	(23, 'Auti - Garažiran', 'CHECKBOX', NULL),
	(24, 'Youtube video', 'URL', NULL),
	(25, 'Ad title', 'TITLE', NULL),
	(27, 'Ad price', 'CURRENCY', NULL),
	(28, 'Ad description', 'DESCRIPTION', NULL),
	(29, 'Datum rođenja', 'DATE', NULL),
	(30, 'Slobodni termin', 'DATEINTERVAL', NULL),
	(31, 'Ad location', 'LOCATION', NULL),
	(32, 'Ad pictures', 'PICTURE', NULL),
	(33, 'Auti - Godina proizvodnje', 'YEAR', NULL),
	(34, 'Auti - Registriran do', 'MONTHYEAR', NULL);

# Dump of table parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parts`;

CREATE TABLE `parts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `parts` WRITE;
/*!40000 ALTER TABLE `parts` DISABLE KEYS */;

INSERT INTO `parts` (`id`, `name`)
VALUES
	(1,'Head'),
	(2,'Body'),
	(3,'Arms'),
	(4,'Legs'),
	(5,'CPU');

/*!40000 ALTER TABLE `parts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table robots
# ------------------------------------------------------------

DROP TABLE IF EXISTS `robots`;

CREATE TABLE `robots` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `year` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `robots` WRITE;
/*!40000 ALTER TABLE `robots` DISABLE KEYS */;

INSERT INTO `robots` (`id`, `name`, `type`, `year`)
VALUES
	(1,'Robotina','mechanical',1972),
	(2,'Astro Boy','mechanical',1952),
	(3,'Terminator','cyborg',2029);

/*!40000 ALTER TABLE `robots` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table robots_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `robots_parts`;

CREATE TABLE `robots_parts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `robots_id` int(10) unsigned NOT NULL,
  `parts_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `robots_id` (`robots_id`),
  KEY `parts_id` (`parts_id`),
  CONSTRAINT `robots_parts_ibfk_1` FOREIGN KEY (`robots_id`) REFERENCES `robots` (`id`),
  CONSTRAINT `robots_parts_ibfk_2` FOREIGN KEY (`parts_id`) REFERENCES `parts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `robots_parts` WRITE;
/*!40000 ALTER TABLE `robots_parts` DISABLE KEYS */;

INSERT INTO `robots_parts` (`id`, `robots_id`, `parts_id`)
VALUES
	(1,1,1),
	(2,1,2),
	(3,1,3);

/*!40000 ALTER TABLE `robots_parts` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
