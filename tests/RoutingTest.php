<?php

/**
 * Some basic routing tests, to make sure any potential new changes around
 * routing do not break existing expected route matching.
 */
class RoutingTest extends PHPUnit_Framework_TestCase
{

    /**
     * @dataProvider routeProvider
     */
    public function testMatching($pattern, $expected)
    {
        /** @var \Phalcon\Mvc\Router $router */
        $router = \Phalcon\Di::getDefault()->getShared('router');
        $router->handle($pattern);

        if ($router->wasMatched()) {
            $this->assertEquals($expected, array($router->getModuleName(), $router->getControllerName(), $router->getActionName(), $router->getParams()), $pattern);
        } else {
            $this->assertEquals($expected, null, "The route wasn't matched by any route");
        }
    }

    public function routeProvider()
    {
        /**
         * array(pattern, array(module, controller, action, array(params)))
         */
        return array(
            array('/', array('frontend', 'index', 'index', array())),
            array('/index', array('frontend', 'index', 'index', array())),
            array('/index/index', array('frontend', 'index', 'index', array())),
            array('/index/test', array('frontend', 'index', 'test', array())),

            /*
            array('/wrong-url-on-purpose', null),
            array('/wrong-url-on-purpose/', array('frontend', 'index', 'notFound', array())),
            array('/wrong-url-on-purpose/deeper', array('frontend', 'index', 'notFound', array())),
            array('/wrong-url-on-purpose/deeper/even-more', array('frontend', 'index', 'notFound', array())),
            array('/wrong-url-on-purpose/deeper/even-more/', array('frontend', 'index', 'notFound', array())),
            */

            array('/user', array('frontend', 'user', 'index', array())),
            array('/user/signup', array('frontend', 'user', 'signup', array())),
            array('/user/signin', array('frontend', 'user', 'signin', array())),
            array('/user/activation', array('frontend', 'user', 'activation', array())),
            array('/user/activation/', array('frontend', 'user', 'activation', array())),
            array('/user/activation/username', array('frontend', 'user', 'activation', array('username'))),
            array('/user/activation/username/', array('frontend', 'user', 'activation', array('username'))),
            array('/user/activation/username/hash', array('frontend', 'user', 'activation', array('username', 'hash'))),
            array('/user/activation/username/hash/', array('frontend', 'user', 'activation', array('username', 'hash'))),
            array('/user/pwdforgot', array('frontend', 'user', 'pwdforgot', array())),
            array('/user/pwdforgot/', array('frontend', 'user', 'pwdforgot', array())),
            array('/user/pwdreset', array('frontend', 'user', 'pwdreset', array())),
            array('/user/pwdreset/', array('frontend', 'user', 'pwdreset', array())),
            array('/user/pwdreset/whateva', array('frontend', 'user', 'pwdreset', array('whateva'))),

            array('/admin', array('backend', 'index', 'index', array())),
            array('/admin/', array('backend', 'index', 'index', array())),
            array('/admin/index', array('backend', 'index', 'index', array())),
            array('/admin/index/index', array('backend', 'index', 'index', array())),
            array('/admin/index/test', array('backend', 'index', 'test', array())),

            array('/admin/user', array('backend', 'user', 'index', array())),
            array('/admin/user/', array('backend', 'user', 'index', array())),

            // trailing slashes!
            array('/user/', array('frontend', 'user', 'index', array())),
            array('/user/signup/', array('frontend', 'user', 'signup', array())),

            // dashes in action names converted properly
            array('/ajax/dictionary-values', array('frontend', 'ajax', 'dictionaryValues', array())),
            array('/admin/dictionary/add-value', array('backend', 'dictionary', 'addValue', array())),

            // various custom routes we don't want to break
            array('/predaja-oglasa', array('frontend', 'predaja-oglasa', 'index', array())),
            array('/auto-moto-i-nautika/automobili', array('frontend', 'categories', 'slug', array())),
            array('/auti-potraznja', array('frontend', 'categories', 'slug', array())),
            // something in the root "namespace" but defined later and actually matching a real controller again
            array('/moj-kutak', array('frontend', 'moj-kutak', 'index', array())),
            // single ad seo version
            array('/ponuda/stara-opel-vectra-oglas-7429', array('frontend', 'ads', 'view', array('entity_id' => '7429', 'seo_slug' => 'ponuda/stara-opel-vectra'))),

            // sections vs categories
            array('/oglasi/auto-moto-nautika', array('frontend', 'sections', 'view', array('auto-moto-nautika'))),
            array('/oglasi/nekaj/osobni-kontakti', array('frontend', 'sections', 'view', array('nekaj', 'osobni-kontakti')))
        );
    }

}
