<?php

use Baseapp\Library\Behavior\NestedSet as NestedSetBehavior;

class TestCategories extends \Phalcon\Mvc\Model
{

    public function initialize()
    {
        $this->addBehavior(new NestedSetBehavior(array(
            'hasManyRoots' => false,
            'rootAttribute' => 'root_id',
            'rightAttribute' => 'rght',
            'parentAttribute' => 'parent_id',
            'useTransactions' => true,
        )));

        // disable not null validations for testing
        $this->setup(array('notNullValidations' => false));
    }

    public function getSource()
    {
        return 'categories';
    }

}
