{# Admin Shop Window Edit View #}
{% if tree is defined %}
<script>var category_tree_items = {{ tree|json_encode }};</script>
{% endif %}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <h1 class="page-header">Shops <small>{{ form_title_long }}</small></h1>

        {{ flashSession.output() }}

        {{ form(NULL, 'id':'frm_featured_shop', 'method':'post', 'autocomplete':'off') }}
            {{ hiddenField('_csrftoken') }}
            {{ hiddenField('next') }}
            {{ hiddenField(['action', 'value':form_action]) }}
            {{ hiddenField(['shop_id', 'value':shop.id]) }}
            {{ hiddenField(['user_id', 'value':shop.getOwner().id]) }}
            {{ hiddenField(['ads', 'value':featured_shop.readAttribute('ads')]) }}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Shopping window details</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            {% set field = 'main_category_id' %}
                            <div class="form-group{{ errors is defined and errors.filter('categories') ? ' has-error' : '' }}">
                                <label for="{{ field }}" class="control-label">Main category</label>
                                <div class="icon_dropdown">{{ main_category_id_dropdown }}</div>
                                {% if errors is defined and errors.filter('categories') %}
                                <p class="help-block">{{ current(errors.filter('categories')).getMessage() }}</p>
                                {% endif %}
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4" id="additional_cat_div"{{ additional_category_ids ? '' : ' style="display:none"' }}>
                            {% set field = 'additional_category_ids' %}
                            <div class="form-group">
                                <label for="{{ field }}" class="control-label">Additional categories (max 2)</label>
                                <div class="icon_dropdown">{{ additional_category_ids_dropdown }}</div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="form-group">
                                <label>Owner</label>
                                <div>{{ shop.getBackendInfoLinksMarkup() }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            {% set field = 'published_at' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Published at</label>
                                <div class='input-group date' id='published_at_datetimepicker'>
                                    <input type="text" class="form-control" name="{{ field }}" id="{{ field }}" placeholder="" value="{{ (_POST[field] is defined ? _POST[field] : (featured_shop.readAttribute(field) ? date('d.m.Y H:i', featured_shop.readAttribute(field)) : '')) }}" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% endif %}
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            {% set field = 'expires_at' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Expires at</label>
                                <div class='input-group date' id='expires_at_datetimepicker'>
                                    <input type="text" class="form-control" name="{{ field }}" id="{{ field }}" placeholder="" value="{{ (_POST[field] is defined ? _POST[field] : (featured_shop.readAttribute(field) ? date('d.m.Y H:i', featured_shop.readAttribute(field)) : '')) }}" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% endif %}
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="form-group">
                                <label>&nbsp;</label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="active"{% if featured_shop.active %} checked="checked"{% endif %} value="1"> Active</label>
                                    &nbsp;
                                    <label><input type="checkbox" name="deleted"{% if featured_shop.deleted %} checked="checked"{% endif %} value="1"> Deleted</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h4>Preview</h4>
                            <div class="row featured-shops-box featured-shops-box-preview">
                                {% set shop_preview = featured_shop.buildShoppingWindowMarkup(shop, false, true) %}
                                {{ shop_preview }}
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-8"><h4 class="margin-top-5px">User's active ads</h4></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="input-group">
                                            {{ textField(['filter-classifieds', 'class':'form-control form-control-md', 'placeholder':"Classified's title"]) }}
                                            <div class="input-group-btn">
                                                <button id="filter-classifieds-btn" class="btn btn-primary btn-md" type="button"><span class="fa fa-search"></span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="selectable-ads" id="all_ads_container">
                                {% if users_active_ads is defined and users_active_ads is iterable %}
                                    {% for ad in users_active_ads %}
                                        {{ partial('shops/ad-chunk', ['ad': ad]) }}
                                    {% endfor %}

                                    {% if pagination_links %}{{ pagination_links }}{% endif %}
                                {% endif %}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <button class="btn btn-primary" type="submit" name="save">Save</button>
                    <button class="btn btn-primary" type="submit" name="saveexit">Save and exit</button>
                    <button class="btn btn-default" type="submit" name="cancel">Cancel</button>
                </div>
            </div>
        {{ endForm() }}
    </div>
</div>
