{# Admin CMS articles Listing/View #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div>
            {% if auth.logged_in(['admin', 'content']) %}
            <div class="pull-right">
                {{ linkTo(['admin/cms-articles/create', '<span class="fa fa-plus fa-fw"></span> Add new', 'class': 'btn btn-info']) }}
            </div>
            {% endif %}
            <h1 class="page-header">Articles</h1>
        </div>
        {{ flashSession.output() }}
        <form class="form-inline" role="form">
            {{ hiddenField('field') }}
            {{ hiddenField('mode') }}
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="input-group search-panel">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="dropdown-label" id="search_field">All fields</span> <span class="caret"></span>
                            </button>
                            <ul data-param="#field" class="dropdown-menu" role="menu">
                                <li role="presentation" class="dropdown-header">Narrow down your search</li>
                                {%- for f in field_options -%}
                                    <li{% if f == field %} class="active"{% endif %}><a href="#{{ f }}">{{ f }}</a></li>
                                {%- endfor -%}
                                <li class="divider"></li>
                                <li><a href="#all">All fields</a></li>
                            </ul>
                        </div>
                        <input type="text" class="form-control" name="q" value="{{ q|escape_attr }}" placeholder="Search term(s)...">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul data-param="#mode" class="dropdown-menu dropdown-menu-right" role="menu">
                                <li role="presentation" class="dropdown-header">Search mode</li>
                                {% for m, m_desc in mode_options %}
                                    <li{% if m == mode %} class="active"{% endif %}><a href="#{{ m }}">{{ m_desc }}</a></li>
                                {% endfor %}
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-sm-6 text-right">
                    {{ category_id_dropdown }}
                    <select id="order-by" name="order_by" class="form-control" title="Order by">
                        {%- for value, title in order_by_options -%}
                            <option {% if value == order_by %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
                        {%- endfor -%}
                    </select>
                    <select id="dir" name="dir" class="form-control" title="ASC/DESC">
                        {%- for value, title in dir_options -%}
                            <option {% if value == dir %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
                        {%- endfor -%}
                    </select>
                    <select id="limit" name="limit" class="form-control" title="Records per page">
                        {%- for value in limit_options -%}
                            <option {% if value == limit %} selected="selected" {% endif %}value="{{ value }}">{{ value }}</option>
                        {%- endfor -%}
                    </select>
                </div>
            </div>
        </form>
        <br>
        {% if page.items.valid() %}
        <table class="table table-striped table-hover table-condensed">
            <tr>
                <th>Title / Category</th>
                <th width="150">Published at</th>
                <th class="text-right">Options</th>
            </tr>
            {% for article in page.items %}
            <tr{{ article.active == '0' ? ' class="danger"' : '' }}>
                <!--[ID: {{ article.id }}]-->
                <td>
                    <div>
                        {{ linkTo(['magazine/article/' ~ article.slug, '<span class="glyphicon glyphicon-eye-open"></span>', 'target': '_blank', 'title': 'Frontend view' ]) }}
                        {{ linkTo(['admin/cms-articles/edit/' ~ article.id, article.title|escape]) }}
                    </div>
                    <div>
                        {% set cat = tree[article.category_id] %}
                        <small>
                            <span class="fa fa-folder fa-fw text-muted"></span>{{ linkTo(['admin/cms-categories/edit/' ~ article.category_id, cat.path['text'], 'class': 'text-muted']) }}
                            {% if article.featured == 1 %}
                                | <span class="fa fa-star fa-fw text-success" title="Featured"></span>{{ date('d.m.Y H:i', strtotime(article.featured_start)) }} - {{ date('d.m.Y H:i', strtotime(article.featured_end)) }}
                            {% else %}
                            &nbsp;
                            {% endif %}
                        </small>
                    </div>
                </td>
                <td>
                    {{ date('d.m.Y @ H:i', strtotime(article.publish_date)) }}
                </td>
                <td class="text-right">
                    <div class="btn-group pull-right">
                        <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog fa-fw"><!--IE--></span><span class="caret"></span></button>
                        <ul class="dropdown-menu" role="menu">
                            <li>{{ linkTo(['admin/cms-articles/delete/' ~ article.id, '<span class="fa fa-trash-o text-danger fa-fw"></span> Delete']) }}</li>
                            <li>{{ linkTo(['admin/cms-articles/activeToggle/' ~ article.id, '<span class="fa fa-' ~ (article.active ? 'check-' : '') ~ 'square-o fa-fw"></span> ' ~ (article.active ? 'Deactivate' : 'Activate')]) }}</li>
                        </ul>
                    </div>
                </td>
            </tr>
            {% endfor %}
        </table>
        {% else %}
        <div class="no-results alert alert-success">
            <p class="text-center">Nothing matched your search query</p>
        </div>
        {% endif %}
        {% if pagination_links %}{{ pagination_links }}{% endif %}
    </div>
</div>
