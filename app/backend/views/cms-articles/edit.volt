{# Admin CMS articles Form View #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <h1 class="page-header">Articles <small>{{ form_title_long }}</small></h1>
        {{ flashSession.output() }}

        {{ form(NULL, 'id' : 'frm_article', 'method' : 'post', 'autocomplete' : 'off') }}
            {{ hiddenField('next') }}
            {{ hiddenField('_csrftoken') }}
            <div class="row">
                <div class="col-lg-9 col-md-8">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            {% set field = 'title' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Title</label>
                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{ article.title|default('') }}" maxlength="256" />
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% endif %}
                            </div>
                            {% set field = 'slug' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Slug</label>
                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{ article.slug|default('') }}" maxlength="256" />
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% endif %}
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            {% set field = 'excerpt' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Excerpt</label>
                                <textarea class="form-control" name="{{ field }}" id="{{ field }}" rows="5" cols="40">{{ article.excerpt | default('') }}</textarea>
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% endif %}
                            </div>
                        </div>
                    </div>
                    {% set field = 'content' %}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label class="control-label" for="{{ field }}">Content</label>
                        <textarea class="form-control" name="{{ field }}" id="{{ field }}" rows="5" cols="40">{{ article.content|default('') }}</textarea>
                    {% if errors is defined and errors.filter(field) %}
                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                    {% endif %}
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="pull-right">
                                <span class="btn btn-success btn-xs" id="importMedia" data-url="{{ url('admin/media?is_popup=1') }}"><span class="fa fa-plus"></span> Add media items</span>
                            </div>
                            <h3 class="panel-title">Media items</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <div data-form="article_media_gallery" class="uploadable-sorter ui-sortable" id="uploadable_sorter">
                                        {% if article_media is iterable %}
                                            {% for media in article_media %}
                                            <div class="uploadable-sorter-item col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                                <div class="attachment" data-id="{{ media.id }}">
                                                    <div class="attachment-preview">
                                                        <div class="thumb">
                                                            <div class="centered">
                                                                <img src="{{ media.get_backend_thumb().src }}">
                                                                <input type="hidden" name="article_media_gallery[]" value="{{ media.id }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="uploadable-sorter-options btn-group btn-group-justified">
                                                        <div class="hidden-sm btn-group btn-group-sm">
                                                            <span style="cursor:move" class="btn btn-default drag-me ui-sortable-handle"><span class="fa fa-arrows"><!--IE--></span></span>
                                                        </div>
                                                        <div class="btn-group btn-group-sm">
                                                            <span class="btn btn-default set-as-main"><span class="fa fa-eye"><!--IE--></span></span>
                                                        </div>
                                                        <div class="btn-group btn-group-sm">
                                                            <span class="btn btn-default move-prev"><span class="fa fa-arrow-circle-o-left"><!--IE--></span></span>
                                                        </div>
                                                        <div class="btn-group btn-group-sm">
                                                            <span class="btn btn-default move-next"><span class="fa fa-arrow-circle-o-right"><!--IE--></span></span>
                                                        </div>
                                                        <div class="btn-group btn-group-sm">
                                                            <span class="btn btn-danger delete"><span class="fa fa-trash-o"><!--IE--></span></span>
                                                        </div>
                                                    </div>
                                                    <div style="padding-top:5px" class="text-left">
                                                        <a href="{{ url('admin/media/edit/' ~ media.id) }}" target="_blank"><span class="fa fa-picture-o fa-fw"></span>Edit</a>
                                                    </div>
                                                </div>
                                            </div>
                                            {% endfor %}
                                        {% endif %}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                    {% set field = 'category_id' %}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label class="control-label" for="{{ field }}">Category</label>
                        {{ category_id_dropdown }}
                        {% if errors is defined and errors.filter(field) -%}
                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                        {%- endif %}
                    </div>
                    {% set field = 'publish_date' %}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label class="control-label" for="{{ field }}">Publish date</label>
                        <div class="input-group date">
                            <input type='text' class="form-control" name="{{ field }}" id="{{ field }}" value="{{ article.publish_date|default(date('Y-m-d H:i')) }}" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    {% if errors is defined and errors.filter(field) %}
                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                    {% endif %}
                    </div>
                    {% set field = 'tags' %}
                    <div class="form-group">
                        <label class="control-label" for="{{ field }}">Tags</label>
                        <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{ article.raw_tags|default('') }}" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="sort_order">Sort order</label>
                        <input class="form-control" name="sort_order" id="sort_order" value="{{ article.sort_order|default('0') }}" />
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="checkbox">
                                <label><input type="checkbox" name="full_page_image"{% if article.full_page_image %} checked="checked"{% endif %} value="1"> Full page image</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="checkbox">
                                <label><input type="checkbox" name="zendesk"{% if article.zendesk %} checked="checked"{% endif %} value="1"> ZenDesk</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="checkbox">
                                <label><input type="checkbox" name="active"{% if article.active %} checked="checked"{% endif %} value="1"> Active</label>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">SEO details</h3>
                        </div>
                        <div class="panel-body">
                            {% set field = 'meta_title' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Page title</label>
                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{ article.meta_title|default('') }}" maxlength="90" />
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% endif %}
                            </div>
                            {% set field = 'meta_description' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">&lt;meta name="description" ... &gt;</label>
                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{ article.meta_description|default('') }}" maxlength="160" />
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% endif %}
                            </div>
                            {% set field = 'meta_keywords' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">&lt;meta name="keywords" ... &gt;</label>
                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{ article.meta_keywords|default('') }}" maxlength="160" />
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% endif %}
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <input type="checkbox" id="featured" name="featured"{% if article.featured %} checked="checked"{% endif %} value="1">
                            <label for="featured" style="margin-bottom:0"><h3 class="panel-title" style="display:inline-block">Featured</h3></label>
                        </div>
                        <div class="panel-body"{{ article.featured == 0 ? ' style="display:none"' : '' }}>
                            {% set field = 'featured_start' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Featured start</label>
                                <div class="input-group date">
                                    <input type='text' class="form-control" name="{{ field }}" id="{{ field }}" value="{{ article.featured_start|default('') }}" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                {% endif %}
                            </div>
                            {% set field = 'featured_end' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Featured end</label>
                                <div class="input-group date">
                                    <input type='text' class="form-control" name="{{ field }}" id="{{ field }}" value="{{ article.featured_end|default('') }}" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                {% endif %}
                            </div>
                        </div>
                    </div>
                    {% if article.id is defined %}
                    <table class="table-condensed table-striped table-bordered media-details">
                        <tbody>
                            <tr>
                                <td>URI</td>
                                <td colspan="2">
                                    <a title="{{ article.title }}" target="_blank" href="{{ article.get_frontend_view_link() }}"><span class="glyphicon glyphicon-link"></span>{{ article.title }}</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Created</td>
                                <td><span class="glyphicon glyphicon-time"></span> {{ article.created_at }}</td>
                                <td class="text-nowrap">
                                    {%- if not user_created is null -%}
                                        {{ linkTo(['admin/user/edit/' ~ user_created.id, '<span class="glyphicon glyphicon-user"></span>' ~ user_created.username, 'class': 'text-nowrap']) }}
                                    {%- else -%}
                                        <span class="glyphicon glyphicon-user"></span> anonymous
                                    {%- endif -%}
                                </td>
                            </tr>
                            <tr>
                                <td>Modified</td>
                                <td><span class="glyphicon glyphicon-time"></span> {{ article.modified_at }}</td>
                                <td class="text-nowrap">
                                    {%- if not user_modified is null -%}
                                        {{ linkTo(['admin/user/edit/' ~ user_modified.id, '<span class="glyphicon glyphicon-user"></span>' ~ user_modified.username, 'class': 'text-nowrap']) }}
                                    {%- else -%}
                                        <span class="glyphicon glyphicon-user"></span> anonymous
                                    {%- endif -%}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    {% endif %}
                </div>
            </div>
            <hr/>
            <div>
                <button class="btn btn-primary" type="submit" name="save">Save</button>
                <button class="btn btn-primary" type="submit" name="saveexit">Save and exit</button>
                <button class="btn btn-default" type="submit" name="cancel">Cancel</button>
            </div>
        {{ endForm() }}
    </div>
</div>
