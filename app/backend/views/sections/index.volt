{# Admin Sections Listing #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div>
            <div class="pull-right">
                <button class="btn btn-primary" name="btnSaveSectionsOrder" id="btnSaveSectionsOrder"><span class="fa fa-save fa-fw"></span> Save order</button>
                {{ linkTo(['admin/sections/create' ~ (this.request.has('location') ? '?location=' ~ this.request.get('location') : ''), '<span class="fa fa-plus fa-fw"></span> Add new', 'class': 'btn btn-info']) }}
            </div>
            <h1 class="page-header">Sections</h1>
        </div>
        <div>
            <div class="pull-right">
                <div class="checkbox">
                    <label>
                        <input id="full-width-section-layout" data-section-location="{{ location }}" type="checkbox"{{ fullWidthLocation ? ' checked="checked"' : '' }}>Full-width section layout
                    </label>
                </div>
            </div>
            <div class="btn-group" id="filter" style="margin-bottom:10px">
                {{ linkTo(['admin/sections', 'Homepage', 'class': (location == 'homepage' ? 'btn btn-default btn-xs active' : 'btn btn-default btn-xs')]) }}
                {{ linkTo(['admin/sections?location=other', 'Other', 'class': (location == 'other' ? 'btn btn-default btn-xs active' : 'btn btn-default btn-xs')]) }}
            </div>
        </div>

        {{ flashSession.output() }}

        {{ form('admin/sections/save-order', 'id' : 'frm_sections', 'method' : 'post') }}

            {{ hiddenField('sections_order') }}
            {{ hiddenField('_csrftoken') }}

            <div class="list-sortable">
                <ul class="sortable ui-sortable">
                    {% for section in sections %}
                    <li id="item_{{section.id}}" class="sortable_li{% if section.active == '0' %} disabled{% endif %}" data-id="{{section.id}}">
                        <div class="sortable_div">
                            <div class="sortable_row">
                                <div class="drag-me ui-sortable-handle"><span class="fa fa-arrows fa-fw"></span></div>
                                <div class="name-cat">
                                    <span class="name">{{section.name}}</span>
                                </div>
                                <div class="actions-cat">
                                    {{ linkTo(['admin/sections/edit/' ~ section.id, '<span class="fa fa-edit"></span>Edit']) }}
                                    ·
                                    {{ linkTo(['admin/sections/active-toggle/' ~ section.id, (section.active == '0' ? '<span class="fa fa-check-square fa-fw"></span>Activate' : '<span class="fa fa-minus-square fa-fw"></span>Deactivate')]) }}
                                    ·
                                    {{ linkTo(['admin/sections/delete/' ~ section.id, '<span class="fa fa-trash-o text-danger"></span>' ]) }}
                                </div>
                            </div>
                        </div>
                    </li>
                    {% else %}
                    <li class="noitems">Currently, there are no sections here! You can start by creating the first one... :)</li>
                    {% endfor %}
                </ul>
            </div>

        {{ endForm() }}
    </div>
</div>
