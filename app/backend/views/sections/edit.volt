<div class="row">
    <div class="col-lg-12 col-md-12">

        <h1 class="page-header">{{ form_title_long }}</h1>

        {{ flashSession.output() }}

        {{ form(NULL, 'id':'frm_section', 'method':'post', 'enctype':'multipart/form-data', 'autocomplete':'off') }}
            {{ hiddenField('next') }}
            {{ hiddenField('json_categories') }}
            {{ hiddenField('_csrftoken') }}
            <div class="row">
                <div class="col-lg-9 col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Section details</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6">
                                            {% set field = 'name' %}
                                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                                <label class="control-label" for="{{ field }}">Name</label>
                                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{section.name | default('')}}" maxlength="64" />
                                            {% if errors is defined and errors.filter(field) %}
                                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                            {% endif %}
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6">
                                            {% set field = 'url' %}
                                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                                <label class="control-label" for="{{ field }}">URL</label>
                                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{section.url | default('')}}" maxlength="512" />
                                            {% if errors is defined and errors.filter(field) %}
                                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                            {% endif %}
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12">
                                            {% set field = 'excerpt' %}
                                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                                <label class="control-label" for="{{ field }}">Excerpt</label>
                                                <textarea class="form-control" name="{{ field }}" id="{{ field }}" rows="5" cols="40">{{section.excerpt | default('')}}</textarea>
                                            {% if errors is defined and errors.filter(field) %}
                                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                            {% else %}
                                                <p class="help-block">Section excerpt</p>
                                            {% endif %}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {% set field = 'link_type' %}
                                                <label class="control-label" for="{{ field }}">Link type</label>
                                                <select class="selectpicker form-control" name="{{ field }}" id="{{ field }}">
                                                    <option value="{{ constant('Baseapp\Models\Sections::LINK_TYPE_DEFAULT') }}" {% if constant('Baseapp\Models\Sections::LINK_TYPE_DEFAULT') == link_type %} selected{% endif %}>Default</option>
                                                    <option value="{{ constant('Baseapp\Models\Sections::LINK_TYPE_CATEGORY') }}" {% if constant('Baseapp\Models\Sections::LINK_TYPE_CATEGORY') == link_type %} selected{% endif %}>Specific category</option>
                                                    <option value="{{ constant('Baseapp\Models\Sections::LINK_TYPE_CUSTOM') }}" {% if constant('Baseapp\Models\Sections::LINK_TYPE_CUSTOM') == link_type %} selected{% endif %}>Custom</option>
                                                </select>
                                            </div>
                                            <div id="specific-category-link-box"{{ constant('Baseapp\Models\Sections::LINK_TYPE_CATEGORY') != link_type ? ' style="display:none"' : '' }}>
                                                {% set field = 'json_link_to_specific_category' %}
                                                <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                                    <label class="control-label" for="{{ field }}">Category</label>
                                                    <select class="selectpicker form-control" name="{{ field }}" id="{{ field }}">
                                                    {% for category_id, category_name in category_ids %}
                                                        <option value="{{ category_id }}" title="{{ category_name|trim('- ') }}" {% if category_id == link_to_specific_category %} selected{% endif %}>{{ category_name }}</option>
                                                    {% endfor %}
                                                    </select>
                                                    {% if errors is defined and errors.filter(field) %}<p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>{% endif %}
                                                </div>
                                            </div>
                                            <div id="custom-link-box"{{ constant('Baseapp\Models\Sections::LINK_TYPE_CUSTOM') != link_type ? ' style="display:none"' : '' }}>
                                                {% set field = 'json_custom_link' %}
                                                <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                                    <label class="control-label" for="{{ field }}">Custom URL</label>
                                                    <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{ custom_link | default('')}}" maxlength="512" />
                                                    {% if errors is defined and errors.filter(field) %}<p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>{% endif %}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="active"{% if section.active %} checked="checked"{% endif %} value="1"> Active</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-heading">
                                    <h3 class="panel-title">Background, Icon &amp; Location</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Background img</h3>
                                        </div>
                                        <div class="panel-body">
                                            {% set field = 'backgroundPic' %}
                                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                                <label class="control-label" for="{{ field }}">Upload new</label>
                                                <input type="file" id="{{ field }}" name="{{ field }}">
                                                {% if not (errors is empty) %}
                                                    {%- for err in errors -%}
                                                        <p class="help-block">{{ err }}</p>
                                                    {%- endfor -%}
                                                {% endif %}
                                            </div>

                                            {% set backgroundPic = section.getBackgroundPic() %}
                                            {% if backgroundPic %}
                                            {% set backgroundPic = backgroundPic.getThumb() %}
                                            <input type="hidden" name="deleteBackgroundPic" id="deleteBackgroundPic" value="" />
                                            <div class="form-group backgroundPicPreview">
                                                <label>Current background pic</label>
                                                {{ backgroundPic.setClass('background-pic').setHeight('45').setWidth('auto') }}
                                                <div>
                                                    <hr />
                                                    <span class="btn btn-sm btn-danger" id="deleteBackgroundPicBtn">Delete current background pic</span>
                                                </div>
                                            </div>
                                            {% endif %}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label" for="section_icon">Icon</label>
                                        {{ section_icon_dropdown }}
                                    </div>
                                    {% set field = 'location' %}
                                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                        <label class="control-label" for="{{ field }}">Location</label>
                                        {{ location_dropdown }}
                                        {% if errors is defined and errors.filter(field) %}
                                            <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                        {% endif %}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Section categories</h3>
                                </div>
                                <div class="panel-body">
                                    {% set field = 'category_ids' %}
                                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                        <select class="selectpicker form-control" name="{{ field }}[]" id="{{ field }}" multiple>
                                        {% for category_id, category_name in category_ids %}
                                            <option value="{{ category_id }}" title="{{ category_name|trim('- ') }}" {% if category_id in selected_category_ids %} selected{% endif %}>{{ category_name }}</option>
                                        {% endfor %}
                                        </select>
                                    {% if errors is defined and errors.filter(field) %}
                                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                    {% endif %}
                                    </div>
                                    <div class="category-sorter">
                                        <hr/>
                                        <div class="list-sortable">
                                            <ul class="sortable ui-sortable">
                                                {% if section_categories %}
                                                {% for section_category in section_categories %}
                                                <li id="item_{{ section_category.id }}" class="sortable_li{% if section_category.active == '0' %} disabled{% endif %}" data-id="{{ section_category.id }}">
                                                    <div class="sortable_div">
                                                        <div class="sortable_row">
                                                            <div class="drag-me ui-sortable-handle"><span class="fa fa-arrows fa-fw"></span></div>
                                                            <div class="name-cat"><span class="name">{{ section_category.name }}</span></div>
                                                            <div class="actions-cat"><span class="category-remove cursor-pointer"><span class="fa fa-trash-o text-danger"></span></span></div>
                                                        </div>
                                                    </div>
                                                </li>
                                                {% endfor %}
                                                {% endif %}
                                            </ul>
                                        </div>
                                        <hr/>
                                        <div class="form-group">
                                            <label class="control-label" for="json_categories_preview_count">Preview categories count</label>
                                            <input class="form-control" name="json_categories_preview_count" id="json_categories_preview_count" value="{{ section.json.categories_preview_count|default('0')}}" maxlength="2" />
                                            <p class="help-block">How many categories should be listed in previews (0 for all)</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Meta details</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label" for="meta_title">&lt;title&gt;</label>
                                <input class="form-control" name="meta_title" id="meta_title" value="{{section.meta_title | default('')}}" maxlength="90" />
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="meta_description">&lt;meta name="description" ... &gt;</label>
                                <input class="form-control" name="meta_description" id="meta_description" value="{{section.meta_description | default('')}}" maxlength="160" />
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="meta_keywords">&lt;meta name="keywords" ... &gt;</label>
                                <input class="form-control" name="meta_keywords" id="meta_keywords" value="{{section.meta_keywords | default('')}}" maxlength="160" />
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Sponsorship</h3>
                        </div>
                        <div class="panel-body">
                            {% set field = 'sponsorshipLogo' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Upload new</label>
                                <input type="file" id="{{ field }}" name="{{ field }}">
                                {% if not (errors is empty) %}
                                    {%- for err in errors -%}
                                        <p class="help-block">{{ err }}</p>
                                    {%- endfor -%}
                                {% endif %}
                            </div>

                            {% set sponsorshipLogo = section.getSponsorshipLogo() %}
                            {% if sponsorshipLogo %}
                            {% set sponsorshipLogo = sponsorshipLogo.getThumb() %}
                            <input type="hidden" name="deleteSponsorshipLogo" id="deleteSponsorshipLogo" value="" />
                            <div class="form-group sponsorshipPreview">
                                <label>Current logo</label>
                                {{ sponsorshipLogo.setClass('sponsorship-logo').setHeight('45').setWidth('auto') }}
                                <div>
                                    <hr />
                                    <span class="btn btn-sm btn-danger" id="deleteSponsorshipLogoBtn">Delete current logo</span>
                                </div>
                            </div>
                            {% endif %}
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12">
                    <hr/>
                    <button class="btn btn-primary" type="submit" name="save">Save</button>
                    <button class="btn btn-primary" type="submit" name="saveexit">Save and exit</button>
                    <button class="btn btn-default" type="submit" name="cancel">Cancel</button>
                </div>
            </div>
        {{ endForm() }}
    </div>
</div>
