{# Admin User Listing/View #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div>
            {% if auth.logged_in(['admin','supersupport','support','sales']) %}
            <div class="pull-right">
                {{ linkTo(['admin/users/create', '<span class="fa fa-plus fa-fw"></span> Add new', 'class': 'btn btn-info']) }}
            </div>
            {% endif %}
            <h1 class="page-header">Users</h1>
        </div>
        <form class="form-inline" role="form">
            {{ hiddenField('field') }}
            {{ hiddenField('mode') }}
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="input-group search-panel">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="dropdown-label" id="search_field">All fields</span> <span class="caret"></span>
                            </button>
                            <ul data-param="#field" class="dropdown-menu" role="menu">
                                <li role="presentation" class="dropdown-header">Narrow down your search</li>
                                {%- for f in field_options -%}
                                    <li{% if f == field %} class="active"{% endif %}><a href="#{{ f }}">{{ f }}</a></li>
                                {%- endfor -%}
                                <li class="divider"></li>
                                <li><a href="#all">All fields</a></li>
                            </ul>
                        </div>
                        <input type="text" class="form-control" name="q" value="{{ q|escape_attr }}" placeholder="Search term(s)...">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul data-param="#mode" class="dropdown-menu dropdown-menu-right" role="menu">
                                <li role="presentation" class="dropdown-header">Search mode</li>
                                {% for m, m_desc in mode_options %}
                                    <li{% if m == mode %} class="active"{% endif %}><a href="#{{ m }}">{{ m_desc }}</a></li>
                                {% endfor %}
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-sm-6 text-right">
                    <select id="user_role" name="user_role" class="form-control" title="User type">
                        {%- for value, title in user_role_options -%}
                            <option {% if value == user_role %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
                        {%- endfor -%}
                    </select>
                    <select id="order-by" name="order_by" class="form-control" title="Order by">
                        {%- for value, title in order_by_options -%}
                            <option {% if value == order_by %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
                        {%- endfor -%}
                    </select>
                    <select id="dir" name="dir" class="form-control" title="ASC/DESC">
                        {%- for value, title in dir_options -%}
                            <option {% if value == dir %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
                        {%- endfor -%}
                    </select>
                    <select id="limit" name="limit" class="form-control" title="Records per page">
                        {%- for value in limit_options -%}
                            <option {% if value == limit %} selected="selected" {% endif %}value="{{ value }}">{{ value }}</option>
                        {%- endfor -%}
                    </select>
                </div>
            </div>
        </form>
        <br>
        {{ flashSession.output() }}
        <table class="table table-striped table-hover table-condensed">
            <tr>
                <th width="60">ID</th>
                <th width="50">Type</th>
                <th width="150">Username</th>
                <th>Full name / Company name</th>
                <th width="350">Email</th>
                <th width="150" class="text-right">Options</th>
            </tr>
            {% for user in page.items %}
            <tr{% if user.banned or user.infraction_reports_count %} class="danger" {% elseif user.login_role != '1' %} class="warning" {% endif %}>
                <td>{{ user.id }}</td>
                <td>
                    {% set font_awesome_icon = user.type == constant('Baseapp\Models\Users::TYPE_PERSONAL') ? 'user' : 'globe' %}
                    {% set font_awesome_icon = user.user_shop_id ? 'shopping-cart' : font_awesome_icon %}
                    <span class="fa fa-{{ font_awesome_icon }} fa-fw"></span>
                    {% if user.remark is defined and user.remark %}
                    <span class="fa fa-warning text-danger fa-fw" data-toggle="tooltip" data-type="html" data-placement="right" title="{{ user.remark|escape|nl2br }}"></span>
                    {% endif %}
                </td>
                <td>{{ user.username }}</td>
                <td>
                    {% if user.type == '1' %}
                    {{ user.first_name ~ ' ' ~ user.last_name|trim }}
                    {% else %}
                    {{ user.company_name|trim }}
                    {% endif %}
                    {% if user.infraction_reports_count %}
                    <small class="text-danger"><span class="fa fa-exclamation-triangle fa-fw"></span> Unresolved infraction reports: {{ shop['infraction_reports_count'] }}</small>
                    {% endif %}
                </td>
                <td><span class="fa fa-{{ user.active ? 'check-circle text-success' : 'times-circle text-danger' }} fa-fw"></span>{{ user.email }}</td>
                <td class="text-right">
                    {{ linkTo(['admin/users/edit/' ~ user.id, '<span class="fa fa-edit fa-fw"></span>Edit']) }}
                </td>
            </tr>
            {% else %}
            <tr><td colspan="6">Currently, there are no users here!</td></tr>
            {% endfor %}
        </table>
        {% if pagination_links %}{{ pagination_links }}{% endif %}
    </div>
</div>
