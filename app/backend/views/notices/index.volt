{# Admin Notices Listing/View #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div>
            <div class="pull-right">
                {{ linkTo(['admin/notices/create', '<span class="fa fa-plus fa-fw"></span> Add new', 'class': 'btn btn-info']) }}
            </div>
            <h1 class="page-header">Company notices</h1>
        </div>
        {{ flashSession.output() }}
        <form class="form-inline" role="form">
            {{ hiddenField('field') }}
            {{ hiddenField('mode') }}
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="input-group search-panel">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="dropdown-label" id="search_field">All fields</span> <span class="caret"></span>
                            </button>
                            <ul data-param="#field" class="dropdown-menu" role="menu">
                                <li role="presentation" class="dropdown-header">Narrow down your search</li>
                                {%- for f in field_options -%}
                                    <li{% if f == field %} class="active"{% endif %}><a href="#{{ f }}">{{ f }}</a></li>
                                {%- endfor -%}
                                <li class="divider"></li>
                                <li><a href="#all">All fields</a></li>
                            </ul>
                        </div>
                        <input type="text" class="form-control" name="q" value="{{ q|escape_attr }}" placeholder="Search term(s)...">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul data-param="#mode" class="dropdown-menu dropdown-menu-right" role="menu">
                                <li role="presentation" class="dropdown-header">Search mode</li>
                                {% for m, m_desc in mode_options %}
                                    <li{% if m == mode %} class="active"{% endif %}><a href="#{{ m }}">{{ m_desc }}</a></li>
                                {% endfor %}
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-sm-6 text-right">
                    <select id="order-by" name="order_by" class="form-control" title="Order by">
                        {%- for value, title in order_by_options -%}
                            <option {% if value == order_by %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
                        {%- endfor -%}
                    </select>
                    <select id="dir" name="dir" class="form-control" title="ASC/DESC">
                        {%- for value, title in dir_options -%}
                            <option {% if value == dir %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
                        {%- endfor -%}
                    </select>
                    <select id="limit" name="limit" class="form-control" title="Records per page">
                        {%- for value in limit_options -%}
                            <option {% if value == limit %} selected="selected" {% endif %}value="{{ value }}">{{ value }}</option>
                        {%- endfor -%}
                    </select>
                </div>
            </div>
        </form>
        <br>
        {% if page.items.valid() %}
        <table class="table table-striped table-hover table-condensed">
            <tr>
                <th class="col-xs-8">Title</th>
                <th class="col-xs-2">Date sent (Received at)</th>
                <th class="col-xs-2 text-right">Options</th>
            </tr>
            {% for notice in page.items %}
            <tr{{ notice.active == '0' ? ' class="danger"' : '' }}>
                <td>
                    {{ linkTo(['admin/notices/edit/' ~ notice.id, notice.title|escape]) }}
                </td>
                <td>
                    {{ date('d.m.Y H:i', strtotime(notice.received_at)) }}
                </td>
                <td class="text-right">
                    {{ linkTo(['admin/notices/activeToggle/' ~ notice.id, (notice.active == 0 ? 'Activate' : 'Deactivate'), 'class': 'btn btn-xs btn-' ~ (notice.active == 0 ? 'info' : 'danger')])}}
                </td>
            </tr>
            {% endfor %}
        </table>
        {% else %}
        <div class="no-results alert alert-success">
            <p class="text-center">Nothing matched your search query</p>
        </div>
        {% endif %}
        {% if pagination_links %}{{ pagination_links }}{% endif %}
    </div>
</div>
