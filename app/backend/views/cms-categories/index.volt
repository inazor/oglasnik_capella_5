{# Admin CMS category Listing/View #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div>
            <div class="pull-right">
                {% if categories is defined %}
                <button class="btn btn-primary" name="btnSaveCategoriesOrder" id="btnSaveCategoriesOrder"><i class="fa fa-save"></i> Save order</button>
                {% endif %}
                {{ linkTo(['admin/cms-categories/create', '<span class="fa fa-plus fa-fw"></span> Add new', 'class': 'btn btn-info']) }}
            </div>
            <h1 class="page-header">Categories</h1>
        </div>
        {{ flashSession.output() }}
        <div>
            <button id="toggle-all" disabled="disabled" class="btn btn-default btn-xs">Toggle All</button>
        </div>
        {{ form('admin/cms-categories/save-order', 'id' : 'frm_categories', 'method' : 'post') }}
            {{ hiddenField('_csrftoken') }}
            {{ hiddenField('categories_order') }}
            {{ hiddenField('tree_root_id') }}
            {% if categories is defined %}
            <div class="nested-list-sortable">
                {% if available_item_actions is not defined %}
                {% set available_item_actions = [] %}
                {% endif %}
                {{ partial("chunks/nested-list", ['items': categories, 'class': 'sortable ui-sortable', 'available_item_actions': available_item_actions]) }}
            </div>
            {% else %}
            <p>Currently there are no categories here! You can start by creating the first one... :)</p>
            {% endif %}
        {{ endForm() }}
    </div>
</div>
