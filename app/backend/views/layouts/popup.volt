{# Admin/Backend Popup Layout #}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{ getTitle() }}
    {{ this.assets.outputCss() }}
</head>
<body>
    <div class="container">
        {{ content() }}
    </div>
    {{ this.assets.outputJs() }}
    {% if scripts is defined AND count(scripts) %}
        {% for script_id, script in scripts %}
            <script type="text/javascript"{% if script_id %} id="{{ script_id }}"{% endif %}>
                {{ script }}
            </script>
        {% endfor %}
    {% endif %}
</body>
</html>
