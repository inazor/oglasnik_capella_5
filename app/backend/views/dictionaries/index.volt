{# Admin Dictionaries Listing/View #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div>
            <div class="pull-right">
                {{ linkTo(['admin/dictionaries/create', '<span class="fa fa-plus fa-fw"></span> Add new', 'class': 'btn btn-info']) }}
            </div>
            <h1 class="page-header">Dictionaries</h1>
        </div>
        {{ flashSession.output() }}
        <form class="form-inline" role="form">
            {{ hiddenField('mode') }}
            <div class="row">
                <div class="col-sm-6">
                    <div class="input-group search-panel">
                        <input type="text" class="form-control" name="q" value="{{ q|escape_attr }}" placeholder="Search term(s)...">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul data-param="#mode" class="dropdown-menu dropdown-menu-right" role="menu">
                                <li role="presentation" class="dropdown-header">Search mode</li>
                                {% for m, m_desc in mode_options %}
                                    <li{% if m == mode %} class="active"{% endif %}><a href="#{{ m }}">{{ m_desc }}</a></li>
                                {% endfor %}
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 text-right">
                    <select id="dir" name="dir" class="form-control" title="ASC/DESC">
                        {%- for value, title in dir_options -%}
                            <option {% if value == dir %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
                        {%- endfor -%}
                    </select>
                    <select id="limit" name="limit" class="form-control" title="Records per page">
                        {%- for value in limit_options -%}
                            <option {% if value == limit %} selected="selected" {% endif %}value="{{ value }}">{{ value }}</option>
                        {%- endfor -%}
                    </select>
                </div>
            </div>
        </form>
        <br>
        {% if page.items %}
        <table class="table table-striped table-hover table-condensed">
            <tr>
                <th width="60" class="text-right">ID</th>
                <th>Dictionary name <span class="text-muted">(click to manage values)</span></th>
                <th width="150" class="text-right">Options</th>
            </tr>
            {% for dictionary in page.items %}
            <tr>
                <td class="text-right">{{ dictionary.id }}</td>
                {% set dictionary_levels = dictionary.getLevels() %}
                <td>
                    {{ linkTo(['admin/dictionaries/manage/' ~ dictionary.id, '<span class="fa fa-book fa-fw"></span>' ~ dictionary.name]) }}
                    {% if dictionary_levels|length > 0 %}
                    <small class="text-muted">
                        <span class="fa fa-long-arrow-right fa-fw"></span>
                        [Levels: {{ dictionary_levels|join(', ') }}]
                    </small>
                    {% endif %}
                </td>
                <td class="text-right text-nowrap">
                    {{ linkTo(['admin/dictionaries/edit/' ~ dictionary.id, '<span class="fa fa-edit fa-fw"></span>Edit']) }}
                    ·
                    {{ linkTo(['admin/dictionaries/delete/' ~ dictionary.id, '<span class="fa fa-trash-o text-danger fa-fw"></span>' ]) }}
                </td>
            </tr>
            {% endfor %}
        </table>
        {% else %}
            <div class="no-results alert alert-success">
                <p class="text-center">Nothing matched your search query</p>
            </div>
        {% endif %}
        {% if pagination_links %}{{ pagination_links }}{% endif %}
    </div>
</div>
