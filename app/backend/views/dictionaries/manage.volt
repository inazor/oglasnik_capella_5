{# Admin Manage dictionary values Listing/View #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div>
            <div class="pull-right">
                <button class="btn btn-primary" name="btnSaveDictionariesOrder" id="btnSaveDictionariesOrder"><span class="fa fa-save"></span> Save order</button>
                {{ linkTo(['admin/dictionaries/createvalue/' ~ dictionary.id, '<span class="fa fa-book fa-fw"></span> Add new value', 'class': 'btn btn-info']) }}
            </div>
            <h1 class="page-header">{{ dictionary.name }} <small>{{ form_title_long }}</small></h1>
        </div>
        {{ flashSession.output() }}
        <div>
            <button id="toggle-all" disabled="disabled" class="btn btn-default btn-xs">Toggle All</button>
        </div>
        {{ form('admin/dictionaries/save-order/' ~ dictionary.id, 'id' : 'frm_dictionaries', 'method' : 'post') }}
            {{ hiddenField('_csrftoken') }}
            {{ hiddenField('dictionaries_order') }}
            {{ hiddenField('tree_root_id') }}
            {% if count(items) > 0 %}
            <div class="nested-list-sortable">
                {% if available_item_actions is not defined %}
                {% set available_item_actions = [] %}
                {% endif %}
                {{ partial("chunks/nested-list", ['items': items, 'class': 'sortable ui-sortable', 'available_item_actions': available_item_actions]) }}
            </div>
            {% else %}
            <p>This dictionary currently has no values! You can start by creating the first one... :)</p>
            {% endif %}
        {{ endForm() }}
    </div>
</div>
