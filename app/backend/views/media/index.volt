{# Media Library index/search #}
<div class="row" id="media_library_container" data-is-popup="{{ is_popup }}">
    <div class="col-lg-12 col-md-12">
        <div>
            <div class="pull-right">
                <button id="media-upload-btn" class="btn btn-info">
                    <span class="glyphicon glyphicon-upload"></span> Upload
                    <span id="total-progress" class="progress progress-total-inline" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                        <span class="progress-bar progress-bar-striped active" style="width:0%;" data-dz-uploadprogress></span>
                    </span>
                </button>
            </div>
            <h1 class="page-header">{{ page_title }} <small id="dropzone-notice">Drag &amp; drop files anywhere to upload</small></h1>
        </div>
        {{ flashSession.output() }}
        <div class="row">
            <div class="col-xs-12">
                <button id="bulk-select-mode-toggle" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-wrench"></span> Bulk Select</button>
                <span class="bulk-select-helpers hidden">
                    <button id="bulk-select-all" class="btn btn-default btn-xs">All</button>
                    <button id="bulk-select-none" class="btn btn-default btn-xs">None</button>
                    <button id="bulk-select-toggle" class="btn btn-default btn-xs">Toggle</button>
                </span>
                {% if is_popup == 0 %}
                <button id="bulk-delete" disabled="disabled" class="btn btn-danger btn-xs hidden"><span class="fa fa-trash-o"></span> Delete Selected</button>
                {% else %}
                <button id="bulk-import" disabled="disabled" class="btn btn-primary btn-xs hidden"><span class="fa fa-download"></span> Import Selected</button>
                {% endif %}
            </div>
        </div>
        <br>
        <form class="form media-search" role="form">
            {{ hiddenField('field') }}
            {{ hiddenField('mode') }}
            {{ hiddenField('is_popup') }}
            <div class="row">
                <div class="col-sm-6">
                    <div class="input-group search-panel">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="dropdown-label" id="search_field">All fields</span> <span class="caret"></span>
                            </button>
                            <ul data-param="#field" class="dropdown-menu" role="menu">
                                <li role="presentation" class="dropdown-header">Narrow down your search</li>
                                {%- for f in available_fields -%}
                                    <li{% if f == field %} class="active"{% endif %}><a href="#{{ f }}">{{ f }}</a></li>
                                {%- endfor -%}
                                <li class="divider"></li>
                                <li><a href="#all">All fields</a></li>
                            </ul>
                        </div>
                        <input type="text" class="form-control" name="q" value="{{ q|escape_attr }}" placeholder="Search term(s)...">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul data-param="#mode" class="dropdown-menu dropdown-menu-right" role="menu">
                                <li role="presentation" class="dropdown-header">Search mode</li>
                                {%- for m, m_desc in mode_options -%}
                                    <li{% if m == mode %} class="active"{% endif %}><a href="#{{ m }}">{{ m_desc }}</a></li>
                                {%- endfor -%}
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <select id="media-type" name="type" class="form-control">
                        {%- for type_id, type_data in available_types -%}
                            <option {% if type_id == type %}selected="selected" {% endif %}value="{{ type_id }}">{{ type_data['title'] }}</option>
                        {%- endfor -%}
                    </select>
                </div>
                <div class="col-sm-2">
                    <select id="order-by" name="order_by" class="form-control">
                        {%- for value, title in order_by_options -%}
                            <option {% if value == order_by %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
                        {%- endfor -%}
                    </select>
                </div>
                <div class="col-sm-2">
                    <select id="dir" name="dir" class="form-control">
                        {%- for value, title in dir_options -%}
                            <option {% if value == dir %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
                        {%- endfor -%}
                    </select>
                </div>
            </div>
        </form>
        <br>

        <div class="row media-search-results">

            <div id="dropzone-errors" class="col-lg-12"></div>

            <div id="dropzone-previews" class="dropzone-previews">
                <div id="dropzone-template">
                    <div class="col-md-3 col-sm-4 col-xs-6">
                        <div tabindex="0" role="checkbox" aria-checked="false" data-id="" class="attachment">
                            <div class="attachment-preview">
                                {% if is_popup == 0 %}<a class="edit-link" href="#">{% endif %}
                                    <div class="thumb">
                                        <div class="centered">
                                            <img data-dz-thumbnail>
                                        </div>
                                        <div class="filename">
                                            <div data-dz-name></div>
                                            <!--small class="size" data-dz-size></small-->
                                        </div>
                                    </div>
                                    <div class="progress media-progress" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                        <div class="progress-bar progress-bar-striped active" style="width:0;" data-dz-uploadprogress></div>
                                    </div>
                                {% if is_popup == 0 %}</a>{% endif %}
                            </div>
                            <a class="action check" href="#" title="Deselect" tabindex="-1"><span class="icon glyphicon glyphicon-ok"></span><span class="icon icon-hover glyphicon glyphicon-minus"></span></a>
                            {% if is_popup == 0 -%}
                            <a class="action delete btn-danger" href="#"><span class="icon fa fa-trash-o"></span></a>
                            {%- else -%}
                            <a class="action import btn-primary" href="#" data-id="" data-src=""><span class="icon fa fa-download"></span></a>
                            {%- endif %}
                        </div>
                    </div>
                </div>
            </div>

        {% for media in page.items %}
            <div class="col-md-3 col-sm-4 col-xs-6">
                <div tabindex="0" role="checkbox" aria-checked="false" data-id="{{ media.id }}" class="attachment">
                    {% set preview = media.get_backend_thumb().toArray() %}
                    {% if preview is defined %}
                    <div class="attachment-preview type-{{ preview['type'] }} subtype-{{ preview['subtype'] }} {{ preview['orientation'] }}">
                        {% if is_popup == 0 %}<a class="edit-link" href="{{ this.url.get('admin/media/edit/' ~ media.id) }}">{% endif %}
                            <div class="thumb">
                                <div class="centered">
                                    {{ preview['tag'] }}
                                </div>
                                {% if 'image' !== preview['type'] %}
                                <div class="filename">
                                    <div>{{- preview['filename_orig']|escape -}}</div>
                                </div>
                                {% endif %}
                            </div>
                        {% if is_popup == 0 %}</a>{% endif %}
                    </div>
                    <a class="action check" href="#" title="Deselect" tabindex="-1"><span class="icon glyphicon glyphicon-ok"></span><span class="icon icon-hover glyphicon glyphicon-minus"></span></a>
                    {% if is_popup == 0 -%}
                    <a class="action delete btn-danger" href="{{ this.url.get('admin/media/delete/' ~ media.id) }}"><span class="icon fa fa-trash-o"></span></a>
                    {%- else -%}
                    <a class="action import btn-primary" href="#" data-id="{{ media.id }}" data-src="{{ preview is defined ? preview['src'] : '' }}"><span class="icon fa fa-download"></span></a>
                    {%- endif %}
                    {% endif %}
                </div>
            </div>
        {% else %}
            <div class="no-results col-lg-12">
                <div class="alert alert-success">
                    <p class="text-center">Nothing matched your search query</p>
                </div>
            </div>
        {% endfor %}
        {% if pagination_links %}
            <div class="clearfix"></div>
            <div class="col-lg-12">{{- pagination_links -}}</div>
        {% endif %}

        </div>

    </div>
</div>
