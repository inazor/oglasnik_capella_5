{# Admin Parameter Edit View #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <h1 class="page-header">Parameters <small>{{ form_title_long }}</small></h1>

        {{ flashSession.output() }}

        {{ form(NULL, 'id' : 'frm_parameters', 'method' : 'post', 'autocomplete' : 'off') }}
            {{ hiddenField('_csrftoken') }}
            {{ hiddenField('next') }}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Parameter details</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 col-lg-3">
                            {% set field = 'type_id' %}
                            <div class="form-group">
                                <label class="control-label" for="{{ field }}">Type</label>
                                <select class="form-control show-tick" name="{{ field }}" id="{{ field }}">
                                {% for parameters_type in parameters_types %}
                                    <option
                                        value="{{ parameters_type.id }}"
                                        data-accept-dictionary="{{ parameters_type.accept_dictionary | default('0') }}"
                                        data-can-other="{{ parameters_type.can_other | default('0') }}"
                                        data-is-dependable="{{ parameters_type.is_dependable | default('0') }}"
                                        {% if parameters_type.class %} data-icon="fa {{ parameters_type.class }} fa-fw"{% endif %}
                                        {% if parameter.type_id == parameters_type.id %} selected{% endif %}
                                    >{{ parameters_type.name | default(parameters_type.id) }}</option>
                                {% endfor %}
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3">
                            {% set field = 'name' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Name</label>
                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{ parameter.name | default('') }}" maxlength="128" />
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% endif %}
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3">
                            {% set field = 'slug' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Slug</label>
                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{ parameter.slug | default('') }}" maxlength="64" />
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% endif %}
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3{% if dictionary_container_is_hidden %} hidden{% endif %}" id="dictionary_container">
                            {% set field = 'dictionary_id' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Dictionary</label>
                                <select class="form-control show-tick" name="{{ field }}" id="{{ field }}">
                                {%- for dictionary in dictionaries -%}
                                    <option {% if dictionary_id is defined and dictionary.id == dictionary_id %}selected="selected" {% endif %}value="{{ dictionary.id }}" data-max-level="{{ dictionary.max_level|default('0') }}">{{ dictionary.name }}</option>
                                {%- endfor -%}
                                </select>
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% endif %}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="dictionary_values_container" class="panel panel-default{% if dictionary_container_is_hidden %} hidden{% endif %}">
                <div class="panel-heading">
                    <h4 class="panel-title">Current dictionary values</h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <ul id="current_dictionary_values">
                                {% if dictionary_values %}
                                {% for dict_value in dictionary_values %}
                                <li>{{ dict_value.name }}</li>
                                {% else %}
                                <li class="text-danger">No values found in this dictionary...</li>
                                {% endfor %}
                                {% endif %}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <button class="btn btn-primary" type="submit" name="save">{{ save_text }}</button>
                <button class="btn btn-primary" type="submit" name="saveexit">{{ save_text }} and exit</button>
                <button class="btn btn-default" type="submit" name="cancel">Cancel</button>
            </div>
        {{ endForm() }}
    </div>
</div>
