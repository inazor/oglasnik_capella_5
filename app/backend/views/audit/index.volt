{# Audit Log Viewer #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <h1 class="page-header">{{ page_title }}</h1>
        {{ flashSession.output() }}
        <form class="form-inline" role="form">
            {{ hiddenField('model') }}
            {{ hiddenField('field') }}
            {{ hiddenField('mode') }}
            <div class="row">
                <div class="col-sm-6">
                    <div class="input-group search-panel">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="dropdown-label" id="model_name">Specific model / All models</span> <span class="caret"></span>
                            </button>
                            <ul data-param="#model" class="dropdown-menu" role="menu">
                                <li role="presentation" class="dropdown-header">Narrow down your search</li>
                                {% for model_value, model_name in model_options %}
                                    <li{% if model_value == model %} class="active"{% endif %}><a href="#{{ model_value }}">{{ model_name }}</a></li>
                                {% endfor %}
                                <li class="divider"></li>
                                <li><a href="#all">All models</a></li>
                            </ul>
                        </div>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle no-border-radius" data-toggle="dropdown">
                                <span class="dropdown-label" id="search_field">Specific field / Everything</span> <span class="caret"></span>
                            </button>
                            <ul data-param="#field" class="dropdown-menu" role="menu">
                                <li role="presentation" class="dropdown-header">Narrow down your search</li>
                                {% for f in field_options %}
                                    <li{% if f == field %} class="active"{% endif %}><a href="#{{ f }}">{{ f }}</a></li>
                                {% endfor %}
                                <li class="divider"></li>
                                <li><a href="#all">Everything</a></li>
                            </ul>
                        </div>
                        <input type="text" class="form-control" name="q" value="{{ q|escape_attr }}" placeholder="Search term(s)...">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul data-param="#mode" class="dropdown-menu dropdown-menu-right" role="menu">
                                <li role="presentation" class="dropdown-header">Search mode</li>
                                {% for m, m_desc in mode_options %}
                                    <li{% if m == mode %} class="active"{% endif %}><a href="#{{ m }}">{{ m_desc }}</a></li>
                                {% endfor %}
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <table class="colors-legend table pull-right table-bordered table-condensed" style="width:auto;">
                        <tr>
                            <th scope="row">Legend</th>
                            <td class="success"><span class="fa fa-file-o fa-fw"></span> Create</td>
                            <td class="warning"><span class="fa fa-edit fa-fw"></span> Update</td>
                            <td class="danger"><span class="fa fa-trash-o fa-fw"></span> Delete</td>
                            <td class="info"><span class="fa fa-info-circle fa-fw"></span> Log/Info</td>
                        </tr>
                    </table>
                </div>
            </div>
        </form>
        <table class="table table-striped table-hover master-detail">
            <tr>
                <th>Model <small class="text-muted">Details: <a id="toggle-all" href="javascript:void(0);">toggle all</a> / <a id="show-all" href="javascript:void(0);">show all</a> / <a id="hide-all" href="javascript:void(0);">hide all</a> / <span class="text-muted">(or click a certain row to reveal specifics)</span></small></th>
                <th>Username</th>
                <th>IP</th>
                <th class="text-right">Date</th>
            </tr>
            {% for log in page.items %}
                {% set row_helpers = [
                    'C' : [ 'title': 'Create', 'icon': '<span class="fa fa-file-o fa-fw"></span>', 'classname': 'success' ],
                    'D': [ 'title': 'Delete', 'icon': '<span class="fa fa-trash-o fa-fw"></span>', 'classname': 'danger' ],
                    'U': ['title': 'Update', 'icon': '<span class="fa fa-edit fa-fw"></span>', 'classname': 'warning'],
                    'L': ['title': 'Log', 'icon': '<span class="fa fa-info-circle fa-fw"></span>', 'classname': 'info']
                ] %}
                {% set log_type = log.type %}
                {% set row_title = row_helpers[log.type]['title'] %}
                {% set row_class = row_helpers[log.type]['classname'] %}
                {% set row_icon = row_helpers[log.type]['icon'] %}
                <tr id="log-{{ log.id }}" title="Event Type: {{ row_title }}" class="{{ row_class }} master-row" data-pk-val="{{ log.model_pk_val }}">
                    <td class="col-md-9">
                        {% if log.model_name %}
                            {% set display = log.model_name %}
                            {% if log.model_pk_val and log_type != 'D' %}
                                {% if models_controllers_map[display] is defined %}
                                    {% set model_controller = models_controllers_map[display] %}
                                    {% set edit_href = 'admin/' ~ model_controller ~ '/edit/' ~ log.model_pk_val ~ '?next=' ~ this.request.getURI() %}
                                    {% set display = linkTo([edit_href, display]) %}
                                {% endif %}
                            {% endif %}
                        {% elseif log.message %}
                            {% set display = log.message|e|soft_break %}
                        {% endif %}
                        {{ row_icon }} {{ display }}
                    </td>
                    <td class="col-md-1">
                        {% if log.user_id %}
                            {{ linkTo(['admin/users/edit/' ~ log.user_id, log.username]) }}
                        {% else %}
                            {{ log.username }}
                        {% endif %}
                    </td>
                    <td class="col-md-1">{{ log.ip }}</td>
                    <td class="col-md-1 text-right nowrap">{{ log.created_at }}</td>
                </tr>
                {% if log.details %}
                <tr data-master="log-{{ log.id }}" class="detail-row">
                    <td colspan="4">
                        <table class="table table-condensed table-striped table-bordered" style="width:auto;">
                            {% for detail in log.details %}
                            <tr>
                                <td class="field-name"><strong>{{ detail.field_name }}</strong></td>
                                <td class="value old-value"><code>{% if detail.old_value === null %}null{% endif %}{{ detail.old_value }}</code></td>
                                <td class="col-xs-1 arrow" style="width:20px;">&rightarrow;</td>
                                <td class="value new-value"><code>{% if detail.new_value === null %}null{% endif %}{{ detail.new_value }}</code></td>
                            </tr>
                            {% endfor %}
                        </table>
                    </td>
                </tr>
                {% endif %}
                {% if log.controller %}
                <tr data-master="log-{{ log.id }}" class="detail-row">
                    <td colspan="4">
                        <table class="table table-condensed table-striped table-bordered" style="width:auto;">
                            <tr>
                                <td class="field-name"><strong>Controller</strong></td>
                                <td class="value new-value"><code>{{ log.controller }}</code></td>
                            </tr>
                            <tr>
                                <td class="field-name"><strong>Action</strong></td>
                                <td class="value new-value"><code>{{ log.action }}</code></td>
                            </tr>
                            {% if (not(log.params is empty)) %}
                            <tr>
                                <td class="field-name"><strong>Params</strong></td>
                                <td class="value new-value"><code>{{ log.params|soft_break }}</code></td>
                            </tr>
                            {% endif %}
                        </table>
                    </td>
                </tr>
                {% endif %}
            {% else %}
                <tr><td colspan="4"><p class="text-center">No matching records found!</p></td></tr>
            {% endfor %}
        </table>
        {% if pagination_links %}{{ pagination_links }}{% endif %}
    </div>
</div>
