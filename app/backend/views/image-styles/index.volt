{# Admin Image Styles Listing #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div>
            <div class="pull-right">
                {{ linkTo(['admin/image-styles/create', '<span class="fa fa-plus fa-fw"></span> Add new', 'class': 'btn btn-info']) }}
            </div>
            <h1 class="page-header">{{ page_title }}</h1>
        </div>
        {{ flashSession.output() }}
        <table class="table table-striped table-hover">
            <tr>
                <th class="col-xs-7">Slug</th>
                <th class="col-xs-2 text-nowrap text-right">Max Dimensions</th>
                <th class="col-xs-1 text-center">Hard Crop</th>
                <th class="col-xs-1 text-center">Watermark</th>
                <th class="col-xs-1">&nbsp;</th>
            </tr>
            {% for record in styles %}
            <tr>
                <td>{{ linkTo(['admin/image-styles/edit/' ~ record.id, '<i class="fa fa-fw fa-edit"></i>' ~ record.slug]) }}</td>
                <td class="text-right"><code>{{ record.width }} px</code> <span class="text-muted">&times;</span> <code>{{ record.height }} px</code></td>
                <td class="text-center">{% if record.crop %}<i class="fa fa-fw fa-crop"></i>{% endif %}</td>
                <td class="text-center">{% if record.watermark %}Yes{% else %}No{% endif %}</td>
                <td class="text-right">{{ linkTo(['admin/image-styles/delete/' ~ record.id, '<i class="text-danger fa fa-fw fa-trash-o"></i>', 'class': 'text-danger']) }}</td>
            </tr>
            {% else %}
            <tr><td class="info text-center" colspan="5">No image styles found!</td></tr>
            {% endfor %}
        </table>
    </div>
</div>
