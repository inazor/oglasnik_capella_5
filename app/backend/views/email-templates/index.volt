{# Admin Email Templates Editor #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <h1 class="page-header">Email Templates</h1>
        {{ flashSession.output() }}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Email Templates Editor</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    {{ form(NULL, 'id' : 'frm-tplchooser', 'method' : 'post') }}
                        {{ hiddenField('_csrftoken') }}
                        <div class="col-md-6 col-lg-6">
                            <div class="form-group">
                                <label class="control-label" for="template_id">Template file</label>
                                {{ templates_dropdown }}
                            </div>
                        </div>
                    {{ endForm() }}
                </div>
                {% if template_contents != '' %}
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        {{ form(NULL, 'id' : 'frm-tpleditor', 'method' : 'post', 'class' : 'form-horizontal') }}
                            {{ hiddenField('_csrftoken') }}
                            {{ hiddenField(['code', 'value': template_contents])}}
                            {{ hiddenField(['tpl_id', 'value': tpl_id])}}
                            <div id="tpleditor" style="height:300px;">{{ template_contents }}</div>
                            <div class="margin-top-10">
                                <button class="btn btn-primary" type="submit" name="save">Update file</button>
                            </div>
                        {{ endForm() }}
                    </div>
                </div>
                {% endif %}
            </div>
        </div>
    </div>
</div>
