<ul class="{{ class }}">
    {{ partial('chunks/nested-list-simple-items', ['items': items, 'available_item_actions': available_item_actions, 'type': listing_type]) }}
</ul>
