{% for item in items %}
    {% if listing_type == 'export2avus' %}
        {% set item_status = Models_Categories__getMappedStatus(item) %}
    {% elseif listing_type == 'parametrize' %}
        {% set item_status = Models_Categories__getParametersStatus(item) %}
    {% elseif listing_type == 'products' %}
        {% set item_status = Models_Categories__getProductsStatus(item) %}
    {% endif %}

<li class="nested-item-li{% if item.active == 0 %} disabled{% endif %} no-nest" data-id="{{ item.id }}" id="item_{{ item.id }}">
    {% set children = item.children %}
    {% set hasChildren = (count(children) > 0) %}
    <div class="nested-item-div{% if hasChildren %} with-children{% endif %}">
        <div class="nested-item-row no-drag">
            <div class="children{% if hasChildren %} clickable{% endif %}">
                <span class="toggle fa {% if hasChildren %}fa-plus-square-o{% else %}fa-angle-right{% endif %}"></span>
            </div>
            <div class="name-cat">
                <span class="name">{{ item.name }} <small>[{{ item_status['text'] }}]</small></span>
            </div>
            {% if available_item_actions is iterable and item_status['can_be'] %}
            <div class="actions-cat">
            {% for available_action in available_item_actions %}
                {{ linkTo([available_action['path'] ~ item.id, available_action['name']]) }}
                {% if not loop.last %} · {% endif %}
            {% endfor %}
            </div>
            {% endif %}
        </div>
    </div>
    {% if hasChildren %}
    {{ partial('chunks/nested-list-simple', ['items': children, 'class': 'sublist', 'available_item_actions': available_item_actions, 'type': listing_type]) }}
    {% endif %}
</li>
{% endfor %}
