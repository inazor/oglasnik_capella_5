<?php

namespace Baseapp\Backend\Controllers;

use Baseapp\Controllers\BaseBackendController;
use Baseapp\Models\ImageStyles;
use Baseapp\Traits\CrudActions;

class ImageStylesController extends BaseBackendController
{
    use CrudActions;

    public $crud_model_class = 'Baseapp\Models\ImageStyles';

    protected $allowed_roles = array('admin');

    public function indexAction()
    {
        $title = 'Image Styles';

        $this->tag->setTitle($title);
        $this->view->setVar('page_title', $title);
        $this->view->setVar('styles', ImageStyles::findAll());
    }

    public function editAction($entity_id = null)
    {
        $title = 'Edit Image Style';
        $this->tag->setTitle($title);

        $entity_id = (int) $entity_id;
        if (!$entity_id) {
            $this->flashSession->error('Missing required ID parameter for edit action!');
            return $this->redirect_back();
        }

        /**
         * @var $model \Baseapp\Models\ImageStyles
         * @var $entity \Baseapp\Models\ImageStyles
         */
        $model = $this->crud_model_class;
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }

        if ($this->request->isPost()) {
            $saved = $entity->backend_save($this->request);
            if ($saved instanceof ImageStyles) {
                $this->flashSession->success('<strong>Changes successfully saved!</strong>');
                $save_action = $this->get_save_action();
                if ('save' === $save_action) {
                    return $this->redirect_self();
                } else {
                    return $this->redirect_to($this->get_next_url());
                }
            } else {
                $this->view->setVar('errors', $saved);
                $this->flashSession->error('<strong>Error!</strong> Fix fields that failed to validate.');
            }
        }

        $this->view->setVar('form_title', $title);
        $this->view->setVar('entity', $entity);
    }

    public function createAction()
    {
        $title = 'New Image Style';
        $this->tag->setTitle($title);

        $this->view->pick('image-styles/edit');  // Use the same form/view as editAction
        $entity = new ImageStyles();

        if ($this->request->isPost()) {
            $created = $entity->backend_create($this->request);
            if ($created instanceof ImageStyles) {
                $this->flashSession->success('<strong>New Image Style successfully created!</strong>');
                $save_action = $this->get_save_action();
                $next_url = $this->get_next_url();
                // Override next url to go to entity edit in save button case
                if ('save' === $save_action) {
                    $next_url = 'admin/image-styles/edit/' . $created->id;
                }
                return $this->redirect_to($next_url);
            } else {
                $this->view->setVar('errors', $created);
                $this->flashSession->error('<strong>Error!</strong> Please fix any fields that failed to validate.');
            }
        }

        $this->view->setVar('form_title', $title);
        $this->view->setVar('entity', $entity);
    }
}
