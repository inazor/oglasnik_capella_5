<?php

namespace Baseapp\Backend\Controllers;

use Baseapp\Extension\Tag;
use Baseapp\Models\Sections;
use Baseapp\Models\Categories;
use Baseapp\Models\GenericImages;
use Baseapp\Models\MiscSettings;
use Baseapp\Traits\CrudActions;

/**
 * Backend Sections Controller
 */
class SectionsController extends IndexController
{
    use CrudActions;

    /* @var \Baseapp\Models\Sections */
    public $crud_model_class = 'Baseapp\Models\Sections';

    protected $allowed_roles = array('admin');

    /**
     * Index Action
     */
    public function indexAction()
    {
        $location = $this->request->getQuery('location', null, 'homepage');

        $this->tag->setTitle('Sections');

        $this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.js');
        $this->assets->addJs('assets/backend/js/list-sortable.js');
        $this->assets->addJs('assets/backend/js/sections-index.js');

        $sections = Sections::find(array(
            'conditions' => 'location = :location:',
            'bind'       => array('location' => $location),
            'order'      => 'sort_order ASC, name ASC',
        ));

        $this->view->setVar('sections', $sections);
        $this->view->setVar('location', $location);

        $fullWidthLocationRecord = MiscSettings::getSectionLocationFullWidthRecord($location);
        $fullWidthLocationValue  = 0;
        if ($fullWidthLocationRecord && 1 === (int) $fullWidthLocationRecord->value) {
            $fullWidthLocationValue = 1;
        }
        $this->view->setVar('fullWidthLocation', $fullWidthLocationValue);
    }

    /**
     * @param $section \Baseapp\Models\Sections
     */
    public function setup_locations_dropdown($section)
    {
        Tag::setDefault('location', $section->location);
        $location_dropdown = Tag::select(array(
            'location',
            $section->getAvailableLocations(),
            'useEmpty' => false,
            'class'    => 'form-control show-tick',
        ));
        $this->view->setVar('location_dropdown', $location_dropdown);
    }

    public function add_common_crud_assets()
    {
        $this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.js');
        $this->assets->addJs('assets/backend/js/list-sortable.js');
        $this->assets->addJs('assets/backend/js/sections-edit.js');
    }

    public function get_chosen_category_ids()
    {
        $category_ids = array();
        if ($this->request->isPost()) {
            if (trim($categories = $this->request->getPost('json_categories', 'string', ''))) {
                if (false !== strpos($categories, ',')) {
                    $category_ids = explode(',', $categories);
                } elseif ((int) trim($categories)) {
                    $category_ids[] = (int) trim($categories);
                }
            }
        }
        return $category_ids;
    }

    /**
     * Create new section
     *
     * @return \Phalcon\Http\ResponseInterface
     */
    public function createAction()
    {
        $this->tag->setTitle('Create new section');
        $this->view->setVar('form_title_long', 'New section');

        $this->add_common_crud_assets();

        if ($this->request->isPost() && $this->request->hasPost('section_icon')) {
            $section_icon = $this->request->getPost('section_icon');
        } else {
            $section_icon = null;
        }

        // Use the same form/view as editAction
        $this->view->pick('sections/edit');

        // Populate category ids array
        $category_ids = $this->get_chosen_category_ids();

        $entity = new Sections();
        $entity->json = json_decode('{}');  // initialize empty json

        if ($this->request->isPost()) {
            $created = $entity->add_new($this->request, $category_ids);
            if ($created instanceof Sections) {
                $this->flashSession->success('<strong>Successfully created new section!</strong>');
                $save_action = $this->get_save_action();
                $next_url = $this->get_next_url();
                // Override next url to go to entity edit in save button case
                if ('save' === $save_action) {
                    $next_url = 'admin/sections/edit/' . $created->id;
                }

                // Handle avatar upload if we have something properly uploaded...
                if ($this->request->hasFiles()) {
                    if ($this->request->hasPost('deleteSponsorshipLogo') && 'delete' === $this->request->getPost('deleteSponsorshipLogo')) {
                        $entity_sponsorship_logo = $saved->getOrCreateSponsorshipLogo();
                        if ($entity_sponsorship_logo) {
                            $entity_sponsorship_logo->delete();
                        }
                    }

                    if ($this->request->hasPost('deleteBackgroundPic') && 'delete' === $this->request->getPost('deleteBackgroundPic')) {
                        $entity_background_pic = $saved->getOrCreateBackgroundPic();
                        if ($entity_background_pic) {
                            $entity_background_pic->delete();
                        }
                    }

                    $bgPicUploader = $this->createUploader(['upload_base' => GenericImages::getUploadsDir()], ['backgroundPic']);
                    if ($bgPicUploader->isValid()) {
                        $info = $bgPicUploader->move();
                        if (!empty($info)) {
                            $entity_background_pic = $saved->getOrCreateBackgroundPic();
                            foreach ($info as $k => $file) {
                                $entity_background_pic->assignNew($file['filename'], $file);
                            }
                        }
                    }

                    $sponsorLogoUploader = $this->createUploader(['upload_base' => GenericImages::getUploadsDir()], ['sponsorshipLogo']);
                    if ($sponsorLogoUploader->isValid()) {
                        $info = $sponsorLogoUploader->move();
                        if (!empty($info)) {
                            $entity_sponsorship_logo = $saved->getOrCreateSponsorshipLogo();
                            foreach ($info as $k => $file) {
                                $entity_sponsorship_logo->assignNew($file['filename'], $file);
                            }
                        }
                    }
                }

                return $this->redirect_to($next_url);
            } else {
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
                $this->view->setVar('errors', $created);
                $this->view->setVar('link_type', $this->request->getPost('link_type', 'int', Sections::LINK_TYPE_DEFAULT));
                $this->view->setVar('link_to_specific_category', $this->request->getPost('json_link_to_specific_category', 'int', null));
                $this->view->setVar('custom_link', $this->request->getPost('json_custom_link', 'string', null));
            }
        } else {
            // default for any new section
            $entity->active = 1;
            $entity->location = $this->request->hasQuery('location') ? $this->request->getQuery('location') : 'homepage';
            $this->view->setVar('link_type', Sections::LINK_TYPE_DEFAULT);
            $this->view->setVar('link_to_specific_category', null);
            $this->view->setVar('custom_link', null);
        }

        $this->view->setVar('category_ids', Categories::findFirst(1)->getSelectValues('--', null, false));
        $this->view->setVar('selected_category_ids', $category_ids);
        $this->view->setVar('section_categories', $entity->getCategories($category_ids));
        $this->view->setVar('section', $entity);
        Tag::setDefault('json_categories', isset($entity->json->categories) ? implode(',', $entity->json->categories) : '');

        $this->setup_locations_dropdown($entity);
        $this->section_icon_dropdown($section_icon);
    }

    /**
     * Edit existing section
     */
    public function editAction($entity_id = null)
    {
        $entity_id = (int) $entity_id;
        if (!$entity_id) {
            $this->flashSession->error('Missing required ID parameter for edit action!');
            return $this->redirect_back();
        }

        $model = $this->crud_model_class;
        /* @var $entity \Baseapp\Models\Sections */
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }

        $this->tag->setTitle('Edit existing section');
        $this->view->setVar('form_title_long', 'Edit section');

        $this->add_common_crud_assets();

        // Populate category ids array
        $category_ids = $this->get_chosen_category_ids();

        if ($this->request->isPost()) {
            // Determine which save button was used
            $save_action = $this->get_save_action();
            $next_url = $this->get_next_url();
            $saved = $entity->save_changes($this->request, $category_ids);
            if ($saved instanceof Sections) {
                $this->flashSession->success('<strong>Section saved successfully!</strong>');

                // Handle avatar upload if we have something properly uploaded...
                if ($this->request->hasFiles()) {
                    if ($this->request->hasPost('deleteSponsorshipLogo') && 'delete' === $this->request->getPost('deleteSponsorshipLogo')) {
                        $entity_sponsorship_logo = $saved->getOrCreateSponsorshipLogo();
                        if ($entity_sponsorship_logo) {
                            $entity_sponsorship_logo->delete();
                        }
                    }

                    if ($this->request->hasPost('deleteBackgroundPic') && 'delete' === $this->request->getPost('deleteBackgroundPic')) {
                        $entity_background_pic = $saved->getOrCreateBackgroundPic();
                        if ($entity_background_pic) {
                            $entity_background_pic->delete();
                        }
                    }

                    $bgPicUploader = $this->createUploader(['upload_base' => GenericImages::getUploadsDir()], ['backgroundPic']);
                    if ($bgPicUploader->isValid()) {
                        $info = $bgPicUploader->move();
                        if (!empty($info)) {
                            $entity_background_pic = $saved->getOrCreateBackgroundPic();
                            foreach ($info as $k => $file) {
                                $entity_background_pic->assignNew($file['filename'], $file);
                            }
                        }
                    }

                    $sponsorLogoUploader = $this->createUploader(['upload_base' => GenericImages::getUploadsDir()], ['sponsorshipLogo']);
                    if ($sponsorLogoUploader->isValid()) {
                        $info = $sponsorLogoUploader->move();
                        if (!empty($info)) {
                            $entity_sponsorship_logo = $saved->getOrCreateSponsorshipLogo();
                            foreach ($info as $k => $file) {
                                $entity_sponsorship_logo->assignNew($file['filename'], $file);
                            }
                        }
                    }
                }

                if ('save' === $save_action) {
                    return $this->redirect_self();
                } else {
                    return $this->redirect_to($next_url);
                }
            } else {
                $this->view->setVar('errors', $saved);
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
                $this->view->setVar('link_type', $this->request->getPost('link_type', 'int', Sections::LINK_TYPE_DEFAULT));
                $this->view->setVar('link_to_specific_category', $this->request->getPost('json_link_to_specific_category', 'int', null));
                $this->view->setVar('custom_link', $this->request->getPost('json_custom_link', 'string', null));
            }
        } else {
            // used to populate the current section's category ids
            $category_ids = $entity->getCategoryIDs();
            $this->view->setVar('link_type', isset($entity->json->link_type) ? $entity->json->link_type : Sections::LINK_TYPE_DEFAULT);
            $this->view->setVar('link_to_specific_category', isset($entity->json->link_to_specific_category) ? $entity->json->link_to_specific_category : null);
            $this->view->setVar('custom_link', isset($entity->json->custom_link) ? $entity->json->custom_link : null);
        }

        $this->view->setVar('category_ids', Categories::findFirst(1)->getSelectValues('--', null, false));
        $this->view->setVar('selected_category_ids', $category_ids);
        $this->view->setVar('section_categories', $entity->getCategories());
        $this->view->setVar('section', $entity);
        Tag::setDefault('json_categories', isset($entity->json->categories) ? implode(',', $entity->json->categories) : '');

        $this->setup_locations_dropdown($entity);
        $section_icon = null;
        if (isset($entity->json->icon)) {
            $section_icon = $entity->json->icon;
        }
        $this->section_icon_dropdown($section_icon);
    }

    /**
     * Save sections order Action
     */
    public function saveOrderAction()
    {
        if ($this->request->isPost()) {
            $sections_order = json_decode($this->request->getPost('sections_order'), true);

            $overall_status = true;

            if (is_array($sections_order) && count($sections_order)) {
                $current_sort_order = 1;
                foreach ($sections_order as $section_id) {
                    if ($section = Sections::findFirst((int) $section_id)) {
                        $section->sort_order = $current_sort_order;
                        $current_sort_order++;
                        if (false == $section->update()) {
                            $overall_status = false;
                        }
                    }
                }
            }

            if ($overall_status) {
                $this->flashSession->success('<strong>Sections reordered successfully!</strong>');
            } else {
                $this->flashSession->error('<strong>Error!</strong> Sections were not reordered successfully!');
            }
        }

        return $this->redirect_back();
    }

    public function triggerFullWithAction()
    {
        $this->disable_view();
        $this->response->setContentType('application/json', 'UTF-8');
        $response_array = array('status' => false);

        if (!$this->request->isAjax()) {
            $this->response->setStatusCode(400, 'Bad Request');
            $response_array['msg'] = 'Bad Request';
        } else {
            if ($this->request->isPost()) {
                $section_location         = trim($this->request->getPost('location'));
                $fullWidth_value          = (int) $this->request->getPost('value');

                $response_array['status'] = MiscSettings::setSectionLocationFullWidth($section_location, $fullWidth_value);
                if (!$response_array['status']) {
                    $response_array['msg'] = 'Full-width section layout could not be changed!';
                }
            }
        }

        $this->response->setJsonContent($response_array);
        return $this->response;
    }

    /**
     * activeToggle Action
     */
    public function activeToggleAction()
    {
        $params = $this->router->getParams();
        if ($section_id = (int) $params[0]) {
            if ($section = Sections::findFirst($section_id)) {
                if ($section->activeToggle()) {
                    $this->flashSession->success('Active status for section <strong>' . $section->name . '</strong> changed successfully.');
                } else {
                    $this->flashSession->error('Failed changing active status for section <strong>' . $section->name . '</strong>.');
                }
            } else {
                $this->flashSession->error('Section with ID: <strong>'.$section_id.'</strong> not found.');
            }
        }

        return $this->redirect_back();
    }

    /**
     * Helper method to generate section_icon dropdown
     *
     * @param integer $preselected_section_icon    Icon to preselect
     */
    public function section_icon_dropdown($preselected_section_icon = null)
    {
        Tag::setDefault('section_icon', $preselected_section_icon);

        $section_icon_dropdown = Tag::select(array(
            'section_icon',
            Categories::getAvailableIcons(),
            'useEmpty' => true,
            'emptyText' => 'No icon',
            'emptyValue' => '0',
            'class' => 'form-control show-tick'
        ));

        $this->view->setVar('section_icon_dropdown', $section_icon_dropdown);
    }

}
