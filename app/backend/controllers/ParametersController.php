<?php

namespace Baseapp\Backend\Controllers;

use Baseapp\Extension\Tag;
use Baseapp\Library\Tool;
use Baseapp\Library\Utils;
use Baseapp\Models\Dictionaries;
use Baseapp\Models\Parameters;
use Baseapp\Models\ParametersTypes;
use Baseapp\Traits\CrudActions;
use Baseapp\Traits\CrudHelpers;
use Phalcon\Mvc\Model\Resultset;
use Phalcon\Paginator\Adapter\QueryBuilder;

/**
 * Backend Parameters Controller
 */
class ParametersController extends IndexController
{
    use CrudActions;
    use CrudHelpers;

    /* @var \Baseapp\Models\Parameters */
    public $crud_model_class = 'Baseapp\Models\Parameters';

    protected $allowed_roles = array('admin');

    public static function get_types_dropdown_data()
    {
        return ParametersTypes::find(array('order' => 'group_type, id'))->toArray();
    }

    /**
     * Index Action
     */
    public function indexAction()
    {
        $this->tag->setTitle('Parameters');

        $page       = $this->getPageParam();
        $search_for = $this->processSearchParam();
        $mode       = $this->processSearchMode();
        $dir        = $this->processAscDesc('asc');
        $limit      = $this->processLimit();

        // TODO: abstract this sort of checking away into a generic method somewhere and start using everywhere
        // Type filtering
        $default_type = 'ANY';
        $types = self::get_types_dropdown_data();
        $type = $this->request->getQuery('type', 'string', $default_type);
        $type_found = false;
        foreach ($types as &$available_type) {
            if ($available_type['id'] === $type) {
                $type_found = true;
                break;
            }
        }
        if (!$type_found) {
            $type = $default_type;
        }
        $this->view->setVar('available_types', $types);
        $this->view->setVar('type', $type);

        $builder = $this->modelsManager->createBuilder()
            ->columns(array('
                Baseapp\Models\Parameters.id,
                Baseapp\Models\Parameters.name,
                Baseapp\Models\Parameters.type_id,
                Baseapp\Models\Parameters.dictionary_id,
                Baseapp\Models\ParametersTypes.group_type,
                Baseapp\Models\ParametersTypes.name AS type_name,
                Baseapp\Models\ParametersTypes.class AS type_class,
                Baseapp\Models\Dictionaries.name AS dictionary_name
            '))
            ->from('Baseapp\Models\Parameters');

        if ('ANY' !== $type) {
            $builder->innerJoin(
                'Baseapp\Models\ParametersTypes',
                'Baseapp\Models\Parameters.type_id = Baseapp\Models\ParametersTypes.id AND Baseapp\Models\ParametersTypes.id = "' . $type . '"'
            );
        } else {
            $builder->innerJoin(
                'Baseapp\Models\ParametersTypes',
                'Baseapp\Models\Parameters.type_id = Baseapp\Models\ParametersTypes.id'
            );
        }

        $builder->leftJoin('Baseapp\Models\Dictionaries', 'Baseapp\Models\Parameters.dictionary_id = Baseapp\Models\Dictionaries.id');

        $fields_to_search = array(
            'id' => 'Baseapp\Models\Parameters.id',
            'name' => 'Baseapp\Models\Parameters.name'
        );
        $search_params = Utils::build_phalcon_search_sql_params(array_keys($fields_to_search), $search_for, $mode);
        if (null !== $search_params && is_array($search_params)) {
            $conditions = array();
            $binds = array();
            foreach ($search_params as $fld => $val) {
                $field_bind_name = sprintf(':%s:', $fld);
                $conditions[] = $fields_to_search[$fld] . ' LIKE ' . $field_bind_name;
                $binds[$fld] = $val;
                // $builder->orWhere($fld . ' LIKE ' . $field_bind_name, array($fld => $val));
            }
            $conditions = $this->changeSearchParamsLikeIntoEqualsWhenExactMode($conditions, $mode);
            $builder->andWhere(implode(' OR ', $conditions), $binds);
        }

        $builder->orderBy('Baseapp\Models\ParametersTypes.group_type, Baseapp\Models\Parameters.name ' . strtoupper($dir));

        $paginator = new QueryBuilder(array(
            'builder' => $builder,
            'limit'   => $limit,
            'page'    => $page
        ));
        $current_page = $paginator->getPaginate();

        $pagination_links = Tool::paginationAdmin(
            $current_page,
            'admin/parameters',
            'pagination',
            $this->config->settingsBackend->pagination_count_out,
            $this->config->settingsBackend->pagination_count_in
        );

        // FIXME: Not used currently really, but eventually it should be as all the darn
        // backend dropdowns should be generated identically from a single place
        Tag::setDefaults(array(
            'q' => $search_for,
            'mode' => $mode,
            'type' => $type,
            'dir' => $dir,
            'limit' => $limit
        ), true);

        $this->view->setVar('page', $current_page);
        $this->view->setVar('pagination_links', $pagination_links);
    }

    protected function common_view_stuff()
    {
        $this->assets->addJs('assets/backend/js/parameters-edit.js');

        $this->view->setVar('parameters_types', ParametersTypes::find(array('group_type="custom"')));
    }

    /**
     * @param \Baseapp\Models\Parameters $parameter
     */
    public function dictionary_id_dropdown($parameter)
    {
        $container_is_hidden = ((int) $parameter->getParameterType()->accept_dictionary) ? false : true;
        $dict = $parameter->getDictionary();

        $dict_values = null;
        if ($dict) {
            $dict_values = $dict->children();
            $dictionary_id = $dict->id;
            Tag::setDefault('dictionary_id', $dictionary_id);
            $this->view->setVar('dictionary_id', $dictionary_id);
        }

        $this->view->setVar('dictionary_container_is_hidden', $container_is_hidden);
        $this->view->setVar('dictionary_values', $dict_values);

        /**
         * For now we'll be using this simple query for this purpose but there could be some issues; as 'group by'
         * always returns first row that matches it's condition, it could happen that parent->child would switch
         * positions (child->parent) but their id would stay the same, and it could happen that this query would
         * return a child's row instead of parent's... therefore, there is a another version of the query that uses
         * joins but returns the same data that will be commented out in case we will need it..
         */
        // SELECT id, name, (MAX(level) - 1) as 'max_level' FROM `dictionaries` GROUP BY root_id
        $dictionaries = Dictionaries::find(array(
            'columns' => array('id', 'name', '(MAX(level) - 1) as max_level'),
            'group' => 'root_id'
        ));
        /**
         * Alternative query
         * SELECT d.id, d.name, (MAX(d2.level) - 1) as 'max_level'
         *     FROM Dictionaries d
         *     INNER JOIN Dictionaries d2 ON d.id = d2.root_id
         *     WHERE d.lft = 1
         *     GROUP BY d.id
         * $dictionaries = $this->modelsManager->createBuilder()
         *     ->columns(array('Dictionaries.id', 'Dictionaries.name', ('(MAX(Dictionaries2.level) - 1) as max_level')))
         *     ->from(array('Dictionaries' => 'Baseapp\Models\Dictionaries'))
         *     ->leftJoin('Baseapp\Models\Dictionaries', 'Dictionaries.id = Dictionaries2.root_id', 'Dictionaries2')
         *     ->where('Dictionaries.lft = 1')
         *     ->groupBy('Dictionaries.id')
         *     ->getQuery()
         *     ->execute();
         */
        $this->view->setVar('dictionaries', $dictionaries);
    }

    /**
     * Create Action
     */
    public function createAction()
    {
        $this->tag->setTitle('Create parameter');
        // Use the same form/view as editAction
        $this->view->pick('parameters/edit');
        $this->view->setVar('save_text', 'Create');
        $this->view->setVar('form_title_long', 'Add parameter');

        $this->common_view_stuff();

        $parameter = new Parameters();

        if ($this->request->isPost()) {
            $created = $parameter->add_new($this->request);
            if ($created instanceof Parameters) {
                $this->flashSession->success('<strong>Successfully created new parameter!</strong>');
                $save_action = $this->get_save_action();
                $next_url = $this->get_next_url();
                if ('save' == $save_action) {
                    $next_url = 'admin/parameters/edit/'.$parameter->id;
                }
                return $this->redirect_to($next_url);
            } else {
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
                $this->view->setVar('errors', $created);
            }
        }

        $this->dictionary_id_dropdown($parameter);
        $this->view->setVar('parameter', $parameter);
    }

    /**
     * Edit Action
     */
    public function editAction($entity_id = null)
    {
        $this->tag->setTitle('Edit parameter');
        $this->view->setVar('save_text', 'Save');
        $this->view->setVar('form_title_long', 'Edit parameter');

        $this->common_view_stuff();

        if (!$entity_id) {
            $params = $this->router->getParams();
            if (!isset($params[0]) || empty($params[0])) {
                $this->flashSession->error('Missing required ID parameter for edit action');
                return $this->redirect_back();
            }
            $entity_id = (int) $params[0];
        }

        $model = $this->crud_model_class;
        /* @var $entity \Baseapp\Models\Parameters */
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        } elseif ($entity->ParameterType && 'standard' == $entity->ParameterType->group_type) {
            // all 'standard' parameters are protected and vital part of the classifieds site and therefore cannot be
            // edited!
            $this->flashSession->error('This parameter cannot be edited');
            return $this->redirect_back();
        }

        if ($this->request->isPost()) {
            $saved = $entity->save_changes($this->request);
            if ($saved instanceof Parameters) {
                $this->flashSession->success('<strong>Parameter saved successfully!</strong>');
                $next_url = $this->get_next_url();
                $save_action = $this->get_save_action();
                if ('save' === $save_action) {
                    $next_url = 'admin/parameters/edit/'.$entity->id;
                }
                return $this->redirect_to($next_url);
            } else {
                $this->view->setVar('errors', $saved);
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
            }
        }

        $this->dictionary_id_dropdown($entity);
        $this->view->setVar('parameter', $entity);
    }
}
