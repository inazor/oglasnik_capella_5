<?php

namespace Baseapp\Backend\Controllers;

use Phalcon\Di;
use Phalcon\Mvc\Model\Query\Builder;
use Baseapp\Extension\Tag;
use Baseapp\Library\Products\ProductsInterface;
use Baseapp\Library\Products\BackendProductsSelector;
use Baseapp\Models\Categories;
use Baseapp\Traits\CrudActions;

/**
 * Backend ProductsCategories Controller
 */
class ProductsCategoriesController extends IndexController
{

    use CrudActions;

    public $crud_model_class = 'Baseapp\Models\ProductsCategories';

    protected $allowed_roles = array('admin');

    /**
     * Index Action
     */
    public function indexAction()
    {
        $this->tag->setTitle('Manage category products');

        $this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.js');
        $this->assets->addJs('assets/vendor/jquery.nestedSortable.js');
        $this->assets->addJs('assets/backend/js/nested-items-sortable.js');
        $this->assets->addJs('assets/backend/js/categories-index.js');

        $tree = $this->getDI()->get(Categories::MEMCACHED_KEY);
        // TODO: since categories are using a single root tree, everything
        // is wrapped within a master root node, which has id=1 currently,
        // so this grabs those children. But this feels very hackish.
        // Need to try to come up with a better api for this...
        $tree_root_id = 1;
        Tag::setDefault('tree_root_id', $tree_root_id);
        $categories = $tree[$tree_root_id]->children;
        $this->view->setVar('categories', $categories);
        $this->view->setVar('listing_type', 'products');

        $this->view->setVar('available_item_actions', array(
            array(
                'path' => 'admin/products-categories/edit/',
                'name'   => '<span class="fa fa-cog fa-fw"></span>Configure'
            )
        ));
    }

    /**
     * Edit/Assign category products
     */
    public function editAction($category_id = null)
    {
        $category_id = (int) $category_id;
        if (!$category_id) {
            $this->flashSession->error('Missing/invalid $category_id parameter for edit action');
            return $this->redirect_back();
        }

        $category = Categories::findFirst($category_id);
        if (!$category) {
            $this->flashSession->error(sprintf('Category not found [ID: %s]', $category_id));
            return $this->redirect_back();
        }
        if (!$category->TransactionType) {
            $this->flashSession->error(sprintf('Category <strong>%s</strong> has no TransactionType set!', $category->name));
            return $this->redirect_back();
        }

        $this->tag->setTitle('Manage category products');

        $this->view->setVar('category', $category);

        $forms_markup = $this->buildProductsConfigForms($category);
        $this->view->setVar('forms_markup', $forms_markup);

        $this->assets->addJs('assets/backend/js/products-categories-edit.js');
    }

    private function getProductsSelector(Categories $category)
    {
        $selector = new BackendProductsSelector($category);
        return $selector;
    }

    protected function buildProductsConfigForms(Categories $category)
    {
        $markup = array();

        $selector = $this->getProductsSelector($category);
        $groups = $selector->getGrouped();

        foreach ($groups as $group => $products) {
            $markup[$group] = '';
            foreach ($products as &$product) {
                /* @var $product ProductsInterface */
                $markup[$group] .= $product->getConfigForm();
            }
        }

        return $markup;
    }

    protected function savePostedConfig(Categories $category = null, $post_data = array())
    {
        $selector = $this->getProductsSelector($category);
        $groups = $selector->getGrouped();

        $groups_configs = array();
        foreach ($groups as $group => $products) {
            foreach ($products as &$product) {
                /* @var $product ProductsInterface */
                $groups_configs[$group][$product->getFqcn()] = $product->prepPostedConfigData($post_data);
            }
        }

        $settings = $category->getSettings();
        if ($settings) {
            $settings->products_cfg_json = json_encode($groups_configs);
            $saved = $settings->save();

            return $saved;
        }

        return false;
    }

/*
    protected function rebuildCache()
    {
        // TODO: get all categories that have product descriptions and generate an object that will be saved in memcached
        $di = Di::getDefault();
        $db = $di->getShared('db');

        $sqlBuilder = new Builder();
        $sqlBuilder->columns(array('cs.category_id', 'cs.products_cfg_json'));
        $sqlBuilder->addFrom('Baseapp\Models\CategoriesSettings', 'cs');
        $sqlBuilder->innerJoin('Baseapp\Models\Categories', 'c.id = cs.category_id', 'c');
        $sqlBuilder->where('c.active = 1 AND c.transaction_type_id IS NOT NULL');
        $sqlBuilder->orderBy('c.id ASC');

        $parsedBuilder = $sqlBuilder->getQuery()->parse();
        $parsedQuery   = $db->getDialect()->select($parsedBuilder);;
        $result        = $db->query($parsedQuery);
        $result->setFetchMode(\Phalcon\Db::FETCH_OBJ);
        $rows          = $result->fetchAll();

        if ($rows && count($rows)) {
            $categoryProducts = array();

            foreach ($rows as $row) {
                $productsJson = isset($row->products_cfg_json) && $row->products_cfg_json ? json_decode($row->products_cfg_json) : null;
                if ($productsJson) {
                    $categoryProducts[$row->category_id] = $productsJson;
                }
            }
        }

        return null;
    }
*/

    /**
     * Save Action
     */
    public function saveAction($category_id)
    {
        $category = Categories::findFirst($category_id);
        if (!$category) {
            $this->flashSession->error(sprintf('Invalid or non-existent Category specified [ID: %s]', $category_id));
            return $this->redirect_back();
        }

        if (!$category->TransactionType) {
            $this->flashSession->error(sprintf('Category <strong>%s</strong> does not have a TransactionType set!', $category->name));
            return $this->redirect_back();
        }

        if ($this->request->isPost()) {
            $saved = $this->savePostedConfig($category, $this->request->getPost());
            if (!$saved) {
                $this->flashSession->error('Failed saving posted configuration changes!');
            } else {
                $this->flashSession->success('Product configuration changes have been saved.');
            }
        }

        // at the end, redirect the user back to the same page for editing
        // so it looks like nothing happened (except save) :)
        return $this->redirect_to('admin/products-categories/edit/' . $category->id);
    }
}
