<?php

namespace Baseapp\Backend\Controllers;

use Phalcon\Dispatcher;
use Baseapp\Bootstrap;
use Baseapp\Controllers\BaseBackendController;
use Baseapp\Extension\Tag;
use Baseapp\Library\Debug;
use Baseapp\Library\SimpleXMLProcessorRba;
use Baseapp\Library\Tool;
use Baseapp\Library\Utils;
use Baseapp\Models\Orders;
use Baseapp\Models\Users;
use Baseapp\Traits\CrudActions;
use Baseapp\Traits\CrudHelpers;
use Phalcon\Exception;
use Phalcon\Http\Request\FileInterface;
use Phalcon\Paginator\Adapter\QueryBuilder;

class OrdersController extends BaseBackendController
{
    use CrudActions;
    use CrudHelpers;

    protected $session_import_warnings_key = 'orders_bank_xml_upload_import_warnings';

    public $crud_model_class = 'Baseapp\Models\Orders';

    protected $allowed_roles = array('admin', 'finance', 'supersupport');

    protected function getPossibleOrderByOptions()
    {
        $order_bys = array(
            'created_at'   => 'Date created',
            'modified_at'  => 'Date modified',
            'valid_until'  => 'Valid until',
            'id'           => 'Order ID'
        );

        return $order_bys;
    }

    protected function getStylingData()
    {
        $status_names = Orders::getAllStatuses();

        $data = array(
            Orders::STATUS_NEW       => array(
                'title'     => $status_names[Orders::STATUS_NEW],
                'icon'      => '<span class="fa fa-fw"></span>',
                'classname' => 'info'
            ),
            Orders::STATUS_CANCELLED => array(
                'title'     => $status_names[Orders::STATUS_CANCELLED],
                'icon'      => '<span class="fa fa-fw"></span>',
                'classname' => 'warning'
            ),
            Orders::STATUS_EXPIRED   => array(
                'title'     => $status_names[Orders::STATUS_EXPIRED],
                'icon'      => '<span class="fa fa-fw"></span>',
                'classname' => 'danger'
            ),
            Orders::STATUS_COMPLETED => array(
                'title'     => $status_names[Orders::STATUS_COMPLETED],
                'icon'      => '<span class="fa fa-fw"></span>',
                'classname' => 'success'
            )
        );

        return $data;
    }

    /**
     * Index Action
     */
    public function indexAction()
    {
        $this->tag->setTitle('Orders');

        $this->assets->addCss('assets/vendor/jasny-bootstrap-3.1.3-dist/css/jasny-bootstrap.min.css');
        $this->assets->addJs('assets/vendor/jasny-bootstrap-3.1.3-dist/js/jasny-bootstrap.min.js');

        $page       = $this->getPageParam();
        $search_for = $this->processSearchParam();
        $mode       = $this->processSearchMode();
        $dir        = $this->processAscDesc('desc');
        $order_by   = $this->processOrderBy('created_at');
        $limit      = $this->processLimit();

        // If we spot a PBO prefix in there, remove it from the searched string
        // and force the search field to 'id'
        $forced_field = null;
        if (Utils::str_has_pbo_prefix($search_for)) {
            $search_for   = trim(Utils::str_remove_pbo_prefix($search_for));
            $forced_field = 'id';
        }

        Tag::setDefaults(array(
            'q'        => $search_for,
            'mode'     => $mode,
            'dir'      => $dir,
            'order_by' => $order_by,
            'limit'    => $limit
        ), true);

        // Status dropdown
        $any_status = array(0 => 'Any status');
        $statuses   = $any_status + Orders::getAllStatuses();
        $status     = $this->processOptions('status', $statuses, null);

        // Available search fields
        $available_fields = array(
            'id',
            'ad_id',
            'user_id',
            'payment_method',
            'total',
            'cart',
            'created_at',
            'modified_at',
            'valid_until'
        );
        $field = $this->processOptions('field', $available_fields, $forced_field);
        Tag::setDefault('field', $field);
        if (null === $field || empty($field)) {
            $field = 'all';
        }

        // Build the query
        $builder = $this->modelsManager->createBuilder()
            ->columns(array(
                $this->crud_model_class . '.id',
                $this->crud_model_class . '.user_id',
                $this->crud_model_class . '.status',
                $this->crud_model_class . '.total',
                $this->crud_model_class . '.payment_method',
                $this->crud_model_class . '.valid_until',
                $this->crud_model_class . '.created_at',
                $this->crud_model_class . '.modified_at',
                'Baseapp\Models\Users.type',
                'Baseapp\Models\Users.email',
                'Baseapp\Models\Users.username',
                'Baseapp\Models\Users.phone1',
                'Baseapp\Models\Users.phone2',
                'Baseapp\Models\Users.company_name',
                'Baseapp\Models\Users.first_name',
                'Baseapp\Models\Users.last_name',
                'Baseapp\Models\Users.oib',
                'Baseapp\Models\Users.address',
                'Baseapp\Models\Users.city',
                'Baseapp\Models\Users.zip_code',
                'Baseapp\Models\Users.county_id',
                'Baseapp\Models\Users.country_id'
            ))
            ->from($this->crud_model_class);
/*
        $builder->leftJoin(
            'Baseapp\Models\OrdersItems',
            'Baseapp\Models\OrdersItems.order_id = ' . $this->crud_model_class . '.id'
        );
//*/

        // Special case the search for orders matching a specific ad_id
        if ('ad_id' === $field) {
            // This is bugged (causes a duplicate join statement for users table... go Phalcon!)
            // $builder->addFrom('Baseapp\Models\OrdersItems');
            // $builder->andWhere('Baseapp\Models\OrdersItems.ad_id = ' . (int) $search_for);
            $builder->innerJoin(
                'Baseapp\Models\OrdersItems',
                'Baseapp\Models\Orders.id = Baseapp\Models\OrdersItems.order_id AND Baseapp\Models\OrdersItems.ad_id = ' . (int) $search_for
            );
            $builder->groupBy($this->crud_model_class . '.id');
        }

        $builder->leftJoin(
            'Baseapp\Models\Users',
            'Baseapp\Models\Users.id = ' . $this->crud_model_class . '.user_id'
        );

        if ($status > 0) {
            $builder->andWhere($this->crud_model_class . '.status = :status:', array('status' => $status));
        }

        // If a specific field is specified, search only that one, otherwise search across all the fields
        // (but only if there's an actual search term entered)
        $fields_to_search = (null === $field || 'all' === $field) ? $available_fields : array($field);
        $search_params = Utils::build_phalcon_search_sql_params($fields_to_search, $search_for, $mode);
        if (null !== $search_params && is_array($search_params)) {
            $conditions = array();
            $binds = array();
            foreach ($search_params as $fld => $val) {
                // Skipping ad_id field
                if ('ad_id' === $fld) {
                    continue;
                }
                $field_bind_name = sprintf(':%s:', $fld);
                $conditions[] = $this->crud_model_class . '.' . $fld . ' LIKE ' . $field_bind_name;
                $binds[$fld] = $val;
                // $builder->orWhere($fld . ' LIKE ' . $field_bind_name, array($fld => $val));
            }
            if (!empty($conditions)) {
                $conditions = $this->changeSearchParamsLikeIntoEqualsWhenExactMode($conditions, $mode);
                $builder->andWhere(implode(' OR ', $conditions), $binds);
            }
        }

        $builder->orderBy($this->crud_model_class . '.' . $order_by . ' ' . strtoupper($dir));

        $paginator_params = array(
            'builder' => $builder,
            'limit'   => $limit,
            'page'    => $page
        );
        $paginator    = new QueryBuilder($paginator_params);
        $current_page = $paginator->getPaginate();

        // Populates the detail records of each order record about to be shown on this page by only
        // issuing one query with an IN() statement
        $results_array = Orders::buildResultsArray($current_page->items);
        $current_page->items = $results_array;

        // If JSON or CSV (with latest XML Bank statement warnings) are requested,
        // dump the result and return the response early
        if ($this->request->hasQuery('json') || $this->request->hasQuery('import-warnings')) {
            // FIXME/TODO:
            // Again, really just making sure the 'support' user doesn't
            // end up here, because its allowed on the controller level.
            // But when just reading the code, one wonders why this check is here...
            if ($this->auth->logged_in(array('admin', 'finance'))) {
                $this->disable_view();

                if ($this->request->hasQuery('json')) {
                    $results_array_modified = $this->modifySearchResultsForJsonExport($results_array);
                    $this->response->setContentType('application/json', 'UTF-8');
                    $this->response->setJsonContent($results_array_modified);
                } elseif ($this->request->hasQuery('import-warnings')) {
                    if ($this->has_session_import_warnings()) {
                        $this->response->setContentType('text/csv', 'UTF-8');
                        $this->response->setHeader('Content-Disposition', 'attachment; filename="import-warnings.csv"');
                        $this->response->setContent($this->get_session_import_warnings());
                        // clear the session where we store last import's errors/warnings
                        $this->session->set($this->session_import_warnings_key, array());
                    } else {
                        return $this->redirect_to('admin/orders');
                    }
                }

                return $this->response;
            } else {
                $this->flashSession->error($this->insufficient_privileges);
            }
        }

        $pagination_links = Tool::paginationAdmin(
            $current_page,
            'admin/orders',
            'pagination',
            $this->config->settingsBackend->pagination_count_out,
            $this->config->settingsBackend->pagination_count_in
        );

        $this->view->setVar('page', $current_page);
        $this->view->setVar('pagination_links', $pagination_links);
        $this->view->setVar('styling_data', $this->getStylingData());

        $this->view->setVar('json_download_href', $this->buildJsonExportHref());
        $this->view->setVar('import_warnings_download_href', $this->buildImportWarningsHref());
    }

    protected function has_session_import_warnings()
    {
        $has_import_warnings = false;

        $session_import_warnings = array();

        if ($this->session->has($this->session_import_warnings_key)) {
            $session_import_warnings = $this->session->get($this->session_import_warnings_key);
        }

        if (is_array($session_import_warnings) && count($session_import_warnings)) {
            $has_import_warnings = true;
        }

        return $has_import_warnings;
    }

    protected function get_session_import_warnings()
    {
        $content = '';

        if ($this->has_session_import_warnings()) {
            $warnings = $this->session->get($this->session_import_warnings_key);

            $curr_row = 1;
            $content .= '"No",';
            $content .= '"Filename",';
            $content .= '"Order #",';
            $content .= '"PBO",';
            $content .= '"Error/Warning"' . PHP_EOL;
            foreach ($warnings as $warning) {
                foreach ($warning['warnings'] as $row) {
                    $content .= $curr_row . ',';
                    $content .= '"' . $warning['filename'] . '",';
                    $content .= '"' . $row['ID'] . '",';
                    $content .= '"' . $row['PBO'] . '",';
                    $content .= '"' . $row['MSG'] . '"' . PHP_EOL;
                    $curr_row++;
                }
            }
        }

        return $content;
    }

    /**
     * Builds and returns the link/href to be used for exporting latest import warnings from uploaded/parsed
     * XML bank statements.
     *
     * @return string
     */
    protected function buildImportWarningsHref()
    {
        $href = null;

        if ($this->has_session_import_warnings()) {
            $query = $this->request->getQuery();
            unset($query['_url']);
            $query['import-warnings'] = 1;

            $href = $this->url->get('admin/orders', $query);
        }

        return $href;
    }

    /**
     * Builds and returns the link/href to be used for exporting the current page of search results
     * as JSON. It should maintain any existing query parameters and append/modify our `json=1` param
     * (which we're currently using to detect if results should be dumped to JSON or not).
     *
     * @return string
     */
    protected function buildJsonExportHref()
    {
        $query = $this->request->getQuery();
        unset($query['_url']);
        $query['json'] = 1;

        $href = $this->url->get('admin/orders', $query);

        return $href;
    }

    /**
     * Transforms data returned by `Orders::buildResultsArray()` into a slightly
     * different array (specified by Oglasnik, since they want their JSON export fields
     * named differently and certain data in a different format)
     *
     * @param array $results
     *
     * @return array
     */
    protected function modifySearchResultsForJsonExport(array $results)
    {
        $modified_results = array();
        $order_modified   = array();

        foreach ($results as $order) {
            $order_modified['ID'] = $order['id'];

            // Merge user invoice details
            $user_invoice_details            = Users::buildInvoiceDetailsArray($order);
            $order_modified                  = array_merge($order_modified, $user_invoice_details);

            $order_modified['USER_ID']       = $order['user_id'];
            $order_modified['USERNAME']      = $order['username'];

            // Add other stuff they required
            $order_modified['INVOICENUMBER'] = $order['pbo'];
            $order_modified['AMOUNTTOPAY']   = $order['total'];
            $order_modified['AMOUNTTOPAYLP'] = $order['total_lp'];
            $order_modified['INVOICEDATE']   = $order['modified_at'];

            // Adding some stuff I think will come in handy down the line
            $order_modified['STATUS']     = $order['status'];
            $order_modified['STATUSTEXT'] = $order['status_text'];

            if ('offline' === $order['payment_method'] || empty($order['payment_method'])) {
                $order_modified['PAYMENTMETHOD'] = 0;
                $order_modified['CREDITCARDID']  = 'opca_virman';
            } else {
                $order_modified['PAYMENTMETHOD'] = 1;
                $order_modified['CREDITCARDID']  = $order['payment_method'];
            }

            $order_modified['PRODUCTS'] = array();
            foreach ($order['details'] as $k => $product) {
                $price_kn = Utils::lp2kn($product->price);
                $total_kn = Utils::lp2kn($product->total);

                // Simple attempt to get durations as requested. Might suffice.
                $duration = $this->extractDurationNumbersFromDurationPhrases($product->title);
                if (null !== $duration) {
                    $order_modified['PRODUCTS'][$k]['DURATION'] = $duration;
                }

                $order_modified['PRODUCTS'][$k]['PRODUCT_ID']             = $product->product_id;
                $order_modified['PRODUCTS'][$k]['PRODUCTNAME']            = $product->title;
                $order_modified['PRODUCTS'][$k]['PRODUCTQTY']             = $product->qty;
                $order_modified['PRODUCTS'][$k]['PRODUCTPRICE']           = $price_kn;
                $order_modified['PRODUCTS'][$k]['PRODUCTPRICELP']         = $product->price;
                $order_modified['PRODUCTS'][$k]['PRODUCTPRICETOTAL']      = $total_kn;
                $order_modified['PRODUCTS'][$k]['PRODUCTPRICETOTALLP']    = $product->total;
                $order_modified['PRODUCTS'][$k]['PRODUCTDISCOUNT']        = 0;
                $order_modified['PRODUCTS'][$k]['PRODUCTDISCOUNTPRICE']   = $price_kn;
                $order_modified['PRODUCTS'][$k]['PRODUCTDISCOUNTPRICELP'] = $product->price;
            }

            $modified_results[] = $order_modified;
        }

        return $modified_results;
    }

    /**
     * @param $string
     *
     * @return array|int|null
     */
    protected function extractDurationNumbersFromDurationPhrases($string)
    {
        $duration = null;

        // Looking for numbers separated by a space followed by certain known words
        $regex = '/(\d+)\s(dana?|objav[ae]?)/i';

        $matches = array();
        $matched = preg_match_all($regex, $string, $matches);
        if (false !== $matched && $matched > 0) {
            // Only want the numbers (first capturing group)
            if (isset($matches[1]) && !empty($matches[1])) {
                $duration = array_map('intval', $matches[1]);
            }
        }

        if (1 === count($duration)) {
            $duration = (int) $duration[0];
        }

        return $duration;
    }

    /**
     * @param int|null $order_id
     *
     * @return Orders|false
     */
    protected function checkEntity($order_id = null)
    {
        /* @var $entity Orders */
        $entity = Orders::findFirst((int) $order_id);
        if (!$entity) {
            $this->flashSession->error('Invalid Order specified!');
        }

        if ($entity) {
            if (!$entity->isStatusChangeable()) {
                $this->flashSession->error(sprintf('%s (status=%s) cannot be modified any more!', $entity->ident(), $entity->getStatusText()));
                // Resetting back to false so that controller actions can abort/redirect properly
                $entity = false;
            }
        }

        return $entity;
    }

    /**
     * @param $entity Orders
     * @param int $status
     *
     * @return \Phalcon\Http\ResponseInterface
     */
    protected function saveStatus($entity, $status)
    {
        $result = $entity->save(array('status' => $status));
        if ($result) {
            // Do some extra stuff when an order is canceled from the backend
            if ($entity::STATUS_CANCELLED === $status) {
                $entity->handleCancellationOfOrderItems();
            }
            $this->flashSession->success(sprintf('%s status changed to "%s" successfully', $entity->ident(), $entity->getStatusText()));
        } else {
            $errors = $this->errorMessagesTransformer($entity);
            Bootstrap::log($errors);
            $this->flashSession->error(sprintf('Failed saving (%s) status! Error(s): %s', $entity->ident(), $errors));
        }

        return $this->redirect_back();
    }

    protected function errorMessagesTransformer($entity)
    {
        // TODO: extend Phalcon\Validation\Message\Group to encapsulate this via __toString() or something...
        $msgs = array();
        foreach ($entity->getMessages() as $msg) {
            $msgs[] = $msg->getMessage();
        }
        $msg_string = implode(', ', $msgs);

        return $msg_string;
    }

    public function completeAction($order_id = null)
    {
        $entity = $this->checkEntity($order_id);

        if (!$entity) {
            return $this->redirect_back();
        }

        $success = $entity->finalizePurchase();
        if ($success) {
            $this->flashSession->success(sprintf('%s finalized successfully', $entity->ident()));
        } else {
            // FIXME: more detailed error message? Could be expired, or already finalized, or cancelled etc...
            $this->flashSession->error(sprintf('Failed finalizing purchase for %s', $entity->ident()));
        }

        return $this->redirect_back();
    }
/*
    public function cancelAction($order_id = null)
    {
        $entity = $this->checkEntity($order_id);

        if (!$entity) {
            return $this->redirect_back();
        }

        return $this->saveStatus($entity, $entity::STATUS_CANCELLED);
    }
*/
    // Handle file upload
    public function bankXmlUploadAction()
    {
        // Only 'admin' and 'finance' roles should be allowed here
        if (!$this->auth->logged_in(array('admin', 'finance'))) {
            $this->flashSession->error($this->insufficient_privileges);
            return $this->redirect_to('admin/orders');
        }

        if (!$this->request->hasFiles()) {
            $this->flashSession->error('Nothing uploaded!');
            return $this->redirect_to('admin/orders');
        }

        $files = $this->request->getUploadedFiles();

        if (count($files)) {
            // clear the session for last uploaded set of files
            $this->session->set($this->session_import_warnings_key, array());

            foreach ($files as $file) {
                $this->processBankXML($file);
            }
        }

        return $this->redirect_to('admin/orders');
    }

    /**
     * Process bank's payments xml file
     *
     * @param FileInterface|string $file Filepath (as a string) or a \Phalcon\Http\Request\FileInterface
     *
     * @throws \Exception
     */
    protected function processBankXML($file)
    {
        if ($file instanceof FileInterface) {
            $filepath = $file->getTempName();
            $filename = $file->getName();
        } else {
            $filepath = $file;
            $filename = basename($filepath);
        }

        // Fix crap 4 bytes in the beggining of file
        $filepath = $this->fixInvalidXmlFile($filepath);

        $processor = new SimpleXMLProcessorRba();

        // Avoiding errors/warnings from libxml since the RBA xml DTD is missing
        if (!$processor->open($filepath, LIBXML_NOERROR | LIBXML_NOWARNING)) {
            throw new \RuntimeException('Unable to open XML file');
        }

        // Trying to fully/properly validate the XML fails since the referenced DTD is missing:
        // `xmlns="http://www.etna.hr/schemas/Izvod"` -> 404
        /*
        $processor->setParserProperty(\XMLReader::VALIDATE, true);
        if (!$processor->isValid()) {
            $this->flashSession->error('Failed to validate XML');
        }
        */

        $processor->parse();
        $processor->close();

        if ($processor->called()) {

            $audit_log_prefix = 'Bank XML upload (' . $filename . '): ';
            $this->flashSession->success('XML (' . $filename . ') successfully parsed.');

            $processor->processFoundPbos();

            $stats = $processor->getStats();
            if ($stats['pbo_matches'] > 0) {
                $message_matches = sprintf('Found %d payment record(s) matching our PBO prefix', $stats['pbo_matches']);
                if ($stats['pbo_duplicates'] > 0) {
                    $message_matches .= sprintf(' (%d duplicate(s))', $stats['pbo_duplicates']);
                }
                $this->flashSession->notice($message_matches);
                $this->saveAudit($audit_log_prefix . $message_matches);
            } else {
                $message_no_matches = 'No payments matching our PBO prefix were found';
                $this->flashSession->notice($message_no_matches);
                $this->saveAudit($audit_log_prefix . $message_no_matches);
            }
            if ($stats['completed'] > 0) {
                $message_completed = sprintf('Finalized %d orders', $stats['completed']);
                $this->flashSession->notice($message_completed);
                $this->saveAudit($audit_log_prefix . $message_completed);

                // Save info about finalized/completed orders in the audit log
                $completed_messages = $processor->getCompletedMessages();
                if (!empty($completed_messages)) {
                    foreach ($completed_messages as $message) {
                        $this->saveAudit($audit_log_prefix . $message);
                    }
                }
            }

            // Show any warnings that might have have occurred
            $warnings = $processor->getWarnings();
            if (!empty($warnings)) {
                foreach ($warnings as $warning) {
                    $this->flashSession->warning($warning);
                    $this->saveAudit($audit_log_prefix . $warning);
                }

                // get warnings formatted for export also
                $export_warnings = $processor->getWarnings($for_export = true);
                if (!empty($export_warnings)) {
                    $session_order_export_warnings = $this->session->get($this->session_import_warnings_key);
                    $session_order_export_warnings[] = array(
                        'filename' => $filename,
                        'warnings' => $export_warnings,
                    );
                    $this->session->set($this->session_import_warnings_key, $session_order_export_warnings);
                }
            }
        } else {
            $this->flashSession->error('XML parser callbacks have not been called! Is the XML file structure ok?');
        }
    }

    /**
     * Overwrites the original file specified in $filepath with a
     * new one that has certain specific first 8 bytes of crap removed (if those
     * first 8 bytes are that crap: efbf bdef bfbd 0005)
     *
     * @return string
     */
    protected function fixInvalidXmlFile($filepath)
    {
        $num_bytes = 8;
        $suspect_bytes = pack('C*', 0xef, 0xbf, 0xbd, 0xef, 0xbf, 0xbd, 0x00, 0x05);

        $file = fopen($filepath, 'r');
        $first_bytes = fread($file, $num_bytes);

        $has_suspect_bytes = ($first_bytes === $suspect_bytes);
        if ($has_suspect_bytes) {
            $contents = file_get_contents($filepath);
            file_put_contents($filepath, substr($contents, $num_bytes));
            clearstatcache();
        }

        return $filepath;
    }

    // Overriding deleteAction in order to completely prevent deletion
    public function deleteAction($entity_id = null)
    {
        $this->flashSession->error('Orders cannot be deleted!');
        return $this->redirect_back();
    }
}
