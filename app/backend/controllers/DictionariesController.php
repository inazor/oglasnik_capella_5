<?php

namespace Baseapp\Backend\Controllers;

use Baseapp\Extension\Tag;
use Baseapp\Library\Tool;
use Baseapp\Library\Utils;
use Baseapp\Models\Dictionaries;
use Baseapp\Traits\CrudActions;
use Baseapp\Traits\CrudHelpers;

/**
 * Backend Dictionaries Controller
 */
class DictionariesController extends IndexController
{
    use CrudActions;
    use CrudHelpers;

    public $crud_model_class = 'Baseapp\Models\Dictionaries';

    protected $allowed_roles = array('admin');

    /**
     * Index Action
     */
    public function indexAction()
    {
        $this->tag->setTitle('Dictionaries');

        $page       = $this->getPageParam();
        $search_for = $this->processSearchParam();
        $mode       = $this->processSearchMode();
        $dir        = $this->processAscDesc('asc');
        $limit      = $this->processLimit();

        // Check if search terms present and build appropriate stuff
        $condition = null;
        $conditions = array();
        $binds = array();
        $search_params = Utils::build_phalcon_search_sql_params(array('name'), $search_for, $mode);
        if (null !== $search_params && is_array($search_params)) {
            foreach ($search_params as $fld => $val) {
                $field_bind_name = sprintf(':%s:', $fld);
                $conditions[] = $fld . ' LIKE ' . $field_bind_name;
                $binds[$fld] = $val;
            }
        }

        // Setup order by for the roots() call
        $extras = array('order' => sprintf('name %s', strtoupper($dir)));

        $dictionaries = (new Dictionaries())->roots(implode(' AND ', $conditions), $binds, $extras);
        $paginator = new \Phalcon\Paginator\Adapter\Model(array(
            'data'  => $dictionaries,
            'limit' => $limit,
            'page'  => $page
        ));
        $current_page = $paginator->getPaginate();

        $pagination_links = Tool::paginationAdmin(
            $current_page,
            'admin/dictionaries',
            'pagination',
            $this->config->settingsBackend->pagination_count_out,
            $this->config->settingsBackend->pagination_count_in
        );

        $this->view->setVar('page', $current_page);
        $this->view->setVar('pagination_links', $pagination_links);
    }

    /**
     * Create new dictionary
     *
     * @return \Phalcon\Http\ResponseInterface
     */
    public function createAction()
    {
        $this->tag->setTitle('Create new dictionary');

        $this->view->setVar('form_title_long', 'Create new dictionary');
        $this->view->setVar('save_text', 'Create');

        $this->add_common_crud_assets();

        // Use the same form/view as editAction
        $this->view->pick('dictionaries/edit');

        $entity = new Dictionaries();
        if ($this->request->isPost()) {
            $created = $entity->add_new($this->request);
            if ($created instanceof Dictionaries) {
                $this->flashSession->success('<strong>Successfully created new dictionary!</strong>');
                // Determine which save button was used
                $save_action = $this->get_save_action();
                // Override next url to go to entity edit in save button case
                $next_url = $this->get_next_url();
                if ('save' === $save_action) {
                    $next_url = 'admin/dictionaries/edit/' . $created->id;
                }
                return $this->redirect_to($next_url);
            } else {
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
                $this->view->setVar('errors', $created);
            }
        } else {
            // default for any new dictionary
            $entity->active = 1;
        }

        $this->view->setVar('dictionary', $entity);
        $this->parent_id_dropdown();
    }

    /**
     * Edit existing dictionary
     */
    public function editAction($entity_id)
    {
        $this->tag->setTitle('Edit existing dictionary');
        $this->view->setVar('form_title_long', 'Edit dictionary');
        $this->view->setVar('save_text', 'Save');
        $this->add_common_crud_assets();

        $entity_id = (int) $entity_id;
        if (!$entity_id) {
            $this->flashSession->error('Missing required ID parameter for edit action!');
            return $this->redirect_back();
        }

        /* @var $model \Baseapp\Models\Dictionaries */
        /* @var $entity \Baseapp\Models\Dictionaries */
        $model = $this->crud_model_class;
        $entity = $model::findFirst(array(
            'conditions' => 'id = :id: AND level = 1',
            'bind'       => array('id' => $entity_id)
        ));

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }

        if ($this->request->isPost()) {
            $saved = $entity->save_changes($this->request);
            if ($saved instanceof Dictionaries) {
                $this->flashSession->success('<strong>Dictionary saved successfully!</strong>');
                // Determine which save button was used
                $save_action = $this->get_save_action();
                if ('save' === $save_action) {
                    return $this->redirect_self();
                } else {
                    return $this->redirect_to($this->get_next_url());
                }
            } else {
                $this->view->setVar('errors', $saved);
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
            }
        }

        $this->view->setVar('dictionary', $entity);
        $this->parent_id_dropdown($entity);
    }


    public function manageAction($dictionary_id)
    {
        /* @var $dictionary \Baseapp\Models\Dictionaries */
        /* @var $model \Baseapp\Models\Dictionaries */
        $model = $this->crud_model_class;
        $dictionary = $model::findFirst(array(
            'conditions' => 'id = :id: AND level = 1',
            'bind'       => array('id' => $dictionary_id)
        ));

        if (!$dictionary) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $model, $dictionary_id));
            return $this->redirect_back();
        }

        $this->tag->setTitle('Manage dictionary values: ' . $dictionary->name);
        $this->view->setVar('form_title_long', 'Manage dictionary values');

        // $this->assets->addCss('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.css');
        // $this->assets->addCss('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.structure.min.css');
        $this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.js');
        $this->assets->addJs('assets/vendor/jquery.nestedSortable.js');
        $this->assets->addJs('assets/backend/js/nested-items-sortable.js');
        $this->assets->addJs('assets/backend/js/dictionaries-manage.js');

        Tag::setDefault('tree_root_id', $dictionary_id);
        $this->view->setVar('dictionary', $dictionary);

        $tree = $dictionary->getTree();
        $this->view->setVar('items', $tree[$dictionary_id]->children);

        $this->view->setVar('available_item_actions', array(
            array(
                'path' => 'admin/dictionaries/editvalue/',
                'name'   => '<i class="fa fa-edit fa-fw"></i>Edit'
            ),
            array(
                'path' => 'admin/dictionaries/createvalue/',
                'name'   => '<i class="fa fa-code-fork fa-fw"></i>New sub'
            ),
            array(
                'path' => 'admin/dictionaries/delete/',
                'name'   => '<i class="fa fa-trash-o text-danger fa-fw"></i>'
            )
        ));
    }

    /**
     * Save dictionary values order
     */
    public function saveOrderAction($dictionary_id)
    {
        if ($this->request->isPost()) {

            $model = new Dictionaries();
            $root = $model->getRoot($dictionary_id);
            $tree = $root->getTree();

            if (!$tree) {
                $this->flashSession->error('Dictionary tree not found');
                $this->redirect_back();
            }

            if (!$root) {
                $this->flashSession->error('Tree root not found');
                return $this->redirect_back();
            }

            $items = $this->request->getPost('dictionaries_order');
            $items = json_decode($items, true);

            if ($root->updateTreeOrder($items, $tree)) {
                $root->refreshMemcachedDictionaryTree();
                $this->flashSession->success('<strong>Dictionary values reordered successfully!</strong>');
            } else {
                $this->flashSession->error('<strong>Error!</strong> Dictionary values were not reordered successfully!');
            }
            return $this->redirect_to('admin/dictionaries/manage/' . $dictionary_id);
        } else {
            $this->flashSession->error('<strong>Error!</strong> Method not allowed!');
            return $this->redirect_back();
        }
    }

    /**
     * Create new dictionary value
     *
     * @param $parent_id
     *
     * @return \Phalcon\Http\ResponseInterface|void
     */
    public function createValueAction($parent_id)
    {
        if ($this->request->isPost() && $this->request->hasPost('parent_id')) {
            $parent_id = (int) $this->request->getPost('parent_id');
        } else {
            $parent_id = (int) $parent_id;
        }

        if (!$parent_id) {
            $this->flashSession->error('Missing required $parent_id parameter for createValue action!');
            return $this->redirect_back();
        }

        /* @var $model \Baseapp\Models\Dictionaries */
        /* @var $parent \Baseapp\Models\Dictionaries */

        $model = $this->crud_model_class;
        $parent = $model::findFirst($parent_id);

        if (!$parent) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $model, $parent_id));
            return $this->redirect_back();
        }

        /* @var $dictionary \Baseapp\Models\Dictionaries */
        $dictionary = ($parent->isRoot() ? $parent : $parent->root());

        $this->tag->setTitle('Manage dictionary values: ' . $dictionary->name);

        $this->view->setVar('form_title_long', 'Add new value');
        $this->view->setVar('save_text', 'Create');

        $this->add_common_crud_assets();

        // Use the same form/view as editAction
        $this->view->pick('dictionaries/editvalue');

        $entity = new Dictionaries();
        if ($this->request->isPost()) {
            // Determine which save button was used
            $save_action = $this->get_save_action();

            $created = $entity->add_new($this->request, $parent);
            if ($created instanceof Dictionaries) {
                $this->flashSession->success('<strong>Successfully added new value to dictionary!</strong>');
                // Override next url to go to entity edit in save button case
                if ('save' === $save_action) {
                    $next_url = 'admin/dictionaries/editvalue/' . $created->id;
                } else {
                    $next_url = 'admin/dictionaries/manage/' . $dictionary->id;
                }
                return $this->redirect_to($next_url);
            } else {
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
                $this->view->setVar('errors', $created);
            }
        } else {
            // default for any new dictionary
            $entity->active = 1;
        }

        $this->view->setVar('dictionary', $dictionary);
        $this->view->setVar('dictionary_value', $entity);
        $this->parent_id_dropdown($dictionary, $parent_id);
    }

    /**
     * Edit existing dictionary value
     *
     * @param $entity_id
     *
     * @return \Phalcon\Http\ResponseInterface|void
     */
    public function editValueAction($entity_id)
    {
        $entity_id = (int) $entity_id;
        if (!$entity_id) {
            $this->flashSession->error('Missing required ID parameter for editValue action!');
            return $this->redirect_back();
        }

        /* @var $entity \Baseapp\Models\Dictionaries */
        /* @var $model \Baseapp\Models\Dictionaries */
        $model = $this->crud_model_class;
        $entity = $model::findFirst(array(
            'conditions' => 'id = :id: AND level > 1',
            'bind'       => array('id' => $entity_id)
        ));

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $model, $entity_id));
            return $this->redirect_back();
        }

        /* @var $dictionary \Baseapp\Models\Dictionaries */
        $dictionary = ($entity->isRoot() ? $entity : $entity->root());

        $this->tag->setTitle('Manage dictionary values: ' . $dictionary->name);

        $this->view->setVar('form_title_long', 'Edit existing value');
        $this->view->setVar('save_text', 'Save');

        $this->add_common_crud_assets();

        if ($this->request->isPost()) {
            $parent = null;
            if ($parent_id = (int) $this->request->getPost('parent_id')) {
                $parent = $model::findFirst($parent_id);
                $saved = $entity->save_changes($this->request, $parent);
                if ($saved instanceof Dictionaries) {
                    $this->flashSession->success('<strong>Dictionary value saved successfully!</strong>');
                    // Determine which save button was used
                    $save_action = $this->get_save_action();
                    if ('save' === $save_action) {
                        return $this->redirect_self();
                    } else {
                        return $this->redirect_to('admin/dictionaries/manage/' . $dictionary->id);
                    }
                } else {
                    $this->view->setVar('errors', $saved);
                    $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
                }
            } else {
                $this->flashSession->error('<strong>Error!</strong> Unexpected error [ParentID unknown]');
            }
        }

        $this->view->setVar('dictionary', $dictionary);
        $this->view->setVar('dictionary_value', $entity);

        $this->parent_id_dropdown($dictionary, $entity->parent()->id, $entity->getChildrenIDs(true));
    }

    /**
     * Helper method to generate parent_id dropdown
     *
     * @param \Baseapp\Models\Dictionaries  $dictionary
     * @param integer                       $preselected_dictionary_value Value to preselect
     * @param array                         Array of values to disable in dropdown
     */
    public function parent_id_dropdown($dictionary = null, $preselected_dictionary_value = null, $disabled = array())
    {
        if ($dictionary) {
            if ($preselected_dictionary_value) {
                Tag::setDefault('parent_id', $preselected_dictionary_value);
                $parent_id_dropdown = Tag::select(array(
                    'parent_id',
                    'disabled' => $disabled,
                    $dictionary->getSelectValues(),
                    'class' => 'form-control show-tick'
                ));
            } else {
                if ($dictionary->id == $dictionary->root_id) {
                    $parent_id_dropdown = Tag::select(array(
                        'parent_id',
                        array(),
                        'useEmpty' => true,
                        'emptyText' => $dictionary->name,
                        'emptyValue' => $dictionary->root_id,
                        'class' => 'form-control show-tick'
                    ));
                } else {
                    Tag::setDefault('parent_id', $dictionary->root_id);
                    $parent_id_dropdown = Tag::select(array(
                        'root_id',
                        Dictionaries::find('root_id IS NULL'),
                        'using' => array('parent_id', 'name'),
                        'class' => 'form-control show-tick'
                    ));
                }
            }
        } else {
            $parent_id_dropdown = Tag::select(array(
                'parent_id',
                array(),
                'useEmpty' => true,
                'emptyText' => 'New dictionary',
                'emptyValue' => '0',
                'class' => 'form-control show-tick'
            ));
        }

        $this->view->setVar('parent_id_dropdown', $parent_id_dropdown);
    }

    /**
     * Helper method for common crud assets inclusion
     */
    public function add_common_crud_assets()
    {
        $this->assets->addJs('assets/backend/js/dictionaries-edit.js');
    }
}
