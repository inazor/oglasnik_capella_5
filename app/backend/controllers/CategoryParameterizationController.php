<?php

namespace Baseapp\Backend\Controllers;

use Phalcon\Dispatcher;
use Baseapp\Extension\Tag;
use Baseapp\Models\Categories;
use Baseapp\Models\CategoriesFieldsets;
use Baseapp\Models\CategoriesFieldsetsParameters as FieldsetParameter;
use Baseapp\Models\CategoriesSettings as CategorySettings;
use Baseapp\Models\Currency;
use Baseapp\Models\Locations;
use Baseapp\Models\Parameters;

/**
 * Backend Sections Controller
 */
class CategoryParameterizationController extends IndexController
{
    protected $allowed_roles = array('admin');

    /**
     * Override default baseBackendController's beforeExecuteRoute so we can allow
     * specific action (compareAction) to be executed by roles that normally don't
     * have access to this controller
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        if ('compare' === $dispatcher->getActionName()) {
            $this->allowed_roles = array('admin', 'supersupport', 'support', 'moderator', 'sales');
        }

        parent::beforeExecuteRoute($dispatcher);
    }

    /**
     * Index Action
     */
    public function indexAction()
    {
        $this->tag->setTitle('Category parametrization');

        $this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.js');
        $this->assets->addJs('assets/vendor/jquery.nestedSortable.js');
        $this->assets->addJs('assets/backend/js/nested-items-sortable.js');
        $this->assets->addJs('assets/backend/js/categories-index.js');

        $tree = $this->getDI()->get(Categories::MEMCACHED_KEY);
        // TODO: since categories are using a single root tree, everything
        // is wrapped within a master root node, which has id=1 currently,
        // so this grabs those children. But this feels very hackish.
        // Need to try to come up with a better api for this...
        $tree_root_id = 1;
        Tag::setDefault('tree_root_id', $tree_root_id);
        $categories = $tree[$tree_root_id]->children;
        $this->view->setVar('categories', $categories);
        $this->view->setVar('listing_type', 'parametrize');

        $this->view->setVar('available_item_actions', array(
            array(
                'path' => 'admin/category-parameterization/edit/',
                'name'   => '<span class="fa fa-cog fa-fw"></span>Parametrize'
            )
        ));
    }

    /**
     * Edit existing section
     */
    public function editAction($category_id)
    {
        $this->tag->setTitle('Edit category parameters');

        $this->assets->addCss('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.css');
        $this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.js');

        $this->assets->addCss('assets/backend/css/category-parameterization.css');

        $this->assets->addCss('assets/vendor/datepicker3.css');
        $this->assets->addJs('assets/vendor/bootstrap-datepicker.js');

        $this->assets->addCss('assets/vendor/jquery-mentions-master/jquery.mentions.css');
        $this->assets->addJs('assets/vendor/jquery-mentions-master/jquery.mentions.js');

        $this->assets->addJs('assets/backend/js/category-parameterization-edit-adDescTpl.js');
        $this->assets->addJs('assets/backend/js/category-parameterization-edit-offlineDescTpl.js');
        $this->assets->addJs('assets/backend/js/list-sortable.js');
        $this->assets->addJs('assets/backend/js/category-parameterization-edit.js');

        $this->view->setVar('form_title_long', 'Edit category parameters');

        $category_id = (int) $category_id;

        if (!$category_id) {
            $this->flashSession->error('Missing required Category ID parameter for edit action');
            return $this->redirect_back();
        }

        $category = Categories::findFirst($category_id);

        if (!$category) {
            $this->flashSession->error(sprintf('Category not found [ID: %s]', $category_id));
            return $this->redirect_back();
        }

        if (!$category->TransactionType) {
            $this->flashSession->error(sprintf('Category <strong>%s</strong> cannot be parameterized as it has no TransactionType set!', $category->name));
            return $this->redirect_back();
        }

        // load all currently available parameters in a JSON field so we can include them in view
        $standard_parameters = $this->modelsManager->createBuilder()
            ->columns(array(
                'Baseapp\Models\Parameters.id',
                'Baseapp\Models\Parameters.name',
                'Baseapp\Models\Parameters.slug',
                'Baseapp\Models\Parameters.type_id',
                'Baseapp\Models\ParametersTypes.class',
                'Baseapp\Models\ParametersTypes.can_default',
                'Baseapp\Models\ParametersTypes.can_default_type',
                'Baseapp\Models\ParametersTypes.can_other',
                'Baseapp\Models\ParametersTypes.can_prefix_suffix',
                'Baseapp\Models\ParametersTypes.is_dependable',
                'Baseapp\Models\ParametersTypes.is_required',
                'Baseapp\Models\ParametersTypes.is_searchable',
                'Baseapp\Models\ParametersTypes.is_searchable_type'
            ))
            ->from('Baseapp\Models\Parameters')
            ->innerJoin('Baseapp\Models\ParametersTypes')
            ->where('Baseapp\Models\ParametersTypes.group_type = "standard"')
            ->groupBy(array('Baseapp\Models\Parameters.id'))
            ->getQuery()
            ->execute();

        $custom_parameters = $this->modelsManager->createBuilder()
            ->columns(array(
                'Baseapp\Models\Parameters.id',
                'Baseapp\Models\Parameters.name',
                'Baseapp\Models\Parameters.slug',
                'Baseapp\Models\Parameters.type_id',
                'Baseapp\Models\ParametersTypes.accept_dictionary',
                'Baseapp\Models\Parameters.dictionary_id',
                'Baseapp\Models\ParametersTypes.class',
                'Baseapp\Models\ParametersTypes.can_default',
                'Baseapp\Models\ParametersTypes.can_default_type',
                'Baseapp\Models\ParametersTypes.can_other',
                'Baseapp\Models\ParametersTypes.can_prefix_suffix',
                'Baseapp\Models\ParametersTypes.is_dependable',
                'Baseapp\Models\ParametersTypes.is_searchable_type',
                'MAX(Baseapp\Models\Dictionaries.level) as max_level'
            ))
            ->from('Baseapp\Models\Parameters')
            ->innerJoin('Baseapp\Models\ParametersTypes')
            ->leftJoin('Baseapp\Models\Dictionaries', 'Baseapp\Models\Parameters.dictionary_id = Baseapp\Models\Dictionaries.root_id')
            ->where('Baseapp\Models\ParametersTypes.group_type = "custom"')
            ->groupBy(array('Baseapp\Models\Parameters.id'))
            ->getQuery()
            ->execute();

        $category_path = array();
        if ($category_ancestors = $category->ancestors(2)) {
            foreach ($category_ancestors as $category_ancestor) {
                $category_path[] = $category_ancestor->name;
            }
        }
        $category_path[] = $category->name;
        $category_path = implode(' › ', $category_path);

        // move these filter type somewhere else?
        $filter_types = array(
            array('id' => 'TEXT', 'name' => 'Text'),
            array('id' => 'CHECKBOX', 'name' => 'Checkbox'),
            array('id' => 'RADIO', 'name' => 'Radio'),
            array('id' => 'DATE', 'name' => 'Date'),
            array('id' => 'DROPDOWN', 'name' => 'Dropdown'),
            array('id' => 'DROPDOWN_MULTIPLE', 'name' => 'Dropdown (multiple)'),
            array('id' => 'DEPENDABLE_DROPDOWN', 'name' => 'Dependable dropdown'),
            array('id' => 'DEPENDABLE_DROPDOWN_MULTIPLE', 'name' => 'Dependable dropdown multiple'),
            array('id' => 'INTERVAL_TEXT', 'name' => 'Interval text'),
            array('id' => 'INTERVAL_DROPDOWN', 'name' => 'Interval dropdown'),
            array('id' => 'INTERVAL_SLIDER', 'name' => 'Interval slider'),
            array('id' => 'INTERVAL_DATE', 'name' => 'Interval date')
        );

        $this->tag->setTitle('Edit category parameters [' . $category_path . ']');

        $this->view->setVar('filter_types', $filter_types);
        $this->view->setVar('locations_countries', Locations::findCountries());
        $this->choosen_categories_id_dropdown($category_id, array($category_id));
        $currencies = Currency::getIDArrayOnly();
        $this->view->setVar('currencies', $currencies);
        $this->view->setVar('default_currency', $currencies[$this->config->payment->default_currency_id]);
        $this->view->setVar('standard_parameters', $standard_parameters);
        $this->view->setVar('custom_parameters', $custom_parameters);
        $this->view->setVar('category_path', $category_path);
        $this->view->setVar('category', $category);
    }

    /**
     * Save Action
     */
    public function saveAction($original_category_id)
    {
        if ($this->request->isPost()) {
            // check category ids we need to parametrize
            $category_ids = trim($this->request->getPost('chosen_categories'));
            if ($category_ids) {
                $category_ids = explode(',', $category_ids);
            }

            // get JSON encoded string
            $json_post = null;
            if ($this->request->hasPost('category_fieldsets_and_parameters') && '' != trim($this->request->getPost('category_fieldsets_and_parameters'))) {
                $json_post = json_decode(trim($this->request->getPost('category_fieldsets_and_parameters')));
            }

            foreach ($category_ids as $category_id) {
                // first, find category we will work on...
                $category_id = (int) $category_id;

                if (!$category_id) {
                    $this->flashSession->error('Missing/invalid Category ID parameter for save action');
                    return $this->redirect_back();
                }

                $category = Categories::findFirst($category_id);

                if (!$category) {
                    $this->flashSession->error(sprintf('Category not found [ID: %s]', $category_id));
                    return $this->redirect_back();
                }

                if (!$category->TransactionType) {
                    $this->flashSession->error(sprintf('Category <strong>%s</strong> cannot be parameterized as it has no TransactionType set!', $category->name));
                    return $this->redirect_back();
                }

                $current_filter_sort_order = 1;

                $settings = isset($json_post->settings) ? $json_post->settings : null;

                if ($settings) {
                    $ad_desc_tpl = isset($settings->ad_desc_tpl) ? $settings->ad_desc_tpl : null;
                    $offline_desc_tpl = isset($settings->offline_desc_tpl) ? $settings->offline_desc_tpl : null;

                    $category_settings = CategorySettings::findFirst(array(
                        'category_id = :category_id:',
                        'bind' => array(
                            'category_id' => $category_id
                        )
                    ));
                    if ($category_settings) {
                        $category_settings->ad_desc_tpl = $ad_desc_tpl;
                        $category_settings->offline_desc_tpl = $offline_desc_tpl;
                        $category_settings->save();
                    }
                }

                $filters = isset($json_post->filters) ? $json_post->filters : null;

                if (null !== $json_post && isset($json_post->fieldsets) && count($json_post->fieldsets) > 0) {
                    // TODO: !!! THIS ENTIRE ACTION SHOULD BE IN A SINGLE TRANSACTION IN CASE SOMETHING GOES WRONG !!!

                    // find and delete all connected fieldsets and parameters
                    $category_fieldsets = $category->Fieldsets;
                    if ($category_fieldsets) {
                        foreach ($category_fieldsets as $category_fieldset) {
                            $category_fieldset->getParameters()->delete();
                        }
                        $category_fieldsets->delete();
                    }

                    // now we have to create new fieldsets in which we will place parameters sent via POST
                    $curr_fieldset_sort_order = 0;
                    foreach ($json_post->fieldsets as $json_fieldset) {
                        // first thing to check is if this fieldset has parameters... it should as this is already done
                        // on client side with javascript... in case it doesn't, something is wrong...
                        if (isset($json_fieldset->parameters) && count($json_fieldset->parameters) > 0) {

                            $new_fieldset              = new CategoriesFieldsets();
                            $new_fieldset->category_id = $category_id;
                            if (isset($json_fieldset->label) && trim($json_fieldset->label)) {
                                $new_fieldset->name = trim($json_fieldset->label);
                            }
                            $new_fieldset->sort_order = ++$curr_fieldset_sort_order;
                            if (isset($json_fieldset->settings)) {
                                $new_fieldset->settings = json_encode($json_fieldset->settings);
                            }

                            $fieldset_parameters = array();

                            $curr_parameter_sort_order = 0;
                            foreach ($json_fieldset->parameters as $parameter_slug => $json_parameter) {
                                $parameter_price             = null;
                                $parameter_price_currency_id = null;
                                if ($json_parameter->price) {
                                    if (floatval($json_parameter->price) > 0) {
                                        $parameter_price = floatval($json_parameter->price);
                                    } elseif (intval($json_parameter->price) > 0) {
                                        $parameter_price = intval($json_parameter->price);
                                    }

                                    if ($parameter_price && isset($json_parameter->price_currency_id)) {
                                        $parameter_price_currency_id = (int) $json_parameter->price_currency_id;
                                    } else {
                                        $parameter_price_currency_id = 1;
                                    }
                                }

                                $parameter_sort_order = null;
                                $parameter_sort_order_more = 0;
                                if ($filters) {
                                    $tmp_sort_order_index = array_search((int) $json_parameter->id, $filters->filters, true);
                                    if (false !== $tmp_sort_order_index) {
                                        $parameter_sort_order = (int) $tmp_sort_order_index + 1;
                                        if ((int) $json_parameter->id === (int) $filters->more) {
                                            $parameter_sort_order_more = 1;
                                        }
                                    }
                                }

                                $new_parameter                               = new FieldsetParameter();
                                $new_parameter->category_id                  = (int) $category_id;
                                $new_parameter->parameter_id                 = (int) $json_parameter->id;
                                $new_parameter->parameter_slug               = (string) trim($parameter_slug);
                                $new_parameter->price                        = $parameter_price;
                                $new_parameter->price_currency_id            = $parameter_price_currency_id;
                                $new_parameter->sort_order                   = ++$curr_parameter_sort_order;
                                $new_parameter->filters_sort_order           = $parameter_sort_order;
                                $new_parameter->filters_sort_order_show_more = $parameter_sort_order_more;

                                $dbParameter = Parameters::getCached($new_parameter->parameter_id);

                                if (isset($json_parameter->levels)) {
                                    $new_parameter->levels = json_encode($json_parameter->levels);
                                } elseif ('PICTURE' === $dbParameter->type_id) {
                                    $is_searchable_options = null;
                                    if (isset($json_parameter->is_searchable_options)) {
                                        $is_searchable_options = array(
                                            'label_text' => isset($json_parameter->is_searchable_options->label_text) && trim($json_parameter->is_searchable_options->label_text) ? trim($json_parameter->is_searchable_options->label_text) : '',
                                            'type'       => 'CHECKBOX'
                                        );
                                    }

                                    $data = array(
                                        'label_text'            => (string) $json_parameter->label_text,
                                        'button_text'           => null,
                                        'max_pictures'          => null,
                                        'help_text'             => null,
                                        'is_searchable'         => (isset($json_parameter->is_searchable) && intval($json_parameter->is_searchable) ? 1 : 0),
                                        'is_searchable_options' => $is_searchable_options,
                                        'is_required'           => (isset($json_parameter->is_required) && intval($json_parameter->is_required) ? 1 : 0)
                                    );

                                    if (isset($json_parameter->button_text) && trim($json_parameter->button_text)) {
                                        $data['button_text'] = trim($json_parameter->button_text);
                                    }
                                    if (isset($json_parameter->max_pictures) && trim($json_parameter->max_pictures)) {
                                        $data['max_pictures'] = intval($json_parameter->max_pictures);
                                    }
                                    if (isset($json_parameter->help_text) && trim($json_parameter->help_text)) {
                                        $data['help_text'] = trim($json_parameter->help_text);
                                    }

                                    $new_parameter->data = json_encode($data);
                                } elseif ('UPLOADABLE' === $dbParameter->type_id) {
                                    $is_searchable_options = null;
                                    if (isset($json_parameter->is_searchable_options)) {
                                        $is_searchable_options = array(
                                            'label_text' => isset($json_parameter->is_searchable_options->label_text) && trim($json_parameter->is_searchable_options->label_text) ? trim($json_parameter->is_searchable_options->label_text) : '',
                                            'type'       => 'CHECKBOX',
                                            'default_value' => isset($json_parameter->is_searchable_options->default_value) ? $json_parameter->is_searchable_options->default_value : false
                                        );
                                    }

                                    $data = array(
                                        'label_text'            => (string) $json_parameter->label_text,
                                        'button_text'           => null,
                                        'max_items'             => null,
                                        'help_text'             => null,
                                        'is_searchable'         => (isset($json_parameter->is_searchable) && intval($json_parameter->is_searchable) ? 1 : 0),
                                        'is_searchable_options' => $is_searchable_options,
                                        'is_required'           => (isset($json_parameter->is_required) && intval($json_parameter->is_required) ? 1 : 0)
                                    );

                                    if (isset($json_parameter->button_text) && trim($json_parameter->button_text)) {
                                        $data['button_text'] = trim($json_parameter->button_text);
                                    }
                                    if (isset($json_parameter->max_items) && trim($json_parameter->max_items)) {
                                        $data['max_items'] = intval($json_parameter->max_items);
                                    }
                                    if (isset($json_parameter->help_text) && trim($json_parameter->help_text)) {
                                        $data['help_text'] = trim($json_parameter->help_text);
                                    }

                                    $new_parameter->data = json_encode($data);
                                } elseif ('YEAR' === $dbParameter->type_id || 'MONTHYEAR' === $dbParameter->type_id) {
                                    $is_searchable_options = null;
                                    if (isset($json_parameter->is_searchable_options)) {
                                        $is_searchable_options = array(
                                            'label_text' => isset($json_parameter->is_searchable_options->label_text) && trim($json_parameter->is_searchable_options->label_text) ? trim($json_parameter->is_searchable_options->label_text) : '',
                                            'type'       => isset($json_parameter->is_searchable_options->type) ? trim($json_parameter->is_searchable_options->type) : null
                                        );
                                    }

                                    $data = array(
                                        'label_text'            => (string) $json_parameter->label_text,
                                        'placeholder_text'      => null,
                                        'help_text'             => null,
                                        'interval_begin_value'  => 0,
                                        'interval_begin_period' => 'Year',
                                        'interval_end_value'    => 0,
                                        'interval_end_period'   => 'Year',
                                        'is_reverse'            => (isset($json_parameter->is_reverse) && intval($json_parameter->is_reverse) ? 1 : 0),
                                        'default_value'         => null,
                                        'is_searchable'         => (isset($json_parameter->is_searchable) && intval($json_parameter->is_searchable) ? 1 : 0),
                                        'is_searchable_options' => $is_searchable_options,
                                        'is_required'           => (isset($json_parameter->is_required) && intval($json_parameter->is_required) ? 1 : 0),
                                        'is_hidden'             => (isset($json_parameter->is_hidden) && intval($json_parameter->is_hidden) ? 1 : 0)
                                    );

                                    if (isset($json_parameter->placeholder_text) && trim($json_parameter->placeholder_text)) {
                                        $data['placeholder_text'] = trim($json_parameter->placeholder_text);
                                    }
                                    if (isset($json_parameter->help_text) && trim($json_parameter->help_text)) {
                                        $data['help_text'] = trim($json_parameter->help_text);
                                    }
                                    if (isset($json_parameter->interval_begin_value) && trim($json_parameter->interval_begin_value)) {
                                        $data['interval_begin_value'] = intval($json_parameter->interval_begin_value);
                                        if (isset($json_parameter->interval_begin_period) && trim($json_parameter->interval_begin_period)) {
                                            $data['interval_begin_period'] = trim($json_parameter->interval_begin_period);
                                        }
                                    }
                                    if (isset($json_parameter->interval_end_value) && trim($json_parameter->interval_end_value)) {
                                        $data['interval_end_value'] = intval($json_parameter->interval_end_value);
                                        if (isset($json_parameter->interval_end_period) && trim($json_parameter->interval_end_period)) {
                                            $data['interval_end_period'] = trim($json_parameter->interval_end_period);
                                        }
                                    }
                                    if (isset($json_parameter->default_value) && trim($json_parameter->default_value)) {
                                        $data['default_value'] = trim($json_parameter->default_value);
                                    }

                                    $new_parameter->data = json_encode($data);
                                } else {
                                    $is_searchable_options = null;
                                    if (isset($json_parameter->is_searchable_options)) {
                                        $is_searchable_options = array(
                                            'label_text' => isset($json_parameter->is_searchable_options->label_text) && trim($json_parameter->is_searchable_options->label_text) ? trim($json_parameter->is_searchable_options->label_text) : '',
                                            'type'       => isset($json_parameter->is_searchable_options->type) ? trim($json_parameter->is_searchable_options->type) : null
                                        );
                                    }

                                    $data = array(
                                        'label_text'             => (string) $json_parameter->label_text,
                                        'placeholder_text'       => null,
                                        'default_value'          => null,
                                        'other_text'             => null,
                                        'other_label_text'       => null,
                                        'other_placeholder_text' => null,
                                        'help_text'              => null,
                                        'prefix'                 => null,
                                        'suffix'                 => null,
                                        'number_decimals'        => null,
                                        'number_min_value'       => null,
                                        'number_max_value'       => null,
                                        'currency_id'            => null,
                                        'is_searchable'          => (isset($json_parameter->is_searchable) && intval($json_parameter->is_searchable) ? 1 : 0),
                                        'is_searchable_options'  => $is_searchable_options,
                                        'is_required'            => (isset($json_parameter->is_required) && intval($json_parameter->is_required) ? 1 : 0),
                                        'is_hidden'              => (isset($json_parameter->is_hidden) && intval($json_parameter->is_hidden) ? 1 : 0)
                                    );

                                    if (isset($json_parameter->placeholder_text) && trim($json_parameter->placeholder_text)) {
                                        $data['placeholder_text'] = trim($json_parameter->placeholder_text);
                                    }
                                    if (isset($json_parameter->default_value) && trim($json_parameter->default_value)) {
                                        $data['default_value'] = trim($json_parameter->default_value);
                                    }
                                    if (isset($json_parameter->other_text) && trim($json_parameter->other_text)) {
                                        $data['other_text'] = trim($json_parameter->other_text);
                                        if (isset($json_parameter->other_label_text) && trim($json_parameter->other_label_text)) {
                                            $data['other_label_text'] = trim($json_parameter->other_label_text);
                                        }
                                        if (isset($json_parameter->other_placeholder_text) && trim($json_parameter->other_placeholder_text)) {
                                            $data['other_placeholder_text'] = trim($json_parameter->other_placeholder_text);
                                        }
                                    }
                                    if (isset($json_parameter->help_text) && trim($json_parameter->help_text)) {
                                        $data['help_text'] = trim($json_parameter->help_text);
                                    }
                                    if (isset($json_parameter->prefix) && trim($json_parameter->prefix)) {
                                        $data['prefix'] = trim($json_parameter->prefix);
                                    }
                                    if (isset($json_parameter->suffix) && trim($json_parameter->suffix)) {
                                        $data['suffix'] = trim($json_parameter->suffix);
                                    }
                                    if (isset($json_parameter->number_decimals) && $json_parameter->number_decimals !== null) {
                                        $data['number_decimals'] = intval($json_parameter->number_decimals);
                                    }
                                    if (isset($json_parameter->number_min_value) && $json_parameter->number_min_value !== null) {
                                        $data['number_min_value'] = intval($json_parameter->number_min_value);
                                    }
                                    if (isset($json_parameter->number_max_value) && $json_parameter->number_max_value !== null) {
                                        $data['number_max_value'] = intval($json_parameter->number_max_value);
                                    }
                                    if (isset($json_parameter->currency_id) && is_numeric(trim($json_parameter->currency_id))) {
                                        $data['currency_id'] = trim($json_parameter->currency_id);
                                    }
                                    if (isset($json_parameter->max_length) && $json_parameter->max_length !== null) {
                                        $data['max_length'] = intval($json_parameter->max_length);
                                    }

                                    $new_parameter->data = json_encode($data);
                                }

                                $new_parameter->save();

                                $fieldset_parameters[] = $new_parameter;
                            }

                            $new_fieldset->Parameters = $fieldset_parameters;
                            $new_fieldset->save();
                        }
                    }
                }
            }

            // clear cached categories tree
            (new Categories())->deleteMemcachedData();
        }

        // Redirect back to edit when done with save
        return $this->redirect_to('admin/category-parameterization/edit/' . $original_category_id);
    }

    /**
     * Helper method to generate parent_id dropdown
     *
     * @param integer $preselected_category_value   Value to preselect
     * @param array                                 Array of values to disable in dropdown
     */
    public function choosen_categories_id_dropdown($preselected_category_value = 1, $disabled = array())
    {
        Tag::setDefault('choosen_categories_id', $preselected_category_value);

        $disabled = array_merge($disabled, Categories::getCategoriesWithoutTransactionType());

        $choosen_categories_id_dropdown = Tag::select(array(
            'choosen_categories_id',
            Categories::findFirst(1)->getSelectValues(),
            'disabled' => $disabled,
            'multiple' => 'multiple',
            'class' => 'form-control show-tick'
        ));

        $this->view->setVar('choosen_categories_id_dropdown', $choosen_categories_id_dropdown);
    }

    /**
     * Compare two category's parameters so we can get all fields that could be copied from one to another
     * @param int $source_category_id
     * @param int $target_category_id
     * @return json
     */
    public function compareAction($source_category_id, $target_category_id)
    {
        $result = array(
            'status' => false
        );

        $this->disable_view();
        $this->response->setContentType('application/json', 'UTF-8');

        if (!$this->request->isAjax()) {
            $this->response->setStatusCode(400, 'Bad Request');
            $result['msg'] = 'Bad request';
        } else {
            $source_category = Categories::findFirst($source_category_id);
            if ($source_category) {
                $target_category = Categories::findFirst($target_category_id);
                if ($target_category) {
                    $source_parameters = array();
                    $target_parameters = array();

                    if ($source_category_parameters = $source_category->FieldsetParameters) {
                        foreach ($source_category_parameters as $parameter) {
                            $label_text = '';
                            if (isset($parameter->data) && $parameter->data) {
                                if ($json_data = json_decode($parameter->data)) {
                                    $label_text = $json_data->label_text;
                                }
                            } elseif (isset($parameter->levels) && $parameter->levels) {
                                if ($json_levels = json_decode($parameter->levels)) {
                                    $label_text_array = array();
                                    foreach ($json_levels as $level) {
                                        $label_text_array[] = $level->label_text;
                                    }
                                    $label_text = implode(' › ', $label_text_array);
                                }
                            }
                            if (trim($label_text)) {
                                $source_parameters['id_' . $parameter->parameter_id] = $label_text;
                            }
                        }
                    }

                    if ($target_category_parameters = $target_category->FieldsetParameters) {
                        foreach ($target_category_parameters as $parameter) {
                            $label_text = '';
                            if (isset($parameter->data) && $parameter->data) {
                                if ($json_data = json_decode($parameter->data)) {
                                    $label_text = $json_data->label_text;
                                }
                            } elseif (isset($parameter->levels) && $parameter->levels) {
                                if ($json_levels = json_decode($parameter->levels)) {
                                    $label_text_array = array();
                                    foreach ($json_levels as $level) {
                                        $label_text_array[] = $level->label_text;
                                    }
                                    $label_text = implode(' › ', $label_text_array);
                                }
                            }
                            if (trim($label_text)) {
                                $target_parameters['id_' . $parameter->parameter_id] = $label_text;
                            }
                        }
                    }

                    // now, do the actual matching and return only those parameters
                    // that could be transferred to target category
                    $matching_parameters = array_intersect_key($source_parameters, $target_parameters);
                    $result['status']    = true;
                    $result['data']      = $matching_parameters;
                    $result['count']     = count($matching_parameters);
                }
            }
        }
        $this->response->setJsonContent($result);

        return $this->response;
    }
}
