<?php

namespace Baseapp\Backend\Controllers;

use Baseapp\Models\Notices;
use Baseapp\Extension\Tag;
use Baseapp\Library\Tool;
use Baseapp\Library\Utils;
use Phalcon\Paginator\Adapter\QueryBuilder;
use Baseapp\Traits\CrudActions;
use Baseapp\Traits\CrudHelpers;

class NoticesController extends IndexController
{
    use CrudActions;
    use CrudHelpers;

    public $crud_model_class = 'Baseapp\Models\Notices';

    protected $allowed_roles = array('admin');

    protected function getPossibleOrderByOptions()
    {
        $order_bys = array(
            'received_at' => 'Received at',
            'title'       => 'Title'
        );

        return $order_bys;
    }

    public function indexAction()
    {
        $this->tag->setTitle('Company notices');

        $page = $this->getPageParam();
        $search_for = $this->processSearchParam();
        $mode = $this->processSearchMode();
        $dir = $this->processAscDesc('desc');
        $order_by = $this->processOrderBy('id');
        $limit = $this->processLimit();

        Tag::setDefaults(array(
            'q'        => $search_for,
            'mode'     => $mode,
            'dir'      => $dir,
            'order_by' => $order_by,
            'limit'    => $limit
        ), true);

        // Available search fields
        $available_fields = array(
//            'id',
            'title',
            'message',
            'received_at'
        );
        $field = $this->processOptions('field', $available_fields, null);
        Tag::setDefault('field', $field);
        if (null === $field || empty($field)) {
            $field = 'all';
        }

        // Build the query
        $builder = $this->modelsManager->createBuilder()
            ->columns(array('um.*'))
            ->addFrom('Baseapp\Models\Notices', 'um')
            ->where(
                'um.user_id IS NULL AND um.entity = :entity:',
                array(
                    'entity' => 'notice'
                )
            );

        // If a specific field is specified, search only that one, otherwise search across all the fields
        // (but only if there's an actual search term entered)
        $fields_to_search = (null === $field || 'all' === $field) ? $available_fields : array($field);
        $search_params = Utils::build_phalcon_search_sql_params($fields_to_search, $search_for, $mode);
        if (null !== $search_params && is_array($search_params)) {
            $conditions = array();
            $binds = array();
            foreach ($search_params as $fld => $val) {
                $field_bind_name = sprintf(':%s:', $fld);
                $conditions[] = 'n.' . $fld . ' LIKE ' . $field_bind_name;
                $binds[$fld] = $val;
            }
            $conditions = $this->changeSearchParamsLikeIntoEqualsWhenExactMode($conditions, $mode);
            $builder->andWhere(implode(' OR ', $conditions), $binds);
        }

        $builder->orderBy('um.' . $order_by . ' ' . strtoupper($dir));


        $paginator = new QueryBuilder(array(
            'builder' => $builder,
            'limit'   => $limit,
            'page'    => $page
        ));
        $current_page = $paginator->getPaginate();
        $pagination_links = Tool::paginationAdmin(
            $current_page,
            'admin/notices',
            'pagination',
            $this->config->settingsBackend->pagination_count_out,
            $this->config->settingsBackend->pagination_count_in
        );

        $this->view->setVar('page', $current_page);
        $this->view->setVar('pagination_links', $pagination_links);
    }

    public function createAction()
    {
        $this->tag->setTitle('Create new company notice');
        $this->view->setVar('form_title_long', 'Create new company notice');
        $this->view->setVar('form_action', 'add');
        $this->add_common_crud_assets();
        // Use the same form/view as editAction
        $this->view->pick('notices/edit');

        $notice = new Notices();
        if ($this->request->isPost()) {
            $user_id = $this->auth->get_user()->id;
            $notice->created_by_user_id = $user_id;

            $created = $notice->add_new($this->request);
            if ($created instanceof Notices) {
                $this->flashSession->success('<strong>Company notice created!</strong>');
                $save_action = $this->get_save_action();
                $next_url = $this->get_next_url();
                // Override next url to go to entity edit in save button case
                if ('save' === $save_action) {
                    $next_url = 'admin/notices/edit/' . $created->id;
                }
                return $this->redirect_to($next_url);
            } else {
                $this->view->setVar('errors', $created);
                $notice->active = $this->request->hasPost('active') ? 1 : 0;
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
            }
        } else {
            $notice->active = 1;
        }

        $this->view->setVar('notice', $notice);
    }

    public function add_common_crud_assets()
    {
        $this->assets->addJs('assets/vendor/ckeditor/ckeditor.js');
        $this->assets->addJs('assets/vendor/ckeditor/adapters/jquery.js');
        $this->assets->addJs('assets/vendor/moment/moment.js');
        $this->assets->addJs('assets/vendor/moment/locale/hr.js');
        $this->assets->addCss('assets/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.css');
        $this->assets->addJs('assets/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js');
        $this->assets->addJs('assets/backend/js/notices-edit.js');
    }

    /**
     * Edit Action
     */
    public function editAction($entity_id = null)
    {
        $entity_id = (int) $entity_id;

        /* @var $entity Notices */
        $model = $this->crud_model_class;
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }

        $title = 'Edit company notice';
        $this->tag->setTitle($title);
        $this->view->setVar('form_title_long', $title);
        $this->add_common_crud_assets();

        if ($this->request->isPost()) {
            $entity->modified_by_user_id = $this->auth->get_user()->id;
            $saved = $entity->save_changes($this->request);
            if ($saved instanceof Notices) {
                $this->flashSession->success('<strong>Company notice updated!</strong>');
                $next_url = $this->get_next_url();
                $save_action = $this->get_save_action();
                if ('save' === $save_action) {
                    $next_url = 'admin/notices/edit/' . $saved->id;
                }
                return $this->redirect_to($next_url);
            } else {
                $this->view->setVar('errors', $saved);
                $entity->active = $this->request->hasPost('active') ? 1 : 0;
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
            }
        }

        $this->view->setVar('notice', $entity);
        $this->view->setVar('user_created', $entity->CreatedBy);
        $this->view->setVar('user_modified', $entity->ModifiedBy);
    }

    /**
     * activeToggle Action
     */
    public function activeToggleAction($entity_id = null)
    {
        $entity_id = (int) $entity_id;

        /* @var $entity Notices */
        $model = $this->crud_model_class;
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }

        if ($entity->activeToggle()) {
            $this->flashSession->success('Active status for <strong>' . $entity->ident() . '</strong> changed successfully.');
        } else {
            $this->flashSession->error('Failed changing active status for <strong>' . $entity->ident() . '</strong>.');
        }

        return $this->redirect_back();
    }

}
