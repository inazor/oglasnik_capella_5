<?php

namespace Baseapp\Backend\Controllers;

use Baseapp\Extension\Tag;
use Baseapp\Library\Tool;
use Baseapp\Library\Utils;
use Baseapp\Models\Ads;
use Baseapp\Models\Users;
use Baseapp\Models\UsersShops;
use Baseapp\Models\InfractionReports;
use Baseapp\Traits\CrudHelpers;

class InfractionReportsController extends IndexController
{
    use CrudHelpers;

    public $crud_model_class = 'Baseapp\Models\InfractionReports';

    protected $allowed_roles = InfractionReports::ALLOWED_ROLES;

    protected function get_models2controller_map()
    {
        return array(
            'Baseapp\Models\Ads'        => 'ads',
            'Baseapp\Models\Users'      => 'users',
            'Baseapp\Models\UsersShops' => 'shops'
        );
    }

    protected function get_modelNames()
    {
        return array(
            'Baseapp\Models\Ads'        => 'Ads',
            'Baseapp\Models\Users'      => 'Users',
            'Baseapp\Models\UsersShops' => 'Shops'
        );
    }

    protected function get_modelIcons()
    {
        return array(
            'Baseapp\Models\Ads'        => '<span class="fa fa-file fa-fw" title="Ads"></span>',
            'Baseapp\Models\Users'      => '<span class="fa fa-user fa-fw" title="Users"></span>',
            'Baseapp\Models\UsersShops' => '<span class="fa fa-shopping-cart fa-fw" title="Shops"></span>'
        );
    }

    protected function get_resolvedStatus()
    {
        return array(
            'all' => 'All',
            'yes' => 'Resolved',
            'no'  => 'Unresolved'
        );
    }

    /**
     * Index Action
     */
    public function indexAction()
    {
        $title = 'Infraction reports Viewer';
        $this->tag->setTitle($title);
        $this->view->setVar('page_title', $title);

        $page       = $this->getPageParam();
        $search_for = $this->processSearchParam();
        $mode       = $this->processSearchMode();

        $available_fields = array(
            'model_pk_val',
            'reporter_id',
            'report_reason',
            'report_message',
            'reported_at',
            'reported_ip'
        );
        $field = $this->processOptions('field', $available_fields, null);
        Tag::setDefault('field', $field);
        if (null === $field || empty($field)) {
            $field = 'all';
        }

        $model = $this->processOptions('model', $this->get_modelNames(), null);
        Tag::setDefault('model', $model);
        if (null === $model || empty($model)) {
            $model = 'all';
        }

        $resolved = $this->processOptions('resolved', $this->get_resolvedStatus(), null);
        if (null === $resolved || empty($resolved)) {
            $resolved = 'all';
        }
        Tag::setDefault('resolved', $resolved);

        // If a specific field is searched/requested, use only that one, otherwise search all the fields
        $fields_to_search = (null === $field || 'all' === $field) ? $available_fields : array($field);
        // If a specific model is searched/requested, use only that one, otherwise search all the models
        $model_to_search = (null === $model || 'all' === $model) ? null : $model;
        // If a specific resolved status is searched/requested, use only that one, otherwise search all statuses
        $status_to_search = (null === $resolved || 'all' === $resolved) ? null : $resolved;

        $paginator = new \Baseapp\Extension\Paginator\Adapter\SimpleMySQL(array(
            'sql'   => InfractionReports::buildSearchQuery($model_to_search, $status_to_search, $fields_to_search, $search_for, $mode),
            'limit' => $this->config->settingsBackend->pagination_items_per_page,
            'page'  => $page
        ));
        $current_page = $paginator->getPaginate();
        $current_page->items = InfractionReports::hydrateResults($current_page->items, $status_to_search);

        $pagination_links = Tool::paginationAdmin(
            $current_page,
            'admin/infraction-reports',
            'pagination',
            $this->config->settingsBackend->pagination_count_out,
            $this->config->settingsBackend->pagination_count_in
        );

        Tag::setDefaults(array(
            'q'        => $search_for,
            'mode'     => $mode,
            'field'    => $field,
            'model'    => $model
        ), true);
        $this->view->setVar('page', $current_page);
        $this->view->setVar('pagination_links', $pagination_links);
        $this->view->setVar('models_controllers_map', $this->get_models2controller_map());
        $this->view->setVar('models_icons_map', $this->get_modelIcons());
    }

    public function resolveAction()
    {
        $this->disable_view();
        $this->response->setContentType('application/json', 'UTF-8');
        $response_array = array('status' => false);

        if (!$this->request->isAjax()) {
            $this->response->setStatusCode(400, 'Bad Request');
            $response_array['msg'] = 'Bad Request';
        } else {
            $ids = null;
            if ($postIDs = $this->request->getPost('ids', 'string', null)) {
                if (strpos($postIDs, ',') !== false) {
                    $ids = explode(',', $postIDs);
                } elseif ((int) $postIDs) {
                    $ids[] = (int) $postIDs;
                }
            }

            if ($ids) {
                if (InfractionReports::resolveID($ids)) {
                    $response_array = array('status' => true);
                } else {
                    $response_array['msg'] = 'No infraction report found with given ID(s)!?!';
                }
            } else {
                $response_array['msg'] = 'No infraction report ID(s) given!?!';
            }
        }

        $this->response->setJsonContent($response_array);
        return $this->response;
    }
}
