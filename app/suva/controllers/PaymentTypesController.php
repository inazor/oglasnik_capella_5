<?php

namespace Baseapp\Suva\Controllers;

class PaymentTypesController extends IndexController{

    public function indexAction() {
			$this->tag->setTitle("Payment Types");
			$this->n_query_index("PaymentTypes"); //ovo je definirano u BaseController
    }

		public function crudAction($pEntityId = null) {
			$this->n_crud_action("PaymentTypes", $pEntityId); //ovo je definirano u BaseController
			$this->view->pick('payment-types/crud');
		}
}