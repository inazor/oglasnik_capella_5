<?php

namespace Baseapp\Suva\Controllers;



class InsertionsAvusController extends IndexController{

    public function indexAction() {
			\Baseapp\Suva\Library\Nava::printr($_SESSION['_context'], "Session context");
			$this->tag->setTitle("Insertions Avus");
			$this->n_query_index("InsertionsAvus"); //ovo je definirano u BaseController
    }

	public function crudAction($pEntityId = null) {
		//$this->printr($_REQUEST);
		$this->n_crud_action("InsertionsAvus", $pEntityId); //ovo je definirano u BaseController
		$this->view->pick('insertions-avus/crud');
	}
}