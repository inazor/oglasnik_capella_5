<?php

namespace Baseapp\Suva\Controllers;

class CategoriesMappingsController extends IndexController{

    public function indexAction( $pPublicationsId = null , $pAdId = null) {
		//$this->printr($_REQUEST, "CategoriesMappingsController:indexAction:REQUEST");
		//$this->printr($_SESSION['_context'], "CategoriesMappingsController:indexAction:CONTEXT");
		
		$this->tag->setTitle("Categories for Publications");
		
		if ($pPublicationsId){
			$publication = \Baseapp\Suva\Models\Publications::findFirst( $pPublicationsId );
			if ($publication){
				$this->tag->setTitle("CAT for $publication->name");
				
				//Postavljanje defaultnog filtera
				if (!array_key_exists('default_filter', $_SESSION['_context'])) $_SESSION['_context']['default_filter'] = array();
				$_SESSION['_context']['default_filter']['n_publications_id'] = $publication->id;
				
//				if (!array_key_exists('order_by', $_SESSION['_context'])) $_SESSION['_context']['order_by'] = array();
//				if (!array_key_exists('id', $_SESSION['_context']['order_by'])) $_SESSION['_context']['order_by']['id'] = 'desc';

			}
		}
		elseif ($pAdId > 0){
			$cmIds = \Baseapp\Suva\Library\libPCM::getIds(
				array (
					"modelName" => "CategoriesMappings"
					
					,"filters" => array (
						array(
							"modelName" => "Ads"
							,"whereField" => 'id'
							,"whereValue" => $pAdId
						)
					)
				)
			);
			if (!array_key_exists('default_filter', $_SESSION['_context'])) 
					$_SESSION['_context']['default_filter'] = array();
				
			if ( count($cmIds) > 0 ){
				$_SESSION['_context']['default_filter']['id'] = $cmIds;
			}
			else{
				$_SESSION['_context']['default_filter']['id'] = 0;
			} 
				
			//$this->printr($cmIds, "CM IDS");
		}
		
		

		$_SESSION['_context']['step_rec'] = 50;
		
		//error_log("CategoriesMappingsController::indexAction request ".print_r($_REQUEST));
		$this->n_query_index("CategoriesMappings"); //ovo je definirano u BaseController
		//$this->printr($_SESSION['_context'], "CategoriesMappingsController:indexAction: CONTEXT AFTER");
    }
	
	public function crudAction($pEntityId = null) {
		//$this->printr($_REQUEST, "catmap crud REQUEST");
		//$this->printr($_SESSION['_context'], "CategoriesMappingsController:crudAction:CONTEXT");
		if (array_key_exists('_default_publication_id',$_SESSION['_context']))
			$defaults = array('n_publications_id' => (int)$_SESSION['_context']['_default_publication_id'] );
		
		$this->view->pick('categories-mappings/crud');
		
		$this->n_crud_action("CategoriesMappings", $pEntityId, false, $defaults ); //ovo je definirano u BaseController
		
		if ($pEntityId){
			$entity = \Baseapp\Suva\Models\CategoriesMappings::findFirst( $pEntityId );
			$this->tag->setTitle("CAT $entity->name");
		}
	}
	
	public function createDefaultPublicationsCategoriesMappingAction($pCategoryMappingId = null ){
				
		if ( !$pCategoryMappingId > 0 ) return;
		$categoryMapping = \Baseapp\Suva\Models\CategoriesMappings::findFirst( $pCategoryMappingId );
		if ( !$categoryMapping ) return;
		
		// $newPCM = new \Baseapp\Suva\Models\PublicationsCategoriesMappings();
					
		// $newPCM->publication_id = $categoryMapping->n_publications_id;
		// $newPCM->category_mapping_id = $categoryMapping->id;
		// $newPCM->is_active = 1;
		
		\Baseapp\Suva\Library\Nava::runSql("
			insert into n_publications_categories_mappings 
			(publication_id, category_mapping_id, is_active ) 
			values 
			( $categoryMapping->n_publications_id , $categoryMapping->id, 1 )
			");
		
		// $success = $newPCM->create();
		// if (!$success) {
			
			// //error_log("PublicationsController.php: createDefaultCategoryMappingsAction 5 ERRORS:  ".print_r($newPCM->getMessages(), true));
			// $this->printr($newPCM->getMessages, "ERRORS CREATING NEW PCM");
		// }
		// else{ 
			// $this-poruka("successfully created");
			// //error_log("PublicationsController.php: createDefaultCategoryMappingsAction 6 success: $success");
		// }

		$this->redirect_back();
		
	}
}