<?php

namespace Baseapp\Suva\Controllers;

class RolesPermissionsController extends IndexController{

    public function indexAction() {
			$this->tag->setTitle("User's Permissions by Roles");
			$this->n_query_index("RolesPermissions"); //ovo je definirano u BaseController
    }

		public function crudAction($pEntityId = null) {
			//$this->printr($_REQUEST);
			$this->n_crud_action("RolesPermissions", $pEntityId); //ovo je definirano u BaseController
			$this->view->pick('roles-permissions/crud');
		}
}

