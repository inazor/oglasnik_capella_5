<?php

namespace Baseapp\Suva\Controllers;

class LayoutssController extends IndexController{

    public function indexAction() {
		//$this->printr($_SESSION['_context']);
		$_SESSION['_context']['step_rec'] = 20;
		$this->tag->setTitle("Layouts");
		$this->n_query_index("Layoutss"); //ovo je definirano u BaseController
    }

	public function crudAction($pEntityId = null) {
		//$this->printr($_REQUEST);
		$this->view->pick('layoutss/crud');
		$this->n_crud_action("Layoutss", $pEntityId); //ovo je definirano u BaseController
	}
	
	public function deleteAction($pEntityId = null) {
		//$this->printr($_REQUEST);
		//$this->view->pick('layoutss/crud');
		$this->n_delete_action("Layoutss", $pEntityId); //ovo je definirano u BaseController
		
		// nikola nakon delete actiona jednostavno vraca na prethodi url
		return $this->response->redirect($this->request->getServer('HTTP_REFERER'));
	}
}
