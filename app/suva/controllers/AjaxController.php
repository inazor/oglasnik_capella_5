<?php

namespace Baseapp\Suva\Controllers;
use Baseapp\Suva\Models\Ads;
use Baseapp\Suva\Models\Users;
use Baseapp\Suva\Models\UsersFavoriteAds;
use Baseapp\Suva\Models\UsersMessages;
use Baseapp\Suva\Models\UsersEmailAgents;
use Baseapp\Suva\Models\Dictionaries;
use Baseapp\Suva\Models\Locations;
use Baseapp\Suva\Models\Media;
use Baseapp\Suva\Models\Tags;
use Baseapp\Suva\Models\SearchQueries;
use Baseapp\Library\Tool;
use Baseapp\Library\OEmbedUtils;
use Phalcon\Http\Response;
use Phalcon\Paginator\Adapter\QueryBuilder;

use Baseapp\Suva\Models\Categories;
use Baseapp\Suva\Library\Nava;

use Baseapp\Suva\Models\ProductsCategoriesPrices;

/**
 * Ajax Frontend Controller
 */
//class AjaxController extends \Baseapp\Frontend\Controllers\IndexController
class AjaxController extends IndexController
{
    /**
     * @var \Phalcon\Http\Response
     */
    public $response;

    private $_allowOnlyAjax = true;

    /**
     * Initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->response = new Response();
        $this->response->setContentType('application/json', 'UTF-8');
        if ($this->app->config->app->env === 'development') {
            $this->_allowOnlyAjax = false;
        }

        // Hopefully disables the view for all of the controller's actions in a single place
        $this->disable_view();
    }

    /**
     * Test if current request is actually an AJAX request
     * @return boolean
     */
    private function _isAjaxRequest()
    {
        if ($this->_allowOnlyAjax) {
            if (!$this->request->isAjax()) {
                $this->response->setStatusCode(400, 'Bad Request');
                return false;
            }
        }
        return true;
    }

	public function checkMessagesAction(){
		error_log("checkMessages" .microtime());
		//ova funkcija ništa ne radi, sve odradi baseSuvaController. 
		//Obrade jedino trebaju slat poruke
		//Nava::poruka (time());
		
	}

	public function getContextAction(){
		//ovu funkciju poziva klijent da dohvati najnoviji kontekst. 
		//Npr, poziva je svaku sekundu da dohvati nove poruke s klijenta ako ih ima
		//error_log("getContextAction" .microtime());
		
		//ako postoje poruke koje su ubačene od nekog drugog threada, treba ih stavit u context_msgFlashsession da se prikažu
		if ( array_key_exists( '_msg_threaded', $_SESSION['_context'] ) ){
			//više ne prema timestampu nego prema lastSentId-ju
			// if ( ! array_key_exists ( '_msg_threaded_last_sent_timestamp' , $_SESSION['_context'] ) ){
				// $_SESSION['_context']['_msg_threaded_last_sent_timestamp'] = 0;
			// }
			// $lastSent = $_SESSION['_context']['_msg_threaded_last_sent_timestamp'];
			// foreach ( $_SESSION['_context']['_msg_threaded'] as $time => $message ){
				// //ako poruka još nije poslana, stavi je na popis poruka za slanje i makni je iz liste za slanje
				// if ( $time > $lastSent ){
					// array_push( $_SESSION['_context']['_msg_flashsession'], $message );
					// $lastSent = microtime(true);
				// }
				// else {
					// //ako je već prije poslana
					// unset ( $_SESSION['_context']['_msg_threaded'][$time] );
				// }
			// }
			// $_SESSION['_context']['_msg_threaded_last_sent_timestamp'] = $lastSent;

			//error_log("getContextAction _msgThreaded:".print_r ( $_SESSION['_context']['_msg_threaded'], true ));

			
			if ( ! array_key_exists ( '_msg_threaded_last_sent_id' , $_SESSION['_context'] ) ){
				$_SESSION['_context']['_msg_threaded_last_sent_id'] = 0;
			}
			$lastSentId = $_SESSION['_context']['_msg_threaded_last_sent_id'];
			foreach ( $_SESSION['_context']['_msg_threaded'] as $id => $message ){
				//ako poruka još nije poslana, stavi je na popis poruka za slanje i makni je iz liste za slanje
				if ( $id > $lastSentId ){
					array_push( $_SESSION['_context']['_msg_flashsession'], $message );
					$lastSentId = $id;
				}
				else {
					//ako je već prije poslana
					unset ( $_SESSION['_context']['_msg_threaded'] [$id] );
				}
			}
			$_SESSION['_context']['_msg_threaded_last_sent_id'] = $lastSentId;
  

		}
		
		//ova funkcija ništa ne radi, sve odradi baseSuvaController. 
		//Obrade jedino trebaju slat poruke
		//Nava::poruka (time());
		
	}


	public function pushToContextAction ($pParam , $pValue = null){
		// error_log("pushToContextAction");
		if ($pValue){ 
			// error_log ("Postavljam $pParam na $pValue");
			$_SESSION['_context'][$pParam] = $pValue;
		}
		else{
			// error_log("Resetiram $pParam");
			unset ($_SESSION['_context'][$pParam]);
		}
		
		// $this->view->disable();
		// $this->view->finish();
	
	}

    /**
     * Sets a 404 response status code and a JSON encoded response content
     * (with an optional custom message)
     *
     * @param string $custom_message
     */
    private function _notFound($custom_message = '')
    {
        $response_array = array('status' => false);

        $msg = trim($custom_message);
        if (!empty($msg)) {
            $response_array['msg'] = trim($msg);
        }

        $this->response->setStatusCode(404, 'Not found');
        $this->response->setJsonContent($response_array);
    }

	
	public function ctlTextTypeV3Action( 
		$pModelName = null, 
		$pValueSearch = null, 
		$pExprSearch = null, 
		$pExprValue = "''",
		$pExprShowList = "''",
		$pExprShowInput = "''",
		$pAdditionalWhere = '',
		$pNrReturnedRows = 15
		)
	{
        //error_log("AJAX CONTROLLER SUVA ".$pValueSearch);
		//ime tablice iz imena modela
		$class = '\\Baseapp\\Suva\\Models\\'.$pModelName;
		$entity = new $class();
		$tableName = $entity->getSource();
		
       // error_log("ctlTextTypeV3Action");
		//parsiranje SELECT izraza 
		$exprSearch = $this->ctlTextTypeParseSqlExpression($pExprSearch);
		$exprValue = $this->ctlTextTypeParseSqlExpression($pExprValue);
		$exprShowInput = $this->ctlTextTypeParseSqlExpression($pExprShowInput);
		$exprShowList = $this->ctlTextTypeParseSqlExpression($pExprShowList);
		
		$txtSql = 
		" select "
		."		$exprValue as _value"
		."		, $exprShowInput as _show_input "
		."		, $exprShowList as _show_list "
		." from $tableName "
		." where cast( ( $exprSearch )  as char) like '%$pValueSearch%' "
		.$pAdditionalWhere
		." limit $pNrReturnedRows ";
		
		//error_log("suva/AjaxController/ctlTextTypeV3Action: txtSql: $txtSql");
		
        $response_array = \Baseapp\Suva\Library\Nava::sqlToArray( $txtSql );
        
		$jsonContent = json_encode($response_array);
		//error_log("suva/AjaxController/ctlTextTypeV3Action: response: ".print_r($response_array, true));
		
		$this->response = new Response();
		$this->response->setContentType('application/json', 'UTF-8');
		$this->response->setStatusCode(200, "OK");
		$this->response->setContent($jsonContent);
		//$json = "{'id':12}";
		//$this->response->setContent($json);
		
		
//		$this->response->send();
		$this->disable_view();
		//return $this->response;
		
        // $this->response->setJsonContent($response_array);
		
		// //error_log("suva/AjaxController/ctlTextTypeV3Action: response class ".get_class($this->response));
		
		
        // return $this->response;
        //return $response;    
    }

	public function ctlTextTypeParseSqlExpression( $pSqlExpression = null ){
		//error_log("ctlTextTypeParseSqlExpression 1");
		$response = $pSqlExpression;
		$response = str_replace("_SNG_QUOT_","'", $response);
		$response = str_replace("_LEFT_BRACKET_","(", $response);
		$response = str_replace("_RIGHT_BRACKET_",")", $response);
		$response = str_replace("_COMMA_",",", $response);
		$response = str_replace("_SPACE_"," ", $response);
		//error_log("ctlTextTypeParseSqlExpression 2 response: $response");
        return $response;
	}

	public function updateFieldAction( $pTableName = null, $pRecId = null, $pFieldName = null, $pFieldValue = null){
        
		// Nava::poruka2("System");
		
        //error_log("frontend/AjaxController/updateFieldAction 1");
		//error_log ("frontend/AjaxController/updateFieldAction 2  pTableName: $pTableName, pRecId: $pRecId,  pFieldName: $pFieldName, pFieldValue: $pFieldValue  ");
		
		$pFieldValue = urldecode ($pFieldValue);
		$pFieldValue = str_replace('__SLASH__', '/', $pFieldValue);
		
		$modelName = array_key_exists($pTableName, $this->n_table_model_mapping) ? $this->n_table_model_mapping[$pTableName] : null;
		if ($modelName) {
			$class = '\\Baseapp\\Suva\\Models\\'.$modelName;
			//$entity = new $class();
			
			$entity = $pRecId > 0 ? $class::findFirst($pRecId): null;
			if ($entity){
				$fields = $entity->n_fields_list($entity->getSource());
				if (in_array($pFieldName, $fields)){
					$types = $entity->n_field_types($entity->getSource());
					$type = $types[$pFieldName];
				
					$modelValue = $entity->n_interpret_value_from_request($pFieldValue, $type);
					$entity->assign(array($pFieldName =>$modelValue));
					//error_log(print_r($entity->toArray(), true));
					//NIKOLA: dodano jer neki avus oglasi nemaju title i/ili description pa im se ne moze pridjeliti npr layoutid jer se ne mogu update 
					if(isset($entity->title) && $entity->title == '' ) $entity->title = 'placeholder - title was empty';
					if(isset($entity->description) && $entity->description == '' ) $entity->description = 'placeholder - description was emprty';					
					if ($entity->update() === true){
						$response = 'OK';
						//brise sve insertione iz avusa ako se locka ad za avus
						if($pTableName == 'ads'){
							if($entity->n_dont_sync_with_avus == 1){
								Nava::runSql("delete from n_insertions_avus where ad_id = ".$entity->id);
							}
							
						}
						

					} 
					else {
						$greske = $entity->getMessages();
						foreach ($greske as $greska)  
							$err_msg .= $greska.", ";
						$response = "NOK - type = $type,  $pFieldName = $modelValue, $err_msg";
					}
				}
				else $response = 'NOK no field';
			} 
			else $response = 'NOK no entity';
		}
		else $response = "NOK no  model for table $pTableName (probbably not mapped in BaseModel::n_table_model_mapping. Copy also exists in BaseController.".PHP_EOL
				."Current model mapping:".print_r($this->n_table_model_mapping, true).PHP_EOL;
		 
        $this->response->setJsonContent(array($response));
        return $this->response;
    }
	
	public $n_table_model_mapping = array(
        'ads' => 'Ads',
        'categories' => 'Categories',
        'n_actions' => 'Actions',
		'n_categories_mappings' => 'CategoriesMappings',
        'n_discounts' => 'Discounts',
        'n_discounts_products' => 'DiscountsProducts',
        'n_fiscal_locations' => 'FiscalLocations',
        'n_insertions' => 'Insertions',
        'n_issues' => 'Issues',
        'n_order_statuses' => 'OrdersStatuses',
        'n_order_status_transitions' => 'OrderStatusTransitions',
        'n_payment_types' => 'PaymentTypes',
        'n_products' => 'Products',
        'n_products_categories_types' => 'ProductsCategoriesTypes',
        'n_publications' => 'Publications',
		'n_publications_categories' => 'PublicationsCategories',
		'n_publications_categories_mappings' => 'PublicationsCategoriesMappings',
        'n_roles_permissions' => 'RolesPermissions',
		'n_users_contacts' => 'UsersContacts',
        'orders' => 'Orders',
        'orders_items' => 'OrdersItems',
        'orders' => 'Orders',
        'parameters' => 'Parameters',
        'roles' => 'Roles',
        'roles_users' => 'RolesUsers',
        'users' => 'Users',
		'n_products_categories_prices' => 'ProductsCategoriesPrices',
    );

 
	
	
	public function jsonCategoriesAction( $pParentCategory = null){
        
		$categories = \Baseapp\Suva\Models\Categories::find ( " parent_id ".($pParentCategory ? " = $pParentCategory ": " is null ") );
		
		$this->response->setJsonContent($categories->toArray());
		return $this->response;
    }


  
    //AJAX ZA CUSTOMER INFO
    public function contactInfoAction (){
        
        if ($this->request->isAjax()){
			echo "tekst";
		}
            $request = $this->request;
        
//         	$checkedAction = $request->hasPost('_contact_action') ? $request->getPost('_contact_action') : null;
// 			$checkedContactId = $request->hasPost('_contact_id') ? $request->getPost('_contact_id') : null;
// 			$checkedUserId = $request->hasPost('_contact_user_id') ? $request->getPost('_contact_user_id') : null;
// 			$checkedType = $request->hasPost('_contact_type') ? $request->getPost('_contact_type') : null;
// 			$checkedValue = $request->hasPost('_contact_value') ? $request->getPost('_contact_value') : null;
// 			if($checkedAction ){
// 				switch ($checkedAction) {
// 					case "edit":
// 						if($checkedContactId && $checkedValue)
// 							Nava::runSql("update n_users_contacts set name = '$checkedValue' where id = $checkedContactId");
// 						break;
// 					case "delete":
// 						if($checkedContactId)
// 							Nava::runSql("delete from n_users_contacts where id = $checkedContactId");
// 						break;
// 					case "create":
// 						if($checkedUserId && $checkedValue >'' && $checkedType)
// 							//error_log($checkedUserId);
// 							//error_log($checkedValue);
// 							//error_log($checkedType);
						
// 							$txtSql = "insert into n_users_contacts(name,type,user_id) values ('".$checkedValue."','".$checkedType."', ".$checkedUserId.")";
// 							//error_log($txtSql);
// 							Nava::runSql($txtSql);
// 						break;
// 				}
// 			}
        
        
        
    }

	public function pictureUploadAction (){
        //error_log("Ajax::pictureUpload 1");
        if ($this->request->isAjax()){
		//error_log("Ajax::pictureUpload 2");
			
			$filename = $_FILES['file']['name'];
			$file_extension = pathinfo($filename, PATHINFO_EXTENSION);
			$allowed_extensions =  array('jpg');
			$ad_id = $_POST['ad_id'];
			$image_name = $ad_id.'.' . pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION);
			//error_log("Ajax::pictureUpload 3");
			
			if($filename > '' && $ad_id > 0){
				$sourcePath = $_FILES['file']['tmp_name'];  
				$targetPath = "repository/dtp/originals/".$image_name; // Path di je spremljena slika
				$targetPath2 = "repository/dtp/to_process/".$image_name;
				$real_path = "repository/dtp/originals/".$image_name;	
				$paths = array($targetPath, $targetPath2);
				//error_log("Ajax::pictureUpload 4");
				
			foreach($paths as $path){
				if (file_exists($path)) {
    				unlink($path);		
					//error_log("Ajax::pictureUpload 5");
// 					echo "file already exsists";
				}
			}
				//error_log("Ajax::pictureUpload 6");
				if(!in_array($file_extension, $allowed_extensions) ) {
					//error_log("Ajax::pictureUpload 7");
					echo $file_extension." is not a valid extension, you can only upload jpg extension";
					return;
				}else{
					if(move_uploaded_file($sourcePath,$targetPath)){
						//error_log("Ajax::pictureUpload 8");
						copy($targetPath, $targetPath2);
						Nava::runSql("UPDATE ads SET n_picture_path = '$real_path' WHERE id = $ad_id");
						// echo "slika uspješno uploadan-a";
						echo "<img src=\"/$real_path\" id='uploaded_preview' style='width:100%;'/>";
						// echo $_SERVER['DOCUMENT_ROOT'];
						//error_log("Ajax::pictureUpload 9");
						return;
					}
				}
			}else{
				//error_log("Ajax::pictureUpload 1");
				echo 'ne postoji file ili ad_id';
				return;
			}	
		}
    }
	
	public function displayPictureUploadAction (){
        //error_log("Ajax::pictureUpload 1");
        if ($this->request->isAjax()){
		//error_log("Ajax::pictureUpload 2");
			
			$filename = $_FILES['file']['name'];
			$file_extension = pathinfo($filename, PATHINFO_EXTENSION);
			$allowed_extensions =  array('jpg');
			//error_log("Ajax::pictureUpload 3");
			
			if($filename > ''){
				$sourcePath = $_FILES['file']['tmp_name'];  
				$targetPath = "repository/dtp/originals/".$filename; // Path di je spremljena slika
				$targetPath2 = "repository/dtp/to_process/".$filename;
				$real_path = "repository/dtp/originals/".$filename;	
				$paths = array($targetPath, $targetPath2);
				//error_log("Ajax::pictureUpload 4");
				
			foreach($paths as $path){
				if (file_exists($path)) {
    				unlink($path);		
					//error_log("Ajax::pictureUpload 5");
// 					echo "file already exsists";
				}
			}
				//error_log("Ajax::pictureUpload 6");
				if(!in_array($file_extension, $allowed_extensions) ) {
					//error_log("Ajax::pictureUpload 7");
					echo $file_extension." is not a valid extension, you can only upload jpg extension";
					return;
				}else{
					if(move_uploaded_file($sourcePath,$targetPath)){
						//error_log("Ajax::pictureUpload 8");
						copy($targetPath, $targetPath2);
						// echo "slika uspješno uploadan-a";

						// echo $_SERVER['DOCUMENT_ROOT'];
						//error_log("Ajax::pictureUpload 9");
						return;
					}
				}
			}else{
				//error_log("Ajax::pictureUpload 1");
				echo 'ne postoji file ili ad_id';
				return;
			}	
		}
    }
	
	public function removeUploadedPhotoAction (){
        if ($this->request->isAjax()){
			$ad_id = $_POST['ad_id'];
			$original_image = "repository/dtp/originals/$ad_id.jpg";
			$processed_image = "repository/dtp/to_process/$ad_id.jpg";
			$files = array($original_image, $processed_image);
				
			if($ad_id){
				foreach ($files as $file) {
					if(!unlink($file)){	
						echo "file nije uspješno izbrisan, pokušajte ponovo";
						return;
					}else{
						Nava::runSql("UPDATE ads SET n_picture_path = '' WHERE id = $ad_id");
						echo 'picture removed';
					}
				}
			}
		}
    }
	
	public function publicationsUpdateForwardCategoryAction($pPublicationId = null, $pCategoryId = null, $pForwardCategoryId = null , $pIsActive = null){
		//error_log("AjaxController:publicationsUpdateForwardCategoryAction 1");
		
		$response = '';
		
		$publication = $pPublicationId > 0 ? \Baseapp\Suva\Models\Publications::findFirst($pPublicationId) : null ;
		if (!$publication) $response .= 'NOK - invalid publication ID'.PHP_EOL;
		
		$category = $pCategoryId > 0 ? \Baseapp\Suva\Models\Categories::findFirst($pCategoryId) : null ;
		if (!$category) $response .= 'NOK - invalid category ID'.PHP_EOL;
		
		$forwardCategory = $pForwardCategoryId > 0 ? \Baseapp\Suva\Models\Categories::findFirst($pForwardCategoryId) : null;
		if (!$forwardCategory) $response .= 'NOK - invalid category ID'.PHP_EOL;

		$entity = \Baseapp\Suva\Models\PublicationsCategories::findFirst("publication_id = $publication->id and category_id = $category->id");
		if (!$entity) $response .= "NOK - table row not found, publication_id = $publication->id and category_id = $category->id".PHP_EOL;

		if ($response === '') {

			$entity->forward_category_id = $forwardCategory->id;
			$entity->is_active = $pIsActive === '1' ? 1 : 0;
			if ($entity->update() === false) {
				//error_log("AjaxController:publicationsUpdateForwardCategoryAction 4");
				$response .= 'NOK - errors while updating:';
				$greske = $entity->getMessages();
				foreach ($greske as $greska)  
					$response .= $greska.", ";
			}
			else $response = 'OK';
		}
	    $this->response->setJsonContent(array($response));
        return $this->response;
	}
	
	//STARA, SA BRISANJEM ISTIH REDOVA. iZMIJENJENO JE, STAVLJENO JE AUTOMATSKO PUNJENJE U TRIGGER
	public function publicationsUpdateForwardCategoryActionOLD($pPublicationId = null, $pCategoryId = null, $pForwardCategoryId = null){
		//error_log("AjaxController:publicationsUpdateForwardCategoryAction 1");
		
		$response = '';
		
		$publication = $pPublicationId > 0 ? \Baseapp\Suva\Models\Publications::findFirst($pPublicationId) : null ;
		if (!$publication) $response .= 'NOK - invalid publication ID'.PHP_EOL;
		
		$category = $pCategoryId > 0 ? \Baseapp\Suva\Models\Categories::findFirst($pCategoryId) : null ;
		if (!$category) $response .= 'NOK - invalid category ID'.PHP_EOL;
		
		$forwardCategory = $pForwardCategoryId > 0 ? \Baseapp\Suva\Models\Categories::findFirst($pForwardCategoryId) : null;
		if (!$forwardCategory) $response .= 'NOK - invalid category ID'.PHP_EOL;

		$entity = \Baseapp\Suva\Models\PublicationsCategories::findFirst("publication_id = $publication->id and category_id = $category->id");
		
		if ($response === '') {
			//error_log("AjaxController:publicationsUpdateForwardCategoryAction 2");
			if ($entity && $category->id === $forwardCategory->id){
				//error_log("AjaxController:publicationsUpdateForwardCategoryAction 3");
				if ($entity->delete() === false) {
					//error_log("AjaxController:publicationsUpdateForwardCategoryAction 4");
					$response .= 'NOK - errors while deleting:';
					$greske = $entity->getMessages();
					foreach ($greske as $greska)  
						$response .= $greska.", ";
				}
				else $response = 'OK';
			}
			elseif (!$entity && $category->id !== $forwardCategory->id){
				//error_log("AjaxController:publicationsUpdateForwardCategoryAction 5");
				//ako su različiti onda se to snima u bazu ili update-a
				$entity = new \Baseapp\Suva\Models\PublicationsCategories();
				$entity->publication_id = $publication->id;
				$entity->category_id = $category->id;
				$entity->forward_category_id = $forwardCategory->id;
				if ($entity->create() === false) {
					//error_log("AjaxController:publicationsUpdateForwardCategoryAction 6");
					$response .= 'NOK - errors while saving:';
					$greske = $entity->getMessages();
					foreach ($greske as $greska)  
						$response .= $greska.", ";
				}
				else $response = 'OK';
			}
			elseif ($entity && $category->id !== $forwardCategory->id){
				//error_log("AjaxController:publicationsUpdateForwardCategoryAction 7");
				//ako već postoji i treba mu izmijenit forward category
				$entity->forward_category_id = $forwardCategory->id;
				if ($entity->update() === false) {
					//error_log("AjaxController:publicationsUpdateForwardCategoryAction 8");
					$response .= 'NOK - errors while saving:';
					$greske = $entity->getMessages();
					foreach ($greske as $greska)  
						$response .= $greska.", ";
				}
				else $response = 'OK';
			}
			else $response = 'NOK - ne bi trebalo biti ovdje';
		}
		//error_log("AjaxController:publicationsUpdateForwardCategoryAction 9 response: $response");

// 		$this->disable_view();
// 		$this->view->disable();
        $this->response->setJsonContent(array($response));
// 		$this->response->send();
// 		$this->response->setContent($response);
		
        return $this->response;
	}
	
	public function categoryMappingImageUploadAction(){
		//error_log("Ajax::category_mapping_image_upload 1");
		if ($this->request->isAjax()){
		//error_log("Ajax::category_mapping_image_upload 2");
			
			$filename = $_FILES['file']['name'];
			$file_extension = pathinfo($filename, PATHINFO_EXTENSION);
			$allowed_extensions =  array('jpg','gif','png');
			$n_publications_categories_mappings_id = $_POST['n_publications_categories_mappings_id'];
			$image_name = $n_publications_categories_mappings_id.'.' . pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION);
			//error_log("Ajax::category_mapping_image_upload 3");
			
			if($filename > '' && $n_publications_categories_mappings_id > 0){
				$sourcePath = $_FILES['file']['tmp_name'];  
				$targetPath = "repository/categories-mappings/".$image_name; // Path di je spremljena slika
				$path = "repository/categories-mappings/".$image_name;	
				//error_log("Ajax::category_mapping_image_upload 4");
				
			
				if (file_exists($path)) {
    				unlink($path);		
					// echo "file already exsists";
				}
			
				//error_log("Ajax::category_mapping_image_upload 6");
				if(!in_array($file_extension, $allowed_extensions) ) {
					//error_log("Ajax::category_mapping_image_upload 7");
					echo $file_extension." is not a valid extension, you can only upload jpg,png and gif extension";
					return;
				}else{
					if(move_uploaded_file($sourcePath,$targetPath)){
						//error_log("Ajax::category_mapping_image_upload 8");
						Nava::runSql("UPDATE n_publications_categories_mappings SET photo_url = '$path' WHERE id = $n_publications_categories_mappings_id");
						// echo "slika uspješno uploadan-a";
						echo "<img src=\'/$path\' id='preview' style='width:100%;'/>";
						// echo $_SERVER['DOCUMENT_ROOT'];
						//error_log("Ajax::category_mapping_image_upload 9");
						return;
					}
				}
			}else{
				//error_log("Ajax::category_mapping_image_upload 10");
				echo 'ne postoji file ili publications_categories_mappings_id';
				return;
			}	
		}
	}
	
	
	public function imageUploadAction (){
        //error_log("Ajax::pictureUpload 1");
        if ($this->request->isAjax()){
			//error_log("Ajax::pictureUpload 2");
			
			$image_name = $_FILES['file']['name'];
			$file_extension = pathinfo($image_name, PATHINFO_EXTENSION);
			$allowed_extensions =  array('jpg','gif','png');
			
			if($_POST['ad_id']){
				if($_POST['ad_id'] > 0){
					$ad_id = $_POST['ad_id'];
					//error_log("Ajax::pictureUpload 3");
				}
			}
				
			if($image_name > '' && $ad_id > 0){
				$image_name = $ad_id.'.' . pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION);
				$sourcePath = $_FILES['file']['tmp_name'];  
				$targetPath = "repository/dtp/originals/".$image_name; // Path di je spremljena slika
				$targetPath2 = "repository/dtp/to_process/".$image_name;
				$real_path = "repository/dtp/originals/".$image_name;	
				$paths = array($targetPath, $targetPath2);
				//error_log("Ajax::pictureUpload 4");
				
				foreach($paths as $path){
					if (file_exists($path)) {
						unlink($path);		
						//error_log("Ajax::pictureUpload 5");
	// 					echo "file already exsists";
					}
				}
				//error_log("Ajax::pictureUpload 6");
				if(!in_array($file_extension, $allowed_extensions) ) {
					//error_log("Ajax::pictureUpload 7");
					echo $file_extension." is not a valid extension, you can only upload jpg extension";
					return;
				}else{
					if(move_uploaded_file($sourcePath,$targetPath)){
						//error_log("Ajax::pictureUpload 8");
						copy($targetPath, $targetPath2);
						Nava::runSql("UPDATE ads SET n_picture_path = '$real_path' WHERE id = $ad_id");
						// echo "slika uspješno uploadan-a";
						echo "<img src=\"/$real_path\" id='uploaded_preview' style='width:100%;'/>";
						// echo $_SERVER['DOCUMENT_ROOT'];
						//error_log("Ajax::pictureUpload 9");
						return;
					}
				}
			}elseif($image_name > ''){
				$sourcePath = $_FILES['file']['tmp_name'];  
				$targetPath = "repository/dtp/originals/".$image_name; // Path di je spremljena slika
				$targetPath2 = "repository/dtp/to_process/".$image_name;
				$real_path = "repository/dtp/originals/".$image_name;	
				// error_log("Ajax::pictureUpload 4");
					if(!in_array($file_extension, $allowed_extensions) ) {
						// error_log("Ajax::pictureUpload 7");
						echo $file_extension." is not a valid extension, you can only upload jpg extension";
						return;
					}else{
						if(move_uploaded_file($sourcePath,$targetPath)){
							copy($targetPath, $targetPath2);
							// error_log("Ajax::pictureUpload 8");
							return;
						}
					}
			}else{				
				//error_log("Ajax::pictureUpload 1");
				echo 'ne postoji file ili ad_id';
				return;				
			}
				
				
		}
    }
	
	public function removeUploadedImageAction (){
		//error_log('AjaxController::removeUploadedImageAction 1');
        if ($this->request->isAjax()){
			//error_log('AjaxController::removeUploadedImageAction 2');
			$ad_id = $_POST['ad_id'];
			$ad = \Baseapp\Suva\Models\Ads::findFirst($ad_id);
			if($ad->n_picture_path){ 
			//error_log($ad->n_picture_path);
			//error_log('AjaxController::removeUploadedImageAction 3');
				$str = explode("/", $ad->n_picture_path);
				$ad_image = $str[3];
			//error_log($ad_image);
				$original_image = "repository/dtp/originals/$ad_image";
				$processed_image = "repository/dtp/to_process/$ad_image";
				$files = array($original_image, $processed_image);
					
				if($ad_id){
					//error_log('AjaxController::removeUploadedImageAction 4');
					foreach ($files as $file) {
						if(!unlink($file)){	
							echo "file nije uspješno izbrisan, pokušajte ponovo";
							//error_log('AjaxController::removeUploadedImageAction 5');
							return;
						}else{
							Nava::runSql("UPDATE ads SET n_picture_path = '' WHERE id = $ad_id");
							//error_log('AjaxController::removeUploadedImageAction 6');
							echo 'picture removed';
						}
					}
				}
			}
		}
    }
}
