<?php

namespace Baseapp\Suva\Controllers;

class FiscalLocationsController extends IndexController{

    public function indexAction() {
			$this->tag->setTitle("Fiscal Locations");
			$this->n_query_index("FiscalLocations"); //ovo je definirano u BaseController
    }

		public function crudAction($pEntityId = null) {
			$this->n_crud_action("FiscalLocations", $pEntityId); //ovo je definirano u BaseController
			$this->view->pick('fiscal-locations/crud');
		}
}