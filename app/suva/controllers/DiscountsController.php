<?php

namespace Baseapp\Suva\Controllers;

class DiscountsController extends IndexController{

    public function indexAction() {
		//$this->printr($_REQUEST, "DiscountsController::indexAction REQUEST");
		//$this->printr($_SESSION['_context'], "DiscountsController::indexAction REQUEST");
		// \Baseapp\Suva\Library\Nava::sysMessage('test');
		$_SESSION['_context']['step_rec'] = 20;
		$this->tag->setTitle("Discounts");
		$this->n_query_index("Discounts"); //ovo je definirano u BaseController
    }

	public function crudAction($pEntityId = null) {
		//$this->printr($_REQUEST);
		$this->view->pick('discounts/crud');
		$this->n_crud_action("Discounts", $pEntityId); //ovo je definirano u BaseController
	}
}
