<?php

namespace Baseapp\Suva\Controllers;

class PublicationsController extends IndexController{

    public function indexAction() {
		//$this->poruka("publications index");
		//$this->printr($_REQUEST, "PublicationsController: Request");
		//$this->printr($_SESSION['_context'], "PublicationsController: CONTEXT");
		//error_log("PublicationsController: REQUEST:".print_r($_REQUEST,true));
		//error_log("PublicationsController: CONTEXT:".print_r($_SESSION['_context'],true));
		$this->tag->setTitle("Publications");
		$this->n_query_index("Publications"); //ovo je definirano u BaseController
    }

	public function crudAction($pEntityId = null) {
		//$this->printr($_REQUEST, "PublicationsController: Request");
		//$this->printr($_SESSION['_context'], "PublicationsController: CONTEXT");
		
		//ovo služi da bi se kasnije koristilo za botune Category Mappings...
		//provjerava se u PublicationsCategoriesMappingsController: indexAction
		if ($pEntityId) $_SESSION['_context']['_default_publication_id'] = $pEntityId;
			
		//error_log("PublicationsController ENTITY ID : ".$pEntityId);
		
		// if($pEntityId) {
		$this->n_crud_action("Publications", $pEntityId); //ovo je definirano u BaseController
			
		// } else {
			// $this->n_crud_action("Publications", $pEntityId); //ovo je definirano u BaseController
		// }
		
		$this->view->pick('publications/crud');
		
		$name = \Baseapp\Suva\Models\Publications::findFirst($pEntityId)->name;
		if ($pEntityId) $this->tag->setTitle("PUB $name");
	}
	
	public function ajaxUpdateForwardCategoryAction($pPublicationId = null, $pCategoryId = null, $pForwardCategoryId = null){
		//error_log("PublicationsController:ajaxUpdateForwardCategoryAction 1");
		
		$response = '';
		
		$publication = $pPublicationId > 0 ? \Baseapp\Suva\Models\Publications::findFirst($pPublicationId) : null ;
		if (!$publication) $response .= 'NOK - invalid publication ID'.PHP_EOL;
		
		$category = $pCategoryId > 0 ? \Baseapp\Suva\Models\Categories::findFirst($pCategoryId) : null ;
		if (!$category) $response .= 'NOK - invalid category ID'.PHP_EOL;
		
		$forwardCategory = $pForwardCategoryId > 0 ? \Baseapp\Suva\Models\Categories::findFirst($pForwardCategoryId) : null;
		if (!$forwardCategory) $response .= 'NOK - invalid category ID'.PHP_EOL;

		$entity = \Baseapp\Suva\Models\PublicationsCategories::findFirst("publication_id = $publication->id and category_id = $category->id");
		
		if ($response === '') {
			//error_log("PublicationsController:ajaxUpdateForwardCategoryAction 2");
			if ($entity && $category->id === $forwardCategory->id){
				//error_log("PublicationsController:ajaxUpdateForwardCategoryAction 3");
				if ($entity->delete() === false) {
					//error_log("PublicationsController:ajaxUpdateForwardCategoryAction 4");
					$response .= 'NOK - errors while deleting:';
					$greske = $entity->getMessages();
					foreach ($greske as $greska)  
						$response .= $greska.", ";
				}
				else $response = 'OK';
			}
			elseif (!$entity && $category->id !== $forwardCategory->id){
				//error_log("PublicationsController:ajaxUpdateForwardCategoryAction 5");
				//ako su različiti onda se to snima u bazu ili update-a
				$entity = new \Baseapp\Suva\Models\PublicationsCategories();
				$entity->publication_id = $publication->id;
				$entity->category_id = $category->id;
				$entity->forward_category_id = $forwardCategory->id;
				if ($entity->create() === false) {
					//error_log("PublicationsController:ajaxUpdateForwardCategoryAction 6");
					$response .= 'NOK - errors while saving:';
					$greske = $entity->getMessages();
					foreach ($greske as $greska)  
						$response .= $greska.", ";
				}
				else $response = 'OK';
			}
			elseif ($entity && $category->id !== $forwardCategory->id){
				//error_log("PublicationsController:ajaxUpdateForwardCategoryAction 7");
				//ako već postoji i treba mu izmijenit forward category
				$entity->forward_category_id = $forwardCategory->id;
				if ($entity->update() === false) {
					//error_log("PublicationsController:ajaxUpdateForwardCategoryAction 8");
					$response .= 'NOK - errors while saving:';
					$greske = $entity->getMessages();
					foreach ($greske as $greska)  
						$response .= $greska.", ";
				}
				else $response = 'OK';
			}
			else $response = 'NOK - ne bi trebalo biti ovdje';
		}
		//error_log("PublicationsController:ajaxUpdateForwardCategoryAction 9 response: $response");
		
		return 'dddd';
		$this->disable_view();
		$this->view->disable();
//         $this->response->setJsonContent(array($response));
		$this->response->setContent($response);
		$this->response->send();
		
        return $this->response;
	}

	public function createDefaultCategoryTreeAction ( $pPublicationId = null ){
		
		$publication = null;
		if ( $pPublicationId > 0 ){
			$publication = \Baseapp\Suva\Models\Publications::findFirst($pPublicationId);
		} else return $this->greska ("Publication ID not supplied");
		
		
		if (!$publication){
			// //error_log("PublicationsController: createDefaultCategoryTreeAction: 1 Publication ID $pPublicationId not found ");
			return $this->greska (" Publication ID $pPublicationId not found ");
		} 
		
		$rootCategory = \Baseapp\Suva\Models\CategoriesMappings::findFirst(" parent_id is null and n_publications_id = $pPublicationId ");
		
		if ( $rootCategory ){
			//error_log( "PublicationsController: createDefaultCategoryTreeAction: 2 Default root category for publication $publication->name already exists. ");
			return $this->poruka("Default root category for publication $publication->name already exists.");
		} 
		
		//$this->poruka ("OK");
		
		$rootOrigId = 1;
		$rootCopyId = $this->copyCategory($rootOrigId , null, $pPublicationId );
		// if ($rootCopyId) error_log ("PublicationsController: createDefaultCategoryTreeAction: root copy not made.");
		// \Baseapp\Suva\Library\Nava::runSql("update categories set name = '[$publication->slug]' where id = $rootCopyId");
		
		
		$this->r_copyChildren( $rootOrigId, $rootCopyId, $pPublicationId );
		// //error_log("PublicationsController: createDefaultCategoryTreeAction: 3 ");
		
		$this->dispatcher->forward(
			array(
				"controller" => "categories-mappings",
				"action"     => "index",
				"params"     => array( $pPublicationId )
			)
		);
	}

	public function r_copyChildren ( $pParentOrigId = null, $pParentCopyId = null, $pPublicationId = null  ){
		$parentOrig = \Baseapp\Suva\Models\Categories::findFirst($pParentOrigId);
		$parentCopy = \Baseapp\Suva\Models\Categories::findFirst($pParentCopyId);
		
		if (!$parentOrig) {
			//error_log("PublicationsController: r_copyChildren parentOrig not found");
			return $this->greska("Parent ID not supplied");
		}
		$children = \Baseapp\Suva\Models\Categories::find("parent_id = $parentOrig->id");
		foreach ($children as $child){
			$copyId = $this->copyCategory($child->id , $pParentCopyId, $pPublicationId);
			//if (!$copyId ) //error_log("PublicationsController: r_copyChildren 2 category $child_id not copied");
			$this-> r_copyChildren ( $child->id, $copyId, $pPublicationId );
		} 
	}

	public function copyCategory ( $pOriginalCategoryId = null, $pParentId = null, $pPublicationId = null ){
		//error_log ("PublicationsController: copyCategory 1");
		$orig = \Baseapp\Suva\Models\Categories::findFirst($pOriginalCategoryId);
		$copy = new \Baseapp\Suva\Models\CategoriesMappings();

		//ovo dole ne radi, postojeće funkcije za snimanje 
		//koriste samo request pa se nisu mogla pozvati odavde
		
		$copy->assign($orig->toArray());
		$copy->id = null;
		$copy->parent_id = $pParentId;
		$copy->url = null;
		$copy->n_publications_id = $pPublicationId;
		$copy->n_original_category_id = $pOriginalCategoryId;
		$success = $copy->create();
		//if (!$success) error_log ("PublicationsController: copyCategory 2 COPY ERROR");
		return $copy->id;
	}

	public function createDefaultCategoryMappingsAction ( $pPublicationId = null ){
		//WARNING!!! ovo je zapravo PublicationsCategoriesMappings a ne CategoriesMappings
		
		$publication = null;
		//error_log("PublicationsController.php: createDefaultCategoryMappingsAction 1 ");
		if ( $pPublicationId > 0 ){
			$publication = \Baseapp\Suva\Models\Publications::findFirst($pPublicationId);
		} else return $this->greska("Publication ID not supplied");
		
		$countExistingMappings = \Baseapp\Suva\Models\PublicationsCategoriesMappings::find("publication_id = $publication->id")->count();
		// if ($countExistingMappings > 0) return $this->greska("Mappings already exist");
		
		//error_log("PublicationsController.php: createDefaultCategoryMappingsAction 2 ");
		
		//mapiranje prema n_path
		$categoriesMappings = \Baseapp\Suva\Models\CategoriesMappings::find(" n_publications_id = $pPublicationId ");
		foreach ($categoriesMappings as $categoryMapping){
			//error_log("PublicationsController.php: createDefaultCategoryMappingsAction 3 ");
			if ($categoryMapping->n_path){
				//error_log("PublicationsController.php: createDefaultCategoryMappingsAction 1 ");
				$mappedCategory = \Baseapp\Suva\Models\Categories::findFirst("n_path = '$categoryMapping->n_path'");
				if ($mappedCategory){
					//error_log("PublicationsController.php: createDefaultCategoryMappingsAction 4 ");
					// $existingPCM = \Baseapp\Suva\Models\PublicationsCategoriesMappings::findFirst("mapped_category_id = $mappedCategory->id");
					// if($existingPCM){
						// return;
					// }
					$newPCM = new \Baseapp\Suva\Models\PublicationsCategoriesMappings();
					
					$newPCM->publication_id = $publication->id;
					$newPCM->category_mapping_id = $categoryMapping->id;
					$newPCM->mapped_category_id = $mappedCategory->id;
					
					$success = $newPCM->create();
					if (!$success){ 	
						//error_log("PublicationsController.php: createDefaultCategoryMappingsAction 5 ERRORS:  ".print_r($newPCM->getMessages(), true));
					}
					else{ 
						//error_log("PublicationsController.php: createDefaultCategoryMappingsAction 6 success: $success");
					}
				}
			}
		}		
	}
	
	public function refreshAdsSearchWordsAction ( $pPublicationId = null ){
		$publication = null;
		//error_log("PublicationsController.php: createDefaultCategoryMappingsAction 1 ");
		if ( $pPublicationId > 0 ){
			$publication = \Baseapp\Suva\Models\Publications::findFirst($pPublicationId);
		} else return $this->greska ("Publication ID not supplied");
		$this->poruka("Calling function to update ads search words");
		\Baseapp\Suva\Library\libAdsSearchWords::updateAdsSearchWordsForPublication ( $pPublicationId );
		
		$this->view->pick("publications/crud");
		$this->view->setVar("entity", $publication);
	}

	public function createDefaultProductsCategoriesPricesAction( $pPublicationId = null ){
		
		// //error_log('PublicationsController::createDefaultProductsCategoriesPricesAction 1');
		$cms = \Baseapp\Suva\Models\CategoriesMappings::find (" n_publications_id = $pPublicationId ");
		$products = \Baseapp\Suva\Models\Products::find (" publication_id = $pPublicationId ");
		
		// //error_log('PublicationsController::createDefaultProductsCategoriesPricesAction 2');
		foreach ( $cms as $cm ){
			// //error_log('PublicationsController::createDefaultProductsCategoriesPricesAction 3');
			foreach ( $products as $product ){
				// //error_log('PublicationsController::createDefaultProductsCategoriesPricesAction 4');
				$pcpOld = \Baseapp\Suva\Models\ProductsCategoriesPrices::find (" categories_mappings_id = $cm->id and products_id = $product->id ");
				// //error_log('PublicationsController::createDefaultProductsCategoriesPricesAction 5');
				
				if ( count($pcpOld) == 0 ){
					// //error_log('PublicationsController::createDefaultProductsCategoriesPricesAction 6');
					$pcpNew = new \Baseapp\Suva\Models\ProductsCategoriesPrices();
					$pcpNew->categories_mappings_id = $cm->id;
					$pcpNew->products_id = $product->id;
					$pcpNew->publication_id = $pPublicationId;
					// //error_log($pcpNew->categories_mappings_id);
					// //error_log($pcpNew->products_id );
					$pcpNew->create();
					//\Baseapp\Suva\Library\Nava::runSql (" insert into n_products_categories_prices ( categories_mappings_id, products_id ) values (  $cm->id, $product->id  )");
					// //error_log('PublicationsController::createDefaultProductsCategoriesPricesAction 7');
				}
			}
		}
	}

	public function createBlankCategoryMappingAction( $pPublicationId = null ){
		
		if ( !$pPublicationId > 0 ) return;
		$publication = \Baseapp\Suva\Models\Publications::findFirst( $pPublicationId );
		if ( !$publication ) return;

		//kopira se s root kategorije
		$orig = \Baseapp\Suva\Models\CategoriesMappings::findFirst(" n_publications_id = $pPublicationId and parent_id is null ");
		$copy = new \Baseapp\Suva\Models\CategoriesMappings();

		//ovo dole ne radi, postojeće funkcije za snimanje 
		//koriste samo request pa se nisu mogla pozvati odavde
		
		$copy->assign($orig->toArray());
		$copy->id = null;
		$copy->parent_id = null;
		$copy->url = null;
		$copy->n_publications_id = $pPublicationId;
		$copy->n_original_category_id = null;
		$copy->name = 'ENTER NAME';
		$copy->n_path = null;
		$success = $copy->create();
		if ($success){
			$this->poruka("A blank category for publication $publication->name (ID:$copy->id) created");
			$this->redirect_back();
		}
		else {
			$greske = $copy->getMessages();
				foreach ($greske as $greska)  
					$err_msg .= $greska.", ";
			$this->sysMessage("Errors creating default category for publication $publication->name, $err_msg");
			$this->redirect_back();
		}
	}
	
}