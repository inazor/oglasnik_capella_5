<?php

namespace Baseapp\Suva\Controllers;

class ProductsController extends IndexController{

    public function indexAction() {
		//error_log ("ProductsController:: IndexAction 1");
		//$this->printr($_REQUEST, "ProductsController:indexAction, request");
		$this->tag->setTitle("Products");
		
		$publicationId = null;
		if (array_key_exists('default_filter', $_SESSION['_context']))
			if (array_key_exists('publication_id', $_SESSION['_context']['default_filter']))
				$publicationId = $_SESSION['_context']['default_filter']['publication_id'];
		
		if ($publicationId){
			$publication = \Baseapp\Suva\Models\Publications::findFirst( $publicationId );
			if ($publication){
				$this->tag->setTitle("PRO for $publication->name");
				
				//Postavljanje defaultnog filtera
				if (!array_key_exists('default_filter', $_SESSION['_context'])) $_SESSION['_context']['default_filter'] = array();
				$_SESSION['_context']['default_filter']['publication_id'] = $publication->id;
				
//				if (!array_key_exists('order_by', $_SESSION['_context'])) $_SESSION['_context']['order_by'] = array();
//				if (!array_key_exists('id', $_SESSION['_context']['order_by'])) $_SESSION['_context']['order_by']['id'] = 'desc';

			}
		}
		$_SESSION['_context']['step_rec'] = 50;
		$this->n_query_index("Products"); //ovo je definirano u BaseController
		
		
    }

	public function crudAction($pEntityId = null) {
		$this->view->pick('products/crud');
		$this->n_crud_action("Products", $pEntityId); //ovo je definirano u BaseController
		
	}
	
	public function deleteAction( $pModelName= null, $pId = null) {
		$this->n_delete_action ($pModelName, $pId);
		
		$this->view->pick('products/index');
		
		
	}	
	
}