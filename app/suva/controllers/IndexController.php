<?php

namespace Baseapp\Suva\Controllers;

use Baseapp\Suva\Controllers\BaseSuvaController;
use Baseapp\Library\Reporting;
use Baseapp\Library\Moderation;
use Baseapp\Library\Utils;
use Baseapp\Suva\Models\Ads;
use Baseapp\Suva\Models\Orders;
use Baseapp\Suva\Models\FiscalLocations;
use Baseapp\Traits\CrudActions;
use Baseapp\Traits\CrudHelpers;
use Baseapp\Suva\Library\Nava;



/**
 * Suva Index Controller
 */
class IndexController extends BaseSuvaController
{   
    
    use CrudActions;
    use CrudHelpers;
    
    public $crud_model_class = 'Baseapp\Suva\Models\Index';
	
	// DODANO NIKOLA RADI PREBACIVANJA AJAXCONTR U SUVA/AJAXCONTR TEST
	// public function afterExecuteRoute(Dispatcher $dispatcher) {
		
		 // // Making sure the parent is called too!
        // parent::afterExecuteRoute($dispatcher);
		
	// }
    
    
    public function indexAction()
    {
        // $_SESSION['_context']['zadnje_vrijeme'] = date('h:i:s');
		// NIKOLA DODANO RADI LINKA USER KOJI TREBA VODIT NA MOJ-KUTAK
		
		if (method_exists($this, 'setLayoutFeature')) {
            // Homepage must have search box shown and no toggler present at all
            $this->setLayoutFeature('full_page_width', true);
            $this->setLayoutFeature('search_bar', true);
            $this->setLayoutFeature('mobile_search_bar_toggler', false);
            $this->setLayoutFeature('mobile_search_bar_visible_initially', true);
        }
		
		
		//$this->printr($_SESSION['_permissions']);
		$reporting = new Reporting();
        $new_orders = Orders::count('status = ' . Orders::STATUS_NEW);

        $ts = $this->getTodayTimestampRange();
        //error_log(\Phalcon\Version::get());
    

		//BORNA ZAKOMENTIRANO JER USPORAVA;
        // $topCounter = array(
            // 'total_ads_today' => Ads::count('created_at >= ' . $ts['start'] . ' AND created_at <= ' . $ts['end']),
            // 'orders' => $new_orders,
            // 'reported_ads' => $reporting->getAdsCountByModerationStatus(Moderation::MODERATION_STATUS_REPORTED),
            // 'paid_waiting_for_moderation' => $reporting->getModerationWaitingCount(Ads::PAYMENT_STATE_PAID),
            // 'ordered_waiting_for_moderation' => $reporting->getModerationWaitingCount(Ads::PAYMENT_STATE_ORDERED),
            // 'free_waiting_for_moderation' => $reporting->getModerationWaitingCount(Ads::PAYMENT_STATE_FREE)
        // );

//         $this->view->pick('index/index');
        $this->tag->setTitle('Admin panel');
        $this->assets->addCss('assets/vendor/sb-admin-2/css/plugins/morris.css');
        $this->assets->addJs('assets/vendor/sb-admin-2/js/plugins/morris/raphael.min.js');
        $this->assets->addJs('assets/vendor/sb-admin-2/js/plugins/morris/morris.min.js');
        $this->assets->addJs('assets/suva/js/statistics-index.js');
        // $this->view->setVar('topCounter', $topCounter);
        $this->view->setVar('registered_users_by_month', $reporting->getRegisteredUsers());
        $this->view->setVar('fiscal_locations', Nava::arrToDict(FiscalLocations::find()->toArray()));
		
		
		// Nava::poruka("Vrijeme proslo".$_SESSION['_context']['zadnje_vrijeme']);		
		// Nava::poruka("vrijeme sada ".date('h:i:s'));
		
    }
    
    
   
    
    

    protected function getTodayTimestampRange()
    {
        $now  = Utils::getRequestTime();
        $date = new \DateTime();
        $date->setTimestamp($now);

        $date->setTime(0, 0, 0);
        $today_start_ts = $date->getTimestamp();

        $date->setTime(23, 59, 0);
        $today_end_ts = $date->getTimestamp();

        return array(
            'start' => $today_start_ts,
            'end'   => $today_end_ts
        );
    }
    
    
   
    
}
