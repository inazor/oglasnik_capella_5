<?php

namespace Baseapp\Suva\Controllers;

class DiscountsPublicationsController extends IndexController{

    public function indexAction() {
		//$this->printr($_SESSION['_context']);
		$_SESSION['_context']['step_rec'] = 20;
		$this->tag->setTitle("Discounts on Publications");
		$this->n_query_index("DiscountsPublications"); //ovo je definirano u BaseController
    }

	public function crudAction($pEntityId = null) {
		//$this->printr($_REQUEST);
		$this->view->pick('discounts-publications/crud');
		$this->n_crud_action("DiscountsPublications", $pEntityId); //ovo je definirano u BaseController
	}
}
