<?php

namespace Baseapp\Suva\Controllers;

use Phalcon\Dispatcher;
use Baseapp\Extension\Tag;
use Baseapp\Library\Tool;
use Baseapp\Library\Utils;
use Baseapp\Suva\Models\Roles;
use Baseapp\Suva\Models\Locations;
use Baseapp\Traits\CrudActions;
use Baseapp\Traits\CrudHelpers;
use Baseapp\Traits\ParametrizatorHelpers;
use Phalcon\Paginator\Adapter\QueryBuilder;

use Baseapp\Suva\Library\Nava;
use Baseapp\Suva\Library\libAVUS;

use Baseapp\Suva\Models\Publications;
use Baseapp\Suva\Models\Products;

use Phalcon\Mvc\View;
use Phalcon\Db\Adapter\Pdo; 

class ObradeController extends IndexController{
    use CrudActions;
    use CrudHelpers;

  
    public $ime_foldera = "obrade";
    public $ime_objekta = "obrade";
    

    public function indexAction() {
        

    }

    public function updateNpathsAction() {
        $dictionaries = \Baseapp\Suva\Models\Dictionaries::find();
		foreach ($dictionaries as $dictionary){
			$path = $dictionary->getPath();
			$path = str_replace("'" , "", $path);
			//($dictionary->path());
			
			//$dictionary->update(); //ne radi jer se na modelu Dictionaries koristi NestedSetBehavoiur
			$txtSql = "update dictionaries set n_path = '$path' where id = $dictionary->id";
			//error_log("ObradeController: updateDictionaryPathsAction: txtSql:".$txtSql);
			if ( $dictionary->n_path != $path )
				Nava::runSql ($txtSql);
		}
		$this->poruka("Dictionaries updated");
		
		
		
		//error_log("ActionsController:obradaAction 11");
		foreach ( \Baseapp\Suva\Models\Locations::find() as $loc ){
			$path = $loc->getPath();
			$path = str_replace("'" , "", $path);
			//error_log("ActionsController:obradaAction 12 $path");
			if ( $loc->n_path != $path )
				Nava::runSql ("update location set n_path = '$path' where id = $loc->id");
			
			//$dict->update();
		} 
		$this->poruka("Locations updated");

		//error_log("ActionsController:obradaAction update categories path");
		foreach ( \Baseapp\Suva\Models\Categories::find() as $cat ){
			$path = $cat->getPath();
			$path = str_replace("'" , "", $path);
			//error_log("ActionsController:obradaAction 12 $path");
			if ( $cat->n_path != $path )
				Nava::runSql ("update categories set n_path = '$path' where id = $cat->id");
		} 
		$this->poruka("Categories updated");
		

		// //error_log("ActionsController:obradaAction update categories path");
		// foreach ( \Baseapp\Suva\Models\CategoriesMappings::find() as $cat ){
			// $path = $cat->path();
			// $path = str_replace("'" , "", $path);
			// //error_log("ActionsController:obradaAction 12 $path");
			// if ( $cat->n_path != $path )
				// Nava::runSql ("update n_categories_mappings set n_path = '$path' where id = $cat->id");
		// } 
		// $this->poruka("Categories Mappings updated");

		foreach ( \Baseapp\Suva\Models\CategoriesMappings::find() as $cat ){
			$cat->save();
			
			// $path = $cat->path();
			// $path = str_replace("'" , "", $path);
			// //error_log("ActionsController:obradaAction 12 $path");
			// if ( $cat->n_path != $path )
				// Nava::runSql ("update n_categories_mappings set n_path = '$path' where id = $cat->id");
		} 
		$this->poruka("Categories Mappings updated");

		
		$this->view->pick("obrade/index");
		

    }
	
	public function updateUsersContactsAction(){
		$this->view->pick('obrade/index');
		
		//sub_type = '1', '2' za polja koja se refreshaju na temelju tablice users
		//phone1 - subtype=1, phone2 - subtype=2
		
		
		//username
		$txtSql = "delete from n_users_contacts where type = 'phone' and sub_type = '1'";
		Nava::runsql($txtSql);
		$txtSql = $this->sqlInsertUsersContacts( 'username', '1', 'u.username', 1);
		Nava::runsql($txtSql);
		$this->poruka("Username refreshed");
		
		//email
		$txtSql = "delete from n_users_contacts where type = 'email' and sub_type = '1'";
		Nava::runsql($txtSql);
		$txtSql = $this->sqlInsertUsersContacts( 'email', '1', 'u.email', 0);
		Nava::runsql($txtSql);
		$this->poruka("E-mail refreshed");

		//phone1
		$txtSql = "delete from n_users_contacts where type = 'phone' and sub_type = '1'";
		Nava::runsql($txtSql);
		$txtSql = $this->sqlInsertUsersContacts( 'phone', '1', 'u.phone1', 0);
		Nava::runsql($txtSql);
		$this->poruka("Phone 1 refreshed");

		//phone2
		$txtSql = "delete from n_users_contacts where type = 'phone' and sub_type = '2'";
		Nava::runsql($txtSql);
		$txtSql = $this->sqlInsertUsersContacts( 'phone', '2', 'phone2', 0);
		Nava::runsql($txtSql);
		$this->poruka("Phone 2 refreshed");

		//company_name
		$txtSql = "delete from n_users_contacts where type = 'companyname' and sub_type = '1'";
		Nava::runsql($txtSql);
		$txtSql = $this->sqlInsertUsersContacts( 'companyname', '1', 'u.company_name', 1);
		Nava::runsql($txtSql);
		$this->poruka("Company Name refreshed");
		
		//first and last name
		$txtSql = "delete from n_users_contacts where type = 'first_lastname' and sub_type = '1'";
		Nava::runsql($txtSql);
		$txtSql = $this->sqlInsertUsersContacts( 'first_lastname', '1', "concat(u.first_name, ' ', u.last_name)", 1);
		Nava::runsql($txtSql);
		$this->poruka("First and last name refreshed");
		
		//vat No.
		$txtSql = "delete from n_users_contacts where type = 'vat' and sub_type = '1'";
		Nava::runsql($txtSql);
		$txtSql = $this->sqlInsertUsersContacts( 'vat', '1', "u.oib", 1);
		Nava::runsql($txtSql);
		$this->poruka("VAT No. refreshed");

		
		//updateanje novo dodanih kontakata, dodavanje search stringa
		$ucs = \Baseapp\Suva\Models\UsersContacts::find(" sub_type is null ");
		foreach ( $ucs as $uc ){
			$uc->search_string = $uc->makeSearchString();
			$uc->update();
		}
		$this->poruka("Additional user info search string updated (".count($ucs)." items )");
		
	}
	
	public function updateCategoriesNpathAction(){

		foreach ( \Baseapp\Suva\Models\Categories::find() as $cat){
			$cat->n_path = $cat->getPath();
			$txtSql = "update categories set n_path = '".$cat->getPath( ' ' )."' where id = $cat->id";
			\Baseapp\Suva\Library\Nava::runSql($txtSql);
		}

		
	}
	
	public function sqlInsertUsersContacts( $pType = null, $pSubType = null, $pName = null, $pHidden = null ){
		
 
		$lName = "$pName";
		$lType = "'$pType'";
		$lSubType = $pSubType ? "'$pSubType'" : 'null';
		$lHidden = $pHidden ? "$pHidden" : "0";
		$lSearch = " lower(replace(replace(replace($pName, '-', ''), '/', ''), ' ', '')) ";
		
		$txtSql = "  insert into n_users_contacts (name, type, sub_type, user_id, search_string, is_hidden) "
		." select "
		." $lName, $lType, $lSubType, u.id, $lSearch, $lHidden "
		." from "
		."	users u "
		." where length($pName) > 2 ";
		//error_log ($txtSql);
		return $txtSql;
	}
	
	public function updateOrderTotalsAction(){
		$appExecutionTime = ini_get('max_execution_time');
		ini_set('max_execution_time', 150000);
		ini_set ('request_terminate_timeout', 150000);
		
		foreach( \Baseapp\Suva\Models\Orders::find( array("n_total > 0 and total = '0'", 'order' => 'id desc' )) as $order ){
			Nava::updateOrderTotal($order->id);
		}
		$this->view->pick("obrade/index");
		$this->poruka("Order totals updated");
		
		ini_set('max_execution_time', $appExecutionTime);
		ini_set('request_terminate_timeout', $appExecutionTime);
		
	}

	
	public function syncAvusAdsWithRemainingInsertionsAction(){
		
		$_context = $_SESSION['_context'];
		$isThreaded = array_key_exists('sync_avus_is_threaded', $_context) && $_context['sync_avus_is_threaded'] > 0 ? true : false;
		
		
		if ( $isThreaded ){
			
			//Ova akcija se izvršava u threaded - pozove se php funkcija koja poziva akciju, akcija je složena da uzima varijable preko rute, a ne iz konteksta
			
			error_log('is_threaded');
			$sessionId = session_id();
			$_SESSION['_context']['_is_threaded_execution'] = true;
			
			$strExec = "/usr/bin/php -q /home/oglasnik2/oglasnik.hr/current/vendor/pokreniObradu.php syncAvusAdsWithRemainingInsertionsThreaded/$sessionId > /dev/null & ";
			
			error_log ("strExec: $strExec");
			exec( $strExec );
		
			$this->view->pick('obrade/index');
			$this->redirect_to('suva/obrade');
			return;
		}

		
		
		\Baseapp\Suva\Library\libAVUS::syncAvusAdsWithRemainingInsertions();
		$this->view->pick("obrade/index");

	}
	
	public function syncAvusUsersAction(){
		\Baseapp\Suva\Library\libAVUS::syncAvusUsers();
		$this->view->pick("obrade/index");
	}
	public function syncAvusAdsDefaultWithUsersAction() {
		$this->view->pick("obrade/index");
		\Baseapp\Suva\Library\libAVUS::syncAvusAdsDefaultWithUsers();
		
	}
	
	// NIKOLA obrada koja synca avus adove s avus userima
	public function syncAvusAdsWithUsersAction() {
		$this->view->pick("obrade/index");
		\Baseapp\Suva\Library\libAVUS::syncAvusAdsWithUsers();
		
		// $avusAds = \Baseapp\Suva\Models\Ads::find("user_id = 153989 and n_source = 'avus'");
		
		// foreach ( $avusAds as $avusAd ) {
			
			// $avusUser = \Baseapp\Suva\Models\Users::findFirst("n_avus_customer_id = $avusAd->n_avus_customer_number");
			// if( $avusUser ) {
				// $avusAd->user_id = $avusUser->id ;
				// $this->poruka("Syncronized avus user $avusUser->id with Ad $avusAd->id ");
			// }
			// else {
				// $this->poruka("No Ad for Avus user $avusUser->id");
			// }
		// }
		
		// $this->view->pick("obrade/index");
		
	}
	
	// NIKOLA obrada koja synca avus adove s kategorijama - CategoriesMappings
	public function syncAvusAdsWithCategoriesAction() {
		
		// $avusAds = \Baseapp\Suva\Models\Ads::find("n_source = 'avus'");
		
		// foreach ( $avusAds as $avusAd ) {
			
			// $catMap = \Baseapp\Suva\Models\CategoriesMappings::findFirst("id = $avusAd->n_avus_class_id	");
			// if( $catMap ) {
				// $avusAd->n_category_mapping_id  = $catMap->id ;
				// $this->poruka("Syncronized avus AD $avusAd->id with category $catMap->id ");
			// }
			// else {
				// $this->poruka("Found no category for Ad $avusAd->id");
			// }
		// }
		
		$this->view->pick("obrade/index");
		\Baseapp\Suva\Library\libAVUS::syncAvusAdsWithCategories();
	}

	public function syncAvusAdsAction(){
		\Baseapp\Suva\Library\libAVUS::syncAvusAds();
		$this->view->pick("obrade/index");
	}	
	
	public function syncNewAvusAdsAction(){
		\Baseapp\Suva\Library\libAVUS::syncNewAvusAds();
		$this->view->pick("obrade/index");
	}	

	public function syncAvusInsertionsAction(){
		\Baseapp\Suva\Library\libAVUS::syncAvusInsertions();
		$this->view->pick("obrade/index");
	}

	public function avusSqlAction(){
		$_context = $_SESSION['_context'];
		
		$txtSql = array_key_exists ('obrada_sql_avus', $_context ) && strlen ( $_context['obrada_sql_avus']) > 5 ?
			$_context['obrada_sql_avus'] : null;
		\Baseapp\Suva\Library\libAVUS::runTestSql($txtSql);
		$this->view->pick("obrade/index");
		return;
		
		try {
			ini_set('display_errors', 1);
			//$this->poruka("Connecting to $navHost ..");
			// $db = new \PDO("dblib:host=$navHost;port=$navPort;dbname=$navDb", $navUser, $navPassword );
			// //$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_SILENT);// set the PDO error mode to exception 
			// $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);// set the PDO error mode to exception 
			// $this->poruka("Connected to $navHost, running SQLQuery:<pre> $txtSql</pre>"); 
			
			
			$this->poruka( "Connecting AVUS ...");
			$db = new \PDO('dblib:host=195.245.255.21;port=23458;database=avus21;tdsLevel=CS_TDS', 'blackknight', 'blackknight');
			$this->poruka("Connected successfully to AVUS, running query $txtSql"); 
			
			$stmt = $db->prepare($txtSql);

			$rs = $db->query($txtSql);
			
			//rezultat u tablicu
			//prvi red tablice - naslov
			$row = $rs->fetch();
			if ($row){
				$htmTable = "<table border = '1'><tr>";
				$pair = false;
				foreach( $row as $key => $value ){
					if ( $pair === true )
						unset ( $row[$key] );
					$pair = $pair === true ? false : true;
				}
				//$this->printr($row, "ROW 2");
				foreach( $row as $key => $value ){
					$htmTable = $htmTable."<td><b>$key</b></td>";
				}
			}
			$htmTable .="</tr>";
			foreach( $db->query($txtSql) as $row ){
				//izbacuje svaki drugi red iz recordseta, jer ih PDO duplira
				$pair = false;
				$arrKeys = array();
				foreach( $row as $key => $value ){
					if ( $pair === true )
						unset ( $row[$key] );
					$pair = $pair === true ? false : true;
				}
				
				$htmTable .= "<tr>";
				foreach( $row as $key => $value ){
					$htmTable .="<td>$value</td>";
				}
				$htmTable .="</tr>";
				$this->printr($row);
			}
			$htmTable .= "</table>";
			$this->poruka ("<b>Result:</b><br/>$htmTable");
			
			// error_log($htmTable);
		}
		catch( \PDOException $e){ 
			
			$this->greska("PDO ERROR:".$e->getMessage());
			$this->printr($db->errorInfo());
			$this->poruka ("Data retrieved so far:<br/>$htmTable</td></tr></table>");
		}
	}

	public function navisionSqlAction(){
		// $this->printr($_REQUEST, "Obrada request");
		// $this->printr($_SESSION['_context'], "Obrada context");
		// $this->poruka($_SESSION['_context']['obrada_sql']. "  Obrada context SQL");
		
		
		//NAVISION SQL SERVER POSTAVKE
		$navHost = "195.245.255.22";
		$navPort = "1433";
		$navDb = "OGLASNIK_TEST_NEW";
		$navUser = "sa";
		$navPassword = "Tassadar1337";
		
		$this->view->pick("obrade/index");
		
		$txtSql = "select 1";
		$txtSql =" select * from [Oglasnik d_o_o_".'$'."Vendor] ";
		$txtSql =" select * from [Oglasnik d_o_o_".'$'."Resource] ";
		
		/*
			select * from [Oglasnik d_o_o_$Resource] where No_ = 'S145'
			select count (*) from [Oglasnik d_o_o_$AVUS-Header]
			select count (*) from [Oglasnik d_o_o_$AVUS-Invoice]
		
		
		*/
		
		if ( array_key_exists ('obrada_sql', $_SESSION['_context'] ) && strlen ( $_SESSION['_context']['obrada_sql']) > 5) {
			$txtSql = $_SESSION['_context']['obrada_sql'];
		}
		
		try {
			ini_set('display_errors', 1);
			//$this->poruka("Connecting to $navHost ..");
			$db = new \PDO("dblib:host=$navHost;port=$navPort;dbname=$navDb", $navUser, $navPassword );
			//$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_SILENT);// set the PDO error mode to exception 
			$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);// set the PDO error mode to exception 
			$this->poruka("Connected to $navHost, running SQLQuery:<pre> $txtSql</pre>"); 
			
			
			$stmt = $db->prepare($txtSql);

			$rs = $db->query($txtSql);
			
			//rezultat u tablicu
			//prvi red tablice - naslov
			$row = $rs->fetch();
			if ($row){
				$htmTable = "<table border = '1'><tr>";
				$pair = false;
				foreach( $row as $key => $value ){
					if ( $pair === true )
						unset ( $row[$key] );
					$pair = $pair === true ? false : true;
				}
				//$this->printr($row, "ROW 2");
				foreach( $row as $key => $value ){
					$htmTable = $htmTable."<td><b>$key</b></td>";
				}
			}
			$htmTable .="</tr>";
			foreach( $db->query($txtSql) as $row ){
				//izbacuje svaki drugi red iz recordseta, jer ih PDO duplira
				$pair = false;
				$arrKeys = array();
				foreach( $row as $key => $value ){
					if ( $pair === true )
						unset ( $row[$key] );
					$pair = $pair === true ? false : true;
				}
				
				$htmTable .= "<tr>";
				foreach( $row as $key => $value ){
					$htmTable .="<td>$value</td>";
				}
				$htmTable .="</tr>";
				$this->printr($row);
			}
			$htmTable .= "</table>";
			$this->poruka ("<b>Result:</b><br/>$htmTable");
			
			// error_log($htmTable);
		}
		catch( \PDOException $e){ 
			
			$this->greska("PDO ERROR:".$e->getMessage());
			$this->printr($db->errorInfo());
			$this->poruka ("Data retrieved so far:<br/>$htmTable</td></tr></table>");
		}
	}

	public function mySqlAction(){
		$this->printr($_REQUEST, "Obrada request");
		// $this->printr($_SESSION['_context'], "Obrada context");
		// $this->poruka($_SESSION['_context']['obrada_sql']. "  Obrada context SQL");
		
		
		//NAVISION SQL SERVER POSTAVKE
		
		$this->view->pick("obrade/index");
		//return;
		
		if ( array_key_exists ('obrada_mysql', $_SESSION['_context'] ) && strlen ( $_SESSION['_context']['obrada_mysql']) > 5) {
			$txtSql = $_SESSION['_context']['obrada_mysql'];
		}
		
		//rezultat u tablicu
		//prvi red tablice - naslov
		$rs= Nava::sqlToArray($txtSql);
		if ($rs){
			$row = $rs[0];
			if ($row){
				$htmTable = "<table border = '1'><tr>";
				$pair = false;
				foreach( $row as $key => $value ){
					if ( $pair === true )
						unset ( $row[$key] );
					$pair = $pair === true ? false : true;
				}
				//$this->printr($row, "ROW 2");
				foreach( $row as $key => $value ){
					$htmTable = $htmTable."<td><b>$key</b></td>";
				}
			}
			$htmTable .="</tr>";
			foreach( $rs as $row ){
				//izbacuje svaki drugi red iz recordseta, jer ih PDO duplira
				$pair = false;
				$arrKeys = array();
				foreach( $row as $key => $value ){
					if ( $pair === true )
						unset ( $row[$key] );
					$pair = $pair === true ? false : true;
				}
				
				$htmTable .= "<tr>";
				foreach( $row as $key => $value ){
					$htmTable .="<td>$value</td>";
				}
				$htmTable .="</tr>";
				$this->printr($row);
			}
			$htmTable .= "</table>";
			Nava::poruka ("<b>Result:</b><br/>$htmTable");
		}
	}

	
	public function navisionTestFiskalAction(){
		//$this->printr($_REQUEST);
		if ( array_key_exists ( 'obrada_broj_racuna', $_SESSION['_context'] ) && strlen ( $_SESSION['_context']['obrada_broj_racuna']) > 1) {
			$orderId = intval($_SESSION['_context']['obrada_broj_racuna']);
		}else{
			Nava::greska('Unesite order id');
			$this->view->pick('obrade/index');
			return;
		}
		// $this->poruka("ORDER ID: $orderId");
		$orders = \Baseapp\Suva\Models\Orders::find("id = $orderId");
		$results_array = \Baseapp\Suva\Models\Orders::buildResultsArray($orders);
		$arrJson = \Baseapp\Suva\Library\libNavision::modifySearchResultsForJsonExport($results_array);
		// $this->printr($arrJson);
		// $objJson = json_decode( $strJson );
		// $this->printr($objJson);
		foreach( $arrJson as $orderJson ){
			Nava::poruka ("Ubacujem račun u NAV");
			\Baseapp\Suva\Library\libNavision::insertOrderInNav( $orderJson );
		}
		
		$this->view->pick('obrade/index');
	}
	
	public function navisionRemoveFiskalAction(){
		\Baseapp\Suva\Library\libNavision::removeOrderFromNav();
		
		$this->view->pick('obrade/index');
	}
	
	public function navisionGetFiskalAction($orderId = null ){
		
		//test
		if($orderId > 0){
			$orders = \Baseapp\Suva\Models\Orders::find("id in ($orderId)");
		}
		else{
			//test
			//$orders = \Baseapp\Suva\Models\Orders::find("id in (10,12)");
			//svi placeni orderi u statusu retry
			$orders = \Baseapp\Suva\Models\Orders::find("n_status = 5 and n_nav_sync_status = 'retry'");
		}
		
		
		//svi placeni orderi u statusu retry
		// $orders = \Baseapp\Suva\Models\Orders::find("n_status = 5 and n_nav_sync_status = 'retry'");
		
		foreach( $orders as $order ){
			
			\Baseapp\Suva\Library\libNavision::testFiscalization( $order->id );
		}
		
		
		$this->view->pick('obrade/index');
	}
	
	public function navisionSendFiskalAction(){
		//test
		// $orders = \Baseapp\Suva\Models\Orders::find("id in (30831)");
		 
		//svi placeni orderi u statusu unsynced
		$orders = \Baseapp\Suva\Models\Orders::find("n_status in (4,5) and n_nav_sync_status in ('unsynced')");
		
		foreach( $orders as $order ){		
			// $order = \Baseapp\Suva\Models\Orders::find("id = $order->id");
			\Baseapp\Suva\Library\libNavision::insertOrderInNav( $order );
			
		}
		
		$this->view->pick('obrade/index');
		
	}
	
	public function testSendOrdersToNavisionAction(){
		\Baseapp\Suva\Library\libNavision::sendUnfiscalizedOrdersToNav();
		$this->view->pick("obrade/index");
	}
	
	public function updateUserDefaultPaymentTypeAction() {
		$this->view->pick("obrade/index");
		
		$txtSql = "
			UPDATE users SET n_payment_type = 1 
			WHERE n_payment_type IS null 
				
		";
			
		Nava::runSql($txtSql);
		$this->poruka("Users default payment type has been updated !");
		
	}
	
	public function updateOrdersStatusesAction (){
		$this->view->pick("obrade/index");
		
		$txtSql = "
			update orders set n_status = 3 where status = 1; 
			
		";
		Nava::runSql($txtSql);
		
		$txtSql = "
			
			update orders set n_status = 5 where status = 2;
			
		";
		
		Nava::runSql($txtSql);
		
		$txtSql = "
			
			update orders set n_status = 6 where status = 4;
		";
		
		Nava::runSql($txtSql);
		$this->poruka("Orders Statuses has been updated !");
		
		
	}
	
	public function updateOrdersPbosAction (){
		$this->view->pick("obrade/index");
		
		
		$orders = \Baseapp\Suva\Models\Orders::find("n_pbo is null");
		foreach($orders as $order){
			$order->n_pbo = $order->pbo;
			$order->update();
		}
				

		$this->poruka("Orders Statuses has been updated !");
		
		
	}
	
	
	public function avusCharsetToUtf8 ($pText){
		
		$r = $pText;
		
		$r = str_replace(chr(154), chr(0xc5).chr(0xa1), $r); //š
		$r = str_replace(chr(138), chr(0xc5).chr(0xa0), $r); //Š
		$r = str_replace(chr(230), chr(0xc4).chr(0x87), $r); //ć
		$r = str_replace(chr(198), chr(0xc4).chr(0x86), $r); //Ć
		$r = str_replace(chr(232), chr(0xc4).chr(0x8d), $r); //č
		$r = str_replace(chr(200), chr(0xc4).chr(0x8c), $r); //Č
		$r = str_replace(chr(158), chr(0xc5).chr(0xbe), $r); //ž
		$r = str_replace(chr(142), chr(0xc5).chr(0xbd), $r); //Ž
		$r = str_replace(chr(128), 'EUR', $r); //€
		
		$r = str_replace(chr(240), chr(0xc4).chr(0x91), $r); //đ
		$r = str_replace(chr(208), chr(0xc4).chr(0x90), $r); //Đ
		
		$r = str_replace(chr(252),chr(0xc5).chr(0xb1), $r); //uu
		$r = str_replace(chr(220), chr(0xc5).chr(0xb0), $r); //UU

		
		return $r;
	}
	
	function avusCharsetToUtf8OLD($str) {
  	$f[]="\xc2\xac";  $t[]="\x80";
$f[]="\xd9\xbe";  $t[]="\x81";
$f[]="\xc0\x9a";  $t[]="\x82";
$f[]="\xc6\x92";  $t[]="\x83";
$f[]="\xc0\x9e";  $t[]="\x84";
$f[]="\xc0\xa6";  $t[]="\x85";
$f[]="\xc0\xa0";  $t[]="\x86";
$f[]="\xc0\xa1";  $t[]="\x87";
$f[]="\xcb\x86";  $t[]="\x88";
$f[]="\xc0\xb0";  $t[]="\x89";
$f[]="\xd9\xb9";  $t[]="\x8a";
$f[]="\xc0\xb9";  $t[]="\x8b";
$f[]="\xc5\x92";  $t[]="\x8c";
$f[]="\xda\x86";  $t[]="\x8d";
$f[]="\xda\x98";  $t[]="\x8e";
$f[]="\xda\x88";  $t[]="\x8f";
$f[]="\xda\xaf";  $t[]="\x90";
$f[]="\xc0\x98";  $t[]="\x91";
$f[]="\xc0\x99";  $t[]="\x92";
$f[]="\xc0\x9c";  $t[]="\x93";
$f[]="\xc0\x9d";  $t[]="\x94";
$f[]="\xc0\xa2";  $t[]="\x95";
$f[]="\xc0\x93";  $t[]="\x96";
$f[]="\xc0\x94";  $t[]="\x97";
$f[]="\xda\xa9";  $t[]="\x98";
$f[]="\xc4\xa2";  $t[]="\x99";
$f[]="\xda\x91";  $t[]="\x9a";
$f[]="\xc0\xba";  $t[]="\x9b";
$f[]="\xc5\x93";  $t[]="\x9c";
$f[]="\xc0\x8c";  $t[]="\x9d";
$f[]="\xc0\x8d";  $t[]="\x9e";
$f[]="\xda\xba";  $t[]="\x9f";
$f[]="\xd8\x8c";  $t[]="\xa1";
$f[]="\xda\xbe";  $t[]="\xaa";
$f[]="\xd8\x9b";  $t[]="\xba";
$f[]="\xd8\x9f";  $t[]="\xbf";
$f[]="\xdb\x81";  $t[]="\xc0";
$f[]="\xd8\xa1";  $t[]="\xc1";
$f[]="\xd8\xa2";  $t[]="\xc2";
$f[]="\xd8\xa3";  $t[]="\xc3";
$f[]="\xd8\xa4";  $t[]="\xc4";
$f[]="\xd8\xa5";  $t[]="\xc5";
$f[]="\xd8\xa6";  $t[]="\xc6";
$f[]="\xd8\xa7";  $t[]="\xc7";
$f[]="\xd8\xa8";  $t[]="\xc8";
$f[]="\xd8\xa9";  $t[]="\xc9";
$f[]="\xd8\xaa";  $t[]="\xca";
$f[]="\xd8\xab";  $t[]="\xcb";
$f[]="\xd8\xac";  $t[]="\xcc";
$f[]="\xd8\xad";  $t[]="\xcd";
$f[]="\xd8\xae";  $t[]="\xce";
$f[]="\xd8\xaf";  $t[]="\xcf";
$f[]="\xd8\xb0";  $t[]="\xd0";
$f[]="\xd8\xb1";  $t[]="\xd1";
$f[]="\xd8\xb2";  $t[]="\xd2";
$f[]="\xd8\xb3";  $t[]="\xd3";
$f[]="\xd8\xb4";  $t[]="\xd4";
$f[]="\xd8\xb5";  $t[]="\xd5";
$f[]="\xd8\xb6";  $t[]="\xd6";
$f[]="\xd8\xb7";  $t[]="\xd8";
$f[]="\xd8\xb8";  $t[]="\xd9";
$f[]="\xd8\xb9";  $t[]="\xda";
$f[]="\xd8\xba";  $t[]="\xdb";
$f[]="\xd9\x80";  $t[]="\xdc";
$f[]="\xd9\x81";  $t[]="\xdd";
$f[]="\xd9\x82";  $t[]="\xde";
$f[]="\xd9\x83";  $t[]="\xdf";
$f[]="\xd9\x84";  $t[]="\xe1";
$f[]="\xd9\x85";  $t[]="\xe3";
$f[]="\xd9\x86";  $t[]="\xe4";
$f[]="\xd9\x87";  $t[]="\xe5";
$f[]="\xd9\x88";  $t[]="\xe6";
$f[]="\xd9\x89";  $t[]="\xec";
$f[]="\xd9\x8a";  $t[]="\xed";
$f[]="\xd9\x8b";  $t[]="\xf0";
$f[]="\xd9\x8c";  $t[]="\xf1";
$f[]="\xd9\x8d";  $t[]="\xf2";
$f[]="\xd9\x8e";  $t[]="\xf3";
$f[]="\xd9\x8f";  $t[]="\xf5";
$f[]="\xd9\x90";  $t[]="\xf6";
$f[]="\xd9\x91";  $t[]="\xf8";
$f[]="\xd9\x92";  $t[]="\xfa";
$f[]="\xc0\x8e";  $t[]="\xfd";
$f[]="\xc0\x8f";  $t[]="\xfe";
$f[]="\xdb\x92";  $t[]="\xff";
  $result= str_replace($t, $f, $str);
  return $this->avusCharsetToUtf8OLD($result);
  
}
	
	public function rsToHtmlTable ($rs){
		
		
		
		
	}
	
	 
	public function commandAction (){
		
		$appExecutionTime = ini_get('max_execution_time');
		ini_set('max_execution_time', 150000);
		ini_set ('request_terminate_timeout', 150000);
		// \Baseapp\Suva\Library\libDTP::fillArrCategories(  \Baseapp\Suva\Models\Issues::findFirst(101) , 1 );
		
		// $arrAdIds = \Baseapp\Suva\Library\libPCM::getIds(
				// array (
					// "modelName" => "Ads"
					
					// ,"filters" => array (
						// array(
							// "modelName" => "Issues"
							// ,"idValue" => 111
						// )
						// ,array(
							// "modelName" => "CategoriesMappings"
							// ,"idValue" => 5252
						// )
						// ,array(
							// "modelName" => "Ads"
							// ,"whereField" => "moderation"
							// ,"whereValue" =>"'ok'"
						// )
					// )
				// )
			// );
			// error_log(print_r($arrAdIds,true));
	
		// $product = \Baseapp\Suva\Models\Ads::findFirst(900000);
		// $metadata = $product->getModelsMetadata();
		
		// $values = $metadata->getDefaultValues($product);
		// Nava::printr($values,"DEFAULT VALUES");

		// $values = $metadata->getDataTypes($product);
		// Nava::sysMessage($values,"DATA TYPES");
		
		// $values = $metadata->getStrategy();
		// Nava::sysMessage($values,"STRATEGY");
		
		// $values = $metadata->readColumnMap($product);
		// Nava::sysMessage($values,"readColumnMap");

		// $values = $metadata->getAttributes($product);
		// Nava::sysMessage($values,"getAttributes");

		
		// $values = $metadata->getNonPrimaryKeyAttributes($product);
		// Nava::sysMessage($values,"getNonPrimaryKeyAttributes");

		// $values = $metadata->getPrimaryKeyAttributes($product);
		// Nava::sysMessage($values,"getPrimaryKeyAttributes");

		// $values = $metadata->getNotNullAttributes($product);
		// Nava::sysMessage($values,"getNotNullAttributes");		
		

		// $values = $metadata->getDataTypesNumeric($product);
		// Nava::sysMessage($values,"getDataTypesNumeric");	


		// $values = $metadata->getIdentityField($product);
		// Nava::sysMessage($values,"getIdentityField");	


		// $values = $metadata->getBindTypes($product);
		// Nava::sysMessage($values,"getBindTypes");	


		// $values = $metadata->getAutomaticCreateAttributes($product);
		// Nava::sysMessage($values,"getAutomaticCreateAttributes");	


		// $values = $metadata->getEmptyStringAttributes($product);
		// Nava::sysMessage($values,"getEmptyStringAttributes");	


		// $values = $metadata->getColumnMap($product);
		// Nava::sysMessage($values,"getColumnMap");	


		// $values = $metadata->getReverseColumnMap($product);
		// Nava::sysMessage($values,"getReverseColumnMap");	
		
		// foreach ( \Baseapp\Suva\Models\OrdersItems::find() as $oi ){
			// $oi->n_before_save();
			// $oi->update();
		// }
 
		// foreach ( \Baseapp\Suva\Models\OrdersItems::find("n_categories_mappings_id is null") as $oi ){
			
					// //07.04.2017 - ažuriranje CategoryMapping
			// $ad = $oi->Ad();
			// $publication = $oi->Publication();
			// if ( $ad && $publication){
				// //Nava::poruka($ad->id);
				// if($ad->n_is_display_ad == 1 and $ad->n_category_mapping_id > 0 ){
					// $oi->n_categories_mappings_id = $ad->n_category_mapping_id;
				// }else{
					// $catMapId = $ad->getCategoryMappingForPublication( $publication->id );
					// if (!$catMapId) {
						// Nava::sysMessage("Za order item $this->id nije pronađen Category Mapping");
					// }
					// else {
						// $oi->n_categories_mappings_id = $catMapId;
					// }
				// } 
				
			// }

			
			// //$oi->n_before_save();
			// $oi->update();
		// }
		

		// for ($x = 0; $x <= 1; $x++) {
			// Nava::saveMessageToFile ( "Poruka $x");
		// } 
		//exec('ls -all');
		
		// $_SESSION['_context']['_is_threaded_execution'] = true;
		// Nava::poruka("tu sam");
		
		// Nava::runSql("select * from as");
		
		// \Baseapp\Suva\Library\libAdsSearchWords::updateAdsSearchWordsForPublication(5);

			

		
		$this->view->pick('obrade/index');
		
	}

	public function nextIssuesAction(){
		$startDate = '2017-05-27';
		$endDate = '2018-12-31';

		for ($i = strtotime($startDate); $i <= strtotime($endDate); $i = strtotime('+1 day', $i)) {
			if(date('N', $i) == 2){
				$date_published = date('Y-m-d 00:00:00', $i);
				$deadline = date('Y-m-d 08:00:00', $i-1);
				$txtSql = "insert into n_issues(date_published,publications_id,deadline,status) values ('".$date_published."',2,'".$deadline."','prima')";
				Nava::poruka($txtSql);
				Nava::runSql($txtSql);
			}if(date('N', $i) == 5){
				$date_published = date('Y-m-d 00:00:00', $i);
				$deadline = date('Y-m-d 08:00:00', $i-1);
				$txtSql = "insert into n_issues(date_published,publications_id,deadline,status) values ('".$date_published."',2,'".$deadline."','prima')";
				Nava::poruka($txtSql);
				Nava::runSql($txtSql);
			}
			 
		}

		 
		$this->view->pick('obrade/index');
	}
	
	public function threadAction( $pParentSessionId ){
		
		error_log ("threadAction, parentSession = $pParentSessionId");
		
		$_SESSION['_context']['_SID'] = $pParentSessionId;
		$_SESSION['_context']['_is_threaded_execution'] = true;

		Nava::poruka ("Thread 1");
		Nava::poruka ("Thread 2");
		
		Nava::greska ("Greška 1");
		
	}

	public function callThreadAction(  ){
		
		$sessionId = session_id();
		$_SESSION['_context']['_is_threaded_execution'] = true;

		error_log ("prije execa");
		
		//exec("/usr/bin/php -q /home/oglasnik2/oglasnik.hr/current/vendor/pokreniObradu.php thread/$sessionId >> /home/oglasnik2/oglasnik.hr/current/logs/php-errors.log ");
		
		exec("/usr/bin/php -q /home/oglasnik2/oglasnik.hr/current/vendor/pokreniObradu.php thread/$sessionId > /dev/null &");
		
		
		error_log ("nakon execa");
		$this->view->pick('obrade/index');
	}

	public function updateAllUnsyncedAdsAction(){
		\Baseapp\Suva\Library\libCron::updateAllUnsyncedAds();
		$this->view->pick("obrade/index");
	}
	
	
	
	public function updateEveryMinuteAction(){
		\Baseapp\Suva\Library\libCron::updateEveryMinute();
	}
	
	
	public function updateSessionStorageAction(){
		$this->fill_SESSIONDataForSuva(true);
		$this->view->pick("obrade/index");
	}

	public function refreshCategoriesMappingsOnOrdersItemsAction (){
		
		$appExecutionTime = ini_get('max_execution_time');
		ini_set('max_execution_time', 150000);
		ini_set ('request_terminate_timeout', 150000);

		
		foreach ( \Baseapp\Suva\Models\OrdersItems::find() as $oi ){
			error_log("processing order item $oi->id");
			$oi->n_before_save();
			$oi->update();
		}

		Nava::poruka("Orders items successfully updated");
		
		$this->view->pick('obrade/index');
		
	}
	
	//threaded Akcije
	public function offLineIssueXmlThreadedAction( $pParentSessionId, $pIssueId, $pGroupId = null ) {
		
		$_SESSION['_context']['_SID'] = $pParentSessionId;
		$_SESSION['_context']['_is_threaded_execution'] = true;

		$_context = $_SESSION['_context'];

		$issueId = $pIssueId;
		$dtpGroupId = $pGroupId > 0 ? $pGroupId : null;

		Nava::poruka ("Starting creation of XML for Issue $issueId, ".($dtpGroupId ? " group $dtpGroupId " : "")." in a parallel processing ");
		
		
		error_log("offLineIssueXmlThreadedAction: issueId: $issueId, dtpGroupId: $dtpGroupId ");
		
		$result = \Baseapp\Suva\Library\libDTP::createXmlForDtp ( $issueId, $dtpGroupId );

		//ovo da se zaustavi ućitavanje konteksta u parent sessionu
		Nava::unsetThreadedExecutionInParentSession();
		
		$this->view->finish();
		
	}

	public function syncAvusAdsWithRemainingInsertionsThreadedAction( $pParentSessionId ){
		$_SESSION['_context']['_SID'] = $pParentSessionId;
		$_SESSION['_context']['_is_threaded_execution'] = true;


		\Baseapp\Suva\Library\libAVUS::syncAvusAdsWithRemainingInsertions();


		//ovo da se zaustavi ućitavanje konteksta u parent sessionu
		Nava::unsetThreadedExecutionInParentSession();
		
		$this->view->finish();
		
		
	}
	
	public function ispraviGreskuUserAction(){
		//mijenjanje krivo importiranih usera 1
		\Baseapp\Suva\Library\libAVUS::ispraviGreskuUser();
		$this->view->pick('obrade/index');
		
	}

	public function ispraviGreskuUser1Action(){
		//mijenjanje krivo importiranih usera 2
		\Baseapp\Suva\Library\libAVUS::ispraviGreskuUser1();
		$this->view->pick('obrade/index');
		
	}

	public function testAction(){
		$result = \Baseapp\Suva\Library\libAppsetting::get('test_order_43805');
		Nava::printr ($result->toArray());
		$this->view->pick('obrade/index');
		
	}

	
	
}

