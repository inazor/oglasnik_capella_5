<?php

namespace Baseapp\Suva\Controllers;

use Phalcon\Dispatcher;
use Baseapp\Extension\Tag;
use Baseapp\Library\Tool;
use Baseapp\Library\Utils;
use Baseapp\Suva\Models\Roles;
use Baseapp\Suva\Models\Locations;
use Baseapp\Traits\CrudActions;
use Baseapp\Traits\CrudHelpers;
use Baseapp\Traits\ParametrizatorHelpers;
use Phalcon\Paginator\Adapter\QueryBuilder;
use Baseapp\Suva\Library\Nava;

use Baseapp\Suva\Models\Actions;
use Baseapp\Suva\Models\Orders;
use Baseapp\Suva\Models\Ads;
use Baseapp\Suva\Models\Categories;


use Baseapp\Suva\Models\OrdersItems;
use Baseapp\Suva\Models\Users;
use Baseapp\Suva\Models\N_Discounts;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\View;

use mPDF;

/**
gggggg dddd

GRANA VIRTUALKA
 * Suva Users Controller
 */
class ActionsController extends IndexController {
    use CrudActions;
    use CrudHelpers;

    /* @var \Baseapp\Suva\Models\Users */
    public $crud_model_class = 'Baseapp\Suva\Models\Actions';

	public function indexAction() {
		$this->tag->setTitle("Actions");
		$this->n_query_index("Actions"); //ovo je definirano u BaseController
	}
	
	public function crudAction($pEntityId = null) {
		$this->n_crud_action("Actions", $pEntityId); //ovo je definirano u BaseController
		$this->view->pick('actions/crud');
	}

 

// //AKCIJE
    
    // //IZMJENE STATUSA ORDERA
    // /*
        // update orders set n_status = 3 where status = 1;
        // update orders set n_status = 5 where status = 2;
        
        // update orders set n_status = 6 where status = 4;
		
		
		// update orders set n_status = 1 where status = 1;
		// update orders set n_status = 2 where status = 1;
        // update orders set n_status = 3 where status = 1;
		// update orders set n_status = 4 where status = 2;
        // update orders set n_status = 5 where status = 2;
        // update orders set n_status = 6 where status = 3;
        // update orders set n_status = 6 where status = 4;
		
		
		// update orders set n_status = 9 where status = 2;
		// update orders set n_status = 9 where status = 4;
    // */
    
    // // public function actOrderStatusBesplatniAction( $pOrdersId = null ) {

        // // if (!is_int($pOrdersId)){ $this->redirect_to($this->get_next_url());}

        // // Nava::runSql("update orders set n_status = 2, status = 1 where cast(total as decimal) = 0 and id =".$pOrdersId);    

        // // //37 - kad oglas prelazi iz statusa Košarica u Ponuda i dalje je neaktian, osim ako je besplatan 
        // // //    u svim ad-ovima koji dolaze iz besplatnih oglasa (proizvoda na online publikacijama čija je cijena 0  staviti active = 1 )
        // // //    !!! mysql UPDATE ako ima JOIN-a ne smije imar WHERE - sve treba ubacit u JOIN-e
        // // Nava::runSql(
            // // "    update ads a "
            // // ."        join orders_items oi on (a.id = oi.ad_id and oi.total = 0 and a.active = 0 ) "
            // // ."            join orders o on (oi.order_id = o.id and o.id = ".$pOrdersId.") "
            // // ."            join n_products p on (oi.n_products_id = p.id and p.unit_price = 0) "
            // // ."            join n_publications pub on (oi.n_publications_id = pub.id and pub.is_online = 1) "
            // // ."    set active = 1  "
        // // );

        // //46 - kada se Košarica prebacuje u Ponudu ili Besplatne, svi besplatni oglasi na njoj postaju aktivni (polje Active postaje TRUE)
        // Nava::activateFreeAds($pOrdersId);


        // $this->flashSession->success("<strong> The status of Order No. $pOrdersId has been changed to 'Free Ads'.</strong>");
        // return $this->redirect_back();

    // } 

    // // public function actOrderStatusPonudaAction( $pOrdersId = null , $pSalesRepId = null ) {

// // //     if (!is_int($pOrdersId)){ $this->redirect_to($this->get_next_url());}
		
	// // //161 - Kod prebacivanja u "To Offer" prebaciti samo one stavke čija je vrijednost > 0. AKo nema tih stavki, javiti grešku
	// // $paidOrderItems = Nava::sqlToArray("select * from orders_items where total > 0 and order_id = $pOrdersId");
	// // if (!$paidOrderItems){
		// // $this->flashSession->error("The selected document does not contain any paid items. Nothing was transferred to Offer");
		// // return $this->redirect_back();
	// // }
	

    // // //19 - Ako na košarici postoje samo besplatni oglasi, mogu ići samo na Besplatni Oglas (ne u Ponudu i Fakturu). Ako postoje miješano, mogu ići na ponudu i u fakturu zajedno s besplatnima
    // // $order = Nava::sqlToArray("select * from orders where id = ".$pOrdersId)[0];
    // // if ($order['total'] == 0) { $this->redirect_to($this->get_next_url());}


    // // //35 - ... Kod prebacivanja u status Ponuda, trenutno vrijeme upisati u quotation_date
    // // Nava::runSql("update orders set n_status = 3, status = 1, modified_at = NOW(), n_quotation_date = CURDATE(), n_pbo = concat('90000', cast(id as char(5)))  where id =".$pOrdersId);    


    // // //37 - kad oglas prelazi iz statusa Košarica u Ponuda i dalje je neaktivan, osim ako je besplatan 
    // // //      u svim ad-ovima koji dolaze iz besplatnih oglasa (proizvoda na online publikacijama čija je cijena 0  staviti active = 1 )
    // // //    !!! mysql UPDATE ako ima JOIN-a ne smije imar WHERE - sve treba ubacit u JOIN-e
    // // Nava::runSql(
        // // "    update ads a "
        // // ."        join orders_items oi on (a.id = oi.ad_id and oi.total = 0) "
        // // ."            join orders o on (oi.order_id = o.id and o.id = ".$pOrdersId.") "
        // // ."            join n_products p on (oi.n_products_id = p.id and p.unit_price = 0) "
        // // ."            join n_publications pub on (oi.n_publications_id = pub.id and pub.is_online = 1) "
        // // ."    set online_product = 'O:47:'"  
            
    // // );

	// // //133 - kad se cartu mijenja status u ponuda, njegov user id je sales rep id
	// // Nava::runSql("update orders set n_sales_rep_id=".$this->auth->get_user()->id." where id =".$pOrdersId);    

		
    // // //46 - kada se Košarica prebacuje u Ponudu ili Besplatne, svi besplatni oglasi na njoj postaju aktivni (polje Active postaje TRUE)
    // // Nava::activateFreeAds($pOrdersId);
        
        

    // // //4 - SALES REP ID PREMA SADAŠNJEM NAČINU RADA JE VEZAN UZ ORDER, NAJLOGIČNIJE GA JE UNIJETI U TRENUTKJU KADA KOŠARICA POSTAJE PONUDA

    // // //  Nava::runSql("update orders set s = 3, modified_at = NOW(), quotation_date = NOW() where id =".$pOrdersId);     
    // // $this->flashSession->success("<strong> The status of Order No. $pOrdersId has been changed to 'Offer'.</strong>");
    // // return $this->redirect_back();

    // // } 

    // // public function actOrderStatusFakturaAction( $pOrdersId = null ) {

	// // //     if (!is_int($pOrdersId)){ $this->redirect_to($this->get_next_url());}

		// // //19 - Ako na košarici postoje samo besplatni oglasi, mogu ići samo na Besplatni Oglas (ne u Ponudu i Fakturu). Ako postoje miješano, mogu ići na ponudu i u fakturu zajedno s besplatnima
		// // $order = Nava::sqlToArray("select * from orders where id = ".$pOrdersId)[0];
		// // if ($order['total'] == 0) return $this->redirect_back();

		// // //134 - dodati novo polje operator_id koje će kod akcije StatusFaktura ubaciti ID trenutno spojenog korisnika
		// // //status = 2 - completed, njih se automatski šalje na fiskalizaciju 
		// // Nava::runSql("update orders set n_status = 4, status = 2, modified_at = NOW(), n_operator_id=".$this->auth->get_user()->id." where cast(total as decimal) > 0 and id =".$pOrdersId);    
		
		// // $order_item = \Baseapp\Suva\Models\OrdersItems::findFirst("order_id = $pOrdersId");
		

		// // if($order_item && $order_item->n_products_id > 0){
// // // 			$sqlTxt = "update ads set active = 0 where id = ".$order_item->ad_id;
// // // 			//error_log('txtsql je '.$sqlTxt);
// // // 			Nava::runSql($sqlTxt);			
			// // $product = \Baseapp\Suva\Models\Products::findFirst($order_item->n_products_id);
// // // 			//error_log(1);
			// // if($product && $product->is_online_product == 1){
				// // if($order_item->n_first_published_at > 0){
					
					// // $order_item_hours = explode(" ", $order_item->n_first_published_at);
					// // if($order_item_hours[1] == '00:00:00'){
						// // $order_item_hours[1] = date("H:i:s");
// // // 						//error_log(2);
						// // $order_item_date = implode(" ",$order_item_hours);
// // // 						//error_log(3);
// // // 						$order_item->n_first_published_at = $order_item_date;
// // // 						//error_log(4);
// // // 						//error_log($order_item_date);
						// // Nava::runSql("update orders_items set n_first_published_at = '$order_item_date' where order_id = $pOrdersId");
						
						// // //error_log(5);
					// // }
					
				// // }
			// // }
		// // }
		
		// // $this->flashSession->success("<strong> The status of Order No. $pOrdersId has been changed to 'Invoice'.</strong>");
		// // return $this->redirect_back();
	// // } 

    // // public function actOrderStatusPlacenoAction( $pOrdersId = null, $pPaymentDate = null ) {

        // // /* 
        // // time() - trenutno vrijeme u unix timestampu
        
        
        // // */
        
// // //         $this->printr($_REQUEST);
		// // if (!$pOrdersId > 0 ) {
			// // $this->greska("Parameter Order ID not sent  - $pOrdersId");
			// // return $this->redirect_back();
		// // }
		// // $order = \Baseapp\Suva\Models\Orders::findFirst($pOrdersId);
		// // if (!$order ) {
			// // $this->greska("Order No.  $pOrdersId not found.");
			// // return $this->redirect_back();
		// // }
		// // if (!$pPaymentDate) $pPaymentDate = date("Y-m-d" , time());
		// // $pPaymentDate = $order->n_payment_date;
		// // if (!$order->n_payment_methods_id){
			// // $this->greska("Payment method not supplied.");
			// // return $this->redirect_back();
		// // }
		
		
// // // 		385 - sat - time stamp kad će oglas biti objavljen. sada nije dobro. 
// // // 		treba napraviti da se sat (ako je u postavkama kod plaćanja 00.00) automatski postavi s trenutkom plaćanja.
		
		// // $order_item = \Baseapp\Suva\Models\OrdersItems::findFirst("order_id = $pOrdersId");
		

		// // if($order_item && $order_item->n_products_id > 0){
// // // 			$sqlTxt = "update ads set active = 0 where id = ".$order_item->ad_id;
// // // 			//error_log('txtsql je '.$sqlTxt);
// // // 			Nava::runSql($sqlTxt);			
			// // $product = \Baseapp\Suva\Models\Products::findFirst($order_item->n_products_id);
// // // 			//error_log(1);
			// // if($product && $product->is_online_product == 1){
				// // if($order_item->n_first_published_at > 0){
					
					// // $order_item_hours = explode(" ", $order_item->n_first_published_at);
					// // if($order_item_hours[1] == '00:00:00'){
						// // $order_item_hours[1] = date("H:i:s");
// // // 						//error_log(2);
						// // $order_item_date = implode(" ",$order_item_hours);
// // // 						//error_log(3);
// // // 						$order_item->n_first_published_at = $order_item_date;
// // // 						//error_log(4);
// // // 						//error_log($order_item_date);
						// // Nava::runSql("update orders_items set n_first_published_at = '$order_item_date' where order_id = $pOrdersId");
						
						// // //error_log(5);
					// // }
					
				// // }
			// // }
		// // }
		
		
		
			
			
		
         
        // // //115 - RUČNO PLAĆANJE tako da se na popisu preliminary invoice odabere "EDIT", 
		// // //      i da se odabere vrsta plaćanja, i eventualno izmijenen ostali podaci. Na Editor Order Headera staviti dugme Potvrdi plaćanje
        // // //prvovjerit jeli u orderu postoji payment_date i payment_method ako ne postoji javit gresku (flash-session-error)
		
		
        // // //143 - kod prelaska u status plaćeno vrši se slijedeća provjera: ako je bilo koji od datuma objave plaćenih oglasa raniji od datuma plaćanja, order ide u status Late Paid.
        // // //unix timestamp
        // // $lPrvi = Nava::sqlToArray("select min(n_first_published_at) as prvi from orders_items where n_total > 0 and order_id = $pOrdersId")[0]['prvi'];
        // // if (!$lPrvi) {
			// // $this->greska ("Could not find first publication date on order No. $pOrdersId");
			// // return $this->redirect_back();
		// // } 
        
        // // if (strtotime($pPaymentDate) > strtotime($lPrvi)) {
            // // //ako ima zakašnjelih ide u late payment
			// // $order->n_status = 9;
			// // $order->update();
			
			// // $this->greska(
				// // "Status of Order No. $pOrdersId has been changed to 'Late Paid' because there was a paid product that was published "
				// // .$order->n_dateTimeUs2DateTimeHr($lPrvi).", before payment date (".$order->n_dateUs2DateHr($pPaymentDate).")");
            // // return $this->redirect_back();
			
        // // } else {
			// // $order->n_status = 5;
			// // $order->status = 3;
			// // $order->n_payment_date = $pPaymentDate;
			// // $order->update();
			
	        // // //56 - Kad fakturi promijeniš status u "plaćeno", svi ad-ovi koji su na njoj a dodijeljen im je plaćeni proizvod postaju aktivni
			// // $lPaidAds = Nava::sqlToArray("select ad_id from orders_items where order_id = $pOrdersId and total > 0 and ad_id > 0 group by ad_id");
			// // foreach($lPaidAds as $ad) {
// // // 				Nava::activateAd( $ad['ad_id'] );
				// // Nava::runSql("update ads set online_product_id = 1 where id =".$ad['ad_id']); 
				// // Nava::runSql("update ads set online_product = 'O:47:' where id =".$ad['ad_id']); 
			// // }

			// // $this->poruka("Status of Order No. $pOrdersId has been changed to 'Paid'.  "
				// // ." First publication date is ".$order->n_dateTimeUs2DateTimeHr($lPrvi)
				// // .". Payment date is ".$order->n_dateUs2DateHr($pPaymentDate));
			// // return $this->redirect_back();
		// // }
                                        
    // // } 

    // // public function actOrderStatusKosaricaAction( $pOrdersId = null ) {

// // //     if (!is_int($pOrdersId)){ $this->redirect_to($this->get_next_url());}

    // // $order = Nava::sqlToArray("select * from orders where id = ".$pOrdersId );
    // // if (!$order) return null;// $this->redirect_self();
    // // if ($order[0]['n_status'] == 1) return null;

    // // $cart = Nava::sqlToArray("select * from orders where n_status = 1 and user_id = ".$order[0]['user_id'] );
    // // if ($cart){
        // // //cart postoji, kopiraju se stavke i briše se dokument
		// // Nava::sysMessage ("moving Order Item $oi_id to Order $order_id ActionCOntroller");
        // // Nava::runSql("update orders_items set order_id = ".$cart[0]['id']." where order_id = ".$pOrdersId);
    // // } else {
        // // Nava::runSql("update orders set n_status = 1 , status = 1 where id = ".$pOrdersId);
    // // }
    // // //error_log(print_r($order,true));


    // // return null; 

    // // } 
    
    // // public function actOrderStatusKasnjenjeAction( $pOrdersId = null ) {


        // // Nava::runSql("update orders set n_status = 9, status = 4, modified_at = NOW() where id =".$pOrdersId);    

   
        // // $this->flashSession->success("<strong> The status of Order No. $pOrdersId has been changed to 'Late Paid'.</strong>");
        // // return $this->redirect_back();

    // // } 
	
	// // public function actOrderPayWithMethodAction ( $pMethod = null, $pOrdersId = null ){
		// // if (!$pMethod) {
			// // $this->greska("Invalid payment method");
			// // return $this->redirect_back();
		// // }
		
		// // $order = \Baseapp\Suva\Models\Orders::findFirst($pOrdersId);
		// // if (!$order) {
			// // $this->greska("Invalid order ID $pOrdersId");
			// // return $this->redirect_back();
		// // }
		
		// // $method = \Baseapp\Suva\Models\PaymentMethods::findFirst("slug = '$pMethod'");
		// // if (!$method) {
			// // $this->greska("Invalid payment method: $pMethod");
			// // return $this->redirect_back();
		// // }
		
		
		// // $order->n_payment_methods_id  = $method->id;
		// // $order->payment_method = $method->slug;
		// // $order->n_payment_date = date ("Y-m-d", time());
		
		// // //NE PREBACUJE FISKALNU LOKACIJU TRENUTNO SPOJENOG KORISNIKA
		// // $order->n_fiscal_location_id = $this->auth->get_user()->n_fiscal_location_id;
		
		// // //error_log("actOrderPayWithMethodAction 1". print_r($order->toArray(), true));

		// // //error_log(print_r($_REQUEST,true));
		// // if ($order->update() == false) {
			// // $this->greska("Could not update order No. $pOrdersId");
			// // return $this->redirect_back();
		// // }
		
		// // return $this->actOrderStatusPlacenoAction( $pOrdersId, date ("Y-m-d", time()) );
	// // }
	
    // // //PREBACIVANJE ORDERA    
    // // public function actOrderCopyToCartAction( $pOrdersId = null ) {

    // // if (!is_int($pOrdersId)){ $this->redirect_to($this->get_next_url());}

    // // $order = Nava::sqlToArray("select * from orders where id = ".$pOrdersId );
    // // if (!$order) return null;// $this->redirect_self();
    // // if ($order[0]['n_status'] == 1) return null;

    // // $cart = Nava::sqlToArray("select * from orders where n_status = 1 and user_id = ".$order[0]['user_id'] );
    // // if ($cart){
        // // //cart postoji, kopiraju se stavke i briše se dokument
		// // Nava::sysMessage ("moving Order Item $oi_id to Order $order_id ActionCOntroller 396");
        // // Nava::runSql("update orders_items set order_id = ".$cart[0]['id']." where order_id = ".$pOrdersId);
    // // } else {
        // // Nava::runSql("update orders set n_status = 1 where id = ".$pOrdersId);
    // // }
    // // //error_log(print_r($order,true));


    // // return null; 

    // // } 
    
    // // //ORDER ITEMS - PREBACIVANJE, KOPIRANJE, BRISANJE
    // // public function actMoveItemToFreeAdsAction( $pOrdersItemsId = null ) {
		// // //error_log("ActionsController:actMoveItemToFreeAdsAction 1");
        // // //if (!is_int($pOrdersId)){ $this->redirect_to($this->get_next_url());}
        // // /*
        // // 1. provjeriti je li iznos retka besplatan, ako nije vratiti se bez izmjene
        // // Preseljavanje order itema na free listu free ads
        // // 2. provjeriti postoji li lista free ads
        // // 3. ako ne postoji kreirati je
        // // 4. promijeniti order_id 
        // // */
		
		// // if (!$pOrdersItemsId) return $this->greska("Orders Items ID not supplied");
		// // elseif(!$pOrdersItemsId > 0) {
			// // $this->greska("Invalid Orders Items ID  supplied, $pOrdersItemsId");
			// // return $this->redirect_back();
		// // }
		
		// // $oi = \Baseapp\Suva\Models\OrdersItems::findFirst($pOrdersItemsId);
		// // if (!$oi ) {
			// // $this->greska("Order Item No. $pOrdersItemsId not found");
			// // return $this->redirect_back();
		// // }
		// // //error_log("ActionsController:actMoveItemToFreeAdsAction 2");
		// // $order = \Baseapp\Suva\Models\Orders::findFirst($oi->order_id);
		// // if ( !$order ) {
			// // $this->greska("Order No. $oi->order_id, reffered to by Order Item No. $oi->id, not found.");
			// // return $this->redirect_back();
		// // }
		// // //error_log("ActionsController:actMoveItemToFreeAdsAction 3");
        // // //1
		// // if ( !$oi->n_total == 0.0) {
			// // $this->greska ("Order Item No. $oi->id has total of $oi->total and cannot be transferred to free ads list.");
			// // return $this->redirect_back();
		// // }
		// // //error_log("ActionsController:actMoveItemToFreeAdsAction 4");
		// // //traženje Free Ads liste
		// // $free = \Baseapp\Suva\Models\Orders::findFirst( " user_id = $order->user_id and n_status = 2 ");
		// // if (!$free) {
			// // //error_log("ActionsController:actMoveItemToFreeAdsAction 5");
			// // // Kreiranje novog free ordera
			// // $free = new \Baseapp\Suva\Models\Orders();
			// // $free->n_set_default_values($free->getSource());
			// // $free->n_status = 2;
			// // $free->user_id = $order->user_id;
			// // $free->status = 1;
			// // $success = $free->create();
			// // //hendlanje greske
			// // if ($success === true){	
				// // //error_log("ActionsController:actMoveItemToFreeAdsAction 6");
				// // $greska = $free->n_after_save($this->request);
				// // if ($greska) return $this->greska($greska);
			// // } 
			// // else {
				// // //error_log("ActionsController:actMoveItemToFreeAdsAction 7");
				// // $greske = $free->getMessages();
				// // foreach ($greske as $greska)  
					// // $err_msg .= $greska.", ";
				// // $this->flashSession->error('<strong>Errors while creating new cart: </strong>'.$err_msg);
			// // }
		// // }
		// // //error_log("ActionsController:actMoveItemToFreeAdsAction 8");
		// // $oi->order_id = $free->id;
		// // $success = $oi->update();
		// // if ($success === true) {
			// // $this->poruka("<strong> Order Item No. $oi->id has been moved to Free Ads (No.:$free->id) .</strong>");
			// // return $this->redirect_back();
		// // }
		// // else {
			// // //error_log("ActionsController:actMoveItemToFreeAdsAction 9");
			// // $greske = $oi->getMessages();
			// // foreach ($greske as $greska)  
				// // $err_msg .= $greska.", ";
			// // $this->flashSession->error("<strong>Errors while transferring order item No. $oi->id to Free ads: </strong>".$err_msg);
			// // return $this->redirect_back();
		// // }
		// // //error_log("ActionsController:actMoveItemToFreeAdsAction 10");
	
		// // return $this->redirect_back();
    // // } 
    
    // // public function actItemCopyToCartAction( $pOrdersItemsId = null ) {
		
		// // if (!$pOrdersItemsId > 0){
			// // $this->greska("Parameter Orders Items ID not received");
			// // return $this->redirect_back();
		// // }
		
		// // $oi = \Baseapp\Suva\Models\OrdersItems::findFirst($pOrdersItemsId);
		// // if (!$oi){
			// // $this->greska("Parameter Order Item No. $pOrdersItemsId not found");
			// // return $this->redirect_back();
		// // }
		
		// // $response = $oi->copyToCart($this->auth->get_user()->id);
		// // if ( $response > 0 ) $this->poruka("Order Item No. $oi->id copied to Cart");
		// // else $this->greska("Order Item No. $oi->id was NOT copied to cart due to error: $response");
		// // return $this->redirect_back();
		
		
        // // // //if (!is_int($pOrdersId)){ $this->redirect_to($this->get_next_url());}
        // // // /*
        // // // Preseljavanje order itema na free listu free ads
        // // // 1. provjeriti postoji li cart
        // // // 3. ako ne postoji kreirati ga
        // // // 4. kopirati order_item
        // // // 5. kopirati insertions
        // // // 6. kopirati discounts
        // // // */

        // // // //1
        // // // $orders_items_rec = Nava::sqlToArray("select * from orders_items where id= $pOrdersItemsId")[0];
       
        // // // //2
        // // // $order_rec = Nava::sqlToArray("select * from orders where id = ".$orders_items_rec['order_id'] )[0];
        // // // $cart_rec = Nava::sqlToArray("select * from orders where user_id = ".$order_rec['user_id']." and n_status = 1 ");
        // // // if ($cart_rec){
            // // // $cart_id = $cart_rec[0]['id'];
        // // // } else {
            // // // //3
            // // // //132 kad se kreira cart, sales rep je onaj koji je bio logiran kad je cart kreiran
			// // // $cart_id = Nava::createNewCart($order_rec['user_id'], $this->auth->get_user()->id)['id'];
        // // // }
        
        // // // //4
        
        // // // //Nava::runSql("update orders_items set order_id = $cart_id where id = $pOrdersItemsId ");
        // // // //NOVI ORDER ITEM
        // // // Nava::runSql( " insert into orders_items ( "
        // // // ."         order_id "
        // // // ."         ,ad_id "
        // // // ."         ,product_id "
        // // // ."         ,product "
        // // // ."         ,title "
        // // // ."         ,price "
        // // // ."         ,qty "
        // // // ."         ,total "
        // // // ."         ,tax_rate "
        // // // ."         ,tax_amount "
        // // // ."         ,n_products_id "
        // // // ."         ,n_discounts_id "
        // // // ."         ,n_publications_id "
        // // // ."         ,n_first_published_at "
        // // // ."         ,n_expires_at "
        // // // ."         ,n_remark "
        // // // ."         ,n_eps_filename "
        // // // ."         )  "
        // // // ." select "
        // // // ." 	      $cart_id "
        // // // ."         ,ad_id "
        // // // ."         ,product_id "
        // // // ."         ,product "
        // // // ."         ,title "
        // // // ."         ,price "
        // // // ."         ,qty "
        // // // ."         ,total "
        // // // ."         ,tax_rate "
        // // // ."         ,tax_amount "
        // // // ."         ,n_products_id "
        // // // ."         ,n_discounts_id "
        // // // ."         ,n_publications_id "
        // // // ."         ,n_first_published_at "
        // // // ."         ,n_expires_at "
        // // // ."         ,n_remark "
        // // // ."         ,n_eps_filename "
        // // // ." from orders_items oi  "
        // // // ."         where id = $pOrdersItemsId " );      
        
        // // // $newOdersItemsId = Nava::sqlToArray("select max(id) as id from orders_items")[0]['id'];

        // // // //4
        // // // Nava::runSql(
        // // // " insert into n_insertions ( "
        // // // ."      product_id "
        // // // ." 	    ,orders_items_id "
        // // // ." 	    ,issues_id "
        // // // ." 	    ,n_insertionscol "
        // // // ." 	    ,is_active "
        // // // ." ) select  "
        // // // ." 	    product_id "
        // // // ." 	    ,$newOdersItemsId "
        // // // ." 	    ,issues_id "
        // // // ." 	    ,n_insertionscol "
        // // // ." 	    ,is_active "
        // // // ." from n_insertions  "
        // // // ." where orders_items_id = $pOrdersItemsId "
         // // // );
        
        // // // //5
        // // // Nava::runSql(
            // // // " insert into n_orders_items_discounts ( "
            // // // ."	    orders_items_id "
            // // // ."	    ,discounts_id "
            // // // .")  "
            // // // ." select "
            // // // ."	    $newOdersItemsId "
            // // // ."	    ,discounts_id "
            // // // ." from n_orders_items_discounts "
            // // // ." where orders_items_id = $pOrdersItemsId ");
        
        // // // $this->flashSession->success("<strong> Order Item No. $pOrdersItemsId has been copied to Cart. New Order Item's ID is $newOdersItemsId.</strong>");
        // // // return $this->redirect_back();
    // // } 
    
    // // public function actItemDeleteAction( $pOrdersItemsId = null ) {

        // // //if (!is_int($pOrdersItemsId)){ return $this->redirect_back();}
        // // $order_item = Nava::sqlToArray("select * from orders_items where id = $pOrdersItemsId")[0];
        
        // // Nava::runSql("delete from n_orders_items_discounts where orders_items_id = ".$pOrdersItemsId);
        // // Nava::runSql("delete from n_insertions where orders_items_id = ".$pOrdersItemsId);
        // // Nava::runSql("delete from orders_items where id = ".$pOrdersItemsId);
        
        // // Nava::updateOrderTotal($order_item['order_id']);
        
        // // $this->flashSession->success("<strong> Order Item No. $pOrdersItemsId has been deleted.</strong>");
		
        // // return $this->redirect_back();

    // // }
	
	// // public function actDeactivateRelatedOrderItemAction( $pOrdersItemsId = null ) {
		
		// // $order_item = \Baseapp\Suva\Models\OrdersItems::findFirst($pOrdersItemsId);
		// // if(!$order_item) return $this->gresaka("Poslan je krivi broj order itema, $pOrdersItemsId");
		
		// // $order = \Baseapp\Suva\Models\Orders::findFirst($order_item->order_id);
		// // if(!$order) return $this->gresaka("Nije pronaden order br. $order_item->order_id, definiran na order itemu br. $pOrdersItemsId");
		
		// // $deactivated = \Baseapp\Suva\Models\Orders::findFirst("user_id = $order->user_id and n_status = 10");
		// // if(!$deactivated){
			// // Nava::runSql("insert into orders (status, total, created_at, modified_at) values (1,0, now(), now() )");
			// // $novi_id = Nava::sqlToArray("select max(id) as id from orders ")[0]['id'];
			// // $txtSql =  " update orders set "
			  // // ." user_id = $order->user_id "
			  // // .", n_status = 10 "
			  // // .", status = 1 "
			  // // ." where id = $novi_id ";
			// // Nava::runSql($txtSql);
			// // $deactivated = \Baseapp\Suva\Models\Orders::findFirst("user_id = $order->user_id and n_status = 10");
			// // if(!$deactivated) return $this->gresaka("Nije pronaden novi dokuemnt koji sadrzi deaktivirane order iteme.");
		// // }
		// // Nava::sysMessage ("moving Order Item $oi_id to Order $order_id ActionCOntroller 651");
		// // $txtSql = "update orders_items set n_is_active = 0, order_id = $deactivated->id where id = $order_item->id";
		// // $this->poruka($txtSql);
		// // Nava::runSql($txtSql);		
		
        // // $this->poruka("Order item br. $pOrdersItemsId je prebačen na dokument br. $deactivated->id ,  koji sadrži deaktivirane order iteme.");
		// // return $this->redirect_back();

    // // }
    
    // // public function actRenewOrderItemAction ($pOrdersItemsId = null ){
       // // ActionsController::actItemCopyToCartAction($pOrdersItemsId); 
    // // } 
       
    // // //DODAVANJE, BRISANJE
    // // public function actOrderDeleteAction( $pOrdersId = null ) {
        
        // // $os_rec = Nava::sqlToArray("select os.* from n_order_statuses os join orders o on (os.id = o.n_status) where o.id = $pOrdersId ")[0];
        // // $orderStatusId = $os_rec['id'];
        // // $orderStatusName = $os_rec['description'];
        
        // // if ($orderStatusId != 1){
// // //             $this->flashSession->error("Samo dokumenti u statusu Cart smiju biti izbrisani, odabrani dokument ima status $orderStatusName !");
            // // $this->flashSession->error("Only documents in status 'Cart' can be deleted. The selected document has status '$orderStatusName' !");
            // // return $this->redirect_back();
        // // }

        // // Nava::runSql(
            // // "   delete n_insertions " 
            // // ."  from n_insertions "
            // // ."      join orders_items  on (n_insertions.orders_items_id = orders_items.id) "
            // // ."  where orders_items.order_id = $pOrdersId ");
        
        // // Nava::runSql(
            // // "   delete n_orders_items_discounts " 
            // // ."  from n_orders_items_discounts "
            // // ."      join orders_items  on (n_orders_items_discounts.orders_items_id = orders_items.id) "
            // // ."  where orders_items.order_id = $pOrdersId ");

        // // Nava::runSql("delete from orders_items where order_id = ".$pOrdersId);
        // // Nava::runSql("delete from orders where id = ".$pOrdersId);

        // // $this->flashSession->success("<strong> $orderStatusName No. $pOrdersId has been deleted.</strong>");
        // // return $this->redirect_back();

    // // } 
    
    // // public function actAddDisplayAdAction( $pOrdersId = null ) {
        // // //error_log("actAddDisplayAdAction 1");

        // // $this->dispatcher->forward(
            // // array(
                // // "controller" => "orders-items",
                // // "action"     => "create",
                // // "params"     => array( 0, $pOrdersId, 1 )
            // // )
        // // );
        // // //error_log("actAddDisplayAdAction 4");
    // // } 
    
    // // public function actAddDisplayAdToUsersCartAction ( $pUsersId = null ){

		
		
        // // // provjeriti postoji li cart
        // // $cart = Nava::sqlToArray("select * from orders where n_status = 1 and user_id = $pUsersId ")[0];
        
        // // if (!$cart){
            // // //132 kad se kreira cart, sales rep je onaj koji je bio logiran kad je cart kreiran
            // // Nava::runSql("insert into orders (status, total, created_at, modified_at) values (1,0, now(), now() )");
            // // $cart_id = Nava::sqlToArray("select max(id) as id  from orders ")[0]['id'];
            // // Nava::runSql(
              // // " update orders set "
              // // ." user_id = $pUsersId "
              // // .", n_sales_rep_id = ".$this->auth->get_user()->id." "  
              // // .", n_status = 1 "
              // // .", status = 1 "
              // // ." where id = $cart_id "
            // // );
        // // } else {
            // // $cart_id = $cart['id'];
        // // }
		
// // // 		$this->flashSession->success("$cart_id");
// // // 		return $this->redirect_back();
        
        // // $this->actAddDisplayAdAction($cart_id);
    // // }

	// // //FRONTEND
	// // public function actProcessFrontEndAction($pOrdersId = null) {
		
		// // /*
		// // 1. prominit status u 3(insertions se nesmiju brisati pa zato moraju odma ic u offer tj ponudu)
		// // 2. u order prebacit n_product_id sa n_productsa
		// // 3. isto napravit sa publikacijama, pribacit sa n_publicationsa
		// // 4. ubacit category_id preko ad-a
		// // 5. ubacit quantity: ako je online onda je quantity 1, a iz proizvoda se kuži koliko dana objave ima. Ako je offline onda je quantity broj objava
		// // 6. ako je offline onda ubacit onoloko insertiona koliko ima objava (od današnjeg datuma naprijed)
		// // 7. ako je offline onda su datumi (first published i expires at) prvi i zadnji datum objave 
		// // 8. ako je online onda promijanit datume first published i expires at, kopirati sa ad-a
		
		// // ubaciti ono insertions koliko ima objava offline izdanje
		// // preko frontenda se ne mogu odabirat discounti
		
		// // */
		
		// // $debug = null;
		
		
		// // if ($debug) $this->flashSession->success("<strong>.$pOrdersId.</strong>");

	// // //	1. prominit status u 3(insertions se nesmiju brisati pa zato moraju odma ic u offer tj ponudu)
 		// // Nava::runSql("update orders set n_status = '3' where id = $pOrdersId");
		
		// // $order = Orders::findFirst($pOrdersId);
		// // if (!$order) return $this->greska ("Nije pronađen order br. $pOrdersId");
		
		// // $order_items = Nava::sqlToArray("select * from orders_items where order_id = $pOrdersId"); //svi order itemi iz našeg ordera
		// // foreach($order_items as $order_item){
			// // $title = $order_item['title'];
			// // //nać broj objava iz teksta
			// // //skužit jel se radi o online ili o offline
			// // if ($debug) $this->poruka($title);
			
			
			// // $kolicina = 0;
			// // if (strpos($title, 'Online') !== false) {
				// // $is_online = 1;
				// // if ($debug) $this->poruka("online je");
				// // $pos = strpos($title, ' dan');
				// // if ($pos > 0){
					// // if ($debug) $this->poruka($pos);
					// // $kolicina = (int)substr($title,$pos-2,2);
				// // }
			// // }elseif(strpos($title, 'Offline') !== false){
				// // $is_online = 0;
				// // if ($debug) $this->poruka("ofline je");
				// // $pos = strpos($title, ' objav');
				// // if ($pos > 0){
					// // if ($debug) $this->poruka($pos);
					// // $kolicina = (int)substr($title,$pos-2,2);
				// // }
			// // } else return $this->greska("Nije prepoznato je li proizvod online ili offline na order itemu ".$order_item['id']);

			// // if ($kolicina < 1) return $this->greska("nije prepoznat broj dana na order itemu ".$order_item['id']);
			
// // // 		5. ubacit quantity: ako je online onda je quantity 1, a iz proizvoda se kuži koliko dana objave ima. Ako je offline onda je quantity broj objava
			// // $quantity = null;
			// // if($is_online == 1)	{
				// // $quantity = 1;
				// // Nava::runSql("update orders_items set qty = $quantity where id = ".$order_item['id']);
			// // }elseif($is_online == 0){
				// // //quantity
				// // $quantity = $kolicina;				
				// // Nava::runSql("update orders_items set qty = ".$quantity." where id = ".$order_item['id']);
			// // }
			
			// // //4. category_id
			// // $ad = Ads::findFirst($order_item['ad_id']);
			// // if (!$ad) return $this->greska("Nije pronađen AD na order itemu ".$order_item['id']);
			
			// // $category = Categories::findFirst($ad->category_id);
			// // if (!$category) return $this->greska("Nije pronađena kategorija za AD na order itemu ".$order_item['id']);
			
			// // Nava::runSql("update orders_items set n_category_id = ".$ad->category_id." where id = ".$order_item['id']);

// // // 			2. u order prebacit n_product_id sa n_productsa
			// // if ($is_online == 0) {
				// // $txtSql = "select * from n_products where old_product_id collate utf8_croatian_ci ='".$order_item['product_id']."'";
			// // } else {
				// // $txtSql = "select * from n_products where days_published = $kolicina and old_product_id collate utf8_croatian_ci ='".$order_item['product_id']."'";
			// // }
 			// // if ($debug) $this->poruka($txtSql);
			// // $products = Nava::sqlToArray($txtSql);
 			// // if ($debug) $this->printr($products);
			// // if ($products){
				// // $product = $products[0];  //prvi s reda
				// // Nava::runSql("update orders_items set n_products_id = ".$product['id']." where id = ".$order_item['id']);
				
				// // //3. izmijeniti publication_id
				// // if ($product['publication_id'] > 0) Nava::runSql("update orders_items set n_publications_id = ".$product['publication_id']." where id = ".$order_item['id']);
				// // else $this->greska ("Na proizvodu ".$product['id']." nije navedena publikacija na koju se odnosi.");
				
			// // }else{
				// // $this->greska("Nije pronađen proizvod sa OldProduct ID = ".$order_item['product_id']." i Days Published = $kolicina");
			// // }
// // // 		6. ako je offline onda ubacit onoliko insertiona koliko ima objava (od današnjeg datuma naprijed) i promijenit datume (first published i expires at)	
			// // if ($is_online == 0){
				// // //pronaći issuses na kojma ćemo unosit insertione
				
				// // $txtSql = "select * from n_issues where status='prima' and publications_id = ".$product['publication_id']." and deadline > '".$order->created_at."' order by date_published";
				// // if ($debug) $this->poruka($txtSql);
				// // $issues = Nava::sqlToArray($txtSql);
				// // //$this->printr($issues);
				
				// // //provjerit je li postoji dovoljno issua 
				
				// // if (count($issues) < $kolicina) return $this->greska ("Nema dovoljno izdanja za popunjavanje (potrebno ih je $kolicina, a ima ih ".count($issues).")");
				// // //ubacit u tablicu Insertions
				// // $brojac = 0;
				// // foreach($issues as $issue){
					// // $brojac +=1;
					// // if ($brojac <= $kolicina) {
						// // $txtSql = "insert into n_insertions (product_id, orders_items_id, issues_id, is_active) select ".$product['id'].",".$order_item['id'].",".$issue['id'].",1";
						// // //$this->poruka($txtSql);
						// // Nava::runSql($txtSql);
					// // }
				// // }
			// // }
			
// // // 			7. ako je offline onda su datumi (first published i expires at) prvi i zadnji datum objave 
			// // if ($is_online == 0){
				// // //pronaći issuses na kojma ćemo unosit insertione
				// // $this->poruka($quantity);
				// // $txtSql = "select min(deadline) as prvi, max(deadline) as zadnji from n_issues where status='prima' and publications_id = ".$product['publication_id']." and deadline > '".$order->created_at."'";
				// // if ($debug) $this->poruka($txtSql);
				// // $datumi = Nava::sqlToArray($txtSql)[0];
				// // if ($debug) $this->printr($datumi);
				// // $txtSql = "update orders_items set n_first_published_at = UNIX_TIMESTAMP('".$datumi['prvi']."'), n_expires_at = UNIX_TIMESTAMP('".$datumi['zadnji']."') where id = ".$order_item['id'];
				// // if ($debug) $this->poruka($txtSql);
				// // Nava::runSql($txtSql);
			// // }
			
// // // 		8. ako je online onda promijanit datume first published i expires at, kopirati sa ad-a
			// // if ($is_online == 1){
				// // if ($debug) $this->printr($ad->toArray());
				// // $txtSql = "update orders_items set n_first_published_at = ".$ad->created_at.", n_expires_at = ".$ad->expires_at." where id = ".$order_item['id'];
				// // if ($debug) $this->poruka($txtSql);
				// // Nava::runSql($txtSql);
			// // }
		// // }
				
         // // return $this->redirect_back();

	// // }
    
    // //ISPIS
    // public function actOrderCreatePdfAction( $pOrdersId = null ) {

    // if (!is_int($pOrdersId)){ return $this->redirect_back();}

    // //         $this->dispatcher->forward(
    // //             array(
    // //                 "controller" => "fakture",
    // //                 "action"     => "createPdf",
    // //                 "params"     => array( $pOrdersId )
    // //             )
    // //         );



    // //error_log ("actOrderCreatePdfAction:".$pOrdersId."  #######  1");


    // $this->assets->addCss('assets/vendor/jasny-bootstrap-3.1.3-dist/css/jasny-bootstrap.min.css');
    // $this->assets->addJs('assets/vendor/jasny-bootstrap-3.1.3-dist/js/jasny-bootstrap.min.js');

    // $order = Orders::findFirst((int) $pOrdersId);
    // $user = Users::findFirst((int) $order->user_id);

    // $order->items = OrdersItems::find(array(
          // 'conditions' => 'order_id=:order_id:',
          // 'bind'       => array('order_id' => $order->id)
    // ));

     // $order->sales_rep = Users::find(array(
      // 'conditions' => 'id=:n_sales_rep_id:',
      // 'bind'       => array('n_sales_rep_id' => $order->n_sales_rep_id)
    // ));

        // $order->discount = N_Discounts::find(array(
      // 'conditions' => 'id=:n_discounts_id:',
      // 'bind'       => array('n_discounts_id' => $order->n_discounts_id)
    // ));

    // $this->tag->setTitle('Fakture');
    // $this->view->setLayout('common');
    // $this->view->setVar('user', $user);
    // $this->view->setVar('order', $order);

    // $mpdf = new mPDF();


    // $this->view->start();
    // $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    // //$this->view->pick("fakture/intro");
    // $this->view->render('fakture', 'pdfDoc');
    // $this->view->finish();
    // $content = $this->view->getContent();
    // //$html = $this->view->partial('fakture/intro');
    // $mpdf->WriteHTML($content );
    // $mpdf->Output("test.php", "I");
    // $this->view->disable();

    // //error_log ("actOrderCreatePdfAction:".$pOrdersId."  #######  99");


        // //return FaktureController::createPdfAction( $pOrdersId);

    // } 

    // // public function actOrderPrintRelatedQuotationAction( $pOrdersId = null ) {
    // // //error_log ("tu sam 00");

    // // //          ovo ne smije jer se ponovo poziva nakon forwardanja, ne znam zašto, ali tada bez parametra
     // // //if (!is_int($pOrdersId)){ $this->redirect_to($this->get_next_url());}
    // // //error_log ("tu sam 1");
    // // $this->dispatcher->forward(
        // // array(
            // // "controller" => "fakture",
            // // "action"     => "createPdf",
            // // "params"     => array( $pOrdersId, 3 )
        // // )
    // // );
    // // }
	 
		
	// //NAVISION
	// public function actRefreshFromNavAction(){
			
// // 		//u sve orders koji nemaju pbo ubacit pbo:
// // 		$orders = Orders::find(
// // 			array(
// // 				"n_pbo is Null",
// // 			)
// // 		);
// // 		foreach ($orders as $order) {
// // 			if ($order->getPbo()){
// // 				Nava::runSql("update orders set n_pbo = '".$order->getPbo()."' where id = ".$order->id);
// // 			}
// // 		}
		
// // 		//DOHVATIT SVE FAKTURE KOJE NEMAJU JIR I ZIK, U NAV-U POTRAŽITI JIR I ZIK I PREBACITI GA. AKO NE POSTOJI STAVITI N/A
// // 		$sveFaktBez = Nava::arrToDict(Nava::sqlToArray("select * from orders where n_invoice_jir is null"));
// // 		//$sveFaktBez = Nava::arrToDict(Nava::sqlToArray("select * from orders"));
// // 		foreach ( $sveFaktBez as $fakt ){
// // 			$nav = Nava::msSqlToArray(
// // 				"select "
// // 				." sih.[Issuer Protection Code] as zik "
// // 				.", sih.[Unique Invoice No_] as jir "
// // 				.", convert(varchar(30), sih.[Posting DateTime],120) as date_time "
// // 				." from [Oglasnik d_o_o_".'$'."Sales Invoice Header] sih "
// // 				." where [External Document No_] = '".$fakt['n_pbo']."' "
// // 			);
// // 			if ($nav){
// // 				error_log (gettype($nav[0]['date_time']));
// // 				error_log ($nav[0]['date_time']);
// // 				Nava::runSql(
// // 					"update orders set "
// // 					." n_invoice_zik = '".$nav[0]['zik']."' "
// // 					.", n_invoice_jir = '".$nav[0]['jir']."' "
// // 					.", n_invoice_date_time_issue = '".$nav[0]['date_time']."' "
// // 					." where id = ".$fakt['id']
// // 				); 
				
// // 				$this::actOrderStatusPlacenoAction($fakt['id']);
				
// // 			} else {
// // 				Nava::runSql(
// // 					"update orders set "
// // 					." n_invoice_zik = 'N/A' "
// // 					.", n_invoice_jir = 'N/A' "
// // 					." where id = ".$fakt['id']	
// // 				);
// // 			}
			
			
// // 		}
		
// // 		$sveFaktNav = Nava::msSqlToArray('select * from [Oglasnik d_o_o_$Sales Invoice Header] where [Posting date] > '."'2016-03-29'");
// // 		$sveFaktNav = Nava::msSqlToArray('select [External Document No_], [Issuer Protection Code],[Unique Invoice No_] from [Oglasnik d_o_o_$Sales Invoice Header] '
// // 										 ." where [External Document No_] between '900002000'and '910000000' and [Issuer Protection Code] > ''");
		

		
			
			
// // 		$orders = Nava::sqlToArray ("select * from orders where n_invoice_jir is null and id > 2300");
// // // 		$this->poruka(print_r($orders,true));

// // 		$ordNav = Nava::msSqlToArray(
// // 				'select [External Document No_], [Issuer Protection Code],[Unique Invoice No_] from [Oglasnik d_o_o_$Sales Invoice Header] '
// // 				."where 1=1 "
// // // 				." and LEN([External Document No_]) = 9 "
// // // 				." and LEFT([External Document No_],2) = '90' "
// // // 				." and [Posting Date] > '2016-03-29'"
// // // 				." and RIGHT([External Document No_],4)) >'2300' RIGHT([External Document No_],4)) <'2310'"
// // // 				." and [External Document No_] = '900002310'"
// // 				." and [External Document No_] between '900002310' and  '900002320'"
// // 				);
// // 		$this->poruka(print_r($ordNav,true));
			
		// $orders = Nava::sqlToArray ("select * from orders where n_invoice_jir is null and id > 2300");
// // 		$this->poruka(print_r($orders,true));

		// foreach($orders as $order){
			// $ordNav = Nava::msSqlToArray(
				// "select [External Document No_], [Issuer Protection Code],[Unique Invoice No_]"
				// .", convert(varchar(30), [Posting DateTime],120) as date_time "
				// .' from [Oglasnik d_o_o_$Sales Invoice Header] '
				// ."where 1=1 "
// // 				." and LEN([External Document No_]) = 9 "
// // 				." and LEFT([External Document No_],2) = '90' "
// // 				." and convert (INT, RIGHT([External Document No_],6)) = ".$order['id']
				// ." and [External Document No_] = '".$order['n_pbo']."'"
				// );
			// if ($ordNav) $this->poruka(print_r($ordNav,true));
			// if ($ordNav[0]['Issuer Protection Code'] > ''){
				// Nava::runSql(
					// "update orders set "
					// ." n_invoice_zik = '".$ordNav[0]['Issuer Protection Code']."' "
					// .", n_invoice_jir = '".$ordNav[0]['Unique Invoice No_']."' "
					// .", n_invoice_date_time_issue = '".$ordNav[0]['date_time']."' "
					// ." where id = ".$order['id']
				// );
				// $this::actOrderStatusPlacenoAction($order['id']);
			// }
		// }
			
		
// // 		$this->poruka(print_r($sveFaktNav,true));
		// return $this->redirect_back();
		

		
	// }
	
	// //OSTALO
	
	// // public function actUpdateOrderTotalAction($pOrdersId = null){
		// // //error_log ("actUpdateOrderTotalAction 1");
		// // Nava::updateOrderTotal($pOrdersId);
		// // $this->poruka("order total updated");
		// // return $this->redirect_back();
	// // }
    
    // //KREIRANJE NOVIH REDAKA U TABLICAMA
    
	// public function obradaAction(){
		
		// /*
		// //dodavanje patha u bazu 
		// $categories = Categories::find();
		// foreach ($categories as $category){
			// $category->n_path = $category->getPath();
			// //$category->update();
			// Nava::runSql("update categories set n_path = '$category->n_path' where id = $category->id");
		// }
		// */
		
	
		
		
		// //$ois = null;
		// //$ois = Nava::sqlToArray("select oi.id, from orders_items oi join orders o on (oi.order_id = o.id)");
		// //$ois = \Baseapp\Suva\Models\OrdersItems::find("id between 1 and 2000");
		// //$ois = \Baseapp\Suva\Models\OrdersItems::find("id between 2001 and 4000");
		// //$ois = \Baseapp\Suva\Models\OrdersItems::find("id between 4001 and 6000");
		// //$ois = \Baseapp\Suva\Models\OrdersItems::find();

		// // foreach($ois as $oi){	
			// // $oi->n_before_save();
			// // $oi->update();
			// // //error_log(" $oi->id ...  $oi->total  .. $oi->n_total_with_tax ");
			
		// // }
	
// // 		ZA UPDATEANJE TOTALA NA SVIM ORDERIMA
		// // $orders = Orders::find("n_status > 0 ");
		// // //$orders = Orders::find(" id = 2618 ");
		// // foreach($orders as $order)
			// // Nava::updateOrderTotal($order->id);
		
		// //$this->logger->info(print_r(Nava::sqlToArray("describe n_discounts"),true));
		// //return $this->redirect_self();
// // 		$this->n_novi_query('Discounts');

		// // //error_log("ActionsController:obradaAction 1");
		// // foreach ( \Baseapp\Suva\Models\Dictionaries::find() as $dict ){
			// // $path = $dict->getPath();
			// // $path = str_replace("'" , "", $path);
			// // //error_log("ActionsController:obradaAction 2 $path");
			// // Nava::runSql ("update dictionaries set n_path = '$path' where id = $dict->id");
			
			// // //$dict->update();
			
		// // } 
		
		
		// // //error_log("ActionsController:obradaAction 11");
		// // foreach ( \Baseapp\Suva\Models\Locations::find() as $loc ){
			// // $path = $loc->getPath();
			// // $path = str_replace("'" , "", $path);
			// // //error_log("ActionsController:obradaAction 12 $path");
			// // Nava::runSql ("update location set n_path = '$path' where id = $loc->id");
			
			// // //$dict->update();
			
		// // } 



		// // //error_log("ActionsController:obradaAction update categories path");
		// // foreach ( \Baseapp\Suva\Models\Categories::find() as $cat ){
			// // $path = $cat->getPath();
			// // $path = str_replace("'" , "", $path);
			// // //error_log("ActionsController:obradaAction 12 $path");
			// // Nava::runSql ("update categories set n_path = '$path' where id = $cat->id");
		// // } 

		
		
		// // foreach ( \Baseapp\Suva\Models\Ads::find ( " id between 800000 and 829999 "  ) as $ad ){
			
			// // $cart = $ad->User()->getCart();
			// // $product = \Baseapp\Suva\Models\Products::findFirst (138);
			
		
			
			// // $txtSql = "
				// // insert into orders_items (
					// // order_id
					// // ,ad_id
					// // ,product_id
					// // ,title
					// // ,price
					// // ,qty
					// // ,total
					// // ,tax_amount
					// // ,tax_rate
					// // ,n_price
					// // ,n_publications_id
					// // ,n_products_id
					// // ,n_total
					// // ,n_total_with_tax
					// // ,n_category_id
					// // ,n_is_active
					// // ,n_frontend_sync_status
					// // ,n_first_published_at
					// // ,n_expires_at
				
				// // )
				// // values (
					// // $cart->id
					// // ,$ad->id
					// // ,'$product->old_product_id'
					// // ,'created automatically for nekretnine.net test '
					// // ,'0'
					// // ,1
					// // ,'0'
					// // ,'0'
					// // ,0.25
					// // ,0.00
					// // ,5
					// // ,138
					// // ,0
					// // ,0
					// // ,$ad->category_id
					// // ,1
					// // ,'no_sync'
					// // ,'2016-09-01'
					// // ,'2016-12-31'
				// // )
			// // ";
			// // error_log ($txtSql);
			// // \Baseapp\Suva\Library\Nava::runSql( $txtSql );
			
		// //updateanje category mappinga
		// foreach ( \
		// \Suva\Models\CategoriesMappings::find() as $catMap ){
			// $catMap->n_before_save();
			// $catMap->update();
		// }
			
		// // }
		// $this->view->pick('suva/index');
	// }
	 
	// public function updateSearchTermsAction(){
		// //error_log("stavljanje show= 1 ovisno o parent category");
		
		// // $rootCategoryId = 24;
		// // //$ads_search_terms = \Baseapp\Suva\Models\AdsSearchTerms::find([ 'conditions'=>'id IN ('.implode(',',[205428,205562]).')' ]);
		// // $ads_search_terms = \Baseapp\Suva\Models\AdsSearchTerms::find();
		// // //error_log("NIKOLA 1". print_r($ads_search_terms->toArray(), true));
		
		// // foreach ( $ads_search_terms as $ast) {
			// // //error_log("NIKOLA FOREACH ");
			// // $ad = $ast->Ad();
			
			
			// // $category = null;
			// // $parent = null;
			// // $parent2 = null;
			// // $parent3 = null;
			// // $parent4 = null;
			
			// // $category = $ad->Category();
			// // if ($category) 	if ($category->parent_id > 0 )	$parent = \Baseapp\Suva\Models\Categories::findFirst("id = $category->parent_id");
			// // if ($parent) if ($parent->parent_id > 0 ) $parent2 = \Baseapp\Suva\Models\Categories::findFirst("id = $parent->parent_id");
			// // if ($parent2) if ($parent2->parent_id > 0 ) $parent3 = \Baseapp\Suva\Models\Categories::findFirst("id = $parent2->parent_id");
			// // if ($parent3) if ($parent3->parent_id > 0 ) $parent4 = \Baseapp\Suva\Models\Categories::findFirst("id = $parent3->parent_id");
			
			// // $show = 0;
			// // if ($category) if ($category->id == $rootCategoryId) $show = 1;
			// // if ($parent) if ($parent->id == $rootCategoryId) $show = 1;
			// // if ($parent2) if ($parent2->id == $rootCategoryId) $show = 1;
			// // if ($parent3) if ($parent3->id == $rootCategoryId) $show = 1;
			// // // if ($parent4) if ($parent4->id == $rootCategoryId) $show = 1;
			
			// // $txtSql = "UPDATE ads_search_terms SET show_NEKR = $show WHERE ad_id = $ad->id ";
			// // Nava::runsql($txtSql);
			
				// // // //error_log("nikola2 ". print_r($ad->toArray(
			
		// // } 
		
		
		// //
		
		// //postavljanje svega na 0 i null vezano uz NEKR
		// $txtSql = "update ads_search_terms	
					// set	show_NEKR = 0,
					 // category_text_NEKR = null,
					 // product_priority_NEKR = 0,
					 // product_expires_at_NEKR = null,
					 // orders_items_id_NEKR = null,
					 // category_text_NEKR = null,
					 // category_id_lvl_1_NEKR = null,
					 // category_id_lvl_2_NEKR = null,
					 // category_id_lvl_3_NEKR = null,
					 // category_id_lvl_4_NEKR = null,
					 // category_id_lvl_5_NEKR = null

					// ";
		
		// Nava::runsql($txtSql);
		
		// //error_log ("updateSearchTermsAction 1");
		
		
		
		// //postavljanje kategorija, n_path, category_text, show_nekr = 1...
		
		// $txtSql ="
		
// UPDATE ads_search_terms ast
							// JOIN (
								// select 
									// a.id as id 
									// ,c.n_path
								
									// ,c.level as c1lvl
									// ,c.id as c1id
									
									// ,c2.level as c2lvl
									// ,c2.id as c2id
									// ,c3.level as c3lvl
									// ,c3.id as c3id
									// ,c4.level as c4lvl
									// ,c4.id as c4id
									// ,c5.level as c5lvl
									// ,c5.id as c5id
									
								// from 
									// ads a 
										// join (
											// /* NOVO MAPIRANJE */									
											
											// select 
												// a.id as ad_id
											// /*	,pcm.mapped_category_id*/
											// /*	,pcm.mapped_dictionary_id*/
											// /*	,pcm.mapped_location_id*/
												// ,max(pcm.category_mapping_id) as category_mapping_id
											
											// from 
												// ads a
													// left join ads_parameters ap on (a.id = ap.ad_id)
														// join parameters p on (ap.parameter_id = p.id)
															// join parameters_types pt on (p.type_id = pt.id and pt.accept_dictionary = 1)
													// join ads_search_terms ast on a.id = ast.ad_id
													// join categories c on a.category_id = c.id
														// join n_publications_categories_mappings pcm on (pcm.mapped_category_id = c.id and pcm.is_active = 1 and pcm.publication_id = 5 )
															// join n_categories_mappings cm on (pcm.category_mapping_id = cm.id and cm.n_publications_id = 5 )
											// where 1=1
											// and 
											// (
											// (
											// ap.value is null 
											// and pcm.mapped_dictionary_id is null
											// )
											// or 
											// (
											// ap.value = cast(pcm.mapped_dictionary_id as CHAR )
											// )
											// or	a.country_id = pcm.mapped_location_id
											// or	a.county_id = pcm.mapped_location_id
											// or a.city_id = pcm.mapped_location_id
											// or pcm.mapped_location_id is null
											// )
											// group by
												// a.id
										
										
										
										
										// ) novo_mapiranje on (novo_mapiranje.ad_id = a.id)
											// JOIN n_categories_mappings c ON c.id = novo_mapiranje.category_mapping_id
												// left join n_categories_mappings c2 on c2.id = c.parent_id
													// left join n_categories_mappings c3 on c3.id = c2.parent_id
														// left join n_categories_mappings c4 on c4.id = c3.parent_id
															// left join n_categories_mappings c5 on c5.id = c4.parent_id
												
								// where 
									// novo_mapiranje.category_mapping_id in (
								
									
										// select id from 
										// (
											
											// select 
												// c.id
											// from n_categories_mappings c
												// left join n_categories_mappings c1 on c.parent_id = c1.id
													// left join n_categories_mappings c2 on c1.parent_id = c2.id
														// left join n_categories_mappings c3 on c2.parent_id = c3.id
																// left join n_categories_mappings c4 on c3.parent_id = c4.id
											// where 
												// c.id = 1416
												// or c1.id = 1416				
												// or c2.id = 1416				
												// or c3.id = 1416				
												// or c4.id = 1416			
											
											// union
											
											// select 
												// c1.id
											// from n_categories_mappings c
												// left join n_categories_mappings c1 on c.parent_id = c1.id
													// left join n_categories_mappings c2 on c1.parent_id = c2.id
														// left join n_categories_mappings c3 on c2.parent_id = c3.id
																// left join n_categories_mappings c4 on c3.parent_id = c4.id
											// where 
												// c.id = 1416
												// or c1.id = 1416				
												// or c2.id = 1416				
												// or c3.id = 1416				
												// or c4.id = 1416			
											
											// union
											
											// select 
												// c2.id
											// from n_categories_mappings c
												// left join n_categories_mappings c1 on c.parent_id = c1.id
													// left join n_categories_mappings c2 on c1.parent_id = c2.id
														// left join n_categories_mappings c3 on c2.parent_id = c3.id
																// left join n_categories_mappings c4 on c3.parent_id = c4.id
											// where 
												// c.id = 1416
												// or c1.id = 1416				
												// or c2.id = 1416				
												// or c3.id = 1416				
												// or c4.id = 1416			
											
											// union
											
											// select 
												// c3.id
											// from n_categories_mappings c
												// left join n_categories_mappings c1 on c.parent_id = c1.id
													// left join n_categories_mappings c2 on c1.parent_id = c2.id
														// left join n_categories_mappings c3 on c2.parent_id = c3.id
																// left join n_categories_mappings c4 on c3.parent_id = c4.id
											// where 
												// c.id = 1416
												// or c1.id = 1416				
												// or c2.id = 1416				
												// or c3.id = 1416				
												// or c4.id = 1416			
											
											// union
											
											// select 
												// c4.id
											// from n_categories_mappings c
												// left join n_categories_mappings c1 on c.parent_id = c1.id
													// left join n_categories_mappings c2 on c1.parent_id = c2.id
														// left join n_categories_mappings c3 on c2.parent_id = c3.id
																// left join n_categories_mappings c4 on c3.parent_id = c4.id
											// where 
												// c.id = 1416
												// or c1.id = 1416				
												// or c2.id = 1416				
												// or c3.id = 1416				
												// or c4.id = 1416			
											// ) p1 
											// where id >= 1416
											
										// )
									// ) t
									// ON t.id = ast.ad_id

							
									// SET 
									// ast.category_text_NEKR = (replace
									// (replace(replace(replace(replace(replace(replace(replace
									// (replace(replace(replace(replace(replace
									// (t.n_path,'ć','c'),'ž','z'),'č','c'),'š','s'),'Č','C'),'Ć','C'),'Š','S'),'Đ','D'),'đ','d'),'<',''),'>',''),'€','E'),'@','')) collate utf8_croatian_ci,
									

									// ast.show_NEKR = 1
									// ,ast.category_id_lvl_1_NEKR = (case 
										// when t.c1lvl = 1 then t.c1id
										// when t.c2lvl = 1 then t.c2id
										// when t.c3lvl = 1 then t.c3id
										// when t.c4lvl = 1 then t.c4id
										// when t.c5lvl = 1 then t.c5id
										// end)
									// ,ast.category_id_lvl_2_NEKR = (case 
										// when t.c1lvl = 2 then t.c1id
										// when t.c2lvl = 2 then t.c2id
										// when t.c3lvl = 2 then t.c3id
										// when t.c4lvl = 2 then t.c4id
										// when t.c5lvl = 2 then t.c5id
										// end) 
									// ,ast.category_id_lvl_3_NEKR = (case 
										// when t.c1lvl = 3 then t.c1id
										// when t.c2lvl = 3 then t.c2id
										// when t.c3lvl = 3 then t.c3id
										// when t.c4lvl = 3 then t.c4id
										// when t.c5lvl = 3 then t.c5id
										// end) 
								// ,ast.category_id_lvl_4_NEKR =  (case 
										// when t.c1lvl = 4 then t.c1id
										// when t.c2lvl = 4 then t.c2id
										// when t.c3lvl = 4 then t.c3id
										// when t.c4lvl = 4 then t.c4id
										// when t.c5lvl = 4 then t.c5id
										// end) 
								// ,ast.category_id_lvl_5_NEKR =  (case 
										// when t.c1lvl = 5 then t.c1id
										// when t.c2lvl = 5 then t.c2id
										// when t.c3lvl = 5 then t.c3id
										// when t.c4lvl = 5 then t.c4id
										// when t.c5lvl = 5 then t.c5id
										// end)
						// ";
										
		// Nava::runsql($txtSql);
		// //error_log ("updateSearchTermsAction 2");
		
		
		// /* postavljanje prioriteta proizvoda */
		// $txtSql = "UPDATE ads_search_terms ast 
						// JOIN (
							// SELECT oi.ad_id as ad_id , max(pro.priority_online) as priority
								// FROM orders_items oi 
									// JOIN  n_products pro ON oi.n_products_id = pro.id
							// WHERE oi.n_expires_at > NOW()
								  // AND
								  // oi.n_first_published_at < NOW() 
							// GROUP BY oi.ad_id
						// ) t
					// ON t.ad_id = ast.ad_id
					// SET ast.product_priority_NEKR = t.priority
					
					// where ast.show_NEKR = 1
					// ";
		// Nava::runsql($txtSql);
					
		// //error_log ("updateSearchTermsAction 3");

		
		
		// //postavljanje expires_at i order item id ovisno o prioritetu
		// //error_log("ActionsController:updateSearchTermsAction:  postavljanje expires_at i order item id ovisno o prioritetu");
		// $txtSql =  "
			// UPDATE ads_search_terms ast
				// JOIN (
					// SELECT oi.ad_id as ad_id , max(oi.n_expires_at) as expires_at , max(pro.priority_online) as priority, max(pro.name) as IME, max(oi.id) as order_item_id
						// FROM orders_items oi 
							// JOIN  ads_search_terms ast ON oi.ad_id = ast.ad_id 
							// JOIN  n_products pro ON oi.n_products_id = pro.id
					// WHERE
						// oi.n_expires_at > NOW() 
						// AND
					// oi.n_first_published_at < NOW()
						// AND
					// ast.product_priority_NEKR = pro.priority_online			 
						
					// GROUP BY oi.ad_id
				  // ) t
				
				// ON t.ad_id = ast.ad_id
			// SET 
				// ast.product_expires_at_NEKR = t.expires_at
				// , ast.orders_items_id_NEKR = t.order_item_id
				
			// where ast.show_NEKR = 1
			// ";
		// Nava::runsql($txtSql);	
		// //error_log ("updateSearchTermsAction 4");
		
		
		
		// //gašenje onih bez order itema ili expires at
		// //error_log("ActionsController:updateSearchTermsAction:  gašenje onih bez order itema ili expires at");
		// $txtSql = "
			// UPDATE ads_search_terms
				// SET show_NEKR = 0 
			// WHERE 
				// show_NEKR = 1 
				
				// AND 
				// ( 
				// product_expires_at_NEKR < NOW()
				// OR 
				// product_expires_at_NEKR is null
				// or
				// orders_items_id_NEKR IS NULL
				// OR
				// orders_items_id_NEKR = 0
				// )
		// ";
		
		// Nava::runSql($txtSql);
		
		// //error_log ("updateSearchTermsAction 5");
		
		
	// }	
    
	// // public function addToZipAction() {
		// // $zip = \Comodojo\Zip\Zip::create('XmlAndPictures.zip');
		
		// // // set path
		// // $path = $zip->setPath('/home/oglasnikwww/oglasnik.hr/current/public/assets/build/');
		// // // get path
		// // //$path = $zip->getPath();
		// // //error_log("ZIP PATH : $path");
		
		// // //$is_valid = \Comodojo\Zip\Zip::check('XmlAndPictures.zip');

		// // // using array as parameter
		// // //$zip->add( array('/assets/build/ads.css', '/assets/build/backend.css'));
		
		// // // chaining methods
		// // //$zip = $zip->add('/assets/build/ads.css')->add('/assets/build/backend.css')->add('/assets/build/suva.js');
		
		// // //$zip->listFiles();
		// // //$is_valid = \Comodojo\Zip\Zip::check('XmlAndPictures.zip');

		// // // declaring path
		// // $path->add('ads.css')->add('backend.css');
		
		
	// // }
     
    
}
