<?php

//OGLASNIK MYSQL POSTAVKE
$oglaHost = "127.0.0.1";
$oglaDb = "oglasnik_capella";
$oglaUser = "root";
$oglaPassword = "as9Thood";

//NAVISION SQL SERVER POSTAVKE
$navHost = "195.245.255.22";
$navPort = "1433";
$navDb = "OGLASNIK_PROD";
$navUser = "sa";
$navPassword = "Tassadar1337";


$dbOgla = null;
$dbNav = null;

function fCheckLockedTables(){
	$rs = sqlToArray("show open tables from test_oglasnik2_6 where In_use > 0");
	return $rs;
}

function fConnectOgla (){
	
	global $oglaHost;
	global $oglaDb;
	global $oglaUser;
	global $oglaPassword;

	try {
		
		fMsg("Connecting to $oglaHost ..");
		$db = new PDO("mysql:host=$oglaHost; dbname=$oglaDb", $oglaUser, $oglaPassword);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);// set the PDO error mode to exception 
		fMsg("Connected to $oglaHost successfully"); 
		return $db;
		
	}
	catch(PDOException $e){ 
		fMsg("SQL failed: " . $e->getMessage());
	}
}

function fConnectNav (){

	global $navHost;
	global $navPort;
	global $navDb;
	global $navUser;
	global $navPassword;

	try {
		
		fMsg("Connecting to $navHost ..");
		$db = new PDO("dblib:host=$navHost;port=$navPort;dbname=$navDb", $navUser, $navPassword );
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);// set the PDO error mode to exception 
		fMsg("Connected to $navHost successfully"); 
		return $db;
	}
	catch(PDOException $e){ 
		fMsg("SQL failed: " . $e->getMessage());
	}
}

function sqlToArray ( $pSql ){
	global $dbOgla;
	$rs = $dbOgla->query($pSql);
	$result = array();
	foreach ($rs as $row)
		array_push ($result, $row);
	return $result;
}

function runSql ( $pSql ){
	global $dbOgla;
	$dbOgla->query($pSql);
}
