<?php
 
echo "_______UPDATE ADS FROM FRONTEND___________________ ".PHP_EOL."POÈETAK:". date("Y-m-d h:i:sa") . PHP_EOL;

// include "/home/oglasnik2/oglasnik.hr/current/app/suva/library/libUserActions.php";
// require "/home/oglasnik2/oglasnik.hr/current/app/suva/library/libUserActions.php";
// fMsg("Prije poziva funkcije iz libUserActions"); PHP_EOL;
// echo " TEST ". PHP_EOL;
// echo getcwd(). PHP_EOL;
// $f = new libUserActions();
// $f->test();



$host = "192.168.0.113";
$gDebugLevel = 9;
/*debug level:
0 - ništa
1 - samo greške
2 - obavijesti da je connectano, koji ad se sinhronizira
9 - sve poruke za debagiranje
*/

try {
	$dbOgla = new PDO("mysql:host=127.0.0.1; dbname=oglasnik_capella", 'root', 'as9Thood');
    $dbOgla->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);// set the PDO error mode to exception 
    if ( $gDebugLevel >= 2 ) fMsg("Connected to ogladev.oglasnik successfully"); 
	
	$arrLockedTables = fCheckLockedTables(); 
	if ( $arrLockedTables ) {
		 if ( $gDebugLevel >= 2 ) fError("Locked tables exist: ".print_r( $arrLockedTables, true ));
 		exit();
	}
		//GLAVNA FUNKCIJA
	// updateAllActiveUnsyncedAds();  // glavna funkcija koja odraðuje sve aktivne nesyncane	
	updateAllUnsyncedAds();  // glavna funkcija koja odraðuje sve nesyncane (aktivne i neaktivne)
	
	//2.1.2017 - funkcija koja prebacuje u status plaćeno, zahtjev 2443
	fUpdateOrderStatusForOnlinePayment();
	
	//šta ovo radi i zašto?
	fMsg("updateOrdersItemsStatusAdInactive");
	updateOrdersItemsStatusAdInactive();

	fMsg("Updating pushups from frontend");
	updatePushupsFromFrontend();

	//TESTOVI
	//checkMissingSuvaProducts();
	//updateAdTestUnsync( 803395 ); // Pushup
	//updateAdTestUnsync( 842207 ); // Besplatni online i offline
	//updateAdTestUnsync( 842208 ); // Plaæeni online i offline
	//updateAdTestUnsync( 842209 ); // Istaknuti - naslovnica online i offline
	$dbOgla = null;
}
catch(PDOException $e){ 
     if ( $gDebugLevel >= 1  ) echo "SQL failed: " . $e->getMessage().PHP_EOL.PHP_EOL.PHP_EOL.PHP_EOL;
}

 if ( $gDebugLevel >= 2 ) echo PHP_EOL."KRAJ:". date("Y-m-d h:i:sa") . PHP_EOL;

	/*primjeri podataka

	1.  besplatni oglas - uopce se ne radi order i order item ad 842207
		ako ima ad koji je unsynced a nema order itema na njemu
			naæi proizvod za online i offline prema slijedeæim kriterijima: 
				ako ima samo jedan proizvod za publikaciju oglasnik koji ima old_product id onda se uzima njega
				ako ih ima više onda se traži rijeè iz teksta

	2. plaæeni online i offline: order 20927, ad  842208	,  order item 28258, 28257

	3. istaknuti - naslovnica order 20928, ad 842209, oi 28259, 28260 
	*/
	/* Pushup
	
	SQL:
		// //primjer ad sa pushupima
		// set @adId = 803395;
		// update orders_items set n_frontend_sync_status = 'unsynced' where ad_id = @adId;
		// update orders set n_frontend_sync_status = 'unsynced' where id in (select order_id from  orders_items  where ad_id = @adId) ;
		// update ads set n_frontend_sync_status = 'unsynced' where id = @adId;

		// set @adId = 803395;
		// select * from orders_items where ad_id = @adId;
		// select * from orders  where id in (select order_id from  orders_items  where ad_id = @adId) ;
		// select * from ads where id = @adId;
	*/
	/* Besplatni oglas online i offline 
		- uopce se ne radi order i order item ad 842207
		ako ima ad koji je unsynced a nema order itema na njemu
			naæi proizvod za online i offline prema slijedeæim kriterijima: 
				ako ima samo jedan proizvod za publikaciju oglasnik koji ima old_product id onda se uzima njega
				ako ih ima više onda se traži rijeè iz teksta
				
		// SQL
		set @adId = 842207;
		delete from orders_items set where ad_id = @adId;
		delete from orders orders set n_frontend_sync_status = 'unsynced' where id in (select order_id from  orders_items  where ad_id = @adId) ;
		update ads set n_frontend_sync_status = 'unsynced' where id = @adId;

		set @adId = 842207;
		select * from orders_items where ad_id = @adId;
		select * from orders  where id in (select order_id from  orders_items  where ad_id = @adId) ;
		select * from ads where id = @adId;
		
		
	*/
	/* naèin rada: 
	iæ po adovima koji nisu sincani i trenutno su aktivni.
		ako ad nema order itema 
			ako user nema free_ads dokument - napravit ga
				getFreeAdsOrderId()
		za svaki order item na adu
		ako ad nema order itema s online proizvodom koji nisu sincani
			parsirat online proizvod na adu  - processSerializedProduct()
			naæi suva proizvod - findSuvaProduct()
				ako nije besplatan - error (OrdersItems, cijena nije ista kao i cijena na Productu No.11 )
			createFrontendOrderItem()
			updateOrderItemForSuva()
		ako ad nema order itema s offline proizvodom koji nisu sincani, napravit
			parsirati offline proizvod - processSerializedProduct()
			naæi suva proizvod - findSuvaProduct()
				ako nije besplatan - error()
			createFrontendOrderItem()
			updateOrderItemForSuva()

		za svaki order item koji postoji 
			parsirat koji proizvod je naveden na OI - processSerializedProduct()
			naæi suva proizvod - findSuvaProduct()
			updateati order item - updateOrderItemForSuva()

		ako nema grešaka 
			ad.n_frontend_sync_status = synced
		
	funkcija fError( pModel = (Ads, Orders,OrdersItems), ModelId, Message)
		ubaci u tablicu audit
			user_id = 120096
			username = igornazor
			modelname
				Baseapp\Models\Ads
				Baseapp\Models\Orders
				Baseapp\Models\OrdersItems
			model_pk_val - ModelId
			message - message
			ip - 127.0.0.1
			
		echo na ekran
				
		
	funkcija getFreeAdsOrderId(ad.user_id)
		traži order sa n_statusom = 2  po useru
		ako ga naðe vraæa id
		ako ne naðe, kreira ga
			naæ kod u Navi ili u Actions za kreiranje carta

	funkcija createFrontendOrderItem(ad.id, cart_id, productSerialized)
		naæi kod u Navi
		na kraju fUpdateOrderTotals

	funkcija updateOrderItemForSuva (order_item.id)
		processSerializedProduct(oi.product)
		findSuvaProduct()
		usporediti cijenu na order itemu i cijenu na suva proizvodu
			ako nisu iste - error (OrdersItems, cijena nije ista kao i cijena na Productu No.11 )
		updateati na order itemu
			n_price
			n_publications_id
			n_products_id
			n_total
			n_total_with_tax tota- * tax_amount
			n_category_id
			n_is_active
			n_frontend_sync_status = synced
		ako je offline proizvod
			dodati slijedeæih insertiona koliko je broj objava
				iæ po svim issues, sttaus = prima, publikacija je OGLA
		
		
	funkcija fUpdateOrderForSuva (order_id)
		azurirat polja
			n_status
			n_total
			n_frontend_sync_status



	*/
	/* test spajanja na bazu*/{
		// echo PHP_EOL." TEST BAZE, UNSYNCED ORDERS:";
		// var_dump ( sqlToArray ( "select count(id) as broj from orders where n_frontend_sync_status = 'unsynced' " ) );
		// echo PHP_EOL;
		// return;
	}


//pozivajuæe funkcije
function updateAllActiveUnsyncedAds (){
	global $gDebugLevel;
	//Iæi po adovima koji nisu sincani i trenutno su aktivni.
	$txtSql = "select id from ads where active = 1  and n_frontend_sync_status = 'unsynced' ";
	$ads = sqlToArray($txtSql);
	fMsg("Query $txtSql found ".count($ads)." records. ");
	foreach ($ads as $ad){
		$adId = $ad['id'];
		fUpdateAdForSuva( $adId );
	}
}

function updateAllUnsyncedAds (){
	//Ici po adovima koji nisu syncani i trenutno su aktivni.
	global $gDebugLevel;
	$txtSql = "select id from ads where n_frontend_sync_status = 'unsynced' ";
	$ads = sqlToArray($txtSql);
	fMsg("Query $txtSql found ".count($ads)." records. ");
	foreach ($ads as $ad){
		$adId = $ad['id'];
		fUpdateAdForSuva( $adId, true );
	}
}

function updateOrdersItemsStatusAdInactive(){
	/*
		ako je oglas bio neaktivan u trenutku syncanja, imao je krive datume 
		published_at i expires_at, pa se nije moglo napraviti ispravan order item.
		Zato se order item stavlja u status ad_inactive. 
		Nakon što oglas postane aktivan ponovo se prolazi kroz te oglase i upisuje se pravi 
		first_published_at i expires_at
	
	*/
	
	global $gDebugLevel;
	$txtSql  =" 
		select oi.id 
		from orders_items oi 
			join ads a on (oi.ad_id = a.id and a.active = 1 and oi.n_frontend_sync_status = 'ad_inactive')
	";
	$ois = sqlToArray($txtSql);
	fMsg("Query $txtSql found ".count($ois)." records. ");
	foreach ($ois as $oi){
		$orderItemId = $oi['id'];
		fUpdateOrderItemForSuvaStatusAdInactive( $orderItemId );
	}
}

function updatePushupsFromFrontend(){
	/*
		Ide kroz sve order iteme koji nisu synced imaju n_source frontend i proizvod pushup
		
	*/
	
	$txtSql = "select id, order_id, product from orders_items where n_source = 'frontend' and n_frontend_sync_status in ('unsynced', 'ad_inactive') and product_id = 'pushup'";
	$ois = sqlToArray($txtSql);
	fMsg("Query $txtSql found ".count($ois)." records. ");
	foreach ($ois as $oi){
		$orderItemId = $oi['id'];
		$orderId = $oi['order_id'];
		$productSerialized = $oi['product'];
		fUpdateOrderItemForSuva( $orderItemId, $productSerialized  );
		fUpdateOrderForSuva ( $orderId, $pShowMessages );
	}
}

function updateAdTestUnsync ($pAdId = null ){
	global $gDebugLevel;
	if (!$pAdId ) return;
	
	runSql ("update orders_items set n_frontend_sync_status = 'unsynced' where ad_id = $pAdId ");
	runSql ("update orders set n_frontend_sync_status = 'unsynced' where id in (select order_id from  orders_items  where ad_id = $pAdId) ");
	runSql ("update ads set n_frontend_sync_status = 'unsynced' where id = $pAdId ");
	
	fUpdateAdForSuva($pAdId);
}

function updateAdTestDeleteOrders ($pAdId = null ){
	global $gDebugLevel;
	if (!$pAdId ) return;
	$orderId = sqlToArray("select max(order_id) as order_id from order_items where ad_id = $pAdId ")[0]['order_id'];
	if ( $orderId > 0 ) {
		runSql ("delete from orders where id = $orderId ");
		runSql ("delete from orders_items where order_id = $orderId ");
	}
	runSql ("update ads set n_frontend_sync_status = 'unsynced' where id = $pAdId ");
	
	fUpdateAdForSuva($pAdId);
}

function checkMissingSuvaProducts(){
	
	global $gDebugLevel;
	$ads = sqlToArray("select * from ads limit 10000");
	
	foreach ($ads as $ad){
		if ( $ad['online_product'] ){
			//fMsg("online product for ad ".$ad['id']);
			$product = fProcessSerializedProduct ( $ad['online_product'] );
			$suva = fFindSuvaProduct ( $product );
			if ( $suva ) fMsg ("online product for ad ".$ad['id'].",  Suva product ".$suva['id']);
			else fMsg ("online product for ad ".$ad['id'].",  Suva product NOT FOUND");
		}

		
		if ( $ad['offline_product'] ){
			//fMsg("offline product for ad ".$ad['id']);
			$product = fProcessSerializedProduct ( $ad['offline_product'] );
			$suva = fFindSuvaProduct ( $product );
			if ( $suva ) fMsg ("offline product for ad ".$ad['id'].",  Suva product ".$suva['id']);
			else fMsg ("offline product for ad ".$ad['id'].",  Suva product NOT FOUND");
		}
		
		// if ( $ad['offline_product'] ){
			// fMsg("offline product for ad ".$ad['id']);
			// $product = fProcessSerializedProduct ( $ad['offline_product'] );
			// //fMsg(print_r($product, true));
			// fFindSuvaProduct ( $product );
		// }

		
	}
	
	
}




//glavne funkcije
function fUpdateAdForSuva ( $pAdId = null , $pShowMessages = false ){
	global $gDebugLevel;
	if (!$pAdId) return fError("updateAdForSuva: Ad id not supplied");
	//$txtSql = "select * from ads where id =  $pAdId ";
	//2.1.2017 - ako nema ni online ni offline proizvod onda je u postupku predaje i ne synca se
	$txtSql = "select * from ads where id =  $pAdId and ( online_product_id is not null or offline_product_id is not null ) ";
	
	
	$ads = sqlToArray($txtSql);
	if (count ($ads) != 1 ) fError("fUpdateAdsForSuva Query $txtSql found ".count($ads)." records. instead of 1 ", "SYS_ERR");
	if ($pShowMessages) fMsg("Query $txtSql found ".count($ads)." records. ");
	foreach ($ads as $ad){
		
		$isError = false; 
		$ad_id = $ad['id'];
		echo PHP_EOL."Ad $ad_id ..";
		// unsync all order items and orders belonging to this ad
		//ne unsyncaj one koji imaju status no_sync
		runSql("update orders_items set n_frontend_sync_status = 'unsynced' where ad_id =  $ad_id and n_frontend_sync_status <> 'no_sync'");
		
		//unsyncaj order koji pripada adu
		runSql("update orders set n_frontend_sync_status = 'unsynced' where n_frontend_sync_status <> 'no_sync' and id in (select order_id from  orders_items  where ad_id = $ad_id)");
		runSql("update ads set n_frontend_sync_status = 'unsynced' where id = $ad_id");
		
		
		$ois = sqlToArray("select * from orders_items where ad_id =  $ad_id and n_frontend_sync_status <> 'no_sync'");
		//ako ad nema order itema , znači da su dodani besplatni proizvodi, pa treba kreirat order iteme za besplatne proizvode
		if ( count($ois) == 0 ){
			if ($pShowMessages) fMsg ("Ad $ad_id , user:".$ad['user_id'].", nema order itema 1");
			
			//ako user nema free_ads dokument - napravit ga
			$orderId = fGetFreeAdsOrderId ( $ad['user_id'] );
			if ($pShowMessages) fMsg ("Ad $ad_id - Free Ads Order = $orderId 2");
			
			//ako ima online proizvod
			if ( $ad['online_product_id'] ){
				if ($pShowMessages) fMsg ("Ad $ad_id - ima online proizvod 3");
				//napravit FrontEnd order item
				$orderItemId = fCreateFrontendOrderItem ( $orderId , $ad['online_product'], $ad['id'] );
				
				//ažurirat suva proizvod
				if ( $orderItemId ){
					
					if ($pShowMessages) fMsg ("Ad $ad_id - kreiran frontend order item $orderItemId za online proizvod 4");
					$status = fUpdateOrderItemForSuva ( $orderItemId,  $ad['online_product'], $pShowMessages );
					if ( !$status ) fError ("Ad $ad_id - order item $orderItemId  nije updatean");
					$isError = $status == true ? $isError : true;
					 
					//2.1.2017 - dodati broj syncanog order itema u ads
					if ($status){
						runSql("update ads set n_fe_online_order_item_id = $orderItemId where id = $ad_id");
					}
				}
				else {
					fError ("Ad $ad_id - nije kreirankreiran frontend order item  za online proizvod ");
					$isError = true;
				}
			}

			//ako ima offline proizvod, isto kao i za online
			if ( $ad['offline_product_id'] ){
				if ($pShowMessages) fMsg ("Ad $ad_id - ima online proizvod 5");
				
				$orderItemId = fCreateFrontendOrderItem ( $orderId , $ad['offline_product'], $ad['id'] );
				if ( $orderItemId ){
					if ($pShowMessages) fMsg ("Ad $ad_id - kreiran frontend order item $orderItemId za offline proizvod 6");
					$status = fUpdateOrderItemForSuva ( $orderItemId,  $ad['offline_product'], $pShowMessages );
					if ( !$status ) fError ("Ad $ad_id - order item $orderItemId  nije updatean");
					$isError = $status == true ? $isError : true;
					
					//2.1.2017 - dodati broj syncanog order itema u ads
					if ($status){
						runSql("update ads set n_fe_offline_order_item_id = $orderItemId where id = $ad_id");
					}
				}
				else {
					fError ("Ad $ad_id - nije kreirankreiran frontend order item  za offline proizvod ");
					$isError = true;
				}
			}
			
			$status = fUpdateOrderForSuva ( $orderId, $pShowMessages );
			if ( !$status ) fError ("Ad $ad_id - order $orderId  nije updatean");
			$isError = $status == true ? $isError : true;
			
		}
		else {
			/*
				za svaki order item koji postoji 
					parsirat koji proizvod je naveden na OI - processSerializedProduct()
					naæi suva proizvod - findSuvaProduct()
					updateati order item - updateOrderItemForSuva()
			*/
			if ($pShowMessages) fMsg ("Ad $ad_id IMA order itema 8");
			foreach ( $ois as $oi ){
				$oiId = $oi['id'];
				$orderId = $oi['order_id'];
				if ( $oi['n_frontend_sync_status'] == 'unsynced' ) {
					if ($pShowMessages) fMsg ("Ad $ad_id - za order item $oiId  9");
					$status = fUpdateOrderItemForSuva( $oiId , $oi['product'], $pShowMessages );
					$isError = $status == true ? $isError : true;
				}
				else {
					fError ("Order Item $oiId found to be synced while belonging to an unsynced ad $ad_id. Skipped."); 
				}
				
			$status = fUpdateOrderForSuva ( $orderId, $pShowMessages);
			$isError = $status == true ? $isError : true;
			}
		}
		if ( $isError === false ) {
			runSql ("update ads set n_frontend_sync_status = 'synced' where id = $ad_id ");
			echo ("  .. synced");
		}
		else fError ("Ad $ad_id not updated due to errors.");
		if ($pShowMessages) fMsg ("Ad $ad_id ------------------------------");
	}
}

function fUpdateOrderItemForSuvaStatusAdInactive( $pOrderItemId = null){
	/*
		Ako je ad na ovom order itemu aktivan a order item je imao status ad_inactive
		onda mu se upisuju first published at i expires at iz oglasa.
		Status ad_inactive dobije ako se synca s oglasom koji je trenutno neaktivan
		
		
		
		vraĆa: true ili false
	*/
	global $gDebugLevel;
	//PROVJERE PARAMETARA
	if ( !$pOrderItemId ) {
		$fError ("Order Item ID not supplied to function fUpdateOrderItemForSuvaStatusAdInactive ", "SYS_ERR");
		return null;
	}
	
 
	//DOHVAT MODELA
	$oi = sqlToArray(" select * from orders_items where id = $pOrderItemId ")[0];
	$ad = sqlToArray("select * from ads where id = ".$oi['ad_id'])[0];
	
	
	if ( $ad['active'] == 1  && $oi['n_frontend_sync_status'] == 'ad_inactive' ){
	
		//PRIPREMA SQL-A
		$n_first_published_at = date("Y-m-d H:i:s", $ad['first_published_at']);
		$n_expires_at = date("Y-m-d H:i:s", $ad['expires_at']);

		
		$txtSql = "
			update orders_items set
				n_first_published_at = '$n_first_published_at' 
				, n_expires_at = '$n_expires_at' 
				, n_frontend_sync_status = 'synced'
			where id = $pOrderItemId
		";
		
		//fMsg ($txtSql);
		runSql($txtSql);
		fMsg("fUpdateOrderItemForSuvaStatusAdInactive odradjeno");
	}

}



function fUpdateOrderItemForSuva( $pOrderItemId = null, $pProductSerialized = null, $pShowMessages = false ){
	/*
	funkcija updateOrderItemForSuva (order_item.id)
		processSerializedProduct(oi.product)
		findSuvaProduct()
		usporediti cijenu na order itemu i cijenu na suva proizvodu
			ako nisu iste - error (OrdersItems, cijena nije ista kao i cijena na Productu No.11 )
		updateati na order itemu
			n_price
			n_publications_id
			n_products_id
			n_total
			n_total_with_tax tota- * tax_amount
			n_category_id
			n_is_active
			n_frontend_sync_status = synced
		ako je offline proizvod
			dodati slijedeæih insertiona koliko je broj objava
				iæ po svim issues, status = prima, publikacija je OGLA
		
		vraæa: true ili false
	*/

	global $gDebugLevel;
	
	//PROVJERE PARAMETARA
	if ( !$pOrderItemId ) {
		$fError ("Order Item ID not supplied to function fUpdateOrderItemForSuva ", "SYS_ERR");
		return null;
	}
	
	if ( !$pProductSerialized ) {
		fError ("Serialized product not supplied to function fUpdateOrderItemForSuva ", "SYS_ERR");
		return null;
	}
 
	/*POSEBAN SLUÈAJ tamo di je 'n/a' bi trebalo bit syncano jer je nastalo iz SUVE, 
		ali kod masovnog unsyncanja se može dogodit sa ostane unsyncan, 
		pa ga samo treba vratit da je syncan */
	if ($pProductSerialized == 'n/a'  || $pProductSerialized == 'O:47:') {
		runSql ("update orders_items set n_frontend_sync_status = 'synced' where id = $pOrderItemId ");
		return true;
	}
 
	//DOHVAT MODELA
	$oi = sqlToArray(" select * from orders_items where id = $pOrderItemId ")[0];
	$order = sqlToArray("select * from orders where id = ".$oi['order_id'])[0];
	$ad = sqlToArray("select * from ads where id = ".$oi['ad_id'])[0];
	
	if ($pShowMessages) fMsg ("fUpdateOrderItemForSuva: Processing product from  order item  $pOrderItemId ");
	$frontendProduct = fProcessSerializedProduct ( $pProductSerialized );
	if ($pShowMessages) fMsg ( "Frontend product found:".print_r ($frontendProduct, true ) );
	$suvaProduct = fFindSuvaProduct ( $frontendProduct );
	if (!$suvaProduct ) {
		fError ("Suva product for orderitem $pOrderItemId not found.");
		return false;
	}
	
	$publication = sqlToArray("select * from n_publications where id = ".$suvaProduct['publication_id'])[0];
	
	//14.10.2016 - dogovor s Patricijom: Ako oglas nije aktivan, onda ostaje u statusu unsynced, i ponovo se pokušava sinhronizirati
	//PRIPREMA SQL-A
	$n_price = (((int)$oi['price']) / 100 + 0.0) * 0.8 ;//patricija - na frintendu su cijene s PDV-om
	$n_publications_id = $publication['id'];
	$n_products_id = $suvaProduct['id'];
	$n_total = (((int)$oi['total']) / 100) * 0.8;//patricija - na frintendu su cijene s PDV-om

	// //kod oglasa koji još nisu aktivni ne postoji first published at, samo created at i expires at
	// $n_first_published_at = date("Y-m-d H:i:s", ( $ad['first_published_at'] === null ? $ad['created_at'] : $ad['first_published_at'] ) );

	//2.1.2017 - dodaje se published_at a ne first_published_at (patricija)
	//kod oglasa koji još nisu aktivni ne postoji first published at, samo created at i expires at
	$n_first_published_at = date("Y-m-d H:i:s", ( $ad['first_published_at'] === null ? $ad['created_at'] : $ad['first_published_at'] ) );

	
	$n_expires_at = date("Y-m-d H:i:s", $ad['expires_at']);
	$n_total_with_tax = ((int)$oi['total']) / 100 + ((int)$oi['tax_amount']) / 100;
	$n_category_id = $ad['category_id'];
	$n_is_active = 1;
	$n_frontend_sync_status =   $ad['active'] == 1 ?  'synced' : 'ad_inactive';

	
	$txtSql = "
		update orders_items set
			n_price = $n_price
			, n_publications_id = $n_publications_id
			, n_expires_at = '$n_expires_at' 
			, n_products_id = $n_products_id
			, n_first_published_at = '$n_first_published_at' 
			, n_total = $n_total
			, n_total_with_tax = $n_total_with_tax
			, n_category_id = $n_category_id
			, n_is_active = $n_is_active
			, n_frontend_sync_status = '$n_frontend_sync_status'
		where id = $pOrderItemId
	";
	
	if ($pShowMessages) fMsg ($txtSql);
	runSql($txtSql);


	//INSERTIONI - ubacivanje insertiona za offline izdanje

	if ( $suvaProduct['is_online_product'] == 1 ) return true;
	//ISSUES - dohvat onoliko koliko je definirano na Suva productu
	//PRIPREMA SQL-A
	$publications_id = $publication['id'];
	$noIssues = $suvaProduct['days_published'] > 0 ?  $suvaProduct['days_published'] : 1 ;
	if ($pShowMessages) fMsg ("Suva proizvod ".$suvaProduct['id']. "ima  $noIssues objava.");
	
	$rs = sqlToArray( "select * from n_issues where status = 'prima' and publications_id = $publications_id limit $noIssues " );
	if ( count( $rs) != $noIssues ){
		fError (" wrong number of available issues found when creating insertions for order item $pOrderItemId, product ".$suvaProduct['id'].", publication $publications_id, ( $noIssues required and ".count( $rs)." available). MAYBE PRODUCT IS WRONGLY SET TO BE OFFLINE?" );
		return null;
	}
	$maxDate = 0;
	foreach ( $rs as $issue ){
		$dateIssue = strtotime($issue['date_published']);
		// fMsg("DATE ISSUE $dateIssue");
		if ( $dateIssue > $maxDate ) $maxDate = $dateIssue;
		// fMsg("IF DATE ISSUE > MAXDATE $maxDate");
		//PRIPREMA SQL-A
		$created_at = date ("Y-m-d h:I:s");
		$product_id = $suvaProduct['id'];
		$orders_items_id = $oi['id'];
		$issues_id = $issue['id'];

		$txtSql = "
			insert into n_insertions (
				created_at 
				, product_id 
				, orders_items_id 
				, issues_id 
			)
			values (
				'$created_at'
				, $product_id
				, $orders_items_id
				, $issues_id
			);
		";
		if ($pShowMessages) fMsg("ISSUE: $issues_id  SQL:".$txtSql);
		runSql($txtSql);
	}
	if ($maxDate > 0){
		$dateString = date("Y-m-d h:I:s", $maxDate);
		runSql("update orders_items set n_expires_at = '$dateString' where id = $pOrderItemId ");
		fMsg("Expires At for OrderItem $pOrderItemId update to $dateString");
	}
	
	return true;
	
}

function fUpdateOrderforSuva( $pOrderId ,$pShowMessages = false ){
	/*
	funkcija fUpdateOrderForSuva (order_id)
		azurirat polja
			n_status
			n_total
			n_frontend_sync_status
		vraæa: true ili false
	*/
	global $gDebugLevel;
	//PROVJERE PARAMETARA
	if ( !$pOrderId ) {
		$fError ("Order ID not supplied to function fUpdateOrderForSuva ", "SYS_ERR");
		return false;
	}
	
	//DOHVAT MODELA
	$order = sqlToArray("select * from orders where id = $pOrderId")[0];
	$fiscal_location = sqlToArray("select * from n_fiscal_locations where nav_fiscal_location_id = 'ONL1' limit 1")[0];
	$sumOrdersItems = sqlToArray("
		select 
			SUM(n_total) as n_total
			,SUM(n_total_with_tax) as n_total_with_tax
		from 
			orders_items
		where order_id = $pOrderId
	")[0];
	
	//PRIPREMA SQL-A
	
	//n_status
	/*
		const STATUS_NEW = 1; // Order record created (when "submit order" is clicked), could be payed offline
		const STATUS_COMPLETED = 2; // Order fulfilled/finalized
		const STATUS_CANCELLED = 3; // Order cancelled by the customer (or admin)
		const STATUS_EXPIRED = 4; // Will probably have to be processed via cron or something...
	*/

	
	$os = $order['status'];
	$orderTotal = (int)$order['total'];
	if ( $os == 1  and $orderTotal == 0 ) $n_status = 2;
	elseif ( $os == 1 and $orderTotal > 0 ) $n_status = 3;
	elseif ( $os == 2 and $orderTotal == 0 ) $n_status = 2;
	elseif ( $os == 2 and $orderTotal > 0 ) $n_status = 5;
	//elseif ( $os == 3 ) $n_status = 5;
	elseif ( $os == 4 ) $n_status = 9;
	else {
		fError (" Order $pOrderId : status not recognized: ".$order['status'], "SYS_ERR" );
		return false;
	}

	
	$n_total = $sumOrdersItems['n_total'] ? $sumOrdersItems['n_total'] : 0.00;
	$n_total_with_tax = $sumOrdersItems['n_total_with_tax'] ? $sumOrdersItems['n_total_with_tax'] : 0.00;
	$n_tax_amount = $n_total_with_tax - $n_total;
	$n_fiscal_location_id = $fiscal_location['id'];
	
	$txtSql = "
		update orders set
			n_status = $n_status
			, n_total = $n_total
			, n_tax_amount = $n_tax_amount 
			, n_total_with_tax = $n_total_with_tax 
			, n_fiscal_location_id = $n_fiscal_location_id
			, n_frontend_sync_status = 'synced'
		where id = $pOrderId
	";
	if ($pShowMessages) fMsg($txtSql);
	runSql($txtSql);
	
	return true;
}

function fProcessSerializedProduct( $pSerialized = null ){
	//TODO - sredit greške ako ne naðe nešto obavezno, u ['error'];
	/* REGEX
		'/   - poæetak stringa
		/'   - kraj stringa
		/s'  - kraj stringa, ako se hoæe matchat i linebreakovi
		(aabb) - kad se nadje aabb neka ga se doda u novi red arraya
		
		
	*/
	global $gDebugLevel;
	if (strlen ($pSerialized ) < 10) return null;
	
	$r = array (
		'is_online_product' => null,
		'name' => null,
		'old_product_id' => null,
		'old_product_option' => null,
		'old_product_extras' => null,
		'old_product_extras_options' => null,
		'is_pushup' => false,
		'cost' => null,
		'error' => null
	);
	
	
	//echo PHP_EOL."TEXT:".PHP_EOL.$pSerialized.PHP_EOL.PHP_EOL.PHP_EOL;

	$rgxOnline = '/Baseapp\\\\Library\\\\Products\\\\([^\\\\]+)\\\\([^\\\\^"]+)"/'; //matchat  Online ili Offline, i onda ime proizvoda 
	//matchat  Online ili Offline, i onda ime proizvoda 
 
	$rgxOnline = '/Baseapp\\\\Library\\\\Products\\\\([^\\\\]+)\\\\([^\\\\^"]+)".*s:5:"...id";s:[0-9]{1,2}:"([^"]+)";/'; // 
	$rgxOnline = '/Baseapp\\\\Library\\\\Products\\\\([^\\\\]+)\\\\([^\\\\^"]+)":[0-9]{1,2}:{s:5:"...id";s:[0-9]{1,2}:"([^"]+)";/'; // 
	preg_match ($rgxOnline, $pSerialized, $arrMatch);
	if( array_key_exists (1, $arrMatch) ){
		if( $arrMatch[1] == 'Online' ) $r['is_online_product'] = true;
		elseif( $arrMatch[1] == 'Offline' ) $r['is_online_product'] = false;
	}
	$r['name'] = array_key_exists( 2, $arrMatch ) ? $arrMatch[2] : null;
	$r['old_product_id'] = array_key_exists (3, $arrMatch ) ? $arrMatch[3] : null;
	//var_dump ($arrMatch);

	
	if ( $r['name'] === 'Pushup' ){
		//fMsg ("tu sam".PHP_EOL);
		//traženje online_product_id  i spremanje u old_product_option
		
		//ako je Pushup onda se traži s:17:"online_product_id";s:16:"istaknuti-online"; i   s:7:" * cost";s:4:"1500";
		$rgxOption = '/s:17:"online_product_id";s:[0-9]{1,2}:"([^"]+).*s:7:"...cost";s:[0-9]{1,2}:"([0-9]+)";/s';
		
		preg_match ($rgxOption, $pSerialized, $arrMatch);
		$r['is_pushup'] = true;
		$r['old_product_id'] = 'push-up';
		$r['old_product_option'] = array_key_exists( 1, $arrMatch ) ? $arrMatch[1] : null;
		$r['cost'] = array_key_exists( 2, $arrMatch ) ? ((int)$arrMatch[2])/100 : null;
		//var_dump ($arrMatch);
	}
	else{
		//za sve koji nisu Pushup
		
		//traženje opcije
		$rgxOption = '/selected";s:[0-9]{1,2}:"([^"]+)"/'; // matchaj ;s:11:" * selected";s:7:"60 dana";s:18:"
		preg_match ($rgxOption, $pSerialized, $arrMatch);
		$r['old_product_option'] = array_key_exists ( 1 , $arrMatch )  ? $arrMatch[1] : null;
		//var_dump ($arrMatch);
		
		//traženje extras 
		$rgxExtras = '/selected_extras";a:[0-9]{1,2}:.s:[0-9]{1,2}:"([^"]+)";s:[0-9]{1,2}:"([^"]+)"/';//matchati: * selected_extras";a:1:{s:20:"Objava na naslovnici";s:6:"3 dana"
		preg_match ($rgxExtras, $pSerialized, $arrMatch);
		$r['old_product_extras'] = array_key_exists( 1, $arrMatch ) ? $arrMatch[1] : null;
		$r['old_product_extras_options'] = array_key_exists( 2, $arrMatch ) ? $arrMatch[2] : null;
		//var_dump ($arrMatch);
	}
	
	
	
	$errMsg = null;
	if ( $r['is_online_product'] === null ) $errMsg .=  "is_online_product, ";
	if (! $r['name'] ) $errMsg .=  "name, ";
	if (! $r['old_product_id'] ) $errMsg .=  "old_product_id, ";
	$errMsg = $errMsg ? "Could not find ".$errMsg : $errMsg ;
	if ( $r['is_pushup']  && !( $r['old_product_option'] && $r['cost'] ) ) $errMsg .= "  Pushup product parameters not found.";
	
	if ($errMsg) {
		$r['error'] = $errMsg;
		fError ($errMsg." for serialized product:".PHP_EOL.$pSerialized, "SYS_ERR");
	}

	
	
	return $r;
	
}

function fFindSuvaProduct ( $p ){
	global $gDebugLevel;
	if ( $p['old_product_id'] == null ) {
		fError("parameters supplied to fFindSuvaProduct do not contain old_product_id :".print_r($p, true), "SYS_ERR");
		return null;
	}
	
	$txt_old_product_id = " and old_product_id = '".$p['old_product_id']."' ";
	
	$txt_old_product_option = $p['old_product_option'] === null ? " and old_product_option is null " : " and old_product_option = '".$p['old_product_option']."' ";
	
	$txt_old_product_extras = $p['old_product_extras'] === null ? " and old_product_extras is null " : " and old_product_extras = '".$p['old_product_extras']."' ";
	
	$txt_old_product_extras_options = $p['old_product_extras_options'] === null ? " and old_product_extras_options is null " : " and old_product_extras_options = '".$p['old_product_extras_options']."' ";
	
	$txtSql = "select * from n_products where 1 = 1 $txt_old_product_id $txt_old_product_option $txt_old_product_extras $txt_old_product_extras_options ";
	
	$rs = sqlToArray($txtSql );

	//Ako upit ne naðe toèno 1 suva proizvod, greška
	if (count( $rs ) !== 1) {
		fError  ("error, query: $txtSql ".PHP_EOL."Found ".count( $rs )." records!!");
		return null;
	} 
	else return $rs[0];
	
}

function fGetFreeAdsOrderId ( $pUserId = null ){
	/*	
		funkcija getFreeAdsOrderId(ad.user_id)
			traži order sa n_statusom = 2  po useru
			ako ga naðe vraæa id
			ako ne naðe, kreira ga
				naæ kod u Navi ili u Actions za kreiranje carta
	*/
	
	global $gDebugLevel;
	$txtSql = " select * from orders where n_status = 2 and user_id = $pUserId order by id desc";
	$rs = sqlToArray( $txtSql );

	$jePostoji = count($rs) > 0 ? true : false;

	if ($jePostoji) return $rs[0]['id'];
	else {
		//kreiranje nove liste free ads
		runSql("insert into orders (status, total, created_at, modified_at) values (1,0, now(), now() )");
		
		$newId = sqlToArray("select max(id) as id from orders ")[0]['id'];
		
		
		
		$txtSql =  " update orders set "
		  ." user_id = $pUserId "
		  .", n_status = 2 "
		  .", status = 1 "
		  .", n_source = 'frontend_sync' "
		  
		  ." where id = $newId ";
		
		//fMsg ($txtSql);
		runSql($txtSql);
		return $newId;
	}
	
}
	
function fCreateFrontendOrderItem( $pOrderId = null, $pProductSerialized = null, $pAdId = null ){
	/*
	funkcija createFrontendOrderItem(ad.id, cart_id, productSerialized)
		naæi kod u Navi
		na kraju fUpdateOrderTotals
	
		vraæa: orderItemId ili null
	*/

	global $gDebugLevel;
	global $dbOgla;
	//PROVJERE PARAMETARA
	if ( !$pOrderId ) {
		$fError ("Order ID not supplied to function fCreateFrontendOrderItem ", "SYS_ERR");
		return null;
	}
	
	if ( !$pProductSerialized ) {
		$fError ("Serialized product not supplied to function fCreateFrontendOrderItem ", "SYS_ERR");
		return null;
	}
	
	if ( !$pAdId) {
		$fError ("Ad ID not supplied to function fCreateFrontendOrderItem ", "SYS_ERR");
		return null;
	}

	//DOHVAT MODELA
	//fMsg ("fCreateFrontendOrderItem: Processing product from  ad $pAdId ");
	$frontendProduct = fProcessSerializedProduct ( $pProductSerialized );
	if ( $frontendProduct['error'] ) {
		fError ( $frontendProduct['error'] );
		return null;
	}
	$suvaProduct = fFindSuvaProduct( $frontendProduct );
	
	$order = sqlToArray(" select * from orders where id = $pOrderId ")[0];
	$ad = sqlToArray("select * from ads where id = $pAdId ")[0];
	
	//PRIPREMA SQL-A
	$adId = $ad['id'];
	$oldProductId = $frontendProduct['old_product_id'];
	//$productSerialized = mysql_real_escape_string ( $pProductSerialized );
	$productSerializedQuoted = $dbOgla->quote( $pProductSerialized );  //funkcija na dbOgla - treba global dbOgla
	// echo PHP_EOL."Quoted:".$productSerializedQuoted.PHP_EOL;
	// die();
	$title = $frontendProduct['old_product_id'] . "- created by batch";
	$price = (int)$suvaProduct['unit_price'] * 100;
	$qty = 1;
	$total = $price;
	$taxRate = 0.25;
	$taxAmount = (int)($taxRate * $total);
	$nSource = 'frontend_sync';
	
	$txtSql = "
		insert into orders_items ( 
			order_id 
			,ad_id 
			,product_id 
			,product 
			,title 
			,price 
			,qty 
			,total 
			,tax_rate 
			,tax_amount
			,n_source
			)  
		values (
			$pOrderId
			,$pAdId
			,'$oldProductId'
			,$productSerializedQuoted
			,'$title'
			,'$price'
			,'$qty'
			,'$total'
			,'$taxRate'
			,'$taxAmount'
			,'$nSource'
		)
	";
	
	//echo PHP_EOL."SQL Quoted:".$txtSql.PHP_EOL;
	runSql($txtSql);
	
	
	$newId = sqlToArray("select max(id) as id from orders_items ")[0]['id'];
	return $newId;
}


function fUpdateOrderStatusForOnlinePayment(){
	/*
		traži ordere koji nisu u statusu paid a pripadaju oglasima kojima je latest payment state 5
	
	*/
	global $gDebugLevel;
	
	$txtSql = "
	select 
		*
	from 
		orders
	where
		status = 2 
		and n_status in ( 3,4 ) 
		and n_frontend_sync_status = 'synced'
		and n_source = 'frontend'
	";
	$rsOrders = sqlToArray ($txtSql);
	if ($rsOrders){
		$rsSettings = sqlToArray("select value_decimal as id  from n_app_settings where name = 'fin_default_wspay_operator_id'" );
		$operatorId =  ( $rsSettings && $rsSettings[0]['id'] > 0  ? intval($rsSettings[0]['id']) : null ); 
		
		$rsSettings = sqlToArray("select value_decimal as id  from n_app_settings where name = 'fin_default_wspay_fiscal_location_id'" );
		$fiscalLocationId =  ( $rsSettings && $rsSettings[0]['id'] > 0  ? intval($rsSettings[0]['id'])  : null ); 
		$navSyncStatus = 'unsynced';
		if ( ! $fiscalLocationId || !$operatorId ){
			fError ("system paremeters fin_default_wspay_fiscal_location_id or fin_default_wspay_operator_id  not set");
			return;
		}
		foreach ($rsOrders as $o){
			$oId = $o['id'];
			$frontendPaymentMethod = $o['payment_method'];
			$txtSql = "select id from n_payment_methods where frontend_payment_method ='$frontendPaymentMethod'";
			$rsPm = sqlToArray($txtSql);
			$pmId = null;
			if ( $rsPm && $rsPm[0]['id'] > 0 ) $pmId = $rsPm[0]['id'];
			if ( $pmId ){
				
				$txtSql = "
					update orders set
						n_status = 5
						,n_payment_methods_id = $pmId
						,n_operator_id = $operatorId
						,n_fiscal_location_id = $fiscalLocationId
						,n_invoice_date_time_issue = now()
						,n_payment_date = now()
						,n_nav_sync_status = '$navSyncStatus'
					where id = $oId
				";
				fMsg( $txtSql );
				runSql ( $txtSql );
			}
			else{
				fError ("Unable to synchronize frontend payment method  $frontendPaymentMethod ");
			}
		}
	}
}

//pomoćne funkcije
function fError ( $pMessage = "", $pType = "ERROR", $pModel = "", $pModelId = null ){

/*
funkcija error( pModel = (Ads, Orders,OrdersItems), ModelId, Message)
ubaci u tablicu audit
	user_id = 120096
	username = igornazor
	modelname
		Baseapp\Models\Ads
		Baseapp\Models\Orders
		Baseapp\Models\OrdersItems
	model_pk_val - ModelId
	message - message
	ip - 127.0.0.1
	
echo na ekran
*/
	global $gDebugLevel;
	if (!$pMessage ) return false;

	$message = "FE SYNC $pType : ".$pMessage;
	$txtSql = "
	insert into audit (user_id, username, message, ip, type, created_at)
	values (
	120096
	,'igornazor'
	,'$message'
	,'127.0.0.1'
	,'S'
	,'".date("Y-m-d h:I:s")."'
	)
	";

	//ZA SADA NE U BAZU
	//runSql($txtSql);

	echo PHP_EOL.date("Y-m-d h:I:s")." ".$message;
}

function fCheckLockedTables(){
	global $gDebugLevel;
	$rs = sqlToArray("show open tables from test_oglasnik2_6 where In_use > 0");
	return $rs;
}

function fMsg ( $pMessage , $pTitle = "" ){
	global $gDebugLevel;
	$msg = is_array($pMessage) ? print_r($pMessage, true) : $pMessage;
	$title = $pTitle ? $pTitle.":".PHP_EOL : "";
	
	echo PHP_EOL.$title.$msg;
}



function sqlToArray ( $pSql ){
	global $gDebugLevel;
	global $dbOgla;
	$rs = $dbOgla->query($pSql);
	$result = array();
	foreach ($rs as $row)
		array_push ($result, $row);
	return $result;
}

function runSql ( $pSql ){
	global $gDebugLevel;
	global $dbOgla;
	$dbOgla->query($pSql);
}

