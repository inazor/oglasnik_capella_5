{# Admin Home View #}

<div class="row">
    <div class="col-lg-12 col-md-12">
        <h1 class="page-header">Dashboard</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12">
		{{ partial("partials/blkFlashSession") }}
    </div>
</div>

<div class="row">
    {% if auth.logged_in(['admin','support','moderator']) %}
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <span class="fa fa-file-text fa-5x"></span>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{ topCounter['total_ads_today'] }}</div>
                        <div>Total ads submitted today!</div>
                    </div>
                </div>
            </div>
            <a href="{{ url('admin/ads?field=created_at&q=' ~ date('Y-m-d')) }}">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><span class="fa fa-arrow-circle-right"></span></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    {% endif %}
    {% if auth.logged_in(['admin','finance','support']) %}
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <span class="fa fa-shopping-cart fa-5x"></span>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{ topCounter['orders'] }}</div>
                        <div>New Orders!</div>
                    </div>
                </div>
            </div>
            <a href="{{ url('admin/orders?status=' ~ constant("Baseapp\Models\Orders::STATUS_NEW")) }}">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><span class="fa fa-arrow-circle-right"></span></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    {% endif %}
    {% if auth.logged_in(['admin','support','moderator']) %}
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <span class="fa fa-exclamation-triangle fa-5x"></span>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{ topCounter['reported_ads'] }}</div>
                        <div>Reported ads!</div>
                    </div>
                </div>
            </div>
            <a href="{{ url('suva/ads2?moderation=' ~ constant("Baseapp\Library\Moderation::MODERATION_STATUS_REPORTED")) }}">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><span class="fa fa-arrow-circle-right"></span></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    {% endif %}
</div> 
{% if auth.logged_in(['admin','support','moderator']) %}
<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <span class="fa fa-eye fa-5x"></span>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{ topCounter['paid_waiting_for_moderation'] }}</div>
                        <div><b>Paid</b> ads waiting for moderation!</div>
                    </div>
                </div>
            </div>
            <a href="{{ url('suva/ads2/awaiting-moderation?payment_type=paid&order_by=id&dir=desc&limit=20') }}">
                <div class="panel-footer">
                    <span class="pull-left">View <b>paid</b> ads</span>
                    <span class="pull-right"><span class="fa fa-arrow-circle-right"></span></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <span class="fa fa-eye fa-5x"></span>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{ topCounter['ordered_waiting_for_moderation'] }}</div>
                        <div><b>Ordered</b> ads waiting for moderation!</div>
                    </div>
                </div>
            </div>
            <a href="{{ url('suva/ads2/awaiting-moderation?payment_type=ordered&order_by=id&dir=desc&limit=20') }}">
                <div class="panel-footer">
                    <span class="pull-left">View <b>ordered</b> ads</span>
                    <span class="pull-right"><span class="fa fa-arrow-circle-right"></span></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <span class="fa fa-eye fa-5x"></span>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{ topCounter['free_waiting_for_moderation'] }}</div>
                        <div><b>Free</b> ads waiting for moderation!</div>
                    </div>
                </div>
            </div>
            <a href="{{ url('suva/ads2/awaiting-moderation?payment_type=free&order_by=id&dir=desc&limit=20') }}">
                <div class="panel-footer">
                    <span class="pull-left">View <b>free</b> ads</span>
                    <span class="pull-right"><span class="fa fa-arrow-circle-right"></span></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
</div>
{% endif %}
{% if registered_users_by_month %}
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <script>
                var registered_users_by_month = [];
                {% for key, data in registered_users_by_month %}
                registered_users_by_month.push({y: '{{ key }}', a: {{ data['private']|default(0) }}, b: {{ data['company']|default(0) }}});
                {% endfor %}
            </script>
            <div class="panel-heading">
                Registered users (by months)
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-3 col-md-4 hidden-sm hidden-xs">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center"><small><strong>Month/Year</strong></small></th>
                                        <th class="text-center"><small><strong># Private</strong></small></th>
                                        <th class="text-center"><small><strong># Company</strong></small></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {% for key, data in registered_users_by_month %}
                                    <tr>
                                        <td class="text-center">{{ key }}</td>
                                        <td class="text-center">{{ data['private']|default(0) }}</td>
                                        <td class="text-center">{{ data['company']|default(0) }}</td>
                                    </tr>
                                    {% endfor %}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-8">
                        <div id="registered-users-chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{% endif %}
