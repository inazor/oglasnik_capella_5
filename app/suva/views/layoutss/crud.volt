{% set _form_id = (_context['_form_id'] is defined ) ? ( _context['_form_id'] ) : '1' %}

{% set _subform_id = 0 %}
{% if errors is not defined %}{% if _context['errors'] is defined %}{% set errors = _context['errors'] %}{% endif %}{% endif %}
{% set _is_ajax_submit = (_context['_is_ajax_submit'] === true ) ? true : false %}

{% if entity is not defined and p is defined %}{% if p['entity'] is defined %}{% set entity = p['entity'] %}{% endif %}{% endif %}

{% set _context['_form_url'] = '/suva/layoutss/crud' ~ ( entity.id > 0 ? ('/' ~ entity.id ) : '' ) %}





<div class="row">
	<div class="col-lg-12 col-md-12">
		<div><h1 class="page-header">{% if entity.id is not defined %}New {% endif %}Layout</h1></div>
		{{ partial("partials/blkFlashSession") }}
		{% if entity is defined %}
			{{ form(NULL, 'id' :  '_form_' ~ _form_id , 'method' : 'post', 'autocomplete' : 'off') }}
				{{ hiddenField('_csrftoken') }}
				{{ hiddenField('next') }}
				<input type="hidden" name="id" value="{{entity.id}}"></input>
				
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								{% if entity.id is defined %}
									<div class="pull-right">[{{ _m.t('ID:') }} <b>{{ entity.id }}</b> ]</div>
								{% endif %}
								<h3 class="panel-title">{{ _m.t('Layout details') }}</h3>
							</div>
							<div class="panel-body">
								<div class="row">
									{{ partial("partials/ctlText",['value':entity.name, 'title': _m.t('Name'), 'width':3, 'field':'name', '_context':_context] ) }} 
									{{ partial("partials/ctlTextArea",['value':entity.description, 'title': _m.t('Description'), 'width':6, 'field':'description'] ) }}
								</div>
								<div class="row">
									{{ partial("partials/ctlTextArea",['value':entity.xml, 'title': _m.t('XML Code'), 'width':12, 'field':'xml', 'cols':80, 'rows':10] ) }}
								</div>
								<div class="row">
									{{ partial("partials/ctlText", [
										'value': entity.variables, 
										'title':  _m.t('Variables Used In XML'), 
										'width': 4, 
										'field': 'variables'
									] ) }}
									{{ partial("partials/ctlCheckbox",['value':entity.is_active, 'title': _m.t('Is Active'), 'width':1, 'field':'is_active', 'errors':errors] ) }} 
									
								</div>
							</div>
						</div>
					</div>
				</div>
				{% include "partials/btnSaveCancel.volt" %}
			{{ endForm() }}
		{% else %}
			{{ _m.t('ENTITY NIJE DEFINIRAN') }}
		{% endif %}
	</div>
</div>

<script src="//cdn.ckeditor.com/4.5.9/basic/ckeditor.js"></script>
