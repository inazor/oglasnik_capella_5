{# Email Templates Layout #}
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>{{ title }}</title>
    <style type="text/css">
        .ExternalClass {width:100%;}
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height:20px; }
        body { -webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
        body { margin:0; padding:0; }
        table td { border-collapse:collapse; }
    </style>
</head>
<body style="background:#ffffff; min-height:0; color:#666666;font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:20px;"
      alink="#009FE3" link="#009FE3" bgcolor="#FFFFFF" text="#666666" yahoo="fix">
<div id="body_style" style="padding:0;">
    <table cellpadding="10" cellspacing="0" border="0" bgcolor="#FFFFFF" width="100%" align="center" style="border-collapse:collapse;">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF" width="100%" align="center" style="border-collapse:collapse;">
                    <tr><td><br /></td></tr>
                    <tr>
                        <td width="100%">
                            <img src="cid:email-logo-header" alt="oglasnik.hr" title="oglasnik.hr" width="197" height="37" style="display:block;" border="0"/>
                        </td>
                    </tr>
                    <tr><td><br /></td></tr>
                    <tr>
                        <td width="100%">
                            <h1 style="color:#333333;font-size:22px;line-height:27px;font-weight:normal;margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;">{{ title }}</h1>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF" width="100%" align="center" style="border-collapse:collapse;">
                                <tr><td><br /></td></tr>
                                <tr>
                                    <td width="100%" height="2" bgcolor="#009FE3" style="background-color:#009FE3;"></td>
                                </tr>
                                <tr><td><br /></td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%">
                            {{ content() }}
                        </td>
                    </tr>
                    <tr>
                        <td width="100%">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF" align="center" style="border-collapse:collapse;">
                                <tr><td><br /></td></tr>
                                <tr>
                                    <td width="100%" height="2" bgcolor="#009FE3" style="background-color:#009FE3;"></td>
                                </tr>
                                <tr><td><br /></td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%">
                            <table width="100%" cellpadding="20" cellspacing="0" border="0" bgcolor="#F2F2F2" width="100%" align="center" style="border-collapse:collapse;">
                                <tr>
                                    <td style="background-color:#f2f2f2;color:#666666;font-size:12px;line-height:20px;">
                                        {{ _m.t('Kontaktirajte nas:') }}<br />
                                        <a href="mailto:kontakt@oglasnik.hr" style="color:#009FE3;"><span style="color:#009FE3;font-weight:bold;">{{ _m.t('kontakt@oglasnik.hr') }}</span></a> {{ _m.t('| ') }}<a href="tel:+38516102800" style="color:#009FE3;"><span style="color:#009FE3;font-weight:bold;">{{ _m.t('01 6102-800') }}</span></a><br />
                                        <b>{{ _m.t('Oglasnik d.o.o.') }}</b><br />
                                        <b>{{ _m.t('Savska cesta 41') }}</b><br />
                                        <b>{{ _m.t('10000 Zagreb') }}</b>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr><td><br /></td></tr>
                    <tr>
                        <td width="100%">
                            <img src="cid:email-logo-footer" alt="oglasnik.hr" title="oglasnik.hr" width="113" height="23" style="display:block;" border="0"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
