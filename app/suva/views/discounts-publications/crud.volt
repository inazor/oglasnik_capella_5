{% if _is_ajax_submit is not defined %}
	{% set _is_ajax_submit = '' %}
{% endif %}

<div class="row">
	<div class="col-lg-12 col-md-12">
		<div><h1 class="page-header">{% if entity.id is not defined %}New {% endif %}Discounts on Publication</h1></div>
		{{ partial("partials/blkFlashSession") }}
		{% if entity is defined %}
			{{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
				{{ hiddenField('_csrftoken') }}
				{{ hiddenField('next') }}
				<input type="hidden" name="id" value="{{entity.id}}"></input>
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								{% if entity.id is defined %}
									<div class="pull-right">[ID: <b>{{ entity.id }}</b> ]</div>
								{% endif %}
								<h3 class="panel-title">Discounts on Publications details</h3>
							</div>
							<div class="panel-body">
								<div class="row">
								
									{{ partial("partials/ctlDropdown",['value':entity.discounts_id, 'title':'Discount', 'list':_model.Discounts(), 'width':4, 'field':'discounts_id', 'errors': errors] ) }}							
									{{ partial("partials/ctlDropdown",['value':entity.publications_id, 'title':'Publication', 'list':_model.Publications(), 'width':5, 'field':'publications_id', 'errors': errors] ) }}						
									{{ partial("partials/ctlCheckbox",['value':entity.is_active, 'title':'is active', 'width':3, 'field':'is_active', 'errors': errors] ) }}

								</div>
							</div>
						</div>
					</div>
				</div>
				{% include "partials/btnSaveCancel.volt" %}
			{{ endForm() }}
		{% else %}
			ENTITY NIJE DEFINIRAN
		{% endif %}
	</div>
</div>