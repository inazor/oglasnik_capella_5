{# Admin User Listing/View #}

{% if pagination_links is not defined %}
	{% set pagination_links = '' %}
{% endif %}
<div class="row">
	
	<div class="col-lg-12 col-md-12">
		<div>
			
			<h1 class="page-header">Actions</h1>
		</div>
		<br/>
		{{ partial("partials/btnAddNew", ['controller':'actions'] ) }}
		{{ partial("partials/blkFlashSession") }}
		<table class="table table-striped table-hover table-condensed">

			{{ partial("partials/blkTableSort",[
				'_context' : _context,
				'fields':[ 
					['title':'Options'													,'width':220	,'show_navigation' : true ] 
					,['title':'ID'						,'name':'id'					,'width':100	,'search_ctl':'ctlNumeric'	]
					,['title':'Name'					,'name':'name'					,'width':250	,'search_ctl':'ctlText'	] 
					,['title':'Context'					,'name':'context'				,'width':150	,'search_ctl':'ctlText'	] 
					,['title':'Description'				,'name':'description'			,'width':200	,'search_ctl':'ctlText'	] 
					,['title':'Html link'				,'name':'html_link'				,'width':100	,'search_ctl':'ctlText'	] 
					,['title':'HTML Attrubutes'			,'name':'html_attributes'		,'width':200	,'search_ctl':'ctlText'	]  
					,['title':'Is active'				,'name':'is_active'				,'width':100	,'search_ctl':'ctlNumeric'	]
					,['title':'Require Confirmation'	,'name':'require_confirmation'	,'width':100 	,'search_ctl':'ctlNumeric'	]
			] ]) }} 
            {% for id in ids %}
			{% set entity = _model.getOne('Actions',id) %}
				<tr>
					<td class="text-right">
						{{ partial("partials/btnEditDelete", ['value':entity.id , 'controller':'actions'] ) }}
						{{ linkTo(['suva/roles-permissions?_context%5Bfilter%5D%5Baction_id%5D=' ~ entity.id, 'Permissions' ]) }}
						{#{ linkTo(['admin/roles_permissions', 'Permissions' ]) }#}
					</td>
					<td>{{ entity.id }}</td>
					<td>{{ entity.name }}</td>
					<td>{{ entity.context }}</td>
					<td>{{ entity.description }}</td>
					<td>{{ entity.html_link }}</td>
					<td>{{ entity.html_attributes}}</td>
					<td>
						<div class="col-lg-3 col-md-3">
						{% set field = 'is_active' %} {% set value = entity.is_active %}{% set title = 'Is active' %}
						<div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">


							<input disabled="disabled" type="checkbox" name="{{ field }}" id="{{ field }}" value="1" {% if value == 1  %} checked{% endif %} maxlength="2" />
							{% if errors is defined and errors.filter(field) %}
							<p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
							{% endif %}
						</div>
						</div>
					</td>
					<td>
						<div class="col-lg-3 col-md-3">
						{% set field = 'require_confirmation' %} {% set value = entity.require_confirmation %}{% set title = 'Require Confirmation' %}
						<div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">


							<input disabled="disabled" type="checkbox" name="{{ field }}" id="{{ field }}" value="1" {% if value == 1  %} checked{% endif %} maxlength="2" />
							{% if errors is defined and errors.filter(field) %}
							<p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
							{% endif %}
						</div>
						</div>
					</td>
				</tr>
            {% else %}
				<tr><td colspan="6">Currently, there are none here!</td></tr>
            {% endfor %}
        </table>
        {% if pagination_links %}
			{{ pagination_links }}
		{% endif %}
    </div>
</div>