{% if _is_ajax_submit is not defined %}
	{% set _is_ajax_submit = null %}
{% endif %}

	
	<div class="row">
		<div class="col-lg-12 col-md-12">
			<div><h1 class="page-header">{% if entity.id is not defined %}New {% endif %}Action</h1></div>
			<br/>{{ partial("partials/blkFlashSession") }}
			{% if entity is defined %}
			{{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
			{{ hiddenField('_csrftoken') }}
			{{ hiddenField('next') }}
			<input type="hidden" name="id" value="{{entity.id}}"></input>
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							{% if entity.id is defined %}
								<div class="pull-right">[ID: <b>{{ entity.id }}</b> ]</div>
							{% endif %}
							<h3 class="panel-title">Action details</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								{{ partial("partials/ctlText",['value':entity.name, 'title':'Name', 'width':6, 'field':'name', '_context': _context] ) }}
								{{ partial("partials/ctlText",['value':entity.context, 'title':'Context', 'width':3, 'field':'context', '_context': _context] ) }}
								{{ partial("partials/ctlCheckbox",['value':entity.is_active, 'title':'is active', 'width':3, 'field':'is_active', '_context': _context] ) }}
								{{ partial("partials/ctlCheckbox",['value':entity.require_confirmation, 'title':'Require Confirmation', 'width':3, 'field':'require_confirmation', '_context': _context] ) }}
							</div>
							<div class = "row">
								{{ partial("partials/ctlText",['value':entity.description, 'title':'Description', 'width':12, 'field':'description', '_context': _context] ) }}
							</div>
							<div class="row">
								{{ partial("partials/ctlText",['value':entity.html_link, 'title':'Html link', 'width':12, 'field':'html_link', '_context': _context] ) }}
							</div>
							<div class = "row">
								{{ partial("partials/ctlText",['value':htmlspecialchars(entity.html_attributes), 'title':'Html attributes', 'width':12, 'field':'html_attributes', '_context': _context] ) }}
								
							</div>
						</div>
					</div>
				</div>
			</div>
			{% include "partials/btnSaveCancel.volt" %}
			{{ endForm() }}
			{% else %}
			ENTITY NIJE DEFINIRAN
			{% endif %}
		</div>
