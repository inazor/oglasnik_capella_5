<div class="row">
	
	<div class="col-lg-12 col-md-12">
		<div>
			
			<h1 class="page-header">Payment Methods</h1>
		</div>
		<br/>
		{{ partial("partials/btnAddNew", ['controller':'payment-methods'] ) }}
			{{ partial("partials/blkFlashSession") }}
			<table class="table table-striped table-hover table-condensed">
			{{ partial("partials/blkTableSort",['fields':[ 
					['title':'Options'												,'width':150	]
					,['title':'ID'					,'name':'id'					,'width':100		,'search_ctl':'ctlNumeric']
					,['title':'Slug'				,'name':'slug'					,'width':300		,'search_ctl':'ctlText'	  ]
					,['title':'Name'				,'name':'name'					,'width':300		,'search_ctl':'ctlText'	  ] 
					
					,['title':'Nav Id'				,'name':'nav_payment_type_id'	,'width':100		,'search_ctl':'ctlText'	  ]
					,['title':'Nav Payment Type Id'	,'name':'nav_id'				,'width':100		,'search_ctl':'ctlNumeric'	  ]
					,['title':'Is fiscalized'		,'name':'is_fiscalized'			,'width':100		,'search_ctl':'ctlNumeric']					
					,['title':'Is active'			,'name':'is_active'				,'width':100		,'search_ctl':'ctlNumeric']					
			] ]) }}

		{% for id in ids %}
			{% set entity = _model.getOne('PaymentMethods',id) %}
            <tr>
				<td class="text-right">{{ partial("partials/btnEditDelete", ['value':entity.id , 'controller':'payment-methods'] ) }}</td>
                <td>{{ entity.id }}</td>
				<td>{{ entity.slug }}</td>
                <td>{{ entity.name }}</td>
				<td>{{ entity.nav_id }}</td>
				<td>{{ entity.nav_payment_type_id }}</td>
				<td>{{ entity.is_fiscalized }}</td>
				<td>{{ partial("partials/ctlCheckbox",['value':entity.is_active, 'readonly':1 ] ) }}</td>
						
                
            </tr>
            {% else %}
            <tr><td colspan="6">Currently, there are no payment methods here!</td></tr>
            {% endfor %}
        </table>

    </div>
</div>