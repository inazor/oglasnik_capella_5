{% if pagination_links is not defined%}
	{% set pagination_links = null %}
{% endif %}
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div>
			
			<h1 class="page-header">{{ _m.t('Moderation Reasons') }}</h1>
		</div>
		<br/>
		{{ partial("partials/btnAddNew", ['controller':'moderation-reasons'] ) }}
			{{ partial("partials/blkFlashSession") }}
			<table class="table table-striped table-hover table-condensed">
			{{ partial("partials/blkTableSort",['fields':[ 
				['title':	_m.t('Options')															,'width':150	,'show_navigation' : true ] 
				,['title':	_m.t('ID')							,'name':'id'						,'width':100		,'search_ctl':'ctlNumeric']
				,['title':_m.t('Moderation Status')				,'name':'moderation_status'			,'width':100		,'search_ctl':'ctlNumeric']
				,['title':		_m.t('Name')					,'name':'name'						,'width':400	,'search_ctl':'ctlText'	  ] 
				,['title':	_m.t('Description')					,'name':'description'				,'width':300	,'search_ctl':'ctlText'	  ]
				,['title':	_m.t('Is active')					,'name':'is_active'					,'width':100	,'search_ctl':'ctlText'	  ] 				
			] ]) }}
			
		{% for id in ids %}
			{% set entity = _model.getOne('ModerationReasons',id) %}

            <tr>
				<td class="text-right">{{ partial("partials/btnEditDelete", ['value':entity.id , 'controller':'moderation-reasons'] ) }}</td>
                <td>{{ entity.id }}</td>
                <td>{{ entity.moderation_status }}</td>
				<td>{{ entity.name }}</td>
				<td>{{ entity.description }}</td>
                <td><input disabled="disabled" type="checkbox" {% if entity.is_active == 1  %} checked {% endif %} maxlength="2" /></td>
            </tr>
            {% else %}
            <tr><td colspan="6">{{ _m.t('Currently, there are no moderation reasons here!') }}</td></tr>
            {% endfor %}
        </table>
        {% if pagination_links %}{{ pagination_links }}{% endif %}
    </div>
</div>
