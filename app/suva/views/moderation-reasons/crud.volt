{% if _is_ajax_submit is not defined %}
	{% set _is_ajax_submit = '' %}
{% endif %}

<div class="row">
	<div class="col-lg-12 col-md-12">
		<div><h1 class="page-header">{% if entity.id is not defined %}New {% endif %}{{ _m.t('Moderation Reasons') }}</h1></div>
		{{ partial("partials/blkFlashSession") }}
		{% if entity is defined %}
			{{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
				{{ hiddenField('_csrftoken') }}
				{{ hiddenField('next') }}
				<input type="hidden" name="id" value="{{entity.id}}"></input>
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								{% if entity.id is defined %}
									<div class="pull-right">[{{ _m.t('ID:') }} <b>{{ entity.id }}</b> ]</div>
								{% endif %}
								<h3 class="panel-title">{{ _m.t('Moderation Reasons details') }}</h3>
							</div>
							<div class="panel-body">
							<div class="row">
									{{ partial("partials/ctlText",['value':entity.moderation_status, 'title': _m.t('Moderation Status'), 'width':3, 'field':'moderation_status', '_context': _context] ) }}	
									{{ partial("partials/ctlText",['value':entity.name, 'title': _m.t('Name'), 'width':3, 'field':'name', '_context': _context] ) }}										
									
								</div>
								<div class="row">
									{{ partial("partials/ctlText",['value':entity.description, 'title': _m.t('Description'), 'width':3, 'field':'description', '_context': _context] ) }}				
									{{ partial("partials/ctlCheckbox",['value':entity.is_active, 'title': _m.t('Is Active'), 'width':2, 'field':'is_active',  '_context': _context] ) }}
								</div>
							</div>
						</div>
					</div>
				</div>
				{% include "partials/btnSaveCancel.volt" %}
			{{ endForm() }}
		{% else %}
			{{ _m.t('ENTITY NIJE DEFINIRAN') }}
		{% endif %}
	</div>
</div>