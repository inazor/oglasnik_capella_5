{# CONTEXT<pre>{{ print_r( _context, true ) }}</pre>#}


{% if _context['default_filter']['publication_id'] is defined %}
	{% set publication = _model.Publication( _context['default_filter']['publication_id'] ) %}
	{% set pubCatFilter = "n_publications_id = " ~ publication.id %}
	{% set pubCatMapFilter = " active = 1 and n_publications_id = " ~ publication.id %}
	
{% else %}
	{% set pubCatFilter = "1=1" %}
	{% set pubCatMapFilter = " active = 1 and 1=1 " %}
	
{% endif %}
<!-- <style>
td {
border: 1px solid black;

}

</style> -->


<div class="row">
    <div class="col-lg-12 col-md-12">
       
		<div>
			{#
			{{ partial("partials/btnAddNew", ['controller':'publications-categories-mappings'] ) }}
			
			<a class="pull-right" href = "/suva/categories-mappings/index/{{  publication.id }}">
				<span class="btn btn-primary">Categories for this publication</span>&nbsp;
			</a>
			<a class="pull-right" href = "/suva/publications/crud/{{  publication.id }}">
				<span class="btn btn-primary">Edit Publication</span>&nbsp;
			</a>
			#}
			
			<h1 class="page-header">
				Category Mappings 
				{% if publication is defined %}
					for <i>{{ publication.name }}</i>
				{% else %}
					for Publications
				{% endif %}
			</h1>
			
        </div>
		
        <br>
        {{ partial("partials/blkFlashSession") }}
        <table style="width:2350px !important;" class="table table-striped table-hover table-condensed">

		{{ partial("partials/blkTableSort",[
			'_context' : _context,
			'fields':[ 
				 ['title':'Options'													,'width':150	,'show_navigation' : true ] 
				,['title':'ID'						,'name':'id'					,'width':100		,'search_ctl':'ctlText'		] 
				,['title':'Publication Category'	,'name':'category_mapping_id'	,'width':450	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.CategoriesMappings( [ pubCatMapFilter ,'order' :  'n_path' ] ) , 'option_fields':['n_path','id'] ]]  

				,['title':'Mapped Global Category'	,'name':'mapped_category_id'	,'width':450	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.Categories( [ 'order' :  '' ] ) , 'option_fields':['n_path','id'] ]]  
 
				,['title':'Mapped Dictionary'	,'name':'mapped_dictionary_id'		,'width':450		]
				,['title':'Mapped Location'		,'name':'mapped_location_id'		,'width':450			]		

				
				,['title':'Is Active'			,'name':'is_active'					,'width':100		] 
			] ]) }}

			
			{% for id in ids %}
				{% set entity = _model.getOne('PublicationsCategoriesMappings',id) %}
				
				<!-- editiranje preko ajaxa -->
				{% set _context['current_entity_table_name'] = 'n_publications_categories_mappings' %}
				{% set _context['current_entity_id'] = entity.id %}
				{% set _context['ajax_edit'] = true %}
				 
				<tr>
					<td>{{ partial("partials/btnEditDelete", ['value':entity.id , 'controller':'publications-categories-mappings'] ) }}</td>
					<td>{{ entity.id }}</td>
					<td>
						{% set categoryMapping = entity.CategoryMapping() %}
						{{ categoryMapping.id ~ ' - ' ~ categoryMapping.n_path }}
					</td>
					<td>
						{% set mappedCategory = entity.MappedCategory() %}
						{{ partial("partials/ctlTextTypeV3",[
							'field' : 'mapped_category_id'
							, 'value' : entity.mapped_category_id
							, '_context' : _context 
							, 'value_show' :  mappedCategory.id ~ ' - ' ~ mappedCategory.n_path
							, 'model_name' : 'Categories' 
							, 'expr_search' : 'n_path'
							, 'expr_value' : 'id' 
							, 'expr_show_input' : 'n_path'
							, 'expr_show_list' : 'n_path'
							, 'additional_where' : ' and active = 1 and transaction_type_id is not null order by n_path '
							, 'nr_returned_rows' : 100
						]) }}
						
						{#{ partial("partials/ctlTextTypeV3",[
							'field' : 'mapped_category_id'
							, 'value' : entity.mapped_category_id
							, '_context' : _context 
							, 'value_show' :  entity.MappedCategory().n_path
							, 'model_name' : 'Categories' 
							, 'expr_search' : "concat_LEFT_BRACKET_id_COMMA__SNG_QUOT__SPACE__SNG_QUOT__COMMA_n_path_RIGHT_BRACKET_"
							, 'expr_value' : 'id' 
							, 'expr_show_input' : 'n_path'
							, 'expr_show_list' : "concat_LEFT_BRACKET_id_COMMA__SNG_QUOT__SPACE__SNG_QUOT__COMMA_n_path_RIGHT_BRACKET_"
							, 'additional_where' : '  order by n_path '
						]) }#}
						
						
					</td>
					
					{% set mappedDictionary =  entity.MappedDictionary() %}
					{% set mappedDictionary_npath =  mappedDictionary ? mappedDictionary.n_path : '' %}				
					<td>
						{{ partial("partials/ctlTextTypeV3",[
							'field' : 'mapped_dictionary_id'
							, 'value' : entity.mapped_dictionary_id
							, '_context' : _context 
							, 'value_show' :  mappedDictionary_npath
							, 'model_name' : 'Dictionaries' 
							, 'expr_search' : 'name'
							, 'expr_value' : 'id' 
							, 'expr_show_input' : 'n_path'
							, 'expr_show_list' : 'n_path'
							, 'additional_where' : ' and active = 1 order by n_path '
						]) }}
					</td>
					
					{% set mappedLocation =  entity.MappedLocation() %}
					{% set mappedLocation_name =  mappedLocation ? mappedLocation.name : '' %}			
					<td>
						{{ partial("partials/ctlTextTypeV3",[
							'field' : 'mapped_location_id'
							, 'value' : entity.mapped_location_id
							, '_context' : _context 
							, 'value_show' :  mappedLocation_name
							, 'model_name' : 'Locations' 
							, 'expr_search' : 'name'
							, 'expr_value' : 'id' 
							, 'expr_show_input' : 'n_path'
							, 'expr_show_list' : 'n_path'
							, 'additional_where' : '  order by name '
							, 'nr_returned_rows' : 30
						]) }}
					</td>
					{# ne spada na PCM nego na CM
					<td>
						{{ partial("partials/ctlDropdown",[
							'value' : entity.layout_id, 
							'list' : _model.Layouts(), 
							'field' : 'layout_id', 
							'_context' : _context				
						] )}}	
					</td>
					#}
					<td>{{ partial("partials/ctlCheckbox",['value':entity.is_active, 'field':'is_active', '_context':_context ] ) }}</td>
					
				</tr>
            {% else %}
				<tr><td colspan="6">Currently, there are no Category Mappings for Publication!</td></tr>
            {% endfor %}
        </table>

    </div>
	
	{#
	<div class = "col-lg-12 col-md-12">
		<!-- POSTAVKE KATEGORIJA -->
	
		<input type="hidden" name="id" value="{{entity.id}}"></input>
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						{% if entity.id is defined %}
							<div class="pull-right">[ID: <b>{{ entity.id }}</b> ]</div>
						{% endif %}
						<h3 class="panel-title"><b>Publications Categories settings</b></h3>
					</div>
					<div class="panel-body">
						  <!-- lista kategorija mora biti sortirana po n_path zato ide obratna logika  -->
						{% set catlist = _model.Categories([' level > 1  and transaction_type_id is not null ','order':'n_path']) %}
						{% for cat in catlist %}
							{% if entity.id is defined %}
								{% set pubcat = _model.PublicationCategory("publication_id = " ~ entity.id ~ " and category_id = " ~ cat.id ) %}
								
								 <!-- postavke za brzo editiranje postavit ručno jer nije klasična  lista  -->
								{% set _context['current_entity_table_name'] = 'n_publications_categories' %}
								{% set _context['current_entity_id'] = pubcat.id %}
								{% set _context['ajax_edit'] = true %}

								
								<div class="row">
									<!-- CATEGORY_ID -->
									{{ partial("partials/ctlDropdown",[
										'value':pubcat.layouts_id, 
										'list':_model.Layouts(), 
										'field':'layouts_id', 
										'width':1, 
										'_context':_context
									] ) }}
									<div class="col-lg-4 col-md-4" >
										{{ pubcat.id }} {{ pubcat.Category().n_path }} {{ pubcat.category_id }}
									</div>
									
									<!-- IS_ACTIVE -->
									{{ partial("partials/ctlCheckbox",[
										'value': pubcat.is_active, 
										'width':1, 
										'field':'is_active',
										'_context':_context
									] ) }}							
									
									{{ partial("partials/ctlDropdown",[
										'value': pubcat.dtp_groups_id,
										'list': _model.DtpGroups(), 
										'field':'dtp_groups_id', 
										'width': 1, 
										'option_fields':['name'],
										'_context': _context
									] ) }}	
									
									{{ partial("partials/ctlDropdown",[
										'value': pubcat.forward_category_id,
										'list': catlist, 
										'field':'forward_category_id', 
										'width': 5, 
										'option_fields': ['n_path','id'],
										'_context': _context
									] ) }}										
								</div>	
							{% endif %}	
						{% endfor %}

					</div>

				</div>
			</div>
		</div>
	</div>
	#}
</div>
