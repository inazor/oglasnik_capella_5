{# CONTEXT<pre>{{ print_r( _context, true ) }}</pre> #} 

{#{{ _context['_gfid'] }}<br/>#}

{% if _context['default_filter']['n_publications_id'] is defined %}
	{% set publication = _model.Publication( _context['default_filter']['n_publications_id'] ) %}
	{% set pubFilter = "n_publications_id = " ~ publication.id %}
{% else %}
	{% set pubFilter = "1=1" %}
{% endif %}
{{pubFilter}}
<!-- <style>
	.form-control {
	
	padding: 0px 0px;
	
	}

</style> -->


<div class="row">
    <div class="col-lg-12 col-md-12">
        <div>
			{{ partial("partials/btnAddNew", ['controller':'categories-mappings'] ) }}

		
            <h1 class="page-header">
				Categories 
				{% if publication is defined %}
					for <i>{{ publication.name }}</i>
				{% else %}
					for Publications
				{% endif %}
			</h1>
        </div>
        <br> 
        {{ partial("partials/blkFlashSession") }}
        <table style="width:2200px !important;" class="table table-striped table-hover table-condensed">

		{{ partial("partials/blkTableSort", [
			'_context' : _context,
			'fields':[ 
				 ['title':'Options'											,'width':150	,'show_navigation' : true ]  
				
				,['title':'ID'					,'name':'id'				,'width':100	,'search_ctl':'ctlText'	] 
				,['title':'Parent Category'		,'name':'parent_id'			,'width':500	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.CategoriesMappings( [ pubFilter , 'order':'n_path' ]), 'option_fields':['n_path','id'] ]]  
				,['title':'Name'				,'name':'name'				,'width':300	,'search_ctl':'ctlText' ]
				,['title':'Avus_class_id'		,'name':'avus_class_id'		,'width':200	,'search_ctl':'ctlText' ] 
				,['title':'Sort Order'			,'name':'sort_order'		,'width':100	,'search_ctl':'ctlNumeric'] 
				,['title':'EPS File'			,'name':'eps_file_path'		,'width':250	,'search_ctl':'ctlText'	] 
				,['title':'DTP Group'			,'name':'dtp_group_id'		,'width':200	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list': _model.DtpGroups(), 'option_fields':['name'] ]]  
				,['title':'Publication'			,'name':'n_publications_id'	,'width':300	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.Publications( ), 'option_fields':['id', 'name'] ]]  
				,['title':'DTP Layout'			,'name':'layout_id'			,'width':200	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.Layouts( " is_active = 1 ") ]]  
				,['title':'Is Active'			,'name':'active'			,'width':100	,'search_ctl':'ctlText'	] 	
				
			] ]) }}
			
			{% for id in ids %}
				{% set entity = _model.getOne('CategoriesMappings',id) %}
				
				<!-- editiranje preko ajaxa -->
				{% set _context['current_entity_table_name'] = 'n_categories_mappings' %}
				{% set _context['current_entity_id'] = entity.id %}
				{% set _context['ajax_edit'] = true %}
				
				<tr>
					<td>{{ partial("partials/btnEditDelete", ['value':entity.id , 'controller':'categories-mappings'] ) }}</td>
					<td>{{ entity.id }}</td>
					<td>
						{% set _parentName = '' %}
						{% if entity.parent_id > 0 %}
							{% set _parentName =  _model.CategoryMapping( entity.parent_id ).n_path %}
						{% endif %}
						
						{{ partial("partials/ctlTextTypeV3",[
							'field' : 'parent_id'
							, 'value' : entity.parent_id
							, '_context' : _context 
							, 'value_show' :  _parentName
							, 'model_name' : 'CategoriesMappings' 
							, 'expr_search' : 'n_path'
							, 'expr_value' : 'id' 
							, 'expr_show_input' : 'n_path'
							, 'expr_show_list' : 'n_path'
							, 'additional_where' : ' and ' ~ pubFilter  ~ ' and active = 1 order by n_path '
							,'debug':false
						]) }}
					</td>				
					<td>
						{{ partial("partials/ctlText",[
							'value':entity.name, 
							'field':'name',
							'_context': _context
						] )}} 
					</td>
					<td>
						{{ partial("partials/ctlText",[
							'value':entity.avus_class_id, 
							'field':'avus_class_id',
							'_context': _context
						] )}} 
					</td>
					<td>
						{{ partial("partials/ctlNumeric",[
							'value':entity.sort_order, 
							'field':'sort_order', 
							'decimals':0, 
							'_context': _context
						] ) }}
					</td>
					<td>
						{{ partial("partials/ctlText",[
							'value' : entity.eps_file_path, 
							'field' : 'eps_file_path',
							'_context' : _context
						] )}} 
					</td>

					<td>
						{{ partial("partials/ctlDropdown",[
							'value': entity.dtp_group_id,
							'list': _model.DtpGroups(), 
							'field': 'dtp_group_id', 
							'option_fields':['name'],
							'_context': _context
						] ) }}	
					</td>
					<td>{{ entity.Publication().name }}</td>
					
					<td>
						{{ partial("partials/ctlDropdown",[
							'value' : entity.layout_id, 
							'list' : _model.Layouts(), 
							'field' : 'layout_id', 
							'_context' : _context				
						] )}}	
					</td>

					<td>
						{{ partial("partials/ctlCheckbox",[
							'field' : 'active',
							'value' : entity.active, 
							'_context' : _context
						] ) }}
					</td>					
				</tr>
            {% else %}
				<tr><td colspan="6">Currently, there are no Categories for publication here!</td></tr>
            {% endfor %}
        </table>

    </div>
</div>

