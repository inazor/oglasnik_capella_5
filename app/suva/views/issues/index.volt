<div class="row">
	
	<div class="col-lg-12 col-md-12">
		<div>
			
			<h1 class="page-header">{{ _m.t('Issues') }}</h1>
		</div>
		<br/>
		{{ partial("partials/btnAddNew", ['controller':'issues'] ) }}
			{{ partial("partials/blkFlashSession") }}
			<table class="table table-striped table-hover table-condensed">
				{{ partial("partials/blkTableSort",['fields':[ 
				['title': _m.t('Options')										,'width':150	,'show_navigation' : true ]
				,['title':	 _m.t('ID'	)			,'name':'id'				,'width':100		,'search_ctl':'ctlNumeric'	]
				,['title': _m.t('Datum izdavanja')	,'name':'date_published'	,'width':200	,'search_ctl':'ctlDate'	] 
				,['title':	 _m.t('Status'	)		,'name':'status'			,'width':100	,'search_ctl':'ctlText'	] 
				,['title': _m.t('Publication')	,'name':'publications_id'	,'width':250	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list': _model.Publications() ]] 
				,['title':	 _m.t('Deadline')		,'name':'deadline'			,'width':200	,'search_ctl':'ctlDate'	] 
			] ]) }}
		
            {% for id in ids %}
				{% set entity = _model.getOne('Issues', id) %}
				{% set _context['current_entity_id'] = entity.id %}
				{% set _context['ajax_edit'] = true %}
				<tr>
					<td >
						{{ partial("partials/btnEditDelete", ['value':entity.id , 'controller':'issues'] ) }}
					</td>
					<td>{{ entity.id }}</td>
					<td>
						{{ partial("partials/ctlDate",[
							'value': entity.date_published, 
							'field': 'date_published',
							'_context': _context 
						] ) }}
					</td>
					<td>{{ entity.status }}</td>
					<td>{{ _model.getOne('Publications', entity.publications_id).name }}</td>
					<td>
						{#{ partial("partials/ctlDate",[
							'value':entity.deadline, 
							'field': 'deadline',
							'show_time': true,
							'_context':_context 
						] ) }#}
						{{ entity.deadline  }}
					</td>
					
				</tr>
            {% else %}
            <tr><td colspan="6">{{ _m.t('Currently, there are no issues here!') }}</td></tr>
            {% endfor %}
        </table>

    </div>
</div>
