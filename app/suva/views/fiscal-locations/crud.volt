{% if _is_ajax_submit is not defined %}
	{% set _is_ajax_submit = '' %}
{% endif %}

<div class="row">
	<div class="col-lg-12 col-md-12">
		<div><h1 class="page-header">{% if entity.id is not defined %}New {% endif %}Roles Permissions</h1></div>
		{{ partial("partials/blkFlashSession") }}
		{% if entity is defined %}
			{{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
				{{ hiddenField('_csrftoken') }}
				{{ hiddenField('next') }}
				<input type="hidden" name="id" value="{{entity.id}}"></input>
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								{% if entity.id is defined %}
									<div class="pull-right">[ID: <b>{{ entity.id }}</b> ]</div>
								{% endif %}
								<h3 class="panel-title">Roles Permissions details</h3>
							</div>
							<div class="panel-body">
								<div class="row">
									{{ partial("partials/ctlText",['value':entity.name, 'title':'Name', 'width':3, 'field':'name', '_context': _context] ) }}				
									{{ partial("partials/ctlText",['value':entity.nav_fiscal_location_id, 'title':'NAV fiscal location', 'width':3, 'field':'nav_fiscal_location_id', '_context': _context] ) }}
								</div>
							</div>
						</div>
					</div>
				</div>
				{% include "partials/btnSaveCancel.volt" %}
			{{ endForm() }}
		{% else %}
			ENTITY NIJE DEFINIRAN
		{% endif %}
	</div>
</div>