{% if _is_ajax_submit is not defined %}
	{% set _is_ajax_submit = null %}
{% endif %}
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div><h1 class="page-header">{% if entity.id is not defined %}New {% endif %}Roles Permissions</h1></div>
		{{ partial("partials/blkFlashSession") }}

		{#{ print_r(entity.toArray(),true) }#}
		{% if entity is defined %}
			{{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
				{{ hiddenField('_csrftoken') }}
				{{ hiddenField('next') }}
				<input type="hidden" name="id" value="{{entity.id}}"></input>
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								{% if entity.id is defined %}
									<div class="pull-right">[ID: <b>{{ entity.id }}</b> ]</div>
								{% endif %}
								<h3 class="panel-title">Roles Permissions details</h3>
							</div>
							<div class="panel-body">
								<div class="row">
									{{ partial("partials/ctlText",['value':entity.name, 'title':'Name', 'width':4, 'field':'name', '_context': _context] ) }}			
									
									{{ partial("partials/ctlDropdown",['value':entity.role_id, 'title':'Role', 'list':_model.Roles(), 'width':2, 'field':'role_id', '_context': _context] ) }}

								</div>
								<div class="row">
									{{ partial("partials/ctlDropdown",['value':entity.action_id, 'title':'Action', 'list':_model.Actions(['order' : 'name' ]), 'width' : 4, 'field':'action_id', '_context': _context] ) }}
									{{ partial("partials/ctlCheckbox",['value':entity.is_active, 'title':'Active', 'width':2, 'field':'is_active', '_context': _context] ) }}
								</div>
								<div class="row">
									{{ partial("partials/ctlDropdown",['value':entity.order_status_id, 'title':'Order Status', 'list':_model.OrdersStatuses(), 'width':2, 'field':'order_status_id', '_context': _context] ) }}
								</div>
								
								<div class="row">
									{{ partial("partials/ctlText",['value':entity.role_name_filter, 'title':'Role name Filter', 'width':4, 'field':'role_name_filter', '_context': _context] ) }}	
									{{ partial("partials/ctlText",['value':entity.action_name_filter, 'title':'Action name Filter', 'width':4, 'field':'action_name_filter', '_context': _context] ) }}			
									{{ partial("partials/ctlCheckbox",['value':entity.is_permission, 'title':'Is permission', 'width':2, 'field':'is_permission', '_context': _context] ) }}
								</div>
							</div>
						</div>
					</div>
				</div>
				{% include "partials/btnSaveCancel.volt" %}
			{{ endForm() }}
		{% else %}
			ENTITY NIJE DEFINIRAN
		{% endif %}
	</div>
</div>