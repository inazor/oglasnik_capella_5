
{# krivo je prikazivao price i total
<pre>{{print_r(entity.toArray(),true)}}</pre>

{%  set ent2 = entity.toArray() %}
{{gettype(entity.price)}}
<br/>
{{ ent2['price']}}

#}

{#{ 'frmOrdersItemsEdit_actEdit_parOrderStatus_' ~ entity.Order().OrderStatus().name }#}



<div class="row"> 
	<div class="col-lg-12 col-md-12">
		{#
		<div>
			<a href='#' onclick="window.history.back();" class="btn btn-primary">&lt;&lt; Back</a>
			{% if entity.ad_id > 0 %}		
				<button class="btn btn-primary">{{ linkTo('suva/ads/edit/' ~ entity.ad_id, '<span style="color:white;">&lt;&lt; AD</span>') }}</button>
            {% endif %}
			
			{% if order is defined %}
				<button class="btn btn-primary">{{ linkTo('suva/fakture/edit/' ~ order.id, '<span style="color:white;">&lt;&lt; ORDER</span>') }}</button>
				<button class="btn btn-primary">{{ linkTo('suva/korisnici/crud/' ~ order.user_id, '<span style="color:white;">&lt;&lt; USER</span>') }}</button>
			{% endif %}
			{% if x_user_id is defined %}
				<button class="btn btn-primary">{{ linkTo('suva/korisnici/crud/' ~ x_user_id, '<span style="color:white;">&lt;&lt; USER</span>') }}</button>
			{% endif %}
			
			
			<h1 class="page-header">Orders Items</h1>
		</div>
		#}
        {{ partial("partials/blkFlashSession") }}
		{{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
            {{ hiddenField('_csrftoken') }}
            {{ hiddenField('next') }}
			<input type="hidden" name="action" value="{{form_action}}"/>
				
			{% if x_user_id is defined %}
				<input type="hidden" name="x_user_id" id="x_user_id" value="{{x_user_id}}"/>
			{% endif %}
			<input type="hidden" name="order_id" id="order_id" value="{{ entity.order_id}}">
			<input type="hidden" name="ad_id" id="ad_id" value="{{ entity.ad_id}}">
					
			{% if entity.id > 0 %}
				<input type="hidden" name="id" id="id" value="{{ entity.id}}">
			{% endif %}

			<br/>
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							{% if entity.ad_id >0 %}
								<div class="pull-right">&nbsp;&nbsp;[ AD: 
									<b>
										{{ linkTo('suva/ads2/edit/' ~ entity.ad_id, entity.ad_id) }}
									</b> ]</div>
							{% else %}
								<div class="pull-right">&nbsp;&nbsp;[ <b>DISPLAY AD</b> ]</div>
							{% endif %}
							<div class="pull-right">&nbsp;&nbsp;[ PRODUCT ID:
								<b>{{ entity.n_products_id }}</b> ] 
							</div>
							<div class="pull-right">&nbsp;&nbsp;[ PUBLICATION ID: <b>{{ entity.n_publications_id }}</b> ]</div>
							{% if entity.id is defined %}
								<div class="pull-right">&nbsp;&nbsp;[ USER: 
									<b>
										{{ linkTo('suva/korisnici/crud/' ~ entity.Order().User().id, entity.Order().User().username) }}
									</b> ]</div>
								<div class="pull-right">&nbsp;&nbsp;[ STATUS: <b>{{ entity.Order().OrderStatus().name }}</b> ]</div>
								<div class="pull-right">
									&nbsp;&nbsp;
									[ ORDER ID: 
									<b>
									{{ linkTo('suva/fakture/crud/' ~ entity.order_id, entity.order_id) }}
									</b> ]
								</div>
								
								<div class="pull-right">&nbsp;&nbsp;[ ID: <b>{{ entity.id }}</b> ]</div>

							{% endif %}

							<h2 class="panel-title"><strong>Edit Order Item</strong></h2>         
						</div>
						<div class="panel-body">
							<div class="row">
								{# KATEGORIJA SE NE MOŽE MIJENJATI AKO JE DEFINIRAN AD #}
								{{ partial("partials/ctlDropdown",[
									'errors': errors,
									'value':entity.n_category_id, 
									'title':'Category', 
									'list':categories, 
									'field':'n_category_id', 
									'width':6, 
									'option_fields':['path','id'] , 
									'readonly': entity.ad_id > 0 ? 1 : 0 
								] ) }}
							</div>
							<div class="row">
								<div class="col-lg-8 col-md-8">
									<div class="row">
										<div class="issues_count" style="text-align:center;" ><b>Checked: </b></div>
										<div class="col-lg-6 col-md-6">
			                                {% set field = 'n_publications_id' %}
											<label class="control-label" for="{{ field }}">Publication</label>
											<select 
													id="{{field}}" 
													name="{{field}}" 
													class="form-control datepicker {% if pubs[entity.n_publications_id]['is_online'] == 1 %}pub-online{% else %}pub-offline{% endif %}" 
													title="Publication">
												<option value="0">-- Select Publication --</option>
												{%- for pub in publications -%}
													<option {% if pub['id'] == entity.n_publications_id %} selected="selected" {% endif %}	 value="{{ pub['id'] }}">{{ pub['name'] }}</option>
												{%- endfor -%}
											</select>
										</div>
										<div class="col-lg-6 col-md-6" style="overflow:scroll; overflow-x: hidden; width:300px; height:200px;" >
											<label class="control-label">Issues</label>
											<div id="sel-spot-issues">
											{% if entity.n_publications_id == 2 %}
												{% if !(past_issues is empty) %}
													{% for p_issue in past_issues %}

														<input class="checked_issues" disabled type="checkbox" name="offline-issues[]" value="{{ p_issue['issues_id'] }}"><label for="offline-issues"><span style="background-color:#FD8F8F"> {{ date('d.m.Y', strtotime(p_issue['date_published'])) }} {{ p_issue['day_of_week'] }}</span></label><br>	
													{% endfor %}
												{% endif %}	

												{% if !(issues is empty) %}
													{% for issue in issues %}

														<input class="checked_issues" type="checkbox" {% if issue['checked'] == '1' %} checked {% endif %} name="offline-issues[]" value="{{ issue['issues_id'] }}"><label for="offline-issues"><span class="text-primary"> {{ date('d.m.Y', strtotime(issue['date_published'])) }} {{ issue['day_of_week'] }}</span></label><br>
													{% endfor %}
												{% endif %}

												{% if !(next_issues is empty) %}			
													{% for n_issues in next_issues %}

														<input class="checked_issues" type="checkbox" name="offline-issues[]" value="{{ n_issues['id'] }}"><label for="offline-issues"><span class="text-primary"> {{ n_issues['date_published']  }}</span></label><br>	

													{% endfor %}
												{% endif %}		
											{% endif %}
											</div>
										</div>
									</div>
									<hr/>
									<div class="row">
										{{ partial("partials/ctlDropdown",[
											'value' : entity.n_products_id, 
											'title' : 'Product', 
											'list' : _model.Products(), 
											'field' : 'n_products_id', 
											'width' : 6, 
											'option_fields' : ['id','name', 'pcp_unit_price', "' kn', ", 'days_published', "' objava'" ],
											'errors' : errors
										] ) }}
										
										<div class="col-lg-6 col-md-6">
											<label class="control-label">Discounts</label>
											<div id="sel-spot-discounts">
												<div id='select-discounts'>
												{#% if !(discounts is empty) %#}
													{%- for discount in discounts -%}	
														<input type="checkbox" {% if discount['checked'] == '1' %} checked {% endif %} name="offline-discounts[]" value="{{ discount['id'] }}"><label for="offline-discounts"><span class="text-primary">{{ discount['name'] }}</span></label><br>
													{%- endfor -%}
												{#%- endif -%#}
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										{{ partial("partials/ctlText",['value':entity.n_remark, 'title':'Remark', 'width':8, 'field':'n_remark', 'errors': errors] ) }}
										{{ partial("partials/ctlText",['value':entity.n_eps_filename, 'title':'Eps Filename', 'width':4, 'field':'n_eps_filename', 'errors': errors] ) }}
									</div>
								</div>
								<div class="col-lg-4 col-md-4">
									<table class="table table-condensed table-striped table-bordered">
										{% if sales_rep_name  %}
										<tr>
											<td>Ad taker</td>
											<td><span class="fa fa-user"></span> {{  sales_rep_name }}</td>
										</tr>
										{% endif %}
										<tr>{{ad_taker_username}}
											<td>First published at</td>
											<td>
												<div class="col-lg-12 col-md-12">
													{% set field = 'n_first_published_at' %} 
													{% set value = entity.n_first_published_at %} 
													{% set name = 'n_first_published_at' %} 
													<div class="form-group" data-company-label="First Published At" data-personal-label="First Published At">
														 <input placeholder="Choose date" 
																class="form-control" 
																name='{{name}}' 
																id='{{name}}'
																value="{{ date('d.m.Y',strtotime(entity.n_first_published_at)) }}" 
																maxlength="10"
																{% if is_offline is defined %} readonly {% endif %}
																/>									
														{% set first_published_hours_exploded = explode(" ", entity.n_first_published_at) %}
														{% set first_published_hour_exploded = explode(":", first_published_hours_exploded[1]) %}
														<input type="number" id="first_published_hours" value="{{first_published_hour_exploded[0]}}" name="hours" placeholder="hours" min="0" max="23"><span>h</span>
														<input type="number" id="first_published_minutes" value="{{first_published_hour_exploded[1]}}" name="minutes" placeholder="hours" min="0" max="59"><span>m</span>	
														{% if errors is defined and errors.filter(field) %}
															<p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
														{% endif %}
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td>Expires at</td>
											<td>aaa{{ entity.n_expires_at }}</td>
										</tr>
										<tr>
											<td>Order id</td>
											<td>{{ entity.order_id}}</td>
										</tr>
										<tr>
											<td>Created</td>												
											<td><span class="fa fa-clock-o"></span> {{ date('d.m.Y H:i:s', strtotime( entity.n_first_published_at )) }}</td>
										</tr>
										<tr>
											<td>Quantity</td>
											<td><span class="fa fa-money"></span> {{ entity.qty  }}</td>
										</tr>
										<tr>
											<td>Unit Price</td>
											<td><span class="fa fa-money"></span> {{ number_format(entity.n_price, 2, ',', '.')}}</td>
										</tr>
										<tr>
											<td>Total without discount</td>
											<td><span class="fa fa-money"></span> {{ number_format(entity.n_price * entity.qty, 2, ',', '.') }}</td>
										</tr>
										<tr>
											<td>Discount</td>
											<td><span class="fa fa-money"></span> {{ number_format((entity.n_discount_amount), 2, ',', '.')  }}</td>
										</tr>
										<tr>
											<td>Cumulative discount %</td>
											<td>{{ number_format( entity.n_discount_percent, 2, ',', '.') }} %</td>
										</tr>
										<tr> 
											<td><b>Line Total</b></td>
											<td><span class="fa fa-money"> {{ number_format(entity.n_total, 2, ',', '.') }}</td>
										</tr>
										<tr>
											<td>Tax {{ entity.tax_rate * 100 }} % </td>
											<td><span class="fa fa-money"> {{ number_format(entity.n_total_with_tax - entity.n_total, 2, ',', '.') }}</td>
										</tr>
										<tr>
											<td><b>Line Total with Tax</b></td>
											<td><span class="fa fa-money"> {{ number_format(entity.n_total_with_tax, 2, ',', '.') }}</td>
										</tr>
									</table>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
			{% include "partials/btnSaveCancel.volt" %}
		{{ endForm() }}
	</div>
</div>
<!-- ako ima dozvolu moze editirat ako ne onda su inputi readonly -->
{% if not _user.isAllowed('frmOrdersItemsEdit_actEdit_parOrderStatus_' ~ entity.Order().OrderStatus().name) %}
	<script>$("#frm_register :input").attr('disabled',true)</script>
{% endif %}