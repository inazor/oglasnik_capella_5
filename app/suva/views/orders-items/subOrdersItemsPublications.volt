<div class="issues_count" style="text-align:center;" ><b>Checked: </b></div>
<div class="col-lg-6 col-md-6">
	{% set field = 'n_publications_id' %}
	<label class="control-label" for="{{ field }}">Publication</label>
	<select 
			id = "{{field}}" 
			name = "{{field}}" 
			class = "form-control datepicker {% if pubs[entity.n_publications_id]['is_online'] == 1 %}pub-online{% else %}pub-offline{% endif %}" 
			title = "Publication">
		<option value="0">-- Select Publication --</option>
		{%- for pub in publications -%}
			<option {% if pub['id'] == entity.n_publications_id %} selected="selected" {% endif %}	 value="{{ pub['id'] }}">{{ pub['name'] }}</option>
		{%- endfor -%}
	</select>
</div>
<div class="col-lg-6 col-md-6" style="overflow:scroll; overflow-x: hidden; width:300px; height:200px;" >
	<label class="control-label">Issues</label>
	<div id="sel-spot-issues">
	{% if !(issues is empty) %}
		{%- for issue in issues -%}	
			<input class="checked_issues" type="checkbox" {% if issue['checked'] == '1' %} checked {% endif %} name="offline-issues[]" value="{{ issue['issues_id'] }}"><label for="offline-issues"><span class="text-primary"> {{ date('d.m.Y', strtotime(issue['date_published'])) }} {{ issue['day_of_week'] }}</span></label><br>
		{%- endfor -%}
	{%- endif -%}
	</div>
</div>
