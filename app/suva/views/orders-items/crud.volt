
{# krivo je prikazivao price i total
<pre>{{print_r(entity.toArray(),true)}}</pre>

{%  set ent2 = entity.toArray() %}
{{gettype(entity.price)}}
<br/>
{{ ent2['price']}}

#}
{#{ 'frmOrdersItemsEdit_actEdit_parOrderStatus_' ~ entity.Order().OrderStatus().name }#}

{% set _is_readonly = false %}
{% if entity.Order() %}
	{% set _status = entity.Order().n_status %}
	{% if _status == 5 %}{% set _is_readonly = true %}{% endif %}
{% endif %}

{{ partial("partials/blkFlashSession") }}
	<div class="panel panel-default">
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="panel-heading">
					{% if entity.ad_id >0 %}
						<div class="pull-right">&nbsp;&nbsp;[ AD: 
							<b>
								{{ linkTo('suva/ads2/edit/' ~ entity.ad_id, entity.ad_id) }}
							</b> ]</div>
					{% else %}
						<div class="pull-right">&nbsp;&nbsp;[ <b>DISPLAY AD</b> ]</div>
					{% endif %}
					<div class="pull-right">&nbsp;&nbsp;[ PRODUCT ID: <b>{{ linkTo(['suva/products/crud/' ~  entity.n_products_id ,  entity.n_products_id,'target': '_blank' ]) }}</b> ]</div>
					<div class="pull-right">&nbsp;&nbsp;[ PUBLICATION ID: <b>{{ linkTo(['suva/publications/crud/' ~  entity.n_publications_id ,  entity.n_publications_id,'target': '_blank' ]) }}</b> ]</div>
					{% if entity.id is defined %}
						<div class="pull-right">&nbsp;&nbsp;[ USER: 
							<b>
								{{ linkTo('suva/korisnici/crud/' ~ entity.Order().User().id, entity.Order().User().username) }}
							</b> ]</div>
						<div class="pull-right">&nbsp;&nbsp;[ STATUS: <b>{{ entity.Order().OrderStatus().name }}</b> ]</div>
						<div class="pull-right">
							&nbsp;&nbsp;
							[ ORDER ID: 
							<b>
							{{ linkTo('suva/fakture/crud/' ~ entity.order_id, entity.order_id) }}
							</b> ]
						</div>
						
						<div class="pull-right">&nbsp;&nbsp;[ ID: <b>{{ entity.id }}</b> ]</div>

					{% endif %}

					<h2 class="panel-title"><strong>Edit Order Item</strong></h2>         
				</div>
			</div>
			<div class="panel-body">
			
				{{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
					{{ hiddenField('_csrftoken') }}
					{{ hiddenField('next') }}
					<input type="hidden" name="action" value="{{form_action}}"/>
					{% if x_user_id is defined %}
						<input type="hidden" name="x_user_id" id="x_user_id" value="{{x_user_id}}"/>
					{% endif %}
					<input type="hidden" name="order_id" id="order_id" value="{{ entity.order_id}}">
					<input type="hidden" name="ad_id" id="ad_id" value="{{ entity.ad_id}}">
							
					{% if entity.id > 0 %}
						<input type="hidden" name="id" id="id" value="{{ entity.id}}">
					{% endif %}
					
					
					{% set _is_online = true %}  <!-- JE LI ORDER ITEM ONLINE ILI OFFLINE -->
					{% if entity.id > 0 and entity.n_publications_id > 0 and entity.Publication().is_online == 0 %}
						{% set _is_online = false %}
					{% endif %}
					
					{#  ako proizvod nije published, mogu se mijenjati jedinična cijena i količina #}
					{% set _is_qty_price_readonly = true %}
					{% if entity.n_products_id > 0 and entity.Product().is_published == 0 %}
						{% set _is_qty_price_readonly = false %}
					{% endif %}
					{% if _is_readonly == true %}
						{% set _is_qty_price_readonly = true %}
					{% endif %}
					
				
					{# KATEGORIJA SE NE MOŽE MIJENJATI AKO JE DEFINIRAN AD #}
					<div class ="row">
						<div class="col-lg-8 col-md-8">
					
							<div class ="row">
								{{ partial("partials/ctlDropdown",[
									'value' : entity.n_category_id
									, 'title' : 'Category'
									, 'list' : _model.Categories( ["active = 1 and transaction_type_id is not null", 'order' : 'n_path' ] )
									, 'field' : 'n_category_id'
									, 'width' : 12
									, 'option_fields' : [ 'n_path', 'id' ] 
									, 'readonly' : true
									, 'is_submit_onchange' : false
									, '_context' : _context
								] ) }}
							</div>
							<div class="row">
								{{ partial("partials/ctlDropdown",[
									'value': entity.n_publications_id
									, 'title':'Publication'
									, 'list': _model.Publications ("is_active = 1")
									, 'field':'n_publications_id'
									, 'width': 6
									, 'option_fields':['id','name'] 
									, 'readonly': true
									, 'js_onchange' : "alert('click SAVE before continuing!!!');"
									, '_context' : _context
								] ) }}

								{{ partial("partials/ctlTextTypeV3",[
									'title' : "Product"
									, 'field' : 'n_products_id'
									, 'value' : entity.n_products_id
									, 'width' : 6
									, '_context' : _context
									, 'value_show' :  _model.Product( entity.n_products_id ).name
									, 'model_name' : 'Products' 
									, 'expr_search' : 'name'
									, 'expr_value' : 'id' 
									, 'expr_show_input' : 'name'
									, 'expr_show_list' : 'name'
									, 'readonly' : _is_readonly
									, 'additional_where' : ' and publication_id = ' ~ entity.n_publications_id ~  ' and is_active = 1 order by name '
									, 'input_P1' : 'n_publications_id'
									, 'debug' : false
								]) }}
							</div>
							
							<div class="row">
								{% if _is_online === false %}
									{#  sql koji daje listu issuea koji su ili u statusu 'prima', ili u ostalim statusima ako su već označeni #}
									{# phalconov parser neće primit union pa je trebalo stavit dva puta id in(..) #}
									{% 
										set sqlIssuesList = "
											id in (
												select  
													\Baseapp\Suva\Models\Issues.id
												from 
													 \Baseapp\Suva\Models\Insertions  
														join \Baseapp\Suva\Models\Issues  on (
															\Baseapp\Suva\Models\Insertions.issues_id = \Baseapp\Suva\Models\Issues.id 
															and \Baseapp\Suva\Models\Issues.status <> 'prima' 
															and \Baseapp\Suva\Models\Insertions.orders_items_id = " ~ entity.id ~ " )
											)
											or id in (
												select id from \Baseapp\Suva\Models\Issues where status ='prima' and publications_id = " ~ entity.n_publications_id ~ "
											)
										"
									%}
									{#{ sqlIssuesList }#}
									{{ partial ( "partials/ctlMany2Many",[
										'title' : 'Issues'
										, 'width' : 6
										, 'field' : 'offline-issues'
										, 'list': _model.Issues( sqlIssuesList )
										, 'value' : entity.Issues()
										, 'option_fields' : ['id', 'status', 'date_published']
										, 'show_entire_list' : true
										, 'readonly' : _is_readonly
										, 'height' : 400
									] ) }}	
								
								
								{% else %}
									<div class="col-lg-6 col-md-6">
										No insertions on online product
									</div>
								{% endif %}
								{#{ var_dump ( entity.Discounts() ) }#}
								{% if entity.n_publications_id > 0 %}
									{{ partial ( "partials/ctlMany2Many",[
										'title' : 'Discounts'
										, 'width' : 6
										, 'field' : 'offline-discounts'
										, 'list': _model.Discounts( ' id in (select discounts_id from \Baseapp\Suva\Models\DiscountsPublications where publications_id = ' ~ entity.n_publications_id ~ ")" )
										, 'value' : _model.Discounts(' id in ( select discounts_id from  \Baseapp\Suva\Models\OrdersItemsDiscounts where orders_items_id = ' ~ entity.id ~ ')' ) 
										, 'option_fields' : [ 'id', 'tag', 'amount' ]
										, 'show_entire_list' : true
										, 'readonly' : _is_readonly
									] ) }}
								{% else %}
									publication id not specified
								{% endif %}
							</div>
							<div class="row">
								{{ partial("partials/ctlText",[
									'value':entity.n_remark
									, 'title':'Remark'
									, 'width':8
									, 'field':'n_remark'
									, 'readonly' : _is_readonly
									, 'errors': errors
									] ) }}
								{{ partial("partials/ctlText",[
									'value':entity.n_eps_filename
									, 'title':'Eps Filename'
									, 'width':4
									, 'field':'n_eps_filename'
									, 'readonly' : _is_readonly
									, 'errors': errors
									] ) }}
							</div>

						</div>
						<div class="col-lg-4 col-md-4">
							<table class="table table-condensed table-striped table-bordered">
								<tr>
									<td>Ad taker</td>
									<td><span class="fa fa-user"></span> {{ (entity.n_ad_taker_id > 0 ? _model.User( entity.n_ad_taker_id ).username : 'n/a') }}</td>
								</tr>
								<tr>
									<td>First published at</td>
									<td>
										{#
										<div class="col-lg-12 col-md-12">
											{% set field = 'n_first_published_at' %} 
											{% set value = entity.n_first_published_at %} 
											{% set name = 'n_first_published_at' %} 
											<div class="form-group" data-company-label="First Published At" data-personal-label="First Published At">
												 <input placeholder="Choose date" 
														class="form-control" 
														name='{{name}}' 
														id='{{name}}'
														value="{{ date('d.m.Y',strtotime(entity.n_first_published_at)) }}" 
														maxlength="10"
														{% if is_offline is defined %} readonly {% endif %}
														{% if _is_readonly == true %} disabled {% endif %}
														/>									
												{% set first_published_hours_exploded = explode(" ", entity.n_first_published_at) %}
												{% set first_published_hour_exploded = explode(":", first_published_hours_exploded[1]) %}
												<input {% if _is_readonly == true %} disabled {% endif %} type="number" id="first_published_hours" value="{{first_published_hour_exploded[0]}}" name="hours" placeholder="hours" min="0" max="23"><span>h</span>
												<input {% if _is_readonly == true %} disabled {% endif %} type="number" id="first_published_minutes" value="{{first_published_hour_exploded[1]}}" name="minutes" placeholder="hours" min="0" max="59"><span>m</span>
												<input {% if _is_readonly == true %} disabled {% endif %} type="number" id="first_published_seconds" value="{{first_published_hour_exploded[2]}}" name="seconds" placeholder="hours" min="0" max="59"><span>s</span>												
												{% if errors is defined and errors.filter(field) %}
													<p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
												{% endif %}
											</div>
										</div>
										#}
										{{ partial("partials/ctlDate",[
											'title':'Datum', 
											'value': entity.n_first_published_at , 
											'field':'n_first_published_at',
											'width':12, 
											'readonly' : _is_readonly,
											'show_time' : true,
											'_context' : _context
										] ) }}
									</td>
								</tr>
								<tr>
									<td>Expires at</td>
									<td>{{ date('d.m.Y H:i:s',strtotime(entity.n_expires_at)) }}</td>
								</tr>
								<tr>
									<td>Order id</td>
									<td>{{ entity.order_id}}</td>
								</tr>
								<tr>
									<td>Created</td>												
									<td><span class="fa fa-clock-o"></span> {{ date('d.m.Y H:i:s', strtotime( entity.n_created_at )) }}</td>
								</tr>
								<tr>
									<td>Quantity</td>
									<td> {#{ var_dump(_is_qty_price_readonly) }#}
										{{ partial("partials/ctlNumeric",[
											'value' : entity.qty
											, 'field':'qty'
											, 'decimals' : 2
											, 'readonly' : _is_qty_price_readonly
											, '_context': _context
										] ) }}
									</td>
								</tr>
								<tr>
									<td>Unit Price</td>
									<td>
										{#<span class="fa fa-money"></span> {{ number_format(entity.n_price, 2, ',', '.')}}#}
										{{ partial("partials/ctlNumeric",[
											'value' : entity.n_price
											, 'field':'n_price'
											, 'decimals' : 2
											, 'readonly' : _is_qty_price_readonly
											, '_context': _context
										] ) }}
									</td>
								</tr>
								<tr>
									<td>Total without discount</td>
									<td><span class="fa fa-money"></span> {{ number_format(entity.n_price * entity.qty, 2, ',', '.') }}</td>
								</tr>
								<tr>
									<td>Discount</td>
									<td><span class="fa fa-money"></span> {{ number_format((entity.n_discount_amount), 2, ',', '.')  }}</td>
								</tr>
								<tr>
									<td>Cumulative discount %</td>
									<td>{{ number_format( entity.n_discount_percent, 2, ',', '.') }} %</td>
								</tr>
								<tr> 
									<td><b>Line Total</b></td>
									<td><span class="fa fa-money"> {{ number_format(entity.n_total, 2, ',', '.') }}</td>
								</tr>
								<tr>
									<td>Tax {{ entity.tax_rate * 100 }} % </td>
									<td><span class="fa fa-money"> {{ number_format(entity.n_total_with_tax - entity.n_total, 2, ',', '.') }}</td>
								</tr>
								<tr>
									<td><b>Line Total with Tax</b></td>
									<td><span class="fa fa-money"> {{ number_format(entity.n_total_with_tax, 2, ',', '.') }}</td>
								</tr>


							</table>
						</div>
					</div>
					
					{% include "partials/btnSaveCancel.volt" %}
				{{ endForm() }}
			</div>
		</div>
	</div>
</div>
<!-- ako ima dozvolu moze editirat ako ne onda su inputi readonly -->
{#% if not _user.isAllowed('frmOrdersItemsEdit_actEdit_parOrderStatus_' ~ entity.Order().OrderStatus().name) %}
	<!-- <script>$("#frm_register :input").attr('disabled',true)</script> -->
{% endif %#}