<div class="row">
	<div class="col-lg-12 col-md-12">
		<div>
			<h1 class="page-header">fakturaaaa</h1>
		</div>
	</div>
</div>

<div class="page">
<div class="row">
	<div class="col-xs-4">
		<B>{{user.company_name}}</B>
		<BR/>{{user.address}}
		<BR/>{{user.city}}, {{user.zip_code}}
		<BR/>{{user.country}}
	</div>
	<div class="col-xs-4"/>
	<div class="col-xs-4">
		<h4>Oglasnik d.o.o. poduzeće za novinsko-nakladničku djelatnost</h4> 
		<p>Savska cesta 41<br/>
     HR - 10000 ZAGREB<br/>
     prodaja 01/6102800<br/>
     podrška 01/6102875<br/>
     računovodstvo 01/6102847<br/>
     IBAN: HR1624840081100153885<br/>
     MB: 3615090<br/>
     OIB: 97309929902</p>              
	</div>
		</div>
<div class="row">
		<div class="col-xs-2">br.Kupca </div>
		<div class="col-xs-2">{{user.id}}</div>
		<div class="col-xs-4"/>
		<div class="col-xs-2">Broj računa:</div>
		<div class="col-xs-2">1234543523121-OGL-1-1</div>
	</div>
	<div class="row">
		<div class="col-xs-2">OIB:</div>
		<div class="col-xs-2">{{user.oib}}</div>
		<div class="col-xs-4"/>
		<div class="col-xs-2">Datum obavljanja usluge:</div>
		<div class="col-xs-2">{{order.n_invoice_date_time_issue}}</div>
	</div>
	<div class="row">
		<div class="col-xs-2">Vaša narudžba:</div>
		<div class="col-xs-2"/>
		<div class="col-xs-4"/>
		<div class="col-xs-2">Datum dospijeća:</div>
		<div class="col-xs-2">{{order.n_invoice_date_time_issue}}</div>
	</div>
	<div class="row">
		<div class="col-xs-2">Za kupca</div>
		<div class="col-xs-2">{{user.company_name}}</div>
		<div class="col-xs-4"/>
		<div class="col-xs-2">Br. našeg naloga:</div>
		<div class="col-xs-2"/>
	</div>
	<div class="row">
		<div class="col-xs-2"/>
		<div class="col-xs-2"/>
		<div class="col-xs-4"/>
		<div class="col-xs-2">Način plaćanja:</div>
		<div class="col-xs-2">Transakcijski račun</div>
	</div>
	<div class="row">
		<div class="col-xs-2"/>
		<div class="col-xs-2"/>
		<div class="col-xs-4"/>
		<div class="col-xs-2">Zagreb,{{order.n_invoice_date_time_issue}} </div>
		<div class="col-xs-2"/>
	</div>

	<br/>

	<div class="row">
		<div class="col-xs-2">Datum i vrijeme izdavanja:</div>
		<div class="col-xs-2">{{order.n_invoice_date_time_issue}}</div>
		<div class="col-xs-4"/>
		<div class="col-xs-2"/>
		<div class="col-xs-2"/>
	</div>
	<div class="row">
		<div class="col-xs-2">Oznaka operatera:</div>
		<div class="col-xs-2">{{order.sales_rep.first_name}} {{order.sales_rep.last_name}}</div>
		<div class="col-xs-4"/>
		<div class="col-xs-1">ZKI:</div>
		<div class="col-xs-3">{{order.n_invoice_zki}}</div>
	</div>
	<div class="row">
		<div class="col-xs-2">Interni broj dokumenta:</div>
		<div class="col-xs-2">{{order.id}}</div>
		<div class="col-xs-4"/>
		<div class="col-xs-1">JIR:</div>
		<div class="col-xs-3">{{order.n_invoice_jir}}</div>
	</div>
	<hr/>
	<div class="row">
		<div class="col-xs-1">Br.</div>
		<div class="col-xs-1">Datum izdanja</div>
		<div class="col-xs-1">Broj izdanja</div>
		<div class="col-xs-4">Opis</div>
		<div class="col-xs-1">Količina</div>
		<div class="col-xs-1">JMJ</div>
		<div class="col-xs-1">Cijena jedinica</div>
		<div class="col-xs-1">Popust %</div>
		<div class="col-xs-1">Iznos</div>
	</div>
	<hr/>
        {% for item in order.items %}
	<div class="row">
		<div class="col-xs-1">{{item.product_id}}</div>
		<div class="col-xs-1"/>
		<div class="col-xs-1"/>
		<div class="col-xs-4">{{item.title}}</div>
		<div class="col-xs-1">{{item.qty}}</div>
		<div class="col-xs-1">KOM</div>
		<div class="col-xs-1">{{item.price}}</div>
		<div class="col-xs-1">{{item.n_discounts_id.amount}}</div>
		<div class="col-xs-1">{{item.total}}</div>
	</div>

        {% endfor %}
	<hr/>
	<div class="row">
		<div class="col-xs-8"/>
		<div class="col-xs-3">Ukupno KN bez PDV:</div>
		<div class="col-xs-1">{{order.total}}</div>
	</div>
	<div class="row">
		<div class="col-xs-8"/>
		<div class="col-xs-3">25% PDV:</div>
		<div class="col-xs-1">{{order.total * 0.25}}</div>
	</div>
	<div class="row">
		<div class="col-xs-8"/>
		<div class="col-xs-3">Ukupno KN s PDV:</div>
		<div class="col-xs-1">{{order.total * 1.25}}</div>
	</div>

	

</div>
