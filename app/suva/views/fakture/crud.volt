{#{ var_dump (_context) }#}
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div>

		</div>
		{{ partial("partials/blkFlashSession") }}
		{% if entity is defined %}
			{{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
				{{ hiddenField('_csrftoken') }}
				{{ hiddenField('next') }}
				<input type = "hidden" name = "id" value = "{{entity.id}}"></input>
				<div class = "row">
					<div class = "col-lg-12 col-md-12">
						<div class = "panel panel-default">
							<div class = "panel-heading">
								{% if entity.id is defined %}
									<div class = "pull-right">[ID: <b>{{ entity.id }}</b> ]</div>
								{% endif %}
								<h2 class = "panel-title" style = "float:left;">
									<strong>
										{% if ! entity.id > 0 %}
											New Order Header
										{% else %}
											{{ entity.OrderStatus().description }} No. {{ entity.id }}
										{% endif %}
									</strong>
								
								</h2>
								&nbsp;&nbsp;&nbsp;
								{# akcije #}
								{#% for id in _model.getAllowedActionIds( _user.id, entity.n_status, 'orders' ) %}
									{% set action = _model.Action(id) %}
									<a>{{ linkTo('suva/fakture/crud/'~ entity.id, '<span class="btn btn-info btn-xs">Edit</span> ') }}</a>
									{{ action.buttonHtml( entity.id ) }} &nbsp;
								{% endfor  %#}

							</div>
							<div class="panel-body">
								<div class="row">
									
									{{ partial("partials/ctlDropdown",[
										'title':'Status', 
										'value': entity.n_status, 
										'list': _model.OrdersStatuses(), 
										'field': 'n_status', 
										'width': 2, 
										'readonly': true,
										'option_fields': ['id','name'],
										'_context': _context				
									] )}}	
									

									
									{{ partial("partials/ctlTextTypeV3",[
										'title' : "User"
										, 'field' : 'user_id'
										, 'value' : entity.user_id
										, 'width' : 2
										, 'readonly' : true
										, '_context' : _context
										, 'value_show' : entity.user_id ~ ' - ' ~ entity.User().username
										, 'model_name' : 'Users' 
										, 'expr_search' : 'username'
										, 'expr_value' : 'id' 
										, 'expr_show_input' : 'username'
										, 'expr_show_list' : 'username'
										, 'additional_where' : ' order by username '
									]) }}
									


									{{ partial("partials/ctlTextTypeV3",[
										'title' : "Sales Rep."
										, 'field' : 'n_sales_rep_id'
										, 'value' : entity.n_sales_rep_id
										, 'width' : 2
										, '_context' : _context
										, 'value_show' : entity.n_sales_rep_id ~ ' - ' ~ entity.SalesRep().username
										, 'model_name' : 'Users' 
										, 'expr_search' : 'username'
										, 'expr_value' : 'id' 
										, 'expr_show_input' : 'username'
										, 'expr_show_list' : 'username'
										, 'additional_where' : ' order by username '
										, 'readonly' : ( entity.n_status == 3 ? false : true ) 
									]) }}	

									{{ partial("partials/ctlTextTypeV3",[
										'title' : "Operator."
										, 'field' : 'n_operator_id'
										, 'value' : entity.n_operator_id
										, 'width' : 2
										, '_context' : _context
										, 'value_show' : entity.n_operator_id ~ ' - ' ~ entity.Operator().username
										, 'model_name' : 'Users' 
										, 'expr_search' : 'username'
										, 'expr_value' : 'id' 
										, 'expr_show_input' : 'username'
										, 'expr_show_list' : 'username'
										, 'additional_where' : ' order by username '
										, 'readonly' : ( entity.n_status == 3 ? false : true ) 
									]) }}										

									{{ partial("partials/ctlTextTypeV3",[
										'title' : "Sub-customer"
										, 'field' : 'n_sub_customer'
										, 'value' : entity.n_sub_customer
										, 'width' : 2
										, 'readonly' : false
										, '_context' : _context
										, 'value_show' : entity.n_sub_customer ~ ' - ' ~ entity.SubCustomer().username
										, 'model_name' : 'Users' 
										, 'expr_search' : 'username'
										, 'expr_value' : 'id' 
										, 'expr_show_input' : 'username'
										, 'expr_show_list' : 'username'
										, 'additional_where' : ' order by username '
									]) }}										
									
									
									{#{ partial("partials/ctlCheckbox",[
										'value':entity.is_active, 
										'title':'Active', 
										'width':1, 
										'field':'is_active',
										'readonly': true,
										'_context':_context
									] ) }#}
								</div>
								<div class="row">
						 			{{ partial("partials/ctlDate",[
										'title': 'Created', 
										'value': entity.created_at , 
										'field': 'created_at',
										'width': 2, 
										'readonly' : true,
										'show_time' : true,
										'_context' : _context
									] ) }}

						 			{{ partial("partials/ctlDate",[
										'title': 'Modified', 
										'value': entity.modified_at , 
										'field': 'modified_at',
										'width': 2, 
										'readonly' : true,
										'show_time' : true,
										'_context' : _context
									] ) }}

						 			{{ partial("partials/ctlDate",[
										'title': 'Valid Until', 
										'value': entity.valid_until , 
										'field': 'valid_until',
										'width': 2, 
										'readonly' : true,
										'show_time' : false,
										'_context' : _context
									] ) }}
								</div>
								<div class="row">
						 			{{ partial("partials/ctlDate",[
										'title': 'Date and time of Issue', 
										'value': entity.n_invoice_date_time_issue , 
										'field': 'n_invoice_date_time_issue',
										'width': 2, 
										'readonly' : true,
										'show_time' : true,
										'_context' : _context
									] ) }}
								
									{{ partial("partials/ctlText",[
										'title': 'JIR', 
										'value': entity.n_invoice_jir, 
										'field': 'n_invoice_jir',
										'width': 2, 
										'readonly': true,
										'_context': _context
									] )}} 
									
									{{ partial("partials/ctlText",[
										'title': 'ZIK', 
										'value': entity.n_invoice_zik, 
										'field': 'n_invoice_zik',
										'width': 2, 
										'readonly': true,
										'_context': _context
									] )}} 
								
									{{ partial("partials/ctlDropdown",[
										'title':'Fiscal Location', 
										'value': entity.n_fiscal_location_id, 
										'list': _model.FiscalLocations(), 
										'field': 'n_fiscal_location_id', 
										'width': 2, 
										'readonly': true,
										'option_fields': ['id','name'],
										'_context': _context				
									] )}}
									
						 			{{ partial("partials/ctlDate",[
										'title': 'Quotation Date', 
										'value': entity.n_quotation_date , 
										'field': 'n_quotation_date',
										'width': 2, 
										'readonly' : true,
										'show_time' : false,
										'_context' : _context
									] ) }}								
								</div>
								
								<div class="row">
						 			{{ partial("partials/ctlDate",[
										'title': 'Payment Date', 
										'value': entity.n_payment_date , 
										'field': 'n_payment_date',
										'width': 2, 
										'readonly' : (entity.n_status == 3 or entity.n_status == 4 ? false: true),
										'show_time' : false,
										'_context' : _context
									] ) }}
									
									
									{{ partial("partials/ctlDropdown",[
										'title':'Payment Method', 
										'value': entity.n_payment_methods_id, 
										'list': _model.PaymentMethods(), 
										'field': 'n_payment_methods_id', 
										'width': 2, 
										'readonly': true,
										'option_fields': ['id','name'],
										'_context': _context				
									] )}}	
									 
									{{ partial("partials/ctlText",[
										'title': 'Payment Reference', 
										'value': entity.n_payment_reference, 
										'field': 'n_payment_reference',
										'width': 2, 
										'readonly': true,
										'_context': _context
									] )}} 	
									
									{#{ partial("partials/ctlTextType",[
										'title':'Applied Document', 
										'field' : 'n_applied_document_id'
										,'value' : entity.n_applied_document_id
										,'value_show' : entity.n_applied_document_id 
										,'search_field' : 'id'
										,'return_field':'id'
										,'table_name':'orders'
										,'readonly': true
										,'additional_where' : ''
										,'width': 2
									] ) }#}
									{{ partial("partials/ctlTextTypeV3",[
										'title' : "Applied Document"
										, 'field' : 'n_applied_document_id'
										, 'value' : entity.n_applied_document_id
										, 'width' : 2
										, 'readonly' : true
										, '_context' : _context
										, 'value_show' : entity.n_applied_document_id
										, 'model_name' : 'Orders' 
										, 'expr_search' : 'id'
										, 'expr_value' : 'id' 
										, 'expr_show_input' : 'id'
										, 'expr_show_list' : 'id'
										, 'additional_where' : ' '
									]) }}	
									
									{{ partial("partials/ctlText",[
										'title' : "PBO"
										, 'field' : 'n_pbo'
										, 'value' : entity.n_pbo
										, 'width' : 2
										, 'readonly' : true
										, '_context' : _context
									]) }}	
									
									
								</div>
								<div class="row">

									{{ partial("partials/ctlNumeric",[
										'title': 'Total', 
										'value': entity.total, 
										'field': 'total', 
										'decimals': 2, 
										'readonly': true,
										'width': 2, 
										'_context':_context
									] ) }}

									{{ partial("partials/ctlNumeric",[
										'title': 'Tax', 
										'value': entity.n_tax_amount, 
										'field': 'n_tax_amount', 
										'decimals': 2, 
										'readonly': true,
										'width': 2, 
										'_context':_context
									] ) }}

									{{ partial("partials/ctlNumeric",[
										'title': 'Total With Tax', 
										'value': entity.n_total_with_tax, 
										'field': 'n_total_with_tax', 
										'decimals': 2, 
										'readonly': true,
										'width': 2, 
										'_context':_context
									] ) }}
									
									{{ partial("partials/ctlNumeric",[
										'title': 'Discount %', 
										'value': entity.n_discount_percent, 
										'field': 'n_discount_percent', 
										'decimals': 2, 
										'readonly': true,
										'width': 1, 
										'_context':_context
									] ) }}
									
									{{ partial("partials/ctlNumeric",[
										'title': 'Discount', 
										'value': entity.n_discount_amount, 
										'field': 'n_discount_amount', 
										'decimals': 2, 
										'readonly': true,
										'width': 2, 
										'_context':_context
									] ) }}
									
									{{ partial("partials/ctlNumeric",[
										'title': 'Ag.Comm.%', 
										'value': entity.n_agency_commission_percent, 
										'field': 'n_agency_commission_percent', 
										'decimals': 2, 
										'readonly': true,
										'width': 1, 
										'_context':_context
									] ) }}
									
									{{ partial("partials/ctlNumeric",[
										'title': 'Agency Commission ', 
										'value': entity.n_agency_commission_amount, 
										'field': 'n_agency_commission_amount', 
										'decimals': 2, 
										'readonly': true,
										'width': 2, 
										'_context':_context
									] ) }}

								</div>
								<div class="row">
										{{ partial("partials/ctlTextArea",[
											'value' : entity.n_remark
											, 'title' : 'Remark'
											, 'width' : 12
											, 'field' : 'n_remark'
											, '_context' : _context
											, 'readonly': 0 
											, 'rows' : 4
											, 'cols' : 40
										] ) }}
								</div>
							</div>
						</div>
					</div>
				</div>
				{% include "partials/btnSaveCancel.volt" %}
			{{ endForm() }}
			
			<!-- stavke računa -->
			{% if entity.OrdersItems() is not null %}
				<table class="table table-condensed table-striped table-bordered order-details">    
					<thead>
						<tr>
							<th class="text-left">Ad</th>
							<th class="text-left">Order item</th>
							<th class="title">Product</th>
							<th class="title">Publication</th>
							<th class="title">From</th>
							<th class="title">To</th>
							<th class="text-right price">Price</th>
							<th class="text-right qty">Quantity</th>
							<th class="text-right value">Total</th>
							<th class="text-right value"></th>
						</tr>
					</thead>
					<tfoot>
						{% if entity.n_agency_commission_amount > 0  %}
							<tr class="summary total">
								<td colspan="7">&#160;</td>
								<td class="text-right"><strong>Discounts</strong></td>
								<td class="text-right value"><strong>{{number_format((entity.n_agency_commission_amount), 2, ',', '.')}}</strong></td>
								<td class="text-right"></td>
								<td></td>
							</tr>
						{% endif %}
						
						<tr class="summary total">
							<td colspan="7">&#160;</td>
							<td class="text-right"><strong>Total</strong></td>
							<td class="text-right value"><strong>{{number_format((entity.n_total), 2, ',', '.')}}</strong></td>
							<!-- <td class="text-right"><strong>Total incl. VAT: {{number_format((entity.total)+((entity.total)*0.25), 2, ',', '.')}}</strong></td> -->
							<td class="text-right"><strong>Total incl. VAT: {{entity.n_total_with_tax}}</strong></td>
							<td></td>
						</tr>
					</tfoot>
					<tbody>
						{% for item in entity.OrdersItems() %}
						<tr>
<!-- 										<pre>{ { print_r(item, true) } }</pre> -->
							<td class="text-left"><a href="/suva/ads2/edit/{{ item.ad_id }}">{{ item.ad_id }}</a></td>
							<td class="text-left">{{ item.id }}</td>
							<td class="text-left">{{ item.title }}</td>
							<td class="text-left">{{ item.Publication().name }}</td>
							<td class="text-left">{{ _model.n_dateUs2DateHr(item.n_first_published_at) }}</td>
							<td class="text-left">{{ _model.n_dateUs2DateHr(item.n_expires_at) }}</td>
							<td class="text-right price">{{number_format(item.n_price, 2, ',', '.') }}</td>
							<td class="text-right qty">{{ item.qty }}</td>
							<td class="text-right value">{{ number_format(item.n_total , 2, ',', '.') }}</td>
							<td class="text-right">
<!-- 											{ { print_r (item,true) } } -->
<!-- 											AKCIJE NA RAZINI ITEMA -->

						
							{% for id in _model.getAllowedActionIds( _user.id, entity.n_status, 'orders-items' )  %}
								{% set action = _model.Action(id) %}
								{{ action.buttonHtml( item.id ) }}<br/>
							{% endfor  %}
							
							</td>
						</tr>
						{% endfor  %}
					</tbody>
				</table>
                {% endif %}
			
		{% else %}
			ENTITY NIJE DEFINIRAN
		{% endif %}
	</div>
</div>

							
							
							
							


