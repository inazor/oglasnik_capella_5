{# Admin User Listing/View #}
<div class="row">
    <h1>Reports</h1>
		
        
</div>

<hr>
{{ partial("partials/blkFlashSession") }}
</hr>
{% if _model.isUserAllowed('rptCashOut', auth.get_user().id) %}
	<div class="row">
		{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/reports/cashOut', 'method' : 'post', 'autocomplete' : 'off') }}
			{{ hiddenField('_csrftoken') }}
			{{ hiddenField('next') }}		
			
			{% set operator = _user %}
			{% if array_key_exists('cashout_report_operator_id', _context) %}
				{% if _context['cashout_report_operator_id'] > 0 %}
					{% set operator = _model.getOne('Users',_context['cashout_report_operator_id'] ) %}
				{% endif %}	
			{% endif %}	

			{{ partial("partials/ctlTextTypeV3",[
				'title' : "Operator"
				, 'field' : '_context[cashout_report_operator_id]'
				, 'value' : operator.id
				, 'width' : 3
				, '_context' : _context
				, 'value_show' : operator.username
				, 'model_name' : 'Users' 
				, 'expr_search' : 'username'
				, 'expr_value' : 'id' 
				, 'expr_show_input' : 'username'
				, 'expr_show_list' : 'username'
				, 'additional_where' : ' and 1 = 1 '
			]) }}
		
		
			{{ partial("partials/ctlDate",['value':date("d.m.Y"), 'title':'Report date', 'width':2, 'field':'_context[cashout_report_date]'] ) }} 
			{{ partial("partials/ctlDropdown",[
				'value' : _context['_fiscal_location'], 
				'title' : 'Report Fiscal location', 
				'list' : _model.FiscalLocations(), 
				'field' : '_context[cashout_report_fiscal_location_id]', 
				'width' : 3, 
				'option_fields' : ['id','name']
			] )}}
			<div class="col-lg-2 col-md-2"><br/>
				<button type="submit" class="btn btn-warning cash-out">Cash Out Report</button>
			</div>
		{{ endForm() }}	
	</div>
{% endif %}
 
	<div class="row">
		{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/reports/ordersList', 'method' : 'post', 'autocomplete' : 'off') }}
			{{ hiddenField('_csrftoken') }}
			{{ hiddenField('next') }}		
		
			{{ partial("partials/ctlDate",['value':date("d.m.Y"), 'title':'From Date', 'width':1, 'field':'_context[orders_list_from_date]'] ) }} 
			
			{{ partial("partials/ctlDate",['value':date("d.m.Y"), 'title':'To Date', 'width':1, 'field':'_context[orders_list_to_date]'] ) }} 

			{{ partial("partials/ctlDropdown",[
				'value' : 3, 
				'title' : 'Document Status', 
				'list' : _model.OrdersStatuses(" is_active = 1 "), 
				'field' : '_context[orders_list_status]', 
				'width' : 2, 
				'option_fields' : ['id','name']
			] )}}

			{{ partial("partials/ctlDropdown",[
				'value' : 3, 
				'title' : 'Customer Payment Type', 
				'list' : _model.PaymentTypes(" is_active = 1 "), 
				'field' : '_context[orders_list_payment_type]', 
				'width' : 2, 
				'option_fields' : ['id','name']
			] )}}

			{{ partial("partials/ctlDropdown",[
				'value' : 3, 
				'title' : 'NAV Sync', 
				'list' : ['unsynced','synced','no_sync','retry','sent'], 
				'field' : '_context[orders_list_nav_sync_status]',
				'no_ids' : true,				

				'width' : 1
			] )}}
			<div class="col-lg-2 col-md-2"><br/>
				<button type="submit" class="btn btn-warning cash-out">Financial Documents list</button>
			</div>
		{{ endForm() }}	
	</div>
	
	<div class="row">
		{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/reports/avusAdsList', 'method' : 'post', 'autocomplete' : 'off') }}
			{{ hiddenField('_csrftoken') }}
			{{ hiddenField('next') }}		
			
		
			
			<div class="col-lg-2 col-md-2"><br/>
				<button type="submit" class="btn btn-warning cash-out">AVUS ADS LIST</button>
			</div>
		{{ endForm() }}	
	</div>
	<br/>
	
	<div class="row">
		{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/reports/displayAdList', 'method' : 'post', 'autocomplete' : 'off') }}
			{{ hiddenField('_csrftoken') }}
			{{ hiddenField('next') }}		
			
			{{ partial("partials/ctlDropdown",[
				'value':null, 
				'title':'Offline Issue', 
				'list':_model.Issues(), 
				'field':'_context[xml_report_issue_id]', 
				'width':3, 
				'option_fields':['id', 'publications_id','date_published']
			] )}}
			
			
			<div class="col-lg-2 col-md-2"><br/>
				<button type="submit" class="btn btn-warning cash-out">DisplayAd Pictures List</button>
			</div>
		{{ endForm() }}	
	</div>
	<div class="row">
		{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/reports/slikeList', 'method' : 'post', 'autocomplete' : 'off') }}
			{{ hiddenField('_csrftoken') }}
			{{ hiddenField('next') }}		
			
			{{ partial("partials/ctlDropdown",[
				'value':null, 
				'title':'Offline Issue', 
				'list':_model.Issues(), 
				'field':'_context[xml_report_issue_id]', 
				'width':3, 
				'option_fields':['id', 'publications_id','date_published']
			] )}}
			
			{{ partial("partials/ctlDropdown",[
				'value': null,
				'title': 'DTP Group',
				'list': _model.DtpGroups(), 
				'field': '_context[xml_report_group_id]', 
				'width': 1, 
				'option_fields':['name'],
				'_context': _context
			] ) }}	
			
			
			<div class="col-lg-2 col-md-2"><br/>
				<button type="submit" class="btn btn-warning cash-out">Slike List</button>
			</div>
		{{ endForm() }}	
	</div>

	<div class="row">
		{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/reports/allAdsList', 'method' : 'post', 'autocomplete' : 'off') }}
			{{ hiddenField('_csrftoken') }}
			{{ hiddenField('next') }}		
			
			{{ partial("partials/ctlDropdown",[
				'value':null, 
				'title':'Offline Issue', 
				'list':_model.Issues(), 
				'field':'_context[xml_report_issue_id]', 
				'width':3, 
				'option_fields':['id', 'publications_id','date_published']
			] )}}
						
			<div class="col-lg-2 col-md-2"><br/>
				<button type="submit" class="btn btn-warning cash-out">All ads in issue</button>
			</div>
		{{ endForm() }}	
	</div>
	
	{#  
	<!-- PREBACIVANJE NA NOVI EKRAN, ne radi poruka "Please wait", novi ekran nema bootstrap. -->
	<div class="row">
		{{ form(NULL, 'id' : 'frm_allAdsList', 'action' : '/suva/reports/allAdsList', 'method' : 'post', 'autocomplete' : 'off') }}
			{{ hiddenField('_csrftoken') }}
			{{ hiddenField('next') }}		
			
			{{ partial("partials/ctlDropdown",[
				'value':null, 
				'title':'Offline Issue', 
				'list':_model.Issues(), 
				'field':'_context[xml_report_issue_id]', 
				'width':3, 
				'option_fields':['id', 'publications_id','date_published']
			] )}}
						
			<div class="col-lg-2 col-md-2"><br/>
				<div class="btn btn-primary" 
					onclick=" 
						//alert('Please Wait');
						//$(this).html( 'Please Wait...' );
						//$('#image_loading').show();
						ajaxSubmitP ({
							url : '/suva/reports/allAdsList'
							,formId : 'frm_allAdsList'
							,isOpenInNewWindow : true
						});
						//$(this).html( 'All ads in issue..' );
						//$('#image_loading').hide();
					">
					All ads in issue..
				</div>
				<div id="image_loading" 
					style = "
						display:none;
						height: 100px;
						width: 100px;
						position: fixed;
						z-index: 1000;
						left: 50%;
						top: 50%;
						margin: -25px 0 0 -25px;
					">
					<img width = "200px" src="data:image/gif;base64,R0lGODlhEAAQAPQAAP///wAAAPDw8IqKiuDg4EZGRnp6egAAAFhYWCQkJKysrL6+vhQUFJycnAQEBDY2NmhoaAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAAFdyAgAgIJIeWoAkRCCMdBkKtIHIngyMKsErPBYbADpkSCwhDmQCBethRB6Vj4kFCkQPG4IlWDgrNRIwnO4UKBXDufzQvDMaoSDBgFb886MiQadgNABAokfCwzBA8LCg0Egl8jAggGAA1kBIA1BAYzlyILczULC2UhACH5BAkKAAAALAAAAAAQABAAAAV2ICACAmlAZTmOREEIyUEQjLKKxPHADhEvqxlgcGgkGI1DYSVAIAWMx+lwSKkICJ0QsHi9RgKBwnVTiRQQgwF4I4UFDQQEwi6/3YSGWRRmjhEETAJfIgMFCnAKM0KDV4EEEAQLiF18TAYNXDaSe3x6mjidN1s3IQAh+QQJCgAAACwAAAAAEAAQAAAFeCAgAgLZDGU5jgRECEUiCI+yioSDwDJyLKsXoHFQxBSHAoAAFBhqtMJg8DgQBgfrEsJAEAg4YhZIEiwgKtHiMBgtpg3wbUZXGO7kOb1MUKRFMysCChAoggJCIg0GC2aNe4gqQldfL4l/Ag1AXySJgn5LcoE3QXI3IQAh+QQJCgAAACwAAAAAEAAQAAAFdiAgAgLZNGU5joQhCEjxIssqEo8bC9BRjy9Ag7GILQ4QEoE0gBAEBcOpcBA0DoxSK/e8LRIHn+i1cK0IyKdg0VAoljYIg+GgnRrwVS/8IAkICyosBIQpBAMoKy9dImxPhS+GKkFrkX+TigtLlIyKXUF+NjagNiEAIfkECQoAAAAsAAAAABAAEAAABWwgIAICaRhlOY4EIgjH8R7LKhKHGwsMvb4AAy3WODBIBBKCsYA9TjuhDNDKEVSERezQEL0WrhXucRUQGuik7bFlngzqVW9LMl9XWvLdjFaJtDFqZ1cEZUB0dUgvL3dgP4WJZn4jkomWNpSTIyEAIfkECQoAAAAsAAAAABAAEAAABX4gIAICuSxlOY6CIgiD8RrEKgqGOwxwUrMlAoSwIzAGpJpgoSDAGifDY5kopBYDlEpAQBwevxfBtRIUGi8xwWkDNBCIwmC9Vq0aiQQDQuK+VgQPDXV9hCJjBwcFYU5pLwwHXQcMKSmNLQcIAExlbH8JBwttaX0ABAcNbWVbKyEAIfkECQoAAAAsAAAAABAAEAAABXkgIAICSRBlOY7CIghN8zbEKsKoIjdFzZaEgUBHKChMJtRwcWpAWoWnifm6ESAMhO8lQK0EEAV3rFopIBCEcGwDKAqPh4HUrY4ICHH1dSoTFgcHUiZjBhAJB2AHDykpKAwHAwdzf19KkASIPl9cDgcnDkdtNwiMJCshACH5BAkKAAAALAAAAAAQABAAAAV3ICACAkkQZTmOAiosiyAoxCq+KPxCNVsSMRgBsiClWrLTSWFoIQZHl6pleBh6suxKMIhlvzbAwkBWfFWrBQTxNLq2RG2yhSUkDs2b63AYDAoJXAcFRwADeAkJDX0AQCsEfAQMDAIPBz0rCgcxky0JRWE1AmwpKyEAIfkECQoAAAAsAAAAABAAEAAABXkgIAICKZzkqJ4nQZxLqZKv4NqNLKK2/Q4Ek4lFXChsg5ypJjs1II3gEDUSRInEGYAw6B6zM4JhrDAtEosVkLUtHA7RHaHAGJQEjsODcEg0FBAFVgkQJQ1pAwcDDw8KcFtSInwJAowCCA6RIwqZAgkPNgVpWndjdyohACH5BAkKAAAALAAAAAAQABAAAAV5ICACAimc5KieLEuUKvm2xAKLqDCfC2GaO9eL0LABWTiBYmA06W6kHgvCqEJiAIJiu3gcvgUsscHUERm+kaCxyxa+zRPk0SgJEgfIvbAdIAQLCAYlCj4DBw0IBQsMCjIqBAcPAooCBg9pKgsJLwUFOhCZKyQDA3YqIQAh+QQJCgAAACwAAAAAEAAQAAAFdSAgAgIpnOSonmxbqiThCrJKEHFbo8JxDDOZYFFb+A41E4H4OhkOipXwBElYITDAckFEOBgMQ3arkMkUBdxIUGZpEb7kaQBRlASPg0FQQHAbEEMGDSVEAA1QBhAED1E0NgwFAooCDWljaQIQCE5qMHcNhCkjIQAh+QQJCgAAACwAAAAAEAAQAAAFeSAgAgIpnOSoLgxxvqgKLEcCC65KEAByKK8cSpA4DAiHQ/DkKhGKh4ZCtCyZGo6F6iYYPAqFgYy02xkSaLEMV34tELyRYNEsCQyHlvWkGCzsPgMCEAY7Cg04Uk48LAsDhRA8MVQPEF0GAgqYYwSRlycNcWskCkApIyEAOwAAAAAAAAAAAA==" alt="Loading..." />
				</div>
			</div>
		{{ endForm() }}	
	</div>
	#}


	<div class="row">
		{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/reports/offlineCopyPicturesToFolder/to_process', 'method' : 'post', 'autocomplete' : 'off') }}
			{{ hiddenField('_csrftoken') }}
			{{ hiddenField('next') }}		
			
			{{ partial("partials/ctlDropdown",[
				'value':null, 
				'title':'Offline Issue', 
				'list':_model.Issues(), 
				'field':'_context[xml_report_issue_id]', 
				'width':3, 
				'option_fields':['id', 'publications_id','date_published']
			] )}}
			
			{{ partial("partials/ctlDropdown",[
				'value': null,
				'title': 'DTP Group',
				'list': _model.DtpGroups(), 
				'field': '_context[xml_report_group_id]', 
				'width': 1, 
				'option_fields':['name'],
				'_context': _context
			] ) }}	
			
			<div class="col-lg-2 col-md-2"><br/>
				<button type="submit" class="btn btn-warning">Copy pictures to working folder</button>
			</div>

		{{ endForm() }}	
		
	</div>

	<div class="row">
		{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/reports/offlineCopyPicturesToFolder/processed/1/1', 'method' : 'post', 'autocomplete' : 'off') }}
			{{ hiddenField('_csrftoken') }}
			{{ hiddenField('next') }}		
			
			{{ partial("partials/ctlDropdown",[
				'value':null, 
				'title':'Offline Issue', 
				'list':_model.Issues(), 
				'field':'_context[xml_report_issue_id]', 
				'width':3, 
				'option_fields':['id', 'publications_id','date_published']
			] )}}
			
			{{ partial("partials/ctlDropdown",[
				'value': null,
				'title': 'DTP Group',
				'list': _model.DtpGroups(), 
				'field': '_context[xml_report_group_id]', 
				'width': 1, 
				'option_fields':['name'],
				'_context': _context
			] ) }}	
			
			<div class="col-lg-2 col-md-2"><br/>
				<button 
					type="submit" 
					class="btn btn-info"
					title = "TEST - kopira slike direktno sa ad-a, a za one koje ne postoje ubacuje defaultnu sliku."
					>
					TEST Copy pictures to folder 'processed'
				</button>
			</div>

		{{ endForm() }}	
		
	</div>
	
	<div class="row">
		{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/reports/offLineIssueXml', 'method' : 'post', 'autocomplete' : 'off') }}
			{{ hiddenField('_csrftoken') }}
			{{ hiddenField('next') }}		
			
			{{ partial("partials/ctlDropdown",[
				'value': _context['xml_report_issue_id'], 
				'title':'Offline Issue', 
				'list':_model.Issues(), 
				'field':'_context[xml_report_issue_id]', 
				'width':3, 
				'option_fields':['id', 'publications_id','date_published']
			] )}}
			
			{{ partial("partials/ctlDropdown",[
				'value': _context['xml_report_group_id'],
				'title': 'DTP Group',
				'list': _model.DtpGroups(), 
				'field': '_context[xml_report_group_id]', 
				'width': 1, 
				'option_fields':['name'],
				'_context': _context
			] ) }}	

			{{ partial("partials/ctlCheckbox",[
				'value': 0,
				'title': 'Get Constant Updates',
				'field': '_context[xml_report_is_threaded]', 
				'width': 1, 
				'_context': _context
			] ) }}	

			
			<div class="col-lg-2 col-md-2"><br/>
				<button type="submit" class="btn btn-warning">Create XML for Offline Issue</button>
			</div>

		{{ endForm() }}	
		
	</div>
	
	 
	<div class="row">
		{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/reports/dtpErrors', 'method' : 'post', 'autocomplete' : 'off') }}
			{{ hiddenField('_csrftoken') }}
			{{ hiddenField('next') }}		
			
			{{ partial("partials/ctlDropdown",[
				'value':null, 
				'title':'Offline Issue', 
				'list':_model.Issues(), 
				'field':'_context[xml_report_issue_id]', 
				'width':3, 
				'option_fields':['id', 'publications_id','date_published']
			] )}}
			<div class="col-lg-2 col-md-2"><br/>
				<button type="submit" class="btn btn-warning">DTP ERRORS Report</button>
			</div>
		{{ endForm() }}	 
		
	</div>
	
	
	
	<div class="row">
		{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/reports/dtpMissingPictures', 'method' : 'post', 'autocomplete' : 'off') }}
			{{ hiddenField('_csrftoken') }}
			{{ hiddenField('next') }}		
			
			{{ partial( "partials/ctlDropdown", [
				'value': null, 
				'title': 'Offline Issue', 
				'list': _model.Issues(), 
				'field': '_context[xml_report_issue_id]', 
				'width': 3, 
				'option_fields':['id', 'publications_id','date_published']
			] )}}
			<div class="col-lg-2 col-md-2"><br/>
				<button type="submit" class="btn btn-warning">DTP Missing Pictures Report</button>
			</div>
		{{ endForm() }}	
		
	</div>

	
{#% if _model.isUserAllowed('rptOrderPdf', auth.get_user().id) %#}
	<div class="row">
		{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/reports/orderPdf', 'method' : 'post', 'autocomplete' : 'off') }}
			{{ hiddenField('_csrftoken') }}
			{{ hiddenField('next') }}		
			
			{% set orderId = null %}
			{% if _context['orderPdf_report_order_ids'] is defined %}
				{% if _context['orderPdf_report_order_ids'][0] > 0 %}
					{% set orderId = _context['orderPdf_report_order_ids'][0] %}
				{% endif %}	
			{% endif %}	
			
			<!-- TODO REMOVE THIS  -->
			{#{ partial("partials/ctlTextType",[
				'field':'_context[orderPdf_report_order_ids][]'
				,'value': orderId
				,'value_show': orderId 
				,'search_field':'id'
				, 'return_field':'total'
				, 'table_name':'orders'
				, 'title': 'Document ID'
				, 'additional_where' : ' and 1 = 1 '
				, 'width':3] 
			) }#}
			{{ partial("partials/ctlTextTypeV3",[
				'title' : 'Document ID'
				, 'field' : '_context[orderPdf_report_order_ids][]'
				, 'value' : orderId
				, 'width' : 3
				, 'readonly' : false
				, '_context' : _context
				, 'value_show' : orderId
				, 'model_name' : 'Orders' 
				, 'expr_search' : 'total'
				, 'expr_value' : 'id' 
				, 'expr_show_input' : 'id'
				, 'expr_show_list' : 'total'
				, 'additional_where' : ' and 1 = 1  '
			]) }}			
			

	
			<div class="col-lg-2 col-md-2"><br/>
				<button type="submit" class="btn btn-warning cash-out">Document PDF Preview</button>
			</div>
		{{ endForm() }}	
	</div>
	<div class="row">
	<h3>Upload bank statement</h3>	         
                  <br/>         
                {%- if errors is defined -%}
                    {%- set upload_errors = errors.filter('upload') -%}
                {%- else -%}
                    {% set upload_errors = [] %}
                {%- endif -%}
                {{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/orders/bank-xml-upload', 'method' : 'post', 'enctype' : 'multipart/form-data', 'autocomplete' : 'off', 'class' : 'form-inline orders-xml-upload') }}
                {{ hiddenField('_csrftoken') }}
                <div style="padding-left:20px" class="form-group{{ not(upload_errors is empty) ? ' has-error' : '' }}">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn btn-info btn-file"><span class="fileinput-new"><span class="glyphicon glyphicon-upload"></span> Odabir datoteke (Bank Statement)</span><span class="fileinput-exists"><span class="glyphicon glyphicon-edit"></span> Change file</span><input type="file" name="upload" id="upload"></span>
                        <span class="fileinput-filename-wrap">
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput"><span class="glyphicon glyphicon-remove"></span></a>
                        </span>
                    </div>
                    {%- for err in upload_errors -%}
                        <p class="help-block">{{ err.getMessage() }}</p>
                    {%- endfor -%}
                </div>
                <div style="padding-bottom:8px;" class="form-group text-right">
                    <button class="btn btn-primary">Izvrsi obradu </button>
                </div>
                {{ endForm() }}
                      
            
        
	</div>
	
	
	<div class="row">
		{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/reports/categoriesMappingsList', 'method' : 'post', 'autocomplete' : 'off') }}
			{{ hiddenField('_csrftoken') }}
			{{ hiddenField('next') }}		
			
			{{ partial("partials/ctlDropdown",[
				'value':null, 
				'title':'Publications', 
				'list':_model.Publications(), 
				'field':'_context[report_param_publication_id]', 
				'width':3, 
				'option_fields':['id', 'name']
			] )}}
			

			<div class="col-lg-2 col-md-2"><br/>
				<button 
					type="submit" 
					class="btn btn-info"
					title = "Publication Categories List"
					>
					Publication Categories List
				</button>
			</div>

		{{ endForm() }}	
		
	</div>
