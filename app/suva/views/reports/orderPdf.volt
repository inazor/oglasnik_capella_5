<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous"/>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"/>

{% set documentCount = 0 %}
{% for id in ids %}
	{% set documentCount = documentCount + 1 %}
	{% if documentCount > 1	%} <pagebreak />  {% endif %}
	
	{% set order = _model.Order(id) %}
	{% set user = order.User() %}
	{% set sub_customer = order.SubCustomer() %}
	{% set sales_rep = order.SalesRep() %}
	
	<!-- /////////////////////////////////////////////////////PONUDA/////////////////////////////////////////////////////////////// -->
	{% if order.n_status == 3 %}
		<div class="page" style="font:12px arial, sans-serif;  margin:0;" >
	
			<div class="row">
			<div class="col-xs-7 text-right">&nbsp;</div>
				<div class="col-xs-3 text-right">
					<!-- <img src="http://grupacija.oglasnik.hr/slike/Oglasnik-negativ_grupacija.oglasnik.hr.png"/> -->
					<img src="../public/assets/img/ogl-logo.jpg"/>
				</div>
			</div>
			<br/>
			<div class="row" > 
				<div class="col-xs-3">
					<br/>
					<br/>
					<br/>
					<b>
						{% if user.company_name >'' %}
							{{user.company_name}}
						{% endif %}
						
						{% if !(user.company_name >'') %}
							{{user.first_name}} {{user.last_name}}
						{% endif %}
					</b>
					<br/>{{user.address}}
					<br/>{{user.zip_code}} {{user.city}}
					<br/>{{_model.Country(user.country_id).name}}
					
				</div>
				<div  class="col-xs-3">&nbsp;
					{% if order.n_status == 5 %}
						<h1 style="color:green;">PLAĆENO</h1>	
					{% endif %}			
				</div>
				<div  class="col-xs-4" style="border-bottom:1px solid black; font:9px arial, sans-serif;">
					<strong>Oglasnik d.o.o. poduzeće za novinsko-nakladničku djelatnost</strong> 
					<p>Savska cesta 41<br/>
						HR - 10000 ZAGREB<br/>
						prodaja 01/6102800<br/>
						podrška 01/6102875<br/>
						računovodstvo 01/6102847<br/>
						IBAN: HR1624840081100153885<br/>
						MB: 3615090<br/>
						OIB: 97309929902</p>              
				</div>
			</div>
			<br/>
			<br/>


			<div class="row">
				<div  class="col-xs-2">
					<span>br.Kupca</span>
				</div>
				<div  class="col-xs-3">
					<span>{{user.id}}</span>
				</div>
				<div style="font:16px arial, sans-serif;" class="col-xs-4">
					<span><strong>						
						{% if order.n_status == 5 %}
								Račun br. {{order.id}} / {{ date("Y", strtotime(order.n_quotation_date)) }}
						{% endif %}
						
						{% if order.n_status == 3 %}
								{% if order.n_pbo != null %}
									Ponuda br. {{ order.n_pbo }}
								{% else %}
									Ponuda br. {{order.id+500000000}}
								{% endif %}
						{% endif %}
						
						{% if order.n_status == 8 %}
								Odobrenje
						{% endif %}
						
						{% if order.n_status == 4 %}
								Račun br. {{order.id}} /  {{ date("Y", strtotime(order.n_quotation_date)) }}
						{% endif %}
					</strong></span>
				</div>
			</div>
			
			<br/>
			<br/>
			
			<div class="row">
				<div  class="col-xs-2">OIB:	</div>			
				<div class="col-xs-3">{{user.oib}}</div>
				{% if order.n_invoice_fiscal_number > '' %}
					<div  class="col-xs-2">Broj računa: </div>				
					<div  class="col-xs-2">
						<span>{{order.n_invoice_fiscal_number}}</span>
					</div>
				{% endif %}
			</div>
			<div class="row">
				<div  class="col-xs-2">VAT No.:</div>			
				<div  class="col-xs-3">{{user.n_vat_number}}</div>
			</div>
			<div class="row">
				<div  class="col-xs-2">
					<span>Za kupca:</span>
				</div>			
				<div  class="col-xs-3">
					{% if sub_customer %}
						{{sub_customer.company_name}} {{sub_customer.first_name}} {{sub_customer.last_name}}
					{% endif %}
					
				</div>
			</div>
			

			{% if order.n_status !== 3 %}
				<div class="row">
					<div  class="col-xs-2">
						<span>Operator:</span>
					</div>			
					<div  class="col-xs-3">
						<span>{{_model.User(order.n_operator_id).first_name}} {{_model.User(order.n_operator_id).last_name}}</span>
					</div>
				</div>
			{% endif %}

			{% if order.n_status == 3 %}
				<div class="row">
					<div  class="col-xs-2">
						<span>Ponudu izradio:</span>
					</div>			
					<div  class="col-xs-3">
						{% if sales_rep %}
							<span>{{ sales_rep.first_name}} {{ sales_rep.last_name}}</span>
						{% endif %}
					</div>
				</div>
			{% endif %}

			<br/>
			<br/>
			<br/>
			<div class="row">
				<div  class="col-xs-2">
					<span>Datum ponude:&nbsp;</span>
				</div>			
				<div  class="col-xs-3">
					<span>
						{% if order.n_quotation_date > 0 %}
							{{ date("d.m.Y", strtotime(order.n_quotation_date)) }}
						{% endif %}
					</span>
				</div>
				
			</div>
			
			<div class="row">
				<div  class="col-xs-2">
					<span>&nbsp;</span>
				</div>			
				<div  class="col-xs-2">
					<span>&nbsp;</span>
				</div>
				{% if order.n_invoice_zik > '' %}
					<div  class="col-xs-1">
						<span>ZKI: </span>
					</div>
					<div  class="col-xs-4">
						<span>
							{{ order.n_invoice_zik }}
						</span>
					</div>
				{% endif %}
			</div>
			
			<div class="row">
				<div  class="col-xs-2">
					<span>&nbsp;</span>
				</div>			
				<div  class="col-xs-2">
					<span>&nbsp;</span>
				</div>
				{% if order.n_invoice_jir > '' %}
					<div  class="col-xs-1">
						<span>JIR: </span>
					</div>
					<div  class="col-xs-4">
						<span>
							{{ order.n_invoice_jir }}
						</span>
					</div>
				{% endif %}
			</div>
			
			<br/>
			<br/>

			<table style="width:100%; font:11px arial, sans-serif; border-bottom:1px solid black;">
				<thead style="font-weight:bold; border-bottom:1px solid black">
					<tr style="font-weight:bold; border-bottom:1px solid black">
						{% if order.n_status == 3 %}
							<td style="font-weight:bold; width:4%">Br.</td>
							<td style="font-weight:bold; width:25%">Opis</td>
							<td style="font-weight:bold; width:20%">Naslov oglasa</td>
							<td style="font-weight:bold; width:16%">Datum izdanja</td>
							<td style="font-weight:bold; width:5%">Kol.</td>
							<td style="font-weight:bold; width:5%">JMJ</td>
							<td style="font-weight:bold; width:10%; text-align:right;">Cijena jedinica</td>
							<td style="font-weight:bold; width:10%; text-align:right;">Popusti</td>
							<td style="font-weight:bold; width:10%; text-align:right;">Iznos</td>{{oi_discounts.name}}
						{% else %}
							<td style="font-weight:bold; width:4%">Br.</td>
							<td style="font-weight:bold; width:30%">Opis</td>
							<td style="font-weight:bold; width:31%">Datum objave</td>
							<td style="font-weight:bold; width:5%">Kol.</td>
							<td style="font-weight:bold; width:5%">JMJ</td>
							<td style="font-weight:bold; width:10%; text-align:right;">Cijena jedinica</td>
							<td style="font-weight:bold; width:10%; text-align:right;">Popusti</td>
							<td style="font-weight:bold; width:10%; text-align:right;">Iznos</td>{{oi_discounts.name}}
						{% endif %}
					</tr>
				</thead>
			</table>
			<table style="width:100%; font:11px arial, sans-serif; border-bottom:1px solid black; ">
				<tbody>
					{% set brojac = 0 %}
					{% for item in order.OrdersItems() %}
						{% set brojac += 1 %}  
						{% if order.n_status == 3 %}
							<tr>
								<td valign="top" style="width:4%">{{ brojac }}</td>
								<td valign="top" style="width:25%">{{ item.Product().name }}</td>
								<td valign="top" style="width:20%">{{ _model.Ad(item.ad_id).title }}</td>
								<td valign="top" style="width:16%">
									{% for oii in _model.Insertions("orders_items_id ="~item.id) %}
										{% set issue_date = explode(" ", _model.Issue(oii.issues_id).date_published) %}
										{{issue_date[0]}}<br/>
									{% endfor %}
								</td>
								<td valign="top" style="width:5%">{{ item.qty }}</td>
								<td valign="top" style="width:5%">KOM</td>
								<td valign="top" style="width:10%; text-align:right;">{{ number_format((item.n_price), 2, ',', '.')}}</td>
								<td valign="top" style="width:10%; text-align:right;"> 
									{% for oid in item.OrdersItemsDiscounts() %}
										{{ oid.Discount().name }}<br/>
									{% endfor %}
								</td>
								<td valign="top" style="width:10%; text-align:right;">{{number_format((item.n_total), 2, ',', '.')}}</td>
							</tr>
						{% else %}
							<tr>
								<td valign="top" style="width:4%">{{ brojac }}</td>
								<td valign="top" style="width:11%">{% if order.n_first_published_at != null %} {{ date("d.m.Y", strtotime(item.n_first_published_at)) }} {% endif %}</td>
								<td valign="top" style="width:35%">{{ item.Product().name }}</td>
								<td valign="top" style="width:35%">{{ item.n_first_published_at }} - {{ item.n_expires_at }}</td>
								<td valign="top" style="width:5%">{{ item.qty }}</td>
								<td valign="top" style="width:5%">KOM</td>
								<td valign="top" style="width:10%; text-align:right;">{{ number_format((item.n_price), 2, ',', '.')}}</td>
								<td valign="top" style="width:20%; text-align:right;"> 
									{% for oid in item.OrdersItemsDiscounts() %}
										{{ oid.Discount().name }}<br/>
									{% endfor %}
								</td>
								<td valign="top" style="width:10%; text-align:right;">{{number_format((item.n_total), 2, ',', '.')}}</td>
							</tr>
						{% endif %}
					{% endfor %}
				</tbody>	
			</table>

			
			<table style="width:100%; font:11px arial, sans-serif;">
				{% if !user.n_vat_number > 0 %}
					<tr>
						<td style="font-weight:bold; width:84%; text-align:right;">Ukupno:</td>
						<td style="font-weight:bold; width:16%; text-align:right;">{{number_format((order.n_total + order.n_agency_commission_amount), 2, ',', '.')}}</td>
					</tr>
					<tr>
						<td style="font-weight:bold; width:84%; text-align:right;">Agencijska provizija {{ order.n_agency_commission_percent }}%:</td>
						<td style="font-weight:bold; width:16%; text-align:right;">{{ number_format((order.n_agency_commission_amount), 2, ',', '.') }}</td>
					</tr>
					
					<tr>
						<td style="font-weight:bold; width:84%; text-align:right;">Osnovica:</td>
						<td style="font-weight:bold; width:16%; text-align:right;">{{number_format((order.n_total), 2, ',', '.')}}</td>
					</tr>
					
					<tr>
						<td style="font-weight:bold; width:84%; text-align:right;">PDV {{ round(order.n_tax_amount / order.n_total * 100) }}% :</td>
						<td style="font-weight:bold; width:16%; text-align:right;">{{number_format((order.n_tax_amount), 2, ',', '.')}}</td>
					</tr>
					<tr>
						<td style="font-weight:bold; width:84%; text-align:right;">Ukupno KN s PDV:</td>
						<td style="font-weight:bold; width:16%; text-align:right;">{{number_format((order.n_total_with_tax ), 2, ',', '.')}}</td>
					</tr>
				{% else %}
					<tr>
						<td style="font-weight:bold; width:84%; text-align:right;">Ukupno:</td>
						<td style="font-weight:bold; width:16%; text-align:right;">{{number_format((order.n_total + order.n_agency_commission_amount), 2, ',', '.')}}</td>
					</tr>
				{% endif %}	
			
			</table>
			
			{% if order.n_status == 4 or order.n_status == 5 or order.n_status == 8 %}
				<table class="table table-bordered" style="width:50%; font:11px arial, sans-serif; font-wight:bold;">
					<thead style="font-weight:bold;">
						<tr style="font-weight:bold;" >
							<td>Specifikacija iznosa PDV-a</td>
						</tr>
					</thead>
					<tr>
						<td>PDV %</td>
						<td>Osnovica</td>
						<td>Iznos PDV-a</td>
						<td>Ukupno</td>
					</tr>
					<tr>
						<td>25%</td>
						<td>{{number_format((order.n_total ), 2, ',', '.')}}</td>
						<td>{{number_format((order.n_tax_amount ), 2, ',', '.')}}</td>
						<td>{{number_format((order.n_total_with_tax ), 2, ',', '.')}}</td>
					</tr>
					<tr>
						<td></td>
						<td>{{number_format((order.n_total ), 2, ',', '.')}}</td>
						<td>{{number_format((order.n_tax_amount ), 2, ',', '.')}}</td>
						<td>{{number_format((order.n_total_with_tax ), 2, ',', '.')}}</td>
					</tr>
				</table>
			{% endif %}

		
			<table style="width:100%; font:11px arial, sans-serif;">
				<thead style="font-weight:bold;">
					<tr style="font-weight:bold;">
						<td style="width:10%">Uvjeti plaćanja:</td>
						{% if order.n_status == 5%}
							<td style="width:18%"><b>Plačeno</b></td>		
						{% else %}
							<td style="width:18%">{% if order.n_invoice_date_time_issue != null %} {{order.n_invoice_date_time_issue}} {% endif %}</td>
						{% endif %}
									
					</tr>
				</thead>
			</table>
			
			{% if order.n_status == 4 or order.n_status == 4%}
				<table style="width:100%; font:11px arial, sans-serif;">
					<thead style="font-weight:bold;">
						<tr style="font-weight:bold;">
							<td style="width:100%">Uplatiti na IBAN: HR1624840081100153885</td>									
						</tr>
					</thead>
				</table>
				<table style="width:100%; font:11px arial, sans-serif;">
					<thead style="font-weight:bold;">
						<tr style="font-weight:bold;">
							<td style="width:100%">Prilikom plaćanja u poziv na broj navedite broj računa: {{order.id}}</td>									
						</tr>
					</thead>
				</table>
			{% endif %}

			<table style="width:100%; font:11px arial, sans-serif;">
				<thead style="font-weight:bold;">
					<tr style="font-weight:bold;">
						{% if order.n_status == 3 %}
						<td style="width:20%">Prilikom plaćanja navedite poziv na broj:&nbsp;&nbsp; </td>
						<td style="width:52%;"> {{order.n_pbo}}</td>
						{% endif %}
					</tr>
				</thead>
			</table>
			

			{% if order.n_remark %}
				<br></br><br></br>
				<table style="width:100%; font:11px arial, sans-serif;">
					<thead style="font-weight:bold;">
						<tr style="font-weight:bold;">
							<td style="width:100%"><b>Napomene:</b></td>				
						</tr>
					</thead>
					<tr>
						<td>{{order.n_remark}}</td>
					</tr>
				</table>
				<br/>
			{% endif %}   

			{% if order.n_status == 3 %}
				<table style="width:100%; font:11px arial, sans-serif;">
					<thead style="font-weight:bold;">
						<tr style="font-weight:bold;">
							<td style="width:100%">
								Oglasnik d.o.o. zadržava pravo da tekst i oblik prilagodi oglasnom prostoru, kao i da lektorski intervenira ne mijenjajući pri tome bit sadržaja i izgled teksta.<br/>
								U slučaju spora nadležan je sud u Zagrebu.<br/><br/>
								Izjava naručitelja:<br/>
								Neopozivo naručujem proizvode i usluge pod uvjetima iskazanim na ovoj ponudi / narudžbenici, te	u znak prihvaćanja sadržaja i uvjeta istu
								ovjeravam pečatom tvrtke i vlastoručnim potpisom.<br/><br/>
								Potpis ________________________________  Pečat 	________________________________
							</td>
						</tr>
					</thead>
				</table>
			{% endif %}

			{% if user.n_vat_number > 0 %}
				<table style="width:100%; font:11px arial, sans-serif;">
					<thead style="font-weight:bold;">
						<tr style="font-weight:bold;">
							<td style="width:20%">VAT is not included according to Art.17.1 VATLaw (reverse charge)</td>					
						</tr>
					</thead>
				</table>
			{% endif %} 
			<br/>
			<br/>
			<br/>
		</div>

		<div class="row" style="position:absolute; bottom:0; height:40px; margin-top:40px; font:8.5px arial, sans-serif; width:85%">
			<hr/>
					Poduzeće je registrirano u Trgovačkom sudu u Zagrebu, Reg.broj 080109869; Temeljni kapital: 2.320.000&nbsp;HRK u potpunosti uplaćen; 
					Članovi uprave: Željko Hudoletnjak 
			<br/>
					Poslovna banka: RBA 11; HRK Žiro račun: 2484008-1100153885; Devizni žiro račun: 2484008-1100153885; IBAN: HR1624840081100153885; SWIFT: RZBHHR2X
		</div>
		
		
		
		
	<!-- /////////////////////////////////////////////////////FAKTURA/////////////////////////////////////////////////////////////// -->
	{% elseif order.n_status == 5 or order.n_status == 4%}
		<div class="page" style="font:12px arial, sans-serif;  margin:0;" >	
			<div class="row">
			<div class="col-xs-7 text-right">&nbsp;</div>
				<div class="col-xs-3 text-right">
					<img src="../public/assets/img/ogl-logo.jpg"/>
				</div>
			</div>
			<br/>
			<div class="row" > 
				<div class="col-xs-3">
					<br/>
					<br/>
					<br/>
					<b>
						{% if user.company_name >'' %}
							{{user.company_name}}
						{% endif %}
						
						{% if !(user.company_name >'') %}
							{{user.first_name}} {{user.last_name}}
						{% endif %}
					</b>
					<br/>{{user.address}}
					<br/>{{user.zip_code}} {{user.city}}
					<br/>{{_model.Country(user.country_id).name}}
					
				</div>
				<div  class="col-xs-3">&nbsp;
					{% if order.n_status == 5 %}
						<h1 style="color:green;">PLAĆENO</h1>	
					{% endif %}			
				</div>
				<div  class="col-xs-4" style="border-bottom:1px solid black; font:9px arial, sans-serif;">
					<strong>Oglasnik d.o.o. poduzeće za novinsko-nakladničku djelatnost</strong> 
					<p>Savska cesta 41<br/>
						HR - 10000 ZAGREB<br/>
						prodaja 01/6102800<br/>
						podrška 01/6102875<br/>
						računovodstvo 01/6102847<br/>
						IBAN: HR1624840081100153885<br/>
						MB: 3615090<br/>
						OIB: 97309929902</p>              
				</div>
			</div>
			<br/>
			<br/>


			<div class="row">
				<div  class="col-xs-2">
					<span>br.Kupca</span>
				</div>
				<div  class="col-xs-3">
					<span>{{user.id}}</span>
				</div>
			</div>
			
			<br/>
			<br/>
			
			<div class="row">
				<div  class="col-xs-2">OIB:	</div>			
				<div class="col-xs-3">{{user.oib}}</div>
				<div  class="col-xs-2">Broj računa: </div>				
				<div  class="col-xs-2">
					<span>{{order.n_invoice_fiscal_number}}</span>
				</div>
			</div>
			
			<div class="row">
				<div  class="col-xs-2">VAT No.:</div>			
				<div  class="col-xs-3">{{user.n_vat_number}}&nbsp;</div>
				<div  class="col-xs-2">Datum otpreme </div>				
				<div  class="col-xs-2">
					<span></span>
				</div>
			</div>
			
			<div class="row">
				<div  class="col-xs-2">
					<span>Vaša narudžba:</span>
				</div>			
				<div  class="col-xs-3">
					{{order.id+500000000}}					
				</div>
				<div  class="col-xs-2">Datum dospijeća: </div>				
				<div  class="col-xs-2">
					<span></span>
				</div>
			</div>
				
			<div class="row">
				<div  class="col-xs-2">
					<span>Za kupca:</span>
				</div>			
				<div  class="col-xs-3">&nbsp;
					{% if sub_customer %}
						{{sub_customer.company_name}} {{sub_customer.first_name}} {{sub_customer.last_name}}
					{% endif %}					
				</div>
				<div  class="col-xs-2">Br.našeg naloga </div>				
				<div  class="col-xs-2">
					<span></span>
				</div>
			</div>
			
			<div class="row">
				<div  class="col-xs-2">
					<span>&nbsp;</span>
				</div>			
				<div  class="col-xs-3">
					&nbsp;
				</div>
				<div  class="col-xs-2">Način plaćanja:</div>				
				<div  class="col-xs-2">
					<span>{{order.payment_method}}</span>
				</div>
			</div>
			
			<div class="row">
				<div  class="col-xs-2">
					<span>&nbsp;</span>
				</div>			
				<div  class="col-xs-3">
					&nbsp;
				</div>
				<div  class="col-xs-2">&nbsp;</div>				
				<div  class="col-xs-2">
					<span></span>
				</div>
			</div>
			
			<br/>
			<br/>
			
			
			<div class="row">
				<div  class="col-xs-3">
					<span>Datum i vrijeme izdavanja:</span>
				</div>			
				<div  class="col-xs-2">&nbsp;
					<span></span>
				</div>
			</div>
					
			<div class="row">
				<div  class="col-xs-2">
					<span>Oznaka operatera:</span>
				</div>			
				<div  class="col-xs-2">
					<span>{{_model.User(order.n_operator_id).first_name}} {{_model.User(order.n_operator_id).last_name}}</span>
				</div>
				<div  class="col-xs-1">ZKI:</div>				
				<div  class="col-xs-4">
					<span>{{order.n_invoice_zik}}</span>
				</div>
			</div>
			
			<div class="row">
				<div  class="col-xs-2">
					<span>Interni br dokumenta:</span>
				</div>			
				<div  class="col-xs-2">
					<span>{{order.id+500000000}}</span>
				</div>
				<div  class="col-xs-1">JIR:</div>				
				<div  class="col-xs-4">
					<span>{{order.n_invoice_jir}}</span>
				</div>
			</div>
			
			<br/>
			
			<table style="width:100%; font:11px arial, sans-serif; border-bottom:1px solid black;">
				<thead style="font-weight:bold; border-bottom:1px solid black">
					<tr style="font-weight:bold; border-bottom:1px solid black">
						
							<td style="font-weight:bold; width:4%">Br.</td>
							<td style="font-weight:bold; width:30%">Opis</td>
							<td style="font-weight:bold; width:31%">Opis 2</td>
							<td style="font-weight:bold; width:5%">Kol.</td>
							<td style="font-weight:bold; width:5%">JMJ</td>
							<td style="font-weight:bold; width:10%; text-align:right;">Cijena jedinica</td>
							<td style="font-weight:bold; width:10%; text-align:right;">Popusti</td>
							<td style="font-weight:bold; width:10%; text-align:right;">Iznos</td>{{oi_discounts.name}}
						
					</tr>
				</thead>
			</table>
			<table style="width:100%; font:11px arial, sans-serif; border-bottom:1px solid black; ">
				<tbody>
					{% set brojac = 0 %}
					{% for item in order.OrdersItems() %}
						{% set brojac += 1 %}  
				
							<tr>
								<td valign="top" style="width:4%">{{ brojac }}</td>
								<td valign="top" style="width:30%">{{ item.Product().name }}</td>
								<td valign="top" style="width:31%">
									{% if item.Product().is_online_product == 1 %}
										{{ item.n_first_published_at }} - {{ item.n_expires_at }}
									{% else %}
										Broj objava: {{item.qty}}
									{% endif %}
								</td>
								<td valign="top" style="width:5%">{{ item.qty }}</td>
								<td valign="top" style="width:5%">KOM</td>
								<td valign="top" style="width:10%; text-align:right;">{{ number_format((item.n_price), 2, ',', '.')}}</td>
								<td valign="top" style="width:10%; text-align:right;"> 
									{% for oid in item.OrdersItemsDiscounts() %}
										{{ oid.Discount().name }}<br/>
									{% endfor %}
								</td>
								<td valign="top" style="width:10%; text-align:right;">{{number_format((item.n_total), 2, ',', '.')}}</td>
							</tr>
						
					{% endfor %}
				</tbody>	
			</table>

			
			<table style="width:100%; font:11px arial, sans-serif;">
				{% if !user.n_vat_number > 0 %}
					<tr>
						<td style="font-weight:bold; width:84%; text-align:right;">Ukupno:</td>
						<td style="font-weight:bold; width:16%; text-align:right;">{{number_format((order.n_total + order.n_agency_commission_amount), 2, ',', '.')}}</td>
					</tr>
					<tr>
						<td style="font-weight:bold; width:84%; text-align:right;">Agencijska provizija {{ order.n_agency_commission_percent }}%:</td>
						<td style="font-weight:bold; width:16%; text-align:right;">{{ number_format((order.n_agency_commission_amount), 2, ',', '.') }}</td>
					</tr>
					
					<tr>
						<td style="font-weight:bold; width:84%; text-align:right;">Osnovica:</td>
						<td style="font-weight:bold; width:16%; text-align:right;">{{number_format((order.n_total), 2, ',', '.')}}</td>
					</tr>
					
					<tr>
						<td style="font-weight:bold; width:84%; text-align:right;">PDV {{ round(order.n_tax_amount / order.n_total * 100) }}% :</td>
						<td style="font-weight:bold; width:16%; text-align:right;">{{number_format((order.n_tax_amount), 2, ',', '.')}}</td>
					</tr>
					<tr>
						<td style="font-weight:bold; width:84%; text-align:right;">Ukupno KN s PDV:</td>
						<td style="font-weight:bold; width:16%; text-align:right;">{{number_format((order.n_total_with_tax ), 2, ',', '.')}}</td>
					</tr>
				{% else %}
					<tr>
						<td style="font-weight:bold; width:84%; text-align:right;">Ukupno:</td>
						<td style="font-weight:bold; width:16%; text-align:right;">{{number_format((order.n_total + order.n_agency_commission_amount), 2, ',', '.')}}</td>
					</tr>
				{% endif %}	
			
			</table>
			
			{% if order.n_status == 4 or order.n_status == 5 or order.n_status == 8 %}
				<table class="table table-bordered" style="width:50%; font:11px arial, sans-serif; font-wight:bold;">
					<thead style="font-weight:bold;">
						<tr style="font-weight:bold;" >
							<td>Specifikacija iznosa PDV-a</td>
						</tr>
					</thead>
					<tr>
						<td>PDV %</td>
						<td>Osnovica</td>
						<td>Iznos PDV-a</td>
						<td>Ukupno</td>
					</tr>
					<tr>
						<td>25%</td>
						<td>{{number_format((order.n_total ), 2, ',', '.')}}</td>
						<td>{{number_format((order.n_tax_amount ), 2, ',', '.')}}</td>
						<td>{{number_format((order.n_total_with_tax ), 2, ',', '.')}}</td>
					</tr>
					<tr>
						<td></td>
						<td>{{number_format((order.n_total ), 2, ',', '.')}}</td>
						<td>{{number_format((order.n_tax_amount ), 2, ',', '.')}}</td>
						<td>{{number_format((order.n_total_with_tax ), 2, ',', '.')}}</td>
					</tr>
				</table>
			{% endif %}

		
			<table style="width:100%; font:11px arial, sans-serif;">
				<thead style="font-weight:bold;">
					<tr style="font-weight:bold;">
						<td style="width:10%">Uvjeti plaćanja:</td>
						{% if order.n_status == 5%}
							<td style="width:18%"><b>Plačeno</b></td>		
						{% else %}
							<td style="width:18%">{% if order.n_invoice_date_time_issue != null %} {{order.n_invoice_date_time_issue}} {% endif %}</td>
						{% endif %}
									
					</tr>
				</thead>
			</table>
			
			
		<br/>
		<br/>
			
			<div class="row">
				<div  class="col-xs-1">
					<span>Fakturirao:</span>
				</div>			
				<div  class="col-xs-3">
					&nbsp;
				</div>
				<div  class="col-xs-2">
					<span>Odgovorna osoba:</span>
				</div>			
				<div  class="col-xs-3">
					&nbsp;
					{{odg_osoba_id}}
				</div>
			</div>
			
			{% if order.n_status == 4 %}
				<table style="width:100%; font:11px arial, sans-serif;">
					<thead style="font-weight:bold;">
						<tr style="font-weight:bold;">
							<td style="width:100%">Uplatiti na IBAN: HR1624840081100153885</td>									
						</tr>
					</thead>
				</table>
				<table style="width:100%; font:11px arial, sans-serif;">
					<thead style="font-weight:bold;">
						<tr style="font-weight:bold;">
							<td style="width:100%">Prilikom plaćanja u poziv na broj navedite broj računa: {{order.id}}</td>									
						</tr>
					</thead>
				</table>
			{% endif %}

			
			

			{% if order.n_remark %}
				<br></br><br></br>
				<table style="width:100%; font:11px arial, sans-serif;">
					<thead style="font-weight:bold;">
						<tr style="font-weight:bold;">
							<td style="width:100%"><b>Napomene:</b></td>				
						</tr>
					</thead>
					<tr>
						<td>{{order.n_remark}}</td>
					</tr>
				</table>
				<br/>
			{% endif %}   

			

			{% if user.n_vat_number > 0 %}
				<table style="width:100%; font:11px arial, sans-serif;">
					<thead style="font-weight:bold;">
						<tr style="font-weight:bold;">
							<td style="width:20%">VAT is not included according to Art.17.1 VATLaw (reverse charge)</td>					
						</tr>
					</thead>
				</table>
			{% endif %} 
			<br/>
			<br/>
			<br/>
		</div>

		<div class="row" style="position:absolute; bottom:0; height:40px; margin-top:40px; font:8.5px arial, sans-serif; width:85%">
			<hr/>
					Poduzeće je registrirano u Trgovačkom sudu u Zagrebu, Reg.broj 080109869; Temeljni kapital: 2.320.000&nbsp;HRK u potpunosti uplaćen; 
					Članovi uprave: Željko Hudoletnjak 
			<br/>
					Poslovna banka: RBA 11; HRK Žiro račun: 2484008-1100153885; Devizni žiro račun: 2484008-1100153885; IBAN: HR1624840081100153885; SWIFT: RZBHHR2X
		</div>	
		
	
	
		
	{% endif %}
{% endfor %}