{% if _is_ajax_submit is not defined %}
	{% set _is_ajax_submit = '' %}
{% endif %}

<div class="row">
	<!-- FlashSession -->
	<div class="col-lg-12 col-md-12">
		{{ partial("partials/blkFlashSession") }}
	</div>
</div>

<div class="row">
	<div class="col-lg-12 col-md-12">
		<div><h1 class="page-header">{% if entity.id is not defined %}New {% endif %}Ad {{ entity.id}}</h1></div>
		{#{ partial("partials/blkFlashSession") }#}
		{% if entity is defined %}
			{% set _context['ajax_edit'] = true %}
		
			{{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
				{{ hiddenField('_csrftoken') }}
				{{ hiddenField('next') }}
				<input type="hidden" name="id" value="{{entity.id}}"></input>
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								{% if entity.id is defined %}
									<div class="pull-right">[ID: <b>{{ entity.id }}</b> ]</div>
								{% endif %}
								<h3 class="panel-title">Ad details</h3>
							</div>
						</div>
					</div>
				<div class="row">
					{{ partial("partials/ctlDropdown",[
						'value':entity.category_id, 
						'title':'Category', 
						'list': _model.Categories(' transaction_type_id > 0 '),
						'width':12, 
						'option_fields':['n_path'],
						'field':'category_id', 
						'_context': _context
					] ) }}
					
					{{ partial("partials/ctlText",[
						'value':entity.phone1, 
						'title':'Phone 1', 
						'width': 6, 
						'field':'phone1', 
						'_context': _context
					] )}}
					
					{{ partial("partials/ctlText",[
						'value':entity.phone2, 
						'title':'Phone 2', 
						'width': 6, 
						'field':'phone2', 
						'_context': _context
					] )}}		
				</div>
				<div class= "row">
					{% for cf in entity.Category().CategoriesFieldsets([ 'order': 'sort_order' ]) %}
					
						{% if cf.name %}
							<div class="col-lg-12 col-md-12">
								<div class = "panel panel-default">
									<div class = "panel-heading>
										<h3 class = "panel-title">{{ cf.name }}</h3>
									</div>
								</div>
							</div>
						{% endif %}
											
						{% for cfp in cf.CategoriesFieldsetsParameters(['order':'sort_order']) %}
							{% set parameter = cfp.Parameter() %}
							
							{% set properties = cfp.data ? json_decode(cfp.data) : json_decode(cfp.level) %}
							{{ partial("partials/blkParameters") }}
						{% endfor %}
					{% endfor %}
				</div>
					
	
								</div>
							</div>
						</div>
					</div>
				</div>
				{% include "partials/btnSaveCancel.volt" %}
			{{ endForm() }}
		{% else %}
			ENTITY NIJE DEFINIRAN
		{% endif %}
	</div>
</div>

