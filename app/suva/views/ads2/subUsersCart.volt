											
{% set _url = '/suva/ads2/subUsersCart/' ~ user.id  %}
{% set _is_ajax_submit = true %}
{% set _window_id = 'subUsersCart' %}

{% set _active_tab = (array_key_exists('_active_tab', _context) ? _context['_active_tab'] : '') %}


<!-- ZA AŽURIRANJE BROJA ELEMENATA NA GORNJEM PROZORU -->

<script>
	{% set count_elements = 0 %}
	{% for ao in user.Orders('n_status = 1')  %}
		{% for oi in ao.OrdersItems() %}
			{% set count_elements +=1 %} {% set QTY = oi.qty %}
		{% endfor %}
	{% endfor %}
	{% if count_elements > 0 %}
		$( '#count_items_users_cart' ).html( '({{ count_elements }}) ');
	{% else %}
		$( '#count_items_users_cart' ).html('');
	{% endif %}
	
</script>

{{ form(NULL, 'id': '_form_' ~ _window_id , 'enctype':"multipart/form-data", 'method': 'post', 'autocomplete': 'off') }}
	{{ hiddenField('next') }}
	{{ hiddenField('_csrftoken') }}
	{{ hiddenField(['user_id', 'value': user.id]) }}
	
	<input type="hidden" name="_context[selected_user_id]" value = "{{user.id}}" ></input>
	<input type="hidden" name="_context[clicked_action]" id="_clicked_action_subUsersCart" value = "" ></input>


	<input type="hidden" name="moderation" value="ok"/> 
	<input type="hidden" name = "_context[_active_tab]" id="_context_active_tab_subUsersCart" value="{{ _active_tab }}"></input>
	
 	<!-- DODANO KAO PARAMETAR AKCIJE -->
	<input type="hidden" name="_context[clicked_action_params][user_id]" value="{{ user.id }}"/> 
	<input type="hidden" name="_context[clicked_action_params][selected_user_id]" value="{{ _context['selected_user_id'] }}"/> 
	<input type="hidden" name="_context[clicked_action_params][logged_user_id]" value="{{ auth.get_user().id }}"/> 
	
	

	<div class="row">
		<div class="col-lg-12 col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					{% if _is_ajax_submit %}
						{# 
						NE TREBA VIŠE, RADI PREKO AUTOMATSKIH AKCIJA
						<span 
							class="btn btn-primary" 
							style="text-align:right; height:25px; margin-bottom:5px;" 
							onclick="
								$('#_clicked_action_subUsersCart').val('to_offer');
								$('#_subUsersCart_context_clicked_action_params_orders_items').val ( $('#users_cart_tab').val() );
								console.log ($('#1234').val());
								ajaxSubmit( '{{ _url }}' , '{{_window_id}}' , '_form_{{_window_id}}' );
							"

							>
							To Offer
						</span>
				
						<span 
							class = "btn btn-primary" 
							style = "text-align:right; height:25px; margin-bottom:5px;" 
							onclick = "
								$('#_clicked_action_subUsersCart').val('to_free_ads');
								ajaxSubmit( '{{ _url }}' , '{{_window_id}}' , '_form_{{_window_id}}' );
								"
							>
							To Free Ads
						</span>
						<span 
							class = "btn btn-primary" 
							style = "text-align:right; height:25px; margin-bottom:5px;" 
							onclick = "
								if ( confirm('Are you sure to remove selected items?')){
									$('#_clicked_action_subUsersCart').val('remove_items_from_cart');
									ajaxSubmit( '{{ _url }}' , '{{_window_id}}' , '_form_{{_window_id}}' );
								}
								"
							>
							Remove Items
						</span>
						#}
					{% else %}
						<button 
							class="btn btn-primary" 
							style="text-align:right; height:25px; margin-bottom:5px;" 
							type="submit" 
							name="save"
							onclick="$('#_clicked_action').val('to_offer');
									 $('#_clicked_action_subUsersCart').val('to_offer');
							"

							>
							To Offer
						</button>
				
						<button 
							class = "btn btn-primary" 
							style = "text-align:right; height:25px; margin-bottom:5px;" 

							type = "submit" 
							name = "save"
							onclick = "$('#_clicked_action').val('to_free_ads');
									   $('#_clicked_action_subUsersCart').val('to_free_ads');
							"
							>
							To Free Ads
						</button>
						<button 
							class = "btn btn-primary" 
							style = "text-align:right; height:25px; margin-bottom:5px;" 

							type = "submit" 
							name = "save"
							onclick = "
								if ( confirm('Are you sure to remove selected items?')){
									$('#_clicked_action').val('remove_items_from_cart');
									$('#_clicked_action_subUsersCart').val('remove_items_from_cart');
								}
								"
							>
							Remove Items
						</button>
					{% endif %}
					<span 
						class="btn btn-primary" 
						style="text-align:right; height:25px; margin-bottom:5px;" 
						
						onclick="
							var options = document.getElementById('users_cart_tab').options; 
							var count = 0;	 
							var total = 0.0;
							for ( var i=0; i < options.length; i++ ) {
								
								var option = options[i];
								if (option.selected) {
									count++;
									var price_cart = option.getAttribute('price_cart');
									var price = parseFloat (price_cart);
									total += price;
								}
							 }
							 total_pdv = total * 1.25;
							 $('.cart_total').html(total.toFixed(2));
							 $('.cart_total_pdv').html(total_pdv.toFixed(2));
							 $('.selected_carts').html(count); 
						"
								
						>
						Calculate
					</span>
					
					{% for action_id in _model.getAllowedActionIds( auth.get_user().id, 1, 'orders-items' ) %}
							{% set action = _model.Action( action_id ) %}
							{{ action.buttonHtml_submitViaActionInputOnForm( [
								'action_input_id': '_clicked_action_' ~ _window_id
								,'is_ajax' : _is_ajax_submit
								,'action_url' : _url
								,'action_window_id' : _window_id
								,'require_confirmation': action.require_confirmation
							] ) }}
					{% endfor  %}

					
					 
					<h3 class="panel-title" style="font-family:Courier; font-size:12px;" >
						{#{ str_replace("+","&nbsp;","ORDER++OBJ.++AD+++++DATE+FROM+++DATE+TO+++QTY+PRICE++++++PRODUCT++++++++++++++++++++++++CATEGORY+++++++++NASLOV OGLASA")}#}
						{{ str_replace("+","&nbsp;","ORDER++OBJ.++AD+++++DATE+FROM+++DATE+TO+++QTY+++++PRICE+PRODUCT++++++++++++++++++++++++CATEGORY+++++++++++++++++++++++NASLOV OGLASA+++AdTaker")}}
					</h3>

				</div>
				<div class="panel-body">
					<select
					id="users_cart_tab"
					
					style="display:block !important; width:100%; height:300px;"
					
					name="_context[clicked_action_params][orders_items][]" 
					
					multiple
					
					
					>
					
						{#% for oi in user.Orders("n_status = 1").OrdersItems(['order': 'id desc'])  %#}{# ovo gore ne radi #}

						{% for ao in user.Orders("n_status = 1")  %}
							{% for oi in ao.OrdersItems(['order': 'id desc', 'limit':30]) %}	
							
							<!-- PRIVREMENE VRIJEDNOSTI -->
							{% set product_name = '' %}{% if oi.Product() %}{% set product_name = oi.Product().name %}{% endif %}
							{% set category_path = '' %}{% if oi.Category() %}{% set category_path = oi.Category().n_path %}{% endif %}
							{% set ad_desc = '' %}{% if oi.Ad() %}{% set ad_desc = oi.Ad().title %}{% endif %}
							{% set latePublished = ( oi.n_first_published_at < date('Y-m-d') ) ? true : false %}
							{% set _now = strtotime(date('Y-m-d', time())) %}{# bez sati i minuta, da bi sve danas bilo ne-crveno #}
							{% set _item_n_first_published_at = strtotime(oi.n_first_published_at) %}

							
						
							<option 
									
									price_cart = "{{oi.n_total}}" 
									id = "{{oi.id}}"
									quantity = "{{ oi.qty }}"
									onmouseover="this.style.backgroundColor='#ffa6a6'"
									onmouseout="this.style.backgroundColor=''"
									style="
										font-family:Courier; font-size:12px;
										{% if _item_n_first_published_at < _now %} color:red; {% endif %}
										
										"
									ondblclick = "window.open ('/suva/orders-items/edit/' + $(this).val() +'?_context[_kill_tab_on_exit]=1','_blank');" 
									target="_blank";
									value="{{oi.id}}"
									{% if latePublished %}title = "Late published ( {{ oi.n_first_published_at }} )"{% endif %}
									>

								
								<!-- public function txtPad ( $pValue, $pLength, $pType = 'text', $pAlign = null, $pDecimals = null ) -->
								
								{{ _model.txtPad( oi.id , 6 ) }}
								{{ _model.txtPad( oi.Publication().slug , 4 ) }}
								{{ _model.txtPad( oi.ad_id , 6 ) }}
								
								{{ _model.txtPad( oi.n_first_published_at , 10, 'date' ) }}
								{{ _model.txtPad( oi.n_expires_at , 10, 'date' ) }}
								
								{{ _model.txtPad( oi.qty , 3, 'numeric', null, 0 ) }}
								{{ _model.txtPad( oi.n_total , 10, 'numeric' ) }}
								{{ _model.txtPad( product_name , 30 ) }}
								{{ _model.txtPad( category_path ~ '  ' , 30, 'text', 'right' ) }}
								{{ _model.txtPad( ad_desc , 100 ) }}
								{{ _model.txtPad( ( oi.n_ad_taker_id > 0 ? _model.User( oi.n_ad_taker_id ).username : 'n/a') , 20 ) }}								
								
								
							</option>
							{% endfor %}
						{% endfor %}

					</select>
				</div>
			</div>
		</div>
	</div>
{{ endForm() }}

	<div class="row">
		<div class="col-lg-1 col-md-1">
			<span>Selected: </span>
		</div>
		<div class="col-lg-1 col-md-1">
			<span class="selected_carts"></span>
		</div>
		<div class="col-lg-1 col-md-1">
			<span>&nbsp;</span>
		</div>
		<div class="col-lg-1 col-md-1">
			<span>Total: </span>
		</div>
		<div class="col-lg-1 col-md-1">
			<span class="cart_total"></span>
		</div>
		<div class="col-lg-1 col-md-1">
				<span>Total(PDV): </span>
			</div>
			<div class="col-lg-1 col-md-1">
				<span class="cart_total_pdv"></span>
			</div>
	</div>
	{#<script>
	function prDefault(e) {
	
	e.preventDefault(); 
	}
	
	function totalAndtotalPDV( n_total) {
	
		$('.cart_total').html({{oi.n_total}}.toFixed(2));
		$('.cart_total_pdv').html(({{oi.n_total}}*1.25).toFixed(2));
	
	}
	
	
	
	</script>#}
