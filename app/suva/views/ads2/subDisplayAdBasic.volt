{# AJAX BLOK - NA SVAKU FORMU #}
{% if _form_id is not defined %}{% set _form_id = '1' %}{% endif %}
{% if _is_ajax_submit is not defined %}{% set _is_ajax_submit = false %}{% endif %}
{% set _subform_id = 0 %}


	<!-- GORNJI DIO SA USER DETAILS, CONTACT INFO, INSERTIONS -->
	<div class="row">
		<div class="col-md-12 col-lg-12">
			<div class="panel-heading">
				<h3 class="panel-title">Ad details
					{% if ad is defined and ad.id is defined %}
						<div class="pull-right">
							<a target="_blank" href="{{ ad.get_frontend_view_link() }}" class="btn btn-xs btn-default"><i class="fa fa-fw fa-eye"></i>View ad</a>
						</div>							
						<div class="pull-right">[MODERATION: <b>{{ ad.moderation }}</b> ]</div>
						<div class="pull-right">[ID: <b>{{ ad.id }}</b> ]</div>
						{% if ad.soft_delete == 1%}
							<div class="pull-right"><b style="color:red">THIS AD IS SOFT DELETED AND CANNOT BE MODIFIED!</b></div>
						{% endif %}
					{% endif %}
				</h3>
			</div>
		
		</div>
		
		<!-- USER DETAILS -->
		<div class="col-md-2 col-lg-2">
			<div class="panel panel-default" style="overflow-x:scroll; height:160px; white-space:nowrap;">

				<div class="panel-heading">
					<h3 class="panel-title">
						User {{ linkTo(['suva/korisnici/crud/' ~ ad.user_id, ad.User().username, 'target': '_blank', 'class':'username_redirect ']) }}
						&nbsp;[ID:<b>{{ ad.user_id }}</b>]
					</h3>				
				</div>
				<div class="panel-body">
					<div class="tab-content">
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<span>{{ad.User().company_name}}</span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<span>{{ad.User().first_name}} {{ad.User().last_name}}</span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<span>{{ad.User().address}}</span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<span>{{ad.User().zip_code}} {{ad.User().city}}</span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<span>OIB:{{ad.User().oib}}</span>
							</div>
						</div>							
					</div>
				</div>
			</div>
		</div>
		
		<!-- CONTACT INFO -->
		<div class="col-md-3 col-lg-3">
		
			
			{% set _ids = [] %}
			{% set ucs = ad.User().UsersContacts() %}
			{% for uc in ucs %}
				{% set _ids[uc.id] = uc.id %}
			{% endfor %}
			
			{% set _subform_id += 1 %}
			{{ partial("partials/wraSubFormAjaxV2",[
				'_window_id' : 'subUsersContacts',
				'_url': '/suva/users-contacts/index/' ~ ad.user_id
			] ) }}
			
		
		</div>
		
		<!-- INSERTIONS -->
		<div class="col-md-7 col-lg-7">

			<!-- subInsertions -->
			{% set _subform_id += 1 %}
			{{ partial("partials/wraSubFormAjax",[
				'_form_id' : _form_id,
				'_subform_id' : 'subInsertions',
				'_url': '/suva/ads2/subInsertions/' ~ ad.id, 
				'_model': _model, 
				'_context': _context, 
				'p': ['ad' : ad ]
			] ) }} 

		</div>
	</div>		

	<!-- DONJI DIO S EDITOROM AD-a, PUBLICATIONS-ISSUES, MODERATION, AD INFO -->
	<div class="row">
	
		<!--LIJEVA POLOVINA EKRANA  - EDITOR AD-a-->
		<div class="col-lg-8 col-md-8">
			{{ form(NULL, 'id': 'frm_ads', 'enctype':"multipart/form-data", 'method': 'post', 'autocomplete': 'off') }}
				{{ hiddenField('next') }}
				{{ hiddenField('_csrftoken') }}
				{% if ad is defined and ad.id is defined %}
					{{ hiddenField(['ad_id', 'value': ad.id]) }}
				{% endif %}
				{{ hiddenField(['user_id', 'value': ad.user_id]) }}
				<input type="hidden" name="_context[selected_user_id]" value = "{{ad.user_id}}" ></input>
				<input type="hidden" name="_context[clicked_action]" id="_clicked_action_subAdBasic" value = "" ></input>

				<input type="hidden" name="n_first_uploaded_photo_id" value="{{ad.n_first_uploaded_photo_id}}"/> 
				<input type="hidden" name="moderation" value="ok"/> 
				<input type="hidden" id="_context_active_tab_subAdBasic" name = "_context[_active_tab]" value="{{ _context['_active_tab'] }}"></input>
				<input type="hidden" name="category_id" value="{{ad.category_id}}"></input>				
				<input type="hidden" name="n_is_display_ad" id="cbx_hidden_n_is_display_ad" value="{{ ad.n_is_display_ad }}"></input>
						 

				<div class="panel panel-default">
					<div class="row">
						{{ partial("partials/ctlText",[
							'title': 'Phone 1',
							'value':ad.phone1, 
							'field':'phone1', 
							'width':4
						] ) }}
						
						{{ partial("partials/ctlText",[
							'title': 'Phone 2',
							'value':ad.phone2, 
							'field':'phone2', 
							'width':4
						] ) }}						
					</div>
					
					<div class="row">
						{{ partial("partials/ctlText",['value':ad.title, 'title':'Title', 'width':8, 'field':'title', '_context': _context] ) }}
					</div>
					
					<div class="row">
						{{ partial("partials/ctlTextTypeV3",[
							'title' : "Publication"
							, 'field' : 'ad_n_publication_id'
							, 'value' : ad.n_publication_id
							, 'width' : 5
							, '_context' : _context
							, 'value_show' :  _model.Publication( ad.n_publication_id ).name
							, 'model_name' : 'Publications' 
							, 'expr_search' : 'name'
							, 'expr_value' : 'id' 
							, 'expr_show_input' : 'name'
							, 'expr_show_list' : 'name'
							, 'additional_where' : ' and is_active = 1 order by name '
						]) }}
				
						{{ partial("partials/ctlTextTypeV3",[
							'title' : "Publication Category"
							, 'field' : 'ad_n_category_mapping_id'
							, 'value' : ad.n_category_mapping_id
							, 'width' : 5
							, '_context' : _context
							, 'value_show' :  _model.CategoryMapping( ad.n_category_mapping_id ).name
							, 'model_name' : 'CategoriesMappings' 
							, 'expr_search' : 'n_path'
							, 'expr_value' : 'id' 
							, 'expr_show_input' : 'n_path'
							, 'expr_show_list' : 'n_path'
							, 'additional_where' : ' and n_publications_id = __P1__  order by n_path '
							, 'input_P1' : 'ad_n_publication_id'
						]) }}
					</div>
				</div>
				<div class="panel panel-default image_upload_block">
					<div class="row">
						<div class="col-md-3 col-lg-3">
							<input name="imageUpload" id="imageUpload" type="file" onchange="loadFile(event)"></input>
						</div>
						<div class="col-md-3 col-lg-3">
							<span 
								class="image_submit btn btn-primary"
								value="Image submit" 
								onclick="
									if($('#imageUpload').prop('files')[0] > ''){
										var file_data = $('#imageUpload').prop('files')[0];
										var form_data = new FormData();
										form_data.append('file', file_data);  
										$.ajax({
											type:'POST',
											url: '/suva/ajax/imageUpload',
											data:form_data,
											cache:false,
											contentType: false,
											processData: false,
											success:function(data){
												
											}						
										});
									}else{
										alert('Odaberite sliku')
									}
								">Image submit								
							</span>
							<script>
								$('.image_submit').click(function() {
									if($('#imageUpload').prop('files')[0] > '') setTimeout(function() {
										$('.submit_image_upload').click()
									}, 500); 
								});
							</script>
						</div>						
					</div>
					<br/>
					<div class="row">
						<div class="col-md-3 col-lg-3">
							<span>Current preview</span>
						</div>
						<div class="col-md-3 col-lg-3">
							<span>Uploaded preview</span>
						</div>
						<div class="col-md-3 col-lg-3">
							<span>Final preview</span>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3 col-lg-3">
							<span>X</span>
						</div>
						<div class="col-md-3 col-lg-3">
							<span>X</span>
						</div>
						<div class="col-md-3 col-lg-3">
							<span>X</span>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3 col-lg-3">
							<img id="current_preview" style="width:100%;"/>
						</div>
						<div class="col-md-3 col-lg-3">
							{% if ad.n_picture_path != '' %}
								<script>document.getElementById("delete_uploaded_photo").style.display = "inline";</script>
								<img src="/{{ad.n_picture_path}}" id="uploaded_preview" style="width:100%;"/>
							{% endif %}
						</div>
						<div class="col-md-3 col-lg-3">
							<img id="final_preview" style="width:100%;"/>
						</div>
					</div>
					
				</div>
								
				<br/>
				<button class="btn btn-primary submit_image_upload" accesskey="s" type="submit" name="save">Save</button>
			
			{{ endForm() }}
		</div> 
		<!-- lijeva polovina ekrana zavr�ava-->
		
		{% if ad.id > 0 %}
			<!-- DESNA POLOVINA EKRANA -->
			<div class="col-md-4 col-lg-4">
				<div class = "row">
				
					<!-- FORMA -->
					{{ form(NULL, 'id': 'frm_ads', 'enctype':"multipart/form-data", 'method': 'post', 'autocomplete': 'off') }}
						{{ hiddenField('next') }}
						{{ hiddenField('_csrftoken') }}
						{% if ad is defined and ad.id is defined %}
							{{ hiddenField(['ad_id', 'value': ad.id]) }}
						{% endif %}
						{{ hiddenField(['user_id', 'value': ad.user_id]) }}
						<input type="hidden" name="_context[selected_user_id]" value = "{{ad.user_id}}" ></input>
						<input type="hidden" name="_context[clicked_action]" id="_clicked_action_subPublicationsAndIssues" value = "" ></input>

						<input type="hidden" name="n_first_uploaded_photo_id" value="{{ad.n_first_uploaded_photo_id}}"/> 
						<input type="hidden" name="moderation" value="ok"/> 
						<input type="hidden" id="_context_active_tab_subPublicationsAndIssues" name = "_context[_active_tab]" value="{{ _context['_active_tab'] }}"></input>
			
						<!-- Publications & Issues -->
						
							{{ partial("ads2/subPublicationsAndIssues" ) }}
						
					
						<!-- Moderation  -->
						{% if _user.isAllowed('frmAdsEdit_secModeration') %}
							{{ partial("ads2/subModeration" ) }}
						{% endif %}

					{{ endForm() }}	
					
					<!-- Informacije o datumima -->
					{% if ad is defined and ad.id is defined %}
						<div class="col-md-12 col-lg-12">
							<table class="table table-condensed table-striped table-bordered">
								<tr{{ ad.latest_payment_state == constant('Baseapp\Models\Ads::PAYMENT_STATE_CANCELED_ORDER') ? ' class="danger"' : '' }}>
									<td>Last payment state</td>
									<td><span class="fa fa-money"></span> {{ Models_Ads__getPaymentStateString(ad.latest_payment_state) }}</td>
								</tr>
								<tr>
									<td>Created</td>
									<td><span class="fa fa-clock-o"></span> {{ date('d.m.Y H:i:s', ad.created_at) }}</td>
								</tr>
								<tr>
									<td>Last modified</td>
									<td><span class="fa fa-clock-o"></span> {{ date('d.m.Y H:i:s', ad.modified_at) }}</td>
								</tr>
								<tr>
									<td>Virtual publish date (sort)</td>
									<td><span class="fa fa-clock-o"></span> {{ date('d.m.Y H:i:s', ad.sort_date) }}</td>
								</tr>
								<tr>
									<td>First published at</td>
									<td><span class="fa fa-clock-o"></span> {{ date('d.m.Y H:i:s', ad.first_published_at) }}</td>
								</tr>
								<tr>
									<td>Last published at</td>
									<td><span class="fa fa-clock-o"></span> {{ date('d.m.Y H:i:s', ad.published_at) }}</td>
								</tr>
								{% set classname = ad.isExpired() ? 'danger' : 'success' %}
								<tr class="{{ classname }}">
									<td>Expire{{ ad.isExpired() ? 'd' : 's' }}</td>
									<td><span class="fa fa-clock-o"></span> {{ date('d.m.Y H:i:s', ad.expires_at) }}</td>
								</tr>
								{% set classname = ad.active ? 'success' : 'danger' %}
								<tr class="{{ classname }}">
									<td><b>Currently active</b></td>
									<td><span class="fa fa-{{ ad.active ? 'check' : 'close' }}"></span> {{ ad.active ? 'YES' : 'NO' }}</td>
								</tr>
							</table>
						</div>
					{% endif %}
				</div>

				
			</div> <!-- desna polovina ekrana -->
		{% endif %}
	</div>


<script>

	 var loadFile = function(event) {
		 var current_preview = document.getElementById('current_preview');
		 current_preview.style.display = "inline"
		 current_preview.src = URL.createObjectURL(event.target.files[0]);			
		 
	}
	
</script>