{#{ var_dump( ad ) }#}


{% if _is_ajax_submit is not defined %}
	{% set _is_ajax_submit = '' %}
{% endif %}

{%  set _form_id = 'frmCrudBasic' %}
{%  set _window_id = 'crudBasic' %}
{%  set _url = '/suva/ads2/crudBasic/' ~ ( ad.id > 0 ? ad.id : 0 ) ~ '/' ~ ad.user_id %}
{%  set _function_to_execute_after_load = 'ctlFileUploadAjax' %}

{% if ad.user_id > 0 %}
	{% set _additional_js_onclick_save = 
		"
		ajaxSubmit ( '/suva/users-contacts/index/" ~ ad.user_id ~ "', 'subUsersContacts' );
		ajaxSubmit ( '/suva/ads2/subInsertions/" ~ ad.id ~ "' , 'subInsertions' );
		" %}
{% else %}
	{% set _additional_js_onclick_save = ''%}
{% endif %}

{#% if ad.id > 0 %}
	{% set _adIzBaze = _model.Ad(ad.id) %}
	{% if ad.active !== _adIzBaze.active %}
		{%  set ad.active = _adIzBaze.active %}
	{% endif %}
{% endif %#}



<div class="row">
	<div class="col-lg-12 col-md-12">
		{#{ partial("partials/blkFlashSession") }#}
	</div>
	<div class="col-lg-12 col-md-12">
		<div class="panel-heading">
			<h3 class="panel-title">Ad details
				{% if ad is defined and ad.id is defined %}
					&nbsp;&nbsp;&nbsp;<a href="/ff-oglas-{{ad.id}}" target="_blank"><i class="fa fa-fw fa-eye"></i>View ad</a>

					{% if ad.manual_deactivation == 1 %}
						<div class="pull-right"><b>MANUAL DEACTIVATION</b></div>
					{% endif %}
					<div class="pull-right">[ACTIVE (visible online): <b>
						{% if ad.active == 1 %}
							YES
						{% else %}
							NO
						{% endif %}
					</b> ]</div>
					
					<div class="pull-right">[MODERATION: <b>{{ ad.moderation }}</b> ]</div>
					<div class="pull-right">[ID: <b>{{ ad.id }}</b> ]</div>
					{% if ad.soft_delete == 1%}
						<div class="pull-right"><b style="color:red">THIS AD IS SOFT DELETED AND CANNOT BE MODIFIED!</b></div>
					{% endif %}
				{% endif %}
			</h3>
		</div>
	</div>
</div>

{% if ad.n_source == 'avus' %}
	{{ partial("ads2/blkAvusAdInfo") }}
{% endif %}

{{ form(NULL, 'id': _form_id , 'enctype':"multipart/form-data", 'method': 'post', 'autocomplete': 'off') }}
	{{ hiddenField('next') }}
	{{ hiddenField('_csrftoken') }}
	{% if ad is defined and ad.id is defined %}
		{{ hiddenField(['ad_id', 'value': ad.id]) }}
	{% endif %}
	{{ hiddenField(['user_id', 'value': ad.user_id]) }}
	<input type="hidden" name="_context[selected_user_id]" value = "{{ad.user_id}}" ></input>
	<input type="hidden" name="_context[clicked_action]" id="_clicked_action_subAdBasic" value = "" ></input>

	<input type="hidden" name="n_first_uploaded_photo_id" value="{{ad.n_first_uploaded_photo_id}}"/> 
	<input type="hidden" name="moderation" value="ok"/> 
	<input type="hidden" id="_context_active_tab_subAdBasic" name = "_context[_active_tab]" value="{{ _context['_active_tab'] }}"></input>	
	<input type="hidden" name="category_id" value="{{ad.category_id}}"></input>							

	<div class="row">
		{{ partial("partials/ctlText",[
				'title': 'Category',
				'value': ad.Category().n_path, 
				'field': 'aa', 
				'readonly' : true,
				'width': 12
			] ) }}
	</div>
	{#{ ad.Category(ad.category_id).n_path }#}
	<div class="row">
		{% if ad.id is defined %}
			{{ partial("partials/ctlTextArea",[
				'title': 'Remark',
				'value':ad.n_remark, 
				'field':'n_remark', 
				'width':9,
				'rows':2,
				'cols':40
			] ) }}
		{% endif %}
						
	</div>
	<div class="row">
		{{ ad.User().id}}
		{{ partial("partials/ctlText",[
			'title': 'Phone 1',
			'value': ad.phone1, 
			'field':'phone1', 
			'width':4
		] ) }}
		
		{{ partial("partials/ctlText",[
			'title': 'Phone 2',
			'value': ad.phone2, 
			'field':'phone2', 
			'width':4
		] ) }}
	</div>
	
	<!-- RENDERED_PARAMETERS  -->
	
	<div id="ads_category_parameters">{{ rendered_parameters }}</div>	

	<!-- 360 SPIN ID -->
	<div class="panel panel-default 360_spin">
		<div class="row">
			{{ partial("partials/ctlText",[
				'title': '360 Spin ID',
				'value':ad.n_360_spin_id, 
				'field':'n_360_spin_id', 
				'width':4
			] ) }}
		</div>
	</div>
	{#% set _is_js_debug = true %#}
	{{ partial("partials/btnSaveCancel") }}
{{ endForm() }}


