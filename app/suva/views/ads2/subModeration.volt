{% if _context is defined %}
	
	{% if _context['_errors'] is defined %}
		{% set _errors = _context['errors'] %}
	
	{% endif %}

{% endif %}

{% set _url = '/suva/ads2/subModeration/' ~ ad.id %}

{% set can_be_moderated = true %}<!--TODO treba vidit kako se postavlja ova varijabla-->

<div class="col-lg-12 col-md-12">
	<!-- FORMA -->
	{{ form(NULL, 'id' :  _form_id , 'enctype' : "multipart/form-data", 'method' : 'post', 'autocomplete' : 'off') }}
		{{ hiddenField('next') }}
		{{ hiddenField('_csrftoken') }}
		{% if ad is defined and ad.id is defined %}
			{{ hiddenField(['ad_id', 'value': ad.id]) }}
		{% endif %}
		{{ hiddenField(['user_id', 'value': ad.user_id]) }}

		<input type="hidden" name="_context[selected_user_id]" value = "{{ad.user_id}}" ></input>
		<input type="hidden" name="_context[clicked_action]" id="_clicked_action_subModeration" value = "" ></input>


		<!-- <input type="hidden" name="moderation" value="ok"/>  -->
	{#	<input type="hidden" id="_context_active_tab_subPublicationsAndIssues" name = "_context[_active_tab]" value="{{ _context['_active_tab'] }}"></input>
	#}
		{% if can_be_moderated %}
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="panel-title">
						<strong>Ad moderation</strong>
						<div 
							class="pull-right glyphicon glyphicon-resize-small"
							onclick = "$('#_form_moderation_panel').toggle();">
						</div>
					</div>
					<!-- Publikacije, izdanja, proizvodi i popusti -->
					<div class="panel-body" id = "_form_moderation_panel">
						<div class="row">
							<div class="col-lg-6 col-md-6">
								<div class="form-group">
									<div class="icon_dropdown">
										<label class="control-label" for="moderation">Status</label>
										{% if moderation_dropdown is defined %}
											{{ moderation_dropdown }}
										{% endif %}
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							{% if moderation_reasons is defined %}
								{% if moderation_reasons is iterable %}
									<div id="moderation_reason_box" class="col-lg-12 col-md-12">
										{% for reason, reason_dropdown in moderation_reasons %}
											{% set field = 'moderation_reason_' ~ reason %}
											<div id="{{ field }}_box" class="form-group moderation-reason-group{{ _errors is defined and _errors.filter(field) ? ' has-error' : '' }}">
												<label class="control-label" for="{{ field }}">{{ reason|upper }} Reason</label>
												{{ reason_dropdown }}
												{%- if _errors is defined and _errors.filter(field) -%}
												<p class="help-block">{{- current(_errors.filter(field)).getMessage() -}}</p>
												{%- endif -%}
											</div>
										{% endfor %}
									</div>
								{% endif %}
							{% endif %}
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12">
								<button class="btn btn-primary pull-right" type="submit" name="savemoderation">Save moderation status</button>
								{% if moderation_reasons is defined %}
									{% if moderation_reasons is iterable %}
										<div id="reason_email_box" class="checkbox checkbox-primary pull-right" style="margin-right:20px;">
											<input name="send_reason_email" id="send_reason_email" type="checkbox" checked="checked">
												<label class="control-label" for="send_reason_email"> Send reason email</label>
										</div>
									{% endif %}
								{% endif %}
							</div>
						</div>
						{{ partial ("partials/btnSaveCancel") }}
					</div>
				</div>
			</div>
		{% else %}
			<div class="panel panel-danger">
				<div class="panel-heading">
					<div class="panel-title">
						<span class="fa fa-exclamation-triangle fa-fw"></span> 
						<strong>Warning! Ad cannot be moderated!</strong>
						<div 
							class="pull-right glyphicon glyphicon-resize-small"
							onclick = "$('#_form_moderation_warning_panel').toggle();">
						</div>
					</div>
				</div>
				<div class="panel-body" id = "_form_moderation_warning_panel">
					<div class="text-danger">{{ moderation_warning }}</div>
				</div>
			</div>
		{% endif %}
	{{ endForm() }}	
</div>
	<script>
		if($('#moderation').val() == 'ok'){
			$('#moderation_reason_ok_box').show()
			$('#moderation_reason_nok_box').hide()
		} 
		else if($('#moderation').val() == 'nok'){
			$('#moderation_reason_ok_box').hide()
			$('#moderation_reason_nok_box').show()
		} 
		
		$( "#moderation" ).change(function() {
		  if($(this).val() == 'ok'){
		
			$('#moderation_reason_ok_box').show()
			$('#moderation_reason_nok_box').hide()
		
		  }else if($(this).val() == 'nok'){
			$('#moderation_reason_ok_box').hide()
			$('#moderation_reason_nok_box').show()
		  }
		});
	</script>