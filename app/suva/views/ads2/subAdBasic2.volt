{# AJAX BLOK - NA SVAKU FORMU #}
{% if _form_id is not defined %}{% set _form_id = '1' %}{% endif %}
{% if _is_ajax_submit is not defined %}{% set _is_ajax_submit = false %}{% endif %}
{% set _subform_id = 0 %}


	<!-- GORNJI DIO SA USER DETAILS, CONTACT INFO, INSERTIONS -->
	<div class="row">
		<div class="col-md-12 col-lg-12">
			<div class="panel-heading">
				<h3 class="panel-title">Ad details
					{% if ad is defined and ad.id is defined %}
						<div class="pull-right">
							<a target="_blank" href="{{ ad.get_frontend_view_link() }}" class="btn btn-xs btn-default"><i class="fa fa-fw fa-eye"></i>View ad</a>
						</div>							
						<div class="pull-right">[MODERATION: <b>{{ ad.moderation }}</b> ]</div>
						<div class="pull-right">[ID: <b>{{ ad.id }}</b> ]</div>
						{% if ad.soft_delete == 1%}
							<div class="pull-right"><b style="color:red">THIS AD IS SOFT DELETED AND CANNOT BE MODIFIED!</b></div>
						{% endif %}
					{% endif %}
				</h3>
			</div>
		
		</div>
		
		<!-- USER DETAILS -->
		<div class="col-md-2 col-lg-2">
			<div class="panel panel-default" style="overflow-x:scroll; height:160px; white-space:nowrap;">

				<div class="panel-heading">
					<h3 class="panel-title">
						User {{ linkTo(['suva/korisnici/crud/' ~ ad.user_id, ad.User().username, 'target': '_blank', 'class':'username_redirect ']) }}
						&nbsp;[ID:<b>{{ ad.user_id }}</b>]
					</h3>				
				</div>
				<div class="panel-body">
					<div class="tab-content">
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<span>{{ad.User().company_name}}</span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<span>{{ad.User().first_name}} {{ad.User().last_name}}</span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<span>{{ad.User().address}}</span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<span>{{ad.User().zip_code}} {{ad.User().city}}</span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<span>OIB:{{ad.User().oib}}</span>
							</div>
						</div>							
					</div>
				</div>
			</div>
		</div>
		
		<!-- CONTACT INFO -->
		<div class="col-md-3 col-lg-3">
		
			
			{% set _ids = [] %}
			{% set ucs = ad.User().UsersContacts() %}
			{% for uc in ucs %}
				{% set _ids[uc.id] = uc.id %}
			{% endfor %}
			
			{% set _subform_id += 1 %}
			{{ partial("partials/wraSubFormAjaxV2",[
				'_window_id' : 'subUsersContacts',
				'_url': '/suva/users-contacts/index/' ~ ad.user_id
			] ) }}
			
		
		</div>
		
		<!-- INSERTIONS -->
		<div class="col-md-7 col-lg-7">

			<!-- subInsertions -->
			{% set _subform_id += 1 %}
			{{ partial("partials/wraSubFormAjax",[
				'_form_id' : _form_id,
				'_subform_id' : 'subInsertions',
				'_url': '/suva/ads2/subInsertions/' ~ ad.id, 
				'_model': _model, 
				'_context': _context, 
				'p': ['ad' : ad ]
			] ) }} 

		</div>
	</div>		

	<!-- DONJI DIO S EDITOROM AD-a, PUBLICATIONS-ISSUES, MODERATION, AD INFO -->
	<div class="row">
	
		<!--LIJEVA POLOVINA EKRANA  - EDITOR AD-a-->
		<div class="col-lg-8 col-md-8">
			{{ form(NULL, 'id': 'frm_ads', 'enctype':"multipart/form-data", 'method': 'post', 'autocomplete': 'off') }}
				{{ hiddenField('next') }}
				{{ hiddenField('_csrftoken') }}
				{% if ad is defined and ad.id is defined %}
					{{ hiddenField(['ad_id', 'value': ad.id]) }}
				{% endif %}
				{{ hiddenField(['user_id', 'value': ad.user_id]) }}
				<input type="hidden" name="_context[selected_user_id]" value = "{{ad.user_id}}" ></input>
				<input type="hidden" name="_context[clicked_action]" id="_clicked_action_subAdBasic" value = "" ></input>

				<input type="hidden" name="n_first_uploaded_photo_id" value="{{ad.n_first_uploaded_photo_id}}"/> 
				<input type="hidden" name="moderation" value="ok"/> 
				<input type="hidden" id="_context_active_tab_subAdBasic" name = "_context[_active_tab]" value="{{ _context['_active_tab'] }}"></input>									
		
		
				<div class="row">
				{% if ad.id is defined %}
					{{ partial("partials/ctlTextArea",[
						'title': 'Remark',
						'value':ad.n_remark, 
						'field':'n_remark', 
						'width':9,
						'rows':2,
						'cols':40
					] ) }}
				{% endif %}
				
				<div class="display_ad_title_remark" {% if ad.n_is_display_ad == 0 %} style=" display:none;" {% endif %}>
				{{ partial("partials/ctlText",[
						'title': 'Naslov',
						'value':ad.title, 
						'field':'display_title', 
						'width':9
				] ) }}
					
				</div>
					<input type="hidden" name="category_id" value="{{ad.category_id}}"></input>
					<!--Category: ako user nema kermission onda je readonly ako ima permission onda dropdown-->
					{{ partial("partials/ctlDropdown",[
						'value':ad.category_id, 
						'title':'Category', 
						'list':_model.Categories(['level > 1 and transaction_type_id > 0', 'order':'n_path']), 
						'field':'ad_category_id', 
						'width':9, 
						'option_fields':['n_path'],
						'_context': _context,
						'readonly' : ( ! auth.get_user().isAllowed('frmAdsEdit_fldCategoryId_actChange') ),
						'_context': _context				
					] )}}	

					
					<!-- Is Display Ad -->
					<div class="col-lg-3 col-md-3"> 
						<div class="form-group">			
							<label class="control-label" for="n_is_display_ad">Is display ad</label>			
							<br/>
							<input
								type="checkbox" 
								name="n_is_display_ad_cbx" 
								id="n_is_display_ad" 
								onclick="					
									if($(this).is(':checked')){
										$('#cbx_hidden_n_is_display_ad').val(1);
										$('#block_upload_image').show();
										$('#block_upload_image2').show();
										$('#ads_category_parameters').hide();
										$('.360_spin').hide(); 
										$('.display_ad_title_remark').show();																 
										$('label.product_label:not(:contains(Display))').hide(); 
										$('label.product_label:contains(Display)').show();

									}else{
										$('#cbx_hidden_n_is_display_ad').val(0);
										$('#block_upload_image').hide();
										$('#block_upload_image2').hide();
										$('#ads_category_parameters').show();	
										$('.360_spin').show();
										$('.display_ad_title_remark').hide();
										$('label.product_label:not(:contains(Display))').show();
										$('label.product_label:contains(Display)').hide();

									}
									//alert($('#cbx_hidden_n_is_display_ad').val());
									//sakrit i prikazat  description, uploaad itd
									
								"
								value="0"
								{% if ad.n_is_display_ad == 1 %}checked{% endif %}
								maxlength="2"/>
							<input 
								type="hidden" 
								name="n_is_display_ad" 
								id="cbx_hidden_n_is_display_ad" 
								value="{{ ad.n_is_display_ad }}"/>
						</div>	
					</div>
				</div>
				<div class="row">
					{{ partial("partials/ctlText",[
						'title': 'Phone 1',
						'value':ad.phone1, 
						'field':'phone1', 
						'width':4
					] ) }}
					
					{{ partial("partials/ctlText",[
						'title': 'Phone 2',
						'value':ad.phone2, 
						'field':'phone2', 
						'width':4
					] ) }}
					
				</div>
				
				<!-- DISPLAY AD BLOK - prikazuje se samo ako je ad.n_is_display_ad == 1 -->
				<div style="margin-bottom:3px;  {% if ad.n_is_display_ad == 0 %}  display:none; {% endif %}" class="row" id="block_upload_image">
					<div class="col-lg-12 col-md-12"> 
														
						<input style="float:left; margin-left:15px;" name="imageUpload" id="imageUpload" onchange="loadFile(event)" type="file"></input>
						<script>
						   var loadFile = function(event) {
							 var current_preview = document.getElementById('current_preview');
							 current_preview.style.display = "inline"
							 current_preview.src = URL.createObjectURL(event.target.files[0]);			
							 var remove_display_photo = document.getElementById('remove_display_photo')
							 remove_display_photo.style.display = "inline"
						   }

						   function create_zip() {
							 var zip = new JSZip();
							 zip.add("hello1.txt", "Hello First World\n");
							 zip.add("hello2.txt", "Hello Second World\n");
							 content = zip.generate();
							 location.href="data:application/zip;base64," + content;
						   }		
						</script>

							<!-- <input type="button" value="zip" onclick="create_zip()"></input> -->
						<input 
							class="submit_picture"
							value="submit picture"
							style="float:left;"
							type="button"
							onclick="if($('#uploaded_preview').attr('src') > ''){
										alert('Please remove the picture before uploading another one');
										return;
									 } 
									 if($('#uploaded_preview_append').find('#current_preview').attr('src') > ''){
										alert('Please remove the picture before uploading another one');
										return;
									 }
									 
									if($('#imageUpload').prop('files')[0] > ''){
										var file_data = $('#imageUpload').prop('files')[0];
										var form_data = new FormData();
										form_data.append('file', file_data); 
										form_data.append('ad_id', {{ ad.id }}); 
										$.ajax({
											type:'POST',
											url: '/ajax/pictureUpload',
											data:form_data,
											cache:false,
											contentType: false,
											processData: false,
											success:function(data){
												$('#uploaded_preview').remove();															 			
												$('#uploaded_preview_append').append($('#current_preview').clone());
												console.log($('#current_preview').attr('src'));
												document.getElementById('delete_uploaded_photo').style.display = 'inline';
												alert('image uploaded');
											}						
										});
									}
									 else{
										alert('Odaberite sliku')
									}"
						></input>							
					</div>
				 
					
					<!-- <div class="row" style="margin-left:5px;"> --> 
					<div class="col-md-3 col-lg-2">
						<span>current preview</span>
					</div>
					<div class="col-md-3 col-lg-2">
						<span>uploaded preview</span>
					</div>
					<div class="col-md-3 col-lg-2">
						<span>final preview</span>
					</div>
					<div class="col-md-3 col-lg-6">
						
					</div>
				</div>
				
				<div id="block_upload_image2" class="row" style="margin-left:5px;  {% if ad.n_is_display_ad == 0 %}  display:none; {% endif %}">
					<div class="col-md-3 col-lg-2">										
						<span
							id="remove_display_photo"
							onclick="$('#imageUpload').replaceWith($('#imageUpload').val('').clone(true))
									 $('#current_preview').attr('src','')
									 $(this).css('display', 'none') "
							style="color:red; display:none; cursor:pointer;">X
						</span>
					</div>
					
					<div class="col-md-3 col-lg-2" id="remove_uploaded_photo">
					
						<span
							onclick="
									var form_data = new FormData();
									form_data.append('ad_id', {{ ad.id }}); 
									if(confirm('Are you sure you want to delete this picture from the database?')){
										$.ajax({
											type:'POST',
											url: '/ajax/removeUploadedPhoto',
											data:form_data,
											cache:false,
											contentType: false,
											processData: false,
											success:function(data){
											$('.remove_uploaded_photo').css('display','none')
											$('#uploaded_preview').remove()
											$('#uploaded_preview_append').find('#current_preview').remove();
													
											}						
										})
									}"
							class="remove_uploaded_photo" 
							id="delete_uploaded_photo"
							style="color:red; display:none;">X
						</span>
					
					</div>
					
					<div class="col-md-3 col-lg-2">
						<span style="color:red; display:none;">X</span>
					</div>
					
					<div class="col-md-3 col-lg-6">
						&nbsp;
					</div>
					
<!-- 										</div>
				
				
				<div class="row" style="margin-left:5px; margin-bottom:10px;"> -->
					<div class="col-md-3 col-lg-2">
						<img id="current_preview" style="width:100%;"/>
					</div>	
					<div class="col-md-3 col-lg-2" id="uploaded_preview_append">
						{% if ad.n_picture_path != '' %}
							<script>document.getElementById("delete_uploaded_photo").style.display = "inline";</script>
							<img src="/{{ad.n_picture_path}}" id="uploaded_preview" style="width:100%;"/>
						{% endif %}
					</div>


					
					<div class="col-md-3 col-lg-2 final_image_preview_test">
						{% if ad.n_picture_path != '' %}
						<script>
							$.ajax({
								url:'/repository/dtp/processed/{{ad.id}}.jpg',
								type:'HEAD',
								error: function()
								{
									//file not exists
								},
								success: function()
								{
									$('.final_image_preview_test').append('<img id="final_preview" src="/repository/dtp/processed/{{ad.id}}.jpg" style="width:100%;"/>')
								}
							});
						</script>
							
						{% endif %}													
					</div>	
					
					
					<div class="col-md-3 col-lg-6">
						&nbsp;
					</div>
					
				</div>
				
				<!-- RENDERED_PARAMETERS  -->
				<div id="ads_category_parameters"  {% if ad.n_is_display_ad == 1 %} style=" display:none;" {% endif %}>{{ rendered_parameters }}</div>	
			
				<!-- 360 SPIN ID -->
				<div {% if ad.n_is_display_ad == 1 %} style=" display:none;" {% endif %} class="panel panel-default 360_spin">
					<div class="row">
						{{ partial("partials/ctlText",[
							'title': '360 Spin ID',
							'value':ad.n_360_spin_id, 
							'field':'n_360_spin_id', 
							'width':4
						] ) }}
					</div>
				</div>
				<button class="btn btn-primary" onclick="validate();" accesskey="s" type="submit" name="save">Save</button>
			
			{{ endForm() }}
		</div> <!-- lijeva polovina ekrana -->
		
		{% if ad.id > 0 %}
			<!-- DESNA POLOVINA EKRANA -->
			<div class="col-md-4 col-lg-4">
				<div class = "row">
				
					<!-- FORMA -->
					{{ form(NULL, 'id': 'frm_ads', 'enctype':"multipart/form-data", 'method': 'post', 'autocomplete': 'off') }}
						{{ hiddenField('next') }}
						{{ hiddenField('_csrftoken') }}
						{% if ad is defined and ad.id is defined %}
							{{ hiddenField(['ad_id', 'value': ad.id]) }}
						{% endif %}
						{{ hiddenField(['user_id', 'value': ad.user_id]) }}
						<input type="hidden" name="_context[selected_user_id]" value = "{{ad.user_id}}" ></input>
						<input type="hidden" name="_context[clicked_action]" id="_clicked_action_subPublicationsAndIssues" value = "" ></input>

						<input type="hidden" name="n_first_uploaded_photo_id" value="{{ad.n_first_uploaded_photo_id}}"/> 
						<input type="hidden" name="moderation" value="ok"/> 
						<input type="hidden" id="_context_active_tab_subPublicationsAndIssues" name = "_context[_active_tab]" value="{{ _context['_active_tab'] }}"></input>
			
						<!-- Publications & Issues -->
						
							{{ partial("ads2/subPublicationsAndIssues" ) }}
						
					
						<!-- Moderation  -->
						{% if auth.get_user().isAllowed('frmAdsEdit_secModeration') %}
							{{ partial("ads2/subModeration" ) }}
						{% endif %}

					{{ endForm() }}	
					
					<!-- Informacije o datumima -->
					{% if ad is defined and ad.id is defined %}
						<div class="col-md-12 col-lg-12">
							<table class="table table-condensed table-striped table-bordered">
								<tr{{ ad.latest_payment_state == constant('Baseapp\Models\Ads::PAYMENT_STATE_CANCELED_ORDER') ? ' class="danger"' : '' }}>
									<td>Last payment state</td>
									<td><span class="fa fa-money"></span> {{ Models_Ads__getPaymentStateString(ad.latest_payment_state) }}</td>
								</tr>
								<tr>
									<td>Created</td>
									<td><span class="fa fa-clock-o"></span> {{ date('d.m.Y H:i:s', ad.created_at) }}</td>
								</tr>
								<tr>
									<td>Last modified</td>
									<td><span class="fa fa-clock-o"></span> {{ date('d.m.Y H:i:s', ad.modified_at) }}</td>
								</tr>
								<tr>
									<td>Virtual publish date (sort)</td>
									<td><span class="fa fa-clock-o"></span> {{ date('d.m.Y H:i:s', ad.sort_date) }}</td>
								</tr>
								<tr>
									<td>First published at</td>
									<td><span class="fa fa-clock-o"></span> {{ date('d.m.Y H:i:s', ad.first_published_at) }}</td>
								</tr>
								<tr>
									<td>Last published at</td>
									<td><span class="fa fa-clock-o"></span> {{ date('d.m.Y H:i:s', ad.published_at) }}</td>
								</tr>
								{% set classname = ad.isExpired() ? 'danger' : 'success' %}
								<tr class="{{ classname }}">
									<td>Expire{{ ad.isExpired() ? 'd' : 's' }}</td>
									<td><span class="fa fa-clock-o"></span> {{ date('d.m.Y H:i:s', ad.expires_at) }}</td>
								</tr>
								{% set classname = ad.active ? 'success' : 'danger' %}
								<tr class="{{ classname }}">
									<td><b>Currently active</b></td>
									<td><span class="fa fa-{{ ad.active ? 'check' : 'close' }}"></span> {{ ad.active ? 'YES' : 'NO' }}</td>
								</tr>
							</table>
						</div>
					{% endif %}
				</div>
					
			</div> <!-- desna polovina ekrana -->
		{% endif %}
	</div>


