{#
ulaz:
	user
	_order_status_id


#}


{% set _url = '/suva/ads2/subOrdersWithItems/' ~ user.id ~ '/' ~ _order_status_id  %}
{% set _is_ajax_submit = true %}
{% set _window_id = 'subOrdersWithItems_' ~ _order_status_id %}

{% set _active_tab = (array_key_exists('_active_tab', _context) ? _context['_active_tab'] : '') %}


{#
_url:{{_url}}.._is_ajax_submit:{{_is_ajax_submit}}.._window_id:{{_window_id}}..
#}
 
<!-- ZA AŽURIRANJE BROJA ELEMENATA NA GORNJEM PROZORU -->
<script>
	{% set count_elements = user.Orders('n_status = ' ~ _order_status_id ).count() %}
	{% if count_elements > 0 %}
		$( '#count_items_order_status_{{_order_status_id}}' ).html( '({{ count_elements }}) ');
	{% else %}
		$( '#count_items_order_status_{{_order_status_id}}' ).html( '');
	{% endif %}
</script>


{{ form(NULL, 'id': '_form_' ~ _window_id, 'enctype':"multipart/form-data", 'method': 'post', 'autocomplete': 'off') }}
	{{ hiddenField('next') }}
	{{ hiddenField('_csrftoken') }}
	{% if ad is defined and ad.id is defined %}
		{{ hiddenField(['ad_id', 'value': ad.id]) }}
	{% endif %}
	{{ hiddenField(['user_id', 'value': user.id]) }}
	<input type="hidden" name="_context[selected_user_id]" value = "{{user.id}}" ></input>
	<input type="hidden" name="_context[clicked_action]" id="_clicked_action_{{_window_id}}" value = "" ></input>
	<input type="hidden" id="_context_active_tab_{{_window_id}}" name = "_context[_active_tab]" value="{{ _active_tab }}"></input>

	<!-- DODANO KAO PARAMETAR AKCIJE -->
	<input type="hidden" name="_context[clicked_action_params][user_id]" value="{{ user.id }}"/> 
	<input type="hidden" name="_context[clicked_action_params][logged_user_id]" value="{{ auth.get_user().id }}"/> 

	<div role="tabpanel" class="tab-pane" id="{{ _window_id }}">
		<div class="row">
			<div class="col-lg-12 col-md-12" >
				<div class="panel panel-default" >
				
					<!-- NOVA VERZIJA SA STAVKAMA -->
					<div class="panel-heading" style="overflow:visible;">
						
						
						<span 
							class = "btn btn-primary" 
							style = "text-align:right; height:25px; margin-bottom:5px;" 							
							onclick = "
								if (zadnjiKliknutiOrder > 0)
									window.open('/suva/reports/orderPdf/' + zadnjiKliknutiOrder);
								else
									alert('Select Order first!');"
							>
							Preview PDF 
						</span> 

						
						{#
						<span 
							class = "btn btn-primary" 
							style = "text-align:right; height:25px; margin-bottom:5px;" 							
							onclick = "
							alert('Zadnji kliknuti ' + zadnjiKliknutiOrder);
							var users_item_selected = $( '.order_with_item_select option:selected' ).attr('id')
										window.open('http://ogladev2.oglasnik.hr/suva/reports/orderPdf/'+users_item_selected)"
							>
							Preview PDF 
						</span> 
						#}
						{#
						<button 
							class = "btn btn-primary" 
							style = "text-align:right; height:25px; margin-bottom:5px;" 							
							type = "submit" 
							name = "save"
							onclick = "$('#_clicked_action_{{_window_id}}').val('copy_orders_items_to_cart');"
							>
							Copy Items To Cart
						</button>
						#}
					 	<!-- <span
							class = "btn btn-primary"
							style = "text-align:right; height:25px; margin-bottom:5px;"
							onclick = "	var users_offer_selected = $( '#_form_subOrdersWithItems_3 option:selected' ).attr('id');
										window.open('http://slaviceva40.zapto.org/suva/reports/orderPdf/'+users_offer_selected,'_blank');
							"
							>
							Preview PDF
						</span>	  -->
						{% for action_id in _model.getAllowedActionIds( auth.get_user().id, _order_status_id, 'orders-items' ) %}
								{% set action = _model.Action( action_id ) %}
								
								
								{{ action.buttonHtml_submitViaActionInputOnForm( [
									'action_input_id': '_clicked_action_' ~ _window_id
									,'is_ajax' : _is_ajax_submit
									,'action_url' : _url
									,'action_window_id' : _window_id
									,'require_confirmation': action.require_confirmation
								] ) }}
						
						{% endfor  %}
						{% for action_id in _model.getAllowedActionIds( auth.get_user().id, _order_status_id, 'orders' ) %}
								{% set action = _model.Action( action_id ) %}
								{{ action.buttonHtml_submitViaActionInputOnForm( [
									'action_input_id': '_clicked_action_' ~ _window_id
									,'is_ajax' : _is_ajax_submit
									,'action_url' : _url
									,'action_window_id' : _window_id
									,'require_confirmation': action.require_confirmation
								] ) }}
						{% endfor  %}
					</div>
				</div>

				<!-- INVOICE - NASLOVNI RED -->
				<div class="panel panel-default" style = "overflow-x:scroll; height:400px;" >
					<div class="panel-heading">
						<p class="panel-title" style="font-family:Courier; font-size:12px; padding-left:3px;" >
							{{ str_replace("+","&nbsp;","INVOICE+DATE++++++++++++TOTAL++TOT.W.PDV+ITEM+++FROM++++++++++++++++TO++++++++++++++++++PUBL+PRODUCT+++++LINE+TOTAL+AD++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")}}
						</p> 
					</div>
					<div class="panel-body">
						<!-- <select style="display:block !important; height:360px;" name="_orders_items_ids[]" multiple> -->
						<select class="order_with_item_select" style="display:block !important; height:360px; width:100%;" name="_context[clicked_action_params][orders_and_items][]" multiple>
							{% for order in user.Orders(['n_status = ' ~ _order_status_id, 'order':'id desc'])  %}
								<option  
									id = "{{order.id}}"
									onmouseover="this.style.backgroundColor='#ffa6a6'"
									onmouseout="this.style.backgroundColor='lightblue';"
									style="font-family:Courier; font-size:12px; background-color:lightblue;"
									value="order_{{order.id}}"
									order = "{{order.id}}"
									onclick = "zadnjiKliknutiOrder = {{order.id}};"
									{# onclick= " window.open('/suva/fakture/crud/{{order.id}}','_blank');  " #}
									{# ondblclick= " window.open('/suva/fakture/crud/{{order.id}}','_blank');" #}
									ondblclick= " window.open('/suva/fakture/crud/{{order.id}}?_context[_close_window_on_exit]=__TRUE__','_blank');"
									>
									{{ _model.txtPad( order.id , 7 ) }}
									{{ _model.txtPad( order.n_payment_date , 10 , 'date' ) }}
									{{ _model.txtPad( order.n_total , 10, 'currency' ) }}
									{{ _model.txtPad( order.n_total_with_tax , 10, 'currency' ) }}
								</option>
								{% for item in order.OrdersItems() %}
								
									{% set _product = item.Product() %}
									{% if _product %}
										{% set _publication_slug = _product.Publication().slug %}
										{% set _product_is_online = _product.is_online_product %}
										{% set _product_name = _product.name %}
									{% else %}
										{% set _publication_slug = 'ERR PRODUCT NOT DEFINED' %}
										{% set _product_is_online = 0 %}
										{% set _product_name = 'ERR NOT DEFINED' %}
									{% endif %}
										
									{% set _ad = item.Ad() %}
									{%  set _ad_title = _ad.title ~ ' - ' ~ _ad.description %}
										
									{% set _now = strtotime(date('Y-m-d', time())) %}{# bez sati i minuta, da bi sve danas bilo ne-crveno #}
									{% set _item_n_first_published_at = strtotime(item.n_first_published_at) %}
										
										
										<option 
										
											id = "{{item.id}}"
											onmouseover = "this.style.backgroundColor='#ffa6a6'"
											onmouseout = "this.style.backgroundColor=''"
											style = "
												font-family:Courier; font-size:12px;
												{% if _item_n_first_published_at  <  _now %} color:red; {% endif %}
												{% if _item_n_first_published_at  <  _now and _order_status_id == 4 or _order_status_id == 5 %} color:black ; {% endif %}
											"
											value = "item_{{item.id}}"
											order = "{{order.id}}"
											onclick = "zadnjiKliknutiOrder = {{order.id}};"
											ondblclick = " window.open('/suva/orders-items/edit/{{item.id}}','_blank');  " 
											>
											
											{{ _model.txtPad ('' , 40 ) }}
											{{ _model.txtPad( item.id , 6 ) }}

											{% if _product_is_online == 1 %}
												{{ _model.txtPad( item.n_first_published_at , 19, 'datetime' ) }}
											{% else %}
												{% set n_first_published_at = explode(" ", item.n_first_published_at) %}
												{{ _model.txtPad( n_first_published_at[0] , 10, 'date' ) }}{{ _model.txtPad( '' , 9 ) }}                                           
											{% endif %}

											{% if _product_is_online == 1 %}
												{{ _model.txtPad( item.n_expires_at , 19, 'datetime' ) }}
											{% else %}
												{% set n_expires_at = explode(" ", item.n_expires_at) %}
												{{ _model.txtPad( n_expires_at[0] , 10, 'date') }}{{ _model.txtPad( '' , 9 ) }}
											{% endif %}	

											{% if _product %}
												{{ _model.txtPad( _publication_slug , 4 ) }}
											{% endif %}
											{{ _model.txtPad( _product_name , 10 ) }}
											{{ _model.txtPad( item.n_total , 11, 'currency' ) }}
											{{ _model.txtPad( item.ad_id , 7 ) }}
											{{ _model.txtPad( _ad_title , 100) }}
										</option>
									{#% endif %#}
								{% endfor %}
							{% endfor %}
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>
{{ endForm() }}