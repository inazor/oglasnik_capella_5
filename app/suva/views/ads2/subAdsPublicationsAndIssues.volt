
	<div class="col-md-12 col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-title">
					<strong>Publications & Issues</strong>
					<div 
						class="pull-right glyphicon glyphicon-resize-small"
						onclick = "$('#_form_publications_panel2').toggle();"
					></div>
				</div>
			</div>
			<!-- Publikacije, izdanja, proizvodi i popusti -->
			<div class="panel-body" id = "_form_publications_panel2">
				<div class="tab-content">
					<br/>
					{% if _is_ajax_submit %}
						<span 
							class="btn btn-primary btn-xs pull-right" 
							onclick = "
								$('#_clicked_action').val('add_insertions_for_ad');
								$('#_clicked_action_{{_window_id}}').val('add_insertions_for_ad');
								ajaxSubmit( '{{ _url }}' , '{{_window_id}}' , '_form_{{_window_id}}' );
								
								"
							>
							Add <b>I</b>nsertions
						</span>
					{% else %}
						<button 
							class="btn btn-primary btn-xs pull-right" 
							accesskey="i"
							type="submit" 
							name="save"
							onclick="$('#_clicked_action').val('add_insertions_for_ad');
									 $('#_clicked_action_subAdsPublicationsAndIssues').val('add_insertions_for_ad');
							"
							>
							Add <b>I</b>nsertions
						</button>
					{% endif %}
					<div class="row">
						<!-- Publikacije -->
						{% set publications = _model.Publications() %}
						{% for publication in publications %}
								<div class="col-md-12 col-lg-12" >
									<div  id = "pub_{{publication.id}}_issues_list2" style="overflow-y:hidden; padding-left:13px; width:100%; min-height:0px; max-height:80px; overflow-x: hidden; {% if publication.is_online == 1 %} background-color:#B9D8A8; {% else %} background-color:#A0D2D0; {% endif %}">
									{#<p style="color:#018be3; "  id = "pub_{{publication.id}}_issues_list2">#}
										{% if publication.is_online == 0 %}
										<div 
											id = "issues_list_toggle"
											class="pull-right fa fa-arrows-v"
											onclick = "var clicks = $(this).data('clicks');
													  if (clicks) {
														$('#pub_{{publication.id}}_issues_list2').css({'min-height':0,'max-height':300,'height':''})
													  } else {
														$('#pub_{{publication.id}}_issues_list2').css({'min-height':'','max-height':'','height':'100%'})
													  }
													  $(this).data('clicks', !clicks);"

										></div>
										{% endif %}

										<label style="vertical-align:top;">
											{{ publication.slug }}
										</label>
										
										<!-- Izdanja na pojedinoj publikaciji -->
										{% if publication.is_online == 0 %}
											{#% for issue in publication.Issues(["status='prima'", 'limit':8]) %#}
											{% for issue in publication.Issues(["status='prima'"]) %}		
											<label style="font-weight:normal; margin-left:5px; text-align:center;">
												<input type="checkbox" 
												name="_checked_publications2[{{publication.id}}][]" 
												id="{{ issue.id }}" 
												value="{{ issue.id }}"
												></input>
												<br/>
												{{ date("d.m", strtotime(issue.date_published))}}
											</label>
											{% endfor %}
										{% else %}
										<label style="vertical-align:top;">
											<span style="vertical-align:top;">{{ partial("partials/ctlDate",['value': date('d.m.Y', time() ) ,   'field':'_checked_dates2[' ~ publication.id ~ ']', 'width': 7] ) }}</span>
											<p style="vertical-align:top;" class="to_date2"></p>
										</label>
										
										
										{% endif %}
										
									{#</p>#}
									</div>


									<div class="panel panel-default">
										<!-- selektor proizvoda, da se skrati lista -->
										<input 
											size="10"
											placeholder="Type to search..."
											id="pub_{{publication.id}}_selector2" 
											></input> 
											<div 
												id = "product_list_toggle2"
												class="pull-right fa fa-arrows-v"
												onclick = "var clicks = $(this).data('clicks');
														  if (clicks) {
															$('.product_box2_{{publication.id}}').css({'min-height':0,'max-height':100,'height':''})
														  } else {
															$('.product_box2_{{publication.id}}').css({'min-height':'','max-height':'','height':'100%'})
														  }
														  $(this).data('clicks', !clicks);"

											></div>

										<div 
											class="product_box2_{{publication.id}}" 
											style="
												overflow:scroll 
												padding-left:13px; 
												width:100%; 
												min-height:0px; 
												max-height:100px; 
												overflow-x: hidden;
												font-family:Courier; font-size:14px;
												">
											{% for product in publication.Products(['order':'sort_order, name']) %}

											<label 
												{% if product.is_online_product == 1 %}
													dana = {{product.days_published}}
												{% endif %} 
												class="product_label2" 
												id="prod2_{{publication.id}}_{{product.id}}" 
												vrij="{{product.name}} {{product.unit_price}}" 
												style="font-weight:normal; width:100%;"
											>
												<input					
													days_published = '{{product.days_published}}'
													type="radio" 
													{% if product.is_online_product == 1 %}
														dana = {{product.days_published}}
													{% endif %}
													class="product_radio2"
													checked1 = ""
													name="_checked_products2[{{publication.id}}]" 
													value="{{product.id}}"
													onclick = "
														var next_objava_list = parseInt($(this).attr('days_published'));
														var if_checked_product = $(this).attr('checked1')
														if(next_objava_list >= 0){
														var objava = 0;	
														$( '#pub_{{publication.id}}_issues_list2 label input' ).each(function( index, element ) {
														objava ++;
														
														if(if_checked_product == 22 || if_checked_product == ''){
																	
																	element.checked = true;
																	if(objava > (next_objava_list) ){
																		element.checked = false;
																		return;
																	}
																	
																}else{
																	element.checked = false;
																}
																
															  })

														   }
														var this_input=$(this);
														if(this_input.attr('checked1')=='11')
														{
															this_input.attr('checked1','11')
														}
														else
														{
															this_input.attr('checked1','22')
														}
														$('.radio-button').prop('checked', false);
														if(this_input.attr('checked1')=='11')
														{
															this_input.prop('checked', false);
															this_input.attr('checked1','22')
														   
														}
														else{
															this_input.prop('checked', true);
															this_input.attr('checked1','11')
														}
														  "
												>
												</input>
												{{ str_replace("_","&nbsp",str_pad(substr(product.name, 0,20),20,"_")) }}
												{{ str_replace("_","&nbsp",str_pad(substr(product.getPrice(ad.category_id), 0,7),7,"_")) }}
												kn
												{% if product.days_published > 0 %}
													{{ str_replace("_","&nbsp",str_pad(substr(product.days_published , 0,3),3,"_")) }}
												{% else %}
													{{ str_replace("_","&nbsp",str_pad(substr(' ', 0,3),3,"_")) }}
												{% endif %}
												
												{% if product.is_online_product == 1 and product.days_published > 0 %}
													{{ str_replace("_","&nbsp",str_pad(substr('dana', 0,7),7,"_")) }}
												{% elseif product.is_online_product == 0 and product.days_published > 0 %}
													{{ str_replace("_","&nbsp",str_pad(substr('objava', 0,7),7,"_")) }}
												{% else %}
													{{ str_replace("_","&nbsp",str_pad(substr(' ', 0,7),7,"_")) }}
												{% endif %}
												
												{% if product.is_display_ad > 0 %}
													{{ str_replace("_","&nbsp",str_pad(substr('Display AD', 0,11),11,"_")) }}
												{% elseif product.is_picture_required > 0 %}
													{{ str_replace("_","&nbsp",str_pad(substr('Picture', 0,11),11,"_")) }}
												{% else %}
													{{ str_replace("_","&nbsp",str_pad(substr(' ', 0,11),11,"_")) }}
												{% endif %}

											</label>

											
									
											{% endfor %}
											
										</div>
										<script>
										$.extend($.expr[":"], {
											"containsIN": function(elem, i, match, array) {
												return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
											}										
										});
											
										$.extend($.expr[":"], {
											"doesentContain": function(elem, i, match, array) {
												return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) <= 0;
											}										
										});

											
										
											
										$('#pub_{{publication.id}}_selector2').keyup(function(){									
											$('.product_box2_{{publication.id}} label').css("background-color",'');
											$('.product_box2_{{publication.id}} label').show()
											$('.product_box2_{{publication.id}}').css({'min-height':0,'max-height':100,'height':''})
											
											var value= $(this).val();				
											$('.product_box2_{{publication.id}} label:containsIN("'+value+'")').css("background-color",'yellow'); 
											$('.product_box2_{{publication.id}} label:doesentContain("'+value+'")').hide()
											
											$('.product_box2_{{publication.id}}').css({'min-height':'','max-height':'','height':'100%'})
											if(value == ''){
												$('.product_box2_{{publication.id}} label:doesentContain("'+value+'")').show()
												$('.product_box2_{{publication.id}} label').css("background-color",'');
												$('.product_box2_{{publication.id}}').css({'min-height':0,'max-height':100,'height':''})
												
											}
										 });
											
									</script>
									</div>
									
									
									<!-- Popusti na publikaciji -->
									<div class="panel panel-default">
										<div class='pull-left popusti_tab2'><span><b>Popusti</b></span></div>
										<div 
											id = "discount_list_toggle2"
											class="pull-right fa fa-arrows-v"
											onclick = "var clicks = $(this).data('clicks');
													  if (clicks) {
														$('.discount_box2_{{product.id}}').css({'min-height':0,'max-height':100,'height':''})
													  } else {
														$('.discount_box2_{{product.id}}').css({'min-height':'','max-height':'','height':'100%'})
													  }
													  $(this).data('clicks', !clicks);"

										></div>
										<div class="popusti2 discount_box2_{{product.id}}" style="overflow:scroll; padding-left:13px; width:100%; min-height:0px; max-height:100px; overflow-x: hidden">
											{% for discount in publication.Discounts() %}
											
													<label class="popust_checkbox_value2" style="font-weight:normal;">
														<input type="checkbox" name="_checked_discounts2[{{publication.id}}][]" value="{{discount.id}}"></input>
															{{discount.name}}
													</label>
													<br/>
												
											{% endfor %}
										</div>
									</div>
								</div>
						{% endfor %}
						<script>
							
							
							
							$( ".product_label2" ).click(function() {
							if($(this).attr('dana')){
							  var date_val = $('input[name="_checked_dates2[1]"]').val();

								var res = date_val.split(".");
								res0 = res[0];
								res1 = res[1];
								res2 = res[2];
								tt = res1+'.'+res0+'.'+res2
								
								var date = new Date(tt);

								var newdate = new Date(date);

								newdate.setDate(newdate.getDate() +parseInt($(this).attr('dana')));

								var dd = newdate.getDate();
								var mm = newdate.getMonth() + 1;
								var y = newdate.getFullYear();

								var someFormattedDate = dd + '.' + mm + '.' + y;
							
								$('.to_date2').text('do '+someFormattedDate);
								
							}
							
							});

							$(".product_radio2" ).click(function() {
							if($(this).attr('dana')){
								 if($(this).attr('checked1') == 22){
									$('.to_date2').hide();
								}
								 if($(this).attr('checked1') == 11){
									$('.to_date2').show();
								}
							}
							});

							$("input[name='_checked_dates2[1]']").change(function() {
								var clicked_product = $("input[checked1='11']").attr('dana')
								
								if(clicked_product > 0){
							
									var res = $(this).val().split(".");
									res0 = res[0];
									res1 = res[1];
									res2 = res[2];
									tt = res1+'.'+res0+'.'+res2

									var date = new Date(tt);

									var newdate = new Date(date);

									newdate.setDate(newdate.getDate() +parseInt(clicked_product));

									var dd = newdate.getDate();
									var mm = newdate.getMonth() + 1;
									var y = newdate.getFullYear();

									var someFormattedDate = dd + '.' + mm + '.' + y;

									$('.to_date2').text('do '+someFormattedDate);
										
								}
												
							
							});
							
						</script>
						<script>$('label.product_label2:contains(Display)').hide()</script>
					</div>
				</div>
			</div>
			<!-- <script>$('#_form_publications_panel2').hide();</script> -->
		</div>
	</div>
