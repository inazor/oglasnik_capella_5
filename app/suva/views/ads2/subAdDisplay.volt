{# AJAX BLOK - NA SVAKU FORMU #}
{% if _form_id is not defined %}{% set _form_id = '1' %}{% endif %}
{% if _is_ajax_submit is not defined %}{% set _is_ajax_submit = false %}{% endif %}
{% set _subform_id = 0 %}


	<!-- GORNJI DIO SA USER DETAILS, CONTACT INFO, INSERTIONS -->
	<div class="row">
		<div class="col-md-12 col-lg-12">
			<div class="panel-heading">
				<h3 class="panel-title">Ad details
					{% if ad is defined and ad.id is defined %}
						<div class="pull-right">
							<a target="_blank" href="{{ ad.get_frontend_view_link() }}" class="btn btn-xs btn-default"><i class="fa fa-fw fa-eye"></i>View ad</a>
						</div>							
						<div class="pull-right">[MODERATION: <b>{{ ad.moderation }}</b> ]</div>
						<div class="pull-right">[ID: <b>{{ ad.id }}</b> ]</div>
						{% if ad.soft_delete == 1%}
							<div class="pull-right"><b style="color:red">THIS AD IS SOFT DELETED AND CANNOT BE MODIFIED!</b></div>
						{% endif %}
					{% endif %}
				</h3>
			</div>
		
		</div>
		
		<!-- USER DETAILS -->
		<div class="col-md-2 col-lg-2">
			<div class="panel panel-default" style="overflow-x:scroll; height:160px; white-space:nowrap;">

				<div class="panel-heading">
					<h3 class="panel-title">
						User {{ linkTo(['suva/korisnici/crud/' ~ ad.user_id, ad.User().username, 'target': '_blank', 'class':'username_redirect ']) }}
						&nbsp;[ID:<b>{{ ad.user_id }}</b>]
					</h3>				
				</div>
				<div class="panel-body">
					<div class="tab-content">
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<span>{{ad.User().company_name}}</span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<span>{{ad.User().first_name}} {{ad.User().last_name}}</span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<span>{{ad.User().address}}</span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<span>{{ad.User().zip_code}} {{ad.User().city}}</span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<span>OIB:{{ad.User().oib}}</span>
							</div>
						</div>							
					</div>
				</div>
			</div>
		</div>
		
		<!-- CONTACT INFO -->
		<div class="col-md-3 col-lg-3">
		
			
			{#% set _ids = [] %}
			{% set ucs = ad.User().UsersContacts() %}
			{% for uc in ucs %}
				{% set _ids[uc.id] = uc.id %}
			{% endfor %}
			
			{% set _subform_id += 1 %#}
			
			{{ partial("partials/wraSubFormAjaxV2",[
				'_window_id' : 'subUsersContactsDisplay',
				'_url': '/suva/users-contacts/index/' ~ ad.user_id
			] ) }}
			
		
		</div>
		
		<!-- INSERTIONS -->
		<div class="col-md-7 col-lg-7">

			<!-- subInsertions -->
			{% set _subform_id += 1 %}
			{{ partial("partials/wraSubFormAjaxV2",[
				'_window_id' : 'subInsertionsDisplay',
				'_url': '/suva/ads2/subInsertions/' ~ ad.id
			] ) }}
		</div>
	</div>		

	<!-- DONJI DIO S EDITOROM AD-a, PUBLICATIONS-ISSUES, MODERATION, AD INFO -->
	<div class="row">
	
		<!--LIJEVA POLOVINA EKRANA  - EDITOR AD-a-->
		<div class="col-lg-8 col-md-8">
			
			{{ partial("partials/wraSubFormAjaxV2",[
				'_window_id' : 'crudDisplay'
				,'_url': '/suva/ads2/crudDisplay/' ~ ( ad.id > 0 ? ad.id : 0 ) ~ '/' ~ ad.user_id
			] ) }}

		</div> <!-- lijeva polovina ekrana -->

		{% if ad.id > 0 %}
			<!-- DESNA POLOVINA EKRANA -->
			
			<div class="col-md-4 col-lg-4">
				
					{% set _is_js_debug = true %}
					{{ partial("partials/wraSubFormAjaxV2",[
						'_window_id' : 'subPublicationsAndIssuesAjaxDisplay'
						,'_url': '/suva/ads2/subPublicationsAndIssuesAjax/' ~ ( ad.id > 0 ? ad.id : 0 )
					] ) }}				
					{% set _is_js_debug = false %}
				
					
			</div> <!-- desna polovina ekrana -->
		{% endif %}
	</div>


