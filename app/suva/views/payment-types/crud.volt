{% if errors is not defined %}
	{% set errors = null %}
{% endif %}

{% if _is_ajax_submit is not defined %}
	{% set _is_ajax_submit = '' %}
{% endif %}
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div><h1 class="page-header">{% if entity.id is not defined %}New {% endif %}Payment Types</h1></div>
		{{ partial("partials/blkFlashSession") }}
		{% if entity is defined %}
			{{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
				{{ hiddenField('_csrftoken') }}
				{{ hiddenField('next') }}
				<input type="hidden" name="id" value="{{entity.id}}"></input>
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								{% if entity.id is defined %}
									<div class="pull-right">[ID: <b>{{ entity.id }}</b> ]</div>
								{% endif %}
								<h3 class="panel-title">Payment Types</h3>
							</div>
							<div class="panel-body">
								<div class="row">
									{{ partial("partials/ctlText",['value':entity.name, 'title':'Name', 'width':3, 'field':'name', 'errors': errors] ) }}
									{{ partial("partials/ctlText",['value':entity.navision_payment_type, 'title':'Navision Payment Type', 'width':3, 'field':'navision_payment_type', 'errors': errors] ) }}
									{{ partial("partials/ctlCheckbox",['value':entity.is_active, 'title':'is_active', 'width':3, 'field':'is_active', 'errors': errors] ) }}
								</div>
								
							</div>
						</div>
					</div>
				</div>
				{% include "partials/btnSaveCancel.volt" %}
			{{ endForm() }}
		{% else %}
			ENTITY NIJE DEFINIRAN
		{% endif %}
	</div>
</div>