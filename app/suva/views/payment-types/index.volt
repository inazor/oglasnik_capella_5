<div class="row">
	
	<div class="col-lg-12 col-md-12">
		<div>
			
			<h1 class="page-header">Payment Types</h1>
		</div>
		<br/>
		{{ partial("partials/btnAddNew", ['controller':'payment-types'] ) }}
			{{ partial("partials/blkFlashSession") }}
			<table class="table table-striped table-hover table-condensed">
			{{ partial("partials/blkTableSort",['fields':[ 
					['title':'Options'																,'width':150	]
					,['title':'ID'								,'name':'id'						,'width':100		,'search_ctl':'ctlNumeric']
					,['title':'Name'							,'name':'name'						,'width':300		,'search_ctl':'ctlText'	  ] 
					,['title':'Navision Payment Type'			,'name':'navision_payment_type'		,'width':250		,'search_ctl':'ctlText'	  ] 		
					,['title':'Is active'						,'name':'is_active'					,'width':100		,'search_ctl':'ctlNumeric'	]					
			] ]) }}

		{% for id in ids %}
			{% set entity = _model.getOne('PaymentTypes',id) %}
            <tr>
				<td class="text-right">{{ partial("partials/btnEditDelete", ['value':entity.id , 'controller':'payment-types'] ) }}</td>
                <td>{{ entity.id }}</td>
                <td>{{ entity.name }}</td>
				<td>{{ entity.navision_payment_type }}</td>
				<td>{{ partial("partials/ctlCheckbox",['value':entity.is_active, 'readonly':1 ] ) }}</td>
            </tr>
            {% else %}
            <tr><td colspan="6">Currently, there are no payment types here!</td></tr>
            {% endfor %}
        </table>

    </div>
</div>