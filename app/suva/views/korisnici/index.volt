
{% set _form_url = '/suva/korisnici' %}
{% if _form_id is not defined %}{% set _form_id = '1' %}{% endif %}
{% set _subform_id = 0 %}
{% if errors is not defined and _context is defined %}{% if _context['errors'] is defined %}{% set errors = _context['errors'] %}{% endif %}{% endif %}

{% if ids is not defined and p is defined %}{% if p['ids'] is defined %}{% set ids = p['ids'] %}{% endif %}{% endif %}



	{#{ print_r(_context,true)}#}


{% set is_ajax = false %} 
{% if _context is defined %}{% if array_key_exists('_is_ajax_submit',_context) %}{% if _context['_is_ajax_submit'] === true %}{% set is_ajax = true %}{% endif %}{% endif %}{% endif %}


<div class="row">
	<div class="col-lg-12 col-md-12">
		<div style="margin-bottom: 50px;" >
			
			<h1 class="page-header">Users</h1>
			{{ partial("partials/btnAddNew", ['controller':'korisnici', 'params': "_context[_default_n_source]=backend" ]  ) }}
		</div>
		<br/>
			{{ partial("partials/blkFlashSession") }}
			<table style="width:2000px !important;" class="table table-striped table-hover table-condensed">
			{{ partial("partials/blkTableSort",[
			'_context' : _context,
			'fields':[ 
				 ['title':	 _m.t('Options')									,'width':250	,'show_navigation' : true ]
				,['title':	 _m.t('ID'	)	,'name':'id'					,'width':300	,'search_ctl':'ctlNumeric'	]
				,['title':	 _m.t('Type')	,'name':'type'					,'width':150	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list': [['id':1,'name':'Person'],['id':2,'name':'Company']], 'option_fields':['name'] ]] 
				,['title': _m.t('Username')	,'name':'username'				,'width':250	,'search_ctl':'ctlText'	] 
				,['title': _m.t('First Name')	,'name':'first_name'			,'width':250	,'search_ctl':'ctlText'	] 
				,['title': _m.t('Last Name')	,'name':'last_name'				,'width':250	,'search_ctl':'ctlText'	] 
				,['title': _m.t('Company Name'),'name':'company_name'			,'width':500	,'search_ctl':'ctlText'	] 
				,['title': _m.t('E-mail')	,'name':'email'					,'width':100	,'search_ctl':'ctlText'	] 
				,['title':	 _m.t('Phone 1')	,'name':'phone1'				,'width':250	,'search_ctl':'ctlText'	] 
				,['title':	 _m.t('Phone 2')	,'name':'phone2'				,'width':250	,'search_ctl':'ctlText'	] 
				,['title':	 _m.t('OIB'	)	,'name':'oib'					,'width':300	,'search_ctl':'ctlText'	]
				,['title':	 _m.t('VAT No.')	,'name':'n_vat_number'			,'width':100	,'search_ctl':'ctlText'	] 				
				,['title':	 _m.t('Source')	,'name':'n_source'				,'width':250	,'search_ctl':'ctlText'	] 
				,['title':	 _m.t('AVUS No.'),'name':'n_avus_customer_number','width':150	,'search_ctl':'ctlText'	] 
				,['title':	 _m.t('AVUS Id.'),'name':'n_avus_customer_id','width':150	,'search_ctl':'ctlText'	] 
				,['title':	 _m.t('Roles')	,'name':'__UsersRoles__role_id'	,'width':250	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list': _model.Roles() , 'option_fields':['name'] ]]  
				
			] ]) }}
			
			{% for id in ids %}
				{% set entity = _model.getOne('Users', id) %}
				
				{% set _context['current_entity_id'] = entity.id %}
				{% set _context['ajax_edit'] = true %}
				
				{#{ print_r(_context,true)}#}

				<tr>
					<td>
						{{ linkTo(['suva/korisnici/crud/' ~ entity.id , 'Edit', 'class':'btn btn-info btn-xs', 'style':''] )}}
						{{ linkTo(['suva/korisnici/choose/' ~ entity.id ~ '/' , 'Choose', 'class':'btn btn-info btn-xs', 'style':'background-color:green;'] )}}
						
						&nbsp;						
					</td> 
					<td>{{ entity.id }}</td>
					<td>
						{% if entity.type == 1 %}<span class="fa fa-user fa-fw"></span>{% endif %}
						{% if entity.type == 2 %}<span class="fa fa-globe fa-fw"></span>{% endif %}
						{% if entity.remark is defined and strlen(trim(entity.remark)) > 0 %}
                    		<span class="fa fa-warning text-danger fa-fw" data-toggle="tooltip" data-type="html" data-placement="right" title="{{ entity.remark }}"></span>
                    	{% endif %}
					</td>
					<td>{{ entity.username }}</td>
					<td>{{ entity.first_name }}</td>
					<td>{{ entity.last_name }}</td>
					<td>{{ entity.company_name }}</td>
					<td>{{ entity.email }}</td>
					<td>{{ entity.phone1 }}</td>
					<td>{{ entity.phone2 }}</td>
					<td>{{ entity.oib }}</td>
					<td>{{ entity.n_vat_number }}</td>
					<td>{{ entity.n_source }}</td>
					<td>{{ entity.n_avus_customer_number }}</td>
					<td>{{ entity.n_avus_customer_id }}</td>
					<td>
						{{ partial ( "partials/ctlMany2Many",[
							'field' : '__Roles_'
							, 'list': _model.Roles()
							, 'value' : entity.Roles()
							, 'show_entire_list' : false
							, 'readonly' : true
						] ) }}	
					</td>
				</tr>
			{% else %}
				<tr><td colspan="7">{{ _m.t('Currently, there are no users here!') }}</td></tr>
			{% endfor %}
		</table>
	</div>
</div>


