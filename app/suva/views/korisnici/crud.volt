{# Admin User Edit View #}

{% set _user_crud = true %}
{#{ var_dump(errors)}#}
<div class="row">

    <div class="col-lg-12 col-md-12">
	
      {{ partial("partials/blkFlashSession") }}
	 
		{% if entity is defined %}
			{% if entity.id > 0 %}
			  {{ form( NULL, 
					'id' : 'frm_choose_user', 
					'action' : '/suva/korisnici/choose',
					'method' : 'post', 
					'autocomplete' : 'off',
					'style' : 'float:left;'
				) }}
					{{ hiddenField('_csrftoken') }}
					{{ hiddenField('next') }}
					<input type="hidden" name="_context[selected_user_id]" value = "{{entity.id}}"/>
					<button  class= "btn btn-info btn-xl  text-right" type="submit">{{ _m.t('Choose this user') }}</button>
				{{endForm()}}

			{% endif %}
			<button class="pull-right">{{ linkTo('suva/korisnici/impersonate/'~ entity.id, '<span text-right class="fa fa-users">' ~ _m.t('Impersonate') ~ '</span>')  }}</button>
			
			
	  
			{{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
			{{ hiddenField('_csrftoken') }}
			{{ hiddenField('next') }}			

			{#<input type="hidden" name="action" value="{{form_action}}"></input>#}
			<input type="hidden" name="id" value="{{entity.id}}"></input>

				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								{% if entity.id is defined %}
									<div class="pull-right">[{{ _m.t('ID:') }} <b>{{ entity.id ~ (entity.import_id|default(false) ? '</b> /' ~ _m.t('ImportID:') ~ '<b>' ~ entity.import_id : '') }}</b> ]</div>
								{% endif %}
								
								<h3 class="panel-title">
									{{ _m.t('User details for') }} <b>{{entity.username}}</b>
								</h3>
								
							</div> 
							<div class="panel-body">
								<ul id="UserDetails" class="nav nav-tabs" role="tablist">
									<li role="presentation" class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">User details</a> </li>
									<li role="presentation"><a href="#roles" aria-controls="roles" role="tab" data-toggle="tab">User Roles <small>(check all that should apply)</small></a></li>
									<li role="presentation"><a href="#additional" aria-controls="additional" role="tab" data-toggle="tab">Additional Info</a></li>
								</ul>
								  <!-- Tab panes -->
								<div class="tab-content">
									<!-- User Details -->
									<div role="tabpanel" class="tab-pane active" id="details">

											<div class="row">
												<div class="col-lg-8 col-md-8">
													<div class="row">
														{{ partial("partials/ctlText",['value':entity.username, 'title':_m.t('Username'), 'width':4, 'field':'username', '_context': _context] ) }}
														{{ partial("partials/ctlText",['value':'__NOT_WRITTEN__', 'title':_m.t('Password'), 'width':4, 'field':'password', 'password':1, '_context': _context] ) }}
														{{ partial("partials/ctlText",['value':entity.email, 'title':_m.t('E-mail'), 'width':4, 'field':'email', '_context': _context] ) }}
													
														<div class="col-lg-4">
															<div class="form-group">
																<label class="control-label">{{ _m.t('User type') }}</label>
																<div>
																	<label class="radio-inline"><input onclick="userType($(this).val())" type="radio"{% if entity.type == constant("Baseapp\Models\Users::TYPE_PERSONAL") %} checked="checked"{% endif %} value="{{ constant("Baseapp\Models\Users::TYPE_PERSONAL") }}" id="userType1" name="type"> Person</label>
																	<label class="radio-inline"><input onclick="userType($(this).val())" type="radio"{% if entity.type == constant("Baseapp\Models\Users::TYPE_COMPANY") %} checked="checked"{% endif %} value="{{ constant("Baseapp\Models\Users::TYPE_COMPANY") }}" id="userType2" name="type"> Company</label>
																</div>
															</div>
														</div>
														{% if entity.type == constant("Baseapp\Models\Users::TYPE_PERSONAL") %} 													
														<script>
														$( document ).ready(function() {
															$('label[for=company_name], input#company_name').hide();
															
														});
														
														</script>
														{% endif %}
														<script>
														function userType(value) {
														
														if (value == 1) {
															$('label[for=company_name], input#company_name').hide();
															$('label[for=first_name], input#first_name').text('First Name');
															$('label[for=last_name], input#last_name').text('Last Name'); }
														if (value == 2) {
															$('label[for=company_name], input#company_name').show();
															$('label[for=first_name], input#first_name').text('First Name( Responsible Person)');
															$('label[for=last_name], input#last_name').text('Last Name ( Responsible Person)'); }
														}
														
														</script>
														
														{{ partial("partials/ctlText",[
															'value':entity.n_source
															,'title':_m.t('Source')
															,'width':3
															,'field':'n_source'
															,'readonly' : true
															, '_context': _context
														] ) }}
														
													</div> 
													<div class="row">
													
													
														{{ partial("partials/ctlText",[
														'value':entity.first_name
														, 'title':_m.t('First name'), 'width':3
														, 'field':'first_name'
														, '_context': _context] )
														}}
														
														{{ partial("partials/ctlText",[
														'value':entity.last_name
														, 'title':_m.t('Last name')
														, 'width':4
														, 'field':'last_name'
														, '_context': _context] )
														}}
														
														{#{ partial("partials/ctlTextType",[
															'title':_m.t('Last Name')
															,'field':'last_name'
															,'value':entity.last_name
															,'value_show':entity.last_name
															,'search_field':'last_name'
															, 'return_field':"concat_LEFT_BRACKET_first_name_COMMA__SNG_QUOT__SPACE__SNG_QUOT__COMMA_last_name_COMMA__SNG_QUOT__SPACE__SNG_QUOT__COMMA_phone1_RIGHT_BRACKET_"
															, 'table_name':'users'
															, 'readonly': false
															, 'additional_where' : ' '
															, 'js_beforecreate_value' : 'e.append("<a class=\x22 btn btn-info btn-xs \x22 href =\x22/suva/korisnici/choose/' + obj.id + '\x22 >Choose</a>" );'
															, 'do_not_fill_clicked_option_value' : true
															, 'keep_typed_text' : true
															, 'list_item_button_string' : 'Choose'
															, 'list_item_button_href' : '/suva/korisnici/choose/_LIST_ITEM_ID_'
															, 'width':5] 
														) }#}

														
														
														
														{{ partial("partials/ctlText",[
														'value':entity.company_name
														, 'title':'Company name'
														, 'width':4
														, 'field':'company_name'
														, '_context': _context] )
														}} 
														
													</div>
													<div class="row">
														{#{ partial("partials/ctlTextType",[
															'title':_m.t('Phone 1')
															,'field':'phone1'
															,'value':entity.phone1
															,'value_show':entity.phone1
															,'search_field':'phone1'
															, 'return_field':"concat_LEFT_BRACKET_first_name_COMMA__SNG_QUOT__SPACE__SNG_QUOT__COMMA_last_name_COMMA__SNG_QUOT__SPACE__SNG_QUOT__COMMA_phone1_RIGHT_BRACKET_"
															, 'table_name':'users'
															, 'readonly': false
															, 'additional_where' : ' '
															, 'do_not_fill_clicked_option_value' : true
															, 'keep_typed_text' : true
															, 'list_item_button_string' : 'Choose'
															, 'list_item_button_href' : '/suva/korisnici/choose/_LIST_ITEM_ID_'
															, 'width':3] 
														) }#}
														{{ partial("partials/ctlText",['value':entity.phone1, 'title':'Phone 1', 'width':3, 'field':'phone1', '_context': _context] ) }}
														{{ partial("partials/ctlCheckbox",['value':entity.phone1_public, 'title':'Public', 'width':1, 'field':'phone1_public', '_context': _context] ) }}
														<div class= "col-lg-2 col-md-2"></div>
														{{ partial("partials/ctlText",['value':entity.phone2, 'title':'Phone 2', 'width':3, 'field':'phone2', '_context': _context] ) }}
														{{ partial("partials/ctlCheckbox",['value':entity.phone2_public, 'title':'Public', 'width':1, 'field':'phone2_public', '_context': _context] ) }}
													</div>
												</div>
												<!-- Contact Info -->
												<div class="col-lg-4 col-md-4">
													<div class="panel panel-default" style="overflow:scroll; height:160px; white-space:nowrap;">
														<div class="panel-heading" style="display:block; white-space: nowrap; width:100%;">
															<h3 class="panel-title">{{ _m.t('Contact Info') }}</b>
															<span class="fa fa-fw fa-plus"
																	onclick = "$('#new_contact_div').show();"
																	style = "display:none; float:right !important;"/>
															</h3>
														</div>
														<div class="panel-body">
															<div class="tab-content">
																<div class="row">
																	<div class="col-md-12 col-lg-12">
																		<input type="hidden" name="_contact_action" id="_contact_action" />
																		<input type="hidden" name="_contact_id" id="_contact_id" />  
																		<input type="hidden" name="_contact_user_id" id="_contact_user_id" value="{{entity.id}}"/>
																		<input type="hidden" name="_contact_type" id="_contact_type" />
																		<input type="hidden" name="_contact_value" id="_contact_value" />
																		<div id ="new_contact_div" style="display:none;">
																			<label style="font-weight:normal;"><input type="radio" name="_contact_type" value="phone" checked>{{ _m.t('Phone') }}</input></label>
																			<label style="font-weight:normal;"><input type="radio" name="_contact_type" value="email">{{ _m.t('Email') }}</input></label>

																			<input 
																				name="_new_contact_value" 
																				id="new_contact_value"
																				style="float:left; width:100%; "
																				onchange="$('#_contact_action').val('contact_create');
																							$('#_contact_value').val($('#new_contact_value').val());
																							$('#_contact_type').val($('#new_contact_type').val());
																							$('#_contact_user_id').val({{ entity.id }});">
																			</input>
																		</div>

																		{% for contact in entity.UsersContacts("is_hidden = 0") %}
																			<div style="display:block; width:100% !important; font-size:12px;">
																				{{contact.name}}
																			</div>
																		{% endfor %}
																	</div>	
																</div>
															</div>
														</div>
													</div>
												</div>
												
											</div>

										 
											<div class="row">
												<!-- TODO REMOVE THIS -->
												{#{ partial("partials/ctlTextType",[
													'title': _m.t('OIB')
													,'field':'oib'
													,'value':entity.oib
													,'value_show':entity.oib
													,'search_field':'oib'
													, 'return_field':"concat_LEFT_BRACKET_first_name_COMMA__SNG_QUOT__SPACE__SNG_QUOT__COMMA_last_name_COMMA__SNG_QUOT__SPACE__SNG_QUOT__COMMA_phone1_RIGHT_BRACKET_"
													, 'table_name':'users'
													, 'readonly': ( entity.id > 0 and entity.oib > '' ? true : false )
													, 'additional_where' : ' '
													, 'do_not_fill_clicked_option_value' : true
													, 'keep_typed_text' : true
													, 'list_item_button_string' : 'Choose'
													, 'list_item_button_href' : '/suva/korisnici/choose/_LIST_ITEM_ID_'
													, 'width':5] 
												) }#}
												{#{ partial("partials/ctlTextTypeV3",[
													'title' :  _m.t("OIB")
													, 'field' : 'oib'
													, 'value' : entity.oib
													, 'width' : 2
													, 'readonly' : false
                                                    , 'keepTypedText' : true
													, '_context' : _context
													, 'value_show' : entity.oib
													, 'model_name' : 'Users' 
													, 'expr_search' : 'oib'
													, 'expr_value' : 'id' 
													, 'expr_show_input' : 'oib'
													, 'expr_show_list' : 'concat_LEFT_BRACKET_first_name_COMMA__SNG_QUOT__SPACE__SNG_QUOT__COMMA_last_name_COMMA__SNG_QUOT__SPACE__SNG_QUOT__COMMA_oib_RIGHT_BRACKET_'
													, 'additional_where' : ' '
												]) }#}													
												
												{#'readonly': ( entity.id > 0 and entity.oib > '' ? 1 : 0 ),	#}
												{{ partial("partials/ctlText",[
													'value':entity.oib,
													'title': _m.t('OIB.'), 
													'width':2, 
													'field':'oib',
													
													'_context': _context,
													'_maxlength': ( _user_crud == true ? 12 : 999 )
												] ) }} 
												{#, 'readonly': ( entity.id > 0 and entity.n_vat_number > '' ? 1 : 0 ) #}
												{{ partial("partials/ctlText",[
												'value':entity.n_vat_number
												, 'title': _m.t('VAT No.'), 'width':2
												, 'field':'n_vat_number'
												
												, '_context': _context] ) }} 
												{#{ partial("partials/ctlCheckbox",['value':entity.n_r1, 'title': _m.t('R1 User'), 'width':1, 'field':'n_r1', '_context': _context] ) }#}	
												{{ partial("partials/ctlCheckbox",['value':entity.n_pays_vat, 'title': _m.t('Pays VAT'), 'width':1, 'field':'n_pays_vat', '_context': _context] ) }}	
											</div>
											<div class="row">
												{{ partial("partials/ctlDate",['value':entity.birth_date, 'title': _m.t('Birth date'), 'width':2, 'field':'birth_date', '_context': _context] ) }} 
												<div class="col-lg-1 col-md-1">&nbsp;</div>
												
												{{ partial("partials/ctlCheckbox",['value':entity.active, 'title': _m.t('Active'), 'width':1, 'field':'active', '_context': _context] ) }}
												{{ partial("partials/ctlCheckbox",['value':entity.newsletter, 'title': _m.t('Newsletter'), 'width':1, 'field':'newsletter', '_context': _context] ) }}

											</div>
											<div class="row">
												{#{ partial("partials/ctlDropdown",[
													'value':entity.country_id, 
													'title': _m.t('Country'), 
													'list':_model.Countries(), 
													'field':'country_id', 
													'width':2, 
													'option_fields':['id','name'],
													'_context': _context				
												] )}#}	
												
												{# defaultno Hrvatska #}
												{% if not (entity.country_id > 0) %}{% set entity.country_id = 1 %}{% endif %}
												
												{{ partial("partials/ctlTextTypeV3",[
													'title' :  _m.t("Country")
													, 'field' : 'country_id'
													, 'value' : entity.country_id
													, 'width' : 2
													, '_context' : _context
													, 'value_show' :  _model.Location( entity.country_id ).name
													, 'model_name' : 'Locations' 
													, 'expr_search' : 'name'
													, 'expr_value' : 'id' 
													, 'expr_show_input' : 'name'
													, 'expr_show_list' : 'name'
													, 'additional_where' : ' and level = 1 order by name '
												]) }}
												
												{{ partial("partials/ctlTextTypeV3",[
													'title' :  _m.t("County")
													, 'field' : 'county_id'
													, 'value' : entity.county_id
													, 'width' : 3
													, '_context' : _context
													, 'value_show' :  _model.Location( entity.county_id ).name
													, 'model_name' : 'Locations' 
													, 'expr_search' : 'name'
													, 'expr_value' : 'id' 
													, 'expr_show_input' : 'name'
													, 'expr_show_list' : 'name'
													, 'additional_where' : ' and parent_id = __P1__ and level = 2 order by name '
													, 'input_P1' : 'country_id'
											
												]) }}
												 
												<!-- TODO REMOVE THIS -->
												{#{ partial("partials/ctlDropdown",[
													'value':entity.county_id, 
													'title':_m.t('County' ), 
													'list':_model.Counties("parent_id IS NOT NULL"), 
													'field':'county_id', 
													'width':3, 
													'option_fields':['id','name'],
													'_context': _context
																								
												] )}#}	
												
												
												{{ partial("partials/ctlTextTypeV3",[
													'title' : _m.t("City" )
													, 'field' : 'city'
													, 'value' : entity.city
													, 'width' : 2
													, '_context' : _context
													, 'value_show' :  entity.city
													, 'model_name' : 'Locations' 
													, 'expr_search' : 'name'
													, 'expr_value' : 'name' 
													, 'expr_show_input' : 'name'
													, 'expr_show_list' : 'name'
													, 'additional_where' : ' and parent_id = __P1__ and level = 3  order by name '
													, 'input_P1' : 'county_id'
													, 'keep_typed_text' : true
												]) }}
												
										
												{{ partial("partials/ctlText",['value':entity.zip_code, 'title':'ZIP / Post', 'width':1, 'field':'zip_code', '_context': _context] ) }}
												{#{ partial("partials/ctlText",['value':entity.city, 'title':'City', 'width':2, 'field':'city', '_context': _context] ) }#}
												{{ partial("partials/ctlText",['value':entity.address, 'title':'Address', 'width':3, 'field':'address', '_context': _context] ) }}
											</div>
											<hr></hr>
											

										
									</div> 
									
									<!-- User Roles -->
									<div role="tabpanel" class="tab-pane" id="roles">
									{#
										<div class="row">
											<div class="col-md-12 col-lg-12">
												
												
												
												{% for ru in entity.RolesUsers() %} 
													{% set rus[ru.role_id] = ru.role_id %}
												{% endfor %}
												
												{% for role in _model.Roles() %}
													<br/>
													{% if _model.isUserAllowed('rolRead_' ~ role.name, _user.id) %}
														<div class="col-md-6 col-lg-6">
															<label>
																<input 
																	type="checkbox" 
																	{% if !_model.isUserAllowed('rolUpdate_' ~ role.name, _user.id) %}
																		readonly
																	{% endif %}
																	name="_context[selected_roles_users][]"
																	{% if rus[role.id] is defined %} checked="checked"{% endif %} 
																	value="{{ role.id }}"/> 
																		{{ role.name }} 
																		<small class="help-block">({{ role.description }})</small>
															</label>
														</div>
													{% endif %}
												{% endfor %}
												
											</div>
										</div>
										#}
										<div class="row">
											{{ partial ( "partials/ctlMany2Many",[
												'title' :  _m.t('Roles')
												, 'width' : 6
												, 'field' : '_context[selected_roles_users]'
												, 'list': _model.Roles()
												, 'value' : entity.Roles()
												, 'option_fields' : ['id', 'name', 'description']
												, 'show_entire_list' : true
												, 'readonly' : false
											] ) }}	
										</div>
										
									</div> 

										<!--PO�ETAK ADDITIONAL INFO-->
								  <div role="tabpanel" class="tab-pane" id="additional">
									  
									  <div class="row">
												{{ partial("partials/ctlTextArea",['value':entity.remark, 	'title': _m.t('Remark(internal)'), 'width':4, 'rows':4, 'cols':10, 'field':'remark', '_context': _context] ) }}
												{{ partial("partials/ctlCheckbox",['value':entity.suspended, 'title': _m.t('Suspended'), 'width':1, 'field':'suspended', '_context': _context] ) }}
												{{ partial("partials/ctlCheckbox",['value':entity.banned,	 'title': _m.t('Banned'), 'width':1, 'field':'banned', '_context': _context] ) }}
												{{ partial("partials/ctlTextArea",['value':entity.ban_reason, 'title': _m.t('Ban Reason(internal)'), 'width':3, 'rows':4, 'cols':10, 'field':'ban_reason', '_context': _context] ) }}
											</div>
											<hr></hr>
												<div class="row">
													<!-- TODO REMOVE THIS -->
													{#{ partial("partials/ctlTextType",[
													'field':'n_agency_id'
													,'value_show':entity.Agency().company_name
													,'search_field':'company_name'
													, 'return_field':'company_name'
													, 'table_name':'users'
													, 'title': _m.t('Agency')
													, 'additional_where' : ' and type = 2 order by company_name'
													, 'width':3] 
													) }#}
													
													{{ partial("partials/ctlTextTypeV3",[
													'title' :  _m.t("Agency")
													, 'field' : 'n_agency_id'
													, 'value' :entity.Agency().company_name
													, 'width' : 3
													, 'readonly' : false
													, '_context' : _context
													, 'value_show' : entity.Agency().company_name
													, 'model_name' : 'Users' 
													, 'expr_search' : 'username'
													, 'expr_value' : 'id' 
													, 'expr_show_input' : 'username'
													, 'expr_show_list' : 'username'
													, 'additional_where' : ' and type = 2 order by company_name'
													]) }}													
													
													
													
													{{ partial("partials/ctlNumeric",['value':entity.n_agency_commission, 'title':'Commision', 'width':2, 'field':'n_agency_commission', 'decimals':2, '_context': _context] ) }}	
													
													{{ partial("partials/ctlDropdown",[
													'value':entity.n_payment_type_id, 
													'title': _m.t('Payment Type'), 
													'list':_model.PaymentTypes(), 
													'field':'n_payment_type_id', 
													'width':3, 
													'readonly' : (not _user.isAllowed('frmUsersCrud_fldPaymentType_actUpdate')),
													'option_fields':['id','name'],
													'_context': _context				
													] )}}	
												</div>
											<hr></hr>
											<div class="row">
												{{ partial("partials/ctlCheckbox",['value':entity.n_is_salesman, 'title':'Is User Salesman', 'width':2, 'field':'n_is_salesman', '_context': _context] ) }}	
												{{ partial("partials/ctlText",['value':entity.n_oznaka_operatera, 'title':'Oznaka operatera', 'width':3, 'field':'n_oznaka_operatera', '_context': _context] ) }} 
												{{ partial("partials/ctlText",['value':entity.n_nav_department_id, 'title':'Department', 'width':2, 'field':'n_nav_department_id', '_context': _context] ) }}
												{{ partial("partials/ctlDropdown",[
													'value':entity.n_fiscal_location_id, 
													'title': _m.t('Current Fiscal location'), 
													'list':_model.FiscalLocations(), 
													'field':'n_fiscal_location_id', 
													'width':3, 
													'option_fields':['id','name'],
													'_context': _context				
												] )}}	
												
											</div>
											<div class="row">
												{{ partial("partials/ctlText",['value':entity.n_source, 'title':'Source', 'width':1, 'field':'n_source', '_context': _context] ) }} 
												{{ partial("partials/ctlText",['value':entity.n_avus_customer_number, 'title':'Avus Customer No.', 'width':2, 'field':'n_avus_customer_number', '_context': _context] ) }} 
												{{ partial("partials/ctlText",['value':entity.n_avus_customer_id, 'title':'Avus Customer ID', 'width':2, 'field':'n_avus_customer_id', '_context': _context] ) }} 
												
												<!-- TODO REMOVE THIS -->
												{#{ partial("partials/ctlTextType",[
													'title': _m.t('Sales Rep.'), 
													'field' : 'n_sales_rep_id'
													,'value' : entity.n_sales_rep_id
													,'value_show' : entity.n_sales_rep_id ~ ' - ' ~ _model.User(entity.n_sales_rep_id).username
													,'search_field' : 'username'
													, 'return_field':'username'
													, 'table_name':'users'
													, 'additional_where' : ' order by username '
													, 'width':2
												] ) }#}

												{% if entity.n_sales_rep_id > 0 %}
													{% set sales_rep = _model.User(entity.n_sales_rep_id).username %}
												{% else %}
													{% set sales_rep = '' %}
												{% endif %}
												{{ partial("partials/ctlTextTypeV3",[
													'title' :  _m.t("Sales Rep")
													, 'field' : 'n_sales_rep_id'
													, 'value' : entity.n_sales_rep_id
													, 'width' : 3
													, 'readonly' : false
													, '_context' : _context
													, 'value_show' : entity.n_sales_rep_id ~ ' - ' ~ sales_rep
													, 'model_name' : 'Users' 
													, 'expr_search' : 'username'
													, 'expr_value' : 'id' 
													, 'expr_show_input' : 'username'
													, 'expr_show_list' : 'username'
													, 'additional_where' : ' order by username '
												]) }}												
											</div>
									  
								  </div>
							</div>
							</div>
						</div>
						<div class="col-lg-12 col-md-12">
							{{ partial ("partials/btnSaveCancel") }}				
						</div>
					</div>
				</div>

			{{ endForm() }}			
		{% else %}
		
			{{ _m.t('ERROR: ENTITY IS NOT DEFINED') }}
		{% endif %}
	</div>

</div>


				
	




				