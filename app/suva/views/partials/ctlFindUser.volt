{#
	ctlFindUser parametri:  
	field - 'ime_polja'  - ako je postavljen dodaje klasu inputa (postaje form control)
	width - ako je postavljen dodaje div class="col-mg-width-..width".....
	title - ako je naveden dodaje Label
	value - polje čije vrijednost se upisuje u bazu
	readonly - ako je postavljen (1) dodaje READONLY na input
	placeholder - defaultni tekst (( Unesite vrijednost.. ))

	primjer; ctlFindUser(['value':entity.n_sales_rep_id, 'title':'Sales Rep', 'width':3, 'field':'n_sales_rep_id',   'errors':errors ] ) 
#}

{{ javascript_include("assets/suva/js/n_user_search.js") }}

{% if _context is defined %}
		{#{% if _context['ajax_edit'] is defined and _context['current_entity_id'] is defined and _context['current_entity_table_name'] is defined and field is defined %}
			{% if _context['ajax_edit'] === true %}
				{% set is_ajax = true %}
				{% set ajax_table = _context['current_entity_table_name'] %}
				{% set ajax_id = _context['current_entity_id'] %}
			{% endif %}
		{% endif %}#}
		
		{% if _context['_errors'] is defined %}
			{% set _errors = _context['errors'] %}
			
		{% endif %}
		
	{% endif %}

{% set user = value %}
<div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
	<label class="control-label" for="{{ field }}_input">Sales rep</label>

		{# Showing the user selector only for new ads #}
		{%- if (not(user is empty)) -%}
				<input type="hidden" name="{{ field }}" id="{{ field }}" value="{{ user.id }}" data-username="{{ user.username }}" />
		{%- else -%}
				<input type="hidden" name="{{ field }}" id="{{ field }}" value="" data-username="" />
		{%- endif -%}
		<input type="text" class="form-control" id="{{ field }}_input" value="" onmouseover="getUserName('{{field}}');" />
	{%- if _errors is defined and _errors.filter(field) -%}
		<p class="help-block">{{- current(_errors.filter(field)).getMessage() -}}</p>
	{%- endif -%}
</div>