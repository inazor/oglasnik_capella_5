{#
Upotreba:
	{% include "partials/btnSaveCancel.volt" %}

#}

{% set _ajax = false %}
{% if _is_ajax_submit is not defined %}
		{% set _is_ajax_submit = '' %}
	{% endif %}
{% if _is_ajax_submit === true %}{% set _ajax = true %}{% endif %}


<div>
		{% if _ajax %}
			{% if _caller_url is defined %}
				<div 
					class = "btn btn-primary"
					onclick = " ajaxSubmit ( '{{_url}}', '{{ _window_id }}', '{{ _form_id }}', '{{_url}}' );
								ajaxSubmit ( '{{ _caller_url}}', '{{ _window_id }}' );
							"
				>
					Save and Exit
				</div>
				<div 
					class = "btn btn-warning"
					onclick = " ajaxSubmit ( '{{ _caller_url}}', '{{ _window_id }}' );"
				>
					Exit
				</div>
				
			{% else %}
				<div 
					class = "btn btn-primary"
					onclick = " ajaxSubmit ( '{{_url}}', '{{ _window_id }}', '{{ _form_id }}' );"
				>
					Save..
				</div>
			{% endif %}
			 
		{% else %}
			<button class="btn btn-primary" type="submit" accesskey="s" name="save">Save</button>
			<button class="btn btn-primary" type="submit" name="saveexit">Save & Exit</button>
			<!-- <button class="btn btn-primary" type="submit" name="_action_save_new">Save & New</button> -->

			<button class="btn btn-warning" type="submit" name="cancel">Exit</button>
		{% endif %}
	

</div>