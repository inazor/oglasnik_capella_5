{#
Upotreba:
	{% include "partials/btnSaveCancel.volt" %}

#}

{% set _ajax = false %}{% if _context['_is_ajax_submit'] === true %}{% set _ajax = true %}{% endif %}



{% if _ajax %}
	{% if _context['_form_id'] is not defined %}BtnSaveExit: _context[_form_id] NOT DEFINED<br/> {% endif %} 
	{% if _context['_form_url'] is not defined %}BtnSaveExit: _context[_form_url] NOT DEFINED<br/> {% endif %} 
	
	{% set form_id_full = "_form_" ~ _context['_form_id'] %}
	{% set form_container_id_full = "_form_container_" ~ _context['_form_id'] %}
{% endif %}

<div>
		{% if _ajax %}
			<div 
				class = "btn btn-primary"
				onclick = "
					//alert('clicked');
					var data_to_send = $('#{{ form_id_full }}').serialize();
					//alert('data to send:' + data_to_send);
 					$.ajax({
						url     : '{{ _context['_form_url'] }}',
						type    : 'POST',
						data    : data_to_send,
						success	: function( data, status, jqXHR ){
									//alert('success');
									//alert(data);
									$('#{{ form_container_id_full }}').html(data);
									$( '#{{ form_container_id_full }}' ).css('border', '3px solid green');
										setTimeout(function(){
											$( '#{{ form_container_id_full }}' ).css('border', '1px solid black');
											}, 1000);
									
									
								},
						error	: function( jqXHR, textStatus, errorThrown ){
									
									alert('ERROR, textStatus: ' + textStatus + 'errorThrown:' + errorThrown );
									//alert('jqXHR:' + jqXHR  + ' typeof:' + typeof(jqXHR));
								}
					});     
					
				"
			>
				Save..
			</div>
		{% else %}
			<button class="btn btn-primary" type="submit" accesskey="s" name="save">Save</button>
			<button class="btn btn-warning" type="submit" name="cancel">Exit</button>
		{% endif %}
		

</div>