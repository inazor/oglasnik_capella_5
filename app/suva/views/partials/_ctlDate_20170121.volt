{#
	parametri:  
	field - 'ime_polja'  - ako je postavljen dodaje klasu inputa (postaje form control)
	value - vrijednost polja
	width - ako je postavljen dodaje div class="col-mg-width-..width".....
	title - ako je naveden dodaje Label
	show_time - ako je postavljen prikazuje i vrijeme
	readonly - ako je postavljen (1) dodaje READONLY na input
	placeholder - defaultni tekst (( Unesite vrijednost.. ))
	
	ajax_edit - ako je definiran onda se izmjene odmah šalju ajaxom. Potrebne su još varijable ajax_id, ajax_table
	ajax_id - vidi ajax_edit
	ajax_table - vidi ajax_edit
	

	primjer; ctlDate(['value':entity.name, 'title':'Name', 'width':3, 'field':'name', 'readonly':1 ] ) 
			 ctlDate( ['value':'2015-12-31' , 'title':'Datum', 'width':3, 'field':'date'] )
 			{{ partial("partials/ctlDate",[
				'title':'Datum', 
				'value':'2015-12-31' , 
				'field':'date',
				'width':3, 
				'readonly' : false,
				'show_time' : true,
				'_context' : _context
			] ) }}
#}

	
	{% set _show_time = false %}{% if show_time is defined %}{% if show_time === true %}{% set _show_time = true %}{% endif %}{% endif %}
	{% set _readonly = false %}{% if readonly is defined %}{% if readonly === true %}{% set _readonly = true %}{% endif %}{% endif %}
 

	{% set is_ajax = false %}
	
	{% if errors is defined %}
		{% set _errors = errors %}
	{% endif %}
	
	{% if value is not defined %}
		{% set value = '' %}
	
	{% endif %}
	
	{% if field is  not defined %}
		{% set field = '' %}
	{% endif %}

	{% if _context is defined %}
		{% if _context['ajax_edit'] is defined and _context['current_entity_id'] is defined and _context['current_entity_table_name'] is defined and field is defined %}
			{% if _context['ajax_edit'] === true %}
				{% set is_ajax = true %}
				{% set ajax_table = _context['current_entity_table_name'] %}
				{% set ajax_id = _context['current_entity_id'] %}
			{% endif %}
		{% endif %}
		
		{% if _context['_errors'] is defined %}
			{% set _errors = _context['errors'] %}
		{% endif %}
		
	{% endif %}

	
	{% if width is defined %}<div class="col-lg-{{ width }} col-md-{{ width }}">{% endif %} 
		{% if  field  is defined %}<div class="form-group{{ _errors is defined and _errors.filter( field ) ? ' has-error' : '' }}">{% endif %}
			{% if title is defined %}<label {% if field is defined %} class="control-label" for="{{ field }}" {% endif %} >{{ title }}</label>{% endif %}
			<input 
				{% if placeholder is defined %} placeholder = "{{ placeholder}}"{% endif %}
				{% if _readonly %} readonly {% endif %}
				
				class = "
					{% if field is defined %} form-control {% endif %}
					{% if is_ajax %} ajax_edit {% endif %}
				"
				
				{% if field is defined %} class="form-control" name="{{ field }}" {% endif %} 
				{% if _show_time %}
					value="{{ (value ? date('d.m.Y H:i:s', strtotime(value)) : '') }}" 
					maxlength="19"
					
					{#% if ! _readonly %}
						onmouseover="$(this).datetimepicker({
									 format: 'dd.mm.yyyy h:i',
									 weekStart: 1,
									 language: 'hr',
									 autoclose: true});" 
					{% endif %#}
				{% else %}
					value="{{ (value ? date('d.m.Y', strtotime(value)) : '') }}" 
					maxlength="10"
					
					{% if ! _readonly %}
						onmouseover="$(this).datepicker({
									 format: 'dd.mm.yyyy',
									 weekStart: 1,
									 language: 'hr',
									 autoclose: true});" 
					{% endif %}
				{% endif %}
				
				
				{% if is_ajax %}
					{% set id_string = 'id__' ~ ajax_id ~ '__field__' ~ field %}
					id = "{{ id_string }}"
					onchange = " ctlDateUpdateField( '{{ id_string }}', '{{ ajax_table }}', '{{ ajax_id }}', '{{ field }}' );"
					{% else %}
						id="{{  field  }}"
					{% endif %}

				
				/>
		{% if field is defined %}</div>{% endif %}
	{% if width is defined %}</div>{% endif %}