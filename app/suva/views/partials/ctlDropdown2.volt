{#
	ctlDropdown parametri:
		field - 'ime_polja'  - ako je postavljen dodaje klasu inputa (postaje form control)
		value - vrijednost polja
		width - ako je postavljen dodaje div class="col-mg-width-..width".....
		label - ako je naveden prikazuje se tekst labela, a ako nije prikazuje se defaultni labela
		no_label - ako je 1 ne prikazuje se label
		list - array koji sadrži listu vrijednosti
		option_fields - ako je postavljeno, od ovih se polja slaže tekst u dropdownu (npr: ['id', 'path' ). Ako nije ništa navedeno default je 'name'
		readonly - ako je 1 nema odabira nego samo prikaz ako nije definiran ili nije 1 onda ga se ignorira
		no_ids - ako je postavljena, vraćaju se vrijednosti sa liste
		placeholder - ako je definiran, dodaje se početno nultno stanje koje ne vraća vrijednost

		primjer: ctlDropdown( ['value':entity.category_id, 'title':'Category', 'list':categories, 'field':'category_id', 'width':4, 'option_fields':['path','id'] ])

		kada se šalje lista bez id-jeva: partial("partials/ctlDropdown",['value':entity.status, 'title':'Status', 'list':statuses, 'width':3, 'field':'status', 'no_ids':1, 'errors': errors] )
#}

	{% if p is defined %}
		{% if p['field'] is defined %}{% set field = p['field'] %}{% endif %}
		{% if p['value'] is defined %}{% set value = p['value'] %}{% endif %}
		{% if p['width'] is defined %}{% set width = p['width'] %}{% endif %}
		{% if p['title'] is defined %}{% set title = p['title'] %}{% endif %}
		{% if p['list'] is defined %}{% set list = p['list'] %}{% endif %}
		{% if p['option_fields'] is defined %}{% set option_fields = p['option_fields'] %}{% endif %}
		{% if p['readonly'] is defined %}{% set readonly = p['readonly'] %}{% endif %}
		{% if p['no_ids'] is defined %}{% set no_ids = p['no_ids'] %}{% endif %}
		{% if p['placeholder'] is defined %}{% set placeholder = p['placeholder'] %}{% endif %}
	{% endif %}
	
	{% if not(is_array(list) ) %} {% set list = list.toArray() %}{% endif %}

	{% if  readonly is defined %}
		{% if readonly == 1 %}
			{% if  width is defined %}<div class="col-lg-{{ width }} col-md-{{ width }}">{% endif %} 
				{% if  field is defined %}<div class="form-group{{ errors is defined and errors.filter( field ) ? ' has-error' : '' }}">{% endif %}
					{% if  title is defined %}<label {% if  field  is defined %}class="control-label" for="{{  field }}" {% endif %} >{{  title }}</label>{% endif %}
							{%- for item in  list -%}
								{% if no_ids is defined %} 
									{% if item ==  value %} 
										<span 
										{% if  field is defined %} class="form-control" name="{{  field }}" id="{{  field }}" {% endif %}
										/>
											{{item }}
										</span>
									{% endif %}
								{% else %}
									{% if item['id'] ==  value %} 
										<span 
										{% if  field is defined %} class="form-control" name="{{  field }}" id="{{  field }}" {% endif %}
										/>
											{% if  option_fields is defined %}
												{% for option_field in  option_fields %}
													{{ item[option_field] }}&nbsp;
												{% endfor %}
											{% else %}
												{{ item['id'] }} {{ item['name'] }}
											{% endif %}
										</span>
									{% endif %}

								{% endif %}
							{%- endfor -%}
				{% if  field is defined %}
					</div>
				{% endif %}
			{% if  width is defined %}</div>{% endif %}
		{% endif %}
	{% else %}
		{% if  width is defined %}<div class="col-lg-{{ width }} col-md-{{ width }}">{% endif %} 
			{% if  field is defined %}<div class="form-group{{ errors is defined and errors.filter( field ) ? ' has-error' : '' }}">{% endif %}
				{% if  title  is defined %}<label {% if  field  is defined %}class="control-label" for="{{  field }}" {% endif %} >{{  title  }}</label>{% endif %}
					<select 
						{% if  field  is defined %} 
							class="form-control" name="{{  field  }}" id="{{  field  }}" 
						{% endif %}
					>
						{% if placeholder is defined %}
							<option value = "">{{placeholder}}</option>
						{% endif %}
						
						{%- for item in  list -%}
							<option 
								{% if no_ids is defined %} 
									{% if item ==  value %} selected="selected" {% endif %} 
								{% else %}
									{% if item['id'] ==  value %} selected="selected" {% endif %} 
								{% endif %}
								value="{% if no_ids is defined %}{{item}}{% else %}{{item['id']}}{% endif %}"> 
									{% if no_ids is defined %} 
										{{item }}
									{% else %}
										{% if  option_fields is defined %}
											{% for option_field in  option_fields %}
												{{ item[option_field] }}&nbsp;
											{% endfor %}
										{% else %}
											{{ item['id'] }} {{ item['name'] }}
										{% endif %}
									{% endif %}
							</option>
						{%- endfor -%}
					</select>
			{% if  field is defined %}
					{% if errors is defined and errors.filter( field ) %}
						<p class="help-block">{{ current(errors.filter( field )).getMessage() }}</p>
					{% endif %}
				</div>
			{% endif %}
		{% if  width  is defined %}</div>{% endif %}
	{% endif %}