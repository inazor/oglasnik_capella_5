{#
	ctlDropdown parametri:
		field - 'ime_polja'  - ako je postavljen dodaje klasu inputa (postaje form control)
		width - ako je postavljen dodaje div class="col-mg-width-..width".....
		title - ako je naveden dodaje Label
		list - array koji sadrži listu vrijednosti
		option_fields - ako je postavljeno, od ovih se polja slaže tekst u dropdownu (npr: ['id', 'path' ). Ako nije ništa navedeno default je 'name'
		readonly - ako je 1 nema odabira nego samo prikaz ako nije definiran ili nije 1 onda ga se ignorira
		no_ids - ako je postavljena, vraćaju se vrijednosti sa liste
		placeholder - ako je definiran, dodaje se početno nultno stanje koje ne vraća vrijednost

		primjer: ctlDropdown( ['value':entity.category_id, 'title':'Category', 'list':categories, 'field':'category_id', 'width':4, 'option_fields':['path','id'] ])

		kada se šalje lista bez id-jeva: partial("partials/ctlDropdown",['value':entity.status, 'title':'Status', 'list':statuses, 'width':3, 'field':'status', 'no_ids':1, 'errors': errors] )
#}

	{% if p is defined %}
		{% if p['field'] is defined %}{% set field = p['field'] %}{% endif %}
		{% if p['width'] is defined %}{% set width = p['width'] %}{% endif %}
		{% if p['title'] is defined %}{% set title = p['title'] %}{% endif %}
		{% if p['list'] is defined %}{% set list = p['list'] %}{% endif %}
		{% if p['option_fields'] is defined %}{% set option_fields = p['option_fields'] %}{% endif %}
		{% if p['readonly'] is defined %}{% set readonly = p['readonly'] %}{% endif %}
		{% if p['no_ids'] is defined %}{% set no_ids = p['no_ids'] %}{% endif %}
		{% if p['placeholder'] is defined %}{% set placeholder = p['placeholder'] %}{% endif %}
	{% endif %}

	
	{% if errors is defined %}
		{% set _errors = errors %}
	{% endif %}

	{% if _context is defined %}
		{% if _context['ajax_edit'] is defined and _context['current_entity_id'] is defined and _context['current_entity_table_name'] is defined and field is defined %}
			{% if _context['ajax_edit'] === true %}
				{% set is_ajax = true %}
				{% set ajax_table = _context['current_entity_table_name'] %}
				{% set ajax_id = _context['current_entity_id'] %}
			{% endif %}
		{% endif %}
		
		{% if _context['_errors'] is defined %}
			{% set _errors = _context['errors'] %}
		{% endif %}
		
	{% endif %}
	
	
	{% if  readonly is defined %}
		{% if readonly == 1 %}

		{% endif %}
	{% else %}
		{% if  width is defined %}<div class="col-lg-{{ width }} col-md-{{ width }}">{% endif %} 
			{% if  field is defined %}<div class="form-group{{ _errors is defined and _errors.filter( field ) ? ' has-error' : '' }}">{% endif %}
				{% if  title  is defined %}<label {% if  field  is defined %}class="control-label" for="{{  field }}" {% endif %} >{{  title  }}</label>{% endif %}
					<div {% if  field is defined %} class="form-control" name="{{  field }}" id="{{  field }}" {% endif %} >
						{%- for item in  list -%}
							<div id="{{item['id']}}" name="{{item['id']}}" style="float:left; border-radius: 5px; background: #73AD21; padding: 2px; margin-left:3px; margin-bottom:3px;" >
								{% if  option_fields is defined %}
									{% for option_field in  option_fields %}
										{{ item[option_field] }}
									{% endfor %}
								{% else %}
									{{ item['id'] }} {{ item['name'] }}
								{% endif %}
							</div>&nbsp;
						{%- endfor -%}
					</div>		
			
			{% if  field is defined %}
					{% if errors is defined and _errors.filter( field ) %}
						<p class="help-block">{{ current(_errors.filter( field )).getMessage() }}</p>
					{% endif %}
				</div>
			{% endif %}
		{% if  width  is defined %}</div>{% endif %}
	{% endif %}