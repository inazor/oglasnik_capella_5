{#
	blkTableSort2 parametri:  
	name - field name, ako nije definirano, nema botuna za sortiranje
	title - naslov na vrhu tablice
	class - ako je definirana, klasa od elementa <th>
	
	limit_options - 
	
	'search_ctl_params': ['field': 'q', 'placeholder': 'Enter search'] - parametri za search kontrolu (ono što bi se inače unijelo kad bi se htjelo pozvati tu kontrolu)
		Ako se ne unese ništa, stavlja defaultne vrijednosti fields i placeholder(field mora biti q)

	primjer; {{ partial("partials/blkTableSort",['fields':field_options, 'mode_options':mode_options, 'order_by_options':order_by_options, 'dir_options':dir_options, 'limit_options':limit_options ] ) }}

			{{ partial("partials/blkTableSort",['fields':[ 
				 ['title':'ID'				,'name':'id'				,'width':60		,'search_ctl':'ctlText'		,'search_ctl_params': ['field': 'q', 'placeholder': 'Enter search']] 
				,['title':'Name'			,'name':'name'				,'width':400	,'search_ctl':'ctlText'		,'search_ctl_params': ['field': 'q', 'placeholder': 'Enter search']] 
				,['title':'Unit price'		,'name':'unit_price'		,'width':100	,'search_ctl':'ctlNumeric'	,'search_ctl_params': ['field': 'q', 'placeholder': 'Enter search', 'decimals':2 ]]  
				,['title':'Publication'		,'name':'publication_id'	,'width':250	] 
				,['title':'Category'		,'name':'category_id'		,'width':150	] 
				,['title':'ID Resursa Nav'	,'name':'id_resursa_nav'	,'width':70 	] 
				,['title':'Old product ID'	,'name':'old_product_id'	,'width':200	,'search_ctl':'ctlText'		,'search_ctl_params': ['field': 'q', 'placeholder': 'Enter search']]  
				,['title':'Is Online Product','name':'is_online_product','width':50 	,'search_ctl':'ctlNumeric'	,'search_ctl_params': ['field': 'q', 'placeholder': 'Enter search']]  
				,['title':'Days Published'	,'name':'days_published'	,'width':50 	,'search_ctl':'ctlNumeric'	,'search_ctl_params': ['field': 'q', 'placeholder': 'Enter search', 'decimals':0 ]]
				,['title':'Is Display Ad'	,'name':'is_display_ad'		,'width':50		] 
				,['title':'Is Pushup Product','name':'is_pushup_product','width':50		] 
				,['title':'Is Active'		,'name':'is_active'			,'width':50		] 
				,['title':'Options'										,'width':200	] 
			] ]) }}	
			
		{{ partial("partials/blkTableSort",[
			'filter_values':filter_values
			,'sort_values':sort_values
			,'fields':[ 
				['title':'ID'			,'name':'id'			,'width':60		,'search_ctl':'ctlNumeric'	]
				,['title':'Name'		,'name':'name'			,'width':200	,'search_ctl':'ctlText'	] 
				,['title':'Amount'		,'name':'amount'		,'width':100	,'search_ctl':'ctlNumeric'	,'search_ctl_params': [ 'decimals':2 ] ] 
				,['title':'Tag'			,'name':'tag'			,'width':200	,'search_ctl':'ctlText'	] 
				,['title':'Category'	,'name':'category_id'	,'width':300	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':categories, 'option_fields':['path','id'] ]] 
				,['title':'Is active'	,'name':'is_active'		,'width':60		,'search_ctl':'ctlNumeric'	]
				,['title':'Options'								,'width':150	]
			] 
		]) }}
	
#}	
<form>
			<tr>
				<th>	
					<button type="submit">Submit</button>
				</th>
			</tr>
<!-- NASLOVI	 -->
	<tr>
		{%- for field in  fields -%}
			<th
				{% if  field['class'] is defined %} class = "{{field['class']}}" {% endif %}
				{% if  field['width'] is defined %} width = "{{field['width']}}" {% endif %}
				>{{ field['title'] }}
			</th>	
		{%- endfor -%}
	</tr>
	<!-- SORT GORE-GOLJE -->
		<tr>
			{%- for field in  fields -%}
				{#
					provjera je li se sortira po nazivima u vezanoj tablici
				#}
				{% set sort_field_name = 'sort_values[' ~ field['name'] ~ ']' %}
				{% if field['search_ctl'] == 'ctlDropdown' %}
					{% set sort_field_name = 'sort_values[' ~ field['name'] ~ ']' ~ '[name]' %}
					{% if field['search_ctl_params'] is defined %}
						{% if field['search_ctl_params']['option_fields'] is defined %}
							{% set sort_field_name = 'sort_values[' ~ field['name'] ~ ']' ~ field['search_ctl_params']['option_fields'][0] %}
						{% endif %}
					{% endif %}
				{% endif %}
				<th>
					{% if field['name'] is defined %}
							{% if sort_values[field['name']] is defined %}{% set val = sort_values[field['name']] %}{% else %}{% set val = '' %}{% endif %}
							
							<div style = "align:left; width:135px; overflow:hidden;">
								&nbsp;
										
								<input type="radio" {% if val == 'asc' %}checked{% endif %} name="{{sort_field_name}}" value="asc"></input>&nbsp;&nbsp;&nbsp;
								<input type="radio" {% if val == 'desc' %}checked{% endif %} name="{{sort_field_name}}" value="desc"></input>&nbsp;&nbsp;&nbsp;
								<input type="radio" name="{{sort_field_name}}" value=""></input>
								<br/>
								<div  class="glyphicon glyphicon-arrow-up btn-xs"/>
								<div  class="glyphicon glyphicon-arrow-down btn-xs"/>
								<div  class="glyphicon glyphicon-remove btn-xs"/>
							</div>
					{% endif %}
				</th>	
			{%- endfor -%}
		</tr>	
	<!-- PRETRAGA -->
		<tr>
			{%- for field in  fields -%}
				<th>
					{% if field['search_ctl'] is defined  %}
						{% if field['search_ctl_params'] is defined %}
							{% set params = field['search_ctl_params'] %}
						{% else %}
							{% set params = [] %}
						{% endif %}	
						{% if !(params['field'] is defined) %} 
							{% set params = params + ['field': 'filter_values[' ~ field['name'] ~ ']'] %} 
						{% endif %}	
						{% if !(params['placeholder'] is defined) %} {% set params = params + ['placeholder':'Enter Search'] %} {% endif %}	
						
						{#
						{{ field['name']  }} {{ filter_values[field['name']] }}
						{{ field['search_ctl'] }}
						#}
						{% if filter_values[field['name']] is defined %} 
							{% set params = params + ['value': 
							[field['name']] ] %} 
						{% endif %}
						{#{print_r(params,true)}#}
						
						
							<input type="hidden" name="field" id="field" value="{{ field['name'] }}"/>
							<input type="hidden" name="mode" id="mode" value="fuzzy"/>
							{{ partial("partials/" ~ field['search_ctl'],['p': params] ) }}

					{% endif %}
					
				</th>	
			{%- endfor -%}
		</tr>

	
</form>		
