{%- macro ctlText( p ) %}
{#
	ctlText parametri:  
	field - 'ime_polja'  - ako je postavljen dodaje klasu inputa (postaje form control)
	width - ako je postavljen dodaje div class="col-mg-width-..width".....
	title - ako je naveden dodaje Label
	readonly - ako je postavljen (1) dodaje READONLY na input

	primjer; ctlText(['value':entity.name, 'title':'Name', 'width':3, 'field':'name', 'readonly':1 ] ) 
#}

	{% if p['width'] is defined %}<div class="col-lg-{{p['width']}} col-md-{{p['width']}}">{% endif %} 
		{% if p['field'] is defined %}<div class="form-group{{ errors is defined and errors.filter(p['field']) ? ' has-error' : '' }}">{% endif %}
			{% if p['title'] is defined %}<label {% if p['field'] is defined %}class="control-label" for="{{ p['field'] }}" {% endif %} >{{ p['title'] }}</label>{% endif %}
			<input 
				{% if p['readonly'] is defined %} readonly {% endif %} 
				{% if p['field'] is defined %} class="form-control" name="{{ p['field'] }}" id="{{ p['field'] }}" {% endif %} 
				value="{{ p['value'] | default('') }}" maxlength="45" 
			/>
		{% if p['field'] is defined %}</div>{% endif %}
	{% if p['width'] is defined %}</div>{% endif %}
{%- endmacro %}






{%- macro ctlDate( p ) %}
{#
	parametri:  
	field - 'ime_polja'  - ako je postavljen dodaje klasu inputa (postaje form control)
	width - ako je postavljen dodaje div class="col-mg-width-..width".....
	title - ako je naveden dodaje Label
	readonly - ako je postavljen (1) dodaje READONLY na input

	primjer; ctlDate(['value':entity.name, 'title':'Name', 'width':3, 'field':'name', 'readonly':1 ] ) 
			 ctlDate( ['value':'2015-12-31' , 'title':'Datum', 'width':3, 'field':'date'] )
#}

	{% if p['width'] is defined %}<div class="col-lg-{{p['width']}} col-md-{{p['width']}}">{% endif %} 
		{% if p['field'] is defined %}<div class="form-group{{ errors is defined and errors.filter(p['field']) ? ' has-error' : '' }}">{% endif %}
			{% if p['title'] is defined %}<label {% if p['field'] is defined %}class="control-label" for="{{ p['field'] }}" {% endif %} >{{ p['title'] }}</label>{% endif %}
			<input 
				{% if p['readonly'] is defined %} readonly {% endif %} 
				{% if p['field'] is defined %} class="form-control" name="{{ p['field'] }}" id="{{ p['field'] }}" {% endif %} 
		   
				value="{{ (p['value'] ? date('d.m.Y', strtotime(p['value'])) : '') }}" 
				maxlength="10"
				onmouseover="	 $(this).datepicker({
									 format: 'dd.mm.yyyy',
									 weekStart: 1,
									 language: 'hr',
									 autoclose: true
				});">
		{% if p['field'] is defined %}</div>{% endif %}
	{% if p['width'] is defined %}</div>{% endif %}
{%- endmacro %}


{%- macro ctlDropdown( p ) %}
{#
	ctlDropdown parametri:
		field - 'ime_polja'  - ako je postavljen dodaje klasu inputa (postaje form control)
		width - ako je postavljen dodaje div class="col-mg-width-..width".....
		title - ako je naveden dodaje Label
		list - array koji sadrži listu vrijednosti
		option_fields - ako je postavljeno, od ovih se polja slaže tekst u dropdownu (npr: ['id', 'path']). Ako nije ništa navedeno default je 'name'
		--readonly - ako je postavljen (1) dodaje READONLY na input

		primjer: ctlDropdown( ['value':entity.category_id, 'title':'Category', 'list':categories, 'field':'category_id', 'width':4, 'option_fields':['path','id'] ])
#}

	{% if p['readonly'] is defined %}
		{% if p['width'] is defined %}<div class="col-lg-{{p['width']}} col-md-{{p['width']}}">{% endif %} 
			{% if p['field'] is defined %}<div class="form-group{{ errors is defined and errors.filter(p['field']) ? ' has-error' : '' }}">{% endif %}
				{% if p['title'] is defined %}<label {% if p['field'] is defined %}class="control-label" for="{{ p['field'] }}" {% endif %} >{{ p['title'] }}</label>{% endif %}
						{%- for item in p['list'] -%}
							{% if item['id'] == p['value'] %} 
								<span 
								{% if p['field'] is defined %} class="form-control" name="{{ p['field'] }}" id="{{ p['field'] }}" {% endif %}
								/>
									{% if p['option_fields'] is defined %}
										{% for option_field in p['option_fields'] %}
											{{ item[option_field] }}&nbsp;
										{% endfor %}
									{% else %}
										{{ item['id'] }} {{ item['name'] }}
									{% endif %}
								</span>
							{% endif %}
						{%- endfor -%}
			{% if p['field'] is defined %}
				</div>
			{% endif %}
		{% if p['width'] is defined %}</div>{% endif %}
	{% else %}
		{% if p['width'] is defined %}<div class="col-lg-{{p['width']}} col-md-{{p['width']}}">{% endif %} 
			{% if p['field'] is defined %}<div class="form-group{{ errors is defined and errors.filter(p['field']) ? ' has-error' : '' }}">{% endif %}
				{% if p['title'] is defined %}<label {% if p['field'] is defined %}class="control-label" for="{{ p['field'] }}" {% endif %} >{{ p['title'] }}</label>{% endif %}
					<select 
						{% if p['field'] is defined %} 
							class="form-control" name="{{ p['field'] }}" id="{{ p['field'] }}" 
						{% endif %}
					>
						{%- for item in p['list'] -%}
							<option 
								{% if item['id'] == p['value'] %} selected="selected" {% endif %} 
								value="{{ item['id'] }}"> 
									{% if p['option_fields'] is defined %}
										{% for option_field in p['option_fields'] %}
											{{ item[option_field] }}&nbsp;
										{% endfor %}
									{% else %}
										{{ item['id'] }} {{ item['name'] }}
									{% endif %}
							</option>
						{%- endfor -%}
					</select>
			{% if p['field'] is defined %}
					{% if errors is defined and errors.filter(pField) %}
						<p class="help-block">{{ current(errors.filter(pField)).getMessage() }}</p>
					{% endif %}
				</div>
			{% endif %}
		{% if p['width'] is defined %}</div>{% endif %}
	{% endif %}
{%- endmacro %}


{%- macro ctlCheckbox( p) %}
{#
	ctlText parametri:  
	field - 'ime_polja'  - ako je postavljen dodaje klasu inputa (postaje form control)
	width - ako je postavljen dodaje div class="col-mg-width-..width".....
	title - ako je naveden dodaje Label
	readonly - ako je postavljen (1) dodaje READONLY na input
	value - polje koje ima vrijednost - entity.is_active

	primjer; ctlText(['value':entity.name, 'title':'Name', 'width':3, 'field':'name', 'readonly':1 ] ) 
#}
	{% if p['width'] is defined %}<div class="col-lg-{{p['width']}} col-md-{{p['width']}}">{% endif %} 
		{% if p['field'] is defined %}<div class="form-group{{ errors is defined and errors.filter(p['field']) ? ' has-error' : '' }}">{% endif %}
			{% if p['title'] is defined %}<label {% if p['field'] is defined %}class="control-label" for="{{ p['field'] }}" {% endif %} >{{ p['title'] }}</label>{% endif %}
			<br/>
		<input type="checkbox" 
				{% if p['readonly'] is defined %} disabled="disabled" {% endif %} 
				{% if p['field'] is defined %} name="{{ p['field'] }}" id="{{ p['field'] }}" {% endif %} 
				value="1" 
				{% if p['value'] == "1"  %} checked {% endif %} 
				maxlength="2" 
			/>
			{% if errors is defined and errors.filter(pField) %}
				<p class="help-block">{{ current(errors.filter(pField)).getMessage() }}</p>
			{% endif %}
		{% if p['field'] is defined %}</div>{% endif %}
	{% if p['width'] is defined %}</div>{% endif %}
{%- endmacro %}

{%- macro btnSaveCancel() %}
        <div>
            <button class="btn btn-primary" type="submit" name="save">Save</button>
            <button class="btn btn-primary" type="submit" name="saveexit">Save and exit</button>
            <button class="btn btn-default" type="submit" name="cancel">Cancel</button>
        </div>
{%- endmacro %}