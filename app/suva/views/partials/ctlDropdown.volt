{#
	ctlDropdown parametri:
		field - 'ime_polja'  - ako je postavljen dodaje klasu inputa (postaje form control)
		value - vrijednost polja
		width - ako je postavljen dodaje div class="col-mg-width-..width".....
		title - ako je naveden dodaje Label
		list - array koji sadrži listu vrijednosti
		option_fields - ako je postavljeno, od ovih se polja slaže tekst u dropdownu (npr: ['id', 'path' ). Ako nije ništa navedeno default je 'name'
		readonly - ako je 1 nema odabira nego samo prikaz ako nije definiran ili nije 1 onda ga se ignorira
		no_ids - ako je postavljena, vraćaju se vrijednosti sa liste
		placeholder - ako je definiran, dodaje se početno nultno stanje koje ne vraća vrijednost
		js_onchange - ako je true, onda se forma submita nakon šta se promijeni vrijednost
		

		primjer: 
										{{ partial("partials/ctlDropdown",[
											'value':ad.n_moderation_reasons_id, 
											'title':'Moderation Reason', 
											'list':_model.ModerationReasons(), 
											'field':'n_moderation_reasons_id', 
											'width':4, 
											'option_fields':['id','name'],
											'errors': errors				
										] )}}	
					- za listu vrijednosti
										{{ partial("partials/ctlDropdown",[
											'value':ad.n_moderation_reasons_id, 
											'title':'Moderation Reason', 
											'list':['waiting', 'ok','nok', 'reported'], 
											'field':'n_moderation_reasons_id', 
											'width':4, 
											'option_fields':['id','name'],
											'errors': errors				
										] )}}	


		kada se šalje lista bez id-jeva: partial("partials/ctlDropdown",['value':entity.status, 'title':'Status', 'list':statuses, 'width':3, 'field':'status', 'no_ids':1, 'errors': errors] )
#}

	{% if p is defined %}
		{% if p['field'] is defined %}{% set field = p['field'] %}{% endif %}
		{% if p['value'] is defined %}{% set value = p['value'] %}{% endif %}
		{% if p['width'] is defined %}{% set width = p['width'] %}{% endif %}
		{% if p['title'] is defined %}{% set title = p['title'] %}{% endif %}
		{% if p['list'] is defined %}{% set list = p['list'] %}{% endif %}
		{% if p['option_fields'] is defined %}{% set option_fields = p['option_fields'] %}{% endif %}
		{% if p['readonly'] is defined %}{% set readonly = p['readonly'] %}{% endif %}
		{% if p['no_ids'] is defined %}{% set no_ids = p['no_ids'] %}{% endif %}
		{% if p['placeholder'] is defined %}{% set placeholder = p['placeholder'] %}{% endif %}
		{% if p['_context'] is defined %}{% set _context = p['_context'] %}{% endif %}
	{% endif %}
	{% if placeholder is defined %}
		{% set _placeholder = placeholder %}
	{% else %}
		{% set _placeholder = null %}
	{% endif%}
	{% if value is defined %}
		{% set _value = value %}
	{% else %}
		{% set _value = null %}
	{% endif%}
	
	{% if _errors is not defined and _context is defined %}
		{% if _context['errors'] is defined %}
			{% set _errors = _context['errors'] %}
		{% endif %}
	{% endif %}
	
	{#{% if value === '' %}{% set value = null %}{% endif %}#}
	
	{% if not(is_array(list) ) %} {% set list = list.toArray() %}{% endif %}
	
	{#
	VALUE:{{ value}}{{ gettype(value)}}
	{%- for item in  list -%}
						{{item['id']}}
						{% if item['id'] ==  value %} JEE {% endif %}
	{% endfor %}
	#}
	{#	AJAX_EDIT {{ ajax_edit }} #}
	
	{% set _readonly = false %}
	{% if  readonly is defined %}
		{% if readonly === 1 or readonly === true %}
			{% set _readonly = true %}
		{% endif %}
	{% endif %}
	
	{% set _js_onchange = "" %}
	{% if js_onchange is defined %}
		{% set _js_onchange = js_onchange %}
	{% endif %}
	
		
	
	{% if  _readonly %}
		{% if  width is defined %}<div class="col-lg-{{ width }} col-md-{{ width }}">{% endif %} 
			{% if  field is defined %}<div class=" form-group{{ _errors is defined and _errors.filter( field ) ? ' has-error' : '' }}">{% endif %}
				{% if  title is defined %}<label {% if  field  is defined %}class="control-label" for="{{  field }}" {% endif %} >{{  title }}</label>{% endif %}
						{%- for item in  list -%}
							{% if no_ids is defined %} 
								{% if item ==  _value %} 
									<span 
										style = "overflow:hidden;"
									{% if  field is defined %} class="form-control" name="{{  field }}" id="{{  field }}" {% endif %}
									/>
										{{item }}
									</span>
								{% endif %}
							{% else %}
								{% if item['id'] ==  _value %} 
									<span 
										style = "overflow:hidden;"
									{% if  field is defined %} class="form-control" name="{{  field }}" id="{{  field }}" {% endif %}
									/>
										{% if  option_fields is defined %}
											{% for option_field in  option_fields %}
												{% if substr(option_field, 0 , 1  ) == "'" %}
													{{  str_replace("'", "", option_field) }}
												{% else %}
													{{ item[option_field] }}&nbsp;
												{% endif %}
											{% endfor %}
										{% else %}
											{{ item['id'] }} {{ item['name'] }}
										{% endif %}
									</span>
								{% endif %}

							{% endif %}
						{%- endfor -%}
			{% if  field is defined %}
				</div>
			{% endif %}
		{% if  width is defined %}</div>{% endif %}
	{% else %}
	<!-- TODO: ovo izbrisati greske idu preko contexta -->
		{#{% if errors is defined %}
			{% set _errors = errors %}
		{% endif %}#}

		{% set is_ajax = false %}
		{% if _context is defined %}
			{% if _context['ajax_edit'] is defined and _context['current_entity_id'] is defined and _context['current_entity_table_name'] is defined and field is defined %}
				{% if _context['ajax_edit'] === true %}
					{% set is_ajax = true %}
					{% set ajax_table = _context['current_entity_table_name'] %}
					{% set ajax_id = _context['current_entity_id'] %}
				{% endif %}
			{% endif %}

			{% if _context['_errors'] is defined %}
				{% set _errors = _context['errors'] %}
			{% endif %}

		{% endif %}


		
		{#
		VALUE:{{ value }} {{gettype(value)}}  
		{% if value === null  %}NULL{% endif %}
		{% if value === ''  %}PRAZAN{% endif %}
		#}
		<script>
		/*$('select option')
		.filter(function() {
			return !this.value || $.trim(this.value).length == 0 || $.trim(this.text).length == 0;
			})
			.remove();*/
		</script>
		
		{% if  width is defined %}<div class="col-lg-{{ width }} col-md-{{ width }}">{% endif %} 
			{% if  field is defined %}<div class=" form-group{{ _errors is defined and _errors.filter( field ) ? ' has-error' : '' }}">{% endif %}
				{% if  title  is defined %}<label {% if  field  is defined %}class="control-label" for="{{  field }}" {% endif %} >{{  title  }}</label>{% endif %}
					{% if is_ajax %}
						{% set id_string = 'id__' ~ ajax_id ~ '__field__' ~ field %}
					{% endif %}
					
					<select 
						style="box-sizing : border-box;"
						class = "
							{% if  field  is defined %} form-control {% endif %}
							{% if is_ajax %} ajax_edit {% endif %}
						"
					
						{% if  field  is defined %} 
							name="{{  field  }}" 
						{% endif %}
						
						
						{% if is_ajax %}
							id = "{{ id_string }}"
						{% else %}
							id = "{{  field  }}"
						{% endif %}
						
						onchange = "
							{% if is_ajax %}
								ctlDropdownUpdateField( '{{ id_string }}', '{{ ajax_table }}', '{{ ajax_id }}', '{{ field }}' );
							{% endif %}
							
							{{ _js_onchange }}
							
							{#% if _js_onchange %}
								var form = $(this).parents('form:first');
								alert (window.location.href);
								$('#next').attr( 'value', '' );
								alert ($('#next').val());
								form.submit();
							{% endif %#}
							"
								
					>
						<option {% if _value === null %}selected{% endif %} value = "" >{{_placeholder}}</option>
						
						{%- for item in  list -%}
							<option 
								{% if no_ids is defined %} 
									{% if item ==  _value %} selected="selected" {% endif %} 
								{% else %}
									{% if item['id'] ==  _value %} selected="selected" {% endif %}
								{% endif %}
								value="{% if no_ids is defined %}{{item}}{% else %}{{item['id']}}{% endif %}"> 
									{% if no_ids is defined %} 
										{{ item }}
									{% else %}
										{% if  option_fields is defined %}
											{% for option_field in  option_fields %}
												
												{% if substr( option_field, 0 , 1  ) == "'" %}
													{{  str_replace("'", "", option_field) }}
												{% else %}
													{{ item[option_field] }}&nbsp;
												{% endif %}
											{% endfor %}
										{% else %}
											{{ item['id'] }} {{ item['name'] }}
										{% endif %}
									{% endif %}
							</option>
						{%- endfor -%}
					</select>
			{% if  field is defined %}
					{% if _errors is defined and _errors.filter( field ) %}
						<p class="help-block">{{ current(_errors.filter( field )).getMessage() }}</p>
					{% endif %}
				</div>
			{% endif %}
		{% if  width  is defined %}</div>{% endif %}
	{% endif %}