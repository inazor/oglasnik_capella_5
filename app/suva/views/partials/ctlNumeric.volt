{#
	ctlNumeric parametri:  
	field - 'ime_polja'  - ako je postavljen dodaje klasu inputa (postaje form control)
	width - ako je postavljen dodaje div class="col-mg-width-..width".....
	title - ako je naveden dodaje Label
	readonly - ako je postavljen (1) dodaje READONLY na input
	placeholder - defaultni tekst (( Unesite vrijednost.. ))
	decimals - broj decimalnih mjesta, ako je postavljen mijenja se zarez u točku automatski
	uom - jedinica mjere, ako je definirana, prikazuje se desno od broja
	ajax_edit - ako je definiran onda se izmjene odmah šalju ajaxom. Potrebne su još varijable ajax_id, ajax_table
	ajax_id - vidi ajax_edit
	ajax_table - vidi ajax_edit
	
	primjer; 
		{{ partial("partials/ctlNumeric",[
			'value':entity.unit_price, 
			'title':'Unit price', 
			'width':1, 
			'field':'unit_price', 
			'decimals':2, 
			'errors': errors
		] ) }}
		 
		{{ partial("partials/ctlNumeric",[
			'value':entity.unit_price, 
			'title':'Unit price', 
			'width':1, 
			'field':'unit_price', 
			'decimals':2, 
			'_context': _context
		] ) }}
#}

	{#{ print_r(_context,true)}#}

	{% if p is defined %}
		{% if p['field'] is defined %}{% set field = p['field'] %}{% endif %}
		{% if p['value'] is defined %}{% set value = p['value'] %}{% endif %}
		{% if p['width'] is defined %}{% set width = p['width'] %}{% endif %}
		{% if p['title'] is defined %}{% set title = p['title'] %}{% endif %}
		{% if p['decimals'] is defined %}{% set decimals = p['decimals'] %}{% endif %}
		{% if p['_context'] is defined %}{% set _context = p['_context'] %}{% endif %}
		{% if p['readonly'] is defined %}{% set readonly = p['readonly'] %}{% endif %}
		{% if p['placeholder'] is defined %}{% set placeholder = p['placeholder'] %}{% endif %}
	{% endif %}
	{% if value is defined %}
		{% set _value = value %}
	{% else %}
		{% set _value = null %}
	{% endif%}
	
	{% set is_ajax = false %}
	<!-- TODO: ovo izbrisati greske idu preko contexta -->
	{#{% if errors is defined %}{% set _errors = errors %}{% endif %}#}
	
	{% set _readonly = false %}{% if readonly is defined %}{% if readonly == true %}{% set _readonly = true %}{% endif %}{% endif %}
	{% set _decimals = false %}{% if decimals is defined %}{% if decimals > 0 %}{% set _decimals = decimals %}{% endif %}{% endif %}
 

	{% if _context is defined %}
		{% if _context['ajax_edit'] is defined and _context['current_entity_id'] is defined and _context['current_entity_table_name'] is defined and field is defined %}
			{% if _context['ajax_edit'] === true %}
				{% set is_ajax = true %}
				{% set ajax_table = _context['current_entity_table_name'] %}
				{% set ajax_id = _context['current_entity_id'] %}
			{% endif %}
		{% endif %}
		
		{% if _context['_errors'] is defined %}
			{% set _errors = _context['errors'] %}
		{% endif %}
		
	{% endif %}
	

	
	
	<style>
		input::-webkit-inner-spin-button {
			/* display: none; <- Crashes Chrome on hover */
			-webkit-appearance: none;
			margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
		}
	
	</style>
	

	
	

	{% if  width is defined %}<div class="col-lg-{{ width}} col-md-{{ width }}" >{% endif %} 
		{% if  field  is defined %}<div class="form-group{{ _errors is defined and _errors.filter( field ) ? ' has-error' : '' }}">{% endif %}
			{% if  title  is defined %}<label {% if  field  is defined %}class="control-label" for="{{  field  }}" {% endif %} >{{  title  }}</label>{% endif %}
			<input 
				style="box-sizing : border-box; text-align:right;"
				type="number"
				{% if placeholder is defined %} placeholder = "{{ placeholder}}" {% endif %}
				{% if  _readonly %} readonly {% endif %} 
				class = "
					{% if  field  is defined %}form-control{% endif %}
					{% if is_ajax %} ajax_edit {% endif %}
				"
				{% if  field  is defined %}  name="{{  field  }}"  {% endif %} 
				{% if _value === null %} value= "" {% endif %}
				{% if _value === '' %} {{ _value == null }}  {% endif %}
				{% if _decimals %}
					value="{{ number_format(_value, _decimals,'.','') }}" 
				{% else %}
					value="{{ _value }}" 
				{% endif %}
					
				
				maxlength="45" 
				{% if _decimals and is_ajax === false %}
						onchange = "this.value = this.value.replace(',','.' ); this.value = parseFloat(this.value).toFixed({{ _decimals }});" 
				{% endif %} 
				
				{% if is_ajax %}
					{% set id_string = 'id__' ~ ajax_id ~ '__field__' ~ field %}
					id = "{{ id_string }}"
					onchange = " ctlNumericUpdateField( '{{ id_string }}', '{{ ajax_table }}', '{{ ajax_id }}', '{{ field }}' ); "
				{% else %}
					id="{{  field  }}"
				{% endif %}
					
				
			/>
			{% if uom is defined %}
				{% if strlen(uom) > 0 %}
					<div style="float:left;">{{ uom }}</div>
				{% endif %}
			{% endif %}
		{% if  field  is defined %}</div>{% endif %}
	{% if  width  is defined %}</div>{% endif %}