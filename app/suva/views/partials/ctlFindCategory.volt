{#
	ctlFindUser parametri:  
	field - 'ime_polja'  - ako je postavljen dodaje klasu inputa (postaje form control)
	width - ako je postavljen dodaje div class="col-mg-width-..width".....
	title - ako je naveden dodaje Label
	readonly - ako je postavljen (1) dodaje READONLY na input
	placeholder - defaultni tekst (( Unesite vrijednost.. ))

	primjer; ctlText(['value':entity.name, 'title':'Name', 'width':3, 'field':'name',  'user':'sales_rep', 'readonly':1, 'errors':errors ] ) 
#}

{{ javascript_include("assets/suva/js/n_user_search.js") }}

{% set field = 'n_sales_rep_id' %}
{% set user = sales_rep %}
<div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
	<label class="control-label" for="{{ field }}_input">Sales rep</label>

		{# Showing the user selector only for new ads #}
		{%- if (not(user is empty)) -%}
				<input type="hidden" name="{{ field }}" id="{{ field }}" value="{{ user.id }}" data-username="{{ user.username }}" />
		{%- else -%}
				<input type="hidden" name="{{ field }}" id="{{ field }}" value="" data-username="" />
		{%- endif -%}
		<input type="text" class="form-control" id="{{ field }}_input" value="" onmouseover="getUserName('{{field}}');" />
	{%- if errors is defined and errors.filter(field) -%}
		<p class="help-block">{{- current(errors.filter(field)).getMessage() -}}</p>
	{%- endif -%}
</div>