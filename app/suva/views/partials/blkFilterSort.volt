{#
	blkFilterSort parametri:  
	field_options -
	mode_options - 
	order_by_options - 
	dir_options - 
	limit_options - 

	primjer; {{ partial("partials/blkFilterSort",['field_options':field_options, 'mode_options':mode_options, 'order_by_options':order_by_options, 'dir_options':dir_options, 'limit_options':limit_options ] ) }}
#}

<form class="form-inline" role="form">
	{{ hiddenField('field') }}
	{{ hiddenField('mode') }}
	<div class="row">
		<div class="col-lg-5 col-sm-6">
			<div class="input-group search-panel">
				<div class="input-group-btn">
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
						<span class="dropdown-label" id="search_field">All fields</span> <span class="caret"></span>
					</button>
					<ul data-param="#field" class="dropdown-menu" role="menu">
						<li role="presentation" class="dropdown-header">Narrow down your search</li>
						{%- for f in field_options -%}
							<li{% if f == field %} class="active"{% endif %}><a href="#{{ f }}">{{ f }}</a></li>
						{%- endfor -%}
						<li class="divider"></li>
						<li><a href="#all">All fields</a></li>
					</ul>
				</div>
				<input type="text" class="form-control" name="q" value="{{ q|escape_attr }}" placeholder="Search term(s)...">
				<div class="input-group-btn">
					<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
						<span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul data-param="#mode" class="dropdown-menu dropdown-menu-right" role="menu">
						<li role="presentation" class="dropdown-header">Search mode</li>
						{% for m, m_desc in mode_options %}
							<li{% if m == mode %} class="active"{% endif %}><a href="#{{ m }}">{{ m_desc }}</a></li>
						{% endfor %}
					</ul>
				</div>

			</div>

		</div>
		{#
		<div class="col-lg-6 col-sm-6 text-right">
			<select id="order-by" name="order_by" class="form-control" title="Order by">
				{%- for value, title in order_by_options -%}
					<option {% if value == order_by %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
				{%- endfor -%}
			</select>
			<select id="dir" name="dir" class="form-control" title="ASC/DESC">
				{%- for value, title in dir_options -%}
					<option {% if value == dir %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
				{%- endfor -%}
			</select>
			<select id="limit" name="limit" class="form-control" title="Records per page">
				{%- for value in limit_options -%}
					<option {% if value == limit %} selected="selected" {% endif %}value="{{ value }}">{{ value }}</option>
				{%- endfor -%}
			</select>
		</div>
		#}
	</div>
 </form>
