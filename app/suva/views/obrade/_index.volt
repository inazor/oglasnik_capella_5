<h1>Obrade</h1>

<br></br>
{{ partial("partials/blkFlashSession") }}

<br></br>
<div class ="row">
	{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/obrade/updateUsersContacts/', 'method' : 'post', 'autocomplete' : 'off') }}
		{{ hiddenField('_csrftoken') }}
		{{ hiddenField('next') }}		
		
		<div class="col-lg-2 col-md-2"><br/>
			<button type="submit" class="btn btn-warning">Update User's contacts from Users</button>
		</div>
	{{ endForm() }}	
</div>

<div class ="row">
	{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/obrade/updateOrderTotals', 'method' : 'post', 'autocomplete' : 'off') }}
		{{ hiddenField('_csrftoken') }}
		{{ hiddenField('next') }}		
		
		<div class="col-lg-2 col-md-2"><br/>
			<button type="submit" class="btn btn-warning">Update totals on all orders</button>
		</div>
	{{ endForm() }}	
</div>

<div class ="row">
	{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/obrade/syncAvusUsers', 'method' : 'post', 'autocomplete' : 'off') }}
		{{ hiddenField('_csrftoken') }}
		{{ hiddenField('next') }}		
		
		<div class="col-lg-2 col-md-2"><br/>
			<button type="submit" class="btn btn-warning">Synchronize users from AVUS</button>
		</div>
	{{ endForm() }}	
</div>

<div class ="row">
	{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/obrade/syncAvusAds', 'method' : 'post', 'autocomplete' : 'off') }}
		{{ hiddenField('_csrftoken') }}
		{{ hiddenField('next') }}		
		
		<div class="col-lg-2 col-md-2"><br/>
			<button type="submit" class="btn btn-warning">Synchronize Ads from AVUS</button>
		</div>
	{{ endForm() }}	
</div>

<div class ="row">
	{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/obrade/syncNewAvusAds', 'method' : 'post', 'autocomplete' : 'off') }}
		{{ hiddenField('_csrftoken') }}
		{{ hiddenField('next') }}		
		
		<div class="col-lg-2 col-md-2"><br/>
			<button type="submit" class="btn btn-warning">Synchronize New Ads from AVUS</button>
		</div>
	{{ endForm() }}	
</div>

<div class ="row">
	{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/obrade/syncAvusInsertions', 'method' : 'post', 'autocomplete' : 'off') }}
		{{ hiddenField('_csrftoken') }}
		{{ hiddenField('next') }}		
		
		<div class="col-lg-2 col-md-2"><br/>
			<button type="submit" class="btn btn-warning">Synchronize Insertions from AVUS</button>
		</div>
	{{ endForm() }}	
</div>

<div class ="row">
	{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/obrade/syncAvusAdsWithUsers', 'method' : 'post', 'autocomplete' : 'off') }}
		{{ hiddenField('_csrftoken') }}
		{{ hiddenField('next') }}		
		
		<div class="col-lg-2 col-md-2"><br/>
			<button type="submit" class="btn btn-warning">Synchronize Ads from AVUS with USERS</button>
		</div>
	{{ endForm() }}	
</div>
<div class ="row">
	{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/obrade/syncAvusAdsWithCategories', 'method' : 'post', 'autocomplete' : 'off') }}
		{{ hiddenField('_csrftoken') }}
		{{ hiddenField('next') }}		
		
		<div class="col-lg-2 col-md-2"><br/>
			<button type="submit" class="btn btn-warning">Synchronize Ads from AVUS with CATEGORIES</button>
		</div>
	{{ endForm() }}	
</div>
<div class ="row">
	{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/obrade/syncAvusAdsDefaultWithUsers', 'method' : 'post', 'autocomplete' : 'off') }}
		{{ hiddenField('_csrftoken') }}
		{{ hiddenField('next') }}		
		
		<div class="col-lg-2 col-md-2"><br/>
			<button type="submit" class="btn btn-danger">Synchronize Avus Ads with default User with Suva user</button>
		</div>
	{{ endForm() }}	
</div>


<div class ="row">
	{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/obrade/avusSql', 'method' : 'post', 'autocomplete' : 'off') }}
		{{ hiddenField('_csrftoken') }}
		{{ hiddenField('next') }}		
		{{ partial("partials/ctlTextArea",[
				'value': _context['obrada_sql_avus'], 
				'rows' : 5,
				'title':'SQL AVUS:', 
				'field':'_context[obrada_sql_avus]', 
				'width': 8
		] )}}
		<div class="col-lg-2 col-md-2"><br/>
			<button type="submit" class="btn btn-warning">AVUS RUN SQL STATEMENT</button>
		</div>
	{{ endForm() }}	
</div>

<div class ="row">
	{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/obrade/navisionSql', 'method' : 'post', 'autocomplete' : 'off') }}
		{{ hiddenField('_csrftoken') }}
		{{ hiddenField('next') }}		
		
		{{ partial("partials/ctlTextArea",[
				'value': _context['obrada_sql'], 
				'rows' : 5,
				'title':'SQL NAVISION:', 
				'field':'_context[obrada_sql]', 
				'width': 8
		] )}}
		<div class="col-lg-2 col-md-2"><br/>
			<button type="submit" class="btn btn-warning">NAVISION RUN SQL STATEMENT</button>
		</div>
	{{ endForm() }}	
</div>

<div class ="row">
	{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/obrade/navisionTestFiskal', 'method' : 'post', 'autocomplete' : 'off') }}
		{{ hiddenField('_csrftoken') }}
		{{ hiddenField('next') }}		
		
		{{ partial("partials/ctlText",[
				'value': _context['obrada_broj_racuna'], 
				'rows' : 5,
				'title':'RAČUN', 
				'field':'_context[obrada_broj_racuna]', 
				'width': 2
		] )}}
		<div class="col-lg-2 col-md-2"><br/>
			<button type="submit" class="btn btn-warning">NAVISION FISKALIZIRAJ RAČUN</button>
		</div>
	{{ endForm() }}	
</div>

<div class ="row">
	{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/obrade/navisionRemoveFiskal', 'method' : 'post', 'autocomplete' : 'off') }}
		{{ hiddenField('_csrftoken') }}
		{{ hiddenField('next') }}		

		<div class="col-lg-2 col-md-2"><br/>
			<button type="submit" class="btn btn-warning">NAVISION IZBRISI ZADNJI RAČUN</button>
		</div>
	{{ endForm() }}	
</div>


<div class ="row">
	{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/obrade/navisionSendFiskal', 'method' : 'post', 'autocomplete' : 'off') }}
		{{ hiddenField('_csrftoken') }}
		{{ hiddenField('next') }}		
		
		<div class="col-lg-2 col-md-2"><br/>
			<button type="submit" class="btn btn-warning">POŠALJI PODATKE ZA FISKALIZACIJU U NAVISION</button>
		</div>
	{{ endForm() }}	
</div>

<div class ="row">
	{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/obrade/navisionGetFiskal', 'method' : 'post', 'autocomplete' : 'off') }}
		{{ hiddenField('_csrftoken') }}
		{{ hiddenField('next') }}		
		
		<div class="col-lg-2 col-md-2"><br/>
			<button type="submit" class="btn btn-warning">DOHVATI NAVISION FISKALIZACIJU</button>
		</div>
	{{ endForm() }}	
</div>

{#
	<div class ="row">
		{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/obrade/testSendOrdersToNavision', 'method' : 'post', 'autocomplete' : 'off') }}
			{{ hiddenField('_csrftoken') }}
			{{ hiddenField('next') }}		
			
			<div class="col-lg-2 col-md-2"><br/>
				<button type="submit" class="btn btn-warning">TEST SENDING ORDERS TO NAVISION</button>
			</div>
		{{ endForm() }}	
	</div>
#}
<div class ="row">
	{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/obrade/updateUserDefaultPaymentType', 'method' : 'post', 'autocomplete' : 'off') }}
		{{ hiddenField('_csrftoken') }}
		{{ hiddenField('next') }}		
		
		<div class="col-lg-2 col-md-2"><br/>
			<button type="submit" class="btn btn-warning">UPDATE USER DEFAULT PAYMENT TYPE</button>
		</div>
	{{ endForm() }}	
</div>

<div class ="row">
	{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/obrade/updateOrdersStatuses', 'method' : 'post', 'autocomplete' : 'off') }}
		{{ hiddenField('_csrftoken') }}
		{{ hiddenField('next') }}		
		
		<div class="col-lg-2 col-md-2"><br/>
			<button type="submit" class="btn btn-warning">UPDATE ORDERS STATUSES</button>
		</div>
	{{ endForm() }}	
</div>


<div class ="row">
	{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/obrade/updateNpaths', 'method' : 'post', 'autocomplete' : 'off') }}
		{{ hiddenField('_csrftoken') }}
		{{ hiddenField('next') }}		
		
		<div class="col-lg-2 col-md-2"><br/>
			<button type="submit" class="btn btn-warning">UPDATE n_path FIELD on Categories, Categories Mappings Dictionaries and Locations</button>
		</div>
	{{ endForm() }}	
</div>

<div class ="row">
	{{ form(NULL, 'id' : 'frm_register', 'action' : '/suva/obrade/updateSessionStorage', 'method' : 'post', 'autocomplete' : 'off') }}
		{{ hiddenField('_csrftoken') }}
		{{ hiddenField('next') }}		
		
		<div class="col-lg-2 col-md-2"><br/>
			<button type="submit" class="btn btn-warning">UPDATE APP SETTINGS AND TRANSLATIONS IN $_SESSION</button>
		</div>
	{{ endForm() }}	
</div>