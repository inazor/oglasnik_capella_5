{# Admin's - Error 503 | base-app | 2.0 #}
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Greška 503 - Usluga privremeno nedostupna</h1>
        <h4 class="text-muted">Vraćamo se uskoro.</h4>
    </div>
</div>
