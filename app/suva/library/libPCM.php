<?php

namespace Baseapp\Suva\Library;


use Baseapp\Suva\Library\Nava;

class libPCM {

public static function getIds ( $p = array() ) {
	
	/*
		$p = array (
			"modelName" => "Ads"
			
			,"filters" => array (
				array(
					"modelName" => "Issues"
					,"idValue" => 100
				)
				,array(
					"modelName" => "CategoriesMappings"
					,"idValue" => 5381
				)
				,array(
					"modelName" => "Ads"
					,"whereField" => "moderation"
					,"whereValue" =>"'ok'"
				)
			)
		)
	
	
	*/
	
	
	$result = array();
	
	if ( ! array_key_exists('modelName' , $p )) return $result;
	
	if ( ! array_key_exists('filters' , $p )) return $result;
	
	
	if ($p['modelName'] == "Ads"){
		$txtSelect = " a_pcm.ad_id ";
		$rsFieldName = "ad_id";
	}
	elseif ($p['modelName'] == "PublicationsCategoriesMappings"){
		$txtSelect = " a_pcm.pcm_id ";
		$rsFieldName = "pcm_id";
	}
	elseif ($p['modelName'] == "CategoriesMappings"){
		$txtSelect = " a_pcm.cm_id ";
		$rsFieldName = "cm_id";
	}
	
	else return $result;
	
	$critIssueId = "";
	$critCategoryMappingId = "";
	$critAdWhere = "";
	$critOrderWhere = "and o.n_status in (2,4,5)";
	foreach ( $p['filters'] as $filter ){
		if ( $filter['modelName'] == "Issues" && $filter['idValue'] > 0 ){
			$critIssueId = " and  iss.id = ".$filter['idValue'];
		}
		
		if ( $filter['modelName'] == "CategoriesMappings" && $filter['idValue'] > 0 ){
			$critCategoryMappingId = " and pcm.category_mapping_id = ".$filter['idValue'];
		}

		if ( $filter['modelName'] == "Ads" && $filter['whereField'] > "" && $filter['whereValue'] > ""){
			$whereField = $filter['whereField'];
			$whereValue = $filter['whereValue'];
			$critAdWhere .= " and a.$whereField = $whereValue ";
		}
		
		if ( $filter['modelName'] == "Orders" && $filter['whereField'] > "" && $filter['whereValue'] > ""){
			$whereField = $filter['whereField'];
			$whereValue = $filter['whereValue'];
			$critOrderWhere .= " and  o.$whereField in ($whereValue) ";
		}
		
		
		
	}
	
			$txtSql = "
select 
	$txtSelect
from 
(


select 
	a.id as ad_id, pcm.id as pcm_id, pcm.category_mapping_id as cm_id
from ads a 
	join ads_parameters ap on (ap.ad_id = a.id)
		join parameters p on (p.id = ap.parameter_id)
			join parameters_types pt on (pt.id = p.type_id and pt.accept_dictionary = 1)
		join n_publications_categories_mappings pcm on ( 
			pcm.mapped_category_id = a.category_id
			$critCategoryMappingId
			and pcm.mapped_location_id is null 
			and pcm.mapped_dictionary_id is not null
			/*and pcm.mapped_dictionary_id = cast(ap.value as UNSIGNED)*/
			and pcm.mapped_dictionary_id = ap.n_dictionary_id
			)
where 1=1
	$critAdWhere
union

select 
	a.id as ad_id, pcm.id as pcm_id, pcm.category_mapping_id as cm_id
from
	ads a
		join n_publications_categories_mappings pcm  on (
			pcm.mapped_category_id = a.category_id 
			$critCategoryMappingId
			and pcm.mapped_dictionary_id is null
			and pcm.mapped_location_id is not null
			and pcm.mapped_location_id in (
				a.country_id, a.county_id, a.municipality_id, a.city_id)
			)
where 1=1
	$critAdWhere

union

select 
	a.id as ad_id, pcm.id as pcm_id, pcm.category_mapping_id as cm_id
from
	ads a
		join n_publications_categories_mappings pcm  on (
			pcm.mapped_category_id = a.category_id 
			$critCategoryMappingId
			and pcm.mapped_dictionary_id is null
			and pcm.mapped_location_id is null
		)
where 1=1
	$critAdWhere

union

select 
	a.id as ad_id, pcm.id as pcm_id, pcm.category_mapping_id as cm_id
from ads a 
	join ads_parameters ap on (ap.ad_id = a.id)
		join parameters p on (p.id = ap.parameter_id)
			join parameters_types pt on (pt.id = p.type_id and pt.accept_dictionary = 1)
		join n_publications_categories_mappings pcm on ( 
			pcm.mapped_category_id = a.category_id
			$critCategoryMappingId
			and pcm.mapped_location_id is not null 
			and pcm.mapped_dictionary_id is not null
			/*and pcm.mapped_dictionary_id = cast(ap.value as UNSIGNED)*/
			and pcm.mapped_dictionary_id = ap.n_dictionary_id
			and pcm.mapped_location_id in (
				a.country_id, a.county_id, a.municipality_id, a.city_id
			)
		)
where 1=1
	$critAdWhere

) a_pcm
	join orders_items oi on (a_pcm.ad_id = oi.ad_id)
		join orders o on (
			oi.order_id = o.id 
			$critOrderWhere )
		join n_insertions ins on ( oi.id = ins.orders_items_id )
			join n_issues iss on (
				ins.issues_id = iss.id
				$critIssueId
			)
		";
		//error_log("libPCM SQL".$txtSql);
		$rsIds = \Baseapp\Suva\Library\Nava::sqlToArray($txtSql);
		if ($rsIds)
			foreach ($rsIds as $row)
				if (! in_array ( $row[ $rsFieldName ], $result ) )
					array_push($result, $row[ $rsFieldName ]);
	
		return $result;
	}
	
	public static function getCategoriesMappingsForAd ($pAdId){
		
		if (! $pAdId) {
			return null;
		}
		$adId = $pAdId;
		
		$txtSql = "
		
select 
	ad_id, publication_id, cm_id, pcm_id
from 
(


select 
	a.id as ad_id, pcm.id as pcm_id, pcm.category_mapping_id as cm_id, pcm.publication_id
from ads a 
	join ads_parameters ap on (ap.ad_id = a.id)
		join parameters p on (p.id = ap.parameter_id)
			join parameters_types pt on (pt.id = p.type_id and pt.accept_dictionary = 1)
		join n_publications_categories_mappings pcm on ( 
			pcm.mapped_category_id = a.category_id
			and pcm.mapped_location_id is null 
			and pcm.mapped_dictionary_id is not null
			and pcm.mapped_dictionary_id = ap.n_dictionary_id
			)
where 1=1
	and a.id = $adId
union

select 
	a.id as ad_id, pcm.id as pcm_id, pcm.category_mapping_id as cm_id, pcm.publication_id
from
	ads a
		join n_publications_categories_mappings pcm  on (
			pcm.mapped_category_id = a.category_id 

			and pcm.mapped_dictionary_id is null
			and pcm.mapped_location_id is not null
			and pcm.mapped_location_id in (
				a.country_id, a.county_id, a.municipality_id, a.city_id)
			)
where 1=1
	and a.id = $adId

union

select 
	a.id as ad_id, pcm.id as pcm_id, pcm.category_mapping_id as cm_id, pcm.publication_id
from
	ads a
		join n_publications_categories_mappings pcm  on (
			pcm.mapped_category_id = a.category_id 

			and pcm.mapped_dictionary_id is null
			and pcm.mapped_location_id is null
		)
where 1=1
	and a.id = $adId

union

select 
	a.id as ad_id, pcm.id as pcm_id, pcm.category_mapping_id as cm_id, pcm.publication_id 
from ads a 
	join ads_parameters ap on (ap.ad_id = a.id)
		join parameters p on (p.id = ap.parameter_id)
			join parameters_types pt on (pt.id = p.type_id and pt.accept_dictionary = 1)
		join n_publications_categories_mappings pcm on ( 
			pcm.mapped_category_id = a.category_id
			and pcm.mapped_location_id is not null 
			and pcm.mapped_dictionary_id is not null
			/*and pcm.mapped_dictionary_id = cast(ap.value as UNSIGNED)*/
			and pcm.mapped_dictionary_id = ap.n_dictionary_id
			and pcm.mapped_location_id in (
				a.country_id, a.county_id, a.municipality_id, a.city_id
			)
		)
where 1=1
	and a.id = $adId

) a_pcm		
		
		
		";
		$rs = Nava::sqlToArray($txtSql);
		$sqlWhere = "-1 ";
		
		$result = null;
		if ($rs){
			foreach ($rs as $row){
				$sqlWhere .= ", ".$row['cm_id'];
			}
			$result = \Baseapp\Suva\Models\CategoriesMappings::find("id in ( $sqlWhere )");
		}
		return $result;

		
	}
	
		
}