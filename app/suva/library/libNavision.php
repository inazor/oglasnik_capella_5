<?php

namespace Baseapp\Suva\Library;


use Baseapp\Suva\Library\Nava;

/* POMOĆNI SQL_OVI
SQLOVI POMOĆNI
select * from [Oglasnik d_o_o_$AVUS-Header] where HEADERID = 1706997;
select * from [Oglasnik d_o_o_$AVUS-Invoice] where HEADERID = 1706997;
select * from [Oglasnik d_o_o_$AVUS-InvoiceRow] where HEADERID = 1706997;
select * from [Oglasnik d_o_o_$AVUS-Payment] where HEADERID = 1706997;
select * from [Oglasnik d_o_o_$AVUS-Revenue] where HEADERID = 1706997;
select * from [Oglasnik d_o_o_$AVUS-Address] where HEADERID = 1706997;



	online - duration = broj dana
	istaknuti-online-bez i premium-bez nemaju mogućnost objave na naslovnici.

	osnovni				 nav-id: 31171
	istaknuti-online	 nav-id: 30046
	istaknuti-online-bez nav-id: 30046
	premium				 nav-id: 30136
	premium-bez			 nav-id: 30136
	pinky 				 nav-id:

	pushup 				 nav-id: 31172
	izlog				 nav-id: 690
	naslovnica			 nav-id: 31173

	offline-duration =	broj objava

	mali-privatni		nav-id: 30102
	mali-poslovni		nav-id: 30105
	mali-foto		nav-id: 30138
	mali-bold		nav-id: 31131
	istaknuti-offline	nav-id: 30135
	istaknuti-boja		nav-id: 31113


	update n_products set id_resursa_nav = '30136' where is_online_product = 1;
	update n_products set id_resursa_nav = '30107' where is_online_product = 0;
	update n_products set id_resursa_nav = '31171' where old_product_id = 'osnovni';
	update n_products set id_resursa_nav = '30046' where old_product_id = 'istaknuti-online';
	update n_products set id_resursa_nav = '30046' where old_product_id = 'istaknuti-online-bez';
	update n_products set id_resursa_nav = '30136' where old_product_id = 'premium';
	update n_products set id_resursa_nav = '30136' where old_product_id = 'premium-bez';
	update n_products set id_resursa_nav = '31172' where old_product_id = 'pushup';
	update n_products set id_resursa_nav = '30103' where old_product_id = 'mali-privatni';
	update n_products set id_resursa_nav = '30105' where old_product_id = 'mali-poslovni';
	update n_products set id_resursa_nav = '30138' where old_product_id = 'mali-foto';
	update n_products set id_resursa_nav = '31131' where old_product_id = 'mali-bold';
	update n_products set id_resursa_nav = '30135' where old_product_id = 'istaknuti-offline';
	update n_products set id_resursa_nav = '31113' where old_product_id = 'istaknuti-boja';
	update n_products set id_resursa_nav = '690' where old_product_id = 'izlog';
	
	// Ivana Arežina: 12.12.2016
	update n_products set id_resursa_nav = id;
	*/


class libNavision {
	
	static $dbNav = null;
	static $navCompanyName = 'Oglasnik d_o_o_$';

	/* 
		funkcija za dohvat računa u jsonu
		
		funkcija za slanje na fiskalizaciju
	
	*/

	public static function sendUnfiscalizedOrdersToNav(){
		$orders = \Baseapp\Suva\Models\Orders::find ("n_status in (4,5) and n_nav_sync_status = 'unsynced'");
		
		foreach (  $orders as $order )
			self::insertOrderInNav( $order );
	}
	
	public static function send_order_to_nav($orderId){
		// po čemu se razlikuje ova funkcija od prethodne?
		
		if($orderId > 0){
			$order = \Baseapp\Suva\Models\Orders::findFirst($orderId);
			$method = \Baseapp\Suva\Models\PaymentMethods::findFirst($order->n_payment_methods_id);
		}
		
		//AKO PREBACUJEMO U PLACENO, TREBA PROVJERIT DA LI JE TAJ RACUN VEC POSLAN U NAVISION 9.1.2017
		//VEC POSTOJI? LINIJA 116
		

		$orders = \Baseapp\Suva\Models\Orders::find("id = $orderId");

		foreach($orders as $order)
			\Baseapp\Suva\Library\libNavision::insertOrderInNav( $order );
		
		$order->n_nav_sync_status;
		$order->update();

	}
	
	
	public static function test_order_before_sending_to_nav($pOrderId){
		/*
			Provjera ima li order na sebi operatera
			Provjera ima li operater n_nav_department_id i n_oznaka_operatera
		*/
		$order = $pOrderId > 0 ? \Baseapp\Suva\Models\Orders::findFirst( $pOrderId ) : null;
		$operater = $order && $order->n_operator_id > 0 ? \Baseapp\Suva\Models\Users::findFirst($order->n_operator_id) : null;
			
			
		if  ( ! $order  ){
			Nava::greska ("Nije pronađen order $pOrderId");
		}
		elseif( $order && ! $operater ){
			Nava::greska ("Na orderu $order->id nije pronađen operater ");
		}
		elseif ( $operater && strlen($operater->n_nav_department_id)  == 0 ) {
			Nava::greska ("Na orderu $order->id, za operatera $operater->id, $operater->username nije naveden n_nav_department_id ");
		}
		elseif ( $operater && strlen($operater->n_oznaka_operatera)  == 0 ) {
			Nava::greska ("Na orderu $order->id, za operatera $operater->id, $operater->username nije naveden n_oznaka_operatera ");
		}
		else return true;
		
		return false;
	}
	
	public static function insertOrderInNav($pOrder){
		//glavna funkcija za generiranje SQL-a koji ubacuje order u NAV
	
	$racun = $pOrder;
	$navTablePrefix = 'Oglasnik d_o_o_$'; 
	$user = $racun->User();
	$operator = $racun->Operator();
	$salesrep = $racun->SalesRep();
	$fiscalLocation = $racun->FiscalLocation();

	//treba javit grešku??
	if(  ! self::test_order_before_sending_to_nav($racun->id) ){
		return false;
	}

	$method = $racun->PaymentMethod();
	if ($method){
		$creditcardid = $method->nav_credit_card_id;
		$paymentformid = $method->nav_payment_type_id;
	}
	else{
		$creditcardid = 9999;
		$paymentformid = 9999;
	}
		
	//je li račun već postoji?
	if ( strlen($racun->n_pbo) > 5 ){
		$navTableName = self::$navCompanyName."AVUS-Invoice";
		$invoiceNumber = $racun->n_pbo;
		$txtSql = "select * from [$navTableName] where INVOICENUMBER = '$invoiceNumber'";
		$rsExisting = self::sqlToArray( $txtSql );
		if ( $rsExisting ) {
			Nava::poruka("Račun s PBO: $invoiceNumber je već upisan u NAV!");
			return;
		}
	}
	else{
		Nava::greska("Račun $racun->id nema PBO");
		return;
	}

	//ako nema invoice s PBO iz računa, dodaje se novi HEADERID za NAV tablice
	$newId = self::getNewHeaderId();
	Nava::sysMessage("New Header ID = $newId");

	$isCompany = ( $user->type == 1 ? false : true );
	if ( $isCompany ) {
		$customernumber = $racun->user_id;
		$taxnumber = $user->n_vat_number > '' ? $user->n_vat_number : $user->oib; 	
		$first_name = $user->company_name;
		$last_name = "";
	} 
	else {
		$customernumber = 9999999;
		$taxnumber = 9999999;
		$first_name = $user->first_name;
		$last_name = $user->last_name;
	}
	

	/*HEADERID*/$headerid = $newId;
	/*CUSTOMERKINDID*/$customerkindid = 0;
	/*CUSTOMERCLASSID*/$customerclassid = 0;
	/*CREATEDBY*/$createdby = 0;
	/*CREATEDAT*/
	$originalDate = $racun->modified_at;
	$createdat = date("Y-m-d", strtotime($originalDate));

	
	/*TAXCOUNTRYCODE*/
	$location = \Baseapp\Suva\Models\Locations::findFirst($user->country_id);
	$taxcountrycode = $location->iso_code > ' ' ? $location->iso_code : ''; 
	
		
	/*CREDITLIMIT*/$creditlimit = '0.00';
	/*DAYSTOPAYTHEINVOICE*/$daystopaytheinvoice = 0;
	/*CASHDISCOUNTPERCENTAGE*/$cashdiscountpercentage = '0.00';
	/*INVOICEFORMID*/$invoiceformid = 1;
	/*CUSTOMERINVOICETYPEID*/$customerinvoicetypeid = 1;
	/*ACCOUNTANCYSTATEID*/$accountancystateid = 0;
	/*ACCOUNTINGMODIFY*/$accountingmodify = 0;
	/*BRANCHGROUPID*/$branchgroupid = 0;
	/*CURRENCYID*/$currencyid = 0;
	/*AMOUNTTOPAY*/$amounttopay = $racun->n_total_with_tax;
	/*VATID*/$vatid = 0;
	/*VATPERCENTAGE*/$vatpercentage = '25';
	/*VATAMOUNT*/$vatamount = ($amounttopay * 0.25 *0.8);

	/*CUSTOMERNAME*/$customername =  $user->company_name;
	/*INVOICETYPEID*/$invoicetypeid = 0;
	/*DEPARTMENTGROUPID*/$departmentgroupid = 0;
	
	//IGOR:IZMJENA
	///*OFFICENUMBER*/$officenumber = 7041;
	$officenumber = $salesrep->n_nav_department_id > 0 ? $salesrep->n_nav_department_id : 7041;
	
	
	/*OFFERNUMBER*/$offernumber = $racun->n_pbo;

	/*EMail*/$email = "racuni@oglasnik.hr";
	/*SendInoice*/$sendinvoice = 1;
	
	//IGOR:IZMJENA
	///*FiscOperator*/$FiscOperator = 1000;
	$FiscOperator = $operator->n_oznaka_operatera > 0 ? $operator->n_oznaka_operatera : 1000;
	
	
	//IGOR:IZMJENA
	/*FiscLocation*/
	$FiscLocation = strlen( $fiscalLocation->nav_fiscal_location_id ) > 0 ? $fiscalLocation->nav_fiscal_location_id : 'ONL1';
	
	
	//$entrycreationdate = date("Y-m-d");
	$entrycreationdate = '1753-01-01 00:00:00.000';
	
	//$entrycreationtime = date("Y-m-d");
	$entrycreationtime = '1753-01-01 00:00:00.000';
	
	$entrycreationdatetime = date("Y-m-d");
	echo $headerid . ' - ' . $racun->n_pbo . " - ";
	
	$invoicerowcount = count($racun->OrdersItems());


	
	$sql = "
INSERT INTO [Oglasnik d_o_o_".'$'."AVUS-Header] (
	HEADERID
	,CUSTOMERNUMBER
	,CUSTOMERKINDID
	,CUSTOMERCLASSID
	,CREATEDBY
	,CREATEDAT
	,TAXCOUNTRYCODE
	,CREDITLIMIT
	,DAYSTOPAYTHEINVOICE
	,CASHDISCOUNTPERCENTAGE
	,INVOICEFORMID
	,CUSTOMERINVOICETYPEID
	,PAYMENTFORMID
	,ACCOUNTANCYSTATEID
	,ACCOUNTINGMODIFY
	,BRANCHGROUPID
	,CURRENCYID
	,DEBITORACCOUNTNEW 
	,VATID
	,VATPERCENTAGE
	,CUSTOMERNAME
	,TAXNUMBER
	,[Entry Creation Date]
	,[Entry Creation time]
	,[Entry Creation Date_Time]
	,[Processing Error] 
	,[Processing Status]
	,[Ready To Process]
	,STATUS
	,[Error description]
	,[Ready for creation]
	,[Customer No_]
	,[Import ID]
	)
VALUES (
	$headerid
	,'$customernumber'
	, $customerkindid
	, $customerclassid
	, $createdby
	,'$createdat'
	,'$taxcountrycode'
	, $creditlimit
	, $daystopaytheinvoice
	, $cashdiscountpercentage
	, $invoiceformid
	, $customerinvoicetypeid
	, $paymentformid
	, $accountancystateid
	, $accountingmodify
	, $branchgroupid
	, $currencyid
	,''
	, $vatid
	, $vatpercentage
	,'$customername'
	,'$taxnumber'
	,'$entrycreationdate'
	,'$entrycreationtime'
	,'$entrycreationdatetime'
	,0
	,0
	,0
	,0
	,''
	,0
	,''
	,''
	)	
	";
	
	//Nava::poruka($sql);
	self::runSql($sql);


	/*AVUSInvoice*/
	$invoicenumber = $racun->n_pbo;
	$linenoInvoice = self::getNewInvoiceId();
	$processingstatus = 0;
	$readyforcreation = 0;
	$errordescription = '';
	$invoiceno = '';
	$posteddocumentno = '';
	$currencycode = '';
	$paymentmethodcode = '';
	$fiscsubject = 0;
	$fiscwholesaleterminal  = '';
	$fisclocationcode = '';
	$postingdatetime = '1753-01-01 00:00:00.000';
	$issuerprotectioncode = '';
	$uniqueinvoiceno = '';
	$fiscregistered = 0;
	$latedelivery = 0;
	$fiscdocno = '';
	
	
	$sql = "
	INSERT into [Oglasnik d_o_o_".'$'."AVUS-Invoice] (
	HEADERID
	,INVOICENUMBER
	,CUSTOMERNUMBER
	,INVOICETYPEID
	,INVOICEFORMID
	,DEPARTMENTGROUPID
	,CURRENCYID
	,AMOUNTTOPAYPRECASHDISCOUNT
	,CASHDISCOUNTPERCENTAGE
	,CACHDISCOUNTNETAMOUNT
	,CACHDISCOUNTGROSSAMOUNT
	,VATID
	,VATPERCENTAGE
	,VATAMOUNT
	,AMOUNTTOPAY
	,INVOICEDATE
	,SETTLEMENTDATE
	,PAYMENTFORMID
	,INVOICEROWCOUNT
	,OFFICENUMBER
	,TAXCOUNTRYCODE
	,OFFERNUMBER
	,PAYMENTTYPEID
	,CREDITCARDID
	,INVOICENUMBERREFERENCE
	,[E-Mail for Sending Invoice]
	,[Send Invoice]
	,[Printer Name]
	,[Print Invoice]
	,[Fisc_ Operator]
	,[Fisc _Location]
	,[LINENO]
	,[Processing Status]
	,[Ready for creation]
	,[Error description]
	,[Invoice No_]
	,[Posted document No_]
	,[Currency Code]
	,[Payment Method Code]
	,[Fisc_ Subject]
	,[Fisc_ Wholesale Terminal]
	,[Fisc_ Location Code]
	,[Posting DateTime]
	,[Issuer Protection Code]
	,[Unique Invoice No_]
	,[Fisc_ Registered]
	,[Late Delivery]
	,[Fisc_ Doc_ No_]
	) 
VALUES (
	'$headerid'
	,'$invoicenumber'
	,'$customernumber'
	,'$invoicetypeid'
	,'$invoiceformid'
	,'$departmentgroupid'
	,'$currencyid'
	, 0.00
	, 0.00
	, 0.00
	, 0.00
	, 0
	,'$vatpercentage'
	,'$vatamount'
	,'$amounttopay'
	,'$createdat'
	,'$createdat'
	,'$paymentformid'
	,'$invoicerowcount'
	,'$officenumber'
	,'$taxcountrycode'
	,'$offernumber'
	,'$paymentformid'
	,'$creditcardid'
	,''
	,'$email'
	,'$sendinvoice'
	,''
	, 1
	,'$FiscOperator'
	,'$FiscLocation'
	,$linenoInvoice
	,'$processingstatus'
	,$readyforcreation
	,'$errordescription'
	,'$invoiceno'
	,'$posteddocumentno'
	,'$currencycode'
	,'$paymentmethodcode'
	,$fiscsubject
	,'$fiscwholesaleterminal'
	,'$fisclocationcode'
	,'$postingdatetime'
	,'$issuerprotectioncode'
	,'$uniqueinvoiceno'
	,$fiscregistered
	,$latedelivery
	,'$fiscdocno'
	)";
	
	
	//Nava::poruka($sql);
	self::runSql($sql);


	/*AVUSInvoiceRow*/
	$i = 1;
	foreach ($racun->OrdersItems() as $oi) {
		$product = $oi->Product();

		//DESCRIPTION
		if ($product->is_online_product == 1) {
			$description = date ("d.m.y", strtotime( $oi->n_first_published_at ))." - ".date ("d.m.y", strtotime( $oi->n_expires_at )); 
		}
		else {
			// $description = "Broj objava: ".$oi->qty * $product->days_published;
			// $description = "Broj objava: ".$product->days_published;
			$description = "Broj objava: ".$oi->qty;
		}
		
		$amounttopay = $oi->n_total;
		$adformatid = intval($product->id_resursa_nav);
		// error_log('ad-format-id '.$adformatid);
		// error_log(print_r($proizvod,true));
		$objectid = ( $product->Publication() ? $product->Publication()->nav_object_id : 999 );

		//$accountingdate = date("Y-m-d");
		$accountingdate = '1753-01-01 00:00:00.000';
		$revenueamount = $oi->n_total_with_tax;
		
		$linenoRevenue = self::getNewRevenueId();
		/*"PRODUCTNAME": "Online Istaknuti oglas - 5 dana, Objava na naslovnici - 3 dana [Oglas: 70152]",*/
		/*AVUSRevenue*/
		$sql = "
	INSERT INTO [Oglasnik d_o_o_".'$'."AVUS-Revenue] (
		HEADERID
		,INVOICELINENO
		,ROWPOSITION
		,[LINENO]
		,INSERTIONREVENUETYPEID
		,REVENUE_ACCOUNTCODE
		,REVENUE_AMOUNT
		,REVENUESHARE
		,OBJECTID
		,ACCOUNTINGDATE
		,REVERSE
		,INVOICENUMBER
		) 
	VALUES (
		$headerid 
		,$linenoInvoice
		,$i
		,$linenoRevenue
		,0
		,''
		,$revenueamount
		,100.00
		,$objectid 
		,'$accountingdate'
		,0
		,$offernumber)";
		
		//Nava::poruka($sql);
		self::runSql($sql);
		

		/*AVUSInvoiceRow*/
		$priceQty = $oi->n_price * $oi->qty;
		$vatamount = $priceQty * 0.25;
		$netamount = $priceQty ;
		$customernumber = $racun->n_pbo;
		$departmentgroupid = 0;
		$departmentid = 0;
		$adtypeid = 2;
		$adlayoutid = 0;
		$orderclassid = 0;
		$apperancedate = date("Y-m-d", strtotime($originalDate));
		$realapperancedate = date("Y-m-d", strtotime($originalDate));
		$issuenumber = '';
		$orderid = 0;
		$ordernumberstring = '';
		$invoicerowtext = '';
		$classid = 0;
		$classnumber = '';
		$classname = '';
		$unitid = 0;
		$height = 0.00;
		$width = 0.00;
		$adrecordid = 0;
		$amounttopay = $oi->n_total;
		$vatamount = $amounttopay * 0.25;
		$invoicelineno = 0;
		
		$sql = "
		INSERT INTO [Oglasnik d_o_o_".'$'."AVUS-InvoiceRow] (
		HEADERID
		,INVOICELINENO
		,ROWPOSITION
		,CUSTOMERNUMBER
		,OBJECTID
		,DEPARTMENTGROUPID
		,DEPARTMENTID
		,ADTYPEID
		,ADFORMATID
		,ADLAYOUTID
		,ORDERCLASSID
		,APPEARANCEDATE
		,REALAPPEARANCEDATE
		,ISSUENUMBER
		,ORDERID
		,ORDERNUMBERSTRING
		,INVOICEROWTEXT
		,CLASSID
		,CLASSNUMBER
		,CLASSNAME
		,UNITID
		,HEIGHT
		,WIDTH
		,ADRECORDID
		,OFFERNUMBER
		,COLUMNS
		,RATECARDPRICEUNITID
		,BASICPRICE
		,SINGLEPRICE
		,FACTOR
		,NETAMOUNT
		,GROSSAMOUNT
		,VATID
		,VATPERCENTAGE
		,VATAMOUNT
		,AMOUNTTOPAY
		,CUSTOMERCLASSID
		,PAYMENTCLASSID
		,PAYMENTSTATEID
		,AGENCYCOMMISSIONAMOUNT
		,SALESMANCOMMISSIONAMOUNT
		,REVENUEAMOUNT
		,ISSUEACCOUNTINGDATE
		,PAYMENTFORMID
		,NETTOFLAG
		,OFFICESALESMANNUMBER
		,SALESMANSHORTNAME
		,PLACEMENTCOLORID
		,ORDERPACKAGEID
		,TOGETHERIDX
		,TOGETHEREXT
		,INVOICENUMBER
		,OFFICENUMBER
		,PARTNERSHIPCOMMISSION
		,OFFERINVOICENUMBER
		,ROWPOSITIONREFERENCE
		,[Description 2]
		,[Ready for creation]
		,[Error description]
		,[Resource No_]
		) 
		VALUES (
		$headerid 
		,$linenoInvoice
		,$i
		,'$customernumber'
		,$objectid
		,$departmentgroupid
		,$departmentid
		,$adtypeid
		,$adformatid
		,$adlayoutid
		,$orderclassid
		,'$apperancedate'
		,'$realapperancedate'
		,'$issuenumber'
		,$orderid
		,'$ordernumberstring'
		,'$invoicerowtext'
		,$classid
		,'$classnumber'
		,'$classname'
		,$unitid
		,$height
		,$width
		,$adrecordid
		,'$offernumber'
		,0.00
		,0
		,0.00
		,0.00
		,1
		,$amounttopay
		,0.00
		,0
		,$vatpercentage
		,$vatamount 
		,$amounttopay
		,0
		,0
		,0
		,0.00
		,0.00
		,0.00
		,'1999-12-31'
		,$paymentformid
		,0
		,0
		,''
		,0
		,0
		,0
		,0
		,$offernumber
		,'$officenumber'
		,0.00
		,'$offernumber'
		,0
		,'$description'
		,0
		,''
		,''
		)
		";
		
		//Nava::poruka($sql);
		self::runSql($sql);
		
		
		/*AVUSPayment*/
		$accountingdate = '1753-01-01 00:00:00.000';
		$lineno = self::getNewPaymentId();
		$paymentid = 1;
		$paymentamount = $oi->n_total_with_tax;
		
		$sql = "
		INSERT INTO [Oglasnik d_o_o_".'$'."AVUS-Payment] (
		HEADERID
		,[INVOICELINENO]
		,ROWPOSITION
		,[LINENO]
		,PAYMENTID
		,PAYMENTAMOUNT
		,PAYMENTFORMID
		, PAYMENTSOURCEID
		,PAYMENTDESTINATIONID
		,PAYMENTACCOUNTCODE
		,ACCOUNTINGDATE
		,INVOICENUMBER
		,CREDITCARDID
		,PAYMENTTYPEID
		,[Payment Method Code]
		,[Ready for creation]
		,[Error description]
		,[aifra kupca-kartiYar]
		,[Processing Status]
		) 
		VALUES (
		$headerid
		,$linenoInvoice
		,$i
		,$lineno
		,$paymentid
		,$paymentamount
		,$paymentformid
		,1
		,1
		,''
		,'$accountingdate'
		,'$offernumber'
		,$creditcardid
		,$paymentformid
		,''
		,0
		,''
		,''
		,0	
		)
		";
		
		// Nava::poruka($sql);
		self::runSql($sql);
		
		$i++;
	}
	
	/*AVUSAddress*/
	$street = $user->address;
	$zipcode = $user->zip_code;
	$city = $user->city;
	$telareacode = '0';
	$telephonenumber = $user->phone1;
	$linenoAddress = self::getNewAddressId();
	
	$sql = "
	INSERT INTO [Oglasnik d_o_o_".'$'."AVUS-Address] (
	HEADERID
	,[LINENO]
	,FORMOF
	,FIRST_NAME
	,LAST_NAME
	,STREET
	,ZIPCODE
	,CITY
	,TELCOUNTRYCODE
	,TELAREACODE
	,TELEPHONENUMBER)
	VALUES ( 
	$headerid
	, $linenoAddress
	, 0 
	,'$first_name'
	,'$last_name'
	,'$street'
	,'$zipcode'
	,'$city'
	,'385'
	,'$telareacode'
	,'$telephonenumber'
	)
	";
	
	//Nava::poruka($sql);
	self::runSql($sql);
	

	$sql = 'UPDATE [Oglasnik d_o_o_$AVUS-Header] SET [Ready To Process]= 1 , [Processing Error] = 1 WHERE HEADERID='.$headerid;
	// Nava::poruka($sql);
	self::runSql($sql);

	//n_nav_sync_status u order	
	Nava::runSql("update orders set n_nav_sync_status = 'sent', n_nav_header_id = $headerid where id = ".$racun->id);
	
} 
	
	public static function removeOrderFromNav(){
		
		$rs = self::sqlToArray("select max(HEADERID) as id from [Oglasnik d_o_o_".'$'."AVUS-Header]");
		$maxId = $rs[0]['id'];
		
		self::runSql("delete from [Oglasnik d_o_o_".'$'."AVUS-Header] where HEADERID = $maxId");
		self::runSql("delete from [Oglasnik d_o_o_".'$'."AVUS-Invoice] where HEADERID = $maxId");
		self::runSql("delete from [Oglasnik d_o_o_".'$'."AVUS-InvoiceRow] where HEADERID = $maxId");
		self::runSql("delete from [Oglasnik d_o_o_".'$'."AVUS-Payment] where HEADERID = $maxId");
		self::runSql("delete from [Oglasnik d_o_o_".'$'."AVUS-Revenue] where HEADERID = $maxId");
		self::runSql("delete from [Oglasnik d_o_o_".'$'."AVUS-Address] where HEADERID = $maxId");
		
		Nava::poruka("Račun izbrisan");
		
	}
	
	//pomoćne funkcije za generiranje SQL-a kojim se šalje u Nav
	public static function getNewHeaderId(){

		$navTableName = self::$navCompanyName."AVUS-Header";
		$rs = self::sqlToArray("select max (HEADERID) as id from [$navTableName]");
		return $rs[0]['id'] + 1;

	}

	public static function getNewInvoiceId(){

		$navTableName = self::$navCompanyName."AVUS-Invoice";
		$rs = self::sqlToArray("select max([LINENO]) as id from [$navTableName]");
		return $rs[0]['id'] + 1;

	}

	public static function getNewRevenueId(){

		$navTableName = self::$navCompanyName."AVUS-Revenue";
		$rs = self::sqlToArray("select max([LINENO]) as id from [$navTableName]");
		return $rs[0]['id'] + 1;

	}

	public static function getNewAddressId(){

		$navTableName = self::$navCompanyName."AVUS-Address";
		$rs = self::sqlToArray("select max([LINENO]) as id from [$navTableName]");
		return $rs[0]['id'] + 1;

	}

	public static function getNewPaymentId(){

		$navTableName = self::$navCompanyName."AVUS-Payment";
		$rs = self::sqlToArray("select max([LINENO]) as id from [$navTableName]");
		return $rs[0]['id'] + 1;

	}
	
	
	//NAV FUNKCIJE

	public static function getJirZikForOrder ( $pOrderId ){
		$order =\Baseapp\Suva\Models\Orders::findFirst($pOrderId);
		// error_log('getJirZikForOrder 1');
		if (!$order){
			Nava::greska("Order Nr. $pOrderId  not found");
			// error_log('getJirZikForOrder 2');
			return false;
		}
		
		//$strlen = strlen((string)$pOrderId);
		
		// $txtSql = 	" select 
				// [Issuer Protection Code]
				// , [Unique Invoice No_]
				// , [Invoice No_]
				// , [INVOICENUMBER]
				// from [Oglasnik d_o_o_".'$'."AVUS-Invoice] 
				// where 1=1 
				// and [INVOICENUMBER] = '$order->n_pbo'";
				
		$txtSql ="
		select 
				[Fisc_ Location Code]
				,[Fisc_ Doc_ No_]
				,[Fisc_ Wholesale Terminal]
				,[Issuer Protection Code]
				,[Unique Invoice No_]
				from [Oglasnik d_o_o_".'$'."Sales Invoice Header] 
				where 1=1 
				and [No_] = '$order->n_pbo'

		";
		
		//spojit u jedan
		// Nava::poruka($txtSql);
		$rs = self::sqlToArray($txtSql);
		// error_log(print_r($rs));
		// error_log(print_r($rs,true));
		
		$broj = count($rs);
		if ($broj == 0){
			// error_log('getJirZikForOrder 3');
			Nava::sysMessage ("U Navisionu nije pronađena faktura s brojem $order->n_pbo");
			return false;
		}
		elseif ( $broj > 1 ){
			// error_log('getJirZikForOrder 4');
			Nava::greska ("U Navisionu je pronađeno $broj faktura s brojem $order->pbo");
			return false;
		}
		else{
			// error_log('getJirZikForOrder 5');
			return $rs;
		}
	}
	
public static function waitForFiscalization( $pOrderId = null ){
	/*
		čekanje nekoliko sekundi da Navision fiskalizira račun i pokušaj dohvata JIR-a i ZIK-a
	
	*/
	sleep(1);
	// error_log('waitForFiscalization 1');
	for ($i=1; $i<= 2; $i++){
		$result = self::getJirZikForOrder ($pOrderId);
		if ( $result['Fisc_ Doc_ No_'] > '' ){
			// error_log('waitForFiscalization 2');
			return $result;
		}
		sleep(1);
	}
	// error_log('waitForFiscalization 3');
	return false;
	
}

public static function testFiscalization( $pOrderId = null ){
	/*
		provjerava postoji li u Navisionu invoice no, JIR i ZKI
		Ako postoji sve, onda stavlja u syced
		ako ne postoji JIR i ZKI onda provjerava je li to dopušteno (payment method->is_fiscalized)
			ako je dopušteno, onda synca
		u suprotnom stavlja u status retry
	
	*/
	
	if (!$pOrderId){
		return Nava::sysError("testFiscalization::orderid not supplied");
	}
	$order = \Baseapp\Suva\Models\Orders::findFirst($pOrderId);
	if (!$order){
		return Nava::sysError("testFiscalization:: order $pOrderId not found");
	}
	if (!$order->n_status == 5 && !$order->n_status == 4){
		return Nava::sysError("testFiscalization:: order $pOrderId has disallowed status $order->n_status");
	}
	
	$navResponse = self::getJirZikForOrder($pOrderId);

	if ($navResponse){
		Nava::poruka('Navision fiskalizacija');
		$jir = $navResponse[0]['Unique Invoice No_'] > '' ? $navResponse[0]['Unique Invoice No_'] : null;
		$zik = $navResponse[0]['Issuer Protection Code'] > '' ? $navResponse[0]['Issuer Protection Code'] : null;
		//$invoiceNo = $navResponse[0]['Invoice No_'] > '' ? $navResponse[0]['Invoice No_'] : null;
		
		//5.1.2017 - Ivana - drugi broj računa iz Navisiona
		$invoiceNo = $navResponse[0]['Fisc_ Doc_ No_']."-".$navResponse[0]['Fisc_ Location Code']."-".$navResponse[0]['Fisc_ Wholesale Terminal'];
		
		$isFiscalized = $order->PaymentMethod()->is_fiscalized == 1 ? true : false;
		if ($isFiscalized && $jir && $zik && $invoiceNo){
			//ako račun treba fiskalizrati, proknjižen je i dima JIR i ZKI
			$order->n_invoice_zik = $zik;
			$order->n_invoice_jir = $jir;
			$order->n_invoice_fiscal_number = $invoiceNo;
			$order->n_nav_sync_status = 'synced';
			$order->update();
			Nava::poruka("Račun je uspješno fiskaliziran: Order:$order->id");
		}
		elseif ( !$isFiscalized && $invoiceNo ){
			//ako račun ne treba fiskalizirati i proknjižen je
			$order->n_invoice_fiscal_number = $invoiceNo;
			$order->n_nav_sync_status = 'synced';
			$order->update();
			Nava::poruka("Račun je uspješno fiskaliziran: Order:$order->id");
		}
		else{
			//ako nešto ne štima, stavlja ga se na "retry" i kasnije se opet pokušava
			$order->n_nav_sync_status = 'retry';
			$order->update();
			Nava::sysMessage ("Greška pri fiskalizaciji: Order:$order->id, Is Fiscalized: $isFiscalized, Invoice No: $invoiceNo, JIR: $jir, ZKI: $zik");
		}
	}else{
		$order->n_nav_sync_status = 'retry';
		$order->update();
		Nava::greska ("Greška pri fiskalizaciji: Order:$order->id");
	}
}
	


	//MS SQL FUNKCIJE
	public static function fConnectNav (){

		//NAVISION SQL SERVER POSTAVKE
		$navHost = "195.245.255.22";
		$navPort = "1433";
		$navDb = "OGLASNIK_TEST_NEW";
		$navUser = "sa";
		$navPassword = "Tassadar1337";		

		try {
			
			// error_log("Connecting to $navHost ..");
			$db = new \PDO("dblib:host=$navHost;port=$navPort;dbname=$navDb", $navUser, $navPassword );
			$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);// set the PDO error mode to exception 
			// error_log("Connected to $navHost successfully"); 
			return $db;
		}
		catch(\PDOException $e){ 
			error_log("SQL failed: " . $e->getMessage());
		}
	}

	public static function sqlToArray ( $pSql){
		self::$dbNav = self::$dbNav ? self::$dbNav : self::fConnectNav();
		//$dbNav = self::fConnectNav();
		// $this->poruka($pSql);
		$rs = self::$dbNav->query($pSql);
		$result = array();
		foreach ($rs as $row)
			array_push ($result, $row);
		return $result;
	}

	public static function runSql ( $pSql ){
		try {
			self::$dbNav = self::$dbNav ? self::$dbNav : fConnectNav();
			self::$dbNav->query($pSql);
			Nava::sysMessage($pSql);
		}
		catch(\PDOException $e){ 
			error_log("SQL failed: " . $e->getMessage());
			Nava::greska("SQL failed: " . $e->getMessage());
		}
	}
	
	
}