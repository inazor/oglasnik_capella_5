<?php

namespace Baseapp\Suva\Library;


use Baseapp\Suva\Library\Nava;
use Baseapp\Suva\Library\libAppSetting;

class libDTP {
	 
	//greške ERR100 - ERR199
	public static $arrCatMaps = array();
	public static $rsAds = array();

	//folderi koji se koriste u createXmlForDtp i r_catToXml
	public static $processedFolder = '';
	public static $mediaFolder = '';
	
	
	public static $dtp_indesignRootFolder = '';
	public static $dtp_categoryEpsRootFolder = '';
	
	public static function createXmlForDtpNEWNEW ( $pIssueId = -1 , $pDtpGroupId = null, $user = null ){

		//varijable iz parametara
		$issue = \Baseapp\Suva\Models\Issues::findFirst($pIssueId);
		if(!$issue) return Nava::poruka("Nije pronaden issue sa brojem  $issueId",  -1);

		$dtpGroup = $pDtpGroupId > 0 ? \Baseapp\Suva\Models\DtpGroups::findFirst( $pDtpGroupId ) : null;
		
		
		$appExecutionTime = ini_get('max_execution_time');
		ini_set('max_execution_time', 150000);
		ini_set ('request_terminate_timeout', 150000);

		$publication = $issue->Publication();
		

		{// DEFINIRANJE VARIJABLI ZA FOLDERE

		//pomoćna varijabla
		$issueName = $issue->Publication()->slug."_".date("Y-m-d", strtotime($issue->date_published));
		
		//kreiranje foldera za issue u dtp: OGLT_1016-1-1_GR1
		$issueFolder = "repository/dtp/issues/$issueName";
		if ( self::createFolderIfNotExist( $issueFolder ) === -1 ) { return -1; }
		
		//TEMP folder za spremanje fileova koji će se zipati
		$txtUser = $user && $user->username ? $user->username."/" : "";
		$zipFolder = "repository/dtp/".$txtUser."ZIP_TMP";
		$mediaFolder = "repository/dtp/".$txtUser."ZIP_TMP/media";
		self::$mediaFolder = $mediaFolder;
		
		if ( self::createFolderIfNotExist( "repository/dtp/".$txtUser ) === -1 ) { return -1; }
		
		
		if ( self::deleteFolderIfExist ( $zipFolder ) === -1 ) { return -1; }
		if ( self::createFolderIfNotExist( $zipFolder ) === -1 ) { return -1; }
		if ( self::deleteFolderIfExist ( $mediaFolder ) === -1 ) { return -1; }
		if ( self::createFolderIfNotExist( $mediaFolder ) === -1 ) { return -1; }
		
		
		//folder processed - po novome KZ-002
		$processedFolder = "repository/dtp/processed/$publication->slug/";
		self::$processedFolder = $processedFolder;
		if ( self::createFolderIfNotExist( self::$processedFolder ) === -1 ) { 
			Nava::greska ("Folder ".self::$processedFolder." nije moguće kreirati.");
			return -1; 
		}
	
		//folder i file u koji će se spremati XML
		$xmlFilePath = "repository/dtp/".$txtUser."ZIP_TMP/export.xml";
		Nava::poruka("Adding XML to file <a href = '/$xmlFilePath' target = '_blank'>$xmlFilePath</a> ");
		
		//folder i file u koji će se spremiti zip
		$issueName = $issue->Publication()->slug."_".date("Y-m-d", strtotime($issue->date_published));
		$dtpGroupName = $dtpGroup ? "_$dtpGroup->name": "";
		$zipFilePath = "repository/dtp/issues/$issueName/$issueName$dtpGroupName.zip";
		
		
		$_SESSION['_context']['dtp_zip_file_url'] = $zipFilePath;

	
		//folder na koji će pokazivati fileovi u XML-u (file:////)
		self::$dtp_indesignRootFolder = libAppSetting::get('dtp_indesignRootFolder');
		Nava::poruka ("FOLDER ZA DOHVAT SLIKA U INDESIGNU <b>".self::$dtp_indesignRootFolder."</b>"  );

		}

		//GLAVNI DIO
		$rootCategoryMappings = \Baseapp\Suva\Models\CategoriesMappings::find("parent_id is null and n_publications_id = $publication->id and active = 1");
		if ( $rootCategoryMappings->count() != 1) {
			return Nava::poruka( count( $rootCategoryMappings)." root categories for publication $publication->name found! There is an error in data!", -1);
		}
		$rootCategoryMapping = \Baseapp\Suva\Models\CategoriesMappings::findFirst("parent_id is null and active = 1 and n_publications_id = $publication->id");
		if ( !$rootCategoryMapping ) {
			return Nava::poruka("Root category for publication $publication->name not found", -1);
		}

		
		//ARRAY ZA UBRZANJE GENERIRANJA
		self::fillArrCategoriesNEWNEW($issue, $dtpGroup);
		//Nava::printr(self::$arrCatMaps);
		// return;

		//GLAVNA PETLJA ZA GENERIRANJE XML-a
		Nava::poruka("ReportsController::offLineIssueXmlAction: Processing root category $rootCategoryMapping->id");
		$xml = "";
		foreach ( self::$arrCatMaps[ $rootCategoryMapping->id ]['child_cat_ids'] as $childCatMapId ){
			//ako je zadana dtp grupa onda provjerava ima li dijete tu dtp grupu, a ako nije zadana onda po redu
			//Nava::poruka($childCatMapId);
			if ( ($dtpGroup && self::$arrCatMaps[ $childCatMapId ]['group_id'] == $dtpGroup->id) || !$dtpGroup  ){
				
				$xml .= self::r_catToXmlNEWNEW( $childCatMapId );
			}
		}
		
		{//DODATNE OBRADE XML-a NAKON GENERIRANJA
		//09.09.2016 Hajnal - dodati čvor root i zaglavlje
		$xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'.PHP_EOL."<Root xmlns:aid='http://ns.adobe.com/AdobeInDesign/4.0/'>".$xml.PHP_EOL."</Root>";
		
		
		
		
		//MICANJE SPECIJALNIH KARAKTERA, NON PRINTABLE
		$xml = preg_replace ('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $xml);
		
		 // Match Emoticons
		$regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
		$xml = preg_replace($regexEmoticons, '', $xml);

		// Match Miscellaneous Symbols and Pictographs
		$regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
		$xml = preg_replace($regexSymbols, '', $xml);

		// Match Transport And Map Symbols
		$regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
		$xml = preg_replace($regexTransport, '', $xml);

		// Match Miscellaneous Symbols
		$regexMisc = '/[\x{2600}-\x{26FF}]/u';
		$xml = preg_replace($regexMisc, '', $xml);

		// Match Dingbats
		$regexDingbats = '/[\x{2700}-\x{27BF}]/u';
		$xml = preg_replace($regexDingbats, '', $xml);
		}

		
		{//spremanje u fileove i ZIP-anje
	
			//spremanje xml-a u file
			file_put_contents($xmlFilePath, $xml);
			Nava::poruka ("Created XML file <a href='/$xmlFilePath' target = '_blank'>$xmlFilePath</a>");
			
			
			//kreiranje zip filea iz TEMP foldera
			unlink($zipFilePath);
			try{
				$zip = \Comodojo\Zip\Zip::create( $zipFilePath );
				$zip->add( $xmlFilePath );
				Nava::poruka ("Adding images from <a href='/$mediaFolder' target = '_blank'>$mediaFolder</a> to file  <a href='/$zipFilePath' target = '_blank'>$zipFilePath</a> ...");
				$zip->add( $mediaFolder );		
			}
			catch (\Exception $e){
				Nava::greska("Zipping Error:".$e->getMessage());
			}
		
		}
		ini_set('max_execution_time', $appExecutionTime);
		ini_set('request_terminate_timeout', $appExecutionTime);
		
		return $xml;
		
		
	}

	public static function fillArrCategoriesNEWNEW(  \Baseapp\Suva\Models\Issues $issue, $pDtpGroupId = null ){
	
	$issueId = $issue->id;
	$publicationId = $issue->publications_id;
	$groupId = $pDtpGroupId;
	
	{ //sql stari	
		$txtSql = "
select 
	aaa.*
	,cmaa.parent_id
	,cmaa.sort_order as cm_sort_order
	,cmaa.n_path
	,cmaa.level as cm_level
	,cmaa.dtp_group_id
	,a.user_id
	,a.n_avus_order_id
	,a.description_offline
	,u.username
	,a.n_positioning_desc
	,(case when ps.id is null then pa.id else ps.id end) as product_id
	,(case when ps.id is null then pa.name else ps.name end) as product_name
	,(case when ps.id is null then pa.display_ad_width else ps.display_ad_width end) as display_ad_width
	,(case when ps.id is null then pa.display_ad_height else ps.display_ad_height end) as display_ad_height
	,(case when ps.id is null then pa.layouts_id else ps.layouts_id end) as layouts_id
	,(case when ps.id is null then pa.dtp_hashtag_before_contact_info else ps.dtp_hashtag_before_contact_info end) as dtp_hashtag_before_contact_info
	,(case when ps.id is null then pa.is_display_ad else ps.is_display_ad end) as is_display_ad
	,(case when ps.id is null then pa.is_picture_required else ps.is_picture_required end) as is_picture_required
	,(case when cmaa.level = 1 then cmaa.id when cm1.level = 1 then cm1.id when cm2.level = 1 then cm2.id when cm3.level = 1 then cm3.id when cm4.level = 1 then cm4.id else null end ) as lvl1	
	,(case when cmaa.level = 2 then cmaa.id when cm1.level = 2 then cm1.id when cm2.level = 2 then cm2.id when cm3.level = 2 then cm3.id when cm4.level = 2 then cm4.id else null end ) as lvl2	
	,(case when cmaa.level = 3 then cmaa.id when cm1.level = 3 then cm1.id when cm2.level = 3 then cm2.id when cm3.level = 3 then cm3.id when cm4.level = 3 then cm4.id else null end ) as lvl3	
	,(case when cmaa.level = 4 then cmaa.id when cm1.level = 4 then cm1.id when cm2.level = 4 then cm2.id when cm3.level = 4 then cm3.id when cm4.level = 4 then cm4.id else null end ) as lvl4	
	,(case when cmaa.level = 5 then cmaa.id when cm1.level = 5 then cm1.id when cm2.level = 5 then cm2.id when cm3.level = 5 then cm3.id when cm4.level = 5 then cm4.id else null end ) as lvl5	
	,(case when cmaa.level = 1 then cmaa.sort_order when cm1.level = 1 then cm1.sort_order when cm2.level = 1 then cm2.sort_order when cm3.level = 1 then cm3.sort_order when cm4.level = 1 then cm4.sort_order else null end ) as sort1	
	,(case when cmaa.level = 2 then cmaa.sort_order when cm1.level = 2 then cm1.sort_order when cm2.level = 2 then cm2.sort_order when cm3.level = 2 then cm3.sort_order when cm4.level = 2 then cm4.sort_order else null end ) as sort2	
	,(case when cmaa.level = 3 then cmaa.sort_order when cm1.level = 3 then cm1.sort_order when cm2.level = 3 then cm2.sort_order when cm3.level = 3 then cm3.sort_order when cm4.level = 3 then cm4.sort_order else null end ) as sort3	
	,(case when cmaa.level = 4 then cmaa.sort_order when cm1.level = 4 then cm1.sort_order when cm2.level = 4 then cm2.sort_order when cm3.level = 4 then cm3.sort_order when cm4.level = 4 then cm4.sort_order else null end ) as sort4	
	,(case when cmaa.level = 5 then cmaa.sort_order when cm1.level = 5 then cm1.sort_order when cm2.level = 5 then cm2.sort_order when cm3.level = 5 then cm3.sort_order when cm4.level = 5 then cm4.sort_order else null end ) as sort5	

from (
	select 
		 a_pcm.ad_id , a_pcm.cm_id, 'suva' as source
	from 
	(
	
	
	select 
		a.id as ad_id, pcm.id as pcm_id, pcm.category_mapping_id as cm_id
	from ads a 
		join ads_parameters ap on (ap.ad_id = a.id)
			join parameters p on (p.id = ap.parameter_id)
				join parameters_types pt on (pt.id = p.type_id and pt.accept_dictionary = 1)
			join n_publications_categories_mappings pcm on ( 
				pcm.mapped_category_id = a.category_id
				 /*and pcm.category_mapping_id = 5041*/
				and pcm.publication_id = $publicationId
				and pcm.mapped_location_id is null 
				and pcm.mapped_dictionary_id is not null
				and pcm.mapped_dictionary_id = cast(ap.value as UNSIGNED)
				)
	where 1=1
		 and a.moderation = 'ok' 
	union
	
	select 
		a.id as ad_id, pcm.id as pcm_id, pcm.category_mapping_id as cm_id
	from
		ads a
			join n_publications_categories_mappings pcm  on (
				pcm.mapped_category_id = a.category_id 
				/* and pcm.category_mapping_id = 5041*/
				and pcm.publication_id = $publicationId
				and pcm.mapped_dictionary_id is null
				and pcm.mapped_location_id is not null
				and pcm.mapped_location_id in (
					a.country_id, a.county_id, a.municipality_id, a.city_id)
				)
	where 1=1
		 and a.moderation = 'ok' 
	
	union
	
	select 
		a.id as ad_id, pcm.id as pcm_id, pcm.category_mapping_id as cm_id
	from
		ads a
			join n_publications_categories_mappings pcm  on (
				pcm.mapped_category_id = a.category_id 
				/* and pcm.category_mapping_id = 5041*/
				and pcm.publication_id = $publicationId
				and pcm.mapped_dictionary_id is null
				and pcm.mapped_location_id is null
			)
	where 1=1
		 and a.moderation = 'ok' 
	
	union
	
	select 
		a.id as ad_id, pcm.id as pcm_id, pcm.category_mapping_id as cm_id
	from ads a 
		join ads_parameters ap on (ap.ad_id = a.id)
			join parameters p on (p.id = ap.parameter_id)
				join parameters_types pt on (pt.id = p.type_id and pt.accept_dictionary = 1)
			join n_publications_categories_mappings pcm on ( 
				pcm.mapped_category_id = a.category_id

				and pcm.publication_id = $publicationId
				and pcm.mapped_location_id is not null 
				and pcm.mapped_dictionary_id is not null
				and pcm.mapped_dictionary_id = cast(ap.value as UNSIGNED)
				and pcm.mapped_location_id in (
					a.country_id, a.county_id, a.municipality_id, a.city_id
				)
			)

	where 1=1
		 and a.moderation = 'ok' 
	
	) a_pcm
		join orders_items oi on (a_pcm.ad_id = oi.ad_id)
			join orders o on (
				oi.order_id = o.id 
				and o.n_status in (2,4,5) )
			join n_insertions ins on ( oi.id = ins.orders_items_id )
				join n_issues iss on (
					ins.issues_id = iss.id
					 and  iss.id = $issueId
				)
	union
	
	select 
		insa.ad_id
		, cma.id as cm_id
		,'avus' as source
	from 
		n_insertions_avus insa 
			join ads aa on (insa.ad_id = aa.id)
				join n_categories_mappings cma on (aa.n_avus_class_id = cma.avus_class_id)
	where 
		insa.issue_id = $issueId
) aaa
	join n_categories_mappings cmaa on (aaa.cm_id = cmaa.id and cmaa.n_publications_id = 2)
		join n_categories_mappings cm1 on (cmaa.parent_id = cm1.id)
			join n_categories_mappings cm2 on (cm1.parent_id = cm2.id)
				join n_categories_mappings cm3 on (cm2.parent_id = cm3.id)
					join n_categories_mappings cm4 on (cm3.parent_id = cm4.id)

	join ads a on (aaa.ad_id = a.id)
		left join orders_items oi on (a.id = oi.ad_id)
			left join n_products ps on (oi.n_products_id = ps.id)
		left join n_products pa on (a.n_avus_adlayout_id = pa.avus_adlayout_id)
		left join users u on (a.user_id = u.id)
order by 
	cmaa.level 
	,cmaa.sort_order
		";
	}
	
	
			$txtSql = "
select 
	a.id as ad_id
	,cm.id as cm_id
	,a_cm.source
	,cm.parent_id
	,cm.sort_order as cm_sort_order
	,cm.n_path
	,cm.level as cm_level
	,cm.dtp_group_id
	,a.user_id
	,a.n_avus_order_id
	,a.description_offline
	,a.description
	,u.username
	,a.n_positioning_desc
	,p.id as product_id
	,a_cm.is_paid 
	,p.name as product_name
	,p.display_ad_width
	,p.display_ad_height
	,p.layouts_id
	,l.name as layouts_name
	,p.dtp_hashtag_before_contact_info
	,p.is_display_ad
	,p.is_picture_required
	,(case when cm.level = 1 then cm.id when cm1.level = 1 then cm1.id when cm2.level = 1 then cm2.id when cm3.level = 1 then cm3.id when cm4.level = 1 then cm4.id else null end ) as lvl1	
	,(case when cm.level = 2 then cm.id when cm1.level = 2 then cm1.id when cm2.level = 2 then cm2.id when cm3.level = 2 then cm3.id when cm4.level = 2 then cm4.id else null end ) as lvl2	
	,(case when cm.level = 3 then cm.id when cm1.level = 3 then cm1.id when cm2.level = 3 then cm2.id when cm3.level = 3 then cm3.id when cm4.level = 3 then cm4.id else null end ) as lvl3	
	,(case when cm.level = 4 then cm.id when cm1.level = 4 then cm1.id when cm2.level = 4 then cm2.id when cm3.level = 4 then cm3.id when cm4.level = 4 then cm4.id else null end ) as lvl4	
	,(case when cm.level = 5 then cm.id when cm1.level = 5 then cm1.id when cm2.level = 5 then cm2.id when cm3.level = 5 then cm3.id when cm4.level = 5 then cm4.id else null end ) as lvl5	
	,(case when cm.level = 1 then cm.sort_order when cm1.level = 1 then cm1.sort_order when cm2.level = 1 then cm2.sort_order when cm3.level = 1 then cm3.sort_order when cm4.level = 1 then cm4.sort_order else null end ) as sort1	
	,(case when cm.level = 2 then cm.sort_order when cm1.level = 2 then cm1.sort_order when cm2.level = 2 then cm2.sort_order when cm3.level = 2 then cm3.sort_order when cm4.level = 2 then cm4.sort_order else null end ) as sort2	
	,(case when cm.level = 3 then cm.sort_order when cm1.level = 3 then cm1.sort_order when cm2.level = 3 then cm2.sort_order when cm3.level = 3 then cm3.sort_order when cm4.level = 3 then cm4.sort_order else null end ) as sort3	
	,(case when cm.level = 4 then cm.sort_order when cm1.level = 4 then cm1.sort_order when cm2.level = 4 then cm2.sort_order when cm3.level = 4 then cm3.sort_order when cm4.level = 4 then cm4.sort_order else null end ) as sort4	
	,(case when cm.level = 5 then cm.sort_order when cm1.level = 5 then cm1.sort_order when cm2.level = 5 then cm2.sort_order when cm3.level = 5 then cm3.sort_order when cm4.level = 5 then cm4.sort_order else null end ) as sort5	

from (
	select
		oi.ad_id as ad_id
		,oi.n_categories_mappings_id as cm_id
		,oi.n_products_id as p_id
		,(case when oi.n_total > 0 then 1 else 0 end) as is_paid
		,'suva' as source
	from 
		n_insertions ins 
			join orders_items oi on (ins.orders_items_id = oi.id)
				join orders o on (
					oi.order_id = o.id 
					and o.n_status in (2,4,5) 
				)
	where 
		ins.issues_id = $issueId


	union

		
	select 
		ins.ad_id
		,cm.id as cm_id
		,p.id as p_id
		,0 as is_paid
		,'avus' as source
	from 
		n_insertions_avus ins
			join ads a on (ins.ad_id = a.id)
				left join n_categories_mappings cm on (a.n_avus_class_id = cm.avus_class_id)
				left join n_products p on (a.n_avus_adlayout_id = p.avus_adlayout_id)
	where 
		ins.issue_id = $issueId

	) a_cm

		join n_categories_mappings cm on ( a_cm.cm_id = cm.id )
			left join n_categories_mappings cm1 on (cm.parent_id = cm1.id)
				left join n_categories_mappings cm2 on (cm1.parent_id = cm2.id)
					left join n_categories_mappings cm3 on (cm2.parent_id = cm3.id)
						left join n_categories_mappings cm4 on (cm3.parent_id = cm4.id)

		join ads a on (a_cm.ad_id = a.id)
			left join users u on (a.user_id = u.id)
			
		left join n_products p on (a_cm.p_id = p.id)
			left join n_layouts l on (p.layouts_id = l.id)
	order by
		a_cm.cm_id
		,a.description_offline
	";

		//Nava::poruka("<pre>$txtSql</pre>");
		// return;
		self::$rsAds = Nava::sqlToArray($txtSql);
		
		//po svim recordima iz catmaps
		self::$arrCatMaps = array();
		$lastCmId = null;
		$lastDescription = '';
		$lastIsPaid = null;
		foreach (self::$rsAds as $index => $row){
			//Nava::printr($row);
			$adId = $row['ad_id'];
			$cmId = $row['cm_id'];
			$sortIndex = $row['cm_sort_order'] * 1000000 + $cmId;  //prvo po sort orderu pa poslije po id-ju
			$level = $row['cm_level'];
			$groupId = $row['dtp_group_id'];
			$lvl1 = $row['lvl1'];
			$lvl2 = $row['lvl2'];
			$lvl3 = $row['lvl3'];
			$lvl4 = $row['lvl4'];
			$lvl5 = $row['lvl5'];
			$sort1 = $row['sort1'];
			$sort2 = $row['sort2'];
			$sort3 = $row['sort3'];
			$sort4 = $row['sort4'];
			$sort5 = $row['sort5'];
			
			$isPictureRequired = $row['is_picture_required'] == 1 ? true : false;
			$isDisplayAd = $row['is_display_ad'] == 1 ? true : false;
			
			$isPaid = $row['is_paid'] === 1 ? true : false ;
			$description = $row['description_offline'];
			$isDescription = strlen($description) > 5 ? true : false;
		
			//najprije napunit kategorije u glavni array
			if ($lvl1 && ! array_key_exists ( $lvl1, self::$arrCatMaps )){
				self::$arrCatMaps[$lvl1] = array(
					'id' => $lvl1
					,'level' => 1
					,'group_id' => $groupId
					,'parent' => null
					,'child_cat_ids' => array()
					,'ad_ids' => array()
					,'picture_ads' => array()
					,'display_ads' => array()
					,"export_to_xml" => true	
					);
			}
			if ($lvl2 && ! array_key_exists ( $lvl2, self::$arrCatMaps )){
				self::$arrCatMaps[$lvl2] = array(
					'id' => $lvl2
					,'level' => 2
					,'group_id' => $groupId
					,'parent' => $lvl1
					,'child_cat_ids' => array()
					,'ad_ids' => array()
					,'picture_ads' => array()
					,'display_ads' => array()
					,"export_to_xml" => true	
					);
				self::$arrCatMaps[$lvl1]['child_cat_ids'][$sort2 * 1000000 + $lvl2] = $lvl2;
			}
			if ($lvl3 && ! array_key_exists ( $lvl3, self::$arrCatMaps )){
				self::$arrCatMaps[$lvl3] = array(
					'id' => $lvl3
					,'level' => 3
					,'group_id' => $groupId
					,'parent' => $lvl2
					,'child_cat_ids' => array()
					,'ad_ids' => array()
					,'picture_ads' => array()
					,'display_ads' => array()
					,"export_to_xml" => true	
					);
				self::$arrCatMaps[$lvl2]['child_cat_ids'][$sort3 * 1000000 + $lvl3] = $lvl3;
			}
			if ($lvl4 && ! array_key_exists ( $lvl4, self::$arrCatMaps )){
				self::$arrCatMaps[$lvl4] = array(
					'id' => $lvl4
					,'level' => 4
					,'group_id' => $groupId
					,'parent' => $lvl3
					,'child_cat_ids' => array()
					,'ad_ids' => array()
					,'picture_ads' => array()
					,'display_ads' => array()
					,"export_to_xml" => true	
					);
				self::$arrCatMaps[$lvl3]['child_cat_ids'][$sort4 * 1000000 + $lvl4] = $lvl4;
			}
			if ($lvl5 && ! array_key_exists ( $lvl5, self::$arrCatMaps )){
				self::$arrCatMaps[$lvl5] = array(
					'id' => $lvl5
					,'level' => 5
					,'group_id' => $groupId
					,'parent' => $lvl4
					,'child_cat_ids' => array()
					,'ad_ids' => array()
					,'picture_ads' => array()
					,'display_ads' => array()
					,"export_to_xml" => true	
					);
				self::$arrCatMaps[$lvl4]['child_cat_ids'][$sort5 * 1000000 + $lvl5] = $lvl5;
			}
			
			
			//micanje duplih besplatnih
			//oglasi su poredani o  kategoriji i descriptionu. Ako dolaze dva ista besplatna, miče ih se
			if ( 
				!$isPaid && $isDescription 
				&& $lastCmId === $cmId 
				&& $lastIsPaid === $isPaid 
				&& substr( $lastDescription, 0,100 ) === substr($description, 0,100) 
				){
				//ako je oglas besplatan i dupli je ne šalje se u xml
				continue;
			}
			else{
				$lastIsPaid = $isPaid;
				$lastCmId = $cmId;
				$lastDescription = $description;
			}
			
			//sad kad svi čvorovi sigurno postoje treba u odgovarajući čvor napunit podatke o oglasu
			if ( $isPictureRequired ) self::$arrCatMaps[$cmId]['picture_ads'][$index] = $adId;
			if ( $isDisplayAd )	self::$arrCatMaps[$cmId]['display_ads'][$index] = $adId;
			
			//svakako se puni u glavni array
			self::$arrCatMaps[$cmId]['ad_ids'][$index] = $adId;
		}
		
		//sortiranje lista child Cat_map - ova prema ključu
		foreach(self::$arrCatMaps as $key=>$catMap  ){
			ksort($catMap['child_cat_ids']);
			self::$arrCatMaps[$key]['child_cat_ids'] = $catMap['child_cat_ids']; //treba ovako dodijeliti jer ne zapamti samo ako se ksort-a
		}
		
		//Nava::poruka("<pre>$txtSql</pre>");
		//Nava::printr(self::$arrCatMaps);
		// error_log(print_r(self::$arrCatMaps, true));
	}
		
	public static function r_catToXmlNEWNEW($catMapId ){


		//Generiranje XML-a
		$xml = '';
		$categoryMapping = \Baseapp\Suva\Models\CategoriesMappings::findFirst($catMapId);
		
		
		// LAYOUT ZA KATEGORIJU
		$arrCatMap = array();
		$arrCatMap['category_path'] = str_replace(' > ', '/', $categoryMapping->n_path); //20.12.2016 KZ-002
		$arrCatMap['photo_url'] = "file:///media/$categoryMapping->eps_file_path";

		$arrCatMap["aid_ccolwidth"] = "110.55118110236222";
		$arrCatMap['category_level'] = $categoryMapping->level;
		$arrCatMap['_caller'] = 'categoryMappingLayout';
		//error_log(print_r($arr, true));
		$xml.= $categoryMapping->Layout()->generateXml($arrCatMap); //generira XML i parsira variable_name, itd 
		
		Nava::poruka("<b>Processing category $categoryMapping->id $categoryMapping->n_path</b>");
		
		//ZA SVAKI OGLAS U KATEGORIJI 
		foreach ( self::$arrCatMaps[$catMapId]['ad_ids'] as $rsIndex => $adId ){
			
			$row = self::$rsAds[ $rsIndex ];
			
			$adId = $row['ad_id'];
			$avusOrderId = $row['n_avus_order_id'];
			
			$userId = $row['user_id'];
			$username = $row['username'];
			$productId = $row['product_id'];
			$productName = $row['product_name'];
			$dtpHashtagBeforeContactInfo = $row['dtp_hashtag_before_contact_info'] == 1 ? true : false ;
			$isDisplayAd = $row['is_display_ad'] == 1 ? true : false ;
			$isPictureRequired = $row['is_picture_required'] == 1 ? true : false ;
			
			$displayAdHeight = $row['display_ad_height'] ? $row['display_ad_height'] : 0;
			$displayAdWidth = $row['display_ad_width'] ? $row['display_ad_width'] : 0;
			$positioningDescription = $row['n_positioning_desc'] ? $row['n_positioning_desc'] : "";
			$layoutId = $row['layouts_id'] ? $row['layouts_id'] : -1;
			$layoutName = $row['layouts_name'] ? str_replace(' ', '_', $row['layouts_name']) : "invalid_layout_on_product_$productId";
			
			$source = $row['source'];
			//oglasi koji se importiraji preko agimove skripte(ads adittional data) nemaju description offline
			$description = $row['description_offline'] > '' ? $row['description_offline'] : $row['description'];
	
	
			//PORUKA
			$adType = "Text-only Ad";	
			if ( $isDisplayAd )	$adType = "Display Ad";
			elseif ( $isPictureRequired ) $adType = "Photo Ad";

			//DESCRIPTION
			if ( $source == 'avus' ){
				//dodavanje ### prije telefona
				if ( !strpos($description, '###') && $dtpHashtagBeforeContactInfo ){
					  
					
					// preg_match('/0[1-9]{1}[0-9]{0,1}\/[0-9]{6,7}/', '034/3456201', $matches, PREG_OFFSET_CAPTURE);
					// $string = 'The quick brown fox jumps over the lazy dog. 01/1234567, 021/342512';
										
					
					// $description = str_replace (' 092/', ' ###092/', $description);
					// $description = str_replace (' 091/', ' ###091/', $description);
					// $description = str_replace (' 095/', ' ###095/', $description);
					// $description = str_replace (' 098/', ' ###098/', $description);
					// $description = str_replace (' 099/', ' ###099/', $description);
					// $description = str_replace (' 097/', ' ###097/', $description);
					// $description = str_replace (' 01/', ' ###01/', $description);
					// $description = str_replace (' 020/', ' ###020/', $description);
					// $description = str_replace (' 021/', ' ###021/', $description);
					// $description = str_replace (' 022/', ' ###022/', $description);
					// $description = str_replace (' 023/', ' ###023/', $description);
					// $description = str_replace (' 051/', ' ###051/', $description);
					// $description = str_replace (' 052/', ' ###052/', $description);
					// $description = str_replace (' 040/', ' ###040/', $description);
					// $description = str_replace (' 042/', ' ###042/', $description);
					// $description = str_replace (' 043/', ' ###043/', $description);
					// $description = str_replace (' 048/', ' ###048/', $description);
					// $description = str_replace (' 031/', ' ###031/', $description);
					// $description = str_replace (' 032/', ' ###052/', $description);
					// $description = str_replace (' (+', ' ###(+', $description);
					// Nava::poruka($description);
					
					
					//ubacivanje <Tel></Tel>
					$patterns = array();
					$patterns[0] = '/0[1-9]{1}[0-9]{0,1}\/[0-9]{6,7}/';
					$replacements = array();
					$replacements[0] = '<Tel>$0</Tel>';
					$description = preg_replace($patterns, $replacements, $description);
					//Nava::poruka('izmijenjeno<pre>'.$desc."</pre>");

					//ubacivanje <Tel></Tel>za medjunarodne
					$patterns = array();
					$patterns[0] = '/\+[1-9]{1}[0-9]{0,2} \([0-9]{1,3}\) [0-9]{6,7}/';
					$replacements = array();
					$replacements[0] = '<Tel>$0</Tel>';
					$description = preg_replace($patterns, $replacements, $description);
					//Nava::poruka('izmijenjeno<pre>'.$desc."</pre>");					
					
					
					//ubacivanje <Email></Email>
					$patterns = array();
					$patterns[0] = '/[a-z\d._%+-]+@[a-z\d.-]+\.[a-z]{2,4}\b/i';
					$replacements = array();
					$replacements[0] = '<Email>$0</Email>';
					$description =  preg_replace($patterns ,$replacements, $description);
					

					
					
					$posTel = strpos($description, '<Tel>');
					$posEmail = strpos($description, '<Email>');
					if( !$posTel && !$posEmail ){
						
					}			
					elseif ( !$posTel && $posEmail ){
						$description = substr( $description, 0,$posEmail )."###".substr( $description, $posEmail, 9999 );
					}
					elseif ( !$posEmail && $posTel ){
						$description = substr( $description, 0,$posTel )."###".substr( $description, $posTel, 9999 );
					}
					elseif ( $posTel && $posEmail &&  $posEmail <= $posTel ) {
						$description = substr( $description, 0,$posEmail )."###".substr( $description, $posEmail, 9999 );
					}
					elseif ( $posTel && $posEmail &&  $posTel <= $posEmail  ){
						$description = substr( $description, 0,$posTel )."###".substr( $description, $posTel, 9999 );
					}
		
					
					
					// $posHash = strpos($description, '###') + 3;
					// $descPrijeHash = substr($description, 0, $posHash + 4);
					
					// $descPoslijeHash = substr($description, strlen($descPrijeHash), 9999);
					// $descPoslijeHash = str_replace (", ", "</Tel>, <Tel>", $descPoslijeHash);
					// //$descPoslijeHash = str_replace (",", "</Tel>, <Tel>", $descPoslijeHash);
					
					// $description = $descPrijeHash.$descPoslijeHash;
				}

				
				// //mijenjanje ### u tagove <Tel>... </Tel>
				// if ( strpos($description,"###") ) {
					// $afterLast = substr($description, strrpos($description, '#') + );
					// $beforeFirst = strtok($description, "#");
					// $final = '';
					// $final .= $beforeFirst."<Tel>".$afterLast."</Tel>";
					// $description = $final;
				// }
			}

			// //dodavanje hashtaga ako postoji  telefon
			// if ($dtpHashtagBeforeContactInfo ){
				// $strpos = strpos( $description , "<Tel>");
				// if ($strpos){
					// $description = substr( $description,0, $strpos  )."###".substr( $description, $strpos, 9999 );
				// }
			// }
			
			$description = "$adId $description";
			

			Nava::poruka("1. Processing $adType from $source <a href='/suva/ads2/edit/$adId/$userId' target='_blank'> $adId</a>, category <a href='/suva/categories-mappings/crud/$categoryMapping->id' target='_blank'>$categoryMapping->id </a> $categoryMapping->name, Product $productId $productName");
			
			
			//KOPIRANJE SLIKE
			if ( $isPictureRequired ){
				$adType = "Photo Ad";
				
				 
				
				//traženje slike
				if ($source == 'avus'){
					//2.1.2017 - za picture oglase iz AVUS-a filename AVUS_ORDER_ID.JPG
					// $filename = "$avusOrderId.JPG";
					$searchFilename = "$avusOrderId.*";
					//pronaci ekstenziju
					$pFindPath = self::$processedFolder.$searchFilename;
					$list = glob( $pFindPath );					
					if ( count( $list ) == 1 ){
						$foundFilePath = $list[0];
						$ext = pathinfo($foundFilePath, PATHINFO_EXTENSION);
						$filename = $avusOrderId.".".$ext;
					}else
						Nava::greska("ekstenzija za file $avusOrderId nije pronadena");
				}
				else {
					//ime filea za SUVA slike je layoutName_adId.EPS
					//$filename = $userId."_".$adId.".EPS";
					// $filename = $layoutName."_".$adId.".EPS";
					$searchFilename = $layoutName."_".$adId.".*";
					//pronaci ekstenziju
					$pFindPath = self::$processedFolder.$searchFilename;
					$list = glob( $pFindPath );					
					if ( count( $list ) == 1 ){
						$foundFilePath = $list[0];
						$ext = pathinfo($foundFilePath, PATHINFO_EXTENSION);
						$filename = $layoutName."_".$adId.".".$ext;
					}else
						Nava::greska("ekstenzija za file ".$layoutName."_".$adId." nije pronadena");
				}
				 
				$searchPath = self::$processedFolder.$searchFilename;
				$copyDestination = self::$mediaFolder."/".$filename;
				$result = self::findOneFileAndCopyToFolder ( $searchPath, $copyDestination );
				// error_log("path do slike".$searchPath);
				if ( $result == 1 ){
					//Nava::poruka(". . . . Picture <a href = '/$copyDestination' target ='_blank'>$filename</a> copied to zip folder ".self::$mediaFolder);
				}
				else {
					if ($result == 0){
						Nava::poruka("<span style='color:red;'>. . . . ERROR_:_:File $searchPath Does not exist.</span>") ;
					}
					elseif ( $result == null ){
						Nava::poruka("<span style='color:red;'>. . . . ERROR: Picture $filename NOT copied to zip folder $copyDestination.</span>");
					}
					elseif ( is_array($result) ){
						Nava::poruka("<span style='color:red;'>. . . . ERROR::Search $searchPath returned multiple results (".print_r($result,true)."). Nothing copied.</span>");
					}
					continue;
				}
			}
			else if($isDisplayAd){
				//15.5 ENISA, NA DISPLAY OGLASIMA MAKNIT LAYOUTNAME(DISPLAYAD_)
				//SAD POCINJE userId_adId
				$filename = $userId."_".$adId.".EPS";
					// $searchFilename = $layoutName."_".$adId.".*";
					$searchFilename = $adId.".*";
					//pronaci ekstenziju
					$pFindPath = self::$processedFolder.$searchFilename;
					$list = glob( $pFindPath );					
					if ( count( $list ) == 1 ){
						$foundFilePath = $list[0];
						$ext = pathinfo($foundFilePath, PATHINFO_EXTENSION);
						// $filename = $layoutName."_".$userId."_".$adId.".".$ext;
						$filename = $userId."_".$adId.".".$ext;
					}
			}
			
			
			//LAYOUT OGLASA
			$productLayout = \Baseapp\Suva\Models\Layouts::findFirst( $layoutId );
			if (!$productLayout) {
				Nava::poruka ("<span style='color:red;'>Nije pronađen layout za product $productId:$productName  <a href='/suva/products/crud/$productId' target='_blank'>Edit product</a></span>");
				continue;
			}
			$arrLayoutParameters = 	array(
				'ad_text' => $description
				,'aid_ccolwidth' => "110.55118110236222"
				,'_caller' => 'productLayout'
			);

			if ( $isPictureRequired ) {
				$arrLayoutParameters ['photo_url'] = "file:///media/".$filename;
			}
			elseif ($isDisplayAd){
				//20.12.2016 izmijeneno 
				$arrLayoutParameters ['photo_url'] = "file://display_oglasi/".$filename;
			}
			 
			$arrLayoutParameters ['photo_width'] = $displayAdWidth;
			$arrLayoutParameters ['photo_height'] = $displayAdHeight;
			$arrLayoutParameters ['ad_client'] = $username;
			$arrLayoutParameters ['ad_remark'] = $adId."  ".$positioningDescription;
			$arrLayoutParameters ['category_path'] = str_replace(' > ', '/', $categoryMapping->n_path); //20.12.2016

			
			
			
			$noviXml = $productLayout->generateXml( $arrLayoutParameters );
			
			
			$xml.= $noviXml;
			//$xml .= $productLayout->generateXml( $arrLayoutParameters );

		} 
		
		//REKURZIVNO ZA SVE PODREĐENE KATEGORIJE
		foreach ( self::$arrCatMaps[$catMapId]['child_cat_ids'] as $sortOrder => $childCatId ){
			$xml .= self::r_catToXmlNEWNEW( $childCatId ); 
		}
		return $xml;
	}
		
	
	public static function copyPicturesToWorkingFolderNEW ( $pIssueId = -1 , $pDtpGroupId = null  ){

		//varijable iz parametara
		$issue = \Baseapp\Suva\Models\Issues::findFirst($pIssueId);
		if(!$issue) return Nava::poruka("Nije pronaden issue sa brojem  $issueId",  -1);

		$dtpGroupId = $pDtpGroupId > 0 && \Baseapp\Suva\Models\DtpGroups::findFirst( $pDtpGroupId ) ? $pDtpGroupId : null;
		
		
		$appExecutionTime = ini_get('max_execution_time');
		ini_set('max_execution_time', 150000);
		ini_set ('request_terminate_timeout', 150000);
		
		//verzija s mapiranjem kategorija  > kategorija publikacije --> globalna kategorija + dictionary id
		
		$publication = $issue->Publication();
		
		//kreiranje foldera za publikaciju
		$publicationSubFolder = self::getPublicationFolderName ($publication);
		//error_log("publication :" .$publication);
		//error_log("publicationsubfolder :" .$publicationSubFolder);
		
		//po novome KZ-002  
		$toProcessSubFolder = "repository/dtp/to_process/";
		$toProcessFolder = $toProcessSubFolder.$publicationSubFolder."/";
		//error_log("toProcessFolder :" .$toProcessFolder);
		$processedFolder = "repository/dtp/processed/".$publicationSubFolder."/";
		//error_log("processedFolder :" .$processedFolder);
		if ( self::createFolderIfNotExist( $toProcessFolder ) === -1 ) { return -1; }
		
		//sta radi ova funkcija?
		//$arrCatMaps = self::fillArrCategoriesToProcess ( $issue, $dtpGroupId );
		//error_log("ARRCATMAPSTOWORKING ".print_r($arrCatMaps, true));
		self::fillArrCategoriesNEWNEW ($issue);
		
		
		 
		//ne nalazi nista?
		foreach ( self::$rsAds as $index => $row ){
			if ($row['is_picture_required']){

				$layoutName = strlen( $row['layouts_name'] ) > 0 ? $row['layouts_name'] : null;
				$source = $row['source'];
				$avusId = $row['n_avus_order_id'] > 0 ? $row['n_avus_order_id'] : null;
				$adId = $row['ad_id'];
				
				// filename - traženje imena filea
				if ( $source == 'avus' && $avusId > 0 ){
					$fileName = $avusId;
				}
				elseif ( $source != 'avus' && $layoutName ){
					$fileName = str_replace(' ', '_', $layoutName)."_".$adId;
				}
				else{
					//Greška
					$fileName = "ERROR: AD $adId - source $source, layout $layoutName"; 
					
				}

				//traženje filea na disku
				$txtSearch = $processedFolder.$fileName.".*";
				
				//Nava::poruka("searching for file $txtSearch");
				$list = glob( $txtSearch );
				$count = count( $list );
				if ( $count == 1 ){
					//ništa, već postoji u processed i ne kopira se u to_process
					Nava::poruka ("File $filename već postoji u folderu Processed, ne kopira se.");
				}
				elseif ( $count == 0 ){
					//kopira se u to_process
					$adsMedia = \Baseapp\Suva\Models\AdsMedia::findFirst( array(" ad_id = $adId ", 'order'=>'sort_idx asc') );
					if ($adsMedia){
						//error_log('Reports::offlineCopyPicturesToFolderAction 18');
						$media = $adsMedia->Media();
						if ($media){
							//error_log('Reports::offlineCopyPicturesToFolderAction 19');
							$mediaUrl = $media->get_url();
							if ($mediaUrl ){
								//error_log('Reports::offlineCopyPicturesToFolderAction 20');
								$link_to_picture = ltrim($mediaUrl, '/');
								$pictureName = substr ( $link_to_picture, strrpos($link_to_picture, '/') + 1 );
								$pictureExtension = substr ( $pictureName, strrpos( $pictureName, '.') + 1 );
								$newPicturePath = $toProcessFolder.$fileName.".".$pictureExtension;
								$copySuccess = copy($link_to_picture, $newPicturePath);
								if ($copySuccess) {
									Nava::poruka ( "Picture (ad <a href='/suva/ads2/edit/$adId/$ad->user_id' target='_blank'> $adId</a>)  $link_to_picture copied to $newPicturePath" );
								}
								else{
									$ad = \Baseapp\Suva\Models\Ads::findFirst($adId);
									Nava::greska ( "ERR101: Picture $link_to_picture  (ad <a href='/suva/ads2/edit/$adId/$ad->user_id' target='_blank'> $adId</a>) NOT copied to $newPicturePath" );
								}
							}
							else {
								//error_log('Reports::offlineCopyPicturesToFolderAction 21');
								Nava::poruka(" Ad <a href='/suva/ads2/edit/$adId/$ad->user_id' target='_blank'> $adId</a> does not have media URL. Contact System Adminisrtator");
							}
						}
						else{
							//error_log('Reports::offlineCopyPicturesToFolderAction 22');
							Nava::poruka("Ad <a href='/suva/ads2/edit/$adId/$ad->user_id' target='_blank'> $adId</a> does not have media. Contact system Administrator.");
						}
					}
					else{
						Nava::greska ("ERR102: Ad <a href='/suva/ads2/edit/$adId/$ad->user_id' target='_blank'> $adId</a> does not have picture attached" );
					}
				}
			}
		}
	}

	public static function copyPicturesToWorkingFolder ( $pIssueId = -1 , $pDtpGroupId = null  ){

		//varijable iz parametara
		$issue = \Baseapp\Suva\Models\Issues::findFirst($pIssueId);
		if(!$issue) return Nava::poruka("Nije pronaden issue sa brojem  $issueId",  -1);

		$dtpGroupId = $pDtpGroupId > 0 && \Baseapp\Suva\Models\DtpGroups::findFirst( $pDtpGroupId ) ? $pDtpGroupId : null;
		
		
		$appExecutionTime = ini_get('max_execution_time');
		ini_set('max_execution_time', 150000);
		ini_set ('request_terminate_timeout', 150000);
		
		//verzija s mapiranjem kategorija  > kategorija publikacije --> globalna kategorija + dictionary id
		
		$publication = $issue->Publication();
		
		//kreiranje foldera za publikaciju
		$publicationSubFolder = self::getPublicationFolderName ($publication);
		//error_log("publication :" .$publication);
		//error_log("publicationsubfolder :" .$publicationSubFolder);
		
		//po novome KZ-002  
		$toProcessSubFolder = "repository/dtp/to_process/";
		$toProcessFolder = $toProcessSubFolder.$publicationSubFolder."/";
		//error_log("toProcessFolder :" .$toProcessFolder);
		$processedFolder = "repository/dtp/processed/".$publicationSubFolder."/";
		//error_log("processedFolder :" .$processedFolder);
		if ( self::createFolderIfNotExist( $toProcessFolder ) === -1 ) { return -1; }
		
		//sta radi ova funkcija?
		$arrCatMaps = self::fillArrCategoriesToProcess ( $issue, $dtpGroupId );
		//error_log("ARRCATMAPSTOWORKING ".print_r($arrCatMaps, true));
		
		 
		//ne nalazi nista?
		foreach ( $arrCatMaps as $index => $catMap ){
			Nava::poruka("Obradjuje se category mapping $index ");
			//provjera je li postoji slika na adovima na kojima bi trebala postojati
			foreach ( $catMap['picture_ads'] as $adId => $arr ){
				if ( $arr['is_attached'] == true ){
					$fileName = $arr['filename'];
					//ad ima oglas
					//provjera li postoji među processed, ako postoji ne kopira se iz oglasa
					$txtSearch = $processedFolder.$fileName.".*";
					Nava::poruka("searching for file $txtSearch");
					$list = glob( $txtSearch );
					$count = count( $list );
					if ( $count == 1 ){
						//ništa, već postoji u processed i ne kopira se u to_process
						Nava::poruka ("File $filename već postoji u folderu Processed, ne kopira se.");
					}
					elseif ( $count == 0 ){
						//kopira se u to_process
						$adsMedia = \Baseapp\Suva\Models\AdsMedia::findFirst( array(" ad_id = $adId ", 'order'=>'sort_idx asc') );
						if ($adsMedia){
							//error_log('Reports::offlineCopyPicturesToFolderAction 18');
							$media = $adsMedia->Media();
							if ($media){
								//error_log('Reports::offlineCopyPicturesToFolderAction 19');
								$mediaUrl = $media->get_url();
								if ($mediaUrl ){
									//error_log('Reports::offlineCopyPicturesToFolderAction 20');
									$link_to_picture = ltrim($mediaUrl, '/');
									$pictureName = substr ( $link_to_picture, strrpos($link_to_picture, '/') + 1 );
									$pictureExtension = substr ( $pictureName, strrpos( $pictureName, '.') + 1 );
									$newPicturePath = $toProcessFolder.$fileName.".".$pictureExtension;
									$copySuccess = copy($link_to_picture, $newPicturePath);
									if ($copySuccess) {
										Nava::poruka ( "Picture (ad <a href='/suva/ads2/edit/$adId/$ad->user_id' target='_blank'> $adId</a>)  $link_to_picture copied to $newPicturePath" );
									}
									else{
										$ad = \Baseapp\Suva\Models\Ads::findFirst($adId);
										Nava::greska ( "ERR101: Picture $link_to_picture  (ad <a href='/suva/ads2/edit/$adId/$ad->user_id' target='_blank'> $adId</a>) NOT copied to $newPicturePath" );
									}
								}
								else {
									//error_log('Reports::offlineCopyPicturesToFolderAction 21');
									Nava::poruka(" Ad <a href='/suva/ads2/edit/$adId/$ad->user_id' target='_blank'> $adId</a> does not have media URL. Contact System Adminisrtator");
								}
							}
							else{
								//error_log('Reports::offlineCopyPicturesToFolderAction 22');
								Nava::poruka("Ad <a href='/suva/ads2/edit/$adId/$ad->user_id' target='_blank'> $adId</a> does not have media. Contact system Administrator.");
							}
						}
						else{
							Nava::greska ("ERR102: Ad <a href='/suva/ads2/edit/$adId/$ad->user_id' target='_blank'> $adId</a> does not have picture attached" );
						}
					}
				}
				else {
					//ad nema oglas
					$ad = \Baseapp\Suva\Models\Ads::findFirst($adId);
					if($ad->n_source != 'avus')
					//Nava::greska("Oglas <a href='/suva/ads2/edit/$adId/$ad->user_id' target='_blank'> $adId</a> nema priloženu sliku <PRE>".print_r($arr, true)."</PRE>");
					Nava::greska("ERR103: Oglas <a href='/suva/ads2/edit/$adId/$ad->user_id' target='_blank'> $adId</a> nema priloženu sliku");
				}
			}
		}
	}


	public static function copyPicturesToProcessedFolder ( $pIssueId = -1 , $pDtpGroupId = null  ){

		//varijable iz parametara
		$issue = \Baseapp\Suva\Models\Issues::findFirst($pIssueId);
		if(!$issue) return Nava::poruka("Nije pronaden issue sa brojem  $issueId",  -1);

		$dtpGroupId = $pDtpGroupId > 0 && \Baseapp\Suva\Models\DtpGroups::findFirst( $pDtpGroupId ) ? $pDtpGroupId : null;
		
		
		$appExecutionTime = ini_get('max_execution_time');
		ini_set('max_execution_time', 150000);
		ini_set ('request_terminate_timeout', 150000);
		
		//verzija s mapiranjem kategorija  > kategorija publikacije --> globalna kategorija + dictionary id
		
		$publication = $issue->Publication();
		 
		//kreiranje foldera za publikaciju
		$publicationSubFolder = self::getPublicationFolderName ($publication);
		
		$processedFolder = "repository/dtp/processed/".$publicationSubFolder."/";
		

		if ( self::createFolderIfNotExist( $processedFolder ) === -1 ) { return -1; }
		
		$arrCatMaps = self::fillArrCategories ( $issue, $dtpGroupId );
	
		foreach ($arrCatMaps as $catMap){
			//provjera je li postoji slika na adovima na kojima bi trebela postojati
			foreach ( $catMap['picture_ads'] as $adId => $arr ){
				if ( $arr['is_attached'] == true ){
						//kopira se u to_process
						$adsMedia = \Baseapp\Suva\Models\AdsMedia::findFirst( array(" ad_id = $adId ", 'order'=>'sort_idx asc') );
						if ($adsMedia){
							//error_log'Reports::offlineCopyPicturesToFolderAction 18');
							$media = $adsMedia->Media();
							if ($media){
								//error_log'Reports::offlineCopyPicturesToFolderAction 19');
								$mediaUrl = $media->get_url();
								if ($mediaUrl ){
									//error_log'Reports::offlineCopyPicturesToFolderAction 20');
									$fileName = $arr['filename'];
									$link_to_picture = ltrim($mediaUrl, '/');
									$pictureName = substr ( $link_to_picture, strrpos($link_to_picture, '/') + 1 );
									$pictureExtension = substr ( $pictureName, strrpos( $pictureName, '.') + 1 );
									$newPicturePath = $processedFolder.$fileName.".".$pictureExtension;
									
									$copySuccess = copy($link_to_picture, $newPicturePath);
									if ($copySuccess) {
										Nava::poruka ( "4. Picture $link_to_picture copied to $newPicturePath" );
									}
									else{
										$ad = \Baseapp\Suva\Models\Ads::findFirst($adId);
										Nava::greska ( "ERR104: Picture $link_to_picture  (ad <a href='/suva/ads2/edit/$adId/$ad->user_id' target='_blank'> $adId</a>) NOT copied to $newPicturePath" );
									}
								}
								else {
									//error_log'Reports::offlineCopyPicturesToFolderAction 21');
									Nava::poruka("$adId does not have media URL. Contact System Adminisrtator");
								}
							}
							else{
								//error_log'Reports::offlineCopyPicturesToFolderAction 22');
								Nava::poruka("$adId does not have media. Contact system Administrator.");
							}
						}
						else{
							Nava::poruka ("No media found for ad $adId , contact System Administrator" );
						}
				}
				else {
					//ad nema oglas
					$ad = \Baseapp\Suva\Models\Ads::findFirst($adId);
					//Nava::greska("Oglas <a href='/suva/ads2/edit/$adId/$ad->user_id' target='_blank'> $adId</a> nema priloženu sliku <PRE>".print_r($arr, true)."</PRE>");
					Nava::greska("ERR105: Oglas <a href='/suva/ads2/edit/$adId/$ad->user_id' target='_blank'> $adId</a> nema priloženu sliku");
				}
			}
		}
	}
	
	public static function offlineCopyPicturesToFolder( $pFolderName = null, $pTest = 0, $pDeleteExisting = 0 ) {
		// $this->sysMessage("offlineCopyPicturesToWorkingFolderAction");
		//error_log'Reports::offlineCopyPicturesToFolderAction 1');
		//verzija s mapiranjem kategorija  > kategorija publikacije --> globalna kategorija + dictionary id
		// $this->poruka("offlineCopyPicturesToFolderAction");
		
		if (!$pFolderName){
			$this->sysMessage("offlineCopyPicturesToFolderAction: folder name not supplied.");
			//error_log'Reports::offlineCopyPicturesToFolderAction 2');
		}
		//issue
		if(!array_key_exists('xml_report_issue_id', $_SESSION['_context'] )){
			$this->greska('ERR106: Nije poslan parametar Issue ID');
			//error_log'Reports::offlineCopyPicturesToFolderAction 3');
			return $this->redirect_back();
		}
		$issueId = $_SESSION['_context']['xml_report_issue_id'];
		if($issueId == ''){
			//error_log'Reports::offlineCopyPicturesToFolderAction 4');
			$this->greska("ERR107: Nije odabran Issue");
			return $this->redirect_back();
		}
		$issue = \Baseapp\Suva\Models\Issues::findFirst($issueId);
		if(!$issue){
			//error_log'Reports::offlineCopyPicturesToFolderAction 5');
			$this->greska("ERR108: Nije pronaden issue sa brojem  $issueId");
			return $this->redirect_back();
		}
		
		//dtp Group
		$dtpGroupId = null;
		$dtpGroup = null;
		if ( array_key_exists('xml_report_group_id', $_SESSION['_context']) && $_SESSION['_context']['xml_report_group_id'] > 0 ){
			$dtpGroupId = $_SESSION['_context']['xml_report_group_id'];
			$dtpGroup = \Baseapp\Suva\Models\DtpGroups::findFirst( $dtpGroupId );
		}
			
			
		$publication = $issue->Publication();
		
		$this->view->setVar("_context", $_SESSION['_context']);
		$this->view->setVar("_model", new \Baseapp\Suva\Models\Model());
		//error_log'Reports::offlineCopyPicturesToFolderAction 6');
		//kreiranje foldera za issue u dtp: OGLT_1016-1-1_GR1
		$issueFolderTitle = $issue->Publication()->slug."_".date("Y-m-d", strtotime($issue->date_published));
		if ( $dtpGroup ) $issueFolderTitle .= "_".$dtpGroup->name;

		$issueFolder = "repository/dtp/issues/$issueFolderTitle/";
		$error = null;
		if (!is_dir($issueFolder)){
			//error_log'Reports::offlineCopyPicturesToFolderAction 7');
			$error = \Baseapp\Suva\Library\Nava::folder_create($issueFolder);
			if ($error === null) {
				//error_log'Reports::offlineCopyPicturesToFolderAction 8');
				$this->sysMessage ("Folder $issueFolder created.");
				//error_log("ReportController:offLineIssueXmlAction 3 SUCCESS $error");
			} else {
				//error_log'Reports::offlineCopyPicturesToFolderAction 9');
				//error_log("ReportController:offLineIssueXmlAction 2 ERROR: $error");
				$this->greska("ERR109: Folder $issueFolder not created due to reason: $error");
				return $this->redirect_back();
			}
		}
		
		//provjera, brisanje foldera
		$defaultFolder = $issueFolder.$pFolderName."/";
		if ( is_dir ( $defaultFolder ) ) {
			//error_log'Reports::offlineCopyPicturesToFolderAction 10');
			if ( $pDeleteExisting == 1 ){
				//error_log'Reports::offlineCopyPicturesToFolderAction 11');
				$this->poruka("Folder $defaultFolder alredy exists. Deleting.");
				$error =  Nava::r_deleteDir($defaultFolder);
				if ( ! $error ) {
					//error_log'Reports::offlineCopyPicturesToFolderAction 12');
					$this->poruka("Folder $defaultFolder deleted.");
				}
				else {
					//error_log'Reports::offlineCopyPicturesToFolderAction 13');
					$this->greska("ERR110: Folder $defaultFolder not deleted due to reason: $error");
					return $this->redirect_back();
				}	
			}
			else{
				//error_log'Reports::offlineCopyPicturesToFolderAction 14');
				$this->poruka ("2.Folder $defaultFolder already exists. Keeping its content and overwriting with new content.");
			}
		}
		
		//kreiranje defaultFoldera
		if (!is_dir($defaultFolder)){
			//error_log'Reports::offlineCopyPicturesToFolderAction 15');
			$error = \Baseapp\Suva\Library\Nava::folder_create($defaultFolder);
			if ($error === null) {
				//error_log'Reports::offlineCopyPicturesToFolderAction 16');
				$this->sysMessage ("Folder $defaultFolder created.");
			} else {
				//error_log'Reports::offlineCopyPicturesToFolderAction 17');
				$this->greska("ERR111: Folder $defaultFolder not created due to reason: $error");
				return $this->redirect_back();
			}
		}
		
		$txtSql = "
			select oi.id 
			from orders_items oi
				join n_insertions ins on (oi.id = ins.orders_items_id and ins.published_status = 'ok')
				join n_products p on (oi.n_products_id = p.id and (p.is_picture_required = 1 or p.is_display_ad = 1) )
					
			where 
				ins.issues_id = $issue->id
		";
		foreach ( Nava::sqlToArray ( $txtSql ) as $row ){
			$oi = \Baseapp\Suva\Models\OrdersItems::findFirst ( $row['id'] );
			$ad = $oi->Ad();
			$product = $oi->Product();
			$txtProductType = $product->is_display_ad ? " Display AD " : " Picture AD ";
			//FOTO OGLAS
			$adsMedia = \Baseapp\Suva\Models\AdsMedia::findFirst( array(" ad_id = $ad->id ", 'order'=>'sort_idx asc') );
			if ($adsMedia){
				//error_log'Reports::offlineCopyPicturesToFolderAction 18');
				$media = $adsMedia->Media();
				if ($media){
					//error_log'Reports::offlineCopyPicturesToFolderAction 19');
					$mediaUrl = $media->get_url();
					if ($mediaUrl ){
						//error_log'Reports::offlineCopyPicturesToFolderAction 20');
						$link_to_picture = ltrim($mediaUrl, '/');
						$pictureName = substr($link_to_picture, strrpos($link_to_picture, '/') + 1);
						$pictureExtension = substr ( $pictureName, strrpos( $pictureName, '.') + 1  );
						//$this->sysMessage ("pictureName $pictureName, pictureExtension $pictureExtension ");
						$newPictureName = $ad->id.".".$pictureExtension;
						$newPicurePath = $defaultFolder.$newPictureName;
						copy($link_to_picture, $newPicurePath);
						$this->poruka ( "1. Picture $link_to_picture copied to $newPicurePath" );
						//error_log(" ISSUES LINK SLIKE $link_to_picture");
						//error_log(" ISSUES IME SLIKE $picture_name");
					}
					else {
						//error_log'Reports::offlineCopyPicturesToFolderAction 21');
						$this->greska("ERR113: $txtProductType $ad->id (product $product->id, $product->name) does not have media URL.");
					}
				}
				else{
					//error_log'Reports::offlineCopyPicturesToFolderAction 22');
					$this->sysMessage("$txtProductType $ad->id (product $product->id, $product->name) does not have media. Contact system Administrator.");
				}
			}
			else {
				//error_log'Reports::offlineCopyPicturesToFolderAction 23');
				if ($pTest){
					//error_log'Reports::offlineCopyPicturesToFolderAction 24');
					//ako je testiranje, onda se prebacuje defaultna slika
					$copySuccess = copy ("repository/dtp/oglasnik_logo_default_photo.jpg", $defaultFolder.$ad->id.".jpg");
					$copyMessage = $copySuccess ? "Default photo added. " : "<span style='color:red;'>Default photo could not be added </span>";
					$this->poruka("3. $txtProductType <a href='/suva/ads2/edit/$ad->id' target='_blank'>$ad->id</a> (product $product->id, $product->name) does not have picture (AdsMedia). $copyMessage ");
				}
				else{
					//error_log'Reports::offlineCopyPicturesToFolderAction 25');
					$this->greska("ERR112: $txtProductType <a href='/suva/ads2/edit/$ad->id' target='_blank'>$ad->id</a> (product $product->id, $product->name) does not have picture (AdsMedia). ");
				
				}
			}
			
			
		} // petlja za sve picture adove
		//error_log'Reports::offlineCopyPicturesToFolderAction 26');
		$this->view->pick("reports/index");
	}
	
	public static function createXmlForDtpNEW ( $pIssueId = -1 , $pDtpGroupId = null ){

		//varijable iz parametara
		$issue = \Baseapp\Suva\Models\Issues::findFirst($pIssueId);
		if(!$issue) return Nava::poruka("Nije pronaden issue sa brojem  $issueId",  -1);

		$dtpGroup = $pDtpGroupId > 0 ? \Baseapp\Suva\Models\DtpGroups::findFirst( $pDtpGroupId ) : null;
		
		
		$appExecutionTime = ini_get('max_execution_time');
		ini_set('max_execution_time', 150000);
		ini_set ('request_terminate_timeout', 150000);
		
		//verzija s mapiranjem kategorija  > kategorija publikacije --> globalna kategorija + dictionary id
				
		$publication = $issue->Publication();
		
		//kreiranje foldera za publikaciju
		$publicationSubFolder = self::getPublicationFolderName ($publication);
		
		
		//kreiranje foldera za issue u dtp: OGLT_1016-1-1_GR1
		$issueSubFolder = self::getIssueFolderName ($issue, $dtpGroup);
		$issueFolder = "repository/dtp/issues/$issueSubFolder/";
		if ( self::createFolderIfNotExist( $issueFolder ) === -1 ) { return -1; }
		
		
		//kreiranje foldera za prebacivanje u XML - fileovi se kasnije kopiraju iz foldera processed
		$zipSubFolder = "media/";
		$zipFolder = $issueFolder.$zipSubFolder;
		if ( self::deleteFolderIfExist ( $zipFolder ) === -1 ) { return -1; }
		if ( self::createFolderIfNotExist( $zipFolder ) === -1 ) { return -1; }
		
		// $processedSubFolder = "processed/";
		// $processedFolder = $issueFolder.$processedSubFolder;
		//po novome KZ-002
		$processedSubFolder = "repository/dtp/processed/";
		$processedFolder = $processedSubFolder.$publicationSubFolder."/";

		if ( self::createFolderIfNotExist( $processedFolder ) === -1 ) { return -1; }
		
		
		//dohvat defaultnog folderaza spremanje slika u XML-u

		Nava::poruka ("FOLDER ZA DOHVAT SLIKA U INDESIGNU ".libAppSetting::get( 'dtp_indesignRootFolder' )  );

		//root Category Mapping
		$rootCategoryMappings = \Baseapp\Suva\Models\CategoriesMappings::find("parent_id is null and n_publications_id = $publication->id and active = 1");
		if ( count( $rootCategoryMappings) != 1) {
			return Nava::poruka( count( $rootCategoryMappings)." root categories for publication $publication->name found! There is an error in data!", -1);
		}

		
		$rootCategoryMapping = \Baseapp\Suva\Models\CategoriesMappings::findFirst("parent_id is null and active = 1 and n_publications_id = $publication->id");
		//error_log("ReportsController::offLineIssueXmlAction rootCategoryMapping: $rootCategoryMapping->id");
		if ( !$rootCategoryMapping ) {
			return Nava::poruka("Root category for publication $publication->name not found", -1);
		}
		
		//ARRAY ZA UBRZANJE GENERIRANJA
		$arrCatMaps = self::fillArrCategories ( $issue, $dtpGroup->id );
		
		Nava::printr($arrCatMaps);
		return;
		//GLAVNA REKURZIVNA FUNKCIJA
		$defaultFolder = ''; //ČEMU OVO SLUŽI??
		
		$arrParams = array(
			'categoryMappingId' => $rootCategoryMapping->id, 
			'folder' => $defaultFolder, 
			'issueFolder' => $issueFolder,
			'processedFolder' => $processedFolder,
			'zipSubFolder' => $zipSubFolder,
			'categoryEpsFilesFolder' => 'repository/dtp/categories/',
			'dtp_group_id' => ($dtpGroup ? $dtpGroup->id : null),
			'issueId' => $issue->id
			
		); 
		
		Nava::poruka("ReportsController::offLineIssueXmlAction: Processing root category $rootCategoryMapping->id, params: ".print_r($arrParams, true));
		$xml = self::r_catToXml( $arrParams );
		
		
		//09.09.2016 Hajnal - dodati čvor root i zaglavlje
		
		$xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'.PHP_EOL."<Root xmlns:aid='http://ns.adobe.com/AdobeInDesign/4.0/'>".$xml.PHP_EOL."</Root>";
		
		
		// NIKOLA KREIRANJE ZIP FILE OD DIREKTORIJA TEMP_ZIP
		
		$xmlFileName = "export.xml";
		$xmlFilePath = $issueFolder.$xmlFileName;
		Nava::poruka("Adding XML to file $xmlFilePath ");
		file_put_contents($xmlFilePath, $xml);
		
		$zipFileName = "$issueFolderTitle.zip";
		//brisanje postojećeg filea
		//kreirat zip file iz foldera /repozitory/dtp/temp_zip
		$zipFilePath = $issueFolder.$zipFileName;
		unlink($zipFilePath);
		$zip = \Comodojo\Zip\Zip::create( $zipFilePath );
		
		Nava::poruka ("Adding images from $folderToZip to file  $zipFileName ...");
		$zip->add( $zipFolder);		
		$zip->add( $xmlFilePath );
		
		$_SESSION['_context']['dtp_zip_file_url'] = $zipFilePath;
		//error_log($xml);
		
		
		//MICANJE SPECIJALNIH KARAKTERA, NON PRINTABLE
		$xml = preg_replace ('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $xml);
		
		 // Match Emoticons
		$regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
		$xml = preg_replace($regexEmoticons, '', $xml);

		// Match Miscellaneous Symbols and Pictographs
		$regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
		$xml = preg_replace($regexSymbols, '', $xml);

		// Match Transport And Map Symbols
		$regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
		$xml = preg_replace($regexTransport, '', $xml);

		// Match Miscellaneous Symbols
		$regexMisc = '/[\x{2600}-\x{26FF}]/u';
		$xml = preg_replace($regexMisc, '', $xml);

		// Match Dingbats
		$regexDingbats = '/[\x{2700}-\x{27BF}]/u';
		$xml = preg_replace($regexDingbats, '', $xml);
		
		
		//20.12.2016 KZ-002 zahtjev
		// $xml = str_replace("&lt;Tel&gt;","<Tel>",$xml);
		// $xml = str_replace('&lt;/Tel&gt;','</Tel>',$xml);
		// $xml = str_replace('&lt;Email&gt;','<Email>',$xml);
		// $xml = str_replace('&lt;/Email&gt;','</Email>',$xml);

		// str_replace('&lt;','<',$xml);
		 
		 // $xml = array_map(
			// function($xml) {
				// $xml1 = $xml;
				// str_replace('&gt','>',$xml1);
				// str_replace('&lt','<',$xml1);
				// return $xml1;
				
			// },$xml
		 
		 // );

		
		ini_set('max_execution_time', $appExecutionTime);
		ini_set('request_terminate_timeout', $appExecutionTime);
		
		return $xml;
		
		
	}

	public static function createXmlForDtp ( $pIssueId = -1 , $pDtpGroupId = null ){

		//varijable iz parametara
		$issue = \Baseapp\Suva\Models\Issues::findFirst($pIssueId);
		if(!$issue) return Nava::poruka("Nije pronaden issue sa brojem  $issueId",  -1);

		$dtpGroup = $pDtpGroupId > 0 ? \Baseapp\Suva\Models\DtpGroups::findFirst( $pDtpGroupId ) : null;
		
		
		$appExecutionTime = ini_get('max_execution_time');
		ini_set('max_execution_time', 150000);
		ini_set ('request_terminate_timeout', 150000);
		
		
		//verzija s mapiranjem kategorija  > kategorija publikacije --> globalna kategorija + dictionary id
		
		$publication = $issue->Publication();
		
		//kreiranje foldera za publikaciju
		$publicationSubFolder = self::getPublicationFolderName ($publication);
		
		
		//kreiranje foldera za issue u dtp: OGLT_1016-1-1_GR1
		$issueSubFolder = self::getIssueFolderName ($issue, $dtpGroup);
		$issueFolder = "repository/dtp/issues/$issueSubFolder/";
		if ( self::createFolderIfNotExist( $issueFolder ) === -1 ) { return -1; }
		
		
		$arrCatMaps = self::fillArrCategories ( $issue, ( $dtpGroup ? $dtpGroup->id : null ) );
		
		//kreiranje foldera za prebacivanje u XML - fileovi se kasnije kopiraju iz foldera processed
		$zipSubFolder = "media/";
		$zipFolder = $issueFolder.$zipSubFolder;
		if ( self::deleteFolderIfExist ( $zipFolder ) === -1 ) { return -1; }
		if ( self::createFolderIfNotExist( $zipFolder ) === -1 ) { return -1; }
		
		// $processedSubFolder = "processed/";
		// $processedFolder = $issueFolder.$processedSubFolder;
		//po novome KZ-002
		$processedSubFolder = "repository/dtp/processed/";
		$processedFolder = $processedSubFolder.$publicationSubFolder."/";

		if ( self::createFolderIfNotExist( $processedFolder ) === -1 ) { return -1; }
		
		//dohvat defaultnog folderaza spremanje slika u XML-u

		Nava::poruka ("FOLDER ZA DOHVAT SLIKA U INDESIGNU ".libAppSetting::get( 'dtp_indesignRootFolder' )  );

		//root Category Mapping
		$rootCategoryMappings = \Baseapp\Suva\Models\CategoriesMappings::find("parent_id is null and n_publications_id = $publication->id and active = 1");
		if ( count( $rootCategoryMappings) != 1) {
			return Nava::poruka( count( $rootCategoryMappings)." root categories for publication $publication->name found! There is an error in data!", -1);
		}

		
		$rootCategoryMapping = \Baseapp\Suva\Models\CategoriesMappings::findFirst("parent_id is null and active = 1 and n_publications_id = $publication->id");
		//error_log("ReportsController::offLineIssueXmlAction rootCategoryMapping: $rootCategoryMapping->id");
		if ( !$rootCategoryMapping ) {
			return Nava::poruka("Root category for publication $publication->name not found", -1);
		}
		
		//GLAVNA REKURZIVNA FUNKCIJA
		$defaultFolder = ''; //ČEMU OVO SLUŽI??
		
		$arrParams = array(
			'categoryMappingId' => $rootCategoryMapping->id, 
			'folder' => $defaultFolder, 
			'issueFolder' => $issueFolder,
			'processedFolder' => $processedFolder,
			'zipSubFolder' => $zipSubFolder,
			'categoryEpsFilesFolder' => 'repository/dtp/categories/',
			'dtp_group_id' => ($dtpGroup ? $dtpGroup->id : null),
			'issueId' => $issue->id
		); 
		
		Nava::poruka("ReportsController::offLineIssueXmlAction: Processing root category $rootCategoryMapping->id, params: ".print_r($arrParams, true));
		$xml = self::r_catToXml( $arrParams );
		
		
		//09.09.2016 Hajnal - dodati čvor root i zaglavlje
		
		$xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'.PHP_EOL."<Root xmlns:aid='http://ns.adobe.com/AdobeInDesign/4.0/'>".$xml.PHP_EOL."</Root>";
		
		
		Nava::poruka ("Replacing non-printable characters in XML ");
		//MICANJE SPECIJALNIH KARAKTERA, NON PRINTABLE
		$xml = preg_replace ('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $xml);
		
		 // Match Emoticons
		$regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
		$xml = preg_replace($regexEmoticons, '', $xml);

		// Match Miscellaneous Symbols and Pictographs
		$regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
		$xml = preg_replace($regexSymbols, '', $xml);

		// Match Transport And Map Symbols
		$regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
		$xml = preg_replace($regexTransport, '', $xml);

		// Match Miscellaneous Symbols
		$regexMisc = '/[\x{2600}-\x{26FF}]/u';
		$xml = preg_replace($regexMisc, '', $xml);

		// Match Dingbats
		$regexDingbats = '/[\x{2700}-\x{27BF}]/u';
		$xml = preg_replace($regexDingbats, '', $xml);

	
		// NIKOLA KREIRANJE ZIP FILE OD DIREKTORIJA TEMP_ZIP
		$xmlFileName = "export.xml";
		$xmlFilePath = $issueFolder.$xmlFileName;
		Nava::poruka("Adding XML to file $xmlFilePath ");
		file_put_contents($xmlFilePath, $xml);
		
		$zipFileName = "$issueSubFolder.zip";
		//brisanje postojećeg filea
		//kreirat zip file iz foldera /repozitory/dtp/temp_zip
		$zipFilePath = $issueFolder.$zipFileName;
		unlink($zipFilePath);
		$zip = \Comodojo\Zip\Zip::create( $zipFilePath );
		
		Nava::poruka ("Adding images from $zipFolder to file  $zipFileName ...");
		$zip->add( $zipFolder);		
		$zip->add( $xmlFilePath );
		
		$_SESSION['_context']['dtp_zip_file_url'] = $zipFilePath;
		//error_log($xml);
		
			
		Nava::poruka ("<b>Click to download <a href = '/$zipFilePath' download>/$zipFilePath</a> </b>");

		
		
		//20.12.2016 KZ-002 zahtjev
		// $xml = str_replace("&lt;Tel&gt;","<Tel>",$xml);
		// $xml = str_replace('&lt;/Tel&gt;','</Tel>',$xml);
		// $xml = str_replace('&lt;Email&gt;','<Email>',$xml);
		// $xml = str_replace('&lt;/Email&gt;','</Email>',$xml);

		// str_replace('&lt;','<',$xml);
		 
		 // $xml = array_map(
			// function($xml) {
				// $xml1 = $xml;
				// str_replace('&gt','>',$xml1);
				// str_replace('&lt','<',$xml1);
				// return $xml1;
				
			// },$xml
		 
		 // );

		
		ini_set('max_execution_time', $appExecutionTime);
		ini_set('request_terminate_timeout', $appExecutionTime);
		
		return $xml;
		
		
	}
	
	//PREBACIVANJE U XML ZA OFFLINE IZDANJE
	public static function r_catToXmlNEW($p = Array(), &$pArrCatMaps ){

		/*
			Rekurzivna funkcija za iteraciju kroz stablo kategorija i generiranje xml-a za DTP
		*/
		
		
		
		//PARAMETRI IZ $p
		
		//issueId
		if ( ! array_key_exists('issueId',$p)) return Nava::poruka("Funkcija r_catToXml pozvana bez parametra issueId, <pre>".print_r($p, true)."</pre>");

		//categoryMappingId
		if (! array_key_exists('categoryMappingId',$p)) return Nava::poruka ("Issue $issue->id r_catToXml: parameter categoryMappingId not supplied.");
		$categoryMapping = \Baseapp\Suva\Models\CategoriesMappings::findFirst( ($p['categoryMappingId'] > 0 ? $p['categoryMappingId'] : -1) );
		if (!$categoryMapping) return Nava::greska ("ERR114: Publication Category No. ".$p['categoryMappingId']." not found");

		//2.1.2017 - gleda se je li se kategorija obrađuje
		$arrCatMap = $pArrCatMaps[$categoryMapping->id];
		if ($arrCatMap['export_to_xml'] == false){
			Nava::poruka ("Category $categoryMapping->n_path SE NE IZVOZI JER JE PRAZNA <PRE>".print_r($arrCatMap,true)."</PRE>" );
			Nava::printr($arrCatMap);
			return "";
		}
		
		//level
		$level = array_key_exists('level',$p) && $p['level'] > 0 ? $p['level'] : 0;
		
		//folder
		// $folder = array_key_exists('folder',$p) ? $p['folder'] : null ;
		// if (!$folder ) return Nava::poruka("Issue $issue->id r_catToXml: parameter 'folder' not supplied <pre>".print_r($p, true)."</pre>", "ERROR: Directory to put pictures not supplied.");

		//categoryEpsFilesFolder
		$categoryEpsFilesFolder = array_key_exists('categoryEpsFilesFolder',$p) ? $p['categoryEpsFilesFolder'] : null;
		if (! $categoryEpsFilesFolder ) return Nava::poruka ("Issue $issue->id r_catToXml: parameter 'categoryEpsFilesFolder' not supplied","ERROR: parameter 'categoryEpsFilesFolder' not supplied" );
		
		//issueFolder
		$issueFolder = array_key_exists('issueFolder',$p) ? $p['issueFolder'] : null;
		if (! $issueFolder ) return Nava::poruka ("Issue $issue->id r_catToXml: parameter 'issueFolder' not supplied", null);

		//processedFolder
		$processedFolder = array_key_exists('processedFolder',$p) ? $p['processedFolder'] : null;
		if (! $processedFolder ) return Nava::poruka ("Issue $issue->id r_catToXml: parameter 'processedFolder' not supplied");
		

		//zipSubFolder
		$zipSubFolder = array_key_exists('zipSubFolder',$p) ?  $p['zipSubFolder'] : null;
		if (! $zipSubFolder ) return Nava::poruka ("Issue $issue->id r_catToXml: parameter 'zipSubFolder' not supplied");



		
		//dtp_group_id
		$txtGroup = array_key_exists('dtp_group_id', $p) && $p['dtp_group_id'] > 0 && $categoryMapping->level == 1 ? " and dtp_group_id = ".$p['dtp_group_id'] : "";


		//varijable na temelju parametara

		$issue = \Baseapp\Suva\Models\Issues::findFirst( ($p['issueId'] > 0 ? $p['issueId'] : -1) );
		if (!$issue) return Nava::greska ("ERR116: Issue ".$p['issueId']." not found");
		
		
		
		$dtpLayout = $categoryMapping->Layout();
		if (!$dtpLayout) return Nava::poruka("Nije pronađen layout za CategoryMapping $categoryMapping->id , $categoryMapping->name, layout_id = $categoryMapping->layout_id ");

		
		//$processedFolder = $issueFolder.$processedSubFolder;
		$zipFolder = $issueFolder.$zipSubFolder;
		
		$dtp_indesignRootFolder = \Baseapp\Suva\Library\libAppSetting::get('dtp_indesignRootFolder');
		$dtp_categoryEpsRootFolder = \Baseapp\Suva\Library\libAppSetting::get('dtp_categoryEpsRootFolder');
		
		$imeGrupe = $categoryMapping->dtp_group_id ? ", GRUPA: ".$categoryMapping->DtpGroup()->name." ," : "";

		//error_log("obradjujem kategoriju $categoryMapping->id  $categoryMapping->name ");
		
		
		// //OGLASI IZ FRONTENDA ILI BACKENDA u kategoriji 
		// $adIds = array();
		
		// //oglasi iz Suve
		// $adIds1 = \Baseapp\Suva\Library\libPCM::getIds(
			// array (
				// "modelName" => "Ads"
				
				// ,"filters" => array (
					// array(
						// "modelName" => "Issues"
						// ,"idValue" => $issue->id
					// )
					// ,array(
						// "modelName" => "CategoriesMappings"
						// ,"idValue" => $categoryMapping->id
					// )
					// ,array(
						// "modelName" => "Ads"
						// ,"whereField" => "moderation"
						// ,"whereValue" =>"'ok'"
					// )
				// )
			// )
		// );
		
		// foreach ($adIds1 as $adId)
			// if ( in_array ($adId, $adIds) == false )
				// array_push( $adIds, $adId );
		
		// //display oglasi iz Suve
		// $rsDisplay = Nava::sqlToArray("
			// select a.id
			// from ads a 
				// join orders_items oi on (a.id = oi.ad_id)
					// join n_insertions ins on (oi.id = ins.orders_items_id and ins.issues_id = $issue->id)
			// where
				// n_category_mapping_id = $categoryMapping->id
				// and n_is_display_ad = 1
			// ");
		// if ($rsDisplay){
			// foreach ( $rsDisplay as $row ){
				// $adId = $row['id'];
				// if ( in_array ($adId, $adIds) == false )
					// array_push( $adIds, $adId );
			// }
		// }
		
		// //oglasi iz Avusa
		// $txtSql = "
			// select a.id 
			// from ads a
				// join n_categories_mappings cm on (a.n_avus_class_id = cm.avus_class_id and cm.id = $categoryMapping->id )
					// join n_insertions_avus ia on (a.id = ia.ad_id and ia.issue_id = $issue->id)
		// ";
		// //Nava::poruka($txtSql);
		// $rsAvus = Nava::sqltoArray($txtSql);
		// if ($rsAvus){
			// foreach($rsAvus as $row){
				// $adId= $row['id'];
				// if ( in_array ($adId, $adIds) == false )
					// array_push( $adIds, $adId );
			// }
		// }
		
		  
		//DJECA Kategorije
		$categoryMappingChildren = \Baseapp\Suva\Models\CategoriesMappings::find(
			array(
				" parent_id = $categoryMapping->id and active = 1 $txtGroup",
				'order' => "sort_order asc"
				)
		);
		

		
		//2.1.2017
		$adIds = $arrCatMap['ad_ids'];
		Nava::poruka("<b>Processing category <a href='/suva/categories-mappings/crud/$categoryMapping->id' target='_blank'>$categoryMapping->id </a>$imeGrupe $categoryMapping->n_path . </b>");
		


		
		//Generiranje XML-a
		$xml = '';

		//EPS file Kategorije
		//kopiranje filea glave kategorije u /media Naći postoji li file sa imenom oglasa u folderu $processedFolder ..pretraživanje pomoću wildcarda, neki glob..
		$txtSearch = $categoryEpsFilesFolder.$categoryMapping->eps_file_path;
		$list = glob( $txtSearch );
		$count = count( $list );
		if ( $count == 1 ){
			//kopiranje filea u zipFolder
			//Nava::poruka ( "Picture $txtSearch found.");
			$foundFilePath = $list[0];
			$foundFileName = $categoryMapping->eps_file_path;
			$copyDestination = $zipFolder.$foundFileName;
			$copySuccess = copy($foundFilePath, $copyDestination);
			if ($copySuccess ){
				$pictureProcessingMessage = "Picture <a href = '/$copyDestination' target ='_blank'>$foundFileName</a> copied to zip folder $zipFolder .";
				$isPhotoRetrievalError = false;
			}
			else{
				$pictureProcessingMessage = "<span style = 'color:red;'>ERROR: Picture $foundFileName NOT copied to zip folder $copyDestination.</span>";
			}
		}
		elseif ( $count > 1 ){
			$pictureProcessingMessage = "<span style = 'color:red;'>ERROR::Search $txtSearch returned $count results (".print_r($list,true)."). Nothing copied</span>" ;
		}
		elseif ( $count == 0 ){
			$pictureProcessingMessage = "<span style = 'color:red;'>ERROR::File $txtSearch Does not exist.</span>" ;
		}		
		
	
		
		// XML ČVOR ZA KATEGORIJU
		//ne generira se XML za kategoriju koja ima level=1 (ROOT)
		if ($categoryMapping->level > 1){
			$arrCatMap = array();
			$arrCatMap['category_path'] = str_replace(' > ', '/', $categoryMapping->n_path); //20.12.2016 KZ-002
			$arrCatMap['photo_url'] = $dtp_categoryEpsRootFolder.$categoryMapping->eps_file_path;
			
			$arrCatMap["aid_ccolwidth"] = "110.55118110236222";
			$arrCatMap['category_level'] = $categoryMapping->level;
			$arrCatMap['_caller'] = 'categoryMappingLayout';
			//error_log(print_r($arr, true));
			$categoryMappingXml = $dtpLayout->generateXml($arrCatMap); //generira XML i parsira variable_name, itd 
			
			//error_log ("Issues.php:r_catToXml 5.2: categorymappingxml: = $categoryMappingXml");
			
			$xml .= $categoryMappingXml;
		}
		else{
			Nava::poruka("<b>XML Node for Category <a href='/suva/categories-mappings/crud/$categoryMapping->id' target='_blank'>$categoryMapping->id </a>$imeGrupe $categoryMapping->n_path . skipped as it is root category (level $categoryMapping->level)</b>");
		}
		

		
		//Eliminiranje istih oglasa prema prvih 100 znakova
		$sqlIdsWhere = " id in ( 0 ";
		foreach ($adIds as $adId)
			$sqlIdsWhere .= ", $adId";
		$sqlIdsWhere .= ")";
		
		$sqlIsti = "
			select max(id) as id
			from ads
			where $sqlIdsWhere
			group by left (description_offline, 100)
		";
		$rsIsti = \Baseapp\Suva\Library\Nava::sqlToArray($sqlIsti);
		
		$sqlIdsWhere = " id in ( 0 ";
		if ($rsIsti){
			foreach ($rsIsti as $row)
				$sqlIdsWhere .= ", ".$row['id'];
		}
		$sqlIdsWhere .= ")";
		Nava::poruka ($sqlIdsWhere);
		
		//PO SVIM OGLASIMA U KATEGORIJI
		$ads = \Baseapp\Suva\Models\Ads::find ( array(
			$sqlIdsWhere 
			,'order' => 'description_offline'
			));

		foreach ($ads as $ad){
		
			if ($ad->n_source == 'avus'){
				
				if ( strpos($ad->description_offline,"###") ) {
									
					$afterLast = substr($ad->description_offline, strrpos($ad->description_offline, '#') + 1);
					$beforeFirst = strtok($ad->description_offline, "#");
					$final = '';
					$final .= $beforeFirst."<Tel>".$afterLast."</Tel>";
					$ad->description_offline = $final;
					
				}
								
				$insertions = \Baseapp\Suva\Models\InsertionsAvus::find( "ad_id = $ad->id and  issue_id = $issue->id " );
				if ( count( $insertions ) == 0 ) $issue->greska("ERR115: No insertions found for AVUS ad $ad->id!");
				foreach( $insertions as $insertion ){
					//error_log ("Issues.php:r_catToXml 5.4: insertion: = ".print_r($insertion->toArray(), true));
					$xmlInsertion = " insertion = '$insertion->id' ";
					//$orderItem = $insertion->OrderItem();
					//$product = $orderItem ? $orderItem->Product() : null;
					$product = $ad->n_avus_adlayout_id > 0 ?  \Baseapp\Suva\Models\Products::findFirst(" avus_adlayout_id = $ad->n_avus_adlayout_id ") : null;
					
					//ako nema proizvoda, istraživanje di je problem
					if ( !$product) {
						$issue->greska("ERR117: Za ad iz Avusa $ad->id (AVUS ORDER ID: $ad->n_avus_order_id ) nije pronađen proizvod (adlayout: $ad->n_avus_adlayout_id) ");
						return "";
					}

					
					$pictureProcessingMessage = '';
					$adType = '';
					Nava::poruka("Processing AVUS $adType <a href='/suva/ads2/edit/$ad->id/$ad->user_id' target='_blank'> $ad->id</a>, category <a href='/suva/categories-mappings/crud/$categoryMapping->id' target='_blank'>$categoryMapping->id </a> $categoryMapping->name, Product $product->id $product->name, . $pictureProcessingMessage");

					//dodavanje layouta proizvoda u xml
					$productLayout = $product->Layout();
					if (!$productLayout) {
						Nava::poruka ("Nije pronađen layout za product $product->id:$product->name  <a href='/suva/products/crud/$product->id' target='_blank'>Edit</a>");
						//error_log("Nije pronađen layout za product $product->id:$product->name");
						return;
					}
					
					//dodavanje xml dijela ovisno o layoutu proizvoda
					//20.12.2016- gradele na description offline
					if ($product->dtp_hashtag_before_contact_info == 1){
						$strpos = strpos( $ad->description_offline , "<Tel>");
						if ($strpos){
							
							$ad->description_offline = "POS:".$strpos." ".substr( $ad->description_offline,0, $strpos  )."###".substr( $ad->description_offline, $strpos, 9999 );
						}
					}

					$adText = $ad->id." ".$ad->description_offline;
					$arrLayoutParameters = 	array(
						'ad_text' => $adText
						,'aid_ccolwidth' => "110.55118110236222"
						,'_caller' => 'productLayout'
					); 
					$destinationFileName = str_replace(" ","_",$product->Layout()->name)."_".$foundFileName; //Dodano N
					$arrLayoutParameters ['photo_url'] = $dtp_indesignRootFolder.$zipSubFolder.$destinationFileName;
					//if ($ad->n_is_display_ad){
						$arrLayoutParameters ['photo_width'] = $product->display_ad_width;
						$arrLayoutParameters ['photo_height'] = $product->display_ad_height;
						$arrLayoutParameters ['ad_client'] = $ad->User()->username;
						$arrLayoutParameters ['ad_remark'] = $ad->n_positioning_desc;
						$arrLayoutParameters ['category_path'] = str_replace(' > ', '/', $categoryMapping->n_path); //20.12.2016
					//}
					 
					
					$productXml = $productLayout->generateXml( $arrLayoutParameters );

					//error_log ("Issues.php:r_catToXml 5.8: productxml: = $productXml");
					//$xml .= $productXml.PHP_EOL;
					$xml .= $productXml;
				}
			}
			else{
				//n_source !== 'avus'
				//$ad = \Baseapp\Suva\Models\Ads::findFirst($adId);
				//error_log ("Issues.php:r_catToXml 5.3: ad->id = $ad->id");
				//Nava::poruka("Processing ad $ad->id, categoryMapping $categoryMapping->id $categoryMapping->name ");
				//nađi sve insertione za zadani ad i issue
				$insertions = $ad->Insertions(" issues_id = $issue->id ");
				$insertions = \Baseapp\Suva\Models\Insertions::find(
					" id in 
						( 
							select max(ins.id) 
							from \Baseapp\Suva\Models\Insertions ins 
								join \Baseapp\Suva\Models\OrdersItems oi on (ins.orders_items_id = oi.id and oi.ad_id = $ad->id and ins.issues_id = $issue->id)
							group by ins.issues_id, ins.orders_items_id 
						)
						");
				if ( count($insertions) == 0 && ! ($ad->n_source == 'avus') ) $issue->greska("ERR118: No insertions found for ad $ad->id!");
				foreach( $insertions as $insertion ){
					// //error_log ("Issues.php:r_catToXml 5.4: insertion: = ".print_r($insertion->toArray(), true));
					$xmlInsertion = " insertion = '$insertion->id' ";
					$orderItem = $insertion->OrderItem();
					$product = $orderItem ? $orderItem->Product() : null;
					//Nava::poruka ("product $product->id  display $product->is_display_ad");
					//ako nema proizvoda, istraživanje di je problem
					if ( !$orderItem || !$product) {
						$oi = $insertion->OrderItem();
						if (! $oi ) {
							$issue->greska("ERR119: Za insertion $insertion->id nije pronađen order item  ");
						}
						else{
							$issue->greska("ERR120: Za insertion $insertion->id nije pronađen proizvod (na order itemu $oi->id ");
						}	
						return "";
					}
					
					// PROVJERA POSTOJE LI SLIKE NA PRAVOM MJESTU. PREBACIVANJE SLIKA ĆE IĆI U POSEBNOM KORAKU
					//if ( $product->is_picture_required == 1 || $product->is_display_ad == 1 ){  - više se ne odnoci na display adove
					
					if ( $product->is_display_ad == 1 ){
						$adType = "display ad";
						$pictureProcessingMessage = "";
						$isPhotoRequired = false;
					}
					elseif ( $product->is_picture_required == 1 ){
						$foundFileName = "ERROR FILE NOT FOUND";
						$isPhotoRequired = true;
						$isPhotoRetrievalError = true;
						if ( $product->is_picture_required == 1 ) $adType = "photo ad";
						elseif ( $product->is_display_ad == 1 ) $adType = "display ad";
						
						//Naći postoji li file sa imenom oglasa u folderu $processedFolder
						//pretraživanje pomoću wildcarda, neki glob..
						$txtSearch = $processedFolder.$ad->id.".*";
						$list = glob( $txtSearch );
						$count = count( $list );
						if ( $count == 1 ){
							//kopiranje filea u zipFolder
							//Nava::poruka ( "Picture $txtSearch found.");
							$foundFilePath = $list[0];
							$foundFileName = substr($foundFilePath, strrpos($foundFilePath, '/') + 1);
							
							$destinationFileName = str_replace(" ","_",$product->Layout()->name)."_".$foundFileName;
							
							$copyDestination = $zipFolder.$destinationFileName;
							$copySuccess = copy($foundFilePath, $copyDestination);
							if ($copySuccess ){
								$pictureProcessingMessage = "Picture <a href = '/$copyDestination' target ='_blank'>$foundFileName</a> copied to zip folder $zipFolder .";
								$isPhotoRetrievalError = false;
							}
							else{
								$pictureProcessingMessage = "<span style = 'color:red;'>ERROR: Picture $foundFileName NOT copied to zip folder $copyDestination.</span>";
							}
						}
						elseif ( $count > 1 ){
							$pictureProcessingMessage = "<span style = 'color:red;'>ERROR::Search $txtSearch returned $count results (".print_r($list,true)."). Nothing copied</span>" ;
						}
						elseif ( $count == 0 ){
							$pictureProcessingMessage = "<span style = 'color:red;'>ERROR::File $txtSearch Does not exist.</span> (product $product->id,  oi $oi->id )" ;
						}
					} 
					else{
						$adType = "text-only ad";
						$pictureProcessingMessage = "";
						$isPhotoRequired = false;
					}
					Nava::poruka("Processing $adType <a href='/suva/ads2/edit/$ad->id/$ad->user_id' target='_blank'> $ad->id</a>, category <a href='/suva/categories-mappings/crud/$categoryMapping->id' target='_blank'>$categoryMapping->id </a> $categoryMapping->name . $pictureProcessingMessage");
					
					//dodavanje layouta proizvoda u xml
					$productLayout = $product->Layout();
					if (!$productLayout) {
						Nava::poruka ("Nije pronađen layout za product $product->id:$product->name  <a href='/suva/products/crud/$product->id' target='_blank'>Edit</a>");
						//error_log("Nije pronađen layout za product $product->id:$product->name");
						return;
					}
					
					
					//dodavanje xml dijela ovisno o layoutu proizvoda
					//20.12.2016- gradele na description offline
					if ($product->dtp_hashtag_before_contact_info == 1){
						$strpos = strpos( $ad->description_offline , "<Tel>");
						if ($strpos){
							
							$ad->description_offline = "POS:".$strpos." ".substr( $ad->description_offline,0, $strpos  )."###".substr( $ad->description_offline, $strpos, 9999 );
						}
					}

					
					
					//dodavanje xml dijela ovisno o layoutu proizvoda
					$adText = $ad->id." ".$ad->description_offline;
					$arrLayoutParameters = 	array(
						'ad_text' => $adText
						,'aid_ccolwidth' => "110.55118110236222"
						,'_caller' => 'productLayout'
					); 
					if ( $isPhotoRequired ) $arrLayoutParameters ['photo_url'] = $dtp_indesignRootFolder.$zipSubFolder.$destinationFileName;
					if ($ad->n_is_display_ad){
						//20.12.2016 izmijeneno $arrLayoutParameters ['photo_url'] = "file:///".$issue->id."/display_ad_photos/".$ad->user_id."_".$ad->id.".eps";
						$arrLayoutParameters ['photo_url'] = "file://display_oglasi/".$ad->user_id."_".$ad->id.".eps";
						$arrLayoutParameters ['photo_width'] = $product->display_ad_width;
						$arrLayoutParameters ['photo_height'] = $product->display_ad_height;
						$arrLayoutParameters ['ad_client'] = $ad->User()->username;
						$arrLayoutParameters ['ad_remark'] = $ad->n_positioning_desc;
						$arrLayoutParameters ['category_path'] = str_replace(' > ', '/', $categoryMapping->n_path); //20.12.2016
					}
					$productXml = $productLayout->generateXml( $arrLayoutParameters );

					//error_log ("Issues.php:r_catToXml 5.8: productxml: = $productXml");
					//$xml .= $productXml.PHP_EOL;
					$xml .= $productXml;
				}
			}
		}  // /foreach ($adIds as $adId)
		
		//Nava::poruka(count($categoryMappingChildren). " children of Category $categoryMapping->id found. ");
		foreach ( $arrCatMap['child_cat_ids'] as $childId ){
			//error_log("Issues.php:r_catToXml 6 CHILD CATEGORY".print_r($child->toArray(), true));
			$xml .= self::r_catToXml( 
				array(
					'categoryMappingId' => $childId
					,'level' => $level + 1
					,'folder' => $p['folder']
					,'issueFolder' => $issueFolder
					,'processedFolder' => $processedFolder
					,'zipSubFolder' => $zipSubFolder
					,'categoryEpsFilesFolder' => $categoryEpsFilesFolder
					,'dtp_group_id' => $p['dtp_group_id']
					,'issueId' => $p['issueId']
					)
				, $arrCatMaps
			);
		}
		//error_log("XML ISSUES".print_r($xml,true));
		return $xml;
	}

	public static function r_catToXml($p = Array() ){

		/*
			Rekurzivna funkcija za iteraciju kroz stablo kategorija i generiranje xml-a za DTP
		*/
		//PARAMETRI IZ $p
		
		//issueId
		if ( ! array_key_exists('issueId',$p)) return Nava::poruka("Funkcija r_catToXml pozvana bez parametra issueId, <pre>".print_r($p, true)."</pre>");

		//categoryMappingId
		if (! array_key_exists('categoryMappingId',$p)) return Nava::poruka ("Issue $issue->id r_catToXml: parameter categoryMappingId not supplied.");
		
		//level
		$level = array_key_exists('level',$p) && $p['level'] > 0 ? $p['level'] : 0;
		
		//folder
		// $folder = array_key_exists('folder',$p) ? $p['folder'] : null ;
		// if (!$folder ) return Nava::poruka("Issue $issue->id r_catToXml: parameter 'folder' not supplied <pre>".print_r($p, true)."</pre>", "ERROR: Directory to put pictures not supplied.");

		//categoryEpsFilesFolder
		$categoryEpsFilesFolder = array_key_exists('categoryEpsFilesFolder',$p) ? $p['categoryEpsFilesFolder'] : null;
		if (! $categoryEpsFilesFolder ) return Nava::poruka ("Issue $issue->id r_catToXml: parameter 'categoryEpsFilesFolder' not supplied","ERROR: parameter 'categoryEpsFilesFolder' not supplied" );
		
		//issueFolder
		$issueFolder = array_key_exists('issueFolder',$p) ? $p['issueFolder'] : null;
		if (! $issueFolder ) return Nava::poruka ("Issue $issue->id r_catToXml: parameter 'issueFolder' not supplied", null);

		//processedFolder
		$processedFolder = array_key_exists('processedFolder',$p) ? $p['processedFolder'] : null;
		if (! $processedFolder ) return Nava::poruka ("Issue $issue->id r_catToXml: parameter 'processedFolder' not supplied");
		

		//zipSubFolder
		$zipSubFolder = array_key_exists('zipSubFolder',$p) ?  $p['zipSubFolder'] : null;
		if (! $zipSubFolder ) return Nava::poruka ("Issue $issue->id r_catToXml: parameter 'zipSubFolder' not supplied");

		$categoryMapping = \Baseapp\Suva\Models\CategoriesMappings::findFirst( ($p['categoryMappingId'] > 0 ? $p['categoryMappingId'] : -1) );
		if (!$categoryMapping) return Nava::greska ("ERR126: Publication Category No. ".$p['categoryMappingId']." not found");


		
		//dtp_group_id
		$txtGroup = array_key_exists('dtp_group_id', $p) && $p['dtp_group_id'] > 0 && $categoryMapping->level == 1 ? " and dtp_group_id = ".$p['dtp_group_id'] : "";


		//varijable na temelju parametara
		$issue = \Baseapp\Suva\Models\Issues::findFirst( ($p['issueId'] > 0 ? $p['issueId'] : -1) );
		if (!$issue) return Nava::greska ("ERR132: Issue ".$p['issueId']." not found");
		
		$dtpLayout = $categoryMapping->Layout();
		if (!$dtpLayout) return Nava::poruka("ERR133: Nije pronađen layout za CategoryMapping $categoryMapping->id , $categoryMapping->name, layout_id = $categoryMapping->layout_id ");
		
		//$processedFolder = $issueFolder.$processedSubFolder;
		$zipFolder = $issueFolder.$zipSubFolder;
		
		$dtp_indesignRootFolder = \Baseapp\Suva\Library\libAppSetting::get('dtp_indesignRootFolder');
		$dtp_categoryEpsRootFolder = \Baseapp\Suva\Library\libAppSetting::get('dtp_categoryEpsRootFolder');
		
		$imeGrupe = $categoryMapping->dtp_group_id ? ", GRUPA: ".$categoryMapping->DtpGroup()->name." ," : "";

		//error_log"obradjujem kategoriju $categoryMapping->id  $categoryMapping->name ");
		
		//OGLASI IZ FRONTENDA ILI BACKENDA u kategoriji 
		$adIds = array();
		
		//oglasi iz Suve
		$adIds1 = \Baseapp\Suva\Library\libPCM::getIds(
			array (
				"modelName" => "Ads"
				
				,"filters" => array (
					array(
						"modelName" => "Issues"
						,"idValue" => $issue->id
					)
					,array(
						"modelName" => "CategoriesMappings"
						,"idValue" => $categoryMapping->id
					)
					,array(
						"modelName" => "Ads"
						,"whereField" => "moderation"
						,"whereValue" =>"'ok'"
					)
				)
			)
		);
		
		foreach ($adIds1 as $adId)
			if ( in_array ($adId, $adIds) == false )
				array_push( $adIds, $adId );
		

		//Ostali Oglasi iz Avusa
		$txtSql = "
			select a.id 
			from ads a
				join n_categories_mappings cm on (a.n_avus_class_id = cm.avus_class_id and cm.id = $categoryMapping->id )
					join n_insertions_avus ia on (a.id = ia.ad_id and ia.issue_id = $issue->id)
				join n_products p on (a.n_avus_adlayout_id = p.avus_adlayout_id and p.is_display_ad = 0)
		";
		
		$rsAvusOstali = Nava::sqltoArray($txtSql);
		if ($rsAvusOstali){
			foreach($rsAvusOstali as $row){
				$adId= $row['id'];
				if ( in_array ($adId, $adIds) == false )
					array_push( $adIds, $adId );
			}
		}

		
		

		

		//Eliminiranje istih oglasa prema prvih 100 znakova
		// napravi listu svih
		$sqlIdsWhere = " id in ( 0 ";
		foreach ($adIds as $adId)
			$sqlIdsWhere .= ", $adId";
		$sqlIdsWhere .= ")";
		
		$sqlIsti = "
			select max(id) as id
			from ads
			where $sqlIdsWhere
			group by left (description_offline, 100)
		";
		 Nava::poruka ("Prije eliminarnja istih".$sqlIdsWhere);
		$rsIsti = \Baseapp\Suva\Library\Nava::sqlToArray($sqlIsti);
		
		//ADIDS2 se sastoji od ne-duplikata + display oglasa iz avusa i display oglasa iz suve
		$adIds2 = array();
		$sqlIdsWhere = " id in ( 0 ";
		if ($rsIsti){
			foreach ($rsIsti as $row){
				//$sqlIdsWhere .= ", ".$row['id'];
				array_push($adIds2, $row['id']);
			}
		}
		$sqlIdsWhere .= ")";
		 Nava::poruka ("Nakon eliminarnja istih".$sqlIdsWhere);


		

		//display oglasi iz Suve
		$rsDisplay = Nava::sqlToArray("
			select a.id
			from ads a 
				join orders_items oi on (a.id = oi.ad_id)
					join n_insertions ins on (oi.id = ins.orders_items_id and ins.issues_id = $issue->id)
			where
				n_category_mapping_id = $categoryMapping->id
				and n_is_display_ad = 1
			");
		if ( $rsDisplay ){
			foreach ( $rsDisplay as $row ){
				$adId = $row['id'];
				if ( in_array ($adId, $adIds) == false )
					array_push( $adIds2, $adId );
			}
		}


		
		//Display Oglasi iz Avusa
		$txtSql = "
			select a.id 
			from ads a
				join n_categories_mappings cm on (a.n_avus_class_id = cm.avus_class_id and cm.id = $categoryMapping->id )
					join n_insertions_avus ia on (a.id = ia.ad_id and ia.issue_id = $issue->id)
				join n_products p on (a.n_avus_adlayout_id = p.avus_adlayout_id and p.is_display_ad = 1)
		";
		//Nava::poruka($txtSql);
		$rsAvusDisplay = Nava::sqltoArray($txtSql);
		if ($rsAvusDisplay){
			foreach($rsAvusDisplay as $row){
				$adId= $row['id'];
				if ( in_array ($adId, $adIds) == false )
					array_push( $adIds2, $adId );
			}
		}

		
		  
		//DJECA Kategorije
		$categoryMappingChildren = \Baseapp\Suva\Models\CategoriesMappings::find(
			array(
				" parent_id = $categoryMapping->id and active = 1 $txtGroup",
				'order' => "sort_order asc"
				)
		);


		//ako nema djece ni oglasa, povratak 
		//error_log "count adIds = ".count($adIds).", count categoryMappingChildren = ".count($categoryMappingChildren) );
		if ( count($adIds2) == 0 && count($categoryMappingChildren) == 0 ){
			//nema ni oglasa ni podređenih kategorija, povratak
			//error_log("nula oglasa i nula kategorija, vraćam se iz kategorije $categoryMapping->id  $categoryMapping->name");
			Nava::poruka("<b>Category <a href='/suva/categories-mappings/crud/$categoryMapping->id' target='_blank'>$categoryMapping->id </a>$imeGrupe $categoryMapping->n_path . skipped as it has no ads or child categories</b>");
			return ''; 
		}
		else{
			Nava::poruka("<b>Processing category <a href='/suva/categories-mappings/crud/$categoryMapping->id' target='_blank'>$categoryMapping->id </a>$imeGrupe $categoryMapping->n_path . </b>");
		}

		
		//Generiranje XML-a
		$xml = '';

		//EPS file Kategorije
		//kopiranje filea glave kategorije u /media Naći postoji li file sa imenom oglasa u folderu $processedFolder ..pretraživanje pomoću wildcarda, neki glob..
		$txtSearch = $categoryEpsFilesFolder.$categoryMapping->eps_file_path;
		$list = glob( $txtSearch );
		$count = count( $list );
		if ( $count == 1 ){
			//kopiranje filea u zipFolder
			//Nava::poruka ( "Picture $txtSearch found.");
			$foundFilePath = $list[0];
			$foundFileName = $categoryMapping->eps_file_path;
			$copyDestination = $zipFolder.$foundFileName;
			$copySuccess = copy($foundFilePath, $copyDestination);
			if ($copySuccess ){
				$pictureProcessingMessage = "Picture <a href = '/$copyDestination' target ='_blank'>$foundFileName</a> copied to zip folder $zipFolder .";
				$isPhotoRetrievalError = false;
			}
			else{
				$pictureProcessingMessage = "<span style = 'color:red;'>ERROR: Picture $foundFileName NOT copied to zip folder $copyDestination.</span>";
			}
		}
		elseif ( $count > 1 ){
			$pictureProcessingMessage = "<span style = 'color:red;'>ERROR::Search $txtSearch returned $count results (".print_r($list,true)."). Nothing copied</span>" ;
		}
		elseif ( $count == 0 ){
			$pictureProcessingMessage = "<span style = 'color:red;'>ERROR::File $txtSearch Does not exist.</span>" ;
		}		
		
	
		
		// XML ČVOR ZA KATEGORIJU
		//ne generira se XML za kategoriju koja ima level=1 (ROOT)
		if ($categoryMapping->level > 1){
			$arrCatMap = array();
			$arrCatMap['category_path'] = str_replace(' > ', '/', $categoryMapping->n_path); //20.12.2016 KZ-002
			$arrCatMap['photo_url'] = $dtp_categoryEpsRootFolder.$categoryMapping->eps_file_path;
			
			$arrCatMap["aid_ccolwidth"] = "110.55118110236222";
			$arrCatMap['category_level'] = $categoryMapping->level;
			$arrCatMap['_caller'] = 'categoryMappingLayout';
			//error_log(print_r($arr, true));
			$categoryMappingXml = $dtpLayout->generateXml($arrCatMap); //generira XML i parsira variable_name, itd 
			
			//error_log ("Issues.php:r_catToXml 5.2: categorymappingxml: = $categoryMappingXml");
			
			$xml .= $categoryMappingXml;
		}
		else{
			Nava::poruka("<b>XML Node for Category <a href='/suva/categories-mappings/crud/$categoryMapping->id' target='_blank'>$categoryMapping->id </a>$imeGrupe $categoryMapping->n_path . skipped as it is root category (level $categoryMapping->level)</b>");
		}
		

		// Nava::poruka ("Prije eliminarnja istih".$sqlIdsWhere);
		// //Eliminiranje istih oglasa prema prvih 100 znakova
		// $sqlIdsWhere = " id in ( 0 ";
		// foreach ($adIds as $adId)
			// $sqlIdsWhere .= ", $adId";
		// $sqlIdsWhere .= ")";
		
		// $sqlIsti = "
			// select max(id) as id
			// from ads
			// where $sqlIdsWhere
			// group by left (description_offline, 100)
		// ";
		// Nava::poruka ("Prije eliminarnja istih".$sqlIdsWhere);
		// $rsIsti = \Baseapp\Suva\Library\Nava::sqlToArray($sqlIsti);
		
		// $sqlIdsWhere = " id in ( 0 ";
		// if ($rsIsti){
			// foreach ($rsIsti as $row)
				// $sqlIdsWhere .= ", ".$row['id'];
		// }
		// $sqlIdsWhere .= ")";
		// Nava::poruka ("Nakon eliminarnja istih".$sqlIdsWhere);
		
		
		$sqlIdsWhere = " id in ( 0 ";
		foreach ($adIds2 as $adId)
			$sqlIdsWhere .= ", $adId";
		$sqlIdsWhere .= ")";
		//PO SVIM OGLASIMA U KATEGORIJI
		$ads = \Baseapp\Suva\Models\Ads::find ( array(
			$sqlIdsWhere 
			,'order' => 'description_offline'
			));
			
		//OVDJE SE GENERIRA XML ZA OGLASE U JEDNOJ KATEGORIJI
		foreach ($ads as $ad){
			//NIKOLA ovde ne pojavi oglas 887276 koji je display ad i trebao bi se pojaviti u kat 5430 naslovnica
			//error_log("ID OGLASA   ".$ad->id);
		
			if ($ad->n_source == 'avus'){
				
				//Dodavanje taga <Tel> na telefonski broj kod oglasa iz avusa, ### je već dodan prije - GDJE?
				if ( strpos($ad->description_offline,"###") ) {
					$afterLast = substr($ad->description_offline, strrpos($ad->description_offline, '#') + 1);
					$beforeFirst = strtok($ad->description_offline, "#");
					$final = '';
					$final .= $beforeFirst."<Tel>".$afterLast."</Tel>";
					$ad->description_offline = $final;
				}
								
				$insertions = \Baseapp\Suva\Models\InsertionsAvus::find( "ad_id = $ad->id and  issue_id = $issue->id " );
				if ( count( $insertions ) == 0 ) $issue->greska("ERR131: No insertions found for AVUS ad $ad->id!");
				foreach( $insertions as $insertion ){
					//error_log ("Issues.php:r_catToXml 5.4: insertion: = ".print_r($insertion->toArray(), true));
					$xmlInsertion = " insertion = '$insertion->id' ";
					//$orderItem = $insertion->OrderItem();
					//$product = $orderItem ? $orderItem->Product() : null;
					$product = $ad->n_avus_adlayout_id > 0 ?  \Baseapp\Suva\Models\Products::findFirst(" avus_adlayout_id = $ad->n_avus_adlayout_id ") : null;
					
					//ako nema proizvoda, istraživanje di je problem
					if ( !$product) {
						Nava::greska("ERR130: Za ad iz Avusa $ad->id (AVUS ORDER ID: $ad->n_avus_order_id ) nije pronađen proizvod (adlayout: $ad->n_avus_adlayout_id) ");
						return "";
					}

					
					$pictureProcessingMessage = '';
					$adType = '';
					Nava::poruka("Processing AVUS $adType <a href='/suva/ads2/edit/$ad->id/$ad->user_id' target='_blank'> $ad->id</a>, category <a href='/suva/categories-mappings/crud/$categoryMapping->id' target='_blank'>$categoryMapping->id </a> $categoryMapping->name, Product $product->id $product->name, . $pictureProcessingMessage");

					//dodavanje layouta proizvoda u xml
					$productLayout = $product->Layout();
					if (!$productLayout) {
						Nava::poruka ("Nije pronađen layout za product $product->id:$product->name  <a href='/suva/products/crud/$product->id' target='_blank'>Edit</a>");
						//error_log("Nije pronađen layout za product $product->id:$product->name");
						return;
					}
					
					//dodavanje xml dijela ovisno o layoutu proizvoda
					//20.12.2016- gradele na description offline
					if ($product->dtp_hashtag_before_contact_info == 1){
						$strpos = strpos( $ad->description_offline , "<Tel>");
						if ($strpos){
							
							//$ad->description_offline = "POS:".$strpos." ".substr( $ad->description_offline,0, $strpos  )."###".substr( $ad->description_offline, $strpos, 9999 );
							$ad->description_offline = substr( $ad->description_offline,0, $strpos  )."###".substr( $ad->description_offline, $strpos, 9999 );
						}
					}

					$adText = $ad->id." ".$ad->description_offline;
					$arrLayoutParameters = 	array(
						'ad_text' => $adText
						,'aid_ccolwidth' => "110.55118110236222"
						,'_caller' => 'productLayout'
					); 
					
					//2.1.2017 - za picture oglase iz AVUS-a filename AVUS_ORDER_ID.JPG
					$fileName = self::getPictureFileName(null, $ad->id);
					$fileName.=".JPG";
					$arrLayoutParameters ['photo_url'] = $dtp_indesignRootFolder.$zipSubFolder.$fileName;
					$arrLayoutParameters ['photo_width'] = $product->display_ad_width;
					$arrLayoutParameters ['photo_height'] = $product->display_ad_height;
					$arrLayoutParameters ['ad_client'] = $ad->User()->username;
					$arrLayoutParameters ['ad_remark'] = $ad->n_positioning_desc;
					$arrLayoutParameters ['category_path'] = str_replace(' > ', '/', $categoryMapping->n_path); //20.12.2016
					
					$productXml = $productLayout->generateXml( $arrLayoutParameters );

					//error_log ("Issues.php:r_catToXml 5.8: productxml: = $productXml");
					//$xml .= $productXml.PHP_EOL;
					$xml .= $productXml;
				}
			}
			else{
				//n_source !== 'avus'
				//$ad = \Baseapp\Suva\Models\Ads::findFirst($adId);
				//error_log ("Issues.php:r_catToXml 5.3: ad->id = $ad->id");
				//Nava::poruka("Processing ad $ad->id, categoryMapping $categoryMapping->id $categoryMapping->name ");
				//nađi sve insertione za zadani ad i issue
				$insertions = $ad->Insertions(" issues_id = $issue->id ");
				$insertions = \Baseapp\Suva\Models\Insertions::find(
					" id in 
						( 
							select max(ins.id) 
							from \Baseapp\Suva\Models\Insertions ins 
								join \Baseapp\Suva\Models\OrdersItems oi on (ins.orders_items_id = oi.id and oi.ad_id = $ad->id and ins.issues_id = $issue->id)
							group by ins.issues_id, ins.orders_items_id 
						)
						");
				if ( count($insertions) == 0 && ! ($ad->n_source == 'avus') ) $issue->greska("ERR129: No insertions found for ad $ad->id!");
				foreach( $insertions as $insertion ){
					//error_log ("Issues.php:r_catToXml 5.4: insertion: = ".print_r($insertion->toArray(), true));
					$xmlInsertion = " insertion = '$insertion->id' ";
					$orderItem = $insertion->OrderItem();
					$product = $orderItem ? $orderItem->Product() : null;
					//Nava::poruka ("product $product->id  display $product->is_display_ad");
					//ako nema proizvoda, istraživanje di je problem
					if ( !$orderItem || !$product) {
						$oi = $insertion->OrderItem();
						if (! $oi ) {
							$issue->greska("ERR128: Za insertion $insertion->id nije pronađen order item  ");
						}
						else{
							$issue->greska("ERR127: Za insertion $insertion->id nije pronađen proizvod (na order itemu $oi->id ");
						}	
						return "";
					}
					
					// PROVJERA POSTOJE LI SLIKE NA PRAVOM MJESTU. PREBACIVANJE SLIKA ĆE IĆI U POSEBNOM KORAKU
					//if ( $product->is_picture_required == 1 || $product->is_display_ad == 1 ){  - više se ne odnoci na display adove
					
					if ( $product->is_display_ad == 1 ){
						$adType = "display ad";
						$pictureProcessingMessage = "";
						$isPhotoRequired = false;
					}
					elseif ( $product->is_picture_required == 1 ){
						$foundFileName = "ERROR FILE NOT FOUND";
						$isPhotoRequired = true;
						$isPhotoRetrievalError = true;
						
						if ( $product->is_picture_required == 1 ) $adType = "photo ad";
						elseif ( $product->is_display_ad == 1 ) $adType = "display ad";
						
						//Naći postoji li file sa imenom oglasa u folderu $processedFolder
						//pretraživanje pomoću wildcarda, neki glob..
						$fileName = self::getPictureFileName(  $orderItem->id );
						$txtSearch = $processedFolder.$fileName.".*";
						$list = glob( $txtSearch );
						$count = count( $list );
						if ( $count == 1 ){
							//kopiranje filea u zipFolder
							//Nava::poruka ( "Picture $txtSearch found.");
							$foundFilePath = $list[0];
							$foundFileName = substr($foundFilePath, strrpos($foundFilePath, '/') + 1);
			
							$destinationFileName = $foundFileName;
							
							$copyDestination = $zipFolder.$foundFileName;
							$copySuccess = copy($foundFilePath, $copyDestination);
							if ($copySuccess ){
								$pictureProcessingMessage = "Picture <a href = '/$copyDestination' target ='_blank'>$foundFileName</a> copied to zip folder $zipFolder .";
								$isPhotoRetrievalError = false;
							}
							else{
								$pictureProcessingMessage = "<span style = 'color:red;'>ERROR: Picture $foundFileName NOT copied to zip folder $copyDestination.</span>";
							}
						}
						elseif ( $count > 1 ){
							$pictureProcessingMessage = "<span style = 'color:red;'>ERROR::Search $txtSearch returned $count results (".print_r($list,true)."). Nothing copied</span>" ;
						}
						elseif ( $count == 0 ){
							$pictureProcessingMessage = "<span style = 'color:red;'>ERROR_::File $txtSearch Does not exist.</span> (product $product->id,  oi $orderItem->id )" ;
						}
					} 
					else{
						$adType = "text-only ad";
						$pictureProcessingMessage = "";
						$isPhotoRequired = false;
					}
					Nava::poruka("Processing $adType <a href='/suva/ads2/edit/$ad->id/$ad->user_id' target='_blank'> $ad->id</a>, category <a href='/suva/categories-mappings/crud/$categoryMapping->id' target='_blank'>$categoryMapping->id </a> $categoryMapping->name . $pictureProcessingMessage");
					
					//dodavanje layouta proizvoda u xml
					$productLayout = $product->Layout();
					if (!$productLayout) {
						Nava::poruka ("Nije pronađen layout za product $product->id:$product->name  <a href='/suva/products/crud/$product->id' target='_blank'>Edit</a>");
						//error_log("Nije pronađen layout za product $product->id:$product->name");
						return;
					}
					
					
					//dodavanje xml dijela ovisno o layoutu proizvoda
					//20.12.2016- gradele na description offline
					if ($product->dtp_hashtag_before_contact_info == 1){
						$strpos = strpos( $ad->description_offline , "<Tel>");
						if ($strpos){
							
							$ad->description_offline = "POS:".$strpos." ".substr( $ad->description_offline,0, $strpos  )."###".substr( $ad->description_offline, $strpos, 9999 );
						}
					}

					
					
					//dodavanje xml dijela ovisno o layoutu proizvoda
					$adText = $ad->id." ".$ad->description_offline;
					$arrLayoutParameters = 	array(
						'ad_text' => $adText
						,'aid_ccolwidth' => "110.55118110236222"
						,'_caller' => 'productLayout'
					); 
					if ( $isPhotoRequired ) $arrLayoutParameters ['photo_url'] = $dtp_indesignRootFolder.$zipSubFolder.$destinationFileName;
					if ($ad->n_is_display_ad){
						//20.12.2016 izmijeneno $arrLayoutParameters ['photo_url'] = "file:///".$issue->id."/display_ad_photos/".$ad->user_id."_".$ad->id.".eps";
						$arrLayoutParameters ['photo_url'] = "file://display_oglasi/".$ad->user_id."_".$ad->id.".eps";
						$arrLayoutParameters ['photo_width'] = $product->display_ad_width;
						$arrLayoutParameters ['photo_height'] = $product->display_ad_height;
						$arrLayoutParameters ['ad_client'] = $ad->User()->username;
						$arrLayoutParameters ['ad_remark'] = $ad->n_positioning_desc;
						$arrLayoutParameters ['category_path'] = str_replace(' > ', '/', $categoryMapping->n_path); //20.12.2016
					}
					$productXml = $productLayout->generateXml( $arrLayoutParameters );

					//error_log ("Issues.php:r_catToXml 5.8: productxml: = $productXml");
					//$xml .= $productXml.PHP_EOL;
					$xml .= $productXml;
				}
			}
		}  // /foreach ($adIds as $adId)
		
		//Nava::poruka(count($categoryMappingChildren). " children of Category $categoryMapping->id found. ");
		foreach ( $categoryMappingChildren as $child ){
			//error_log("Issues.php:r_catToXml 6 CHILD CATEGORY".print_r($child->toArray(), true));
			$xml .= self::r_catToXml( 
				array(
					'categoryMappingId' => $child->id
					,'level' => $level + 1
					,'folder' => $p['folder']
					,'issueFolder' => $issueFolder
					,'processedFolder' => $processedFolder
					,'zipSubFolder' => $zipSubFolder
					,'categoryEpsFilesFolder' => $categoryEpsFilesFolder
					,'dtp_group_id' => $p['dtp_group_id']
					,'issueId' => $p['issueId']
					) 
			);
		}
		//error_log("XML ISSUES".print_r($xml,true));
		return $xml;
	}
	
	public static function findOneFile( $pPath = null ){
		//traži file po pathu
		//ako nađe 1, vraća mu ime u stringu
		//inače vraća null
		
		$list = glob( $pPath );
		$count = count( $list );
		if ( $count == 1 ){
			return $list[0];
		}
		else{
			return null;
		}		
	}
	
	public static function getIssueFolderName ( \Baseapp\Suva\Models\Issues $pIssue, $pDtpGroup = null ){
		
		$issue = $pIssue;
		$dtpGroup = $pDtpGroup;
		
		//kreiranje foldera za issue u dtp: OGLT_1016-1-1_GR1
		$issueFolderTitle = $issue->Publication()->slug."_".date("Y-m-d", strtotime($issue->date_published));
		if ( $dtpGroup ) $issueFolderTitle .= "_".$dtpGroup->name;
		$issueFolder = $issueFolderTitle;
	
		return $issueFolder;
	}
	
	public static function getPublicationFolderName ( \Baseapp\Suva\Models\Publications $pPublication ){
		
		//kreiranje foldera za issue u dtp: OGLT_1016-1-1_GR1
		$publicationFolderTitle = $pPublication->slug;
		return $publicationFolderTitle;
	}
	
	public static function getToProcessFolderName (\Baseapp\Suva\Models\Issues $issue){
		
		$publication = $issue->Publication();
		
		//kreiranje foldera za publikaciju
		$publicationSubFolder = self::getPublicationFolderName ($publication);
		
		
		//po novome KZ-002  
		$toProcessSubFolder = "repository/dtp/to_process/";
		$toProcessFolder = $toProcessSubFolder.$publicationSubFolder."/";
		return $toProcessFolder;
		
	}

	public static function getProcessedFolderName ( \Baseapp\Suva\Models\Issues $issue){
		
		$publication = $issue->Publication();
		
		//kreiranje foldera za publikaciju
		$publicationSubFolder = self::getPublicationFolderName ($publication);
		
		
		//po novome KZ-002  
		$processedSubFolder = "repository/dtp/processed/";
		$processedFolder = $processedSubFolder.$publicationSubFolder."/";
		return $processedFolder;
		
	}
	
	public static function getPictureFileName ( $pOrderItemId = null , $pAvusAdId = null ){
		
		$fileName = "";		
		
		if ($pOrderItemId){
			$oi = \Baseapp\Suva\Models\OrdersItems::findFirst($pOrderItemId);
			if ($oi){
				$product = $oi->Product();
				if ($product){
					$layout = $product->Layout();
					if ($layout){
						$fileName = str_replace(" ", "_", $oi->Product()->Layout()->name)."_".$oi->ad_id ;
					}
					else{
						return Nava::greska ("ERR125: Layout not supplied for product $produst->id $product->name ");
					}
				}
				else{
					return Nava::greska ("ERR124: Product not supplied for order item $oi->id  ");
				}
			}
		}
		elseif ($pAvusAdId){
			$ad = \Baseapp\Suva\Models\Ads::findFirst($pAvusAdId);
			$fileName = "".$ad->n_avus_order_id;	
		}
		else {
			return Nava::greska ("ERR123: Parameters for function getPictureFileName not supplied order item: $pOrderItemId, ad: $pAvusAdId");
		}
		
		return $fileName;
		
		
	}

	public static function createFolderIfNotExist( $pFolderFullPath = null ){

		//provjera i kreiranje foldera za cijeli issue
		if ( ! is_dir($pFolderFullPath)){
			Nava::poruka("Folder $pFolderFullPath does not exist. It will be created. ");
			$error = Nava::folder_create($pFolderFullPath);
			if ($error === null) {
				Nava::poruka("Folder $pFolderFullPath created.");
			} else {
				Nava::greska("Folder $pFolderFullPath not created due to reason: $error");
				return -1;
			}
		}	
	}
	
	public static function deleteFolderIfExist ( $pFolderFullPath = null ){

		if ( is_dir ( $pFolderFullPath ) ) {
			Nava::poruka("Folder for ZIP file $pFolderFullPath alredy exists. Deleting.");
			$error =  Nava::r_deleteDir($pFolderFullPath);
			if ( ! $error ) {
				Nava::poruka("ERR121: Folder $pFolderFullPath deleted.");
				
			}
			else {
				Nava::greska("ERR122: Folder $pFolderFullPath not deleted due to reason: $error");
				return -1;
			}	
		}
	}
	 
	public static function fillArrCategories(  \Baseapp\Suva\Models\Issues $issue, $pDtpGroupId = null ){
	
		$catMaps = \Baseapp\Suva\Models\CategoriesMappings::find ( array( "n_publications_id = $issue->publications_id", 'order'=>'id') );
		
		$arrCatMaps = array();
		
		foreach ($catMaps as $catMap){
			$arrAdIds = \Baseapp\Suva\Library\libPCM::getIds(
				array (
					"modelName" => "Ads"
					
					,"filters" => array (
						array(
							"modelName" => "Issues"
							,"idValue" => $issue->id
						)
						,array(
							"modelName" => "CategoriesMappings"
							,"idValue" => $catMap->id
						)
						,array(
							"modelName" => "Ads"
							,"whereField" => "moderation"
							,"whereValue" =>"'ok'"
						)
					)
				)
			);
			  //error_log"IDs 111111111111 oglasa:    ".print_r($arrAdIds,true));
			//display oglasi iz Suve
			$rsDisplay = Nava::sqlToArray("
				select a.id
				from ads a 
					join orders_items oi on (a.id = oi.ad_id)
						join n_insertions ins on (oi.id = ins.orders_items_id and ins.issues_id = $issue->id)
				where
					n_category_mapping_id = $catMap->id
					and n_is_display_ad = 1
				"); 
			if ($rsDisplay){
				foreach ( $rsDisplay as $row ){
					$adId = $row['id'];
					if ( in_array ($adId, $arrAdIds) == false )
						array_push( $arrAdIds, $adId );
				}
			}
			
			//oglasi iz Avusa
			$txtSql = "
				select a.id 
				from ads a
					join n_categories_mappings cm on (a.n_avus_class_id = cm.avus_class_id and cm.id = $catMap->id )
						join n_insertions_avus ia on (a.id = ia.ad_id and ia.issue_id = $issue->id)
			";
			//Nava::poruka($txtSql);
			$rsAvus = Nava::sqltoArray($txtSql);
			if ($rsAvus){
				foreach($rsAvus as $row){
					$adId= $row['id'];
					if ( in_array ($adId, $arrAdIds) == false )
						array_push( $arrAdIds, $adId );
				}
			}
		
			//error_log"IDs 222222222222 oglasa:    ".print_r($arrAdIds,true));
			$txtAdIn = "(0 ";
			foreach ($arrAdIds as $id)
				$txtAdIn .=", $id";
			$txtAdIn .=")";	
			
			
			//lista oglasa sa slikama
			$txtSql = "
				select 
					a.id as ad_id
					,l.name as layout_name
					,am.media_id
					,a.n_source as source
					,a.n_avus_order_id as avus_id
				from ads a 
					join ( 
						select 
							oi.ad_id
							,p.layouts_id
						from orders_items oi 
							join n_insertions ins on (ins.orders_items_id = oi.id and ins.issues_id = $issue->id )
							join n_products p on ( oi.n_products_id = p.id and p.is_picture_required = 1 )
						  
						union
						
						select 
							ina.ad_id
							,p.layouts_id
						from ads a 
							join n_insertions_avus ina on (a.id = ina.ad_id and ina.issue_id = $issue->id )
							join n_products p on ( a.n_avus_adlayout_id = p.avus_adlayout_id and p.is_picture_required = 1 )
					) p1 on a.id = p1.ad_id
						left join n_layouts l on ( p1.layouts_id = l.id )
					left join ads_media am on ( am.ad_id = a.id and am.sort_idx = 1 )
				where 
					a.id in $txtAdIn
			";
			
			//error_log$txtSql);
			$arrPictureAds = array();
			$rs = Nava::sqlToArray($txtSql);
			if ($rs){
				foreach ($rs as $row){
					$source = $row['source'];
					$avusId = $row['avus_id'];
					$adId = $row['ad_id'];
					$layoutName = strlen( $row['layout_name'] ) > 0 ? $row['layout_name'] : null;
					if ( $source == 'avus' && $avusId > 0 ){
						$fileName = $avusId;
					}
					elseif ( $source != 'avus' && $layoutName ){
						$fileName = str_replace(' ', '_', $layoutName)."_".$adId;
					}
					else{
						//Greška
						$fileName = "ERROR: AD $adId - source $source, layout $layoutName"; 
						
					}

					//error_log("ADID   ".$adId);
					$arrPictureAds[$adId] = array(
						'filename' => $fileName
						,'is_attached' => ( $row['media_id'] ? true : false )
						,'source' => $source
					);
				}
			}


			//DJECA Kategorije
			$txtGroup = $pDtpGroupId && $catMap->level == 1 ? " and dtp_group_id = $pDtpGroupId " : "";

			$rs = \Baseapp\Suva\Models\CategoriesMappings::find(
				array(
					" parent_id = $catMap->id and active = 1 $txtGroup ",
					'order' => "sort_order asc"
					)
			);
			
			$arrCatChildren = array();
			foreach($rs as $row){
				array_push($arrCatChildren, $row->id);
			}
			
			$arrCatMaps [$catMap->id] = array(
				'id' => $catMap->id
				,'parent' => $catMap->parent_id
				,'child_cat_ids' => $arrCatChildren
				,'ad_ids' => $arrAdIds
				,'picture_ads' => $arrPictureAds
				,"export_to_xml" => false
			);
			
			if ($catMap->level == 1) $arrCatMaps [$catMap->id]['export_to_xml'] = true;
			
			//error_log (print_r($arrCatMaps[$catMap->id],true));
			
		}
		
		//gledanje kojeg parenta treba exportirati
		foreach ( $arrCatMaps as $key=>$cm ){
			if ( count( $cm['ad_ids'] ) > 0 ){
				
				$arrCatMaps[$key]['export_to_xml'] = true;
				
				$idLvl1 = $cm['parent'];
				if ( array_key_exists( $idLvl1 , $arrCatMaps ) ){
					$arrCatMaps[ $idLvl1 ]['export_to_xml'] = true;
					
					$idLvl2 = $arrCatMaps[ $idLvl1 ]['parent'];
					if ( array_key_exists( $idLvl2 , $arrCatMaps )){
						$arrCatMaps [ $idLvl2 ] ['export_to_xml'] = true;

						$idLvl3 = $arrCatMaps[ $idLvl2 ]['parent'];
						if ( array_key_exists( $idLvl3 , $arrCatMaps )){
							$arrCatMaps [ $idLvl3 ] ['export_to_xml'] = true;

							$idLvl4 = $arrCatMaps[ $idLvl3 ]['parent'];
							if ( array_key_exists( $idLvl4 , $arrCatMaps )){
								$arrCatMaps [ $idLvl4 ] ['export_to_xml'] = true;
							}
						}
					}
				}
			}
		}
		// //error_log (print_r($arrCatMaps, true));
		return $arrCatMaps;
		
	}
		
	public static function fillArrCategoriesToProcess(  \Baseapp\Suva\Models\Issues $issue, $pDtpGroupId = null ){
	
		$catMaps = \Baseapp\Suva\Models\CategoriesMappings::find ( array( "n_publications_id = $issue->publications_id", 'order'=>'id') );
		
		$arrCatMaps = array();
		
		foreach ($catMaps as $catMap){
			$arrAdIds = \Baseapp\Suva\Library\libPCM::getIds(
				array (
					"modelName" => "Ads"
					
					,"filters" => array (
						array(
							"modelName" => "Issues"
							,"idValue" => $issue->id
						)
						,array(
							"modelName" => "CategoriesMappings"
							,"idValue" => $catMap->id
						)
						,array(
							"modelName" => "Orders"
							,"whereField" => "n_status"
							,"whereValue" =>"1,2,3,4,5,6,7,8,9"
						)
					)
				)
			);
			  //error_log"IDs 111111111111 oglasa:    ".print_r($arrAdIds,true));
			//display oglasi iz Suve
			$rsDisplay = Nava::sqlToArray("
				select a.id
				from ads a 
					join orders_items oi on (a.id = oi.ad_id)
						join n_insertions ins on (oi.id = ins.orders_items_id and ins.issues_id = $issue->id)
				where
					n_category_mapping_id = $catMap->id
					and n_is_display_ad = 1
				");
			if ($rsDisplay){
				foreach ( $rsDisplay as $row ){
					$adId = $row['id'];
					if ( in_array ($adId, $arrAdIds) == false )
						array_push( $arrAdIds, $adId );
				}
			}
			
			//oglasi iz Avusa
			$txtSql = "
				select a.id 
				from ads a
					join n_categories_mappings cm on (a.n_avus_class_id = cm.avus_class_id and cm.id = $catMap->id )
						join n_insertions_avus ia on (a.id = ia.ad_id and ia.issue_id = $issue->id)
			";
			//Nava::poruka($txtSql);
			$rsAvus = Nava::sqltoArray($txtSql);
			if ($rsAvus){
				foreach($rsAvus as $row){
					$adId= $row['id'];
					if ( in_array ($adId, $arrAdIds) == false )
						array_push( $arrAdIds, $adId );
				}
			}
		
			//error_log"IDs 222222222222 oglasa:    ".print_r($arrAdIds,true));
			$txtAdIn = "(0 ";
			foreach ($arrAdIds as $id)
				$txtAdIn .=", $id";
			$txtAdIn .=")";	
			
			
			//lista oglasa sa slikama
			$txtSql = "
				select 
					a.id as ad_id
					,l.name as layout_name
					,am.media_id
					,a.n_source as source
					,a.n_avus_order_id as avus_id
				from ads a 
					join ( 
						select 
							oi.ad_id
							,p.layouts_id
						from orders_items oi 
							join n_insertions ins on (ins.orders_items_id = oi.id and ins.issues_id = $issue->id )
							join n_products p on ( oi.n_products_id = p.id and p.is_picture_required = 1 )
						  
						union
						
						select 
							ina.ad_id
							,p.layouts_id
						from ads a 
							join n_insertions_avus ina on (a.id = ina.ad_id and ina.issue_id = $issue->id )
							join n_products p on ( a.n_avus_adlayout_id = p.avus_adlayout_id and p.is_picture_required = 1 )
					) p1 on a.id = p1.ad_id
						left join n_layouts l on ( p1.layouts_id = l.id )
					left join ads_media am on ( am.ad_id = a.id and am.sort_idx = 1 )
				where 
					a.id in $txtAdIn
			";
			
			//error_log$txtSql);
			$arrPictureAds = array();
			$rs = Nava::sqlToArray($txtSql);
			if ($rs){
				foreach ($rs as $row){
					$source = $row['source'];
					$avusId = $row['avus_id'];
					$adId = $row['ad_id'];
					$layoutName = strlen( $row['layout_name'] ) > 0 ? $row['layout_name'] : null;
					if ( $source == 'avus' && $avusId > 0 ){
						$fileName = $avusId;
					}
					elseif ( $source != 'avus' && $layoutName ){
						$fileName = str_replace(' ', '_', $layoutName)."_".$adId;
					}
					else{
						//Greška
						$fileName = "ERROR: AD $adId - source $source, layout $layoutName"; 
						
					}

					//error_log("ADID   ".$adId);
					$arrPictureAds[$adId] = array(
						'filename' => $fileName
						,'is_attached' => ( $row['media_id'] ? true : false )
						,'source' => $source
					);
				}
			}


			//DJECA Kategorije
			$txtGroup = $pDtpGroupId && $catMap->level == 1 ? " and dtp_group_id = $pDtpGroupId " : "";

			$rs = \Baseapp\Suva\Models\CategoriesMappings::find(
				array(
					" parent_id = $catMap->id and active = 1 $txtGroup ",
					'order' => "sort_order asc"
					)
			);
			
			$arrCatChildren = array();
			foreach($rs as $row){
				array_push($arrCatChildren, $row->id);
			}
			
			$arrCatMaps [$catMap->id] = array(
				'id' => $catMap->id
				,'parent' => $catMap->parent_id
				,'child_cat_ids' => $arrCatChildren
				,'ad_ids' => $arrAdIds
				,'picture_ads' => $arrPictureAds
				,"export_to_xml" => false
			);
			
			if ($catMap->level == 1) $arrCatMaps [$catMap->id]['export_to_xml'] = true;
			
			//error_log (print_r($arrCatMaps[$catMap->id],true));
			
		}
		
		//gledanje kojeg parenta treba exportirati
		foreach ( $arrCatMaps as $key=>$cm ){
			if ( count( $cm['ad_ids'] ) > 0 ){
				
				$arrCatMaps[$key]['export_to_xml'] = true;
				
				$idLvl1 = $cm['parent'];
				if ( array_key_exists( $idLvl1 , $arrCatMaps ) ){
					$arrCatMaps[ $idLvl1 ]['export_to_xml'] = true;
					
					$idLvl2 = $arrCatMaps[ $idLvl1 ]['parent'];
					if ( array_key_exists( $idLvl2 , $arrCatMaps )){
						$arrCatMaps [ $idLvl2 ] ['export_to_xml'] = true;

						$idLvl3 = $arrCatMaps[ $idLvl2 ]['parent'];
						if ( array_key_exists( $idLvl3 , $arrCatMaps )){
							$arrCatMaps [ $idLvl3 ] ['export_to_xml'] = true;

							$idLvl4 = $arrCatMaps[ $idLvl3 ]['parent'];
							if ( array_key_exists( $idLvl4 , $arrCatMaps )){
								$arrCatMaps [ $idLvl4 ] ['export_to_xml'] = true;
							}
						}
					}
				}
			}
		}
		// //error_log (print_r($arrCatMaps, true));
		return $arrCatMaps;
		
	}	

	public static function findOneFileAndCopyToFolder ( $pFindPath, $pDestinationPath ){
		//Nava::poruka("kopira se file $pFindPath u $pDestinationPath");
		$list = glob( $pFindPath );
		//Nava::printr($list);
		$count = count( $list );
		if ( $count == 1 ){
			//Nava::poruka("count = 1");
			$foundFilePath = $list[0];
			
			$copySuccess = copy($foundFilePath, $pDestinationPath);
			
			if ($copySuccess ){
				Nava::poruka(". . . .File $foundFilePath je kopiran u $pDestinationPath");
				// error_log(". . . .File $foundFilePath je kopiran u $pDestinationPath");
				return 1;
			}
			else
				return null;
		}
		elseif ($count == 0)
			return 0;
		else
			return $list;
	}
}






	