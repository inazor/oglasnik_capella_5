<?php

namespace Baseapp\Suva;

/**
 * Suva Module
 */
class Module implements \Phalcon\Mvc\ModuleDefinitionInterface
{

    /**
     * Register a specific autoloader for the module
     *
     * @param mixed $di dependency Injector
     *
     * @return void
     */
    public function registerAutoloaders(\Phalcon\DiInterface $di = null)
    {
        $loader = new \Phalcon\Loader();
		//OVDJE SE DODAJU NOVI NAMESPACEOVI, TREBA NAVESTI NAMESPAE I DIREKTORIJ
        $loader->registerNamespaces(array(
            'Baseapp\Suva\Controllers' => __DIR__ . '/controllers/',
			'Baseapp\Suva\Library' => __DIR__ . '/library/',
			'Baseapp\Suva\Models' => __DIR__ . '/models/',
			
			
        ));

        $loader->register();
    }

    /**
     * Register specific services for the module
     *
     * @param object $di dependency Injector
     *
     * @return void
     */
    public function registerServices(\Phalcon\DiInterface $di)
    {
        // FIXME: this is still somewhat duplicated across modules, but it seems to be a core Phalcon limitation?
        $di->get('dispatcher')->setDefaultNamespace('Baseapp\Suva\Controllers');

        // NB: in theory, we could have completely different 'db' services
        // for different modules... and set frontend's db as read-only when needed,
        // while still allowing changes via backend module and such...

        // Registering the view component
        $di->setShared(
            'view',
            function() use($di) {
                $view = new \Phalcon\Mvc\View();
                $view->setViewsDir(__DIR__ . '/views/');
                $view->registerEngines(\Baseapp\Library\Tool::registerEngines($view, $di));

                // Hookup the prophiler view listener if needed
                if ($di->get('config')->app->debug && $di->has('prophiler')) {
                    $prophiler = $di->getShared('prophiler');
                    $em        = $di->get('eventsManager');

                    $em->attach('view', \Fabfuel\Prophiler\Plugin\Phalcon\Mvc\ViewPlugin::getInstance($prophiler));
                    $view->setEventsManager($em);
                }

                return $view;
            }
        );

        // Registering the simpleView component
        $di->setShared(
            'simpleView',
            function() use ($di) {
                $view = new \Phalcon\Mvc\View\Simple();
                $view->setViewsDir(__DIR__ . '/views/');
                $view->registerEngines(\Baseapp\Library\Tool::registerEngines($view, $di));
                return $view;
            }
        );
    }

}
