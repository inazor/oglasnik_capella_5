<?php

namespace Baseapp\Suva\Models;

use Baseapp\Suva\Library\Nava;
use Baseapp\Library\Utils;

use Phalcon\Di;

/**
 * Ads Model
 */
//class Ads extends BaseModelBlamable
class Ads extends \Baseapp\Models\Ads
{
    use traitBaseSuvaModel;
	


    /**
     * Get ad's user details
     */
    public function getUserDetails()
    {
        $ad_user = $this->getUser();

        $user = null;

        $builder = self::query();
        $builder->columns(array(
            'user.type',
            'user.email',
            'user.username',
            'user.phone1',
            'user.phone1_public',
            'user.phone2',
            'user.phone2_public',
            'user.company_name',
            'shop.id as shop_id',
            'loc_country.name as country_name',
            'loc_county.name as county_name'
        ))
        ->innerJoin('Baseapp\Models\Users', 'user.id = Baseapp\Suva\Models\Ads.user_id', 'user')
        ->leftJoin('Baseapp\Models\UsersShops', 'user.id = shop.user_id', 'shop')
        ->leftJoin('Baseapp\Models\Locations', 'user.country_id = loc_country.id', 'loc_country')
        ->leftJoin('Baseapp\Models\Locations', 'user.county_id = loc_county.id', 'loc_county')
        ->where(
            'Baseapp\Suva\Models\Ads.id = :ad_id:',
            array(
                'ad_id' => $this->id
            )
        )
        ->limit(1);
        $results = $builder->execute();
        if ($results && count($results) >= 1) {
            $user = $ad_user;
            $user->ads_page_url = $ad_user->get_ads_page();
            $user->shop = null;
            if ($results[0]->shop_id) {
                $shop = UsersShops::findFirst($results[0]->shop_id);
                if ($shop->isActive()) {
                    $user->shop = $shop;
                }
            }

            $user->phoneNumbers = $this->getPublicPhoneNumbers();
        }

        return $user;
    }

	

    protected static function n_get_possible_listing_options(Ads $ad, $ad_status = null, $moduleRootDir = 'nekretnin3')
    {
        $links = array();

		// if (!array_key_exists( 'moduleRootDir',$_SESSION['_context'] )){
			// $links[] = "GREŠKA nije definiran modulerootdir u Sessionu ". print_r($_SESSION['_context'],true);
			// return $links;
		// }
		// else	
			// $moduleRootDir = $_SESSION['_context']['moduleRootDir'];
		
        if (null === $ad_status) {
            $ad_status = $ad->get_status();
        }

        switch ($ad_status) {
            case 0:
                // continue with submission
                $links[] = '<button data-href="/predaja-oglasa?ad_id=' . $ad->id . '">Nastavi s predajom</button>';
                $links[] = '<button class="delete-btn btn-danger" data-toggle="modal" data-target="#deleteModal" data-formurl="/'. $moduleRootDir .'/moj-kutak/obrisi-oglas/' . $ad->id . '" data-type="delete">Obriši</button>';

                break;
            case 1:
            case 14:
            case 15:
                // active
                $links[] = '<button data-href="/'. $moduleRootDir .'/moj-kutak/uredi-oglas/' . $ad->id . '">Uredi</button>';
                $links[] = '<button data-href="/'. $moduleRootDir .'/moj-kutak/deaktiviraj-oglas/' . $ad->id . '" data-type="deactivate">Deaktiviraj</button>';
                $links[] = '<button data-href="/'. $moduleRootDir .'/moj-kutak/prodani-oglas/' . $ad->id . '" data-type="sold">Prodano</button>';

                $ads_online_product = $ad->getProduct('online');
                if ($ad_status == 1 && $ads_online_product) {
                    if ($ads_online_product->isUpgradable()) {
                        $links[] = '<button data-href="/upgrade/' . $ad->id . '">Izdvoji ovaj oglas</button>';
                    }

                    if ($ads_online_product->hasPushUp()) {
                        $links[] = '<button class="push-up-btn" data-href="/push-up/' . $ad->id . '">Skok na vrh</button>';
                    }
                }

                // ads waiting to be paid
                if ($unpaid_ad_orders = Orders::findUnpaidOrdersForAd($ad)) {
                    $latest_order = $unpaid_ad_orders[count($unpaid_ad_orders) - 1];
                    if ($latest_order) {
                        $links[] = '<button class="payment-details-btn" data-order-id="' . $latest_order->id . '">Podaci za plaćanje<span class="fa fa-search-plus fa-fw"></span></button>';
                    }
                }

                $links[] = '<button class="delete-btn btn-danger" data-toggle="modal" data-target="#deleteModal" data-formurl="/'. $moduleRootDir .'/moj-kutak/obrisi-oglas/' . $ad->id . '" data-type="delete">Obriši</button>';
                $links[] = '<a href="/'.$moduleRootDir.'/ads/view/'.$ad->id.'" class="btn btn-default">Pogledaj oglas</a>';
 
                break;
            case 2:
            case 20:
            case 21:
                // expired
                if ($ad_status != 20 && $ad->canBeRepublished()) {
                    $links[] = '<button data-href="/republish/' . $ad->id . '">Ponovi</button>';
                } else {
                    $links[] = '<button data-href="/'. $moduleRootDir .'/moj-kutak/uredi-oglas/' . $ad->id . '">Uredi</button>';
                }
                if ($ad_status == 20 && $unpaid_ad_orders = Orders::findUnpaidOrdersForAd($ad)) {
                    $latest_order = $unpaid_ad_orders[count($unpaid_ad_orders) - 1];
                    if ($latest_order && !$latest_order->isExpired()) {
                        $links[] = '<button class="payment-details-btn" data-order-id="' . $latest_order->id . '">Podaci za plaćanje<span class="fa fa-search-plus fa-fw"></span></button>';
                    }
                }

                $links[] = '<button class="delete-btn btn-danger" data-toggle="modal" data-target="#deleteModal" data-formurl="/'. $moduleRootDir .'/moj-kutak/obrisi-oglas/' . $ad->id . '" data-type="delete">Obriši</button>';
                $links[] = '<a href="/'.$moduleRootDir.'/ads/view/'.$ad->id.'" class="btn btn-default">Pogledaj oglas</a>';

                break;
            case 3:
                // deactivated
                $links[] = '<button data-href="/'. $moduleRootDir .'/moj-kutak/uredi-oglas/' . $ad->id . '">Uredi</button>';
                $links[] = '<button data-href="/'. $moduleRootDir .'/moj-kutak/aktiviraj-oglas/' . $ad->id . '" data-type="activate">Aktiviraj</button>';
                $links[] = '<button class="delete-btn btn-danger" data-toggle="modal" data-target="#deleteModal" data-formurl="/'. $moduleRootDir .'/moj-kutak/obrisi-oglas/' . $ad->id . '" data-type="delete">Obriši</button>';
                $links[] = '<a href="/'.$moduleRootDir.'/ads/view/'.$ad->id.'" class="btn btn-default">Pogledaj oglas</a>';

                break;
            case 4:
            case 41:
            case 5:
            case 6:
                // inactive
                $links[] = '<button data-href="/'. $moduleRootDir .'/moj-kutak/uredi-oglas/' . $ad->id . '">Uredi</button>';
                $links[] = '<button class="delete-btn btn-danger" data-toggle="modal" data-target="#deleteModal" data-formurl="/'. $moduleRootDir .'/moj-kutak/obrisi-oglas/' . $ad->id . '" data-type="delete">Obriši</button>';
                $links[] = '<a href="/'.$moduleRootDir.'/ads/view/'.$ad->id.'" class="btn btn-default">Pogledaj oglas</a>';

            case 41:
                // ads waiting to be paid
                if ($unpaid_ad_orders = Orders::findUnpaidOrdersForAd($ad)) {
                    $latest_order = $unpaid_ad_orders[count($unpaid_ad_orders) - 1];
                    if ($latest_order && !$latest_order->isExpired()) {
                        $links[] = '<button class="payment-details-btn" data-order-id="' . $latest_order->id . '">Podaci za plaćanje<span class="fa fa-search-plus fa-fw"></span></button>';
                    }
                }
                break;

            case 7:
            case 70:
            case 71:
                // sold
                if ($ad_status != 70 && $ad->canBeRepublished()) {
                    $links[] = '<button data-href="/republish/' . $ad->id . '">Ponovi</button>';
                } else {
                    $links[] = '<button data-href="/'. $moduleRootDir .'/moj-kutak/uredi-oglas/' . $ad->id . '">Uredi</button>';
                }
                if ($ad_status == 70 && $unpaid_ad_orders = Orders::findUnpaidOrdersForAd($ad)) {
                    $latest_order = $unpaid_ad_orders[count($unpaid_ad_orders) - 1];
                    if ($latest_order && !$latest_order->isExpired()) {
                        $links[] = '<button class="payment-details-btn" data-order-id="' . $latest_order->id . '">Podaci za plaćanje<span class="fa fa-search-plus fa-fw"></span></button>';
                    }
                }

                $links[] = '<button class="delete-btn btn-danger" data-toggle="modal" data-target="#deleteModal" data-formurl="/'. $moduleRootDir .'/moj-kutak/obrisi-oglas/' . $ad->id . '" data-type="delete">Obriši</button>';
                $links[] = '<a href="/'.$moduleRootDir.'/ads/view/'.$ad->id.'" class="btn btn-default">Pogledaj oglas</a>';
                break;
        }

        return $links;
    }

    // /**
     // * Transforms the Resultset into a regular array containing all the needed info already formatted
     // * so it could just be echo-ed in the view. Also, resulting array contains main ad images.
     // *
     // * @param Resultset $rows   Resultset we'll be working on
     // * @param string    $module Module where the ads will be displayed
     // *
     // * @return array
     // */
    public static function n_buildResultsArray(Resultset &$rows, $module = 'backend')
    {
        $results = array();

        // Bail if the Resultset ain't valid
        if (!$rows->valid()) {
            return $results;
        }

        $ids       = array();
        $media_ids = array();
        $user_ids  = array();
        $results   = array();

        $thumb_style_details = ('backend' === $module ? 'Oglas-120x90' : 'GridView');
        $no_pic = (new Media())->getNoImageThumb($thumb_style_details);

        // Init the category tree for results decoration (only once in case of repeated calls)
        static $cat_tree = null;
        if (null === $cat_tree) {
            $cat_tree = Di::getDefault()->get('categoryTree');
        }

        foreach ($rows as $k => $row) {
            /* @var Ads $ad */
            if ('backend' === $module) {
                $ad = $row->ad;
                $user_ids[] = $ad->user_id;
                $result = $ad->toArray();
                $result['user_remark'] = $row->user_remark;
                if ($row->user_shop_title) {
                    $result['user_icon'] = 'shopping-cart';
                    $result['user_username'] = $row->user_shop_title . ' (' . $row->user_username . ')';
                } else {
                    $result['user_icon'] = $row->user_type === Users::TYPE_COMPANY ? 'globe' : 'user';
                    $result['user_username'] = $row->user_username;
                }

                // format datetimes
                $result['created_at']   = $ad->created_at ? date('d.m.Y. H:i:s', $ad->created_at) : '';
                $result['published_at'] = $ad->published_at ? date('d.m.Y. H:i:s', $ad->published_at) : '';
                $result['modified_at']  = $ad->modified_at ? date('d.m.Y. H:i:s', $ad->modified_at) : '';

                $result['sort_date_formatted'] = $ad->sort_date ? date('d.m.Y. H:i:s', $ad->sort_date) : null;

                // Fill ad's status information
                $ad_status_class = 'active';
                $ad_status_text  = 'Active';

                if (!isset($ad->online_product) || empty($ad->online_product)) {
                    $ad_status_class = 'inactive';
                    $ad_status_text  = 'Draft';
                } else {
                    if ($ad->moderation == Moderation::MODERATION_STATUS_REPORTED) {
                        $ad_status_text  = 'Reported';
                    } elseif ($ad->is_spam == 1) {
                        $ad_status_class = 'spam';
                        $ad_status_text  = 'Spam';
                    } elseif ($ad->active == 0) {
                        $ad_status_class = 'inactive';
                        $ad_status_text  = 'Inactive';
                        if ($ad->isSoftDeleted()) {
                            $ad_status_class = 'deleted';
                            $ad_status_text  = 'Deleted';
                        }
                    }
                }

                $result['status_class'] = $ad_status_class;
                $result['status_text']  = $ad_status_text;

                // Fill moderation information
                $moderation_icon  = 'square-o';
                $moderation_msg   = 'Awaiting moderation';
                $moderation_class = 'text-warning';
                if ($ad->moderation === Moderation::MODERATION_STATUS_OK) {
                    $moderation_icon  = 'check-square-o';
                    $moderation_msg   = 'Moderation OK';
                    $moderation_class = 'text-success';
                } elseif ($ad->moderation === Moderation::MODERATION_STATUS_NOK) {
                    $moderation_icon  = 'minus-square-o';
                    $moderation_msg   = 'Moderation NOK';
                    $moderation_class = 'text-danger';
                } elseif (!isset($ad->online_product) || empty($ad->online_product)) {
                    $moderation_icon  = 'exclamation-triangle';
                    $moderation_msg   = 'Cannot be moderated';
                    $moderation_class = 'text-danger';
                }
                $result['moderation_icon']  = $moderation_icon;
                $result['moderation_msg']   = $moderation_msg;
                $result['moderation_class'] = $moderation_class;
                // has ad ever been reported?
                $result['reported']         = $ad->moderation == 'reported';

            } else {
                if (isset($row->ad)) {
                    $ad = $row->ad;
                } else {
                    $ad = $row;
                }
                $user_ids[] = $ad->user_id;
                $result = $ad->toArray();
                $result['price'] = $ad->price ? $ad->getPrices() : null;

                $result['online_product'] = '';
                if (!empty($ad->online_product)) {
                    $online_product = unserialize($ad->online_product);
                    if ($online_product instanceof ProductsInterface) {
                        $result['online_product'] = $online_product->getBillingTitleWithSelectedOption();
                    }
                }

                $result['offline_product'] = '';
                if (!empty($ad->offline_product) && $ad->is_offline_exportable()) {
                    $offline_product = unserialize($ad->offline_product);
                    if ($offline_product instanceof ProductsInterface) {
                        $result['offline_product'] = $offline_product->getBillingTitleWithSelectedOption();
                    }
                }

                // format datetimes
                $format                      = 'H:i d.m.Y.';
                $result['created_at']        = $ad->created_at ? date($format, $ad->created_at) : '';
                $result['published_at']      = $ad->published_at ? date($format, $ad->published_at) : '';
                $result['modified_at']       = $ad->modified_at ? date($format, $ad->modified_at) : '';
                $result['expires_at']        = $ad->expires_at ? date($format, $ad->expires_at) : '';
                $result['favorited_ad']      = isset($row->favorited_ad) ? $row->favorited_ad : null;
                $result['favorited_ad_note'] = isset($row->favorited_ad_note) ? $row->favorited_ad_note : null;

                $ad_status_class = 'muted';
                $ad_status_text = '';
                $show_link = true;
                $ad_status = $ad->get_status();
                $result['status'] = $ad_status;

                if ($ad_status == 0) {
                    $ad_status_class = 'warning';
                    $ad_status_text = 'U postupku predaje';
                    // TODO/FIXME: newly submitted ads whose Order has been cancelled by the user now also
                    // end up having status=0 -- this might become a new specific status code?
                } elseif ($ad_status == 1) {
                    $ad_status_class = 'success';
                    $ad_status_text = 'Aktivan';
                } elseif ($ad_status == 14) {
                    $ad_status_class = 'success';
                    $ad_status_text = 'Aktivan';    // (čeka moderaciju)
                } elseif ($ad_status == 15) {
                    $ad_status_class = 'success';
                    $ad_status_text = 'Aktivan <span class="text-danger">(čeka uplatu izdvajanja)</span>';
                } elseif ($ad_status == 2) {
                    $ad_status_class = 'danger';
                    $ad_status_text = 'Istekao';
                } elseif ($ad_status == 20) {
                    $ad_status_class = 'danger';
                    $ad_status_text = 'U postupku obnove (čeka uplatu)';
                } elseif ($ad_status == 21) {
                    $ad_status_class = 'danger';
                    $ad_status_text = 'Istekao (nije moguće produljiti)';
                } elseif ($ad_status == 3) {
                    $ad_status_class = 'danger';
                    $ad_status_text = 'Deaktiviran';
                } elseif ($ad_status == 4) {
                    $ad_status_class = 'danger';
                    $ad_status_text = 'Čeka moderaciju';
                    $show_link = false;
                } elseif ($ad_status == 41) {
                    $ad_status_class = 'danger';
                    $ad_status_text = 'Čeka uplatu';
                    $show_link = false;
                } elseif ($ad_status == 5) {
                    $ad_status_class = 'danger';
                    $ad_status_text = 'Odbijen na moderaciji';
                    $show_link = false;
                } elseif ($ad_status == 6) {
                    $ad_status_class = 'danger';
                    $ad_status_text = 'Odbijen zbog prijave';
                    $show_link = false;
                }

                $result['status_class'] = $ad_status_class;
                $result['status_text']  = $ad_status_text;
                $result['show_link']    = $show_link;

                // process ad options (buttons under the ad)
                $result['links'] = self::get_possible_listing_options($ad, $ad_status);

                // show payment details for ads waiting to be paid.
                if ($unpaid_ad_orders = Orders::findUnpaidOrdersForAd($ad)) {
                    $latest_order = $unpaid_ad_orders[count($unpaid_ad_orders) - 1];
                    if ($latest_order && !$latest_order->isExpired()) {
                        $result['order_id'] = $latest_order->id;
                        $result['barcode_img_src'] = $latest_order->get2DBarcodeInlineSvg();
                        // $result['bank_slip_markup'] = Orders::buildBankSlipMarkup($latest_order, $result['barcode_img_src']);
                        $result['payment_data_table_markup'] = Orders::buildPaymentDataTableMarkup($latest_order, $result['barcode_img_src']);
                        //$result['order_total_formatted'] = Utils::format_money($latest_order->getTotal(), true);
                        //$result['order_pbo'] = $latest_order->getPbo();
                        // $result['barcode_img_src'] = $latest_order->get2DBarcodeInlineImage();
                    }
                }
            }
            // if json_data exist, convert it to json object
            $result['json_data'] = trim($ad->json_data) ? json_decode(trim($ad->json_data)) : null;
            $result['frontend_url'] = $ad->get_frontend_view_link();

            // Decorate the results with extra info about the category it belongs to
            $result['category_node'] = null;
            $result['category_path'] = null;
            $cat = isset($cat_tree[$result['category_id']]) ? $cat_tree[$result['category_id']] : null;
            if ($cat) {
                $result['category_node'] = $cat;
                $result['category_path'] = $cat->path['text'];
            }

            // get short location for current row (if available)
            $rowLocationData = self::getLocationDataUsingOneQuery($ad, $shortLocation = true);
            $result['location'] = null;
            if ($rowLocationData && isset($rowLocationData['text'])) {
                $result['location'] = $rowLocationData['text'];
            }

            // Grab ad ids for later
            $ids[] = $ad->id;
            // Grab media ids also
            if ($result['json_data'] && isset($result['json_data']->ad_media_gallery) && count($result['json_data']->ad_media_gallery)) {
                $result['media_id'] = $result['json_data']->ad_media_gallery[0];
                $media_ids[]        = $result['json_data']->ad_media_gallery[0];
            } else {
                $result['media_id'] = null;
            }
            $result['thumb_pic'] = $no_pic;

            $results[$k] = $result;
        }

        $results = self::enrichResultsWithMedia($results, $media_ids, $thumb_style_details);

        if ('frontend' === $module) {
            $results = self::enrichResultsWithUsers($results, array_unique($user_ids));

            // TODO/FIXME: view counts are currently queried even if they are not displayed!
            // We should either display them even when viewType=grid (since they're fetched anyway) or look into
            // removing them completely -- it makes no sense to have them displayed only on one type of listing
            $results = self::enrichResultsWithViewCounts($results);
        }

        return $results;
    }
	
	
	
	
    public function getData()
    {
        $data = array(
            'ad_title'               => trim($this->title),
            'ad_description'         => trim($this->description),
            'ad_description_offline' => trim($this->description_offline),
            'ad_price'               => $this->getPrice(),
            'ad_price_currency_id'   => intval($this->currency_id),
            'ad_location_1'          => intval($this->country_id),
            'ad_location_2'          => intval($this->county_id),
            'ad_location_3'          => intval($this->city_id),
            'ad_location_4'          => intval($this->municipality_id)
        );
        if ($this->lat || $this->lng) {
            $ad_location_5 = array(
                (floatval($this->lat) ? $this->lat : 0),
                (floatval($this->lng) ? $this->lng : 0)
            );
            $data['ad_location_5'] = implode(',', $ad_location_5);
        }

        if ($this->AdParameters && count($this->AdParameters)) {
            foreach ($this->AdParameters as $AdParameter) {
                $key = 'ad_params_' . $AdParameter->parameter_id . ($AdParameter->level ? '_' . $AdParameter->level : '');
                if ($AdParameter->Parameter->type_id == 'CHECKBOXGROUP') {
                    $data[$key][] = $AdParameter->value;
                } elseif ($AdParameter->Parameter->type_id == 'DATEINTERVAL') {
                    $value = trim($AdParameter->value);
                    $value_array = explode(',', $value);
                    if (2 === count($value_array)) {
                        $data[$key]['from'] = $value_array[0];
                        $data[$key . '_from'] = $value_array[0];
                        $data[$key]['to'] = $value_array[1];
                        $data[$key . '_to'] = $value_array[1];
                    }
                } else {
                    $data[$key] = $AdParameter->value;
                }
            }
        }
        // if ($this->Media->valid()) {
            // $medias = $this->getMedia(array('order' => 'sort_idx ASC'));
            // foreach ($medias as $media) {
                // $data['ad_media_gallery'][] = $media->media_id;
            // }
        // }
		
		foreach ( $this->AdsMedia() as $adMedia )
			$data['ad_media_gallery'][] = $adMedia->media_id;

        return $data;
    }

 
//DODANO

	//BEFORE SAVE, AFTER SAVE
	public function n_before_save ($request = null){

		//20.12.2016 - više nije, ### se dodjeljuje u Issues
		// if ( strlen($this->description_offline) > 0 ){
			// return ($this->description_offline);
			// if (strpos( $this->description_offline, '###' ) === false ) {
				// $this->greska ("Offline Description must contain Phone number (###) ");
				// return "GREŠKA";
			// }
		// }
		return null;
	}
	
	
	public function n_after_save( $request, $pLoggedUserId = null, $pCallingObject = null ){

			
		// error_log("Ads::n_after_save 1");
		$imageName = $request->hasPost('imageUpload') ? $request->getPost('imageUpload') : null;
		//error_log($imageName);
		if($imageName > ''){
			//error_log("n_after_save::image exists");
			

			$targetPath = "repository/dtp/originals/".$imageName; // Path di je spremljena slika
			$targetPath2 = "repository/dtp/to_process/".$imageName;
			$targetPath3 = "repository/dtp/processed/".$imageName;
			$paths = array($targetPath, $targetPath2);
				
			foreach($paths as $path){
				if (file_exists($path)) {
    					copy($path, $targetPath3);	
					

				}
			}
			
		}
		
		//display oglas title
		if($request->hasPost('display_title')){
			if($request->getPost('display_title') > '' && $request->getPost('n_is_display_ad') == 1){
				Nava::runSql("update ads set title = '".$request->getPost('display_title')."' where id = $this->id");
			}
		}
				
		//n_remark polje
		if($request->hasPost('n_remark')) 
		Nava::runSql("update ads set n_remark = '".$request->getPost('n_remark')."' where id = $this->id");
		
		$checkedPublications = $request->hasPost('_checked_publications') ? $request->getPost('_checked_publications') : null;
		$selectedAds = $request->hasPost('_users_ads') ? $request->getPost('_users_ads') : null;
		$checkedOrderItems = $request->hasPost('_users_orders_items') ? $request->getPost('_users_orders_items') : null;
		$checkedAction = $request->hasPost("_context['']") ? $request->getPost('_contact_action') : null;
		$checkedDates = $request->hasPost('_checked_dates') ? $request->getPost('_checked_dates') : null;

		$checkedAction = $request->hasPost('_contact_action') ? $request->getPost('_contact_action') : null;
		$checkedContactId = $request->hasPost('_contact_id') ? $request->getPost('_contact_id') : null;
		$checkedUserId = $request->hasPost('_contact_user_id') ? $request->getPost('_contact_user_id') : null;
		$checkedType = $request->hasPost('_contact_type') ? $request->getPost('_contact_type') : null;
		$checkedValue = $request->hasPost('_contact_value') ? $request->getPost('_contact_value') : null;
		
		
		//prebacivanje slike iz media u to_process ako je učitana nova
		$adsMedia = \Baseapp\Suva\Models\AdsMedia::findFirst(" ad_id = $this->id and sort_idx = 1 ");
		if ($adsMedia){
			// error_log("Ads::n_after_save 6");
			//kopirat file u defaultni folder
			$media = \Baseapp\Suva\Models\Media::findFirst($adsMedia->media_id);
			if ($media){
				if ($media->id == $this->first_uploaded_photo_id){
					//ako već postoji onda se ništa ne radi
				}
				else {
					//ako ne postoji onda se sprema u to_process
					// error_log("Ads::n_after_save 7");
					$fileToCopy = $media->get_original_file();
					$filenameInParts = explode(".",$fileToCopy);
					$extension = $filenameInParts[count($filenameInParts)-1];
					$error = \Baseapp\Suva\Library\Nava::file_copy(
						$fileToCopy,
						'repository/dtp/to_process/',
						(string)$this->id.".".$extension
					);
					if ( $error === null ){
						// error_log("Ads::n_after_save 8");
						// error_log($media->id);						
						// error_log($txtSql);
						
						// $this->n_first_uploaded_photo_id = $media->id;
						// $this->update();
						$txtSql = "update ads set n_first_uploaded_photo_id = ".$media->id." where id = ".$this->id;
						Nava::runSql($txtSql);
						
					}
					else {
						// error_log("Ads::n_after_save 9");
						//return "ERROR FILE $media->filename not copied to folder to_process', fileToCopy: $fileToCopy, extension:$extension, due to reason: $error ";
					}
				}
			}
			else{
				$this->greska( "No media found for ad $this->id , adsMedia found, but media $adsMedia->media_id not found.", "WARNING" );
				// error_log("Ads::n_after_save 10");
				//return "ERROR NO MEDIA FOUND";
			}
		}
		
		//NIKOLA
		//dodavanje polja u tablicu AdsSearchWords
		 \Baseapp\Suva\Library\libAdsSearchWords::updateAdsSearchWordsForAd($this->id); 
		// error_log("Ads::n_after_save 11");

		
		
		if ($okMsg !== ""){
			// error_log("Ads::n_after_save 12");
			if ($pCallingObject instanceof \Baseapp\Suva\Controllers\Ads2Controller){
				$pCallingObject->poruka($okMsg);
				// error_log("Ads::n_after_save 13");
			}
		}
		if ($errMsg !== "") return $errMsg;
		// error_log("Ads::n_after_save 14 return null");
		return null;
		
	}



    //izračunata polja
    public function last_published_OrderItem( $pIncludeOnlinePublications = false ){
        if (!$this->id > 0) return null;
        
		$strIsOnline = $pIncludeOnlinePublications === true ? '' : " and p.is_online = 0 ";
		
        $max_oi_id = Nava::sqlToArray(
            "select "
            ."	max(oi.id) as max_id "
            ."from orders_items oi "
            ."	  join orders o on (oi.order_id = o.id) "
			."    join n_publications p on (oi.n_publications_id = p.id ) $strIsOnline "
            ."where "
            ."	oi.ad_id = $this->id "
            ."	and o.n_status in (1,2,3,4,5) "
        )[0]['max_id'];
// 		//error_log("last published orderitem 1");
// 		//error_log(print_r($max_oi_id,true));
		
		$id = $max_oi_id == null ? -1 : $max_oi_id;
		$result = \Baseapp\Suva\Models\OrdersItems::findFirst($id);
        //error_log("KLASA:".get_class($result));
		//error_log(count($result));
        
		return $result;
    }
	
	//provjerava je li u zadanom folderu postoji slika čije je ime ID od ad-a
	public function has_attached_picture ($pFolder = null){
		
		
		//return false;
		
		if($pFolder && $this->id){
			$picName = (string)$this->id; //ad.id
			$file_name = "$pFolder/$picName"; // file_path/ad.id(ime fajla)
// 			//error_log("Ads:has_attached_picture $file_name");
			//ovde stavi extenzije koje oces
			$files = array("$file_name.jpg", "$file_name.gif", "$file_name.png", "$file_name.eps", "$file_name.EPS");
// 			//error_log("Ads:has_attached_picture ".print_r($files, true));
			
			//idi kroz array fajlova i pogledaj da li postoji taj fajl 
			$jePostoji = false;
			foreach ($files as $file) {
				if (file_exists($file)) $jePostoji = true;
			}
			return $jePostoji;
		}
	}
    
    
    //veze many2one
    public function User(){
		return \Baseapp\Suva\Models\Users::findFirst(  ( $this->user_id > 0 ? $this->user_id : -1 ) );
    }
    
	public function User2(){
		return $this->many2one("Users", "id = $this->user_id ");
    }
 
 
	public function Category(){
        return \Baseapp\Suva\Models\Categories::findFirst($this->category_id);
    }
	public function CategoryMapping(){
        return \Baseapp\Suva\Models\CategoriesMappings::findFirst($this->n_category_mapping_id);
    }
	public function Country(){
        return \Baseapp\Suva\Models\Locations::findFirst($this->country_id);
    }
	public function County(){
        return \Baseapp\Suva\Models\Locations::findFirst($this->county_id);
    }
	public function Municipality(){
        return \Baseapp\Suva\Models\Locations::findFirst($this->municipality_id);
    }
	public function City(){
        return \Baseapp\Suva\Models\Locations::findFirst($this->city_id);
    }
	public function AdSearchTerm(){
        return \Baseapp\Suva\Models\AdsSearchTerms::findFirst($this->ad_id);
    }
	public function AdSearchWord(){
        return \Baseapp\Suva\Models\AdsSearchWords::findFirst($this->ad_id);
    }
	
	//veze one2many
	public function OrdersItems( $p = null ){
// 		if ($this->id > 0)
// 			return \Baseapp\Suva\Models\OrdersItems::find("ad_id = $this->id");
		
		$findParam = array();
		$txtWhere = '';
		if (gettype($p)=='string') $txtWhere = " and $p";
		elseif (is_array($p)){
			if (array_key_exists(0,$p)) $txtWhere = " and $p[0]";
			if (array_key_exists("order",$p)) $findParam["order"] = $p["order"];
			if (array_key_exists("limit",$p)) $findParam["limit"] = $p["limit"];
		} 
		$findParam[] = "ad_id = $this->id ".$txtWhere;
		//return \Baseapp\Suva\Models\Ads::find("user_id = $this->id");
// 		//error_log("Ads::OrdersItems 5 findParam:".print_r($findParam,true));
		return \Baseapp\Suva\Models\OrdersItems::find($findParam);
	}
	public function AdsMedia( $p = null ){
// 		if ($this->id > 0)
// 			return \Baseapp\Suva\Models\OrdersItems::find("ad_id = $this->id");
		
		$findParam = array();
		$txtWhere = '';
		if (gettype($p)=='string') $txtWhere = " and $p";
		elseif (is_array($p)){
			if (array_key_exists(0,$p)) $txtWhere = " and $p[0]";
			if (array_key_exists("order",$p)) $findParam["order"] = $p["order"];
			if (array_key_exists("limit",$p)) $findParam["limit"] = $p["limit"];
		} 
		$findParam[] = "ad_id = $this->id ".$txtWhere;
		//return \Baseapp\Suva\Models\Ads::find("user_id = $this->id");
		//error_log("Ads::OrdersItems 5 findParam:".print_r($findParam,true));
		return \Baseapp\Suva\Models\AdsMedia::find($findParam);
	}
	public function AdsParameters( $p = null ){
// 		if ($this->id > 0)
// 			return \Baseapp\Suva\Models\OrdersItems::find("ad_id = $this->id");
		
		$findParam = array();
		$txtWhere = '';
		if (gettype($p)=='string') $txtWhere = " and $p";
		elseif (is_array($p)){
			if (array_key_exists(0,$p)) $txtWhere = " and $p[0]";
			if (array_key_exists("order",$p)) $findParam["order"] = $p["order"];
			if (array_key_exists("limit",$p)) $findParam["limit"] = $p["limit"];
		} 
		$findParam[] = "ad_id = $this->id ".$txtWhere;
		//return \Baseapp\Suva\Models\Ads::find("user_id = $this->id");
		//error_log("Ads::OrdersItems 5 findParam:".print_r($findParam,true));
		return \Baseapp\Suva\Models\AdsParameters::find($findParam);
	}
	public function AdsSearchTerms( $p = null ){
// 		if ($this->id > 0)
// 			return \Baseapp\Suva\Models\OrdersItems::find("ad_id = $this->id");
		
		$findParam = array();
		$txtWhere = '';
		if (gettype($p)=='string') $txtWhere = " and $p";
		elseif (is_array($p)){
			if (array_key_exists(0,$p)) $txtWhere = " and $p[0]";
			if (array_key_exists("order",$p)) $findParam["order"] = $p["order"];
			if (array_key_exists("limit",$p)) $findParam["limit"] = $p["limit"];
		} 
		$findParam[] = "ad_id = $this->id ".$txtWhere;
		//return \Baseapp\Suva\Models\Ads::find("user_id = $this->id");
// 		//error_log("Ads::OrdersItems 5 findParam:".print_r($findParam,true));
		return \Baseapp\Suva\Models\AdsSearchTerms::find($findParam);
	}
	public function AdsSearchWords( $p = null ){
// 		if ($this->id > 0)
// 			return \Baseapp\Suva\Models\OrdersItems::find("ad_id = $this->id");
		
		$findParam = array();
		$txtWhere = '';
		if (gettype($p)=='string') $txtWhere = " and $p";
		elseif (is_array($p)){
			if (array_key_exists(0,$p)) $txtWhere = " and $p[0]";
			if (array_key_exists("order",$p)) $findParam["order"] = $p["order"];
			if (array_key_exists("limit",$p)) $findParam["limit"] = $p["limit"];
		} 
		$findParam[] = "ad_id = $this->id ".$txtWhere;
		//return \Baseapp\Suva\Models\Ads::find("user_id = $this->id");
// 		//error_log("Ads::OrdersItems 5 findParam:".print_r($findParam,true));
		return \Baseapp\Suva\Models\AdsSearchWords::find($findParam);
	}	
	
	
//Simulirana one-2-many jer je na insertions dodano trigger-polje auto_ad_id
	public function Insertions( $p = null ){
 		$id = $this->id > 0 ? $this->id : -1;
		
		$findParam = array();
		$txtWhere = '';
		if (gettype($p)=='string') $txtWhere = " and $p";
		elseif (is_array($p)){
			if (array_key_exists(0,$p)) $txtWhere = " and $p[0]";
			if (array_key_exists("order",$p)) $findParam["order"] = $p["order"];
			if (array_key_exists("limit",$p)) $findParam["limit"] = $p["limit"];
		} 
		$findParam[] = "is_active = 1 and auto_ads_id = $id ".$txtWhere;
		//error_log("Ads::Insertions 5 findParam:".print_r($findParam,true));
		return \Baseapp\Suva\Models\Insertions::find($findParam);
	}
	
	//simulirane one2many
	public function CategoriesMappings(){
		$id = $this->id > 0 ? $this->id : -1;
		return \Baseapp\Suva\Library\libPCM::getCategoriesMappingsForAd ($id);
	}
	
	

	public function getStatusText($p = null){
		
		// Fill ad's status information
		$ad_status_class = 'active';
		$ad_status_text  = 'Active';
		
		if (!isset($this->online_product) || empty($this->online_product)) {
			$ad_status_class = 'inactive';
			$ad_status_text  = 'Draft';
		} else {
			if ($this->moderation == Moderation::MODERATION_STATUS_REPORTED) {
				$ad_status_text  = 'Reported';
			} elseif ($this->is_spam == 1) {
				$ad_status_class = 'spam';
				$ad_status_text  = 'Spam';
			} elseif ($this->active == 0) {
				$ad_status_class = 'inactive';
				$ad_status_text  = 'Inactive';
				if ($this->isSoftDeleted()) {
					$ad_status_class = 'deleted';
					$ad_status_text  = 'Deleted';
				}
			}
		}
		
		return $ad_status_text;
	}
	public function slugify ( $pText = null) {
		return Utils::slugify($pText);
		
	}
	public function get_nekretnin3_view_link($type = 'href'){
        $link = null;

        if (isset($this->id)) {
            //  server.domain.name/[category_slug]/[ad-title]-oglas-[ad-id]

            $category_slug = $this->getCategory()->url;
            $ad_title_slug = Utils::slugify($this->title);

            $url  = 'nekretnin3/' . $category_slug . '/' . $ad_title_slug . '-oglas-' . $this->id;

            // Use the 'url' resolver service if present, making it more flexible
            $di = $this->getDI();
            if ($di->has('url')) {
                $url = $di->get('url')->get('nekretnin3/' . $category_slug . '/' . $ad_title_slug . '-oglas-' . $this->id);
            }

            $link = $url;

            if ('html' === $type) {
                $markup = '<a href="%s" target="_blank"><i class="fa fa-fw fa-eye"></i>%s</a>';
                $link   = sprintf($markup, $url, 'View ad');
            }
        }

        return $link;
    }
	
	public function get_nekretnin_view_link($type = 'href'){
        $link = null;

        if (isset($this->id)) {
            //  server.domain.name/[category_slug]/[ad-title]-oglas-[ad-id]

            $category_slug = $this->getCategory()->url;
            $ad_title_slug = Utils::slugify($this->title);

            $url  = 'nekretnin/' . $category_slug . '/' . $ad_title_slug . '-oglas-' . $this->id;

            // Use the 'url' resolver service if present, making it more flexible
            $di = $this->getDI();
            if ($di->has('url')) {
                $url = $di->get('url')->get('nekretnin/' . $category_slug . '/' . $ad_title_slug . '-oglas-' . $this->id);
            }

            $link = $url;

            if ('html' === $type) {
                $markup = '<a href="%s" target="_blank"><i class="fa fa-fw fa-eye"></i>%s</a>';
                $link   = sprintf($markup, $url, 'View ad');
            }
        }

        return $link;
    }
	public function get_najam_view_link($type = 'href'){
        $link = null;

        if (isset($this->id)) {
            //  server.domain.name/[category_slug]/[ad-title]-oglas-[ad-id]

            $category_slug = $this->getCategory()->url;
            $ad_title_slug = Utils::slugify($this->title);

            $url  = 'najam/' . $category_slug . '/' . $ad_title_slug . '-oglas-' . $this->id;

            // Use the 'url' resolver service if present, making it more flexible
            $di = $this->getDI();
            if ($di->has('url')) {
                $url = $di->get('url')->get('najam/' . $category_slug . '/' . $ad_title_slug . '-oglas-' . $this->id);
            }

            $link = $url;

            if ('html' === $type) {
                $markup = '<a href="%s" target="_blank"><i class="fa fa-fw fa-eye"></i>%s</a>';
                $link   = sprintf($markup, $url, 'View ad');
            }
        }

        return $link;
    }
	public static function buildNajamViewLink(\stdClass $row, $type = 'href'){
        $link = null;

        if (isset($row->id)) {
            //  server.domain.name/[category_slug]/[ad-title]-oglas-[ad-id]

            $di = Di::getDefault();
            $tree = $di->get('categoryTree');
            $category_slug = $tree[$row->category_id]->url;
            $ad_title_slug = Utils::slugify($row->title);

            $url  = '/najam/' . $category_slug . '/' . $ad_title_slug . '-oglas-' . $row->id;

            // Use the 'url' resolver service if present, making it more flexible
            if ($di->has('url')) {
                $url = $di->get('url')->get('/najam/' . $category_slug . '/' . $ad_title_slug . '-oglas-' . $row->id);
            }

            $link = $url;

            if ('html' === $type) {
                $markup = '<a href="%s" target="_blank"><span class="fa fa-fw fa-eye"></span>%s</a>';
                $link   = sprintf($markup, $url, 'View ad');
            }
        }

        return $link;
    }
	
	
	public static function buildNekretnin3ViewLink(\stdClass $row, $type = 'href'){
        $link = null;

        if (isset($row->id)) {
            //  server.domain.name/[category_slug]/[ad-title]-oglas-[ad-id]

            $di = Di::getDefault();
            $tree = $di->get('categoryTree');
            $category_slug = $tree[$row->category_id]->url;
            $ad_title_slug = Utils::slugify($row->title);

            $url  = '/nekretnin3/' . $category_slug . '/' . $ad_title_slug . '-oglas-' . $row->id;

            // Use the 'url' resolver service if present, making it more flexible
            if ($di->has('url')) {
                $url = $di->get('url')->get('/nekretnin3/' . $category_slug . '/' . $ad_title_slug . '-oglas-' . $row->id);
            }

            $link = $url;

            if ('html' === $type) {
                $markup = '<a href="%s" target="_blank"><span class="fa fa-fw fa-eye"></span>%s</a>';
                $link   = sprintf($markup, $url, 'View ad');
            }
        }

        return $link;
    }
	
	public static function buildNekretninViewLink(\stdClass $row, $type = 'href'){
        $link = null;

        if (isset($row->id)) {
            //  server.domain.name/[category_slug]/[ad-title]-oglas-[ad-id]

            $di = Di::getDefault();
            $tree = $di->get('categoryTree');
            $category_slug = $tree[$row->category_id]->url;
            $ad_title_slug = Utils::slugify($row->title);

            $url  = '/nekretnin/' . $category_slug . '/' . $ad_title_slug . '-oglas-' . $row->id;

            // Use the 'url' resolver service if present, making it more flexible
            if ($di->has('url')) {
                $url = $di->get('url')->get('/nekretnin/' . $category_slug . '/' . $ad_title_slug . '-oglas-' . $row->id);
            }

            $link = $url;

            if ('html' === $type) {
                $markup = '<a href="%s" target="_blank"><span class="fa fa-fw fa-eye"></span>%s</a>';
                $link   = sprintf($markup, $url, 'View ad');
            }
        }

        return $link;
    }
	
	
	public function getPublications(){
		if (!$this->id > 0) return null;
		
		/* vraća popis publikacija koje imaju mapirane kategorije tako da se prikazuje ovaj ad*/
		
		$txtSql = "
		
select 
	p2.publication_id
from (

	select 
		pcm.publication_id
	from
		ads a 
			join n_publications_categories_mappings pcm on (a.category_id = pcm.mapped_category_id and 	a.category_id = pcm.mapped_category_id and pcm.is_active = 1 )
			
	where
		a.id = $this->id
		
	union

	select 
		pcm.publication_id
	from
		ads a 
			join n_publications_categories_mappings pcm on (a.country_id = pcm.mapped_location_id and	a.category_id = pcm.mapped_category_id and pcm.is_active = 1 )
			
	where
		a.id = $this->id

	union

	select 
		pcm.publication_id
	from
		ads a 
			join n_publications_categories_mappings pcm on (a.county_id = pcm.mapped_location_id and	a.category_id = pcm.mapped_category_id and pcm.is_active = 1 )
			
	where
		a.id = $this->id
		
		
	union


	select 
		pcm.publication_id
	from
		ads a 
			join n_publications_categories_mappings pcm on (a.municipality_id = pcm.mapped_location_id and	a.category_id = pcm.mapped_category_id and pcm.is_active = 1 )
			
	where
		a.id = $this->id
		
	union


	select 
		pcm.publication_id
	from
		ads a 
			join n_publications_categories_mappings pcm on (a.city_id = pcm.mapped_location_id and	a.category_id = pcm.mapped_category_id and pcm.is_active = 1 )
			
	where
		a.id = $this->id
		
	union

	select 
		pcm.publication_id
	from
		ads a 
			join ads_parameters ap on (ap.ad_id = a.id)
				join parameters p on (p.id = ap.parameter_id)
					join parameters_types pt on (pt.id = p.type_id and pt.accept_dictionary = 1)
				join n_publications_categories_mappings pcm on ( pcm.mapped_dictionary_id = cast(ap.value as UNSIGNED) )
						
	where
		a.id = $this->id
		and	a.category_id = pcm.mapped_category_id 	
		 and pcm.is_active = 1
) p2 
	join n_publications p on (p2.publication_id = p.id and p.is_active = 1)
		";
	
	//error_log($txtSql);
	
	$rs = \Baseapp\Suva\Library\Nava::sqlToArray( $txtSql );
	$txtWhere = " id in (0 ";
	
	foreach ( $rs as $row)
		$txtWhere .= ", ".$row['publication_id'];
	
	$txtWhere .= ")";
	
	$publications = \Baseapp\Suva\Models\Publications::find($txtWhere);
	
	return $publications;
	
	}
	
	public function getCategoryMappingForPublication( $pPublicationId = null ){
		//return $this->category_id;
		/* nać iz pcm i cm na koji cm se odnosi taj ad */
		
		if (!$this->id > 0) return null;
		if (!$pPublicationId > 0) return null;
		
		
		// $txtSql = "
		// select max(category_mapping_id) as id from(
		
		// select 
			// pcm.category_mapping_id
		// from
			// ads a 
				// join n_publications_categories_mappings pcm on (a.category_id = pcm.mapped_category_id)
				
		// where
			// a.id = $this->id and pcm.publication_id = $pPublicationId
			
		// union

		// select 
			// pcm.category_mapping_id
		// from
			// ads a 
				// join n_publications_categories_mappings pcm on (a.country_id = pcm.mapped_location_id)
				
		// where
			// a.id = $this->id and pcm.publication_id = $pPublicationId

		// union

		// select 
			// pcm.category_mapping_id
		// from
			// ads a 
				// join n_publications_categories_mappings pcm on (a.county_id = pcm.mapped_location_id)
				
		// where
			// a.id = $this->id and pcm.publication_id = $pPublicationId
			
			
		// union


		// select 
			// pcm.category_mapping_id
		// from
			// ads a 
				// join n_publications_categories_mappings pcm on (a.municipality_id = pcm.mapped_location_id)
				
		// where
			// a.id = $this->id and pcm.publication_id = $pPublicationId
			
		// union


		// select 
			// pcm.category_mapping_id
		// from
			// ads a 
				// join n_publications_categories_mappings pcm on (a.city_id = pcm.mapped_location_id)
				
		// where
			// a.id = $this->id and pcm.publication_id = $pPublicationId
			
		// union

		// select 
			// pcm.category_mapping_id
		// from
			// ads a 
				// join ads_parameters ap on (ap.ad_id = a.id)
					// join parameters p on (p.id = ap.parameter_id)
						// join parameters_types pt on (pt.id = p.type_id and pt.accept_dictionary = 1)
					// join n_publications_categories_mappings pcm on ( pcm.mapped_dictionary_id = cast(ap.value as UNSIGNED) )
							
		// where
			// a.id = $this->id and pcm.publication_id = $pPublicationId
		
		// ) as pcmi
		
		// ";
		
		
		$txtSql = "
select  pcm.category_mapping_id as id from (
 

/* cat dict loc 8 */
select 
	a.id adId, pcm.id as pcmID, 8 as priority
from
	ads a
		 
		join ads_parameters ap on (ap.ad_id = a.id)
			join parameters p on (p.id = ap.parameter_id)
				join parameters_types pt on (pt.id = p.type_id and pt.accept_dictionary = 1)
					join n_publications_categories_mappings pcm on ( 
						pcm.publication_id = $pPublicationId
						and pcm.mapped_category_id = a.category_id
						and pcm.mapped_dictionary_id = cast(ap.value as UNSIGNED) 
						and pcm.mapped_location_id in (
								a.country_id
								, a.county_id
								, a.municipality_id
								, a.city_id 
						)
					)
where
	a.id = $this->id 

union
/* cat  loc */
select 
	a.id, pcm.id, 5 as priority
from
	ads a
		join n_publications_categories_mappings pcm on ( 
			pcm.publication_id = $pPublicationId
			and pcm.mapped_category_id = a.category_id
			and pcm.mapped_dictionary_id is null  
			and pcm.mapped_location_id in (
					a.country_id
					, a.county_id
					, a.municipality_id
					, a.city_id 
			)
		)
where
	a.id = $this->id 

union

/* cat dict */
select 
	a.id, pcm.id, 6 as priority
from
	ads a
		 
		join ads_parameters ap on (ap.ad_id = a.id)
			join parameters p on (p.id = ap.parameter_id)
				join parameters_types pt on (pt.id = p.type_id and pt.accept_dictionary = 1)
					join n_publications_categories_mappings pcm on ( 
						pcm.publication_id = $pPublicationId
						and pcm.mapped_category_id = a.category_id
						and pcm.mapped_dictionary_id = cast(ap.value as UNSIGNED) 
						and pcm.mapped_location_id is null 
					)

where
	a.id = $this->id 

union

/* dict loc */
select 
	a.id, pcm.id, 3 as priority
from
	ads a
		 
		join ads_parameters ap on (ap.ad_id = a.id)
			join parameters p on (p.id = ap.parameter_id)
				join parameters_types pt on (pt.id = p.type_id and pt.accept_dictionary = 1)
					join n_publications_categories_mappings pcm on ( 
						pcm.publication_id = $pPublicationId
						and pcm.mapped_category_id is null 
						and pcm.mapped_dictionary_id = cast(ap.value as UNSIGNED) 
						and pcm.mapped_location_id in (
								a.country_id
								, a.county_id
								, a.municipality_id
								, a.city_id 
						)
					)
where
	a.id = $this->id 

union


/* cat */
select 
	a.id, pcm.id, 4 as priority
from
	ads a
		join n_publications_categories_mappings pcm on ( 
			pcm.publication_id = $pPublicationId
			and pcm.mapped_category_id = a.category_id
			and pcm.mapped_dictionary_id is null  
			and pcm.mapped_location_id is null 
		)				
where
	a.id = $this->id 


union 

/* dict */
select 
	a.id, pcm.id, 2 as priority
from
	ads a
		 
		join ads_parameters ap on (ap.ad_id = a.id)
			join parameters p on (p.id = ap.parameter_id)
				join parameters_types pt on (pt.id = p.type_id and pt.accept_dictionary = 1)
					join n_publications_categories_mappings pcm on ( 
						pcm.publication_id = $pPublicationId
						and pcm.mapped_category_id is null 
						and pcm.mapped_dictionary_id = cast(ap.value as UNSIGNED) 
						and pcm.mapped_location_id is null 
					)
where
	a.id = $this->id 


union

/* loc */
select 
	a.id, pcm.id, 1 as priority
from
	ads a
		join n_publications_categories_mappings pcm on ( 
			pcm.publication_id = $pPublicationId
			and pcm.mapped_category_id = a.category_id
			and pcm.mapped_dictionary_id is null  
			and pcm.mapped_location_id in (
					a.country_id
					, a.county_id
					, a.municipality_id
					, a.city_id 
			)
		)
where
	a.id = $this->id 
) p1
join n_publications_categories_mappings pcm on (p1.pcmID = pcm.id)
order by priority desc, pcmId desc
limit 1

		
		";
		//Nava::poruka("Ads::getCategoryMappingForPublication sql<pre> $txtSql </pre>");
		//error_log('Ads::getCategoryMappingForPublication 1');
		$rs = \Baseapp\Suva\Library\Nava::sqltoArray( $txtSql );
		$row = $rs[0];
		$result = $row['id'];
		//error_log('Ads::getCategoryMappingForPublication 2 ' .$result);
		return $result;
		
		
	}
	
}



