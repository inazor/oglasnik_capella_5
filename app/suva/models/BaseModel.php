<?php

namespace Baseapp\Suva\Models;

// use Baseapp\Bootstrap;
// use Phalcon\Di;
// use Phalcon\Mvc\Model\Behavior\Timestampable;
// use Phalcon\Mvc\Model;
// use Baseapp\Library\DateUtils;

//DODANO
use Baseapp\Suva\Library\Nava;

/**
 * A BaseModel other models can inherit in order to keep code duplication
 * within models under control.
 *
 * Of course, since we're in PHP land, overridden child methods have to explicitly
 * call parent:: methods that they override, if they wish to execute code defined in the parent
 * (aka, PHP doesn't do it automatically since it would be considered 'tightly coupled')
 */

//abstract class BaseModel extends Model
abstract class BaseModel extends \Baseapp\Models\BaseModel
{
	use traitBaseSuvaModel;

	
	// //IN:
	// public function initialize (){
		// error_log ("inicijalizira");
		// $result = parent::initialize();
		
		// //error_log(get_class($result));
		// //error_log(print_r($result->toArray(), true));
		
		// return $result;
		
	// }
	
	
	public function initialize_model_with_post($request){
		//error_log("BaseModel:initialize_model_with_post 1 ");
		$fields = $this->n_fields_list($this->getSource());
		$types = $this->n_field_types($this->getSource());
		$nullables = $this->n_is_nullable($this->getSource());
		//error_log("BaseModel:initialize_model_with_post 2.1 ".$this->getSource());

		foreach($fields as $field){
				//error_log("BaseModel:initialize_model_with_post 2 ");
				$family = $this->n_data_family($types[$field]);
				if ($request->hasPost($field)){
						//error_log("BaseModel:initialize_model_with_post 3 ");
						$requestValue = $request->getPost($field);
						$type = $types[$field];
						$nullable = $nullables[$field];
						$modelValue = $this->n_interpret_value_from_request($requestValue, $type, $nullable);
						//error_log("BaseModel:initialize_model_with_post field:$field, requestValue:$requestValue, type:$type, modelValue:$modelValue ");
						$this->assign(array($field =>$modelValue));
				}
		}	
	}
	


	//debug funkcije update, create
	public function n_update(){
		$success = $this->update();
		if ($success === false){
			$greske = $this->getMessages();
			foreach ($greske as $greska)  
				$err_msg .= $greska.", ";
			return $err_msg;
		}
		else return null;
	}
	public function n_create(){
		$success = $this->create();
		if ($success === false){
			$greske = $this->getMessages();
			foreach ($greske as $greska)  
				$err_msg .= $greska.", ";
			return $err_msg;
		}
		else return null;
	}
	
	
    //GENERIČKE FUNKCIJE 
	public function n_before_save( $p = null ){

		}
	public function n_after_save( $p = null  ){

		}
    
    //FUNKCIJE ZA DOHVAT POLJA, STRANIH KLJUČEVA I SL
	public function n_set_default_values($pTableName = null){
		if(!$pTableName) $pTableName = $this->getSource();
		$defaults = \Baseapp\Suva\Library\Nava::sqlToArray(
			"select COLUMN_NAME, COLUMN_DEFAULT	"
			." FROM information_schema.columns "
			." where TABLE_NAME = '$pTableName' and COLUMN_DEFAULT IS NOT NULL "
			." group by COLUMN_NAME "
		);
		$arrAssign = array();
		foreach ($defaults as $default)
			$arrAssign[$default['COLUMN_NAME']] = $default['COLUMN_DEFAULT'];
		$this->assign($arrAssign);
		
	}

    public function n_fields_list($pTableName = null){
        $rows = Nava::sqlToArray("select COLUMN_NAME from information_schema.columns where TABLE_NAME = '$pTableName' group by COLUMN_NAME ");
        $result = array();
        foreach($rows as $row)
            array_push($result, $row['COLUMN_NAME']);
        return $result;
    }

	public function n_field_types($pTableName = null){
		$rows = Nava::sqlToArray("select COLUMN_NAME, DATA_TYPE from information_schema.columns where TABLE_NAME = '$pTableName' group by COLUMN_NAME ");
		$result = array();
		foreach($rows as $row)
			$result[$row['COLUMN_NAME']] = $row['DATA_TYPE'];
		return $result;
	}    

	public function n_is_nullable($pTableName = null){
		$rows = Nava::sqlToArray("select COLUMN_NAME, IS_NULLABLE from information_schema.columns where TABLE_NAME = '$pTableName' group by COLUMN_NAME ");
		$result = array();
		foreach($rows as $row)
			$result[$row['COLUMN_NAME']] = $row['IS_NULLABLE'] === 'YES' ? true : false;
		return $result;
	} 
	
    public function n_foreign_keys($pTableName = null){
      $rows = Nava::sqlToArray("select COLUMN_NAME, REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME from information_schema.key_column_usage where TABLE_NAME = '$pTableName'  and REFERENCED_TABLE_NAME is not null");
			$result = array();
			foreach($rows as $row)
				$result[$row['COLUMN_NAME']] = $row;
			return $result;
    }
    
   
	public function n_data_family($pDataType){
		 
		switch($pDataType){
			case 'bigint':
			case 'int':
			case 'smallint':
			case 'mediumint':
			case 'tinyint':
				return "integer";
			case 'decimal':
			case 'double':
				return "decimal";
			case 'varchar':
			case 'char':
			case 'varchar':
			case 'longtext':
			case 'mediumtext':
			case 'text':
			case 'longtext':
			case 'enum':
			case 'set':
				return "textual";
			case 'date':
			case 'datetime':
				return 'date';
			default:
				return "unknown";
		}
		
	}	 
		
	public function n_interpret_value_from_request($pValue, $pFieldType,  $pIsNullable = false){
		
		$family = $this->n_data_family($pFieldType);
		//error_log ("BaseModel.php: n_interpret_value_from_request: value $pValue, fieldType $pFieldType ");
		if (gettype($pValue) === 'NULL') $result = null;
		elseif ($pValue === '') $result = null;
		elseif ( ($family === 'integer' ||  $family === 'decimal' || $family === 'date' ) && $pValue === '') $result = null;
		elseif ($family === 'integer' && is_numeric(trim($pValue)) && (int)trim($pValue) == trim($pValue)) $result = (int)($pValue);
		elseif ($family === 'decimal' && is_numeric(trim($pValue)) && (float)trim($pValue) == trim($pValue)) $result = (float)($pValue);
		elseif ($family === 'textual' && strlen($pValue) == 0 && $pIsNullable ) $result = null;
		elseif ($family === 'textual' && gettype($pValue) === 'string') $result = $pValue;
		elseif ($family === 'date'){
				$pValue = str_replace('/','.', $pValue); // radi 2015/01/01 - da ide u 2015.01.01
				$pValue = str_replace('-','.', $pValue); // radi 2015-01-01 - da ide u 2015.01.01
				$pValue = str_replace('T',' ', $pValue); // radi 2015-01-01T08:30
				$dmyhms = explode(" ", trim($pValue));
				if ( count($dmyhms) == 1 ) $dmy = explode(".", trim($pValue));
				elseif ( count($dmyhms) == 2 ) $dmy = explode(".", trim($dmyhms[0]));
				else return null;
			
				//$dmy = explode(".", trim($pValue));
				if (count($dmy) != 3) return null;
				
				// 25.02.2015
				if ((int)$dmy[0] > 0 && (int)$dmy[0] < 32 && (int)$dmy[1] > 0 && (int)$dmy[1] < 13 && (int)$dmy[2] > 1969  && (int)$dmy[2] < 9999) 
						$result = "".$dmy[2]."-".$dmy[1]."-".$dmy[0];
				// 2015.02.25
				if ((int)$dmy[2] > 0 && (int)$dmy[2] < 32 && (int)$dmy[1] > 0 && (int)$dmy[1] < 13 && (int)$dmy[0] > 1969  && (int)$dmy[0] < 9999) 
				$result = "".$dmy[0]."-".$dmy[1]."-".$dmy[2]."";
				
				//08:00:00
				if ( count($dmyhms) == 2 ) $result = $result." ".trim($dmyhms[1]);
				
				//error_log("n_interpret_value_from_request date ".$result);
				
		} 
		else $result = null;
		
		return $result;
	}
    
	public function n_dateUs2DateHr ($pDate = null){
		return $this->n_dateTimeUs2DateHr($pDate);
	}
		
	public function n_dateTimeUs2DateHr ($pDate = null){
		
		$pDate = str_replace('-','.', $pDate);
		$pDate = str_replace('/','.', $pDate);
		$pDate = str_replace(':','.', $pDate);
		$pDate = str_replace(' ','.', $pDate);
		
		$result = "";
		$dmy = explode(".", trim($pDate));
		if (count($dmy) < 3) return null;
		if ((int)$dmy[2] > 0 && (int)$dmy[2] < 32 && (int)$dmy[1] > 0 && (int)$dmy[1] < 13 && (int)$dmy[0] > 1969  && (int)$dmy[0] < 9999) 
		$result = "".$dmy[2].".".$dmy[1].".".$dmy[0]."";
		return $result;
	}
		
	public function n_dateTimeUs2DateTimeHr ($pDate = null){
		$pDate = str_replace('-','.', $pDate);
		$pDate = str_replace('/','.', $pDate);
		$pDate = str_replace(':','.', $pDate);
		$pDate = str_replace(' ','.', $pDate);
		
		$dmy = explode(".", trim($pDate));
		if (count($dmy) < 3) return null;
		if ((int)$dmy[2] > 0 && (int)$dmy[2] < 32 && (int)$dmy[1] > 0 && (int)$dmy[1] < 13 && (int)$dmy[0] > 1969  && (int)$dmy[0] < 9999) 
		$result = "".$dmy[2].".".$dmy[1].".".$dmy[0]." ".$dmy[3].":".$dmy[4].":".$dmy[5];
		return $result;
	}
    
	//many2One  
	public function many2one($pModelName = null, $p = null){
		/*korištenje
			npr $ad->many2One("Users", "user_id");
		*/
		
		if (!$pModelName || !$p) return null;
	
		$id = intval($p);
	
		$strWhere = is_numeric($p) ? " id = ".intval($p) : null;
		$strWhere = !$strWhere ? $p : $strWhere;
		if (!$strWhere) return null;
		$class = '\\Baseapp\\Suva\\Models\\'.$pModelName;
		
		$model = new $class();
		$object = $model->findFirst($strWhere);
		
		if ($object === false) return null;
		else return $object;
	}
		
	//one2many
	public function one2many($pModelName, $pKey = null, $p){
		if (!$this->id > 0 || !$pKey ) return null;

		$class = '\\Baseapp\\Suva\\Models\\'.$pModelName;
		$model = new $class();
		 
		$findParam = array();
		$txtWhere = " $pKey = $this->id ";
		if (gettype($p)=='string') $txtWhere = " and $p";
		elseif (is_array($p)){
			if (array_key_exists(0,$p)) $txtWhere = " and $p[0]";
			if (array_key_exists("order",$p)) $findParam["order"] = $p["order"];
			if (array_key_exists("limit",$p)) $findParam["limit"] = $p["limit"];
		} 
		$findParam[] = "".$txtWhere;
		//return \Baseapp\Suva\Models\Ads::find("user_id = $this->id");
		return $model::find($findParam);
	}
	
	
	//Greska kod validacije na beforeSave, vraća true da se to iskoristi za varijablu jeGreska
	public function greskaSnimanje($pTxtGreska = null, $pTxtPolje = null){
		$text = $pTxtGreska;
		$field = $pTxtPolje;
		$type = "InvalidValue";
		$message = new \Phalcon\Validation\Message($text, $field, $type);
		$this->appendMessage($message);
		return true;
	}

	
	//OPĆENITE FUNKCIJE
	public function getOne($pModelName = null, $p = null){
		if (!$pModelName || !$p) return null;
		//if (!is_numeric($p)) return null;
		//error_log("P = ".$p."  $pModelName  ".gettype($p));
		
		$id = intval($p);
		//$strWhere = " id = ".$p;
		$strWhere = is_numeric($p) ? " id = ".intval($p) : null;
		$strWhere = !$strWhere ? $p : $strWhere;
		if (!$strWhere) return null;
		$class = '\\Baseapp\\Suva\\Models\\'.$pModelName;
		$model = new $class();
		//error_log("Model:getModelByNameAndId 1.54 id:$strWhere");
		//error_log("Model:getModelByNameAndId 1.5  model:".get_class($model));
		$object = $model->findFirst($strWhere);
		//error_log("Model:getModelByNameAndId 2  classname:".get_class($object));

		if ($object === false) return null;
		else return $object;
	}
	
	public function getMany($pModelName = null, $p = null, $pDefaultCondition = null){
		if (!$pModelName) return null;
		
		$class = '\\Baseapp\\Suva\\Models\\'.$pModelName;
		$model = new $class();
		
		$findParam = array();
		$findParam[0] = " 1 = 1 ";
		
		if (gettype($p)=='string') $findParam[0] .= " and $p";
		elseif (is_array($p)){
			if ( array_key_exists( 0, $p )) $findParam[0] .= " and $p[0]";
			if ( array_key_exists('order', $p )) $findParam["order"] = $p["order"];
			if ( array_key_exists('limit', $p )) $findParam["limit"] = $p["limit"];
		}
		
		if ( gettype($pDefaultCondition)=='string') 
			$findParam[0] .= " and $pDefaultCondition ";

		//error_log("traitBaseSuvaModel::getMany  1 ".print_r($findParam,true));
		$object = $model->find($findParam);
		//error_log("traitBaseSuvaModel::getMany  2 ".print_r($object->toArray(),true));

		return $object;
	}
	
	
	// public function isAdFree( $pAdId = null, $pPublicationId, $pShowErrors = false ){
		
		// $ad = \Baseapp\Suva\Models\Ads::findFirst (( $pAdId > 0 ? $pAdId : -1 ));
		// if ( ! $ad ){
			// Nava::sysMessage( " isAdFree: Correct Ad id not supplied, $pAdId " );
			// return;
		// }
		
		// $publication = \Baseapp\Suva\Models\Publications::findFirst (( $pPublicationId > 0 ? $pPublicationId : -1 ));
		// if ( ! $publication ){
			// Nava::sysMessage( " isAdFree: Correct Publication id not supplied, $pPublicationId " );
			// return;
		// }
		// if ( $publication->is_online ){
			// $currentlyActiveOrderItem = \Baseapp\Suva\Models\OrdersItems::findFirst ( array(" ad_id = $ad->id and n_first_published_at < NOW() and n_expires_at >= NOW() and n_total = 0 ", 'order' => 'n_expires_at desc' ) );
			// if ($currentlyActiveOrderItem){
				// if ( $pShowErrors ) Nava::greska( "Ad $ad->id has active product on order item $currentlyActiveOrderItem->id " );
				// return false;
			// }
			// else{
				// $lastFreeOrderItem = \Baseapp\Suva\Models\OrdersItems::findFirst ( array(" ad_id = $ad->id and n_expires_at < NOW() and n_total = 0 ", 'order' => 'n_expires_at desc' ) );
				// if ( $lastFreeOrderItem ){
					// return true;
				// }
				// else{
					// if ( $pShowErrors ) Nava::greska(" Ad $ad->id does not have a previous free order item ");
				// }
			// }
		// }
		// else{
			// //potražiti prvi slijedeći issue
			// $nextIssue = \Baseapp\Suva\Models\Issues::findFirst( array(" publications_id = $publication->id and date_published < '".date("Y.m.d")."'", 'order'=>'date_published asc' ) );
			// if {$nextIssue}{
				// //naći postoji li insertion na tom issue i order itema za taj ad
				// $txtSql = ";
					// select 
						// max (oi.id) as oi_id
					// from	
						// orders_items oi 
							// join n_insertions ins on oi ( oi.id = ins.orders_items_id and oi.ad_id = $ad->id and ins.issues_id = $nextIssue->id) 			
				// ";
				// $rs = Nava::sqlToArray($txtSql);
				// $oiId = $rs[0]['oi_id'];
				// if ( $oiId >0 ){
					// //ako postoji insertion na slijedećem insertionu
					// if ( $pShowErrors ) Nava::greska( " Ad $ad->id has active product on order item $oiId " );
					// return false;
				// }
				// else {
				// }
			// }
		// }
	// } 



	public function isAdFree ($pAdId = null, $pUserId = null , $pReturnIsFree = true){
		if ( gettype($pAdId)=='array' ){
			$txtAdId = " and a.id in ( 0 ";
			foreach( $pAdId as $id )
				$txtAdId .=", $id";
			$txtAdId .= " )";
			$this->poruka($txtAdId);
		} elseif ( $pAdId > 0 ){
			$txtAdId = " and a.id = $pAdId ";
		}
		else {
			$txtAdId = "";
		}
		
		$txtUserId = $pUserId ? " and a.user_id = $pUserId " : "";
		
		/*
			oiOLA - online aktivni order item
			oiOLP - online prethodni order item
			oiOFA - offline aktivni order item
			oiOFP - offline prethodni order item
			isFree - ako ne postoje aktivni online i offline proizvod i postoji barem jedan prethodni proizvod koji je besplatan
		*/
		
		$txtSql = "
			select 
				a.id as ad_id
				,oiOLA.id as oiOLA_id
				,oiOLP.id as oiOLP_id
				,oiOFA.id as oiOFA_id
				,oiOFP.id as oiOFP_id
				,( case when  
					oiOLA.id is null 
					and oiOFA.id is null
					and 
					(
						oiOLP.id is not null
						or oiOFP.id is not null
					)
					then 'FREE'
					else 'NOT FREE' end ) as isFree
				,( case when  
					oiOLA.id is null 
					and oiOLP.id is not null
					then 'Y'
					else 'N' end ) as isOnlineExt
				,( case when  
					oiOFA.id is null 
					and oiOFP.id is not null
					then 'Y'
					else 'N' end ) as isOfflineExt						

			from 
				ads a
					left join (
						select oi.* 
						from
						orders_items oi 
						join n_products p on (
							oi.n_products_id = p.id 
							and p.is_online_product = 1
							and p.publication_id = 1
							and oi.n_first_published_at <= now() 
							and oi.n_expires_at >= now()
							/* ovo isključuje aktivne plaćene proizvode and oi.n_total = 0*/
							)
						) oiOLA  on (oiOLA.ad_id = a.id)
							
						left join (
								select oi.* 
								from
									orders_items oi 
								join n_products p on (
									oi.n_products_id = p.id 
									and p.is_online_product = 1
									and p.publication_id = 1
									and oi.n_expires_at < now()
									and oi.n_total = 0
								)
						) oiOLP on (oiOLP.ad_id = a.id)

						left join (
							select 
								oi.*
							from 
								orders_items oi
									join n_products p on (
										oi.n_products_id = p.id 
										and p.is_online_product = 0
										and p.publication_id = 2
										/*and oi.n_first_published_at <= now() */
										and oi.n_expires_at >= now()
										and oi.n_total = 0
									)
/*
									join n_insertions ins on ( oi.id = ins.orders_items_id )
									join (
										select 
											*
										from n_issues 
										where 
											date_published = (
												select 
													min(iss.date_published) as date_published
												from 
													n_issues iss
												where 
													iss.date_published > now() 
													and iss.publications_id = 2
											)
											and publications_id = 2	
									) issNext on (	ins.issues_id = issNext.id)			
*/
						) oiOFA on (oiOFA.ad_id = a.id )
							
						left join (
							select 
								oi.*
							from 
								orders_items oi
									join n_products p on (
										oi.n_products_id = p.id 
										and p.is_online_product = 0
										and p.publication_id = 2
										and oi.n_expires_at < now()
										and oi.n_total = 0
									)
									join n_insertions ins on ( oi.id = ins.orders_items_id )
									join (
										select 
											*
										from n_issues 
										where 
											date_published = (
												select 
													max(iss.date_published) as date_published
												from 
													n_issues iss
												where 
													iss.date_published < now() 
													and iss.publications_id = 2
											)
											and publications_id = 2	
									) issNext on (	ins.issues_id = issNext.id)			
								) oiOFP on (oiOFP.ad_id = a.id )
			where 1=1 
				$txtAdId
				$txtUserId
		";
		
		$rs = Nava::sqlToArray($txtSql);
		if ( $pReturnIsFree ){
			$result = array();
			foreach ( $rs as $row ){
				$result[ $row['ad_id'] ] = $row['isFree'];
			}
		}
		else{
			$result = $rs;

		}
		return $result;
	}

	public function isAdExtendable ( $pAdId = null, $pUserId = null , $pReturnIsFree = true ){
		
		$txtAdId = '';
		if ( gettype( $pAdId )=='array' ){
			$txtAdId = " and a.id in ( 0 ";
			foreach( $pAdId as $id )
				$txtAdId .=", $id";
			$txtAdId .= " )";
			//$this->poruka($txtAdId);
		} elseif ( $pAdId > 0 ){
			$txtAdId = " and a.id = $pAdId ";
		}
		
		$txtUserId = $pUserId ? " and a.user_id = $pUserId " : "";
		
		/*
			oiOLA - online aktivni order item
			oiOLP - online prethodni order item
			oiOFA - offline aktivni order item
			oiOFP - offline prethodni order item
			isFree - ako ne postoje aktivni online i offline proizvod i postoji barem jedan prethodni proizvod koji je besplatan
		*/
		
		$txtSql = "
select 
	a.id as ad_id
	,oiOLA.id as oiOLA_id
	,oiOLP.id as oiOLP_id
	,oiOFA.id as oiOFA_id
	,oiOFP.id as oiOFP_id
	,( case when  
		oiOLA.id is null 
		and oiOFA.id is null
		and 
		(
			oiOLP.id is not null
			or oiOFP.id is not null
		)
		then 'FREE'
		else 'NOT FREE' end ) as isFree
	,( case when  
		oiOLA.id is null 
		and oiOLP.id is not null
		then 'Y'
		else 'N' end ) as isOnlineExt
	,( case when  
		oiOFA.id is null 
		and oiOFP.id is not null
		then 'Y'
		else 'N' end ) as isOfflineExt						

from 
	ads a
		left join (
			/* je li postoji bilo kakav aktivan? */
			select 
				oi.ad_id
				,min(oi.id) as id
			from
			orders_items oi 
			join n_products p on (
				oi.n_products_id = p.id 
				and p.is_online_product = 1
				and p.publication_id = 1
				and oi.n_first_published_at <= now() 
				and oi.n_expires_at >= now()
				/*and oi.n_total = 0*/
				)
			group by 
				oi.ad_id
		) oiOLA  on (oiOLA.ad_id = a.id)
				
		left join (
				/* je li postoji prethodni besplatni ?, ako ih ima vise uzima se s najvecim ID-jem */
				select 
					oi.ad_id
					,max(oi.id) as id 
				from
					orders_items oi 
						join n_products p on (
							oi.n_products_id = p.id 
							and p.is_online_product = 1
							and p.publication_id = 1
							and oi.n_expires_at < now()
							and oi.n_total = 0
						)
				group by 
					oi.ad_id
		) oiOLP on (oiOLP.ad_id = a.id)

		left join (
			/* je li postoji bilo kakav aktivan offline? */
			select 
				oi.ad_id
				,max(oi.id) as id
			from 
				orders_items oi
					join n_products p on (
						oi.n_products_id = p.id 
						and p.is_online_product = 0
						and p.publication_id = 2
						/*and oi.n_first_published_at <= now() */
						and oi.n_expires_at >= now()
						/*and oi.n_total = 0*/
					)
					join n_insertions ins on ( oi.id = ins.orders_items_id )
						join n_issues iss on ( 
							ins.issues_id = iss.id
							and iss.status = 'prima'
							) 
			group by
				oi.ad_id
		) oiOFA on (oiOFA.ad_id = a.id )
			
		left join (
			select 
				oi.ad_id 
				,max(oi.id) as id
				
			from 
				orders_items oi
					join n_products p on (
						oi.n_products_id = p.id 
						and p.is_online_product = 0
						and p.publication_id = 2
						and oi.n_expires_at < now()
						and oi.n_total = 0
					)
			group by
				oi.ad_id
		) oiOFP on (oiOFP.ad_id = a.id )
where 1=1 
	$txtAdId
	$txtUserId
		";
		//$this->poruka($txtSql);
		
/* 29.12.2016 dodano u sql:
	join n_insertions ins on ( oi.id = ins.orders_items_id )
		join n_issues iss on ( 
			ins.issues_id = iss.id
			and iss.status = 'prima'
		) 
*/

		$rs = Nava::sqlToArray($txtSql);
		if ( $pReturnIsFree ){
			$result = array();
			foreach ( $rs as $row ){
				$result[ $row['ad_id'] ] = array(
					'online' => $row['isOnlineExt']
					,'offline' => $row['isOfflineExt']
				);
			}
		}
		else{
			$result = $rs;

		}
		return $result;
	}	

	
}