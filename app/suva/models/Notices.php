<?php

namespace Baseapp\Suva\Models;

// use Baseapp\Bootstrap;
// use Baseapp\Library\Validations\Notices as NoticesValidations;

/**
 * Notices Model
 */
class Notices extends \Baseapp\Models\Notices
{
	use traitBaseSuvaModel;

    // protected $fields_types_map = array(
        // 'boolint' => array(
            // 'active'
        // ),
        // 'date'    => array(
            // 'received_at'
        // ),
        // 'string'  => array(
            // 'title',
            // 'message'
        // )
    // );

    // /**
     // * Notices initialize
     // */
    // public function initialize()
    // {
        // parent::initialize();

        // $this->belongsTo(
            // 'created_by_user_id', __NAMESPACE__ . '\Users', 'id',
            // array(
                // 'alias'    => 'CreatedBy',
                // 'reusable' => true
            // )
        // );
        // $this->belongsTo(
            // 'modified_by_user_id', __NAMESPACE__ . '\Users', 'id',
            // array(
                // 'alias'    => 'ModifiedBy',
                // 'reusable' => true
            // )
        // );
    // }

    // /**
     // * Add a new Company notice
     // *
     // * @param \Phalcon\Http\RequestInterface $request
     // *
     // * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     // */
    // public function add_new($request)
    // {
        // $this->initialize_model_with_post($request);
        // $this->entity = 'notice';

        // $validation = new NoticesValidations();
        // $messages = $validation->validate($request->getPost());

        // if (count($messages)) {
            // return $validation->getMessages();
        // } else {
            // if (true === $this->create()) {
                // return $this;
            // } else {
                // Bootstrap::log($this->getMessages());
                // return $this->getMessages();
            // }
        // }
    // }

    // /**
     // * Save CmsArticle data method
     // *
     // * @param \Phalcon\Http\RequestInterface $request
     // *
     // * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     // */
    // public function save_changes($request)
    // {
        // $validation = new NoticesValidations();
        // $messages = $validation->validate($request->getPost());

        // $this->initialize_model_with_post($request);

        // if (count($messages)) {
            // return $validation->getMessages();
        // } else {
            // if (true === $this->update()) {
                // return $this;
            // } else {
                // Bootstrap::log($this->getMessages());
                // return $this->getMessages();
            // }
        // }
    // }

    // /**
     // * Active/Inactive article, depending on the previous state
     // *
     // * @return bool Whether the update was successful or not
     // */
    // public function activeToggle()
    // {
        // $this->disable_notnull_validations();
        // $this->active = (int) !$this->active;
        // $this->modified_by_user_id = $this->getDI()->getShared('auth')->get_user()->id;
        // $result = $this->update();
        // $this->enable_notnull_validations();
        // return $result;
    // }

}
