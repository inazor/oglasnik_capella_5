<?php

namespace Baseapp\Suva\Models;

use Baseapp\Bootstrap;
use Phalcon\Di;
use Phalcon\Mvc\Model;
use Baseapp\Library\DateUtils;

//DODANO
use Baseapp\Suva\Library\Nava;

trait traitBaseSuvaModel
{
 	public function initialize_model_with_post($request){
		//error_log("BaseModel:initialize_model_with_post 1 ");
		$fields = $this->n_fields_list($this->getSource());
		$types = $this->n_field_types($this->getSource());
		$nullables = $this->n_is_nullable($this->getSource());
		//error_log("BaseModel:initialize_model_with_post 2.1 ".$this->getSource());

		foreach($fields as $field){
				//error_log("BaseModel:initialize_model_with_post 2 ");
				$family = $this->n_data_family($types[$field]);
				if ($request->hasPost($field)){
						//error_log("BaseModel:initialize_model_with_post 3 ");
						$requestValue = $request->getPost($field);
						$type = $types[$field];
						$nullable = $nullables[$field];
						$modelValue = $this->n_interpret_value_from_request($requestValue, $type, $nullable);
						//error_log("BaseModel:initialize_model_with_post field:$field, requestValue:$requestValue, type:$type, modelValue:$modelValue ");
						$this->assign(array($field =>$modelValue));
				}
		}	
	}
	


	//debug funkcije update, create
	public function n_update(){
		$success = $this->update();
		if ($success === false){
			$greske = $this->getMessages();
			foreach ($greske as $greska)  
				$err_msg .= $greska.", ";
			return $err_msg;
		}
		else return null;
	}
	public function n_create(){
		$success = $this->create();
		if ($success === false){
			$greske = $this->getMessages();
			foreach ($greske as $greska)  
				$err_msg .= $greska.", ";
			return $err_msg;
		}
		else return null;
	}
	
	
    //GENERIČKE FUNKCIJE 
	public function n_before_save( $p = null ){
				
		}
	public function n_after_save( $p = null ){
				
		}
    
    //FUNKCIJE ZA DOHVAT POLJA, STRANIH KLJUČEVA I SL
	public function n_set_default_values($pTableName = null){
		if(!$pTableName) $pTableName = $this->getSource();
		$defaults = Nava::sqlToArray("
			select COLUMN_NAME
			,(case when 
					DATA_TYPE in ('datetime', 'date', 'time') 
					and COLUMN_DEFAULT = 'CURRENT_TIMESTAMP' 
				THEN NOW() 
				ELSE COLUMN_DEFAULT END 
				) AS COLUMN_DEFAULT
			 FROM information_schema.columns 
			 where TABLE_NAME = '$pTableName' and COLUMN_DEFAULT IS NOT NULL 
			 group by COLUMN_NAME 
		");
		$arrAssign = array();
		foreach ($defaults as $default){
			$arrAssign[$default['COLUMN_NAME']] = $default['COLUMN_DEFAULT'];
		}
		$this->assign($arrAssign);
		//Nava::sysMessage($arrAssign);
		
	}

    public function n_fields_list($pTableName = null){
        $rows = Nava::sqlToArray("select COLUMN_NAME from information_schema.columns where TABLE_NAME = '$pTableName' group by COLUMN_NAME ");
        $result = array();
        foreach($rows as $row)
            array_push($result, $row['COLUMN_NAME']);
        return $result;
    }

	public function n_field_types($pTableName = null){
		$rows = Nava::sqlToArray("select COLUMN_NAME, DATA_TYPE from information_schema.columns where TABLE_NAME = '$pTableName' group by COLUMN_NAME ");
		$result = array();
		foreach($rows as $row)
			$result[$row['COLUMN_NAME']] = $row['DATA_TYPE'];
		return $result;
	}    

	public function n_is_nullable($pTableName = null){
		$rows = Nava::sqlToArray("select COLUMN_NAME, IS_NULLABLE from information_schema.columns where TABLE_NAME = '$pTableName' group by COLUMN_NAME ");
		$result = array();
		foreach($rows as $row)
			$result[$row['COLUMN_NAME']] = $row['IS_NULLABLE'] === 'YES' ? true : false;
		return $result;
	} 
	
    public function n_foreign_keys($pTableName = null){
      $rows = Nava::sqlToArray("select COLUMN_NAME, REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME from information_schema.key_column_usage where TABLE_NAME = '$pTableName'  and REFERENCED_TABLE_NAME is not null");
			$result = array();
			foreach($rows as $row)
				$result[$row['COLUMN_NAME']] = $row;
			return $result;
    }
    
    // public $n_table_model_mapping = array(
        // 'ads' => 'Ads',
        // 'categories' => 'Categories',
		// 'location' => 'Locations',
        // 'n_actions' => 'Actions',
		// 'n_categories_mappings' => 'CategoriesMappings',
		
        // 'n_discounts' => 'Discounts',
        // 'n_discounts_products' => 'DiscountsProducts',
        // 'n_fiscal_locations' => 'FiscalLocations',
        // 'n_insertions' => 'Insertions',
        // 'n_issues' => 'Issues',
        // 'n_order_statuses' => 'OrdersStatuses',
        // 'n_order_status_transitions' => 'OrderStatusTransitions',
        // 'n_payment_types' => 'PaymentTypes',
        // 'n_products' => 'Products',
        // 'n_products_categories_types' => 'ProductsCategoriesTypes',
        // 'n_publications' => 'Publications', 
		// 'n_publications_categories' => 'PublicationsCategories',
		// 'n_publications_categories_mappings' => 'PublicationsCategoriesMappings',
        // 'n_roles_permissions' => 'RolesPermissions',
        // 'orders' => 'Orders',
        // 'orders_items' => 'OrdersItems',
        // 'orders' => 'Orders',
        // 'parameters' => 'Parameters',
        // 'roles' => 'Roles',
        // 'roles_users' => 'RolesUsers',
        // 'users' => 'Users',

    // );
   
		public function n_data_family($pDataType){
			 
		
			switch($pDataType){
				case 'bigint':
				case 'int':
				case 'smallint':
				case 'mediumint':
				case 'tinyint':
					return "integer";
				case 'decimal':
				case 'double':
					return "decimal";
				case 'varchar':
				case 'char':
				case 'varchar':
				case 'longtext':
				case 'mediumtext':
				case 'text':
				case 'longtext':
				case 'enum':
				case 'set':
					return "textual";
				case 'date':
				case 'datetime':
					return 'date';
				default:
					return "unknown";
			}
			
		}	 
		
		public function n_interpret_value_from_request($pValue, $pFieldType,  $pIsNullable = false){
			
			$family = $this->n_data_family($pFieldType);
			//error_log ("BaseModel.php: n_interpret_value_from_request: value $pValue, fieldType $pFieldType ");
			if (gettype($pValue) === 'NULL') $result = null;
			elseif ($pValue === '') $result = null;
			elseif ( ($family === 'integer' ||  $family === 'decimal' || $family === 'date' ) && $pValue === '') $result = null;
			elseif ($family === 'integer' && is_numeric(trim($pValue)) && (int)trim($pValue) == trim($pValue)) $result = (int)($pValue);
			elseif ($family === 'decimal' && is_numeric(trim($pValue)) && (float)trim($pValue) == trim($pValue)) $result = (float)($pValue);
			elseif ($family === 'textual' && strlen($pValue) == 0 && $pIsNullable ) $result = null;
			elseif ($family === 'textual' && gettype($pValue) === 'string') $result = $pValue;
			elseif ($family === 'date'){
					$pValue = str_replace('/','.', $pValue); // radi 2015/01/01 - da ide u 2015.01.01
					$pValue = str_replace('-','.', $pValue); // radi 2015-01-01 - da ide u 2015.01.01
					$pValue = str_replace('T',' ', $pValue); // radi 2015-01-01T08:30
					$dmyhms = explode(" ", trim($pValue));
					if ( count($dmyhms) == 1 ) $dmy = explode(".", trim($pValue));
					elseif ( count($dmyhms) == 2 ) $dmy = explode(".", trim($dmyhms[0]));
					else return null;
				
					//$dmy = explode(".", trim($pValue));
					if (count($dmy) != 3) return null;
					
					// 25.02.2015
					if ((int)$dmy[0] > 0 && (int)$dmy[0] < 32 && (int)$dmy[1] > 0 && (int)$dmy[1] < 13 && (int)$dmy[2] > 1969  && (int)$dmy[2] < 9999) 
							$result = "".$dmy[2]."-".$dmy[1]."-".$dmy[0];
					// 2015.02.25
					if ((int)$dmy[2] > 0 && (int)$dmy[2] < 32 && (int)$dmy[1] > 0 && (int)$dmy[1] < 13 && (int)$dmy[0] > 1969  && (int)$dmy[0] < 9999) 
					$result = "".$dmy[0]."-".$dmy[1]."-".$dmy[2]."";
					
					//08:00:00
					if ( count($dmyhms) == 2 ) $result = $result." ".trim($dmyhms[1]);
					
					//error_log("n_interpret_value_from_request date ".$result);
					
			} 
			else $result = null;
			
			return $result;
		}
    
		public function n_dateUs2DateHr ($pDate = null){
			return $this->n_dateTimeUs2DateHr($pDate);
		}
		
		public function n_dateTimeUs2DateHr ($pDate = null){
			
			$pDate = str_replace('-','.', $pDate);
			$pDate = str_replace('/','.', $pDate);
			$pDate = str_replace(':','.', $pDate);
			$pDate = str_replace(' ','.', $pDate);
			
			$result = "";
			$dmy = explode(".", trim($pDate));
			if (count($dmy) < 3) return null;
			if ((int)$dmy[2] > 0 && (int)$dmy[2] < 32 && (int)$dmy[1] > 0 && (int)$dmy[1] < 13 && (int)$dmy[0] > 1969  && (int)$dmy[0] < 9999) 
			$result = "".$dmy[2].".".$dmy[1].".".$dmy[0]."";
			return $result;
		}
		
		public function n_dateTimeUs2DateTimeHr ($pDate = null){
			$pDate = str_replace('-','.', $pDate);
			$pDate = str_replace('/','.', $pDate);
			$pDate = str_replace(':','.', $pDate);
			$pDate = str_replace(' ','.', $pDate);
			
			$dmy = explode(".", trim($pDate));
			if (count($dmy) < 3) return null;
			if ((int)$dmy[2] > 0 && (int)$dmy[2] < 32 && (int)$dmy[1] > 0 && (int)$dmy[1] < 13 && (int)$dmy[0] > 1969  && (int)$dmy[0] < 9999) 
			$result = "".$dmy[2].".".$dmy[1].".".$dmy[0]." ".$dmy[3].":".$dmy[4].":".$dmy[5];
			return $result;
		}
    
		//many2One  
		public function many2one($pModelName = null, $p = null){
			/*korištenje
				npr $ad->many2One("Users", "user_id");
			*/
			
			if (!$pModelName || !$p) return null;
		
			$id = intval($p);
		
			$strWhere = is_numeric($p) ? " id = ".intval($p) : null;
			$strWhere = !$strWhere ? $p : $strWhere;
			if (!$strWhere) return null;
			$class = '\\Baseapp\\Models\\'.$pModelName;
			
			$model = new $class();
			$object = $model->findFirst($strWhere);
			
			if ($object === false) return null;
			else return $object;
		}
		
		//one2many
		public function one2many($pModelName, $pKey = null, $p){
			if (!$this->id > 0 || !$pKey ) return null;

			$class = '\\Baseapp\\Models\\'.$pModelName;
			$model = new $class();
			 
			$findParam = array();
			$txtWhere = " $pKey = $this->id ";
			if (gettype($p)=='string') $txtWhere = " and $p";
			elseif (is_array($p)){
				if (array_key_exists(0,$p)) $txtWhere = " and $p[0]";
				if (array_key_exists("order",$p)) $findParam["order"] = $p["order"];
				if (array_key_exists("limit",$p)) $findParam["limit"] = $p["limit"];
			} 
			$findParam[] = "".$txtWhere;
			//return \Baseapp\Suva\Models\Ads::find("user_id = $this->id");
			return $model::find($findParam);
		}
	
	
	//greska kod validacije na beforeSave, vraća true da se to iskoristi za varijablu jeGreska
	public function greskaSnimanje($pTxtGreska = null, $pTxtPolje = null){
		$text = $pTxtGreska;
		$field = $pTxtPolje;
		$type = "InvalidValue";
		$message = new \Phalcon\Validation\Message($text, $field, $type);
		$this->appendMessage($message);
		return true;
	}

	
	
	//OPĆENITE FUNKCIJE
	
	public function getOne($pModelName = null, $p = null){
		if (!$pModelName || !$p) return null;
		//if (!is_numeric($p)) return null;
		//error_log("P = ".$p."  $pModelName  ".gettype($p));
		
		$id = intval($p);
		//$strWhere = " id = ".$p;
		$strWhere = is_numeric($p) ? " id = ".intval($p) : null;
		$strWhere = !$strWhere ? $p : $strWhere;
		if (!$strWhere) return null;
		$class = '\\Baseapp\\Suva\\Models\\'.$pModelName;
		$model = new $class();
		//error_log("Model:getModelByNameAndId 1.54 id:$strWhere");
		//error_log("Model:getModelByNameAndId 1.5  model:".get_class($model));
		$object = $model->findFirst($strWhere);
		//error_log("Model:getModelByNameAndId 2  classname:".get_class($object));

		if ($object === false) return null;
		else return $object;
	}
	
	public function getMany($pModelName = null, $p = null, $pDefaultCondition = null){
		if (!$pModelName) return null;
		
		$class = '\\Baseapp\\Suva\\Models\\'.$pModelName;
		$model = new $class();
		
		$findParam = array();
		$findParam[0] = " 1 = 1 ";
		
		if (gettype($p)=='string') $findParam[0] .= " and $p";
		elseif (is_array($p)){
			if (array_key_exists(0, $p))  $findParam[0] .= " and $p[0]";
			if ($p["order"]) $findParam["order"] = $p["order"];
			if (array_key_exists('limit',$p)) $findParam["limit"] = $p["limit"];
		}
		
		if ( gettype($pDefaultCondition)=='string') 
			$findParam[0] .= " and $pDefaultCondition ";
		
		//error_log("traitBaseSuvaModel::getMany  1 ".print_r($findParam,true));
		$object = $model->find($findParam);
		//error_log("traitBaseSuvaModel::getMany  2 ".print_r($object->toArray(),true));
		
		return $object;
	}
	
	
	public function sysMessage ( $pTxtPoruka = null ){
        if ($pTxtPoruka){

			array_push($_SESSION['_context']['_msg_flashsession'], array('system' => $pTxtPoruka) );
        }
	}
	
	public function printr( $pTxtPoruka = null , $pTxtNaslov = null){
        if ($pTxtPoruka){
			$naslov = $pTxtNaslov ? "<b>$pTxtNaslov</b><br/>" : "";

			array_push($_SESSION['_context']['_msg_flashsession'], array('system' => "$naslov<br/><pre>".print_r($pTxtPoruka,true)."</pre>") );
        }
	}
	
	public function poruka ( $pTxtPoruka = null ){
        if ($pTxtPoruka){

			array_push($_SESSION['_context']['_msg_flashsession'], array('info' => $pTxtPoruka) );
        }
	}
	
    public function greska( $pTxtGreska = null , $pTxtNaslov = null ){
		//naslov se može dodati, ali poruka se ne ispisuje ako nema $pTxtGreska (koristi se za slanje poruke ako je greska u update)
        if ($pTxtGreska){
			$poruka = $pTxtNaslov ? "<b>$pTxtNaslov</b><br/><pre>".$pTxtGreska."</pre>" : $pTxtGreska ;
			array_push($_SESSION['_context']['_msg_flashsession'], array('error' => $poruka) );
        }
    }
	

	
// Postavke aplikacije
	public function getAppSetting( $pName = null ){
		$setting = Nava::sqlToArray("select * from n_app_settings where name = '$pName' ");
		if ( $setting ){
			$rec = $setting[0];
			$type = $rec['type'];
			if ( $type = 'text' ){
				return $rec['value_text'];
			}
			elseif ( $type = 'decimal' ){
				return  $rec['value_decimal'];
			}
			elseif ( $type = 'integer' ){
				return intval( $rec['value_decimal'] );
			}
			elseif ( $type = 'boolean' ){
				$value = $rec['value_decimal'];
				return ( $value == 0 ? false : true);
			}
		}
		else return null;
	}
	
	public function setAppSetting( $pName = null, $pValue = null, $pType = null ){
		$setting = Nava::sqlToArray("select * from n_app_settings where name = '$pName' ");
		
		$value = $this->appSettingValueToSqlValue( $type, $pValue );
		
		if ( $setting ){
			$rec = $setting[0];
			$type = $rec['type'];
			$valueFieldName = $type == 'text' ? 'value_text' : 'value_decimal';
			if ( $type == $pType )	$txtSql = " update n_app_settings set $valueFieldName = $value where name = '$pName' ";
			else return;
		}
		else{
			$valueFieldName = $pType == 'text' ? 'value_text' : 'value_decimal';
			$txtSql = "insert into n_app_settings ( name, type, $valueFieldName ) values ( '$pName', '$pType', $value )";
		}
		// error_log($txtSql);
		Nava::runSql ($txtSql);
	}
	
	public function appSettingValueToSqlValue ( $pType = null, $pValue = null  ){
			
		if ( $pType = 'text' ){
			$value = $pValue;
			$result = "'$value'";
		}
		elseif ( $pType = 'decimal' ){
			$value = $pValue;
			$result = "$value";
		}
		elseif ( $pType = 'integer' ){
			$value = intval($pValue);
			$result = "$value";
		}
		elseif ( $pType = 'boolean' ){

			if ( $pValue == 1 ) $value = 1;
			elseif ( $pValue == 0 ) $value = 0;
			elseif ( $pValue == true ) $value = 1;
			elseif ( $pValue == false ) $value = 0;

			$result = "$value";
		}
		
		else $result = "0";
		
		return $result;
	
	}
	
}
