<?php

namespace Baseapp\Suva\Models;

/**
 * Layouts Model
 */
class Layouts extends BaseModelBlamable
{
    
	//se traitBaseSuvaModel;
	
	public function initialize()
    {
        $this->setSource("n_layouts");
    }

        public function getSource()
    {
        return 'n_layouts';
    }


	public function generateXml($p = array()){
		if (! $this->id > 0) {
			$this->sysMessage("Layouts.php: Layout ID not defined., p:".print_r($p,true));
			return "ERR: Layouts.php: Layout ID not defined.";
		}
		
		
		
		$arrVariables = explode( ",", $this->variables ); //variables je polje u bazi
		//provjera jesu li sve varijable iz teksta na popisu
		//provjera sadrži li popis sve varijable koje se spominju u tekstu
		//error_log(print_r($arrVariables, true));
		//provjera sadrži li p sve varijable s popisa
		foreach ($arrVariables as $var){
			if (!array_key_exists($var, $p)) {
				$txtErr = "ERR123: Layouts.php: key $var not supplied, "
					." layout:".print_r($this->toArray(),true)
					.", params:".print_r($p, true);
				$this->sysMessage("2.Layout $this->id : $txtErr , p:".print_r($p, true));
				return $txtErr;
			}
		}
		
		//zamjena
		//$xml = str_replace ('"',"_DBLQUOT_",$this->xml);
		$xml = $this->xml;
		foreach ($arrVariables as $var){
			//error_log("__".$var."__".print_r($p,true));
/*			
"   &quot;
'   &apos;
<   &lt;
>   &gt;
&   &amp;	
	
*/			 
			$pVar = $p[$var];
			//$pVar = str_replace ("'", "&apos;", $pVar);
			// $pVar = str_replace ('"', "&quot;", $pVar);
			$pVar = str_replace ('"', "'", $pVar);
			//$pVar = str_replace ('<', "&lt;", $pVar);
			//$pVar = str_replace ('>', "&gt;", $pVar);
			//$pVar = str_replace ('&', "&amp;", $pVar);
			 
			
			$xml = str_replace ("__".$var."__", $pVar, $xml);
			//error_log("STRREPLACE __".$var."__, ".$p[$var].PHP_EOL."BEFORE: $this->xml ".PHP_EOL."RESULT: $xml");
			if (strlen(trim($xml)) < 10) Nava::printr ($p, "LAYOUT $this->id SE NIJE GENERIRAO");
		}
		return $xml;
	}

}