<?php

namespace Baseapp\Suva\Models;

use Baseapp\Library\Validations\Publications as PublicationValidations;
use Phalcon\Mvc\Model\Relation as PhRelation;
use Baseapp\Suva\Library\Nava;
/**
 * Publications Model
 */
class Publications extends BaseModelBlamable
{
    /**
     * Publications initialize
     */
    public function initialize()
    {
        parent::initialize();
		
        $this->setSource("n_publications");

		
        $this->hasMany('id', __NAMESPACE__ . '\Products', 'publication_id', array(
            'alias' => 'Products',
            'foreignKey' => array(
                'action' => PhRelation::ACTION_CASCADE
            )
        ));
    }
	

        public function getSource()
    {
        return 'n_publications';
    }

    /**
     * Returns available types (defined as ENUM in the db currently)
     * @return array
     */
    public function getAvailableTypes()
    {
        $options = $this->get_field_options('type');

        // fill array keys with values so that \Phalcon\Select behaves properly...
        $return_options = array();
        foreach ($options as $k => $v) {
            $return_options[$v] = $v;
        }

        return $return_options;
    }

    // public function initialize_model_with_post($request)
    // {
        // $this->type = $request->hasPost('type') ? trim($request->getPost('type')) : 'online';
        // $this->name = $request->hasPost('name') ? trim($request->getPost('name')) : '';
        // $this->active = $request->hasPost('active') ? 1 : 0;
    // }


    /**
     * Add a new Publication
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function add_new($request)
    {
        $this->initialize_model_with_post($request);

        $validation = new PublicationValidations();

        $available_types = $this->getAvailableTypes();
        $validation->add('type', new \Phalcon\Validation\Validator\InclusionIn(array(
            'message' => 'Type must be one of: ' . implode(', ', $available_types),
            'domain' => $available_types
        )));
        $messages = $validation->validate($request->getPost());

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            if (true === $this->create()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Save Publication data method
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function save_changes($request)
    {
        // avoid validation for certain fields that haven't been changed
        $unique_if_changed = array('type');
        $require_unique = array();
        foreach ($unique_if_changed as $field) {
            if ($this->$field != $request->getPost($field)) {
                $require_unique[] = $field;
            }
        }
        $validation = new PublicationValidations();
        $validation->set_unique($require_unique);

        $available_types = $this->getAvailableTypes();
        $validation->add('type', new \Phalcon\Validation\Validator\InclusionIn(array(
            'message' => 'Type must be one of: ' . implode(', ', $available_types),
            'domain' => $available_types
        )));

        $messages = $validation->validate($request->getPost());

        $this->initialize_model_with_post($request);

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            if (true === $this->update()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Active/Inactive publication, depending on the previous state
     *
     * @return bool Whether the update was successful or not
     */
    public function activeToggle()
    {
        $this->disable_notnull_validations();
        $this->active = (int) !$this->active;
        $result = $this->update();
        $this->enable_notnull_validations();
        return $result;
    }
	//DODANO 
	
	public function getNextIssue( $pDateFrom = null ){
		$txtDate = "";
		//error_log("Publications::getNextIssue  1 $pDateFrom");
		if ($pDateFrom) {
			if (is_integer($pDateFrom)) 
				$txtDate = " and date_published >'".date("Y-m-d", $pDateFrom)."'";
			elseif (is_string($pDateFrom))
				$txtDate = " and date_published > '$pDateFrom' ";
		}
		$txtSql = "select id from n_issues "
			."where date_published in ("
			." select min(date_published) as datum from n_issues where status='prima' and date_published > NOW() $txtDate" 
			." and publications_id = $this->id"
			." )";

		//error_log($txtSql);
		$nextIssueId = Nava::sqlToArray( $txtSql )[0]['id'];
		//error_log("Publications::getNextIssue  2  nextIssueId:$nextIssueId");
		if (!$nextIssueId) return null;
		
		return \Baseapp\Suva\Models\Issues::findFirst($nextIssueId);
	}

	
	//veze one2many
	// public function Issues(){
		// return \Baseapp\Suva\Models\Issues::find(
			// array(
				// "status='prima' and publications_id=$this->id"
				// ,"limit" => 5
				// ,"order" => "date_published"
				// )
		// );
	// }
	 // public function Category(){
		 // return \Baseapp\Suva\Models\Categories::find("n_publication_id = $this->id");
	 // }
	public function PublicationsCategoriesMappings( $p = null ) { return $this->getMany('PublicationsCategoriesMappings', $p, " publication_id = $this->id " ); }
	public function CategoriesMappings( $p = null ) { return $this->getMany('CategoriesMappings', $p, " n_publications_id = $this->id " ); }
	public function Issues( $p = null ) { return $this->getMany('Issues', $p, " publications_id = $this->id " ); }
	public function Products( $p = null ) { return $this->getMany('Products', $p, " publication_id = $this->id " ); }
	public function Discounts(){
						
		return \Baseapp\Suva\Models\Discounts::query()
			->columns("Baseapp\Suva\Models\Discounts.*")		
			->where('Baseapp\Suva\Models\Discounts.is_active = 1')
			->andWhere("Baseapp\Suva\Models\DiscountsPublications.is_active = 1")
			->andWhere("Baseapp\Suva\Models\DiscountsPublications.publications_id = $this->id")
			->join("Baseapp\Suva\Models\DiscountsPublications", "Baseapp\Suva\Models\Discounts.id = Baseapp\Suva\Models\DiscountsPublications.discounts_id") 
			->orderBy(' Baseapp\Suva\Models\Discounts.name')
			->execute();
	}
	
	public function getProductsForAdAndCategoryMapping ( $pAdId = null, $pCategoryMappingId = null ){
		if (!$this->id > 0) return null;
		if (!$pAdId ) return null;
		if (!$pCategoryMappingId ) return null;
		
		
		$txtSql = "


select 
	pcp.products_id as product_id
	, (case when pcp.unit_price is null then p.unit_price else pcp.unit_price end ) as unit_price
from (


	select 
		pcm.category_mapping_id
	
	from
		ads a 
			join n_publications_categories_mappings pcm on (a.category_id = pcm.mapped_category_id)
			
	where
		a.id = $pAdId and pcm.publication_id = $this->id
		
		
	union
	
	select 
		pcm.category_mapping_id
	from
		ads a 
			join n_publications_categories_mappings pcm on (a.country_id = pcm.mapped_location_id)
			
	where
		a.id = $pAdId and pcm.publication_id = $this->id
	
	union
	
	select 
		pcm.category_mapping_id
	from
		ads a 
			join n_publications_categories_mappings pcm on (a.county_id = pcm.mapped_location_id)
			
	where
		a.id = $pAdId and pcm.publication_id = $this->id
		
		
	union
	
	
	select 
		pcm.category_mapping_id
	from
		ads a 
			join n_publications_categories_mappings pcm on (a.municipality_id = pcm.mapped_location_id)
			
	where
		a.id = $pAdId and pcm.publication_id = $this->id
		
	union
	
	
	select 
		pcm.category_mapping_id
	from
		ads a 
			join n_publications_categories_mappings pcm on (a.city_id = pcm.mapped_location_id)
			
	where
		a.id = $pAdId and pcm.publication_id = $this->id
		
	union
	
	select 
		pcm.category_mapping_id
	from
		ads a 
			join ads_parameters ap on (ap.ad_id = a.id)
				join parameters p on (p.id = ap.parameter_id)
					join parameters_types pt on (pt.id = p.type_id and pt.accept_dictionary = 1)
				join n_publications_categories_mappings pcm on ( pcm.mapped_dictionary_id = cast(ap.value as UNSIGNED) )
						
	where
		a.id = $pAdId and pcm.publication_id = $this->id
	) p1
	
	join n_products_categories_prices pcp on  (p1.category_mapping_id = pcp.categories_mappings_id and pcp.is_displayed = 1 and pcp.is_active = 1 and pcp.categories_mappings_id = $pCategoryMappingId )
		join n_products p on (p.id = pcp.products_id)
		
		
		
		";
		
		
		//error_log($txtSql);
		
		$rs = \Baseapp\Suva\Library\Nava::sqlToArray( $txtSql );
		$txtWhere = " id in (0 ";
		
		foreach ( $rs as $row)
			$txtWhere .= ", ".$row['product_id'];
		
		$txtWhere .= ")";
		
		$products = \Baseapp\Suva\Models\Products::find( array( $txtWhere, 'order' => 'sort_order, name ' ) );
		
		return $products;
		
		
	}

	public function getProductsForAd ( $pAdId = null ){
		if (!$this->id > 0) return null;
		if (!$pAdId ) return null;
		
		
		$txtSql = "


select 
	pcp.products_id as product_id
	, (case when pcp.unit_price is null then p.unit_price else pcp.unit_price end ) as unit_price
from (


	select 
		pcm.category_mapping_id
	
	from
		ads a 
			join n_publications_categories_mappings pcm on (a.category_id = pcm.mapped_category_id)
			
	where
		a.id = $pAdId and pcm.publication_id = $this->id
		
		
	union
	
	select 
		pcm.category_mapping_id
	from
		ads a 
			join n_publications_categories_mappings pcm on (a.country_id = pcm.mapped_location_id)
			
	where
		a.id = $pAdId and pcm.publication_id = $this->id
	
	union
	
	select 
		pcm.category_mapping_id
	from
		ads a 
			join n_publications_categories_mappings pcm on (a.county_id = pcm.mapped_location_id)
			
	where
		a.id = $pAdId and pcm.publication_id = $this->id
		
		
	union
	
	
	select 
		pcm.category_mapping_id
	from
		ads a 
			join n_publications_categories_mappings pcm on (a.municipality_id = pcm.mapped_location_id)
			
	where
		a.id = $pAdId and pcm.publication_id = $this->id
		
	union
	
	
	select 
		pcm.category_mapping_id
	from
		ads a 
			join n_publications_categories_mappings pcm on (a.city_id = pcm.mapped_location_id)
			
	where
		a.id = $pAdId and pcm.publication_id = $this->id
		
	union
	
	select 
		pcm.category_mapping_id
	from
		ads a 
			join ads_parameters ap on (ap.ad_id = a.id)
				join parameters p on (p.id = ap.parameter_id)
					join parameters_types pt on (pt.id = p.type_id and pt.accept_dictionary = 1)
				join n_publications_categories_mappings pcm on ( pcm.mapped_dictionary_id = cast(ap.value as UNSIGNED) )
						
	where
		a.id = $pAdId and pcm.publication_id = $this->id
	) p1
	
	join n_products_categories_prices pcp on  (p1.category_mapping_id = pcp.categories_mappings_id and pcp.is_displayed = 1 and pcp.is_active = 1)
		join n_products p on (p.id = pcp.products_id)
		
		
		
		";
		
		
		//error_log($txtSql);
		
		$rs = \Baseapp\Suva\Library\Nava::sqlToArray( $txtSql );
		$txtWhere = " id in (0 ";
		
		foreach ( $rs as $row)
			$txtWhere .= ", ".$row['product_id'];
		
		$txtWhere .= ")";
		
		$products = \Baseapp\Suva\Models\Products::find( array( $txtWhere, 'order' => 'sort_order, name ' ) );
		
		return $products;
		
		
	}

	
}
