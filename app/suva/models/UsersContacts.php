<?php

namespace Baseapp\Suva\Models;
use Baseapp\Suva\Library\Nava;



/**
 * Publications Model
 */
class UsersContacts extends BaseModelBlamable
{
    public function initialize()
    {
        $this->setSource("n_users_contacts");
    }

        public function getSource()
    {
        return 'n_users_contacts';
    }
	
	public function makeSearchString(){
		if (!$this->name) return $this->name;
		$result = $this->name;
		
		$result = str_replace("'","_", $result);
		$result = str_replace('"',"_", $result);
		$result = str_replace(".","_", $result);
		$result = str_replace(",","_", $result);
		$result = str_replace("/","_", $result);
		$result = str_replace("-","_", $result);
		$result = str_replace(" ","_", $result);
		$result = str_replace("+","_", $result);
		$result = str_replace("@","_", $result);
		$result = strtolower($result);
		
		return $result;
	}

}
