<?php

namespace Baseapp\Suva\Models;
use Baseapp\Suva\Library\Nava;


/**
 * Publications Model
 */
class PublicationsCategoriesMappings extends BaseModelBlamable
{
    public function initialize()
    {
        $this->setSource("n_publications_categories_mappings");
    }

        public function getSource()
    {
        return 'n_publications_categories_mappings';
    }

	// public function beforeSave( $p  ){
		// if ( $this->category_mapping_id == null ) $this->category_mapping_id = 0;
		
		// return parent::beforeSave( $p )
	// }

	public function CategoryMapping(){
		$id = $this->category_mapping_id > 0 ? $this->category_mapping_id : -1;
		return \Baseapp\Suva\Models\CategoriesMappings::findFirst($id);
	}

	public function Layout(){
		$id = $this->layout_id > 0 ? $this->layout_id : -1;
		return \Baseapp\Suva\Models\Layouts::findFirst($id);
	}
	
	public function MappedCategory(){
		$id = $this->mapped_category_id > 0 ? $this->mapped_category_id : -1;
		return \Baseapp\Suva\Models\Categories::findFirst($id);
	}

	public function MappedDictionary(){
		$id = $this->mapped_dictionary_id > 0 ? $this->mapped_dictionary_id : -1;
		return \Baseapp\Suva\Models\Dictionaries::findFirst($id);
	}
	
	public function MappedLocation(){
		$id = $this->mapped_location_id > 0 ? $this->mapped_location_id : -1;
		return \Baseapp\Suva\Models\Locations::findFirst($id);
	}


	public function Publication(){
		$pubId = $this->publication_id > 0 ? $this->publication_id : -1;
		return \Baseapp\Suva\Models\Publications::findFirst($pubId);
	}
	
	public function getAdIds(){
		$id = $this->mapped_category_id > 0 ? $this->mapped_category_id : -1;
		
		$ads = \Baseapp\Suva\Models\Ads::find(" category_id = $id ");
		
		$arrAdIds = array();
		foreach ( $ads as $ad ){
			$matchesDictionaryId = false;
			if ( $this->mapped_dictionary_id > 0 ){
				$txtSql = " 
					select count(*) as nr_rec
						from ads_parameters ap on (ap.ad_id = $ad->id)
							join parameters p on (p.id = ap.parameter_id)
								join parameters_types pt on (pt.id = p.type_id and pt.accept_dictionary = 1)
					where 
						ap.value = '$this->mapped_dictionary_id'
						
				";
				$rsCount = \Baseapp\Suva\Library\Nava::sqlToArray( $txtSql );
				if ( $rsCount[0]['nr_rec'] > 0 ){
					$matchesDictionaryId = true;
				}
			}	
			$satisfiesCriteriaDictionaryId = ( ( $this->mapped_dictionary_id > 0 && $matchesDictionaryId ) || $this->mapped_dictionary_id == null );
			
			$matchesLocationId = false;
			if ( $this->mapped_location_id > 0  ){
				if ( 
					$ad->country_id == $this->mapped_location_id 
					||
					$ad->county_id == $this->mapped_location_id 
					||
					$ad->municipality_id == $this->mapped_location_id 
					||
					$ad->city_id == $this->mapped_location_id 
				){
					$matchesLocationId = true;
				}
			}
			$satisfiesCriteriaLocationId = ( ( $this->mapped_location_id > 0 && $matchesLocationId ) || $this->mapped_location_id == null );
			
			if ( $satisfiesCriteriaDictionaryId && $satisfiesCriteriaLocationId ){
				array_push( $arrAdIds , $ad->id );
			}
		}
		
		return $arrAdIds;
	}

	
}
