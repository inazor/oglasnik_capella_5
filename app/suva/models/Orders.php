<?php

namespace Baseapp\Suva\Models;

use Baseapp\Library\Debug;
use Baseapp\Library\PDF417\Renderers\ImagickImageRenderer;
use Baseapp\Library\PDF417\Renderers\SvgRenderer;
use Baseapp\Library\Products\Online\Pushup;
use Baseapp\Library\Products\ProductsInterface;
use Baseapp\Library\Products\Trgovina;
use Baseapp\Library\Products\Trgovina\Izlog;
use Baseapp\Library\Utils;
use Illuminate\Support\Facades\HTML;
use Phalcon\Mvc\Model\Query\Builder;
use Phalcon\Mvc\Model\Resultset;
use Baseapp\Traits\BarcodeControllerMethods;

class Orders extends \Baseapp\Models\Orders
{
    
	use traitBaseSuvaModel;
	
	
	public function n_before_save( $request = null ){
		$user = $this->User();
		//error_log("Orders:n_before_save 1 user:".print_r($user->toArray(),true));
		
		//error_log("PRIJE:".print_r($this->toArray(),true));
		
		$this->n_total = 0.00;
		$this->n_total_without_tax = 0.00;
		$this->n_tax_amount = 0.00;
		$this->n_discount_amount = 0.00;
		$this->n_discount_percent = 0.00;
		$this->n_agency_commission_amount = 0.00;
		$this->n_agency_commission_percent = 0.00;
		$this->n_frontend_sync_status = 'no_sync';
		foreach ($this->OrdersItems() as $oi) {
			
			$this->n_total += $oi->n_total;
			$this->n_discount_amount += $oi->n_discount_amount;
		}
		
		$this->n_discount_percent = $this->n_total != 0.00 ? $this->n_discount_amount / $this->n_total * 100 : 0.00;
		
		$this->n_agency_commission_percent = $user ? $user->n_agency_commission : 0.00;
		$this->n_agency_commission_amount = $this->n_total * ( $this->n_agency_commission_percent / 100);
		
		$this->n_total = $this->n_total  - $this->n_agency_commission_amount;
		
		$tax = 0.00;
		if ($user)
			if ($user->n_pays_vat == 1) 
				$tax = 0.25; 
		
		$this->n_tax_amount = $this->n_total * $tax;
		$this->n_total_with_tax = $this->n_total + $this->n_tax_amount;
		
		$this->total = Utils::kn2lp( $this->n_total );
		return null;
		
	}
	

	//BACKWARD COMPATIBILITY ZA AGIMOVU APLIKACIJU
	public function updateAdLatestPaymentState(){
		//Latest payment state služi jer se prema njemu listaju oglasi za moderaciju na adminu
		if ( ! $this->id > 0 ) return;
		
		foreach ( $this->OrdersItems() as $oi ){
			if ( $oi->Product() && $oi->Product()->is_online_product && $oi->Ad() ) {
				\Baseapp\Suva\Library\Nava::runSql("update ads set latest_payment_state = $this->status where id = $oi->ad_id ");
			}
		}
	}
	
	//polja many2one
	public function AppliedDocument(){
		$id = $this->n_applied_document_id >0 ? $this->n_applied_document_id : -1;
		return \Baseapp\Suva\Models\Orders::findFirst($id);
	}
	public function FiscalLocation(){
		$id = $this->n_fiscal_location_id >0 ? $this->n_fiscal_location_id : -1;
		return \Baseapp\Suva\Models\FiscalLocations::findFirst($id);
	}
	
	public function OrderStatus(){
		return \Baseapp\Suva\Models\OrdersStatuses::findFirst($this->n_status);
	}
	public function SalesRep(){
		$id = $this->n_sales_rep_id >0 ? $this->n_sales_rep_id : -1;
		return \Baseapp\Suva\Models\Users::findFirst($id);
	}
	public function Operator(){
		$id = $this->n_operator_id >0 ? $this->n_operator_id : -1;
		return \Baseapp\Suva\Models\Users::findFirst($id);
	}
	public function SubCustomer(){
		$subCustomerId = $this->n_sub_customer >0 ? $this->n_sub_customer : -1;
		return \Baseapp\Suva\Models\Users::findFirst($subCustomerId);
	}
	public function User(){
		$userId = $this->user_id >0 ? $this->user_id : -1;
		return \Baseapp\Suva\Models\Users::findFirst($userId);
	}
	public function PaymentMethod(){
		return \Baseapp\Suva\Models\PaymentMethods::findFirst($this->n_payment_methods_id);
	}
	
	
	//polja one2many
	public function OrdersItems( $p = null ){
		if (!$this->id > 0) return null;
		$findParam = array();
		$txtWhere = '';
		if (gettype($p)=='string') $txtWhere = " and $p";
		elseif (is_array($p)){
			if (array_key_exists(0,$p)) $txtWhere = " and $p[0]";
			if (array_key_exists("order",$p)) $findParam["order"] = $p["order"];
			if (array_key_exists("limit",$p)) $findParam["limit"] = $p["limit"];
		} 
		$findParam[] = "order_id = $this->id ".$txtWhere;
		//return \Baseapp\Suva\Models\Ads::find("user_id = $this->id");
		return \Baseapp\Suva\Models\OrdersItems::find($findParam);
	}
	 
	//kreiranje novih dokumenata za oglasnik
	public function getCartForUser($pUserId = null){
		$existingCart = \Baseapp\Suva\Models\Orders::findFirst(" user_id = $pUserId and n_status = 1 ");
		if ($existingCart) return $existingCart;
		else{
			$cart = new Orders();

			$cart->user_id = $pUserId;
			$cart->status = 1;
			$cart->created_at = date('Y-m-d H:i:s');
			$cart->created_at = date('Y-m-d H:i:s');
			//$cart->ip = $this->request->getClientAddress();
			$cart->n_status = 1;
			
			if ($cart->save() == true) return $cart;
			else {
				$err_msg = "";
				foreach ($cart->getMessages() as $message) {
					$err_msg .= $message . "\n\r";
				return $err_msg;
				}
			}
		}
	}
	
	
	//kreiranje novih dokumenata za oglasnik
	public static function getFreeAdsListForUser($pUserId = null){
		$existingCart = \Baseapp\Suva\Models\Orders::findFirst(" user_id = $pUserId and n_status = 2 ");
		if ($existingCart) return $existingCart;
		else{
			$cart = new Orders();
 
			$cart->user_id = $pUserId;
			$cart->status = 1;
			$cart->created_at = date('Y-m-d H:i:s');
			$cart->created_at = date('Y-m-d H:i:s');
			//$cart->ip = $this->request->getClientAddress();
			$cart->n_status = 2;
			
			if ($cart->save() == true) return $cart;
			else {
				$err_msg = "";
				foreach ($cart->getMessages() as $message) {
					$err_msg .= $message . "\n\r";
				return $err_msg;
				}
			}
		}
	}	
	
	
	public static function createOffer ( $pLoggedUserId = null, $pSelectedUserId = null, $pSource = 'backend' ){
		$pboPrefix = \Baseapp\Suva\Library\libAppSetting::get('fin_invoice_prefix');
		//error_log("Ads2Controller:subUsersCartAction offer4");
		\Baseapp\Suva\Library\Nava::runSql("insert into orders (status, total, created_at, modified_at) values (0,0, now(), now())");
		$order_id = \Baseapp\Suva\Library\Nava::sqlToArray("select max(id) as id  from orders ")[0]['id'];
		$pbo = (intval($pboPrefix) + intval($order_id));
		//Nava::poruka("PBO JE".$pbo);

		//14.2.2017 - parametar pSource po defaultu je 'backend'
		
		$txtSql =
			" update orders set "
		  ." user_id = $pSelectedUserId "
		  .", n_sales_rep_id = $pLoggedUserId "
		  .", n_status = 3 "
		  .", status = 1 "
		  .", n_quotation_date = now() "
		  .", n_pbo = '$pbo'"
		  .", n_source = '$pSource'"
		  ." where id = $order_id ";
		//error_log ($txtSql);ž
		//Nava::poruka($txtSql);
		\Baseapp\Suva\Library\Nava::runSql( $txtSql );
		
		return self::findFirst($order_id);
	}

}
