<?php

namespace Baseapp\Suva\Models;

/**
 * DiscountsPublications Model
 */
class DiscountsPublications extends BaseModelBlamable
{
    public function initialize()
    {
        $this->setSource("n_discounts_publications");
    }

        public function getSource()
    {
        return 'n_discounts_publications';
    }

   /**
     * Add a new 
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function add_new($request)
    {
        $this->initialize_model_with_post($request);

            if (true === $this->create()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
    }

    /**
     * Save Publication data method
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function save_changes($request)
    {

        $this->initialize_model_with_post($request);

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            if (true === $this->update()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }
	
	//veze many2One
	
	Public function Discount(){
		return \Baseapp\Suva\Models\Discounts::findFirst($this->discounts_id);
	}
	Public function Product(){
		return \Baseapp\Suva\Models\Products::findFirst($this->products_id);
	}
	Public function Publication(){
		return \Baseapp\Suva\Models\Publications::findFirst($this->publications_id_id);
	}

}