<?php

namespace Baseapp\Suva\Models;

// use Phalcon\Mvc\Model\Resultset;
// use Baseapp\Library\Validations\Dictionaries as DictionariesValidations;
// use Baseapp\Traits\FormModelControllerTrait;
// use Baseapp\Traits\NestedSetActions;
// use Baseapp\Library\Behavior\NestedSet as NestedSetBehavior;

/**
 * Dictionaries Model
 */
class Dictionaries extends \Baseapp\Models\Dictionaries
{
	use traitBaseSuvaModel;
    // use FormModelControllerTrait;
    // use NestedSetActions;

    // /**
     // * Dictionaries initialize
     // */
    // public function initialize()
    // {
        // parent::initialize();

        // $this->addBehavior(new NestedSetBehavior(array(
            // 'hasManyRoots'   => true,
            // 'rootAttribute'  => 'root_id',
            // 'rightAttribute' => 'rght',
            // 'parentAttribute' => 'parent_id'
        // )));

        // $this->hasMany('id', __NAMESPACE__ . '\Parameters', 'dictionary_id', array(
            // 'alias' => 'Parameters'
        // ));
    // }

    // /**
     // * Add new dictionary or dictionary item (if $parent is not null)
     // *
     // * @param \Phalcon\Http\RequestInterface $request
     // * @param \Baseapp\Suva\Models\Dictionaries|null $parent
     // *
     // * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     // */
    // public function add_new($request, $parent = null)
    // {
        // $this->populate_data_with_post_values($request);
        // if (null == $parent) {
            // $this->parent_id = null;
        // }

        // $validation = new DictionariesValidations();
        // if (null === $parent) {
            // $extra_conditions = array(
                // 'lft = :lft: AND level = :level:',
                // array(
                    // 'lft' => 1,
                    // 'level' => 1
                // )
            // );
            // $validation_message = 'Dictionary name should be unique';
        // } else {
            // $extra_conditions = array(
                // 'parent_id = :parent_id: AND level = :level:',
                // array(
                    // 'parent_id' => $parent->id,
                    // 'level' => $parent->level + 1
                // )
            // );
            // $validation_message = 'Dictionary value name should be unique';
        // }
        // if (null === $parent) {
            // $validation->add('slug', new \Phalcon\Validation\Validator\PresenceOf(array(
                // 'message' => 'Matching slug cannot be empty'
            // )));
            // $validation->add('slug', new \Baseapp\Extension\Validator\Uniqueness(array(
                // 'model' => '\Baseapp\Suva\Models\Dictionaries',
                // 'message' => 'Mathcing slug should be unique',
            // )));
        // }
        // $validation->add('name', new \Baseapp\Extension\Validator\Uniqueness(array(
            // 'model' => '\Baseapp\Suva\Models\Dictionaries',
            // 'extra_conditions' => $extra_conditions,
            // 'message' => $validation_message,
        // )));

        // $messages = $validation->validate($request->getPost());

        // if (count($messages)) {
            // return $validation->getMessages();
        // } else {
            // if (null == $parent) {
                // $insert_result = $this->saveNode();
            // } else {
                // $insert_result = $this->appendTo($parent);
            // }

            // if (true === $insert_result) {
                // return $this;
            // } else {
                // \Baseapp\Bootstrap::log($this->getMessages());
                // return $this->getMessages();
            // }
        // }
    // }

    // /**
     // * Save existing dictionary or dictionary item (if $parent is not null)
     // *
     // * @param \Phalcon\Http\RequestInterface $request
     // * @param \Baseapp\Suva\Models\Dictionaries|null $parent
     // *
     // * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     // */
    // public function save_changes($request, $parent = null)
    // {
        // $validation = new DictionariesValidations();

        // $name_lc      = mb_strtolower($this->name, 'UTF-8');
        // $name_post_lc = mb_strtolower($request->getPost('name'), 'UTF-8');
        // if ($name_lc != $name_post_lc) {
            // if (null === $parent) {
                // $extra_conditions = array(
                    // 'lft = :lft: AND level = :level:',
                    // array(
                        // 'lft' => 1,
                        // 'level' => 1
                    // )
                // );
                // $validation_message = 'Dictionary name should be unique';
            // } else {
                // $extra_conditions = array(
                    // 'parent_id = :parent_id: AND level = :level:',
                    // array(
                        // 'parent_id' => $parent->id,
                        // 'level' => $parent->level + 1
                    // )
                // );
                // $validation_message = 'Dictionary value name should be unique';
            // }
            // $validation->add('name', new \Baseapp\Extension\Validator\Uniqueness(array(
                // 'model' => '\Baseapp\Suva\Models\Dictionaries',
                // 'extra_conditions' => $extra_conditions,
                // 'message' => $validation_message,
            // )));
        // }
        // if (null === $parent) {
            // $validation->add('slug', new \Phalcon\Validation\Validator\PresenceOf(array(
                // 'message' => 'Matching slug cannot be empty'
            // )));
            // if ($this->slug != $request->getPost('slug')) {
                // $validation->add('slug', new \Baseapp\Extension\Validator\Uniqueness(array(
                    // 'model' => '\Baseapp\Suva\Models\Dictionaries',
                    // 'message' => 'Mathcing slug should be unique',
                // )));
            // }
        // }

        // $messages = $validation->validate($request->getPost());

        // $this->populate_data_with_post_values($request);
        // if ($this->parent_id == $this->id) {
            // $this->parent_id = null;
        // }

        // if (count($messages)) {
            // return $validation->getMessages();
        // } else {
            // if ($parent) {
                // // if item was moved to another parent then $parent->id and $this->parent()->id differ!
                // if ((int) $this->parent()->id != (int) $parent->id) {
                    // // $parent = self::findFirst($parent->id);
                    // // $this->root_id = self::findFirst($parent->id)->root()->id;
                    // $save_result = $this->moveAsLast($parent);
                // } else {
                    // // $this->root_id = self::findFirst($parent->id)->root()->id;
                    // $save_result = $this->saveNode();
                // }
            // } else {
                // $save_result = $this->saveNode();
            // }
            // if (true === $save_result) {
                // return $this;
            // } else {
                // \Baseapp\Bootstrap::log($this->getMessages());
                // return $this->getMessages();
            // }
        // }
    // }

    // /**
     // * Helper function so we can get children of the current dictionary and not to expose real function name (children) :)
     // */
    // public function values()
    // {
        // return $this->children();
    // }

    // /**
     // * Helper function to get depth of a dictionary (number of levels where data is stored... It is assumed that all
     // * dictionaries will have at least 2 levels -> first level is the name of the dictionary, and second level are it'd
     // * direct descendats, or how we are calling them here - dictionary values). So, the logic is that this function will
     // * return and array of possible levels only for those dictionaries that have more than 2 levels (like vehicle
     // * make/model hierarchy or HI-FI equipment with Manufacturer/Model hierarchy and so on...)
     // *
     // * @return array Array with possible levels is returned, or in case where only 2 levels exist, an empty array is
     // *               returned
     // */
    // public function getLevels()
    // {
        // // as dictionaries are handeled as a tree, first level nodes are actually Dictionary names, so direct
        // // descendants of a specific dictionary have level = 2, and so on... For user interface, and for easier
        // // understanding of 'levels' we will 'ignore' Dictionary name level, and start counting from the direct
        // // descendats level as first one...

        // $maxLevel = $this->maximum(array(
            // 'root_id = :root_id:',
            // 'column' => 'level',
            // 'bind' => array(
                // 'root_id' => $this->root_id
            // )
        // ));

        // $levels = array();
        // if ($maxLevel > 2) {
            // $maxLevel = $maxLevel - 1;

            // for ($i = 1; $i <= $maxLevel; $i++) {
                // $levels[] = $i;
            // }
        // }

        // return $levels;
    // }

    // /**
     // * Returns an array containing no more than $values_per_level "dict values"
     // * (in this case node names) for each level of the tree that the
     // * current node belongs to.
     // *
     // * Throws if no tree or dictionary is specified and we're not an existing node.
     // *
     // * @param int $dictionary_id_or_tree
     // * @param int|array|null $values_per_level
     // *
     // * @return array
     // * @throws \Exception
     // */
    // public function getExampleTreeValues($values_per_level = 3, $dictionary_id_or_tree = null)
    // {
        // $values = array();
        // $tree = null;

        // if (null === $dictionary_id_or_tree) {
            // // No dict or tree specified, but we're in multi-root mode
            // // and we don't have a way to know which tree to fetch here since
            // // nothing was provided to us except: $model = new Dictionaries();
            // if (!isset($this->id)) {
                // throw new \Exception('No dictionary id or tree specified and no known node available');
            // } else {
                // $root = $this->getRoot();
                // $tree = $root->getTree();
            // }
        // } else {
            // if (is_int($dictionary_id_or_tree)) {
                // $tree = $this->getTree($dictionary_id_or_tree);
            // }
            // if (is_array($dictionary_id_or_tree)) {
                // $tree = $dictionary_id_or_tree;
            // }
        // }

        // if ($tree) {

            // $group_by_level_without_first = function() use ($tree) {
                // $level_tree = array();
                // foreach ($tree as $id => $node) {
                    // if ($id > 0) {
                        // if ($node->level > 1) {
                            // $level_tree[$node->level][$id] = $node;
                        // }
                    // }
                // }
                // unset($id, $node);
                // return $level_tree;
            // };

            // $level_tree = $group_by_level_without_first();
            // $levels_cnt = 0;
            // unset($tree);
            // foreach ($level_tree as $level => $nodes) {
                // $values[$levels_cnt] = array();
                // $values_cnt = 0;
                // foreach ($nodes as $id => $node) {
                    // if ($values_cnt < $values_per_level) {
                        // $values[$levels_cnt][] = $node->name;
                    // }
                    // $values_cnt++;
                // }
                // $levels_cnt++;
            // }
        // }

        // return $values;
    // }

    // public function hasChildren()
    // {
        // $has_children = false;
        // $children = $this->children();
        // if ($children && count($children)) {
            // $has_children = true;
        // }
        // return $has_children;
    // }


// /*  This will remain here in case test show that currently used view->partial() is much slower than this...

    // public function getPrintableTree2()
    // {
        // $output = '';

        // $descendants = $this->descendants();
        // $level = $this->level;
        // $addition_indent = 1;

        // foreach ($descendants as $item) {
            // if ($item->level == 2) {
                // $addition_indent = $item->level -1;
            // }
            // if ($item->level == $level) {
                // $output .= str_repeat("\t", $level-2 + --$addition_indent) . "</li><!-- /.category_li - Level: " . ($level) . "-->\n";
                // --$addition_indent;
            // } elseif ($item->level > $level) {
                // if ($item->level == 2) {
                    // // this is actually a root node
                    // $output .= "\n" . str_repeat("\t", $level-1) . "<ul class='sortable ui-sortable' data-level='".($item->level-1)."'>\n"; // class='sortable ui-sortable'
                // } else {
                    // $output .= str_repeat("\t", $level-2 + $addition_indent) . "<ul class='subcategory' data-level='".($item->level-1)."' data-add='{$addition_indent}'>\n";
                // }
            // } else {
                // --$addition_indent;
                // $output .= str_repeat("\t", $level+1 + --$addition_indent) . "</li><!-- /.category_li - Level: " . $level . " -->\n";
                // for ($i = $level - $item->level; $i; $i--) {
                    // $output .= str_repeat("\t", $level + $i - 2) . "</ul><!-- /.subcategory - Level: " . ($level-1) . " -->\n";
                    // $output .= str_repeat("\t", $level-- + $i - 3) . "</li><!-- /.category_li - Level: " . ($level-1) . " -->\n";
                // }
                // if ($item->level == 2) {
                    // $addition_indent = $item->level -1;
                    // $level--;
                // }
            // }

            // $hasChildren = false;
            // if (count($item->children())) {
                // $hasChildren = true;
            // }

            // $output .= str_repeat("\t", $level-1 + $addition_indent++) . "<li id='item_{$item->id}' data-id='{$item->id}' class='category_li' data-level='{$item->level}' data-add='{$addition_indent}'>\n";
            // $output .= str_repeat("\t", $level-1 + $addition_indent++) . "<div class='category_div".($hasChildren ? ' with-children' : '')."'>\n";
            // $output .= str_repeat("\t", $level-1 + $addition_indent++) . "<div class='category_row'>\n";
            // $output .= str_repeat("\t", $level-1 + $addition_indent) . "<div class='drag-me ui-sortable-handle'><i class='fa fa-arrows'></i></div>\n";
            // $output .= str_repeat("\t", $level-1 + $addition_indent++) . "<div class='childrens".($hasChildren ? ' clickable' : '')."'>\n";
            // $output .= str_repeat("\t", $level-1 + $addition_indent--) . "<span class='toggle fa ".($hasChildren ? 'fa-plus-square-o' : 'fa-angle-right')."'></span>\n";
            // $output .= str_repeat("\t", $level-1 + $addition_indent) . "</div>\n";
            // $output .= str_repeat("\t", $level-1 + $addition_indent++) . "<div class='name-cat'>\n";
            // $output .= str_repeat("\t", $level-1 + $addition_indent--) . "<span class='name'>{$item->name}</span>\n";
            // $output .= str_repeat("\t", $level-1 + $addition_indent) . "</div>\n";
            // $output .= str_repeat("\t", $level-1 + $addition_indent++) . "<div class='actions-cat'>\n";
            // $output .= str_repeat("\t", $level-1 + $addition_indent--) . "<a href='/admin/dictionaries/editValue/{$item->id}'><i class='fa fa-edit'></i> Edit</a> · <a href='/admin/dictionaries/deleteValue/{$item->id}'><i class='fa fa-trash-o text-danger'></i></a>\n";
            // $output .= str_repeat("\t", $level-1 + $addition_indent--) . "</div>\n";
            // $output .= str_repeat("\t", $level-1 + $addition_indent--) . "</div>\n";
            // $output .= str_repeat("\t", $level-1 + $addition_indent) . "</div>\n";

            // $level = $item->level;
        // }

        // for ($i = $level - 1; $i; $i--) {

            // $output .= str_repeat("\t", $level--) . "</li><!-- /.category_li - Level: " . ($level+1) . " -->\n";
            // $output .= str_repeat("\t", $level--) . "</ul><!-- /" . ($level > -1 ? '.subcategory - Level: ' . ($level+1) : '.sortable') . " -->\n";
        // }

        // return $output;
    // }
// */
 
	public function getPath(){
		
		$txt = '';
		if ( $this->id > 0 ){
			$txt = $this->name;
			if ( $this->parent_id > 0 ) {
				$parent1 = \Baseapp\Models\Dictionaries::findFirst ( $this->parent_id );
				if ( $parent1 ){
					$txt = $parent1->name ." - ".$txt;
					
					if ( $parent1->parent_id > 0) {
						$parent2 = \Baseapp\Models\Dictionaries::findFirst ( $parent1->parent_id );
						if ($parent2){
							$txt = $parent2->name ." - ".$txt;
							
							
							if ($parent2->parent_id > 0) {
								$parent3 = \Baseapp\Models\Dictionaries::findFirst ( $parent2->parent_id );
								if ($parent3){
									$txt = $parent3->name ." - ".$txt;
								}
							}
							
						}
					}
				}
			}
		}
		
		return $txt;
		
		
		
	}
}
