<?php

namespace Baseapp\Suva\Models;
use Baseapp\Suva\Library\Nava;


/**
 * Publications Model
 */
class PublicationsCategories extends BaseModelBlamable
{
    public function initialize()
    {
        $this->setSource("n_publications_categories");
    }

        public function getSource()
    {
        return 'n_publications_categories';
    }


	public function PublicationsCategoriesMappings( $p = null ) { return $this->getMany('PublicationsCategoriesMappings', $p, " category_mapping_id = $this->id " ); }

	
	public function Category(){
		$id = $this->category_id > 0 ? $this->category_id : -1;
		return \Baseapp\Suva\Models\Categories::findFirst($id);
	}

	public function ForwardCategory(){
		$id = $this->forward_category_id > 0 ? $this->forward_category_id : -1;
		return \Baseapp\Suva\Models\Categories::findFirst($id);
	}

	public function Layout(){
		$id = $this->layouts_id > 0 ? $this->layouts_id : -1;
		return \Baseapp\Suva\Models\Layouts::findFirst($id);
	}

	public function Publication(){
		$pubId = $this->publication_id > 0 ? $this->publication_id : -1;
		return \Baseapp\Suva\Models\Publications::findFirst($pubId);
	}

	
}
