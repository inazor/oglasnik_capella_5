<?php

namespace Baseapp\Suva\Models;

//use Baseapp\Library\Validations\CategoriesFieldsets as CategoriesFieldsetsValidations;
//use Baseapp\Traits\FormModelControllerTrait;

/**
 * CategoriesFieldsets Model
 */
//class CategoriesFieldsets extends BaseModelBlamable
class CategoriesFieldsets extends \Baseapp\Models\CategoriesFieldsets
{
    use traitBaseSuvaModel;
	
	// //use FormModelControllerTrait;

    // /**
     // * CategoriesFieldsets initialize
     // */
    // public function initialize()
    // {
        // parent::initialize();

        // // Every category fieldset belongs to a category
        // $this->belongsTo(
            // 'category_id', __NAMESPACE__ . '\Categories', 'id',
            // array(
                // 'alias' => 'Category',
                // 'foreignKey' => array(
                    // 'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE
                // )
            // )
        // );

        // // Category fieldset can have many parameters attached to it
        // $this->hasMany(
            // 'id', __NAMESPACE__ . '\CategoriesFieldsetsParameters', 'category_fieldset_id',
            // array(
                // 'alias' => 'Parameters',
                // 'foreignKey' => array(
                    // 'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE
                // )
            // )
        // );
    // }

    // public function getParametersIDs()
    // {
        // $parameter_ids = array();

        // $parameter_ids_array = $this->getParameters(array('columns' => 'parameter_id'))->toArray();

        // foreach ($parameter_ids_array as $pid) {
            // $parameter_ids[] = (int) $pid['parameter_id'];
        // }

        // return $parameter_ids;
    // }

    // public function getSettings()
    // {
        // $settings = null;

        // if ($this->settings && trim($this->settings)) {
            // $settings = json_decode(trim($this->settings));
        // }

        // if ($settings) {
            // return json_encode($settings);
        // }
        // return $settings;
    // }

    // // TODO: remove 'CategoriesFieldsets->render_view()' method
    // public function render_view($data = null) {

        // $render = array(
            // 'html' => '',
            // 'map' => null,
            // 'js' => array()
        // );

        // $parameters = $this->getParameters(array('order' => 'sort_order'));
        // if ($parameters && count($parameters)) {
            // $style = null;
            // if ($this->settings && $fieldset_settings = json_decode($this->settings)) {
                // $style = isset($fieldset_settings->style) ? trim($fieldset_settings->style) : null;
            // }

            // $parameters_html = '';

            // foreach ($parameters as $parameter) {
                // $fieldset_parameter_render_data = $parameter->render_view($data, $style);
                // $fieldset_parameter_html = $fieldset_parameter_render_data['html'];
                // if ($fieldset_parameter_html) {
                    // $parameters_html .= $fieldset_parameter_html;
                // }
                // if (isset($fieldset_parameter_render_data['map'])) {
                    // $render['map'] = $fieldset_parameter_render_data['map'];
                // }
            // }

            // if ($parameters_html) {
                // if ('group' === $style) {
                    // $render['html'] .= '<section class="group">' . PHP_EOL;
                    // if ($this->name) {
                        // $render['html'] .= '    <h3 class="underline"><span class="underline">' . $this->name . '</span></h3>' . PHP_EOL;
                    // } else {
                        // //$render['html'] .= '    <h3 class="underline"></h3>' . PHP_EOL;
                    // }
                    // $render['html'] .= '    <div class="row">' . PHP_EOL;
                    // $render['html'] .= $parameters_html;
                    // $render['html'] .= '    </div>' . PHP_EOL;
                    // $render['html'] .= '</section>' . PHP_EOL;
                // } elseif ('merge' === $style) {
                    // $render['html'] .= '<section class="merge">' . PHP_EOL;
                    // if ($this->name) {
                        // $render['html'] .= '    <h3 class="underline"><span class="underline">' . $this->name . '</span></h3>' . PHP_EOL;
                    // } else {
                        // //$render['html'] .= '    <h3 class="underline"></h3>' . PHP_EOL;
                    // }
                    // $render['html'] .= '    <div class="row">' . PHP_EOL;
                    // $render['html'] .= $parameters_html;
                    // $render['html'] .= '    </div>' . PHP_EOL;
                    // $render['html'] .= '</section>' . PHP_EOL;
                // }
            // }
        // }

        // return $render;
    // }
	
	
//DODANO
//veze many2one
    public function Category(){
		$id = $this->category_id > 0 ? $this->category_id : -1;
		return \Baseapp\Suva\Models\Categories::findFirst($id);
    }

	
//veze one2many

	public function CategoriesFieldsetsParameters ( $p = null ){
		if (! $this->id > 0 ) return null;
		$findParam = array();
		if (gettype($p)=='string') $findParam[0] = " and $p";
		elseif (is_array($p)){
			if ($p[0]) $findParam[0] = " and $p[0]";
			if ($p["order"]) $findParam["order"] = $p["order"];
			if (array_key_exists('limit',$p)) $findParam["limit"] = $p["limit"]; else $findParam["limit"] = 20;
		} 
		$findParam[0] = "category_fieldset_id = $this->id ".$findParam[0];
		
		return \Baseapp\Suva\Models\CategoriesFieldsetsParameters::find($findParam);
	}


}
