<?php

namespace Baseapp\Cli\Tasks;

use Baseapp\Console;
use Baseapp\Models\Ads;
use Baseapp\Models\Users;
use Phalcon\Queue\Beanstalk;

class QueueTask extends MainTask
{
    const SLEEP_AFTER_SEND = 1;
    const BATCH_REST_DURATION = 120;
    const BATCH_REST_TRIGGER_AMOUNT = 500; // Sleep for BATCH_REST_DURATION seconds after each batch of BATCH_REST_TRIGGER_AMOUNT send calls

    // First in list is chosen as default if not specified (where a template might be needed)
    const VALID_NOTIFICATIONS_TEMPLATES = array(
        'ad-expires-soon',
        'ad-expired'
    );

    public function notificationsConsumerAction($params = null)
    {
        /** @var Beanstalk $q */
        $q = $this->getDI()->get('beanstalk');

        $template  = $this->requireValidTemplateParam($this->router->getParams(), 0);
        $tube_name = Ads::NOTIFICATIONS_TUBE_PREFIX . $template;

        try {
            $q->watch($tube_name);
        } catch (\Phalcon\Queue\Beanstalk\Exception $e) {
            Console::exception($e);
        }

        $this->output('Watching tube: ' . $tube_name);
        static $cnt = 0;

        while (($job = $q->reserve()) !== false) {
            $cnt++;

            $this->output('Handling job id: ' . $job->getId());

            $ad_id = $job->getBody();
            $ad    = Ads::findFirst($ad_id);

            $ok = false;
            if ($ad) {
                // Check if the user has/had any kind of shop, and if so, skip notifying that user (as requested)
                // TODO/FIXME: this could be done with a simple join query instead of these separate queries...
                $shop = null;
                if ($ad->user_id) {
                    $ad_user = Users::findFirst($ad->user_id);
                    if ($ad_user) {
                        $shop = $ad_user->getShop();
                    }
                }

                // Send notification unless it's a "shop user"
                if (!$shop) {
                    $ok = Ads::sendExpiryNotificationEmail($ad, $template);
                } else {
                    // Mark as ok/processed even though the actual sending is skipped
                    $ok = true;
                    $this->output('Skipped sending but returning ok, because it is a "shop user".');
                }
            }

            $this->output('Job done, result: ' . var_export($ok, true));
            if ($ok) {
                $deleted = $job->delete();
                if ($deleted) {
                    $this->output('Job deleted.');
                } else {
                    $this->output('Failed to delete job!');
                }
            } else {
                $released = $job->release();
                if ($released) {
                    $this->output('Job released since sending returned false.');
                } else {
                    $this->output('Failed to release job!');
                }
            }

            // sleep for SLEEP_AFTER_EACH_SEND seconds if configured and if send really happened
            if (self::SLEEP_AFTER_SEND > 0 && $ok) {
                // Gives the SMTP server some breathing room between going again
                $this->output('Sleeping for ' . self::SLEEP_AFTER_SEND . ' second(s) after each send()');
                sleep(self::SLEEP_AFTER_SEND);
                $this->output('Done sleeping.');
            }

            // sleep for BATCH_REST_DURATION after each BATCH_REST_TRIGGER_AMOUNT of loop iterations (sends)
            // Aka, sleep for 2 minutes after actually sending 500 emails to give the smtp server
            // a chance to process them...
            $remainder = ($cnt % self::BATCH_REST_TRIGGER_AMOUNT);
            if (0 === $remainder) {
                $this->output('Sleeping for ' . self::BATCH_REST_DURATION . ' second(s) after ' . self::BATCH_REST_TRIGGER_AMOUNT . ' sends');
                sleep(self::BATCH_REST_DURATION);
                $this->output('Done sleeping.');
            }
        }
    }

    public function notificationsProducerAction($params = null)
    {
        $params = $this->router->getParams();

        if (empty($params)) {
            Console::error('Missing required `ad_id` and `template` parameters');
        }

        if (!isset($params[0]) || empty($params[0])) {
            Console::error('Ad ID not specified');
        }

        $ad_id    = $params[0];
        $template = $this->requireValidTemplateParam($params, 1);

        $ad = Ads::findFirst($ad_id);
        if (!$ad) {
            Console::error('Specified Ad (' . $ad_id . ') not found');
        }

        $job_ids = Ads::queueExpiryNotificationJobs(array($ad_id), $template);
        print_r($job_ids);
    }

    public function notificationsClearAction()
    {
        $params = $this->router->getParams();

        if (empty($params) || (!isset($params[0]) || empty($params[0]))) {
            Console::error('Missing required `template` parameter');
        }

        $template = $this->requireValidTemplateParam($params, 0);

        $tube = Ads::NOTIFICATIONS_TUBE_PREFIX . $template;
        $this->output('Clearing tube: ' . $tube);

        /** @var Beanstalk $q */
        // Clearing is done by peeking into all jobs and deleting them in a loop...
        $q = $this->getDI()->get('beanstalk');
        $q->watch($tube);
        while (($job = $q->reserve(0)) !== false) {
            $job->delete();
        }

        $this->output('Cleared.');
    }

    /**
     * @param string $msg
     */
    private function output($msg)
    {
        $date = \DateTime::createFromFormat('U.u', sprintf('%.f', microtime(true)))->format('Y-m-d\TH:i:s.uO');
        echo "\n[" . $date . "] " . $msg;
    }

    /**
     * @param array $params
     * @param int $position
     *
     * @return string
     */
    private function requireValidTemplateParam($params = null, $position)
    {
        if (null === $params) {
            $params = $this->router->getParams();
        }

        if (!isset($params[$position]) || empty($params[$position])) {
            Console::error('Missing required `template` parameter in $params[' . $position . ']');
        }

        // Make sure the specified template is a valid one
        $template = $params[$position];
        if (!in_array($template, self::VALID_NOTIFICATIONS_TEMPLATES)) {
            Console::error('Invalid `template` specified (' . $template . '), valid values: ' . implode(', ', self::VALID_NOTIFICATIONS_TEMPLATES));
        }

        return $template;
    }
}
