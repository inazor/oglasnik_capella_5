<div>
    {% if auth.logged_in(['admin','supersupport','support','moderator','finance','sales']) %}
    <div class="pull-right">
        <a href="{{ this.url.get('admin/ads/edit/' ~ ad.id ~ '?next=' ~ requested_path) }}" class="btn btn-info btn-xs margin-bottom-sm" target="_blank"><span class="fa fa-edit fa-fw"></span> Edit in admin</a>
    </div>
    {% endif %}
    {% include 'chunks/breadcrumbs.volt' %}
</div>
{% if special_warning -%}<div class="alert alert-info text-center">{{ special_warning }}</div>{%- endif %}
{{ flashSession.output() }}

{% set canSendMessage = (ad_user is defined and ad_user and show_contact_form) %}

<div class="row no-gutters-xs">
    <div class="col-md-12">
        <h1 class="h2-like no-top-margin color-light-blue">{{ ad.title|striptags }}</h1>
    </div>
    <div class="col-md-9 overflow-hidden">
        <div class="row">
            <div class="col-md-8 margin-bottom-md classified-view-media-col">
                <div id="galerija" class="gallery">
                    {% if ((ad_render['ad_media']|length > 0) or (ad_render['360'] is defined)) -%}
                        <div class="big">
                            <div id="blueimp-image-carousel" class="blueimp-gallery blueimp-gallery-carousel blueimp-gallery-controls">
                                <div class="slides"></div>
                                <a class="prev">‹</a>
                                <a class="next">›</a>
                                {#<span class="magnifier"><span class="fa fa-search"></span></span>#}
                            </div>
                        </div>
                        <div id="blueimp-fullscreen-gallery" class="blueimp-gallery blueimp-gallery-controls">
                            <div class="slides"></div>
                            <h3 class="title"></h3>
                            <a class="prev">‹</a>
                            <a class="next">›</a>
                            <a class="close"></a>
                            <a class="play-pause"></a>
                            <ol class="indicator"></ol>
                        </div>
                        <ul class="thumbnails" data-title="{{ ad.title|striptags|truncate(80)|escape_attr }}">
                            {# 360 should come first, but still not sure how it should behave, expect problems, because noone thinks ahead... #}
                            {% if ad_render['360'] is defined -%}
                                {% for ad_render_360 in ad_render['360'] %}
                                    {% if ad_render_360['spinid_url'] is defined %}
                                        <li data-type="spin/360" class="spin360" data-carousel-url="{{ ad_render_360['spinid_url'] }}" data-fullscreen-url="{{ ad_render_360['spinid_url'] }}"><img src="{{ ad_render_360['spinid_thumb_url'] }}" alt=""></li>
                                    {% else %}
                                        <li data-type="spingw/360" class="spin360 spin360gw" data-carousel-url="{{ ad_render_360['spinid'] }}" data-fullscreen-url="{{ ad_render_360['spinid'] }}">
                                            <img src="{{ ad_render_360['spinid_thumb_url'] }}" alt="">
                                            <div id="gw-element-{{ ad_render_360['spinid'] }}" data-gw="true" data-gw-id="{{ ad_render_360['spinid'] }}"></div>
                                        </li>
                                    {% endif %}
                                {% endfor %}
                            {% endif -%}
                            {% if (ad_render['ad_media']|length) %}
                                {% for ad_media in ad_render['ad_media'] -%}
                                    {% set carousel_thumb = ad_media.get_thumb('GalleryBig', false, ad.category_id) %}
                                    {% set carousel_orientation = carousel_thumb.getOrientation() %}
                                    {% set carousel_class = carousel_thumb.getClass() %}
                                    {%- set ad_media_carousel = carousel_thumb.getSrc() -%}
                                    {%- set ad_media_fullscreen = ad_media.get_url('GalleryFullscreen', false, ad.category_id) -%}
                                    {%- set ad_media_thumbnail = ad_media.get_url('GalleryThumb', false, ad.category_id) -%}
                                    <li data-class="{{ carousel_class }}" data-carousel-url="{{ ad_media_carousel }}" data-fullscreen-url="{{ ad_media_fullscreen }}"><img data-width="{{ carousel_thumb.getWidth() }}" data-height="{{ carousel_thumb.getHeight() }}" data-orient="{{ carousel_orientation }}" class="{{ carousel_orientation }}" src="{{ ad_media_thumbnail }}" alt=""></li>
                                {% endfor -%}
                            {% endif %}
                        </ul>
                    {% else %}
                        {% set category_fallback = Models_Categories__getFallbackImage(ad.category_id) %}
                        {% set thumb = Models_Media__getNoImageThumb('GalleryBig', null, category_fallback) %}
                        <div class="big {{ thumb.getClass() }}" style="padding-bottom:91.228070175%; background:#fff url({{ thumb.getSrc() }}) center center / cover no-repeat"></div>
                    {% endif -%}
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-4 classified-view-mid-col">
                <div class="top-details">
                    {% set heading_extra_class = '' %}

                    {% if ad_user is defined and ad_user %}
                        {% set ad_user_shop = ad_user.shop %}
                        {% set avatar = ad_user.getAvatar().getSrc() %}
                        <div>
                            <span class="border-radius overflow-hidden"><img class="avatar-header" src="{{ avatar }}" width="30" height="30" /></span>
                            {% set users_username = (ad_user.type == 1 ? ad_user.username : ad_user.company_name) %}
                            {% if ad_user_shop %}{% set users_username = ad_user_shop.title %}{% endif %}
                            {{ users_username }} <small><a href="{{ ad_user.ads_page_url }}">(Prikaži sve oglase)</a></small>
                        </div>
                    {% else %}
                        {% set heading_extra_class = ' no-top-margin' %}
                    {% endif %}

                    {# TODO: implement location based on given address #}
                    {% set adLocationData = ad.getLocationData() %}
                    {% if adLocationData %}
                        <p><img src="{{ url('assets/img/icn_map.png') }}" alt="Lokacija">&nbsp; {{ adLocationData['text'] ~ (adLocationData['gps']|default(null) ? ' <small><a href="#" class="scrollToMap">(Pogledaj na karti)</a></small>' : '')}}</p>
                    {% endif %}

                    {% set adPhoneNumbers = ad.getFormattedPublicPhoneNumbers() %}
                    {% if adPhoneNumbers|length %}
                    <p>
                        <img src="{{ url('assets/img/icn_telephone.png') }}" alt="Telefon">&nbsp;
                        {% for adPhoneNumber in adPhoneNumbers %}{% if adPhoneNumber['link'] is defined %}<a href="tel:{{ adPhoneNumber['link'] }}">{{ adPhoneNumber['text'] }}</a>{% else %}{{ adPhoneNumber['text'] }}{% endif %}{{ !loop.last ? ', ' : '' }}{% endfor %}
                    </p>
                    {% endif %}

                    <hr class="margin-bottom-10px-xs-only">

                    {% set ad_price = ad.getPrices() -%}
                    {% if ad_price %}
                        {% if ad_price['main'] > 0 -%}
                        <span class="price-oglas-details">cijena: {{ ad_price['main'] }}</span>
                        {% if ad_price['other'] %}<br>približno: {{ ad_price['other'] }}{% endif %}
                        <hr class="margin-bottom-0-xs-only">
                        {% endif %}
                    {%- endif %}
                    {% if ad_render['external_url_button'] %}
                    <div class="margin-top-15px visible-xs">
                        <a href="{{ ad_render['external_url_button']['url'] }}" class="btn secondary-red width-full" rel="external nofollow" target="_blank">{{ ad_render['external_url_button']['label'] }}</a>
                    </div>
                    {% endif %}

                    <span class="ad-meta-single hidden-xs">
                    {% if ad.published_at or ad.expires_at %}
                        Oglas je aktivan <b>{{ (ad.published_at ? 'od ' ~ date('d.m.Y.', ad.published_at) : '') }}</b><br/>
                    {% endif %}
                    Oglas prikazan: <b>{{ view_count }} puta</b><br>
                    Šifra oglasa: <b>{{ ad.id }}</b>

                    {% if can_favorite_ad or canSendMessage or ad_render['external_url_button'] %}
                        <div class="row action-btns">
                            {% if ad_render['external_url_button'] %}
                            <div class="col-sm-12 margin-top-10px">
                                <a href="{{ ad_render['external_url_button']['url'] }}" class="btn secondary-red width-full" rel="external nofollow" target="_blank">{{ ad_render['external_url_button']['label'] }}</a>
                            </div>
                            {% endif %}
                            {% if canSendMessage %}
                            <div class="col-sm-12 margin-top-10px">
                                <button class="width-full" data-toggle="modal" data-target="#modal-send-message">Pošaljite upit</button>
                            </div>
                            {% endif %}
                            {% if can_favorite_ad %}
                            {% set save_ad_link = auth.logged_in() ? 'oglas/makeFavorite/' ~ ad.id : 'user/signin?next=oglas/makeFavorite/' ~ ad.id -%}
                            <div class="col-sm-12 margin-top-10px">
                                {{- linkTo([save_ad_link, '<span class="fa fa-star"></span>&nbsp; Spremi u favorite', 'id':'btnAddToFavorites', 'class':'btn toggle-white-blue width-full']) }}
                            </div>
                            {% endif %}
                        </div>
                    {% endif %}
                    </span>

                </div>
            </div>
        </div>

        <div class="row margin-top-sm">
            <div class="col-md-12 oglas-details">
                <div class="margin-top-10px visible-xs hidden-sm hidden-md hidden-lg"></div>
                <div class="ad-details">
                    <h3 class="convert-to-tab-xs initial-open color-light-blue">Opširnije o oglasu<span class="caret hidden-sm hidden-md hidden-lg"></span></h3>
                    <p>{{ ad.description|stripsometags|nl2br }}</p>
                </div>

                <hr class="hidden-xs"/>

                {% for ad_section_parameters in ad_render['html']['other']|default([]) %}
                    {{ ad_section_parameters }}
                    <hr class="hidden-xs"/>
                {% endfor %}

                {% if ad_render['map'] is defined -%}
                <!-- hide this whole row in case map is not visible -->
                <div class="row margin-top-sm margin-bottom-sm">
                    <div class="col-xs-12">
                        {#
                        <div class="btn-group">
                            <a class="btn btn-default fl" href="#">Karta</a>
                            <a class="btn btn-default fl" href="#">Tlocrt</a>
                        </div>
                        #}
                        <div class="form-group checkbox checkbox-primary">
                            <input type="checkbox" id="show-near-by-on-map" value="on" data-classified-id="{{ ad.id }}">
                            <label for="show-near-by-on-map" class="color-light-blue">Prikaži ostale oglase u blizini</label>
                        </div>
                        <div
                                id="google_map_container"
                                data-zoom="{{ ad_render['map']['zoom']|default('8') }}"
                                data-lat="{{ ad_render['map']['lat']|default('0') }}"
                                data-lng="{{ ad_render['map']['lng']|default('0') }}"
                                style="height:420px;"
                        ><!--IE--></div>

                    </div>
                </div>
                <hr>
                {% endif -%}

                {% if can_favorite_ad or canSendMessage %}
                <div class="row action-btns visible-xs hidden-md hidden-lg">
                    {% if canSendMessage %}
                        <div class="col-sm-12 margin-bottom-1em">
                            <button class="width-full" data-toggle="modal" data-target="#modal-send-message">Pošaljite upit</button>
                        </div>
                    {% endif %}
                    {% if can_favorite_ad %}
                        {% set extra_classes = ' margin-top-1em margin-bottom-1em' %}
                        {% if not canSendMessage %}
                            {% set extra_classes = ' margin-top-0 margin-bottom-1em' %}
                        {% endif %}
                        {% set save_ad_link = auth.logged_in() ? 'oglas/makeFavorite/' ~ ad.id : 'user/signin?next=oglas/makeFavorite/' ~ ad.id -%}
                        <div class="col-sm-12{{ extra_classes }}">
                            {{- linkTo([save_ad_link, '<span class="fa fa-star"></span>&nbsp; Spremi u favorite', 'id':'btnAddToFavorites2', 'class':'btnAddToFavorites btn toggle-white-blue width-full']) }}
                        </div>
                    {% endif %}
                </div>
                {% endif %}

                {% if not ad.sold %}
                    <div class="oglas-details-share-buttons clearfix">
                        <button class="facebook" id="btnFBShare"><span class="fa fa-facebook-official"></span><span class="button-txt"> Facebook</span></button>
                        <button class="email" id="btnEmailShare"><span class="fa fa-envelope"></span><span class="button-txt"> Podijeli putem e-maila</span></button>
                        <a rel="nofollow" class="button btn btn-print print" href="{{ ad.get_frontend_view_link() ~ '?layout_type=print' }}"><span class="fa fa-print"></span><span class="button-txt"> Ispis</span></a>
                        <div class="more">
                            <button class="btn more padding-10px"><span class="fa fa-share-alt-square"></span></button>
                            <ul class="menu">
                                <li><a href="#" id="btnGooglePlusShare">Google+</a></li>
                                <li><a href="#" id="btnTwitterShare">Twitter</a></li>
                            </ul>
                        </div>
                    </div>

                    <span class="ad-meta-single ad-meta-single-bottom margin-top-1em visible-xs visible-sm hidden-md hidden-lg">
                    {% if ad.published_at or ad.expires_at %}
                        Oglas je aktivan <b>{{ (ad.published_at ? 'od ' ~ date('d.m.Y.', ad.published_at) : '') }}</b><br/>
                    {% endif %}
                        Oglas prikazan: <b>{{ view_count }} puta</b><br>
                        Šifra oglasa: <b>{{ ad.id }}</b>
                    </span>

                    <hr>

                    {% if can_report_ad %}
                        {% set report_link = auth.logged_in() ? 'oglas/report/' ~ ad.id : 'user/signin?next=oglas/report/' ~ ad.id -%}
                        Oglas krši pravila oglašavanja? {{ linkTo([report_link, 'Prijavite ga', 'id':'btnReportAd', 'class':'text-danger']) }}
                    {% endif %}
                {% endif %}
            </div>
        </div>

    </div>
    <div class="col-md-3 classified-view-side-col">
        {% if banners and banner_300x250 is defined %}
            <div class="banner sidebar-rectangle-holder margin-bottom-sm hidden-sm hidden-xs">{{ banner_300x250 }}</div>
        {% endif %}
        {% if banners and banner_300x600 is defined %}
            <div class="banner sidebar-rectangle-holder hidden-sm hidden-xs">{{ banner_300x600 }}</div>
        {% endif %}
    </div>
</div>

{% if banners and banner_728x90 is defined %}
<div class="row"><div class="banner w728 center banner-728x90 hidden-xs margin-top-sm">{{ banner_728x90 }}</div></div>
{% endif %}

{% if similar_ads is defined and similar_ads and similar_ads is iterable %}
<h2 class="section-title">Slični <span>oglasi</span></h2>
{{ partial('chunks/ads-grid', ['ads':similar_ads] )}}
{% endif %}

{% if canSendMessage %}
{{ partial('chunks/modal-message-user', ['receiver': ad_user, 'formAction':ad.get_frontend_view_link(), 'messageEntityField':'ad_id', 'messageEntityID':ad.id, 'showCaptcha': using_captcha]) }}
{% endif %}

{{ partial('chunks/modal-email-share', ['messageEntityField':'ad_id', 'messageEntityID':ad.id]) }}
