{# Contact form email tpl #}
<p>Pozdrav,<br /><b>{{ username }}</b> kaže:</p>

<p>{{ content|nl2br }}</p>

<p>Šalje: {{ email }}</p>
