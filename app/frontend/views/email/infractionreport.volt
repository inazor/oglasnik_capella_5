{# Infraction report form email tpl #}
<p>Pozdrav,<br />zaprimljena je prijava kršenja pravila Oglasnik.hr portala.</p>

<h3>Prijavljeni subjekt:</h3>
<p>
    ID: <strong>{{ entity_id }}</strong><br />
    Naziv: <strong><a href="{{ url.get(entity_url) }}" target="_blank">{{ entity_name }}</a></strong>
</p>

<h3>Podaci o osobi koja je izvršila prijavu</h3>
<p>
    Korisnik: <strong>{{ reporter_name }}</strong><br />
    Razlog prijave: <strong>{{ reporter_reason }}{% if reported_message %}</strong><br />
    Komentar: <br /><strong>{{ reported_message|nl2br }}</strong>{% endif %}
</p>
