{# Ad expired email tpl #}
<p>Poštovani {{ username }},<br><br>Vaš oglas <b><a href="{{ ad_link }}">{{ ad_title }}</a></b> u rubrici  „{{ ad_category_name }}“ je istekao.</p>
<p>Predlažemo vam da, ako još niste kupili, prodali, unajmlili, iznajmili ili poklonili ono što oglašavate, <b>ponovite</b> svoj oglas, a ako je potrebno i dodatno ga istaknete nekim od naših proizvoda. Ponovite svoj oglas <a href="{{ moj_kutak_expired_ads_link }}">ovdje</a>.</p>
<p>Za sva dodatna pitanja, obratite se našoj korisničkoj podršci na broj telefona: <a href="tel:+38516102885">01/6102-885</a> ili putem e-mail adrese <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr.</a></p>
<p>Vaš Oglasnik</p>
