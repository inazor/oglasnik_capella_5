{# OAuth Welcome email #}
<p>Pozdrav <b>{{ username }}</b>,</p>
<p>Zahvaljujemo Vam na registraciji na <a href="{{ url.get('') }}">www.oglasnik.hr</a>.</p>
<p>Postavke svog korisničkog računa možete provjeriti i izmijeniti <a href="{{ url.get('moj-kutak') }}">ovdje</a>.</p>

