{# Profile/Shop contact form email tpl #}
<p>Pozdrav,<br />primili ste poruku s portala Oglasnik.hr</p>

<p>
    Ime:<br/><strong>{{ name }}</strong><br />
{#
    Email:<br/><strong>{{ email }}</strong><br />
#}
    Poruka:<br/>
    <strong>{{ message|nl2br }}</strong>
</p>

{{ remark is defined and remark ? '<p>' ~  remark ~ '</p>' : '' }}
