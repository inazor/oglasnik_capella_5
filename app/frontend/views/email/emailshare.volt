{# Email share email tpl #}
<p>Poštovani,</p>

{% if type == 'ad' %}
<p>Korisnik {{ sender }} je s vama odlučio podijeliti sljedeći oglas:</p>
<p><a href="{{ ad.get_frontend_view_link() }}">{{ ad.title }}</a></p>
{% endif %}

<p>Srdačan pozdrav,<br>Oglasnik.hr</p>
