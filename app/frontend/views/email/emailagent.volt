{# Email agent email tpl #}
<p>Poštovani <b>{{ user.username }}</b>,</p>

<p>Šaljemo Vam nove oglase koje je vaš agent pronašao.</p>

<table border="0" width="100%" cellpadding="0" cellspacing="0" style="max-width:530px">
{% for ad in ads %}
    <tr height="140">
        <td width="140" height="140">
            <a href="{{ ad['url'] }}"><img width="140" height="140" src="{{ ad['thumb_pic'] }}" alt="{{ ad['title']|escape_attr }}"></a>
        </td>
        <td width="20" height="140" style="font-family:Arial,sans-serif;font-size:12px;color:#999">&nbsp;</td>
        <td width="*" height="140" valign="top">
            <h3><a href="{{ ad['url'] }}" style="font-family:Arial,sans-serif;font-size:20px;color:#009FE3;text-decoration:none">{{ ad['title']|escape_attr }}</a></h3>
            <p style="font-family:Arial,sans-serif;font-size:14px;color:#666">{{ ad['description'] }}</p>
        </td>
    </tr>
    <tr height="20">
        <td width="140" height="20" style="font-family:Arial,sans-serif;font-size:12px;color:#999">{{ ad['sort_date'] }}</td>
        <td width="20" height="20" style="font-family:Arial,sans-serif;font-size:12px;color:#999">&nbsp;</td>
        <td width="*" height="20" align="right" style="font-family:Arial,sans-serif;font-size:14px;color:#666;text-align:right;">
            {% if ad['price']['main'] > 0 %}
            {% if ad['price']['other'] is defined %}{{ ad['price']['other'] }} <abbr title="približno">~</abbr> {% endif %}
            &nbsp;<strong style="font-family:Arial,sans-serif;font-size:14px;color:#009FE3">{{ ad['price']['main'] }}</strong>
            {% endif %}
        </td>
    </tr>
    {% if loop.last == false %}
    <tr height="20"><td colspan="3" height="20" style="font-family:Arial,sans-serif;font-size:12px;color:#999">&nbsp;</td></tr>
    {% endif %}
{% endfor %}
</table>

<p>Srdačan pozdrav,<br>Oglasnik.hr</p>
