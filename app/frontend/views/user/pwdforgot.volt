{# User pwdforgot #}
<section>
    <div class="underline page-title">
        <h1 class="underline">{{ title }}</h1>
        <span>Nemaš korisnički račun? <a href="{{ this.url.get('user/signup') }}">Registriraj se &raquo;</a></span>
    </div>
    {{ flashSession.output() }}
    {% if show_form %}
    <p>Upišite svoju e-mail adresu i poslat ćemo Vam poruku s uputama kako izmijeniti svoju zaboravljenu lozinku.</p>
    {{ form(NULL, 'id' : 'frm_pwdforgot', 'method' : 'post') }}
        {{ hiddenField('_csrftoken') }}
        <div class="row">
            <div class="col-md-6 col-lg-6">
                {% set field = 'email' %}
                <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                    <label for="{{ field }}" class="control-label">
                        E-mail adresa
                        <abbr title="Obavezno polje">*</abbr>
                    </label>
                    {{ emailField([ field, 'placeholder' : 'E-mail adresa', 'class': 'form-control' ]) }}
                    {% if errors is defined and errors.filter(field) %}
                    <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                    {% endif %}
                </div>
            </div>
            <div class="col-md-6 col-lg-6">
                <div class="form-group">
                    <label class="control-label hidden-sm">&nbsp;</label><br class="hidden-sm" />
                    <button class="btn btn-primary" type="submit">Pošalji <span class="fa fa-caret-right fa-fw"></span></button>
                </div>
            </div>
        </div>
    {{ endForm() }}
    {% endif %}
</section>
