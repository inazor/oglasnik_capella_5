<div class="margin-top-md user-registration">
    <div class="row">
        <div class="col-lg-9">
            <div class="padding-20px bg-white border-radius">
                <h2 class="section-title text-left margin-top-0">Registracija <b>pravne osobe</b></h2>
                <p>Već imaš korisnički račun? <a href="{{ this.url.get('user/signin') }}">Prijavi se</a></p>

                {{ flashSession.output() }}

                {% if show_form %}
                {{ form(null, 'id':'frm_register', 'method':'post', 'autocomplete':'off', 'data-user-type':viewFile) }}
                    {{ hiddenField('_csrftoken') }}
                    {{ hiddenField('registration_type') }}

                    <div class="row">
                        <div class="col-sm-6">
                            {% set field = 'username' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label for="{{ field }}" class="control-label required">Korisničko ime<abbr title="Obavezno polje">*</abbr></label>
                                {{ textField([field, 'class':'form-control icon-field username', 'placeholder':'Korisničko ime', 'value':(_POST[field] is defined ? _POST[field] : '')]) }}
                                {% if errors is defined and errors.filter(field) %}<p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>{% endif %}
                            </div>
                        </div>
                        <div class="col-sm-6">
                            {% set field = 'email' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label for="{{ field }}" class="control-label required">E-mail adresa<abbr title="Obavezno polje">*</abbr></label>
                                {{ emailField([field, 'class':'form-control icon-field email', 'placeholder':'E-mail adresa', 'value':(_POST[field] is defined ? _POST[field] : '')]) }}
                                {% if errors is defined and errors.filter(field) %}<p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>{% endif %}
                            </div>
                        </div>
                        <div class="col-sm-6">
                            {% set field = 'password' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label for="{{ field }}" class="control-label required">Lozinka<abbr title="Obavezno polje">*</abbr></label>
                                {{ passwordField([field, 'class':'form-control icon-field password', 'placeholder':'Lozinka', 'value':(_POST[field] is defined ? _POST[field] : '')]) }}
                                {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                {% else %}
                                <p class="help-block">Minimalno 6 znakova</p>
                                {% endif %}
                            </div>
                        </div>
                        <div class="col-sm-6">
                            {% set field = 'repeatPassword' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label for="{{ field }}" class="control-label required">Ponovljena lozinka<abbr title="Obavezno polje">*</abbr></label>
                                {{ passwordField([field, 'class':'form-control icon-field password', 'placeholder':'Lozinka', 'value':(_POST[field] is defined ? _POST[field] : '')]) }}
                                {% if errors is defined and errors.filter(field) %}<p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>{% endif %}
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="required-company-field col-xs-12">
                            {% set field = 'company_name' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label for="{{ field }}" class="control-label required">Naziv poslovnog subjekta<abbr title="Obavezno polje">*</abbr></label>
                                {{ textField([field, 'class':'form-control', 'placeholder':'Naziv poslovnog subjekta', 'value':(_POST[field] is defined ? _POST[field] : '')]) }}
                                {% if errors is defined and errors.filter(field) %}<p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>{% endif %}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        {% set fields = ['oib':'OIB poslovnog subjekta', 'birth_date':'DD.MM.GGGG', 'first_name':'Ime odgovorne osobe', 'last_name':'Prezime odgovorne osobe'] %}
                        {% set field_length = ['oib':11, 'birth_date':10, 'first_name':64, 'last_name':64] %}
                        {% for field, label in fields %}
                        <div class="col-md-6">
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                {% if (field == 'birth_date') %}
                                <label for="{{ field }}" class="control-label required">Datum osnivanja<abbr title="Obavezno polje">*</abbr></label>
                                <input type="hidden" id="{{ field }}" data-minage="0" value="{{ (_POST[field] is defined ? _POST[field] : '') }}">
                                {% elseif (field == 'oib') %}
                                <label for="{{ field }}" class="control-label required">{{ label }}<abbr title="Obavezno polje">*</abbr></label>
                                {{ textField([field, 'class':'form-control', 'placeholder':label, 'data-mask':'00000000000', 'maxlength':field_length[field], 'value':(_POST[field] is defined ? _POST[field] : '')]) }}
                                {% else %}
                                <label for="{{ field }}" class="control-label required">{{ label }}<abbr title="Obavezno polje">*</abbr></label>
                                {{ textField([field, 'class':'form-control', 'placeholder':label, 'maxlength':field_length[field], 'value':(_POST[field] is defined ? _POST[field] : '')]) }}
                                {% endif %}
                                {% if errors is defined and errors.filter(field) %}<p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>{% endif %}
                            </div>
                        </div>
                        {% endfor %}
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="country_id" class="control-label required">Država<abbr title="Obavezno polje">*</abbr></label>
                                {{ country_dropdown }}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="county_id" class="control-label required">Županija<abbr title="Obavezno polje">*</abbr></label>
                                {{ county_dropdown }}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        {% set fields = ['city':'Grad', 'zip_code':'Poštanski broj', 'address':'Adresa'] %}
                        {% for field, label in fields %}
                        <div class="col-md-6">
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label for="{{ field }}" class="control-label required">{{ label }}<abbr title="Obavezno polje">*</abbr></label>
                                {{ textField([field, 'class':'form-control', 'placeholder':label, 'value':(_POST[field] is defined ? _POST[field] : '')]) }}
                                {% if errors is defined and errors.filter(field) %}<p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>{% endif %}
                            </div>
                        </div>
                        {% endfor %}
                    </div>

                    <div class="row">
                        {% set fields = ['phone1':'Telefon #1', 'phone2':'Telefon #2'] %}
                        {% for field, label in fields %}
                        <div class="col-md-6">
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label for="{{ field }}" class="control-label{{ field == 'phone1' ? ' required' : '' }}">{{ label ~ (field == 'phone1' ? '<abbr title="Obavezno polje">*</abbr>' : '') }}</label>
                                <div>
                                    {{ hiddenField([field, 'class':'form-control phone-number-handler', 'placeholder':label, 'value':(_POST[field] is defined ? _POST[field] : '')]) }}
                                    {% set chkfield = field ~ '_public' %}
                                    <div class="checkbox checkbox-primary">
                                        {{ checkField((_POST[chkfield] is not defined) or (_POST[chkfield] is defined and _POST[chkfield] == 'on') ? [chkfield, 'value':'on', 'checked':'checked'] : [chkfield, 'value':'on']) }}
                                        <label for="{{ chkfield }}">Želim prikazati broj telefona u oglasima koje objavim</label>
                                    </div>
                                </div>
                                {% if errors is defined and errors.filter(field) %}<p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>{% endif %}
                            </div>
                        </div>
                        {% endfor %}
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            {% set field = 'terms' %}
                            <div class="form-group checkbox checkbox-primary{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                {{ checkField(_POST[field] is defined and _POST[field] == 'on' ? [field, 'value':'on', 'checked':'checked'] : [field, 'value':'on']) }}
                                <label for="{{ field }}"{{ errors is defined and errors.filter(field) ? ' class="has-error"' : '' }}>
                                    Pročitao sam i slažem se sa <a href="{{ this.url.get('info/uvjeti-koristenja') }}" rel="external">uvjetima korištenja</a>
                                </label>
                                {% if errors is defined and errors.filter(field) %}<p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>{% endif %}
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="checkbox checkbox-primary">
                                {% set field = 'newsletter' %}
                                {{ checkField(((_POST[field] is not defined) or (_POST[field] is defined and _POST[field] == 'on')) ? [field, 'value':'on', 'checked':'checked'] : [field, 'value':'on']) }}
                                <label for="{{ field }}">Želim primati obavijesti o novostima putem e-maila</label>
                            </div>
                        </div>
                    </div>

                {{ endForm() }}
                {% endif %}
            </div>
            {% if show_form %}
            <button class="width-full light-blue margin-top-md margin-bottom-md" id="submitBtn">Završi registraciju</button>
            {% endif %}
        </div>

        <div class="col-lg-3 side-col">
            <h2>Želite li se registrirati kao fizička osoba?</h2>
            <p>Kako bi mogli predati oglas potrebno je napraviti registraciju. Fizička osoba ima mogućnost predaje malih besplatnih oglasa u većini rubrika kao i besplatnu predaju oglasa u tiskanom izdanju Oglasnika.</p>
            <a href="{{ url('user/signup') }}" class="btn width-full margin-top-md">Registracija fizičke osobe</a>
        </div>
    </div>
</div>
