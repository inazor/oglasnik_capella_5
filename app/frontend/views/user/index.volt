{# User index View #}
<section>
    <div class="underline page-title">
        <h1 class="underline">Ola {{ auth.get_user().username }}</h1>
    </div>
    <p class="muted">Kakav krasan dan!</p>
    <p><strong>Logins:</strong> {{ auth.get_user().logins }}</p>
    <p><strong>Zadnja prijava:</strong> {{ date('Y-m-d H:i:s', auth.get_user().last_login) }}</p>
    <p>{{ linkTo('user/signout', 'Odjava' ) }}</p>
</section>
