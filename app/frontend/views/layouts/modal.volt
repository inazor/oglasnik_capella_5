{% include 'chunks/head.volt' %}
<body class="modal-open">

<div class="modal fade modal-generic in" tabindex="-1" role="dialog" style="display:block;">
    <a class="modal-backdrop-logo" href="/"></a>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {{ content() }}
        </div>
    </div>
</div>

<div class="modal-backdrop fade in"></div>
</body></html>
