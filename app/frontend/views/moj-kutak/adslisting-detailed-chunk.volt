<div class="ad-box ad-box-wide" data-classified-id="{{ ad['id'] }}">
    {% if current_action == 'spremljeniOglasi' %}
    <a href="{{ ad['frontend_url'] }}"><h3 class="hidden-sm hidden-md hidden-lg">{{ ad['title']|escape }}</h3></a>
    {% else %}
    <h3 class="hidden-sm hidden-md hidden-lg">{{ ad['title']|escape }}</h3>
    {% endif %}

    <div class="checkbox-wrapper">
        <div class="image-wrapper">
            <input id="oglas-{{ ad['id'] }}" class="checkbox checkbox-light-blue" type="checkbox">
            <label for="oglas-{{ ad['id'] }}"></label>

            {% if current_action == 'spremljeniOglasi' %}
            <a href="{{ ad['frontend_url'] }}">{{ ad['thumb_pic'] }}</a>
            {% else %}
            {{ ad['thumb_pic'] }}
            {% endif %}
        </div>
    </div>
    <div class="info-wrapper">
        {% if current_action == 'spremljeniOglasi' %}
        <a href="{{ ad['frontend_url'] }}"><h3 class="hidden-xs">{{ ad['title']|escape }}</h3></a>
        {% else %}
        <h3 class="hidden-xs">{{ ad['title']|escape }}</h3>
        {% endif %}
        
        {% if current_action == 'spremljeniOglasi' %}
        {% set desc = ad['description_tpl']|default(ad['description'])|striptags|truncate(150)|nl2br -%}
        <p>{{ desc }}</p>
        {% endif %}
        <div class="meta clearfix">
            {% if current_action != 'spremljeniOglasi' %}
            <b>ID:</b> {{ ad['id'] }}<br/>
            <b>{{ ad['status'] == 0 ? 'Stvoren' : 'Predan' }}:</b> {{ ad['status'] == 0 ? ad['created_at'] : ad['published_at'] }}<br />
            {% endif %}
            {% if ad['status'] == 1 OR ad['status'] == 14 OR ad['status'] == 15 %}
            <b>Aktivan do:</b> {{ ad['expires_at'] }}<br />
            {% endif %}
            <b>Kategorija:</b> {{ ad['category_path'] }}<br>

            {% if current_action != 'spremljeniOglasi' %}
                {% if not (ad['online_product'] is empty) %}
                <b>Proizvod na webu:</b> {{ ad['online_product'] }}<br />
                {% endif %}
                {% if ad['offline_product'] %}<b>Naručeni proizvod u tisku:</b> {{ ad['offline_product'] }}<br />{% endif %}
            {% endif %}

            {% set ad_status_class = ad['status_class'] %}
            {% set ad_status_text = ad['status_text'] %}
            {% if current_action == 'spremljeniOglasi' %}
                {% if ad['active'] == '1' %}
                    {% set ad_status_class = 'success' %}
                    {% set ad_status_text = 'Aktivan' %}
                {% else %}
                    {% set ad_status_class = 'danger' %}
                    {% set ad_status_text = 'Neaktivan' %}
                    {% set show_link = false %}
                {% endif %}
            {% endif %}
            {% if ad_status_text %}
            <b>Status oglasa:</b> <span class="text-{{ ad_status_class }}">{{ ad_status_text }}</span><br />
            {% endif %}
            {% set ad_price = ad['price'] %}
            <div class="fr price" style="">
                {% if ad_price['other'] is defined %}<span class="price-euro">{{ ad_price['other'] }}</span><br>{% endif %}
                <span class="price-kn">{{ ad_price['main'] }}</span>
            </div>
        </div>
    </div>

    <div class="actions margin-top-sm width-full">
        {% if current_action == 'spremljeniOglasi' %}
        <button class="delete-btn" data-toggle="modal" data-target="#deleteModal" data-formurl="{{ url('moj-kutak/obrisi-spremljeni-oglas/' ~ ad['id']) }}"><span class="fa fa-trash"></span>&nbsp; Obriši</button>
        {% else %}
            {% for link in ad['links'] %}
            {{ link }}
            {% endfor %}
        {% endif %}
    </div>

    {% if ad['order_id'] is defined %}
        <div class="classified-payment-details" data-order-id="{{ ad['order_id'] }}" style="display:none;">
            {{ partial('chunks/hub-3a', ['order_id': ad['order_id'], 'payment_data_table_markup': ad['payment_data_table_markup']]) }}
            <div class="text-right">
                <span class="btn btn-sm btn-danger order-cancellation-btn margin-top-sm" data-toggle="modal" data-target="#orderCancellationModal" data-formurl="{{ url('moj-kutak/otkazi-narudzbu/' ~ ad['order_id']) }}">Otkaži narudžbu</span>
            </div>
        </div>
    {% endif %}
</div>
