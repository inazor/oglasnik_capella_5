            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingMojiOglasi">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#headingMojiOglasi" href="#mojiOglasi" aria-expanded="true" aria-controls="mojiOglasi">
                                Moji oglasi
                            </a>
                        </h4>
                    </div>
                    <div id="mojiOglasi" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingMojiOglasi">
                        <div class="nav">
                            {{ linkTo(['moj-kutak/svi-oglasi', 'Svi oglasi', 'class': 'list-group-item' ~ (current_action == 'sviOglasi' ? ' active' : '')]) }}
                            {{ linkTo(['moj-kutak/aktivni-oglasi', 'Aktivni oglasi', 'class': 'list-group-item' ~ (current_action == 'aktivniOglasi' ? ' active' : '')]) }}
                            {{ linkTo(['moj-kutak/istekli-oglasi', 'Istekli oglasi', 'class': 'list-group-item' ~ (current_action == 'istekliOglasi' ? ' active' : '')]) }}
                            {{ linkTo(['moj-kutak/deaktivirani-oglasi', 'Deaktivirani oglasi', 'class': 'list-group-item' ~ (current_action == 'deaktiviraniOglasi' ? ' active' : '')]) }}
                            {{ linkTo(['moj-kutak/neobjavljeni-oglasi', 'Neobjavljeni oglasi', 'class': 'list-group-item' ~ (current_action == 'neobjavljeniOglasi' ? ' active' : '')]) }}
                            {{ linkTo(['moj-kutak/podijeli-oglase', 'Podijeli oglase', 'class': 'list-group-item' ~ (current_action == 'podijeliOglase' ? ' active' : '')]) }}
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingMojiPodaci">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#headingMojiPodaci" href="#mojiPodaci" aria-expanded="true" aria-controls="mojiPodaci">
                                Moji podaci
                            </a>
                        </h4>
                    </div>
                    <div id="mojiPodaci" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingMojiPodaci">
                        <div class="nav">
                            {{ linkTo(['moj-kutak/spremljeni-oglasi', 'Spremljeni oglasi', 'class': 'list-group-item' ~ (current_action == 'spremljeniOglasi' ? ' active' : '')]) }}
                            {{ linkTo(['moj-kutak/podaci', 'Moji podaci', 'class': 'list-group-item' ~ (current_action == 'podaci' ? ' active' : '')]) }}
                            {{ linkTo(['moj-kutak/lozinka', 'Promjena lozinke', 'class': 'list-group-item' ~ (current_action == 'lozinka' ? ' active' : '')]) }}
                            {% set trgovina_selected = false %}
                            {% if current_action in ['trgovina','trgovinaIzlog'] %}{% set trgovina_selected = true %}{% endif %}
                            {{ linkTo(['moj-kutak/trgovina', 'Trgovina / izlozi', 'class': 'list-group-item' ~ (trgovina_selected ? ' active' : '')]) }}
                            {# comment out this... as requested, will be released with new design
                            {{ linkTo(['moj-kutak/email-agenti', 'Email agenti', 'class': 'list-group-item' ~ (current_action == 'emailAgenti' ? ' active' : '')]) }}
                            #}
                            {{ linkTo(['moj-kutak/obavijesti', 'Obavijesti', 'class': 'list-group-item' ~ (current_action == 'obavijesti' ? ' active' : '')]) }}
                            {# comment out this item for now... most probably it won't be implemented
                            {{ linkTo(['moj-kutak/narudzbe', 'Moje narudžbe', 'class': 'list-group-item' ~ (current_action == 'orders' ? ' active' : '')]) }}
                            #}
                        </div>
                    </div>
                </div>
            </div>
