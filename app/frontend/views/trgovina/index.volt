<h1 class="margin-top-0">Istaknuta trgovina/Izlog</h1>

<p>Nemojte biti kao svi ostali. Zgrabite priliku i odaberite najbolju poziciju za objavu oglasa - u Izlogu!</p>

<p>Istaknuta trgovina (Izlog) se nalazi iznad liste svih oglasa i omogućuje vam da pokažete više oglasa odjednom.
    Stavite do 4 oglasa i neka vaša online trgovina bude u prvom planu.</p>

<ul>
    <li>odaberite kategoriju i dvije podkategorije u kojoj želite prikazati vašu trgovinu</li>
    <li>ponudite korisnicima sve na jednom mjestu sa jedinstvenom web adresom: <a href="http://www.oglasnik.hr/trgovina/ImeVaseTrgovine">http://www.oglasnik.hr/trgovina/ImeVaseTrgovine</a></li>
    <li>neka vas korisnici zapamte: brendirajte se (objava logotipa, teksta o vama, kontakt podataka)</li>
</ul>
<br>
<p>Usudite se biti prvi - dogovorite jedinstvenu ponudu s našim prodajnim savjetnicima na e-mail <a href="mailto:prodaja@oglasnik.hr">prodaja@oglasnik.hr</a> ili na telefon: <a href="tel:+38516325577">01/6325-577</a></p>
