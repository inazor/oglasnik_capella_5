<!-- Login -->
<div class="modal fade modal-login" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="modal-login-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title section-title" id="modal-login-label">Prijava <b>korisnika</b></h2>
                <p class="text-center margin-top-sm">Nemate profil na Oglasnik.hr? <a href="#">Registriraj se</a></p>
            </div>
            
            <div class="modal-body">
                <form class="row" id="frm_register" method="post" autocomplete="off">
                    <div class="form-group col-md-12">
                        <!--label for="username" class="control-label">Korisničko ime<abbr title="Obavezno polje">*</abbr></label-->
                        <input type="text" id="username" name="username" value="" class="form-control" placeholder="Korisničko ime / email*">
                    </div>
                     <div class="form-group col-md-12">
                        <!--label for="password" class="control-label">Lozinka<abbr title="Obavezno polje">*</abbr></label-->
                        <input type="password" id="password" name="password" value="" class="form-control" placeholder="Lozinka*">
                    </div>
                    <div class="warning col-md-12 margin-bottom-15px"><b>Greška!</b> Korisničko ime i/ili lozinka nisu ispravni.</div>
                    
                    <div class="form-group checkbox checkbox-primary col-sm-6 margin-top-0 margin-bottom-0">
                        <input type="checkbox" id="remember-me" name="remember-me" value="on"><label for="remember-me">Zapamti me</label>
                    </div>
                    <div class="col-sm-6 text-right-sm">
                        <a href="#">Zaboravili ste lozinku?</a>
                    </div>

                    <div class="col-md-12 margin-top-lg">
                        <button type="button" class="width-full modal-btn-send light-blue" data-dismiss="modal">Prijavi se</button>
                    </div>

                    <div class="col-xs-6"><button class="width-full margin-top-sm facebook">Facebook<span class="hidden-xs"> prijava</span></button></div>
                    <div class="col-xs-6"><button class="width-full margin-top-sm google-plus">Google+<span class="hidden-xs"> prijava</span></button></div>

                    <div class="col-xs-12 margin-top-md">
                        By signing up, you agree to Oglasni.hr Terms of Use and Privacy Policy.
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
