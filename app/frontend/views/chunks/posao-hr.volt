<section class="background-dark-blue homepage-posao-hr-promo margin-top-sm">
    <div class="container">
        <a class="posao-hr-logo" target="_blank" href="{{ posaoHr.get('footer_link') }}"><img src="{{ url.get('/assets/img/posao.hr-logo.png') }}" width="175" height="37" alt="posao.hr"></a>
        <h2 class="section-title text-center margin-bottom-0" style="border-top:0;">{{ posaoHr.get('title') }}</h2>
        {% set posaoHrText = posaoHr.get('text') %}
        {% if posaoHrText is defined and posaoHrText %}<p class="intro-text margin-top-0">{{ posaoHrText }}</p>{% endif %}
        <div class="row">
            {% for classified in classifieds %}
            <div class="col-md-3 col-sm-4 col-xs-12">
                <a target="_blank" href="{{ classified.link }}" class="ad-box-white">
                    <span class="logo">
                        <img src="{{ classified.image }}">
                    </span>
                    <span class="title">{{ classified.title }}</span>
                    <span class="meta">{{ classified.location }}{{ classified.employer ? ', <b class="emp">' ~ classified.employer ~ '</b>' : ''}}</span>
                </a>
            </div>
            {% endfor %}
        </div>
        {% set posaoHrFooter = posaoHr.get('footer') %}
        {% if posaoHrFooter %}<p class="text-center margin-0">{{ posaoHrFooter }}</p>{% endif %}
    </div>
</section>
