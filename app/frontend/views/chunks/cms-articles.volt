<div class="row{{ rowClass|default('') ? ' ' ~ rowClass : ''}}">
{% for article in articles %}
    {% include 'chunks/cms-articles-single.volt' %}
    {% if loop.index % 2 == 0 %}<div class="clearfix visible-sm-block"></div>{% endif %}
{% endfor %}
</div>
