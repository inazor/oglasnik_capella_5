<p>Poštovani</p>
<p>
    molimo Vas da nadopunite svoje identifikacijske podatke koji nedostaju.<br/>
    Temeljem Članka 7. Zakona o zabrani i sprječavanju obavljanja neregistrirane
    djelatnosti nije nam dopušteno objavljivanje oglasa ukoliko nam oglašivač
    ne dostavi zakonom propisane podatke.
</p>
<p>If you're not a resident of Croatia, please contact <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a></p>
