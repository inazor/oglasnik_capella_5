{% if ads is defined and ads is iterable and ads|length %}
    <div class="row grid-layout">
        {% set currentIndex = 1 %}
        {% for ad in ads %}
            {% include 'chunks/ads-map-single.volt' %}
            {% if currentIndex % 4 == 0 %}<div class="clearfix"></div>{% endif %}
            {% set currentIndex = currentIndex + 1 %}
        {% endfor %}
    </div>
    {% if pagination_links is defined and pagination_links %}{{ pagination_links }}{% endif %}
{% else %}
    {{ flashSession.output() }}
{% endif %}
