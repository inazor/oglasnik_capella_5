<div class="container">
    <h1 class="h2-like section-title">{{ 'Pretpregled oglasa'|category_name_markup }}</h1>
    <div class="text-center">{{ back_markup }}</div>
    <hr/>

    <div class="row">
        <div class="col-md-12">
            <h1 class="h2-like no-top-margin">{{ ad.title|striptags }}</h1>
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-8 margin-bottom-md">
                    <div id="galerija" class="gallery">
                        {% if ((ad_render['ad_media']|length > 0) or (ad_render['360'] is defined)) -%}
                        <div class="big">
                            <div id="blueimp-image-carousel" class="blueimp-gallery blueimp-gallery-carousel blueimp-gallery-controls">
                                <div class="slides"></div>
                                <a class="prev">‹</a>
                                <a class="next">›</a>
                            </div>
                        </div>
                        <div id="blueimp-fullscreen-gallery" class="blueimp-gallery blueimp-gallery-controls">
                            <div class="slides"></div>
                            <h3 class="title"></h3>
                            <a class="prev">‹</a>
                            <a class="next">›</a>
                            <a class="close"></a>
                            <a class="play-pause"></a>
                            <ol class="indicator"></ol>
                        </div>
                        <ul class="thumbnails">
                            {# 360 should come first, but still not sure how it should behave, expect problems, because noone thinks ahead... #}
                            {% if ad_render['360'] is defined -%}
                                {% for ad_render_360 in ad_render['360'] %}
                                    {% if ad_render_360['spinid_url'] is defined %}
                                        <li data-type="spin/360" class="spin360" data-carousel-url="{{ ad_render_360['spinid_url'] }}" data-fullscreen-url="{{ ad_render_360['spinid_url'] }}"><img src="{{ ad_render_360['spinid_thumb_url'] }}" alt=""></li>
                                    {% else %}
                                        <li data-type="spingw/360" class="spin360 spin360gw" data-carousel-url="{{ ad_render_360['spinid'] }}" data-fullscreen-url="{{ ad_render_360['spinid'] }}">
                                            <img src="{{ ad_render_360['spinid_thumb_url'] }}" alt="">
                                            <div id="gw-element-{{ ad_render_360['spinid'] }}" data-gw="true" data-gw-id="{{ ad_render_360['spinid'] }}"></div>
                                        </li>
                                    {% endif %}
                                {% endfor %}
                            {% endif -%}
                            {% if (ad_render['ad_media']|length) %}
                                {% for ad_media in ad_render['ad_media'] -%}
                                    {% set carousel_thumb = ad_media.get_thumb('GalleryBig', false, ad.category_id) %}
                                    {% set carousel_orientation = carousel_thumb.getOrientation() %}
                                    {% set carousel_class = carousel_thumb.getClass() %}
                                    {%- set ad_media_carousel = carousel_thumb.getSrc() -%}
                                    {%- set ad_media_fullscreen = ad_media.get_url('GalleryFullscreen', false, ad.category_id) -%}
                                    {%- set ad_media_thumbnail = ad_media.get_url('GalleryThumb', false, ad.category_id) -%}
                                    <li data-class="{{ carousel_class }}" data-carousel-url="{{ ad_media_carousel }}" data-fullscreen-url="{{ ad_media_fullscreen }}"><img data-width="{{ carousel_thumb.getWidth() }}" data-height="{{ carousel_thumb.getHeight() }}" data-orient="{{ carousel_orientation }}" class="{{ carousel_orientation }}" src="{{ ad_media_thumbnail }}" alt=""></li>
                                {% endfor -%}
                            {% endif %}
                        </ul>
                        {% else %}
                        {% set category_fallback = Models_Categories__getFallbackImage(ad.category_id) %}
                        {% set thumb = Models_Media__getNoImageThumb('GalleryBig', null, category_fallback) %}
                        <div class="big {{ thumb.getClass() }}" style="padding-bottom:91.228070175%; background:#fff url({{ thumb.getSrc() }}) center center / cover no-repeat"></div>
                        {% endif -%}
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-4 xno-padding-left-lg">
                    <div class="top-details">
                        {% set heading_extra_class = '' %}

                        {% if ad_user is defined and ad_user %}
                            {% set ad_user_shop = ad_user.shop %}
                            {% set avatar = ad_user.getAvatar().getSrc() %}
                            <div>
                                <span class="border-radius overflow-hidden"><img class="avatar-header" src="{{ avatar }}" width="30" height="30" /></span>
                                {% set users_username = (ad_user.type == 1 ? ad_user.username : ad_user.company_name) %}
                                {% if ad_user_shop %}{% set users_username = ad_user_shop.title %}{% endif %}
                                {{ users_username }} <small><a href="{{ ad_user.ads_page_url }}">(Prikaži sve oglase)</a></small>
                            </div>
                        {% else %}
                            {% set heading_extra_class = ' no-top-margin' %}
                        {% endif %}

                        {# TODO: implement location based on given address #}
                        {% set adLocationData = ad.getLocationData() %}
                        {% if adLocationData %}
                            <p><img src="{{ url('assets/img/icn_map.png') }}" alt="Lokacija">&nbsp; {{ adLocationData['text'] ~ (adLocationData['gps']|default(null) ? ' <small><a href="#" class="scrollToMap">(Pogledaj na karti)</a></small>' : '')}}</p>
                        {% endif %}

                        {% set adPhoneNumbers = ad.getFormattedPublicPhoneNumbers() %}
                        {% if adPhoneNumbers|length %}
                        <p>
                            <img src="{{ url('assets/img/icn_telephone.png') }}" alt="Telefon">&nbsp;
                            {% for adPhoneNumber in adPhoneNumbers %}{% if adPhoneNumber['link'] is defined %}<a href="tel:{{ adPhoneNumber['link'] }}">{{ adPhoneNumber['text'] }}</a>{% else %}{{ adPhoneNumber['text'] }}{% endif %}{{ !loop.last ? ', ' : '' }}{% endfor %}
                        </p>
                        {% endif %}
                        <hr>
                        {% set ad_price = ad.getPrices() -%}
                        {% if ad_price %}
                        {% if ad_price['main'] > 0 -%}
                        <span class="price-oglas-details">cijena: {{ ad_price['main'] }}</span>
                        {% if ad_price['other'] %}<br>približno: {{ ad_price['other'] }}{% endif %}
                        <hr>
                        {% endif %}
                        {%- endif %}
                        {% if ad.published_at or ad.expires_at %}
                            Oglas je aktivan <b>{{ (ad.published_at ? 'od ' ~ date('d.m.Y.', ad.published_at) : '') }}</b><br/>
                        {% endif %}
                        Šifra oglasa: <b>{{ ad.id }}</b>

                        <div class="row action-btns">
                            <div class="col-sm-12 margin-top-10px">
                                <button class="width-full" disabled>Pošaljite upit</button>
                            </div>
                            <div class="col-sm-12 margin-top-10px">
                                <button class="toggle-white-blue width-full" disabled><span class="fa fa-star"></span>&nbsp; Spremi u favorite</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {% if ad_render['map'] is defined -%}
            <!-- hide this whole row in case map is not visible -->
            <div class="row margin-top-md">
                <div class="col-xs-12">
                    <!--div class="btn-group">
                        <a class="btn btn-default fl" href="#">Karta</a>
                        <a class="btn btn-default fl" href="#">Tlocrt</a>
                    </div-->
                    <div class="form-group checkbox checkbox-primary">
                        <input type="checkbox" id="show-other" name="show-other" value="on">
                        <label for="show-other">Prikaži ostale oglase u blizini</label>
                    </div>
                    <div
                        id="google_map_container"
                        data-zoom="{{ ad_render['map']['zoom']|default('8') }}"
                        data-lat="{{ ad_render['map']['lat']|default('0') }}"
                        data-lng="{{ ad_render['map']['lng']|default('0') }}"
                        style="height:420px;"
                    ><!--IE--></div>
                </div>
            </div>
            {% endif -%}

            <div class="row">
                <div class="col-xs-12 oglas-details">
                    <h3 class="convert-to-tab-xs">Opširnije o oglasu<span class="caret hidden-sm hidden-md hidden-lg active"></span></h3>
                    <p>{{ ad.description|stripsometags|nl2br }}</p>

                    <hr>

                    {% for ad_section_parameters in ad_render['html']['other']|default([]) %}
                        {{ ad_section_parameters }}
                        <hr>
                    {% endfor %}
                </div>
            </div>
        </div>
        <div class="col-md-3">
            {% if banners and banner_300x250 is defined %}
                <div class="banner sidebar-rectangle-holder margin-bottom-sm hidden-sm hidden-xs">{{ banner_300x250 }}</div>
            {% endif %}
            {% if banners and banner_300x600 is defined %}
                <div class="banner sidebar-rectangle-holder hidden-sm hidden-xs">{{ banner_300x600 }}</div>
            {% endif %}
        </div>
    </div>
</div>
