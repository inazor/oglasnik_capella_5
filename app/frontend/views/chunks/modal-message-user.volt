{% set senderName = '' %}
{% set senderEmail = '' %}

{# determine if catpcha is used, with fallback for older code that doesn't pass that variable yet #}
{% if not(showCaptcha is defined) %}
    {% set showCaptcha = true %}
    {# old logic, do not show captcha to logged in users -- TODO/FIXME: move shit like this into controller actions #}
    {% if auth.logged_in() %}
        {% set showCaptcha = false %}
    {% endif %}
{% endif %}

{% if auth.logged_in() %}
    {% set sender = auth.get_user() %}
    {% set senderName = sender.getDisplayName() %}
    {% set senderEmail = sender.email %}
{% endif %}

<div class="modal fade modal-send-message" id="modal-send-message" tabindex="-1" role="dialog" aria-labelledby="modal-send-message-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {{ form((formAction ? formAction : NULL), 'id':'frm_modal_contact', 'method':'post', 'autocomplete':'off', 'class':'ajax-form') }}

                {{ hiddenField('_csrftoken') }}

                {{ hiddenField([messageEntityField, 'value':messageEntityID]) }}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modal-send-message-label">Online upit</h4>
                </div>
                <div class="destination">
                    Primatelj<br>
                    {% if type|default('user') == 'shop' %}
                        {% set receiverName = receiver.getName() %}
                    {% else %}
                        {% set receiverName = (receiver.type == 1 ? receiver.username : receiver.company_name) %}
                    {% endif %}

                    {% set avatar = receiver.getAvatar().src %}
                    <div class="border-radius overflow-hidden display-inline-block vertical-align-middle"><img src="{{ avatar }}" alt="{{ receiverName }}" class="avatar-header" height="30" width="30" /></div>
                    {{ receiverName }}
                </div>
                <div class="alert text-center hidden"></div>
                <div class="modal-body">
                    {% set field = 'name' %}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label for="{{ field }}" class="control-label">Ime i prezime<abbr title="Obavezno polje">*</abbr></label>
                        {{- textField([field, 'class':'form-control', 'placeholder':'Ime i prezime', 'value':senderName]) }}

                        <p class="help-block">{{ errors is defined and errors.filter(field) ? current(errors.filter(field)).getMessage() : '' }}</p>
                    </div>

                    {% set field = 'email' %}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label for="{{ field }}" class="control-label">E-mail adresa<abbr title="Obavezno polje">*</abbr></label>
                        {{- textField([field, 'class':'form-control', 'placeholder':'Email adresa', 'value':senderEmail]) }}

                        <p class="help-block">{{ errors is defined and errors.filter(field) ? current(errors.filter(field)).getMessage() : '' }}</p>
                    </div>

                    {% set field = 'message' %}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label for="{{ field }}" class="control-label">Poruka<abbr title="Obavezno polje">*</abbr></label>
                        {{ textArea([field, 'class':'form-control', 'placeholder':'Vaša poruka', 'rows':'8', 'value':'']) }}

                        <p class="help-block">{{ errors is defined and errors.filter(field) ? current(errors.filter(field)).getMessage() : '' }}</p>
                    </div>

                    {% if showCaptcha and not(recaptcha_sitekey is empty) -%}
                    {%- set field = 'captcha' -%}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <div id="{{ field }}" class="g-recaptcha" data-sitekey="{{ recaptcha_sitekey }}"></div>
                        <p class="help-block">{{ errors is defined and errors.filter(field) ? current(errors.filter(field)).getMessage() : '' }}</p>
                    </div>
                    {%- endif -%}

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary width-full modal-btn-send text-center light-blue">Pošalji poruku</button>
                </div>
                {{ endForm() }}

        </div>
    </div>
</div>
