{# oib collection/interstitial form #}
<div class="pad-xs-5-only-lr">

{% include('chunks/ads-submission-title-bit') %}
{% include('chunks/ads-submission-missing-reason') %}

{{ form(NULL, 'id' : 'frm_oib', 'method', 'post', 'autocomplete' : 'off') }}
{{ hiddenField('_csrftoken') }}
<div class="row">
    {% set field = 'oib' %}
    <div class="col-lg-12 col-md-12 form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
        <label for="{{ field }}" class="control-label">
            OIB
            <abbr title="Obavezno polje">*</abbr>
        </label>
        {{ textField([field, 'class': 'form-control', 'placeholder': 'OIB', 'maxlength': 11, 'data-mask': '00000000000' ]) }}
        {% if errors is defined and errors.filter(field) %}
            <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
        {% endif %}
        <br>
        <button class="btn btn-primary" type="submit" name="save">Pošalji</button>
    </div>
</div>
{{ endForm() }}

</div>
