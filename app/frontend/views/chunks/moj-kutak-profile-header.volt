<div class="page-header page-header-user-dashboard xs-margin-offset-minus-10px">
    <div class="position-relative fixed-height">
        {% set profile_bg = auth.get_user().getProfileBg() %}
        <div class="profile-picture overflow-hidden {{ profile_bg.getClass() }}" style="background-image:url({{ profile_bg.getSrc() }});"></div>
        <div class="container">
            {% include 'moj-kutak/avatar-coverbg-form.volt' %}
            <a class="izmjeni-sliku" href="{{ this.url.get('moj-kutak/coverbg') }}" data-field="coverbg">Izmjeni svoju cover sliku</a>
            {% set avatar = auth.get_user().getAvatar() %}
            <div class="border-radius avatar-holder overflow-hidden">
                <a title="Izmjeni svoj avatar" href="{{ this.url.get('moj-kutak/avatar') }}" data-field="avatar">
                    {{ avatar }}<span class="edit-photo"></span>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="user-cp-nav xs-margin-offset-minus-10px">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-offset-3 user-header-nav-wrapper">
                <ul>
                    <li{{ current_action_group == 'profil' ? ' class="active"' : '' }}>
                        {{ current_action_group == 'profil' ? '<span>Postavke profila</span>' : linkTo(['moj-kutak/podaci', 'Postavke profila']) }}
                        <ul>
                            <li{{ (current_action == 'podaci' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/podaci', 'Osobni podaci']) }}</li>
                            <li{{ (current_action == 'lozinka' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/lozinka', 'Promjena lozinke']) }}</li>
                            <li{{ (current_action == 'trgovina' or current_action == 'trgovinaIzlog' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/trgovina', 'Trgovina / Izlozi']) }}</li>
                            <li{{ (current_action == 'avatar' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/avatar', 'Promjena avatara']) }}</li>
                            <li{{ (current_action == 'coverbg' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/coverbg', 'Promjena cover slike']) }}</li>
                        </ul>
                    </li>

                    <li{{ current_action_group == 'mojiOglasi' ? ' class="active"' : '' }}>
                        {{ current_action_group == 'mojiOglasi' ? '<span>Moji oglasi</span>' : linkTo(['moj-kutak/svi-oglasi', 'Moji oglasi']) }}
                        <ul>
                            <li{{ (current_action == 'sviOglasi' ? ' class="active"' : '') }}>{% if cumulativeAdsCounters is defined %}<span class="fr">({{ cumulativeAdsCounters['total'] }})</span> {% endif %}{{ linkTo(['moj-kutak/svi-oglasi', 'Svi oglasi']) }}<div class="clearfix"><!--IE--></div></li>
                            <li{{ (current_action == 'aktivniOglasi' ? ' class="active"' : '') }}>{% if cumulativeAdsCounters is defined %}<span class="fr">({{ cumulativeAdsCounters['total_active'] }})</span> {% endif %}{{ linkTo(['moj-kutak/aktivni-oglasi', 'Aktivni oglasi']) }}<div class="clearfix"><!--IE--></div></li>
                            <li{{ (current_action == 'istekliOglasi' ? ' class="active"' : '') }}>{% if cumulativeAdsCounters is defined %}<span class="fr">({{ cumulativeAdsCounters['total_expired'] }}) </span>{% endif %}{{ linkTo(['moj-kutak/istekli-oglasi', 'Istekli oglasi']) }}<div class="clearfix"><!--IE--></div></li>
                            <li{{ (current_action == 'deaktiviraniOglasi' ? ' class="active"' : '') }}>{% if cumulativeAdsCounters is defined %}<span class="fr">({{ cumulativeAdsCounters['total_deactivated'] }})</span> {% endif %}{{ linkTo(['moj-kutak/deaktivirani-oglasi', 'Deaktivirani oglasi']) }}<div class="clearfix"><!--IE--></div></li>
                            <li{{ (current_action == 'neobjavljeniOglasi' ? ' class="active"' : '') }}>{% if cumulativeAdsCounters is defined %}<span class="fr">({{ cumulativeAdsCounters['total_unpublished'] }})</span> {% endif %}{{ linkTo(['moj-kutak/neobjavljeni-oglasi', 'Neobjavljeni oglasi']) }}<div class="clearfix"><!--IE--></div></li>
                            <li{{ (current_action == 'prodaniOglasi' ? ' class="active"' : '') }}>{% if cumulativeAdsCounters is defined %}<span class="fr">({{ cumulativeAdsCounters['total_sold'] }})</span> {% endif %}{{ linkTo(['moj-kutak/prodani-oglasi', 'Prodani oglasi']) }}<div class="clearfix"><!--IE--></div></li>
                            <li{{ (current_action == 'podijeliOglase' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/podijeli-oglase', 'Podijeli oglase']) }}</li>
                        </ul>
                    </li>

                    <li{{ current_action == 'spremljeniOglasi' ? ' class="active"' : '' }}>
                        {{ current_action == 'spremljeniOglasi' ? '<span>Spremljeni oglasi</span>' : linkTo(['moj-kutak/spremljeni-oglasi', 'Spremljeni oglasi']) }}
                    </li>

                    <li{{ current_action == 'poruke' ? ' class="active"' : '' }}>
                        {% set notificationSpan = '' %}
                        {% if userUnreadMsgCount is defined and userUnreadMsgCount > 0 %}
                            {% set notificationSpan = '<span class="fr notification-count"> ' ~ userUnreadMsgCount ~ '</span>' %}
                        {% endif %}
                        {{ current_action == 'poruke' ? '<span>Poruke' ~ notificationSpan ~ '</span>' : linkTo(['moj-kutak/poruke', 'Poruke' ~ notificationSpan]) }}
                    </li>

{#  TODO: do we really need this here? exactly here!?
                    <li class="hidden-sm hidden-md hidden-lg"><a href="#"><button class="btn-danger width-full margin-top-sm margin-bottom-sm"> Deaktiviraj profil</button></a></li>
#}
                </ul>
                <div class="clearfix"><!--IE--></div>
            </div>
        </div>
    </div>
</div>

<div class="container user-cp xs-margin-offset-minus-10px">
    <div class="row">
