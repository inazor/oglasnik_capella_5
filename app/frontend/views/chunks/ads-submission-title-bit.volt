{% if stepType == 'new' %}
    {% set h1_title = 'Predaja novog oglasa' %}
{% elseif stepType == 'republish' %}
    {% set h1_title = 'Ponavljanje oglasa: <span class="blue">' ~ ad.title|striptags|truncate(80) ~ ' <small>[šifra: ' ~ ad.id ~ ']</small></span>' %}
{% elseif stepType == 'upgrade' %}
    {% set h1_title = 'Izdvajanje oglasa: <span class="blue">' ~ ad.title|striptags|truncate(80) ~ ' <small>[šifra: ' ~ ad.id ~ ']</small></span>' %}
{% endif %}

{% if h1_title|default('') %}<h1>{{ h1_title }}</h1>{% endif %}

{{ flashSession.output() }}
