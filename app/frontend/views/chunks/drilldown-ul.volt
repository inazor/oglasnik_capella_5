<ul{{ class ? ' class="' ~ class ~ '"' : ''}}>
{{ partial('chunks/drilldown-li', ['items': items, 'mark_paid_categories': (mark_paid_categories ? mark_paid_categories : false)]) }}
</ul>
