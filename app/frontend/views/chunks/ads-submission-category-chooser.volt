{# Frontend Ad Submit Category Chooser View #}

<div class="new-ad pad-xs-5-only-lr">

    {% include('chunks/ads-submission-title-bit') %}

    {{ steps_wizard_markup }}

    <div class="row margin-top-sm">
        <div class="col-xs-12 col-sm-8 col-md-9">
            <h2 class="choose-category-heading">Odabir kategorije</h2>
        </div>
{#
        <div class="col-xs-12 col-sm-4 col-md-3">
            <input class="cat-search form-control shadow margin-bottom-lg" type="text" placeholder="Upiši ime kategorije...">
        </div>
#}
    </div>
    {% if categories is defined %}
    {{ form(NULL, 'id':'frm_ads_submit', 'method':'post', 'autocomplete':'off') }}
        {{ hiddenField('_csrftoken') }}
        {{ hiddenField(['category_id', 'value':category_id]) }}
        <div class="new-ad-steps bs-row-margins-xs-only">
            <div class="submit-button-container margin-bottom-md" id="submit-button-container"{{ category_id ? ' style="display:block"' : '' }}>
                <button type="submit" name="next" value="ad_form" class="btn btn-primary width-full">Predaj oglas u ovu kategoriju</button>
            </div>
            <div class="mobile-category-selector-breadcrumbs hidden-sm hidden-md hidden-lg">
                <div class="row margin-0">
                    <div class="col-xs-12 bc-container"></div>
                </div>
            </div>
            <div class="category-selector-container" data-current-level="{{ current_category_level }}">
                <div class="mobile-overflow-hidden">
                    {{ partial('chunks/nested-ul', ['items': categories, 'class': 'category-select unstyled border-right', 'mark_paid_categories': true, 'level': 1, 'parent_category_id':null]) }}
                </div>
            </div>
        </div>
        <div class="visible-xs visible-sm clearfix"><!--IE--></div>
        <p class="text-small margin-top-md"><span class="text-danger">*</span> Oglašavanje u određenim kategorijama se naplaćuje. Cjenik oglašavanja pogledajte <a href="{{ url('info/cjenik') }}">ovdje</a>.</p>
    {{ endform() }}
    {% endif %}
</div>
