<div class="popular clearfix">
    <h3>{{ title|default('Popluarne vijesti') }}</h3>
    {% for article in articles %}
    <a class="post" href="{{ article['frontend_url'] }}">
        <div class="image-wrapper border-radius overflow-hidden fl">
            {% set thumb = article['thumb'] %}
            {{ thumb.setAlt(article['title']|striptags|truncate(80)|escape_attr).setHeight(70).setWidth(70).getTag() }}
        </div>
        <b>{{ article['title'] }}</b>
    </a>
    {% endfor %}
</div>
