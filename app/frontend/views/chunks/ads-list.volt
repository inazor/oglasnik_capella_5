{% if enableUpSells is not defined %}
    {% set enableUpSells = true %}
{% endif %}
<div class="category-fullrow-layout">
    {% if specialProductsArray is defined and specialProductsArray %}
    {% if carouselNeeded %}<div id="paid-ads-carousel">{% endif %}
        {% set ad_is_pushable = false %}
        {% for ad in specialProductsArray %}
            {% set thisListingHeaderDesc = ad['listing_heading_desc'] %}
            {% if !lastListingHeaderText %}
                {% set lastListingHeaderText = ad['listing_heading_text'] %}
                {% set lastListingHeaderClass = ad['listing_heading_class'] %}
                {% set showListingHeaderText = true %}
            {% else %}
                {% if lastListingHeaderText != ad['listing_heading_text'] %}
                    {% set lastListingHeaderText = ad['listing_heading_text'] %}
                    {% set lastListingHeaderClass = ad['listing_heading_class'] %}
                    {% set showListingHeaderText = true %}
                {% else %}
                    {% set showListingHeaderText = false%}
                {% endif %}
            {% endif %}

            {% if showListingHeaderText %}
                <div class="ad-box-title ad-box-heading no-checkbox category-listing no-hover{{ lastListingHeaderClass ? lastListingHeaderClass : '' }}">
                    <span class="margin-0 default-typeface">{{ lastListingHeaderText }}</span>
                    {% if not (thisListingHeaderDesc is empty) %}
                    <span class="ad-box-title-desc">{{ thisListingHeaderDesc  }}</span>
                    {% endif %}
                </div>
            {% endif %}

            {% include 'chunks/ads-list-single.volt' %}
        {% endfor %}
        {% for dummyAd in dummyAds %}{{ dummyAd }}{% endfor %}
    {% if carouselNeeded %}</div>{% endif %}
    {% endif %}

    {% for ad in ads %}
        {% set ad_is_pushable = false %}
        {% if enableUpSells %}
            {% if ad['isPushable'] is defined %}{% set ad_is_pushable = ad['isPushable'] %}{% endif %}
        {% endif %}
        {% set thisListingHeaderDesc = ad['listing_heading_desc'] %}
        {% if !lastListingHeaderText %}
            {% set lastListingHeaderText = ad['listing_heading_text'] %}
            {% set lastListingHeaderClass = ad['listing_heading_class'] %}
            {% set showListingHeaderText = true %}
        {% else %}
            {% if lastListingHeaderText != ad['listing_heading_text'] %}
                {% set lastListingHeaderText = ad['listing_heading_text'] %}
                {% set lastListingHeaderClass = ad['listing_heading_class'] %}
                {% set showListingHeaderText = true %}
            {% else %}
                {% set showListingHeaderText = false %}
            {% endif %}
        {% endif %}

        {% if showListingHeaderText %}
            {# in case the ad is pushable, but is first in the current section, it really should not be pushed :) #}
            {% if ad_is_pushable %}{% set ad_is_pushable = false %}{% endif %}

            <div class="ad-box-title ad-box-heading no-checkbox category-listing no-hover{{ lastListingHeaderClass ? lastListingHeaderClass : '' }}">
                <span class="margin-0 default-typeface">{{ lastListingHeaderText }}</span>
                {% if not (thisListingHeaderDesc is empty) %}
                <span class="ad-box-title-desc">{{ thisListingHeaderDesc  }}</span>
                {% endif %}
            </div>
        {% else %}
            {% if is_first_page is defined and is_first_page and ad_is_pushable and loop.index == 1 %}{% set ad_is_pushable = false %}{% endif %}
        {% endif %}

        {% include 'chunks/ads-list-single.volt' %}

        {% if banners is defined and banners %}
            {% if loop.index == 7 or loop.index == 14 %}
                {% if loop.index == 7 and banner_listing_7 is defined %}
                    <div class="banner hidden-xs hidden-sm hidden-md hidden-lg text-center">{{ banner_listing_7 }}</div>
                {% endif %}
                {% if loop.index == 14 and banner_listing_14 is defined %}
                    <div class="banner hidden-xs hidden-sm hidden-md hidden-lg text-center">{{ banner_listing_14 }}</div>
                {% endif %}
            {% endif %}
        {% endif %}
    {% endfor %}
</div>
