{% for item in items %}
    {% if item.active %}
        {% set children = item.children %}
        {% set hasChildren = (count(children) > 0) %}
        <li data-category-id="{{ item.id }}" class="{{ hasChildren ? 'has-children' : 'leaf-node' }}" data-leaf-node="{{ hasChildren ? 'false' : 'true' }}" data-name="{{ item.name }}">
            {% set categoryIcon = '' %}
            {# set blank category icon for main categories #}
            {% if item.level == 2 %}{% set categoryIcon = '<span class="icon"></span>' %}{% endif %}
            {% if item.json.icon is defined and item.json.icon %}
            {% set categoryIcon = '<span class="icon" style="background-image:url(/assets/img/categories/' ~ item.json.icon ~ ')"></span>' %}
            {% endif %}
            {% set paid_category_text = (mark_paid_categories ? (item.paid_category ? ' <span class="text-danger" title="Naplatna kategorija">*</span>' : '') : '') %}
            <a href="#" data-id="{{ item.id }}">{{ categoryIcon ~ item.name ~ paid_category_text }}</a>
            {% if hasChildren %}
            {% set level = item.level %}
            {% set parent_category_id = item.id %}
            {{ partial('chunks/nested-ul', ['items': children, 'class': class, 'mark_paid_categories': (mark_paid_categories ? mark_paid_categories : false), 'level': level, 'parent_category_id': parent_category_id]) }}
            {% endif %}
        </li>
    {% endif %}
{% endfor %}
