{# some terrible hacks to not repeat myself :) #}
{% if banners %}
    <div class="sidebar-rectangle-holder hidden-xs">
        {% for i in 1..3 %}
            {% set banner_slot_name = 'banner_sidebar_right_' ~ i %}
            <?php if (isset($$banner_slot_name) && !empty($$banner_slot_name)) { ?>
            <div class="banner text-center margin-top-sm"><?php echo $$banner_slot_name; ?></div>
            <?php } ?>
        {% endfor %}
    </div>
{% endif %}
