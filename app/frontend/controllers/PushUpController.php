<?php

namespace Baseapp\Frontend\Controllers;

use Baseapp\Library\Debug;
use Baseapp\Library\Products\PushupProductsSelector;
use Baseapp\Library\Products\PushupProductsChooser;
use Baseapp\Library\Utils;
use Baseapp\Library\WspayHelper;
use Baseapp\Models\Ads;
use Baseapp\Models\Orders;
use Baseapp\Traits\BarcodeControllerMethods;
use Phalcon\Dispatcher;
use Phalcon\Escaper;
use Phalcon\Mvc\Model\Query\Builder;

class PushUpController extends IndexController
{
    use BarcodeControllerMethods;

    protected $current_user;

    // Load zendesk for the entire controller as requested
    protected $load_zendesk_widget_script = true;

    /**
     * Overriding beforeExecuteRoute in order to provide some global view variables and secure access.
     *
     * @param Dispatcher $dispatcher
     *
     * @return bool
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        // Making sure the parent is called too!
        parent::beforeExecuteRoute($dispatcher);

        // This controller should not be accessible for anon users
        if (!$this->auth->logged_in()) {
            $this->disable_view();
            $this->redirect_signin();

            return false;
        }

        $this->current_user = $this->auth->get_user();

        if (method_exists($this, 'setLayoutFeature')) {
            // turn off the search bar
            $this->setLayoutFeature('search_bar', false);
        }

        $this->view->setVar('current_action', $dispatcher->getActionName());
        $this->view->setVar('current_action_method', $dispatcher->getActiveMethod());
    }

    /**
     * Overriding afterExecuteRoute in order to provide some global view variables.
     *
     * @param Dispatcher $dispatcher
     *
     * @return bool
     */
    public function afterExecuteRoute(Dispatcher $dispatcher)
    {
        // Making sure the parent is called too!
        parent::afterExecuteRoute($dispatcher);

        // turn off all banner zones for this controller
        $this->view->setVar('banners', false);
    }

    protected function checkEntity(Ads $entity)
    {
        if (!$entity) {
            return false;
        }

        return Ads::isPushable($entity->toArray(), $this->current_user);
    }

    /**
     * @param array $ids
     *
     * @return bool|mixed
     */
    protected function getAndCheckAds(array $ids = array())
    {
        $builder = new Builder();

        $ads = $builder->from('Baseapp\Models\Ads')->inWhere('id', $ids)->getQuery()->execute();

        if (!$ads->valid()) {
            return false;
        }

        $error = false;
        foreach ($ads as $ad) {
            if (!$this->checkEntity($ad)) {
                $error = true;
                break;
            }
        }

        // If we spotted an error, return false, if not return the $ads collection/resultset
        if ($error) {
            return false;
        } else {
            return $ads;
        }
    }

    public function indexAction($entity_id = null)
    {
        $page_title = sprintf('Skok na vrh - potvrda narudžbe');
        $this->tag->setTitle($page_title);

        $this->assets->addJs('assets/js/ie-11-nav-cache-bust.js');

        // Allowing pushup for more than one ad at the same time
        if (is_string($entity_id)) {
            if (strpos($entity_id, ',') !== false) {
                // There's a comma present (used to separate multiple ids), explode on it
                $entity_ids = array_map('trim', explode(',', $entity_id));
            } else {
                // No comma, just a single id assumed
                $entity_ids = (array) $entity_id;
            }
        } else {
            $entity_ids = (array) $entity_id;
        }

        $ads = $this->getAndCheckAds($entity_ids);
        if (!$ads) {
            $this->flashSession->error('<strong>Greška</strong> Neispravni parametri za skok na vrh');
            return $this->redirect_to('moj-kutak');
        }

        // Check if there are any valid unpaid PushUp Orders matching these ad ids, and if there are,
        // redirect to the order if we found a single one, or show an error in case there are many
        // unpaid orders foun
        $unpaid_resultset = Orders::findUnpaidPushUpOrders($this->current_user->id, $entity_ids);
        if (!empty($unpaid_resultset)) {
            if (1 === count($unpaid_resultset)) {
                /* @var \Phalcon\Mvc\Model\Resultset\Complex $unpaid_resultset */
                $unpaid_order_id = $unpaid_resultset[0]->o->id;
                $this->flashSession->notice('Preusmjerili smo vas na plaćanje postojeće skok na vrh narudžbe');
                return $this->redirect_to('push-up/payment/' . $unpaid_order_id);
            } else {
                $this->flashSession->error('<strong>Greška</strong> Odabrani oglas(i) već ima(ju) više skok na vrh narudžbi koje čekaju plaćanje!');
                // return $this->redirect_to('moj-kutak/narudzbe');
                return $this->redirect_back();
            }
        }

        // $current_href = 'push-up/' . implode(',', $entity_ids);

        $selector = new PushupProductsSelector($ads, $this->request->getPost());
        $products_chooser = new PushupProductsChooser($selector);

        // If submitted, create an order for the products and redirect to the payment
        // controller to handle next steps
        if ($this->request->getPost('next')) {
            $order_data = array(
                'user_id' => $this->current_user->id,
            );
            $order = Orders::createWithPushups($products_chooser->getChosenProducts(), $order_data);
            if ($order) {
                return $this->redirect_to('push-up/payment/' . $order->id);
            } else {
                $this->view->setVar('errors', $order);
            }
        }

        $this->view->setVar('products_table', $products_chooser->getMarkup());
        $this->view->setVar('ads', $ads);
    }

    public function paymentAction($order_id = null)
    {
        $allow_payment = true;

        $page_title = sprintf('Skok na vrh - plaćanje narudžbe');
        $this->tag->setTitle($page_title);

        // Basic param checking
        if (!$order_id) {
            $allow_payment = false;
        }

        $order = null;
        if ($order_id) {
            $order = Orders::findFirst($order_id);
        }

        // Making sure the Order exists
        if ($allow_payment && !$order) {
            $allow_payment = false;

            $message = '<strong>Ooops!</strong> Nije moguće pronaći podatke o vašoj narudžbi!';
            $this->flashSession->error($message);
        }

        // Making sure the current user is the one that created it
        if ($allow_payment && $order->user_id != $this->current_user->id) {
            $allow_payment = false;

            $message = '<strong>Ooops!</strong> To nije vaša narudžba!';
            $this->flashSession->error($message);
        }

        // Making sure it's not expired or finalized or similar...
        if ($allow_payment && $order && ($order->isExpired() || !$order->isStatusChangeable())) {
            $allow_payment = false;

            $message = '<strong>Ooops!</strong> Narudžbu više nije moguće provesti (istekla/finalizirana)!';
            $this->flashSession->error($message);
        }

        // Making sure there's a cost attached to it
        if ($allow_payment && $order->getTotalRaw() <= 0) {
            $allow_payment = false;

            $message = '<strong>Ooops!</strong> Ukupan iznos narudžbe ne zahtjeva provođenje plaćanja!';
            $this->flashSession->error($message);
        }

        // If any errors occurred, bail back to 'moj-kutak'
        if (!$allow_payment) {
            $this->redirect_to('moj-kutak');
        }

        // Checking 'offline' payment submit confirmation and redirecting
        if ($this->request->isPost('next')) {
            // We're done here, the order should be fulfilled offline
            $this->flashSession->success('<strong>Super!</strong> Vaša narudžba je zaprimljena i čeka aktivaciju!');
            return $this->redirect_to('moj-kutak');
        }

        // Abort early to prevent errors below if we don't have an order to work with
        if (!$order) {
            return;
        }

        // Build payment thing
        $url_args = array();
        $payment_data = array(
            'ShoppingCartID' => $order->getPbo(),
            'TotalAmount'    => $order->getTotal(),
            'ReturnURL'      => $this->url->get('push-up/payment/' . $order->id, $url_args + ['return' => 1]),
            'CancelURL'      => $this->url->get('push-up/payment/' . $order->id, $url_args + ['cancel' => 1]),
            'ReturnErrorURL' => $this->url->get('push-up/payment/' . $order->id, $url_args + ['error' => 1])
        );
        $helper = new WspayHelper($payment_data, $this->current_user);

        // Example error request we get from WSPay:
        if ($this->request->getQuery('error')) {
            // upgrade/$ad_id?step=2&token=83b6fef8f17b772c417f4d8587631b87aafb1032&error=1&CustomerFirstname=sda
            // &CustomerSurname=asd&CustomerAddress=neka+tamo&CustomerCountry=Hrvatska&CustomerZIP=da+nebi
            // &CustomerCity=ma+je&CustomerEmail=asd@asdf.com&CustomerPhone=123456&ShoppingCartID=900000164
            // &Lang=HR&DateTime=20150621022730&Amount=350&ECI=&PaymentType=VISA&PaymentPlan=0000
            // &ShopPostedPaymentPlan=0000&Success=0&ApprovalCode=&ErrorMessage=ODBIJENO
            $escaper = new Escaper();
            $cart_id = $this->request->getQuery('ShoppingCartID', 'string');
            $remote_message = $escaper->escapeHtml($this->request->getQuery('ErrorMessage', 'string'));
            $data = json_encode($this->request->getQuery());

            $log_message = 'WSPay returned an error for Order #' . $escaper->escapeHtml($cart_id) . ': ' . $remote_message;
            $log_message .= "\n" . $data;
            $this->saveAudit($log_message);

            $this->flashSession->error('<strong>Greška</strong> WSPay vratio: ' . $remote_message . '. Molimo pokušajte ponovo ili odaberite drugi način plaćanja.');
        } elseif ($this->request->getQuery('cancel')) {
            // TODO: Do we even have to bother with anything in case of cancel? Why?
        } elseif ($this->request->getQuery('return') && $this->request->getQuery('Success')) {
            // Verifying received data from WSpay
            $query_vars = $this->request->getQuery();
            $valid_signature = $helper->verifyReturnedSignature($query_vars);
            if (true === $valid_signature) {
                // Great success!
                $save_data = array(
                    'payment_method' => strtolower($this->request->getQuery('PaymentType', 'string')),
                    'approval_code'  => $this->request->getQuery('ApprovalCode')
                );
                $finalized = $order->finalizePurchase($save_data);
                if ($finalized) {
                    // $this->deleteSessionTokenData($token);
                    $this->flashSession->success('<strong>Odlično!</strong> Vaša narudžba je uspješno obrađena!');
                    return $this->redirect_to('moj-kutak');
                } else {
                    $this->flashSession->error('<strong>Greška!</strong> Izvršenje plaćene narudžbe nije uspjelo. Molimo kontaktirajte korisničku podršku!');
                }
            }
        }

        // Display payment method choices
        $this->view->setVar('order_total_formatted', Utils::format_money($order->getTotal(), true));
        $this->view->setVar('order_pbo', $order->getPbo());
        $this->view->setVar('order_table_markup', $order->getPaymentRecapTableMarkup());
        $this->view->setVar('rendered_form',  $helper->getFormMarkup());

        // $barcode_src = $this->url->get('push-up/barcode-order/' . $order->id);
        $barcode_src = $order->get2DBarcodeInlineSvg();
        $this->view->setVar('order_id', $order->id);
        $this->view->setVar('payment_data_table_markup', Orders::buildPaymentDataTableMarkup($order, $barcode_src));
        $this->view->setVar('offline_button_markup', Orders::buildOfflineConfirmButtonMarkup('pushup', 'Naruči'));

        $this->assets->addJs('assets/js/payment-method-toggle.js');
        $this->assets->addJs('assets/js/ie-11-nav-cache-bust.js');
    }
}
