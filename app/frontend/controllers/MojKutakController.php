<?php

namespace Baseapp\Frontend\Controllers;

use Baseapp\Extension\CsrfException;
use Baseapp\Extension\Tag;
use Baseapp\Extension\Validation;
use Baseapp\Library\Utils;
use Baseapp\Models\Orders;
use Baseapp\Models\Users;
use Baseapp\Models\UsersMessages;
use Baseapp\Models\UsersEmailAgents;
use Baseapp\Models\UsersProfileImages;
use Baseapp\Models\UsersShops;
use Baseapp\Models\UsersShopsFeatured;
use Baseapp\Models\Ads;
use Baseapp\Models\Categories;
use Baseapp\Models\Locations;
use Baseapp\Library\Tool;
use Baseapp\Library\Parameters\Parametrizator;
use Baseapp\Traits\CommonAdActions;
use Baseapp\Traits\ParametrizatorHelpers;
use Phalcon\Dispatcher;
use Phalcon\Mvc\View;
use Phalcon\Paginator\Adapter\QueryBuilder;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator\Confirmation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use Baseapp\Library\Validations\AdsFrontend as AdsValidationsFrontend;

/**
 * Frontend MyAccount Controller
 */
class MojKutakController extends IndexController
{
    use ParametrizatorHelpers;
    use CommonAdActions;

    private $current_user;

    // Load zendesk for the entire controller as requested
    protected $load_zendesk_widget_script = true;

    /**
     * Overriding beforeExecuteRoute in order to provide some global view variables and secure access.
     *
     * @param Dispatcher $dispatcher
     *
     * @return bool
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        // Making sure the parent is called too!
        parent::beforeExecuteRoute($dispatcher);

        // This controller should not be accessible for anon users
        if (!$this->auth->logged_in()) {
            $this->disable_view();
            $this->redirect_signin();
            return false;
        }
        $this->current_user = $this->auth->get_user();
        $this->view->setVar('current_user', $this->current_user);

        if (method_exists($this, 'setLayoutFeature')) {
            $this->setLayoutFeature('search_bar', true);
            $this->setLayoutFeature('full_page_width', true);
        }

        /**
         * Due to the potential to be redirected to a 404 page after deleting something, we're just blindly
         * removing the page query variable and redirecting to the first page always... Not ideal, but not really
         * sure how to better handle it at this moment. Maybe later. ¯\_(ツ)_/¯
         */
        $next_url = $this->get_safe_redir_path($this->get_redir_path_from_current_request($this->request->getURI()));
        $next_url_without_paging = Utils::remove_query_var('page', $next_url);
        $this->view->setVar('next_url', $next_url_without_paging);

        $current_action = $dispatcher->getActionName();
        $this->view->setVar('current_action', $current_action);
        $this->view->setVar('current_action_method', $dispatcher->getActiveMethod());

        $current_action_group = array(
            'sviOglasi'          => 'mojiOglasi',
            'aktivniOglasi'      => 'mojiOglasi',
            'istekliOglasi'      => 'mojiOglasi',
            'deaktiviraniOglasi' => 'mojiOglasi',
            'neobjavljeniOglasi' => 'mojiOglasi',
            'podijeliOglase'     => 'mojiOglasi',
            'urediOglas'         => 'mojiOglasi',
            'podaci'             => 'profil',
            'lozinka'            => 'profil',
            'trgovina'           => 'profil',
            'trgovinaIzlog'      => 'profil',
            'avatar'             => 'profil',
            'coverbg'            => 'profil'
        );
        $this->view->setVar('current_action_group', isset($current_action_group[$current_action]) ? $current_action_group[$current_action] : null);
    }

    /**
     * Overriding afterExecuteRoute in order to provide some global view variables.
     *
     * @param Dispatcher $dispatcher
     *
     * @return bool
     */
    public function afterExecuteRoute(Dispatcher $dispatcher)
    {
        // Making sure the parent is called too!
        parent::afterExecuteRoute($dispatcher);

        // turn off all banner zones for this controller
        $this->view->setVar('banners', false);

        $this->assets->addJs('assets/js/my-account.js');
    }

    private function is_user_owner($entity, $custom_message = 'Ne možete uređivati oglas koji nije vaš')
    {
        $same_user = false;

        if ($this->current_user && $entity->user_id == $this->current_user->id) {
            $same_user = true;
        }

        if ($this->request->isPost() && $this->request->hasPost('user_id')) {
            if ((int) $this->request->getPost('user_id', 'int') == $this->current_user->id) {
                $same_user = true;
            }
        }

        if (!$same_user) {
            $this->flashSession->error($custom_message);
        }

        return $same_user;
    }

    /**
     * Index Action
     */
    public function indexAction()
    {
        return $this->dispatcher->forward(array(
            'action' => 'sviOglasi'
        ));
    }

    private function getBulkActions($listing = null)
    {
        $bulkActions = null;
        switch($listing) {
            case 'all':
            case 'active':
                $bulkActions = array(
                    array(
                        'title' => 'Prodano',
                        'attributes' => array(
                            'type'             => 'button',
                            'class'            => 'bulk-sold',
                            'data-bulk-action' => 'sold',
                            'data-toggle'      => 'modal',
                            'data-target'      => '#bulkActionModal',
                            'data-formurl'     => '/moj-kutak/prodani-oglas/'
                        )
                    ),
                    array(
                        'title' => 'Deaktiviraj',
                        'attributes' => array(
                            'type'             => 'button',
                            'class'            => 'bulk-deactivate',
                            'data-bulk-action' => 'deactivate',
                            'data-toggle'      => 'modal',
                            'data-target'      => '#bulkActionModal',
                            'data-formurl'     => '/moj-kutak/deaktiviraj-oglas/'
                        )
                    ),
                    array(
                        'title' => 'Obriši',
                        'attributes' => array(
                            'type'             => 'button',
                            'class'            => 'bulk-delete btn-danger',
                            'data-bulk-action' => 'delete',
                            'data-toggle'      => 'modal',
                            'data-target'      => '#bulkActionModal',
                            'data-formurl'     => '/moj-kutak/obrisi-oglas/'
                        )
                    )
                );
                break;

            case 'deactivated':
                $bulkActions = array(
                    array(
                        'title' => 'Aktiviraj',
                        'attributes' => array(
                            'type'             => 'button',
                            'class'            => 'bulk-activate',
                            'data-bulk-action' => 'activate',
                            'data-toggle'      => 'modal',
                            'data-target'      => '#bulkActionModal',
                            'data-formurl'     => '/moj-kutak/aktiviraj-oglas/'
                        )
                    ),
                    array(
                        'title' => 'Obriši',
                        'attributes' => array(
                            'type'             => 'button',
                            'class'            => 'bulk-delete btn-danger',
                            'data-bulk-action' => 'delete',
                            'data-toggle'      => 'modal',
                            'data-target'      => '#bulkActionModal',
                            'data-formurl'     => '/moj-kutak/obrisi-oglas/'
                        )
                    )
                );
                break;

            case 'expired':
                $bulkActions = array(
                    array(
                        'title' => 'Ponovi',
                        'attributes' => array(
                            'type'             => 'button',
                            'class'            => 'bulk-republish',
                            'data-bulk-action' => 'republish',
                            'data-toggle'      => 'modal',
                            'data-target'      => '#bulkActionModal',
                            'data-formurl'     => '/moj-kutak/ponovi-oglas/'
                        )
                    ),
                    array(
                        'title' => 'Obriši',
                        'attributes' => array(
                            'type'             => 'button',
                            'class'            => 'bulk-delete btn-danger',
                            'data-bulk-action' => 'delete',
                            'data-toggle'      => 'modal',
                            'data-target'      => '#bulkActionModal',
                            'data-formurl'     => '/moj-kutak/obrisi-oglas/'
                        )
                    )
                );
                break;

            case 'inactive':
            case 'sold':
                $bulkActions = array(
                    array(
                        'title' => 'Obriši',
                        'attributes' => array(
                            'type'             => 'button',
                            'class'            => 'bulk-delete btn-danger',
                            'data-bulk-action' => 'delete',
                            'data-toggle'      => 'modal',
                            'data-target'      => '#bulkActionModal',
                            'data-formurl'     => '/moj-kutak/obrisi-oglas/'
                        )
                    )
                );
                break;
        }

        return $bulkActions;
    }

    public function sviOglasiAction()
    {
        $this->tag->setTitle('Moj kutak - Moji oglasi - Svi oglasi');
        $this->view->pick('moj-kutak/adslisting');
        $searchTerm = $this->request->get('term', 'string', null);
        $this->view->setVar('searchTerm', $searchTerm);
        $resulting_array = $this->getPaginatedAds($this->current_user, 'all', null, null, $searchTerm);
        if ($resulting_array) {
            if (!$resulting_array['error']) {
                $this->view->setVar('cumulativeAdsCounters', $this->getCumulativeAdsCounters($this->current_user, $searchTerm));
                $this->view->setVar('ad_listing_title', 'Svi oglasi');
                $this->view->setVar('ads', $resulting_array['ads']);
                $this->view->setVar('results_total', $resulting_array['total']);
                $this->view->setVar('pagination_links', $resulting_array['pagination']);
                $this->view->setVar('bulkActions', $this->getBulkActions('all'));
                $this->assets->addJs('assets/js/my-account-classified-list.js');
            } elseif (isset($resulting_array['trigger_404']) && $resulting_array['trigger_404']) {
                return $this->trigger_404();
            }
        }
    }

    public function aktivniOglasiAction()
    {
        if ($this->request->isAjax()) {
            $this->disable_view();
            $this->response->setContentType('application/json', 'UTF-8');
            $response_array = array('status' => false);
            $paginated_ads = $this->getPaginatedAds($this->current_user, 'active');
            if ($paginated_ads && isset($paginated_ads['ads'])) {
                $ads_markup = array();
                foreach ($paginated_ads['ads'] as $ad) {
                    $ads_markup[] = $this->simpleView->render(
                        'moj-kutak/adslisting-simple-chunk',
                        array(
                            'ad' => $ad,
                        )
                    );
                }
                if (count($ads_markup)) {
                    if ($paginated_ads['pagination']) {
                        $ads_markup[] = $paginated_ads['pagination'];
                    }

                    $response_array['status'] = true;
                    $response_array['data'] = implode(PHP_EOL, $ads_markup);
                }
            }
            $this->response->setJsonContent($response_array);
            return $this->response;
        } else {
            $this->tag->setTitle('Moj kutak - Moji oglasi - Aktivni oglasi');
            $this->view->pick('moj-kutak/adslisting');
            $searchTerm = $this->request->get('term', 'string', null);
            $this->view->setVar('searchTerm', $searchTerm);
            $resulting_array = $this->getPaginatedAds($this->current_user, 'active', null, null, $searchTerm);
            if ($resulting_array) {
                if (!$resulting_array['error']) {
                    $this->view->setVar('cumulativeAdsCounters', $this->getCumulativeAdsCounters($this->current_user));
                    $this->view->setVar('ad_listing_title', 'Aktivni oglasi');
                    $this->view->setVar('ads', $resulting_array['ads']);
                    $this->view->setVar('results_total', $resulting_array['total']);
                    $this->view->setVar('pagination_links', $resulting_array['pagination']);
                    $this->view->setVar('bulkActions', $this->getBulkActions('active'));
                    $this->assets->addJs('assets/js/my-account-classified-list.js');
                } elseif (isset($resulting_array['trigger_404']) && $resulting_array['trigger_404']) {
                    return $this->trigger_404();
                }
            }
        }
    }

    public function istekliOglasiAction()
    {
        $this->tag->setTitle('Moj kutak - Moji oglasi - Istekli oglasi');
        $this->view->pick('moj-kutak/adslisting');
        $searchTerm = $this->request->get('term', 'string', null);
        $this->view->setVar('searchTerm', $searchTerm);
        $resulting_array = $this->getPaginatedAds($this->current_user, 'expired', null, null, $searchTerm);
        if ($resulting_array) {
            if (!$resulting_array['error']) {
                $this->view->setVar('cumulativeAdsCounters', $this->getCumulativeAdsCounters($this->current_user));
                $this->view->setVar('ad_listing_title', 'Istekli oglasi');
                $this->view->setVar('ads', $resulting_array['ads']);
                $this->view->setVar('results_total', $resulting_array['total']);
                $this->view->setVar('pagination_links', $resulting_array['pagination']);
                $this->view->setVar('bulkActions', $this->getBulkActions('expired'));
                $this->assets->addJs('assets/js/my-account-classified-list.js');
            } elseif (isset($resulting_array['trigger_404']) && $resulting_array['trigger_404']) {
                return $this->trigger_404();
            }
        }
    }

    public function deaktiviraniOglasiAction()
    {
        $this->tag->setTitle('Moj kutak - Moji oglasi - Deaktivirani oglasi');
        $this->view->pick('moj-kutak/adslisting');
        $searchTerm = $this->request->get('term', 'string', null);
        $this->view->setVar('searchTerm', $searchTerm);
        $resulting_array = $this->getPaginatedAds($this->current_user, 'deactivated', null, null, $searchTerm);
        if ($resulting_array) {
            if (!$resulting_array['error']) {
                $this->view->setVar('cumulativeAdsCounters', $this->getCumulativeAdsCounters($this->current_user));
                $this->view->setVar('ad_listing_title', 'Deaktivirani oglasi');
                $this->view->setVar('ads', $resulting_array['ads']);
                $this->view->setVar('results_total', $resulting_array['total']);
                $this->view->setVar('pagination_links', $resulting_array['pagination']);
                $this->view->setVar('bulkActions', $this->getBulkActions('deactivated'));
                $this->assets->addJs('assets/js/my-account-classified-list.js');
            } elseif (isset($resulting_array['trigger_404']) && $resulting_array['trigger_404']) {
                return $this->trigger_404();
            }
        }
    }

    public function neobjavljeniOglasiAction()
    {
        $this->tag->setTitle('Moj kutak - Moji oglasi - Neobjavljeni oglasi');
        $this->view->pick('moj-kutak/adslisting');
        $searchTerm = $this->request->get('term', 'string', null);
        $this->view->setVar('searchTerm', $searchTerm);
        $resulting_array = $this->getPaginatedAds($this->current_user, 'inactive', null, null, $searchTerm);
        if ($resulting_array) {
            if (!$resulting_array['error']) {
                $this->view->setVar('cumulativeAdsCounters', $this->getCumulativeAdsCounters($this->current_user));
                $this->view->setVar('ad_listing_title', 'Neobjavljeni oglasi');
                $this->view->setVar('ads', $resulting_array['ads']);
                $this->view->setVar('results_total', $resulting_array['total']);
                $this->view->setVar('pagination_links', $resulting_array['pagination']);
                $this->view->setVar('bulkActions', $this->getBulkActions('inactive'));
                $this->assets->addJs('assets/js/my-account-classified-list.js');
            } elseif (isset($resulting_array['trigger_404']) && $resulting_array['trigger_404']) {
                return $this->trigger_404();
            }
        }
    }

    public function prodaniOglasiAction()
    {
        $this->tag->setTitle('Moj kutak - Moji oglasi - Prodani oglasi');
        $this->view->pick('moj-kutak/adslisting');
        $searchTerm = $this->request->get('term', 'string', null);
        $this->view->setVar('searchTerm', $searchTerm);
        $resulting_array = $this->getPaginatedAds($this->current_user, 'sold', null, null, $searchTerm);
        if ($resulting_array) {
            if (!$resulting_array['error']) {
                $this->view->setVar('cumulativeAdsCounters', $this->getCumulativeAdsCounters($this->current_user));
                $this->view->setVar('ad_listing_title', 'Prodani oglasi');
                $this->view->setVar('ads', $resulting_array['ads']);
                $this->view->setVar('results_total', $resulting_array['total']);
                $this->view->setVar('pagination_links', $resulting_array['pagination']);
                $this->view->setVar('bulkActions', $this->getBulkActions('sold'));
                $this->assets->addJs('assets/js/my-account-classified-list.js');
            } elseif (isset($resulting_array['trigger_404']) && $resulting_array['trigger_404']) {
                return $this->trigger_404();
            }
        }
    }

    public function podijeliOglaseAction()
    {
        $this->tag->setTitle('Moj kutak - Podijeli oglase');
        $this->view->pick('moj-kutak/adslisting');
        $this->view->setVar('shareable_link', $this->current_user->get_ads_page());
        $this->view->setVar('cumulativeAdsCounters', $this->getCumulativeAdsCounters($this->current_user));
        $this->assets->addJs('assets/vendor/zeroclipboard-2.2.0/ZeroClipboard.min.js');
        $this->assets->addJs('assets/js/shareable-link.js');
    }

    public function urediOglasAction($entity_id)
    {
        $continue_with_edit = true;

        $entity_id = (int) $entity_id;

        if (!$entity_id) {
            $this->flashSession->error('Nedostaje ID oglasa koji pokušavate urediti!');
            $continue_with_edit = false;
        }

        $entity = Ads::findFirst(array('id = ?0 AND soft_delete = 0', 'bind' => array($entity_id)));

        if ($continue_with_edit && !$entity) {
            $this->flashSession->error(sprintf('Oglas nije pronađen [ID: %s]', $entity_id));
            $continue_with_edit = false;
        }

        if ($continue_with_edit && !$this->is_user_owner($entity)) {
            $continue_with_edit = false;
        }

        if (!$continue_with_edit) {
            return $this->redirect_back();
        }

        $this->view->setVar('cumulativeAdsCounters', $this->getCumulativeAdsCounters($this->current_user));

        $parametrizator = new Parametrizator();
        $parametrizator->setModule('frontend');
        $parametrizator->setCategory($entity->getCategory());
        $parametrizator->setAd($entity);

        $show_ad_form = true;

        if ($this->request->hasPost('save')) {
            if ($this->request->getPost('save') == 'preview') {
                $this->setLayoutFeature('full_page_width', false);

                // preview the ad before saving
                $validation = new AdsValidationsFrontend();
                $validation = $validation->get();
                $parametrizator->setValidation($validation);
                $parametrizator->setRequest($this->request);
                $messages = $parametrizator->prepare_and_validate();

                if (count($messages)) {
                    $this->view->setVar('errors', $messages);
                    $this->flashSession->error('<strong>Greška!</strong> Ispravite sva crvena polja.');
                    $parametrizator->setErrors($messages);
                } else {
                    $parametrizator->parametrize();

                    $back_markup = '<form action="/moj-kutak/uredi-oglas/' . $entity->id . '" method="post"><input type="hidden" name="post_field" value="' . base64_encode(json_encode($this->request->getPost())) . '"/><button type="submit" name="save" value="back-from-preview" class="btn btn-primary"><span class="fa fa-fw fa-arrow-circle-left"></span> Povratak na uređivanje oglasa</button></form>';

                    $extra_view_data = array(
                        'back_markup' => $back_markup
                    );
                    $this->getAdPreview($parametrizator, $extra_view_data);
                    $show_ad_form = false;
                }
            } elseif ($this->request->getPost('save') == 'back-from-preview') {
                $parametrizator->setData(json_decode(base64_decode($this->request->getPost('post_field')), true));
                $this->assets->addJs('assets/js/classified-view.js');
            } else {
                $saved = $entity->frontend_save_changes($this->request);
                if ($saved instanceof Ads) {
                    $this->flashSession->success('<strong>Super!</strong> Vaš oglas je uspješno pohranjen te je ponovno poslan na moderaciju!');
                    return $this->redirect_back();
                } else {
                    $this->view->setVar('errors', $saved);
                    $this->flashSession->error('<strong>Greška!</strong> Ispravite sva crvena polja.');
                    $parametrizator->setRequest($this->request);
                    $parametrizator->setErrors($saved);
                }
            }
        } elseif ($this->request->getPost('cancel') == 'cancel') {
            return $this->redirect_back();
        } else {
            $parametrizator->setData($entity->getData());
        }

        if ($show_ad_form) {
            $this->tag->setTitle('Moj kutak - Moji oglasi - Uredi oglas');
            $this->view->pick('moj-kutak/adsedit');

            $this->assets->addCss('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.css');
            $this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.js');
            $this->assets->addCss('assets/vendor/datepicker3.css');
            $this->assets->addJs('assets/vendor/bootstrap-datepicker.js');
            // $this->assets->addCss('assets/vendor/awesome-bootstrap-checkbox.css');
            $this->assets->addJs('assets/vendor/jquery.autoNumeric.js');
            $this->assets->addCss('assets/vendor/fancyBox/source/jquery.fancybox.css');
            $this->assets->addJs('assets/vendor/fancyBox/source/jquery.fancybox.pack.js');
            $this->assets->addJs('assets/js/my-account-classified-edit.js');

            $this->setup_rendered_parameters($parametrizator->render_input());
            $this->view->setVar('category', $entity->getCategory());
            $this->view->setVar('ad', $entity);

            $phoneNumberType = $this->current_user->country_id ? ($this->current_user->country_id == 1 ? 'domestic' : 'foreign') : 'auto';
            $this->view->setVar('phoneNumberType', $phoneNumberType);
        }
    }

    /**
     * Previews the Ad before submitting it
     *
     * @param string|null $token
     *
     * @return ResponseInterface
     */
    public function pregledAction()
    {
        // Simplify the main layout to remove distractions
        //$this->setLayoutFeature('full_page_width', true);
        //$this->setLayoutFeature('main_nav', false);
        //$this->setLayoutFeature('footer_links', false);
    }

    protected function getAdPreview($parametrizator, $extras = array())
    {
        $ad = $parametrizator->getAd();
        $page_title = sprintf('Pregled oglasa prije predaje');
        $this->tag->setTitle($page_title . ' - ' . $ad->title);

        $rendered_markup = $ad->getRenderedAd();

        // Load custom assets if needed
        if (isset($rendered_markup['assets'])) {
            $this->setup_assets($rendered_markup['assets']);
        }
        // Load regular assets for the ad view
        $this->assets->addJs('assets/js/classified-view.js');

        $entity_user = $ad->getUserDetails();
        // Check if the ad itself has custom phone numbers on it... if so,
        // then we have to remove phone numbers from $entity_user and replace them
        // with the ones found in the ad
        if ((isset($ad->phone1) && trim($ad->phone1)) || (isset($ad->phone2) && trim($ad->phone2))) {
            $entity_user->phone1 = $ad->phone1;
            $entity_user->phone2 = $ad->phone2;
        }
        $this->view->setVar('ad_user', $entity_user);

        // Quick hack to allow passing additional view data/vars from the action
        if (!empty($extras)) {
            foreach ($extras as $k => $v) {
                $this->view->setVar($k, $v);
            }
        }

        $this->view->pick('chunks/ads-submission-preview');
        $this->view->setVar('ad', $ad);
        $this->view->setVar('ad_render', $rendered_markup);
    }

    /**
     * (Soft)Delete one or multiple ads from user's account
     *
     * @param  int|string $entity_id
     */
    public function obrisiOglasAction($entity_id)
    {
        $continue_with_action = true;
        $multiple_entities    = true;

        if (strpos($entity_id, ',') === false) {
            $entity_id         = (int) $entity_id;
            $multiple_entities = false;
        }

        if (!$entity_id) {
            $this->flashSession->error('Nedostaje ID oglasa koji pokušavate obrisati!');
            $continue_with_action = false;
        }

        if ($continue_with_action) {
            $ads_to_be_deleted = (new Ads())->getUserAdsById($this->current_user->id, $entity_id);

            if ($ads_to_be_deleted) {
                if ($ads_to_be_deleted instanceof Ads) {
                    // we're dealing with only one ad here
                    if ($ads_to_be_deleted->soft_delete()) {
                        $this->flashSession->success('Vaš oglas je <strong>uspješno obrisan</strong>.');
                    } else {
                        $this->flashSession->error('Neuspješan pokušaj brisanja oglasa.');
                    }
                } else {
                    // we're dealing with multiple ads here
                    $delete_ok_count = $delete_nok_count = 0;
                    foreach ($ads_to_be_deleted as $ad) {
                        if ($ad->soft_delete()) {
                            $delete_ok_count++;
                        } else {
                            $delete_nok_count++;
                        }
                    }

                    if ($delete_ok_count) {
                        if ($delete_nok_count) {
                            $this->flashSession->success('Neki vaši oglasi su <strong>uspješno obrisani</strong>.');
                        } else {
                            $this->flashSession->success('Vaši oglasi su <strong>uspješno obrisani</strong>.');
                        }
                    } else {
                        $this->flashSession->error('Neuspješan pokušaj brisanja oglasa.');
                    }
                }
            } else {
                if ($multiple_entities) {
                    $this->flashSession->error(sprintf('Oglasi nisu pronađeni [ID: %s]', implode(', ', explode(',', $entity_id))));
                } else {
                    $this->flashSession->error(sprintf('Oglas nije pronađen [ID: %s]', $entity_id));
                }
            }
        }

        return $this->redirect_back();
    }

    public function prodaniOglasAction($entity_id)
    {
        $continue_with_action = true;
        $multiple_entities    = true;

        if (strpos($entity_id, ',') === false) {
            $entity_id         = (int) $entity_id;
            $multiple_entities = false;
        }

        if (!$entity_id) {
            $this->flashSession->error('Nedostaje ID oglasa koji pokušavate označiti kao prodanim!');
            $continue_with_action = false;
        }

        if ($continue_with_action) {

            if ($users_sold_ads = (new Ads())->sellUserAdsById($this->current_user->id, $entity_id)) {
                if ($multiple_entities) {
                    $this->flashSession->success('Vaši oglasi su <strong>uspješno označeni kao prodani</strong>.');
                } else {
                    $this->flashSession->success('Vaši oglas je <strong>uspješno označen kao prodan</strong>.');
                }
            } else {
                $this->flashSession->error('Neuspješan pokušaj označavanja oglasa kao prodan.');
            }
        } else {
            if ($multiple_entities) {
                $this->flashSession->error(sprintf('Oglasi nisu pronađeni [ID: %s]', implode(', ', explode(',', $entity_id))));
            } else {
                $this->flashSession->error(sprintf('Oglas nije pronađen [ID: %s]', $entity_id));
            }
        }

        return $this->redirect_back();
    }


    public function aktivirajOglasAction($entity_id)
    {
        $continue_with_action = true;
        $multiple_entities    = true;

        if (strpos($entity_id, ',') === false) {
            $entity_id         = (int) $entity_id;
            $multiple_entities = false;
        }

        if (!$entity_id) {
            $this->flashSession->error('Nedostaje ID oglasa koji pokušavate aktivirati!');
            $continue_with_action = false;
        }

        if ($continue_with_action) {
            if ($users_activated_ads = (new Ads())->activateUserAdsById($this->current_user->id, $entity_id)) {
                if ($multiple_entities) {
                    $this->flashSession->success('Vaši oglasi su <strong>uspješno aktivirani</strong>.');
                } else {
                    $this->flashSession->success('Vaši oglas je <strong>uspješno aktiviran</strong>.');
                }
            } else {
                $this->flashSession->error('Neuspješan pokušaj aktivacije oglasa.');
            }
        } else {
            if ($multiple_entities) {
                $this->flashSession->error(sprintf('Oglasi nisu pronađeni [ID: %s]', implode(', ', explode(',', $entity_id))));
            } else {
                $this->flashSession->error(sprintf('Oglas nije pronađen [ID: %s]', $entity_id));
            }
        }

        return $this->redirect_back();
    }

    public function deaktivirajOglasAction($entity_id)
    {
        $continue_with_action = true;
        $multiple_entities    = true;

        if (strpos($entity_id, ',') === false) {
            $entity_id         = (int) $entity_id;
            $multiple_entities = false;
        }

        if (!$entity_id) {
            $this->flashSession->error('Nedostaje ID oglasa koji pokušavate deaktivirati!');
            $continue_with_action = false;
        }

        if ($continue_with_action) {
            if ($users_activated_ads = (new Ads())->deactivateUserAdsById($this->current_user->id, $entity_id)) {
                if ($multiple_entities) {
                    $this->flashSession->success('Vaši oglasi su <strong>uspješno deaktivirani</strong>.');
                } else {
                    $this->flashSession->success('Vaši oglas je <strong>uspješno deaktiviran</strong>.');
                }
            } else {
                $this->flashSession->error('Neuspješan pokušaj deaktivacije oglasa.');
            }
        } else {
            if ($multiple_entities) {
                $this->flashSession->error(sprintf('Oglasi nisu pronađeni [ID: %s]', implode(', ', explode(',', $entity_id))));
            } else {
                $this->flashSession->error(sprintf('Oglas nije pronađen [ID: %s]', $entity_id));
            }
        }

        return $this->redirect_back();
    }

    public function ponoviOglasAction($entity_id)
    {
        $continue_with_action = true;
        $multiple_entities    = true;

        if (strpos($entity_id, ',') === false) {
            $entity_id         = (int) $entity_id;
            $multiple_entities = false;
        }

        if (!$entity_id) {
            $this->flashSession->error('Nedostaje ID oglasa koji pokušavate ponoviti!');
            $continue_with_action = false;
        }

        if ($continue_with_action) {
            $users_republished_ads     = (new Ads())->republishUserAdsById($this->current_user->id, $entity_id);
            $republished_ads_count     = count($users_republished_ads['ok']);
            $not_republished_ads_count = count($users_republished_ads['nok']);

            if ($republished_ads_count) {
                if ($republished_ads_count > 1) {
                    $info = 'Vaši oglasi su <strong>uspješno ponovljeni</strong>.';
                } else {
                    $info = 'Vaš oglas je <strong>uspješno ponovljen</strong>.';
                }

                if ($not_republished_ads_count) {
                    if ($not_republished_ads_count > 1) {
                        $info .= '<br/><small>Neki od oglasa koje ste pokušali ponoviti ne zadovoljavaju uvjete za besplatno ponavljanje.</small>';
                    } else {
                        $info .= '<br/><small>Jedan oglas koji ste pokušali ponoviti ne zadovoljava uvjete za besplatno ponavljanje.</small>';
                    }
                }
                $this->flashSession->success($info);
            } elseif ($not_republished_ads_count) {
                $this->flashSession->error('Nijedan od označenih oglasa ne zadovolja uvjete za besplatno ponavljanje.');
            }
        } else {
            if ($multiple_entities) {
                $this->flashSession->error(sprintf('Oglasi nisu pronađeni [ID: %s]', implode(', ', explode(',', $entity_id))));
            } else {
                $this->flashSession->error(sprintf('Oglas nije pronađen [ID: %s]', $entity_id));
            }
        }

        return $this->redirect_to('moj-kutak/istekli-oglasi');
    }

    public function spremljeniOglasiAction()
    {
        $this->tag->setTitle('Moj kutak - Moji oglasi - Spremljeni oglasi');
        $this->view->pick('moj-kutak/favorite-ads');
        $resulting_array = $this->getPaginatedAds($this->current_user, 'favorite');
        if ($resulting_array) {
            if (!$resulting_array['error']) {
                $this->view->setVar('ad_listing_title', 'Spremljeni oglasi');
                $this->view->setVar('ads', $resulting_array['ads']);
                $this->view->setVar('results_total', $resulting_array['total']);
                $this->view->setVar('pagination_links', $resulting_array['pagination']);
                $this->assets->addJs('assets/js/my-account-favorited-ads.js');
            } elseif (isset($resulting_array['trigger_404']) && $resulting_array['trigger_404']) {
                return $this->trigger_404();
            }
        }
    }

    public function spremiBiljeskuAction($entity_id)
    {
        $this->disable_view();
        $this->response->setContentType('application/json', 'UTF-8');

        if (!$this->request->isAjax()) {
            $this->response->setStatusCode(400, 'Bad Request');
            return false;
        }

        $response_array = array('status' => false);

        $entity_id = (int) $entity_id;
        $continue_with_action = true;

        if (!$entity_id) {
            $response_array['msg'] = 'Nedostaje ID oglasa';
            $continue_with_action  = false;
        }

        if (!$this->request->isPost() || !$this->request->hasPost('note')) {
            $response_array['msg'] = 'Nedostaje bilješka za oglas';
            $continue_with_action  = false;
        }

        $note = $this->request->getPost('note', 'string', null);

        if ($continue_with_action && $this->current_user->save_favorite_note($entity_id, $note)) {
            $response_array['status'] = true;
        }

        $this->response->setJsonContent($response_array);
        return $this->response;
    }

    public function obrisiSpremljeniOglasAction($entity_id)
    {
        $continue_with_action = true;
        $multiple_entities    = true;

        if (strpos($entity_id, ',') === false) {
            $entity_id         = (int) $entity_id;
            $multiple_entities = false;
        }

        if (!$entity_id) {
            $this->flashSession->error('Nedostaje ID oglasa koji pokušavate obrisati iz spremljenih oglasa!');
            $continue_with_action = false;
        }

        if ($continue_with_action) {
            if ($remove_result = $this->current_user->remove_favorite($entity_id)) {
                if ($multiple_entities) {
                    $this->flashSession->success('Oglasi su <strong>uspješno obrisani</strong> iz popisa spremljenih oglasa.');
                } else {
                    $this->flashSession->success('Oglas je <strong>uspješno obrisan</strong> iz popisa spremljenih oglasa.');
                }
            } else {
                if (null === $remove_result) {
                    if ($multiple_entities) {
                        $this->flashSession->error(sprintf('Oglasi nisu pronađeni [ID: %s]', $entity_id));
                    } else {
                        $this->flashSession->error(sprintf('Oglas nije pronađen [ID: %s]', $entity_id));
                    }
                } else {
                    $this->flashSession->error('Neuspješan pokušaj brisanja oglasa iz popisa spremljenih oglasa.');
                }
            }
        }

        return $this->redirect_to('moj-kutak/spremljeni-oglasi');
    }

    public function otkaziNarudzbuAction($order_id)
    {
        $continue_with_action = true;
        $order_id = (int) $order_id;

        if (!$order_id) {
            $this->flashSession->error('Nedostaje ID narudžbe koju pokušavate otkazati!');
            $continue_with_action = false;
        }

        /** @var Orders $order */
        $order = Orders::findFirst(array('id = ?0 AND status = ?1', 'bind' => array($order_id, Orders::STATUS_NEW)));

        if (!$order) {
            $this->flashSession->error(sprintf('Narudžba nije pronađena [ID: %s]', $order_id));
            $continue_with_action = false;
        }

        if ($continue_with_action && !$this->is_user_owner($order, 'Ne možete otkazati narudžbu koja nije vaša')) {
            $continue_with_action = false;
        }

        if ($continue_with_action) {
            if ($order->markCancelled(array($order_id))) {
                $order->handleCancellationOfOrderItems();
                $this->flashSession->success('Vaša narudžba <strong>' . $order->getPbo() . '</strong> je <strong>uspješno otkazana</strong>.');
            } else {
                $this->flashSession->error('Neuspješan pokušaj otkazivanja narudžbe <strong>' . $order->getPbo() . '</strong>.');
            }
        }

        return $this->redirect_back();
    }

    public function podaciAction()
    {
        $this->tag->setTitle('Moj kutak - Moji oglasi - Uređivanje korisničkih podataka');

        $this->view->pick('moj-kutak/profile');
        $this->assets->addJs('assets/vendor/jquery.date-dropdowns.js');
        $this->assets->addJs('assets/js/my-account-user-details.js');

        /**
         * @var Users $user
         */
        $this->current_user = $this->auth->refresh_user();

        // Need to check for $user here too since we could've been logged-out in between requests
        // and someone might attempt refreshing an old POST request
        if ($this->request->isPost() && $this->current_user) {
            $saved = $this->current_user->frontend_save($this->request);
            if ($saved instanceof Users) {
                // Update session-backed user details to match newly saved data
                $this->auth->refresh_user();

                $this->flashSession->success('Vaši podaci su uspješno spremljeni!');
                return $this->redirect_self();
            } else {
                $this->view->setVar('errors', $saved);
                $this->flashSession->error('<strong>Greška!</strong> Ispravite sva crvena polja.');
            }
        }

        $this->country_dropdown();
        $this->county_dropdown();
        $this->view->setVar('user', $this->current_user);
    }

    protected function default_sorting_order_dropdown($shop)
    {
        if (isset($shop->default_sorting_order)) {
            Tag::setDefault('default_sorting_order', $shop->default_sorting_order);
        } else {
            Tag::setDefault('default_sorting_order', $this->config->adSorting->default_value);
        }

        $sorting_order_array = array();
        foreach ($this->config->adSorting->toArray() as $key => $value) {
            if ('default_value' !== $key) {
                $sorting_order_array[$key] = $value['name'];
            }
        }

        $default_sorting_order = Tag::select(array(
            'default_sorting_order',
            $sorting_order_array,
            'useEmpty' => false,
            'class' => 'form-control'
        ));

        $this->view->setVar('default_sorting_order_dropdown', $default_sorting_order);
    }

    protected function country_dropdown()
    {
        if (isset($this->current_user->country_id)) {
            Tag::setDefault('country_id', $this->current_user->country_id);
        } else {
            Tag::setDefault('country_id', 1);
        }

        $countries = Locations::findCountries();
        $selectables = array();
        foreach ($countries as $country) {
            $selectables[$country->id] = array(
                'text'       => $country->name,
                'attributes' => array(
                    'data-iso-code'     => $country->iso_code,
                    'data-calling-code' => $country->calling_code
                )
            );
        }

        $country_dropdown = Tag::select(array(
            'country_id',
            $selectables,
            'useEmpty' => true,
            'emptyText' => 'Odaberite državu...',
            'emptyValue' => '',
            'class' => 'form-control'
        ));
        $this->view->setVar('country_dropdown', $country_dropdown);
    }

    protected function county_dropdown()
    {
        if (isset($this->current_user->county_id)) {
            Tag::setDefault('county_id', $this->current_user->county_id);
        }

        $county_dropdown = Tag::select(array(
            'county_id',
            Locations::findCounties(),
            'using' => array('id', 'name'),
            'useEmpty' => true,
            'emptyText' => 'Odaberite županiju...',
            'emptyValue' => '',
            'class' => 'form-control'
        ));
        $this->view->setVar('county_dropdown', $county_dropdown);
    }

    public function porukeAction()
    {
        $this->tag->setTitle('Moj kutak - Moji podaci - Poruke');

        // Check paging parameter
        $page = 1;
        if ($this->request->hasQuery('page')) {
            $page = $this->request->getQuery('page', 'int', 1);
            if ($page <= 0) {
                $page = 1;
            }

            // Someone trying ?page=1 manually, redirect (avoiding duplicate content)
            if ('1' === $_GET['page']) {
                return $this->redirect_to('moj-kutak/poruke', 301);
            }
        }

        $builder = $this->current_user->getUserMessages($return_builder = true);
        $paginator = new QueryBuilder(array(
            'builder' => $builder,
            'limit'   => $this->config->settingsFrontend->pagination_items_per_page,
            'page'    => $page
        ));
        $current_page = $paginator->getPaginate();

        // Process the resulting page and extend with some additional data
        $results_array = UsersMessages::buildResultsArray($current_page->items);
        $current_page->items = $results_array;

        $pagination_links = Tool::pagination(
            $current_page,
            'moj-kutak/poruke',
            'pagination',
            $this->config->settingsFrontend->pagination_count_out,
            $this->config->settingsFrontend->pagination_count_in
        );

        $this->view->setVar('page', $current_page);
        $this->view->setVar('pagination_links', $pagination_links);
        $this->assets->addJs('assets/js/my-account-user-messages.js');
    }

    public function odgovoriNaPorukuAction($message_id)
    {
        $this->disable_view();
        $this->response->setContentType('application/json', 'UTF-8');
        $response_array = array('status' => false);

        $message = UsersMessages::findFirst(array(
            'id = :id: AND user_id = :user_id:',
            'bind' => array(
                'id'      => (int) $message_id,
                'user_id' => $this->current_user->id
            )
        ));

        if (!$message) {
            $response_array['msg'] = 'Nije pronađena poruka na koju pokušavate odgovoriti!';
            $this->response->setJsonContent($response_array);
            return $this->response;
        }

        // Process reply form
        if ($this->request->isPost()) {
            // If/when csrf checking fails, return an empty 403
            try {
                $this->check_csrf();
            } catch (CsrfException $ex) {
                return $this->do_403();
            }

            $validation_messages = new \Phalcon\Validation\Message\Group();
            $replyMessage = $this->request->getPost('replyMessage', 'string', null);
            if (empty($replyMessage)) {
                $abort = true;
                $validation_messages->appendMessage(new \Phalcon\Validation\Message('Molimo unesite poruku', 'replyMessage'));
            }

            if (!count($validation_messages)) {
                if ($message->sendReply($replyMessage)) {
                    $response_array['status'] = true;
                    $response_array['msg'] = 'Poruka uspješno poslana korisniku';
                } else {
                    $response_array['msg'] = 'Slanje poruke nije uspjelo (serverska greška)!';
                }
            } else {
                // Show errors
                $errors = array();
                foreach ($validation_messages as $message) {
                    $errors[] = array(
                        'field' => $message->getField(),
                        'value' => $message->getMessage()
                    );
                }
                $response_array['msg']    = "Oops!\nMolimo ispravite uočene greške.";
                $response_array['errors'] = $errors;
            }
        }

        $this->response->setJsonContent($response_array);
        return $this->response;
    }

    public function obrisiPorukuAction($message_id)
    {
        $message = UsersMessages::findFirst(array(
            'id = :id: AND user_id = :user_id:',
            'bind' => array(
                'id'      => (int) $message_id,
                'user_id' => $this->current_user->id
            )
        ));

        if (!$message) {
            $this->flashSession->error('<strong>Greška!</strong> Nije pronađena poruka koju pokušavate obrisati!');
            return $this->redirect_to('moj-kutak/poruke');
        }

        if ($message->delete()) {
            $this->flashSession->success('Poruka uspješno obrisana!');
        } else {
            $this->flashSession->error('Brisanje poruke bilo je neuspješno!');
        }

        return $this->redirect_to('moj-kutak/poruke');
    }

    protected function createPasswordValidation()
    {
        $validation = new Validation();

        $validation->add('pwd', new PresenceOf(array(
            'message' => 'Molimo upišite svoju trenutnu lozinku'
        )));
        $validation->add('pwd_new', new StringLength(array(
            'min'            => 6,
            'messageMinimum' => 'Nova željena lozinka je prekratka'
        )));
        $validation->add('pwd_new_repeat', new StringLength(array(
            'min'            => 6,
            'messageMinimum' => 'Ponovljena nova željena lozinka je prekratka'
        )));
        $validation->add('pwd_new', new Confirmation(array(
            'with'    => 'pwd_new_repeat',
            'message' => 'Nove (željene) lozinke nisu identične'
        )));

        return $validation;
    }

    /**
     * Handles users changing their passwords
     *
     * @throws \Baseapp\Extension\CsrfException
     */
    public function lozinkaAction()
    {
        $this->tag->setTitle('Moj kutak - Promjena lozinke');
        $this->view->pick('moj-kutak/profile');
        $show_form = true;

        if ($this->request->isPost()) {
            $validation = $this->createPasswordValidation();
            $errors     = $validation->validate($this->request->getPost());

            try {
                $this->check_csrf();
            } catch (CsrfException $e) {
                $errors->appendMessage(new Message($e->getMessage(), 'pwd'));
            }

            // Don't even bother if new and old pwd are identical
            if (!count($errors)) {
                $pwd            = $this->request->getPost('pwd');
                $pwd_new        = $this->request->getPost('pwd_new');
                $pwd_new_repeat = $this->request->getPost('pwd_new_repeat');
                if ($pwd === $pwd_new && $pwd_new === $pwd_new_repeat) {
                    $errors->appendMessage(new Message('Nova željena lozinka ne razlikuje se od trenutne', 'pwd_new'));
                }
            }

            // TODO: too much nesting!!!1
            if (count($errors)) {
                $this->flashSession->error('<strong>Ooops!</strong> Molimo ispravite uočene greške.');
                $this->view->setVar('errors', $errors);
            } else {
                /* @var Users $user */
                $user = $this->auth->get_user();
                if (!$user) {
                    $this->flashSession->error('<strong>Greška!</strong> Session je istekao? Molimo pokušajte ponovo.');

                    return $this->redirect_self();
                }

                // No basic errors so far, let's get on with the serious stuff...
                $pwd       = $this->request->getPost('pwd');
                $pwd_valid = $this->auth->check_hashed_password($pwd, $user->password);

                if (!$pwd_valid) {
                    // Lets prevent brute forcing other peoples password if they've left their account open somewhere
                    $max_attempts = 5;
                    $attempts     = $this->increasePwdChangeAttemptsCount();
                    $this->saveAudit(sprintf('User %s, failed password change attempt #%s', $this->auth->get_user()->ident(), $attempts));
                    if ($attempts > $max_attempts) {
                        $this->saveAudit(sprintf('User attempted another password change after reaching maximum allowed attempts (%s). Forcing logout.', $this->auth->get_user()->ident(), $max_attempts));
                        $this->flashSession->error('<strong>Greška!</strong> Previše neuspješnih pokušaja promjene lozinke. Automatski smo vas odjavili.');

                        return $this->dispatcher->forward(array(
                            'controller' => 'user',
                            'action'     => 'signout',
                        ));
                    }
                    $this->flashSession->error('<strong>Greška!</strong> Neispravna trenutna lozinka.');

                    return $this->redirect_self();
                } else {
                    // Current password matches the entered one, new passwords pass validations, lets do this!
                    $pwd_new        = $this->request->getPost('pwd_new');
                    $user->password = $this->auth->hash_password($pwd_new);
                    if (true === $user->update()) {
                        $show_form = false;
                        $this->saveAudit(sprintf('User %s changed their password', $this->auth->get_user()->ident()));
                        $this->flashSession->success('<strong>Super!</strong> Uspješno ste promjenili svoju lozinku.');
                        $user->send_pwdchanged_email();
                        $this->clearPwdChangeAttemptsCount();
                        // return $this->redirect_self();
                    } else {
                        $this->flashSession->error('<strong>Ups!</strong> Došlo je do greške prilikom spremanja podataka, molimo pokušajte ponovo.');
                        $this->view->setVar('errors', $user->getMessages());
                    }
                }
            }
        }

        $this->view->setVar('show_form', $show_form);
    }

    protected function getPwdChangeAttemptsCount()
    {
        return $this->session->get('pwd_change_attempts', 0);
    }

    protected function increasePwdChangeAttemptsCount()
    {
        $attempts = $this->getPwdChangeAttemptsCount();
        $attempts++;

        $this->session->set('pwd_change_attempts', $attempts);

        return $attempts;
    }

    protected function clearPwdChangeAttemptsCount()
    {
        return $this->session->remove('pwd_change_attempts');
    }

    public function trgovinaAction()
    {
        $user = $this->auth->get_user();
        $this->view->setVar('user', $user);

        /** @var UsersShops $shop */
        $shop = $user->Shop;
        $this->view->setVar('shop', $shop);

        if (!$shop) {
            // Show an up-sell page if the user doesn't have a store yet
            $this->view->pick('moj-kutak/profile');
            $this->view->setVar('trgovina_upsell', true);
            return;
        }

        $this->view->pick('moj-kutak/profile');
        // List non-deleted shopping windows
        $params = array(
            'deleted = 0',
            'order' => 'id DESC'
        );
        $this->view->setVar('featured_shops', $shop->getShoppingWindows($params));

        $this->tag->setTitle('Moj kutak - Trgovina');

        if ($this->request->isPost()) {
            $post_action = $this->request->getPost('action', 'string', null);

            switch ($post_action) {
                case 'create':
                    break;

                case 'edit':
                    $saved = $shop->frontend_save_changes($this->request);
                    if ($saved instanceof UsersShops) {
                        $this->flashSession->success('Promjene su uspješno spremljene!');
                        return $this->redirect_self();
                    } else {
                        $this->view->setVar('errors', $saved);
                        $this->flashSession->error('<strong>Greška!</strong> Ispravite sva crveno označena polja.');
                    }
                    break;
            }
        }
        $this->default_sorting_order_dropdown($shop);

        // Show the Shop's expiry status
        $shop_expiry_data = '<span class="text-warning">Čeka aktivaciju</span>';
        $now = Utils::getRequestTime();
        if ($shop->expires_at && $shop->expires_at > 0) {
            $expiry_date = date('d.m.Y.', $shop->expires_at);
            if ($shop->expires_at > $now) {
                $shop_expiry_data = '<span class="text-success">Aktivna do: ' . $expiry_date . '</span>';
            } else {
                $shop_expiry_data = '<span class="text-danger">Istekla: ' . $expiry_date . '</span>';
            }
        }
        $this->view->setVar('shop_expiry_data', $shop_expiry_data);
        $this->assets->addCss('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.css');
        $this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.js');
        $this->assets->addCss('assets/vendor/jquery-file-upload/jquery.fileupload.css');
        $this->assets->addJs('assets/vendor/jquery-file-upload/jquery.iframe-transport.js');
        $this->assets->addJs('assets/vendor/jquery-file-upload/jquery.fileupload.js');
        $this->assets->addJs('assets/vendor/ckeditor/ckeditor.js');
        $this->assets->addJs('assets/vendor/ckeditor/adapters/jquery.js');
        $this->assets->addJs('assets/js/my-account-user-shop.js');
    }

    protected function buildCategoriesDropdown($dropdown_name, $selectables = array(), $selected = null, $options = array())
    {
        // Prevent invalid choices from being set as default
        if ($selected) {
            if (is_array($selected)) {
                $selected_array = array();
                foreach ($selected as $sel) {
                    if (array_key_exists($sel, $selectables)) {
                        $selected_array[] = $sel;
                    }
                }
                if (count($selected_array) == 0) {
                    $selected = null;
                } else {
                    $selected = $selected_array;
                }
            } else {
                if (!array_key_exists($selected, $selectables)) {
                    $selected = null;
                }
            }
        }

        $dropdown_id = $dropdown_name;

        if (isset($options['multiple'])) {
            $dropdown_name = $dropdown_name . '[]';
        }

        // Preselected value, if there's a valid one
        if ($selected) {
            if (!is_array($selected)) {
                Tag::setDefault($dropdown_name, $selected);
            }
        }

        // Default options
        $defaults = array(
            $dropdown_name,
            $selectables,
            'class' => 'form-control',
            'id'    => $dropdown_id
        );

        if ($selected) {
            if (is_array($selected)) {
                $selected = implode(',', $selected);
            }
            $defaults['data-preselect'] = $selected;
        }

        // Merge passed in $options
        $options = array_merge($defaults, $options);

        // Build the dropdown
        $category_dropdown = Tag::select($options);
        $this->view->setVar($dropdown_id . '_dropdown', $category_dropdown);
    }

    public function trgovinaIzlogAction()
    {
        // If the current user doesn't have a shop, he can't do shopping windows either
        $shop = $this->current_user->Shop;
        if (!$shop) {
            // Redirecting to /trgovina for now, which is supposed to hold some up-sell for this?
            $this->flashSession->notice('<strong>Napomena!</strong> Kontaktirajte nas za otvaranje svoje trgovine!');
            return $this->redirect_to('trgovina');
        }

        $params = $this->router->getParams();
        if (empty($params)) {
            return $this->redirect_to('moj-kutak/trgovina');
        }

        $featured_shop_action_title = 'Novi izlog';
        $featured_shop_action       = trim($params[0]);
        $featured_shop_id           = null;
        $title_tag                  = 'Moj kutak - Trgovina - Novi izlog';
        $form_action                = 'create';

        if ($featured_shop_action == 'uredi') {
            if (isset($params[1]) && intval($params[1])) {
                $featured_shop_id = intval($params[1]);
                $form_action = 'edit';
                $featured_shop_action_title = 'Uredi izlog';
                $title_tag = 'Moj kutak - Trgovina - Uređivanje izloga';
                $featured_shop = UsersShopsFeatured::findFirst($featured_shop_id);
                if (!$featured_shop) {
                    $this->flashSession->error('<strong>Greška!</strong> Nepostojeći izlog (id: ' . $featured_shop_id . ')');
                    return $this->redirect_to('moj-kutak/trgovina');
                }
                $this->view->setVar('featured_shop_categories', explode(',', $featured_shop->categories));
            } else {
                $this->flashSession->error('<strong>Greška!</strong> Nedostaje id izloga');
                return $this->redirect_to('moj-kutak/trgovina');
            }
        } else {
            $featured_shop = new UsersShopsFeatured();
        }

        if ($this->request->isPost()) {
            if ($form_action == 'create') {
                $created = $featured_shop->frontend_add_new($this->request);
                if ($created instanceof UsersShopsFeatured) {
                    // Create an order for this shopping window and redirect to payment
                    $order = Orders::createShoppingWindowOrder($shop, $created, array('user_id' => $this->current_user->id));
                    if ($order instanceof Orders) {
                        $this->flashSession->success('<strong>Super!</strong> Vaš je izlog uspješno kreiran i biti će aktiviran nakon potvrde plaćanja.');
                        return $this->redirect_to('izlozi/payment/' . $order->id);
                    } else {
                        $errors = $order;
                        $this->flashSession->error('<strong>Greška!</strong> Došlo je do greške prilikom kreiranja narudžbe.');
                    }
                } else {
                    $errors = $created;
                    $errors_markup = $this->buildErrorsListMarkupFromValidationMessages($errors);
                    $this->flashSession->error('<strong>Greška!</strong> Spremanje nije uspjelo. ' . $errors_markup);
                }
            } else {
                $saved = $featured_shop->frontend_save_changes($this->request);
                if ($saved instanceof UsersShopsFeatured) {
                    $this->flashSession->success('Promjene uspješno spremljene');
                    return $this->redirect_to('moj-kutak/trgovina/izlog/uredi/' . $featured_shop_id);
                } else {
                    $errors = $saved;
                    $this->flashSession->error('<strong>Greška!</strong> Ispravite sva crveno označena polja.');
                }
            }

            if (isset($errors)) {
                $this->view->setVar('errors', $errors);
            }
        }

        $this->view->pick('moj-kutak/profile');
        $this->tag->setTitle($title_tag);

        $this->assets->addJs('assets/js/my-account-user-shop-featured.js');

        $this->handleViewDropdowns($featured_shop);

        $this->view->setVar('user', $this->current_user);
        $this->view->setVar('shop', $shop);
        $this->view->setVar('featured_shop', $featured_shop);
        $this->view->setVar('featured_shop_ads', $featured_shop->getAds($only_active_ads = false));
        $resulting_array = $this->getPaginatedAds($this->current_user, 'active');
        if ($resulting_array && !$resulting_array['error'] && isset($resulting_array['ads']) && count($resulting_array['ads'])) {
            $this->view->setVar('user_has_ads', true);
            $this->view->setVar('ads', $resulting_array['ads']);
            $this->view->setVar('results_total', $resulting_array['total']);
            $this->view->setVar('pagination_links', $resulting_array['pagination']);
        } else {
            $this->view->setVar('user_has_ads', false);
        }
        $this->view->setVar('form_action', $form_action);
        $this->view->setVar('featured_shop_action_title', $featured_shop_action_title);
    }

    public function filterClassifiedsAction($term = null)
    {
        $this->disable_view();
        $this->response->setContentType('application/json', 'UTF-8');
        $response_array = array('status' => false);
        $html = '';

        $resulting_array = $this->getPaginatedAds($this->current_user, 'active', null, null, $term);
        if ($resulting_array && !$resulting_array['error'] && isset($resulting_array['ads']) && count($resulting_array['ads'])) {
            $response_array['status'] = true;
            foreach ($resulting_array['ads'] as $ad) {
                $html .= $this->simpleView->render(
                    'moj-kutak/adslisting-simple-chunk',
                    array(
                        'ad' => $ad,
                    )
                );
            }
            if ($resulting_array['pagination']) {
                $html .= $resulting_array['pagination'];
            }
            $response_array['data'] = $html;
        } else {
            $response_array['data'] = 'Nema oglasa koji odgovaraju kriteriju pretrage!';
        }

        $this->response->setJsonContent($response_array);
        return $this->response;
    }

    protected function handleViewDropdowns(UsersShopsFeatured $featured_shop)
    {
        $model       = new Categories();
        $tree        = $this->getDI()->get(Categories::MEMCACHED_KEY);
        $selectables = $model->getSelectablesFromTreeArray(
            $tree,
            '-',
            $with_root = false,
            $depth = null,
            $breadcrumbs = true
        );

        $main_category_id        = $featured_shop->getMainCategory();
        $additional_category_ids = $featured_shop->getAdditionalCategories();

        // Allowing posted categories to override fetched stat (otherwise they're not present/correct when errors happen etc.)
        $posted_categories = $featured_shop->get_categories_from_request($this->request);
        if (!empty($posted_categories)) {
            // First element is the main cat, any others are additional category ids
            $posted_categories_array = explode(',', $posted_categories);
            if (!empty($posted_categories_array)) {
                if (isset($posted_categories_array[0]) && !empty($posted_categories_array[0])) {
                    $main_category_id = array_shift($posted_categories_array);
                    // If there's still stuff in exploded array after shifting, set that as additional category ids
                    if (count($posted_categories_array)) {
                        $additional_category_ids = $posted_categories_array;
                    }
                }
            }
        }

        $this->buildCategoriesDropdown('main_category_id', $selectables, $main_category_id, array(
            'useEmpty'         => true,
            'emptyText'        => 'Odaberite kategoriju',
            'emptyValue'       => '',
            'data-live-search' => true
        ));

        // Filtering $selectables down to only those initially required (can be empty too)
        $children = array();
        if (!empty($additional_category_ids)) {
            foreach ($additional_category_ids as $child_cat_id) {
                if (isset($selectables[$child_cat_id])) {
                    $children[$child_cat_id] = $selectables[$child_cat_id];
                }
            }
        }
        $this->buildCategoriesDropdown('additional_category_ids', $children, $additional_category_ids, array(
            'multiple'         => 'multiple',
            'data-max-options' => 2
        ));

        $this->view->setVar('main_category_id', $main_category_id);
        $this->view->setVar('additional_category_ids', $additional_category_ids);

        // FIXME: $tree appears to be used in trgovina-izlog.volt (and only for the .js part basically)
        $this->view->setVar('tree', $tree);
    }

    public function emailAgentiAction()
    {
        $this->view->pick('moj-kutak/email-agents');
        $this->view->setVar('email_agents', UsersEmailAgents::getAllForUser($this->auth->get_user()));
    }

    public function brisiEmailAgentaAction($email_agent_id)
    {
        $email_agent = UsersEmailAgents::findFirst(array(
            'conditions' => 'user_id = :user_id: AND id = :email_agent_id:',
            'bind' => array(
                'user_id'        => $this->auth->get_user()->id,
                'email_agent_id' => intval($email_agent_id)
            )
        ));

        if ($email_agent) {
            $email_agent_name = $email_agent->name;
            if ($email_agent->delete()) {
                $this->flashSession->success('<strong>Email agent "' . $email_agent_name . '" je uspješno obrisan!</strong>');
            } else {
                $this->flashSession->error('<strong>Greška!</strong> Email agent "' . $email_agent_name . '" nije moguće obrisati!');
            }
        } else {
            $this->flashSession->error('<strong>Greška!</strong> Nije pronađen Email agent koji pokušavate obrisati!');
        }

        return $this->redirect_to('moj-kutak/email-agenti');
    }

    public function uplatnicaAction($order_id = null)
    {
        $pdf    = $this->request->hasQuery('pdf');
        $print  = !$pdf;
        $errors = false;

        $order = Orders::findFirst($order_id);
        if (!$order) {
            $errors = true;
        }

        // Check the order belongs to the current user
        if (!$errors && $order->user_id != $this->current_user->id) {
            $errors = true;
        }

        if ($errors) {
            $this->do_403();
            return;
        }

        $this->view->setVar('order_id', $order->id);
        $this->view->pick('chunks/hub-3a');

        if ($print) {
            $pbo = $order->getPbo();
            $this->tag->setTitle('Print uplatnice: ' . $pbo);
            $this->view->setMainView('layouts/print');
            $barcode_png = $order->get2DBarcodeInlineImage();
            $this->view->setVar('bank_slip_markup', Orders::buildBankSlipMarkup($order, $barcode_png));
            $this->view->setVar('payment_data_table_markup', '');
            // Reset all the added assets so far and just add what's really needed
            $this->assets->set('js', new \Phalcon\Assets\Collection());
            $this->assets->set('css', new \Phalcon\Assets\Collection());
            $this->addBasicAssets();
        } else {
            // TODO: create pdf from rendered view data somehow and download it
            $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        }

        // Prepare offline payment table data
        //$barcode = $order->get2DBarcodeInlineSvg();
        //$this->view->setVar('payment_data_table_markup', Orders::buildPaymentDataTableMarkup($order, $barcode));

        /*$this->view->setVar('order_pbo', $pbo);
        $this->view->setVar('order_total_formatted', Utils::format_money($order->getTotal(), true));
        // $this->view->setVar('order_barcode_img_src', $order->get2DBarcodeInlineImage());
        $this->view->setVar('order_barcode_img_src', $order->get2DBarcodeInlineSvg());
        */
        // $barcode_img_src = $this->getHub3ImgBarcode($order, array('format' => $format));
    }

    public function avatarAction()
    {
        // Avoid empty POST requests without files etc...
        $empty = $this->do204ifEmptyEmptyPostWithoutFiles();
        if ($empty) {
            return;
        }

        $this->tag->setTitle('Moj kutak - Profil - Promjena avatara');
        $this->view->pick('moj-kutak/profile');

        // Handle avatar upload if we have something properly uploaded...
        if ($this->request->hasFiles()) {
            $uploader = $this->createUploader(['upload_base' => UsersProfileImages::getAvatarUploadsDir()]);
            if ($uploader->isValid()) {
                $info = $uploader->move();
                if (!empty($info)) {
                    $user_images = UsersProfileImages::findByUserId($this->current_user->id);
                    if (!$user_images) {
                        // Create a new record to hold the details
                        $user_images = new UsersProfileImages();
                        $user_images->user_id = $this->current_user->id;
                    }
                    foreach ($info as $k => $file) {
                        $user_images->assignNewAvatar($file['filename'], $file);
                    }
                }

                $this->flashSession->success('Avatar je uspješno spremljen!');
                return $this->redirect_back();
            } else {
                $this->view->setVar('errors', $uploader->getErrors());
            }
        }
    }

    public function coverbgAction()
    {
        // Avoid empty POST requests without files etc...
        $empty = $this->do204ifEmptyEmptyPostWithoutFiles();
        if ($empty) {
            return;
        }

        $this->tag->setTitle('Moj kutak - Profil - Promjena cover slike');
        $this->view->pick('moj-kutak/profile');

        // Handle avatar upload if we have something properly uploaded...
        if ($this->request->hasFiles()) {
            $uploader = $this->createUploader([
                'upload_base' => UsersProfileImages::getBgsUploadsDir(),
                'maxsize' => 512000
            ]);
            if ($uploader->isValid()) {
                $info = $uploader->move();
                if (!empty($info)) {
                    $user_images = UsersProfileImages::findByUserId($this->current_user->id);
                    if (!$user_images) {
                        // Create a new record to hold the details
                        $user_images = new UsersProfileImages();
                        $user_images->user_id = $this->current_user->id;
                    }
                    foreach ($info as $k => $file) {
                        $user_images->assignNewProfileBg($file['filename'], $file);
                    }
                }

                $this->flashSession->success('Cover slika je uspješno spremljena!');
                return $this->redirect_back();
            } else {
                $this->view->setVar('errors', $uploader->getErrors());
            }
        }
    }
}
