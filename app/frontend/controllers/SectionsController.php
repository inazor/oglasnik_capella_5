<?php

namespace Baseapp\Frontend\Controllers;

use Baseapp\Bootstrap;
use Baseapp\Library\LandingPagesHelpers;
use Baseapp\Models\UsersShopsFeatured;
use Phalcon\Dispatcher;
use Baseapp\Library\AdsFiltering;
use Baseapp\Models\Sections;

/**
 * Frontend Sections Controller
 */
class SectionsController extends IndexController
{
    /**
     * Overriding afterExecuteRoute in order to provide some global view variables.
     *
     * @param Dispatcher $dispatcher
     *
     * @return bool
     */
    public function afterExecuteRoute(Dispatcher $dispatcher)
    {
        // Making sure the parent is called too!
        parent::afterExecuteRoute($dispatcher);

        $this->add_default_sidebar_banner_zones();

        $this->add_banner_zone('mobile_top', '2285080', '<div id="zone2285080" class="goAdverticum"></div>');
        $this->add_banner_zone('mobile_pagination', '2285057', '<div id="zone2285057" class="goAdverticum"></div>');

        $this->add_banner_zone('listing_7', '2285047', '<div id="zone2285047" class="goAdverticum"></div>');
        $this->add_banner_zone('listing_14', '4084895', '<div id="zone4084895" class="goAdverticum"></div>');

        $this->view->setVar('banner_zone_ids', $this->get_active_zone_ids());
    }

    /**
     * View Action
     */
    public function viewAction()
    {
        $params      = $this->dispatcher->getParams();
        $section_url = implode('/', $params);

        if (!$section_url) {
            return $this->trigger_404();
        }

        $section = Sections::findFirstByUrl($section_url);

        if (!$section) {
            return $this->trigger_404();
        }

        // Setup Open Graph / SEO data
        $page_title = LandingPagesHelpers::buildTitle(array($section->getSEOMeta('title')));
        $this->tag->setTitle($page_title);
        $this->site_desc = $meta_desc = $section->getSEOMeta('description');
        $og_meta         = array(
            'og:title'       => $page_title,
            'og:url'         => $this->url->get($this->requested_uri), // contains scheme, host, etc
            'og:description' => $this->site_desc
        );
        $this->setOpenGraphMeta($og_meta);

        // Make sure our age checking is finished before doing anything else (unless we're
        // forwarded here, which means age restriction fulfillment has completed successfully
        //if (!$this->dispatcher->wasForwarded()) {
            $restricted = $this->ageRestrictionCheckRequest($section->getAgeRestriction(), $og_meta);
            if ($restricted) {
                // Just return since ageRestrictionAction() should've created/disabled the output
                return;
            }
        //}

        // Check if the section contains only a single category, and if so, 302 redirect to it
        $section_categories     = $section->getCategories();
        $section_categories_cnt = count($section_categories);
        if (1 === $section_categories_cnt) {
            /** @var Categories $cat */
            $cat = $section_categories[0];
            if ($cat) {
                return $this->redirect_to($cat->get_url());
            }
        }

        $page = 1;
        if ($this->request->hasQuery('page')) {
            $page = $this->request->getQuery('page', 'int', $page);
            if ($page <= 0) {
                $page = 1;
            }

            // Someone trying ?page=1 manually, redirect (avoiding duplicate content)
            if ('1' === $_GET['page']) {
                return $this->redirect_to($this->buildHref($section, $page = 1), 301);
            }
        }

        $sectionSelectedCategoryID = $this->request->getQuery('category_id', 'int', null);
        $sectionAllCategoryIDs     = $section->getCategoryIDs(true);

        $cacheResults  = true;
        $cacheLifeTime = 120;

        if ($shopping_windows_markup = UsersShopsFeatured::buildSectionShoppingWindowsMarkup($sectionAllCategoryIDs, $cacheResults, $cacheLifeTime)) {
            $this->assets->addCss('assets/vendor/slick/slick.css');
            $this->assets->addJs('assets/vendor/slick/slick.min.js');
        }
        $this->view->setVar('shopping_windows_markup', $shopping_windows_markup);

        $this->view->setVar('is_first_page', ($page == 1));

        $adsFiltering = new AdsFiltering();
        $adsFiltering->cacheResults($cacheResults);
        $adsFiltering->cacheLifeTime($cacheLifeTime);
        $adsFiltering->cachePrefix('section-' . $section->id);
        $adsFiltering->sectionFiltering(true);
        $adsFiltering->setLoggedInUser($this->auth->get_user());
        $adsFiltering->setSortParamData($this->getSortParamData());
        $adsFiltering->setAdCategoriesIDs($sectionAllCategoryIDs);
        if ($sectionSelectedCategoryID) {
            $adsFiltering->setAdCategoryByID($sectionSelectedCategoryID, false);
        }
        $adsFiltering->setBaseURL($section->get_url());
        $adsFiltering->allowSpecialProducts(false);
        $adsFiltering->showOnlyRootCategoriesCounts(false);
        $adsFiltering->alwaysShowCompleteCategoriesTree(true);

        $ads = $adsFiltering->getAds();
        if ($flashSession = $adsFiltering->getFlashSession()) {
            $this->flashSession->$flashSession['type']($flashSession['text']);
        }
        if ($adsFiltering->shouldTrigger404()) {
            return $this->trigger_404();
        }
        $specialProductsArray = $adsFiltering->getSpecialProducts($hydrate = true, $limit = 3, $random = true);
        if (!$adsFiltering->hasError()) {
            $this->buildSortingDropdown();
            if ($specialProductsArray) {
                if (is_array($ads) && !empty($ads)) {
                    $ads = array_merge($specialProductsArray, $ads);
                }
            }
            $this->view->setVar('ads', $ads);
            $totalAdsCount = $adsFiltering->getTotalAdsCount();
            $this->view->setVar('total_items', $totalAdsCount);
            if ($pagination = $adsFiltering->getPagination()) {
                $this->view->setVar('pagination_links_top', $pagination['top']);
                $this->view->setVar('pagination_links', $pagination['bottom']);
            }
            if ($generatedViewVars = $adsFiltering->getGeneratedViewVars()) {
                foreach ($generatedViewVars as $varTitle => $varValue) {
                    $this->view->setVar($varTitle, $varValue);
                }
            }
            $this->assets->addJs('assets/js/sections-view.js');
            // set the appropriate ads listing viewType (grid/list)
            $this->view->setVar('viewType', $this->getViewTypeData());
        }
        $this->view->setVar('section', $section);
    }

    protected function buildHref($section, $page = null)
    {
        $query = $this->request->getQuery();
        unset($query['_url']);

        $page = (int) $page;

        if ($page >= 2) {
            $query['page'] = $page;
        }

        // having ?page=1 is not needed and creates duplicate content
        if (1 === $page) {
            unset($query['page']);
        }

        return $section->get_url($query);
    }

}
