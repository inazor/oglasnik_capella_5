<?php

namespace Baseapp\Frontend\Controllers;
use Baseapp\Models\Ads;
use Baseapp\Models\Users;
use Baseapp\Models\UsersShops;
use Baseapp\Models\UsersFavoriteAds;
use Baseapp\Models\UsersMessages;
use Baseapp\Models\UsersEmailAgents;
use Baseapp\Models\Dictionaries;
use Baseapp\Models\Locations;
use Baseapp\Models\Media;
use Baseapp\Models\Tags;
use Baseapp\Models\SearchQueries;
use Baseapp\Library\Tool;
use Baseapp\Library\OEmbedUtils;
use Phalcon\Http\Response;
use Phalcon\Paginator\Adapter\QueryBuilder;

/**
 * Ajax Frontend Controller
 */
class AjaxController extends IndexController
{
    /**
     * @var \Phalcon\Http\Response
     */
    public $response;

    private $_allowOnlyAjax = true;

    /**
     * Initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->response = new Response();
        $this->response->setContentType('application/json', 'UTF-8');
        if ($this->app->config->app->env === 'development') {
            $this->_allowOnlyAjax = false;
        }

        // Hopefully disables the view for all of the controller's actions in a single place
        $this->disable_view();
    }

    /**
     * Test if current request is actually an AJAX request
     * @return boolean
     */
    private function _isAjaxRequest()
    {
        if ($this->_allowOnlyAjax) {
            if (!$this->request->isAjax()) {
                $this->response->setStatusCode(400, 'Bad Request');
                return false;
            }
        }
        return true;
    }

    /**
     * Sets a 404 response status code and a JSON encoded response content
     * (with an optional custom message)
     *
     * @param string $custom_message
     */
    private function _notFound($custom_message = '')
    {
        $response_array = array('status' => false);

        $msg = trim($custom_message);
        if (!empty($msg)) {
            $response_array['msg'] = trim($msg);
        }

        $this->response->setStatusCode(404, 'Not found');
        $this->response->setJsonContent($response_array);
    }

    /**
     * Find counties of a specific country
     * @return \Phalcon\Http\Response JSON encoded response
     */
    public function countiesAction()
    {
        if ($this->_isAjaxRequest()) {
            $params = $this->router->getParams();
            if (isset($params[0]) && $country_id = $params[0]) {
                $locations = Locations::findCounties($country_id);
                $this->response->setJsonContent($locations->toArray());
            }
        }

        return $this->response;
    }

    /**
     * Returns a JSON encoded response containing locations example data
     * @return \Phalcon\Http\Response JSON encoded response
     */
    public function locationAction()
    {
        if ($this->_isAjaxRequest()) {
            $params = $this->router->getParams();

            if (!isset($params[0])) {
                $this->_notFound('Should I start from 0?');
            }

            switch ($params[0]) {
                case 'getExampleTreeValues':
                    $this->set_location_example_data();
                    break;
                default:
                    $location_id = (int) $params[0];
                    if ($location_id) {
                        $location = Locations::findFirst($location_id);
                    }

                    if (!isset($location) || !$location) {
                        $this->_notFound('Requested location not found');
                    } else {
                        // Return the location record by default
                        $location = Locations::findFirst($location_id);
                        $result   = $location;

                        // Check if something else was requested ('ajax/location/$id/values' only for now?)
                        $requested_value = isset($params[1]) ? trim($params[1]) : null;
                        if (!empty($requested_value)) {
                            if (method_exists($location, $requested_value)) {
                                // Found a method matching the requested name
                                $result = $location->$requested_value();
                            } elseif (isset($location->{$requested_value})) {
                                // Found a model property matching the name
                                $result = $location->{$requested_value};
                            } else {
                                // Nothing found
                                $this->_notFound('The requested location thing does not exist');
                                unset($result);
                            }
                        }

                        // $result could've been unset above
                        // but if it's not, lets check if $result implements 'toArray()' and call it
                        if (isset($result)) {
                            if (method_exists($result, 'toArray')) {
                                $result = $result->toArray();
                            }

                            $this->response->setJsonContent($result);
                        }
                    }
                    break;
            }
        }

        return $this->response;
    }

    private function set_location_example_data()
    {
        $examples = array(
            array('Countries'),
            array('Counties'),
            array('Cities'),
            array('Municipalities'),
            array('Point (Lat/Lng)')
        );
        $this->response->setJsonContent($examples);
    }

    /**
     * Returns dictionary data in a JSON encoded response (if found) or a 404
     * @return \Phalcon\Http\Response
     */
    public function dictionaryAction()
    {
        if ($this->_isAjaxRequest()) {
            $params = $this->router->getParams();

            if (!isset($params[0]) || empty($params[0])) {
                $this->_notFound('Missing required parameter');
            }

            $dictionary_id = (int) $params[0];
            $dictionary = Dictionaries::findFirst($dictionary_id);
            $result = $dictionary;

            if (!$result) {
                $this->_notFound('Dictionary does not exist');
            }

            $requested_value = isset($params[1]) ? trim($params[1]) : null;
            if (!empty($requested_value)) {
                if (method_exists($dictionary, $requested_value)) {
                    // Found a method matching the requested name
                    $result = $dictionary->$requested_value();
                } elseif (isset($dictionary->{$requested_value})) {
                    // Found a model property matching the name
                    $result = $dictionary->{$requested_value};
                } else {
                    // Nothing found but a specific thing was requested
                    $this->_notFound('The requested dictionary thing does not exist');
                    unset($result);
                }
            }

            if (isset($result)) {
                if (method_exists($result, 'toArray')) {
                    $result = $result->toArray();
                }

                $this->response->setJsonContent($result);
            }
        }

        return $this->response;
    }

    /**
     * Returns a JSON encoded response for ajax-based users search (404 if nothing found)
     * @return \Phalcon\Http\Response
     */
    public function usersAction()
    {
        if ($this->_isAjaxRequest()) {
            $params = $this->router->getParams();
            if (isset($params[0]) && $search_string = trim($params[0])) {
                $type = null;
                if (isset($_GET['type']) && intval($_GET['type'])) {
                    $type = intval($_GET['type']);
                }

                $found_users = Users::get_suggest_values($search_string, $type);

                if ($found_users && count($found_users)) {
                    $users_array = array();

                    foreach($found_users as $user) {
                        $full_name = null;
                        $user_icon = 'user';
                        $user_type = 'Users';
                        if (intval($user->type) == 1) {
                            $user_icon = 'user';
                            $user_type = 'Users';
                            if ($tmp_name = trim(trim($user->first_name) . ' ' . trim($user->last_name))) {
                                $full_name = $tmp_name;
                            }
                        } else {
                            $user_icon = 'globe';
                            $user_type = 'Companies';
                            if ($tmp_name = trim($user->company_name)) {
                                $full_name = $tmp_name;
                            }
                        }
                        if (!empty($user->title)) {
                            $user_icon = 'shopping-cart';
                            $user_type = 'Shop';
                            $full_name = $user->title;
                        }
                        $users_array[] = array(
                            'id'        => $user->id,
                            'icon'      => $user_icon,
                            'type'      => $user_type,
                            'remark'    => $user->remark,
                            'username'  => trim($user->username),
                            'full_name' => $full_name,
                            'email'     => $user->email
                        );
                    }
                    $this->response->setJsonContent($users_array);
                } else {
                    $this->_notFound('No user found for this search term?!');
                }
            } else {
                $this->_notFound('Should I guess who you want?');
            }
        }

        return $this->response;
    }

    public function userInfoAction($username)
    {
        if ($this->_isAjaxRequest()) {
            $response_array = array('status' => false);
            if (trim($username)) {
                $userInfo = Users::findFirst(array(
                    'conditions' => 'username = :username:',
                    'bind'       => array(
                        'username' => trim($username)
                    )
                ));

                if ($userInfo) {
                    $response_array['status'] = true;
                    $response_array['data'] = $userInfo->get_public_details();
                } else {
                    $response_array['msg'] = 'No user found!';
                }
            } else {
                $response_array['msg'] = 'No username given';
            }

            $this->response->setJsonContent($response_array);
        }

        return $this->response;
    }

    /**
     * Handles ajax-based upload requests and returns a JSON encoded response
     * with the uploaded file(s) data.
     *
     * @return \Phalcon\Http\Response
     */
    public function uploadAction()
    {
        if ($this->_isAjaxRequest()) {
            $media = new Media();
            $results = $media->handle_upload($this->request);
            $this->response->setJsonContent($results);
        }

        return $this->response;
    }

    public function availableTagsAction($search_term = null)
    {
        if ($this->_isAjaxRequest()) {
            $tags = Tags::getSuggestions($search_term);
            $this->response->setJsonContent($tags);
        }

        return $this->response;
    }

    public function favoriteAdAction($ad_id, $action = 'set')
    {
        if ($this->_isAjaxRequest()) {
            $response_array = array('status' => false);

            if ($user = $this->auth->get_user()) {
                if ($action == 'set') {
                    $result = $user->make_favorite($ad_id);
                } elseif ($action == 'remove') {
                    $result = $user->remove_favorite($ad_id);
                }
                if ($result) {
                    $response_array['status'] = true;
                } else {
                    $response_array['msg'] = 'AddToFavorites error';
                    $response_array['data'] = array(
                        'logged_in' => true
                    );
                }
            } else {
                $response_array['msg'] = 'Not logged in';
                $response_array['data'] = array(
                    'logged_in' => false
                );
            }

            $this->response->setJsonContent($response_array);
        }

        return $this->response;
    }

    public function favoritesAction($ids = null)
    {
        if (null !== $ids && !is_array($ids)) {
            $ids = array_map('trim', explode(',', $ids));
        }

        if (!$this->_isAjaxRequest()) {
            $this->response->setStatusCode(400, 'Bad Request');
            return $this->response;
        }

        if (empty($ids)) {
            $this->response->setStatusCode(400, 'Bad Request');
            return $this->response;
        }

        // If we have no user, return an empty 200 ok
        $user = $this->auth->get_user();
        if (!$user) {
            return $this->response;
        }

        $list = UsersFavoriteAds::checkIdsForUser($ids, $user->id);
        $this->response->setJsonContent($list);

        return $this->response;
    }

    public function readMessageAction($message_id)
    {
        if ($this->_isAjaxRequest()) {
            $response_array = array('status' => false);

            if ($user = $this->auth->get_user()) {

                $continue_with_action = true;
                $multiple_messages    = true;

                if (strpos($message_id, ',') === false) {
                    $message_id        = (int) $message_id;
                    $multiple_messages = false;
                }

                if (!$message_id) {
                    $continue_with_action = false;
                }

                if ($continue_with_action) {
                    $markedAsRead = $user->markMessageAsReadById($message_id);
                    if ($markedAsRead) {
                        $response_array['status'] = true;
                        $response_array['count'] = $user->getUnreadNotificationsCount();
                    } else {
                        $response_array['msg'] = 'Message could not be marked as read!';
                    }
                }
            } else {
                $response_array['msg'] = 'Not logged in';
                $response_array['data'] = array(
                    'logged_in' => false
                );
            }

            $this->response->setJsonContent($response_array);
        }

        return $this->response;
    }

    public function getUserEmailAgentAction()
    {
        if ($this->_isAjaxRequest()) {
            $response_array       = array('status' => false);
            $email_agent_name     = $this->request->getPost('name', 'string', null);

            if ($user = $this->auth->get_user()) {
                $users_email_agent = UsersEmailAgents::getEmailAgentForUserByName($user, $email_agent_name);
                if (!$users_email_agent) {
                    $response_array['status'] = true;
                } else {
                    $response_array['msg'] = 'Već postoji Email agent s tim imenom';
                }
            } else {
                $response_array['msg'] = 'Niste prijavljeni';
                $response_array['data'] = array(
                    'logged_in' => false
                );
            }

            $this->response->setJsonContent($response_array);
        }

        return $this->response;
    }

    public function saveUserEmailAgentAction()
    {
        if ($this->_isAjaxRequest()) {
            $response_array       = array('status' => false);
            $email_agent_name     = $this->request->getPost('name', 'string', null);
            $email_agent_settings = $this->request->getPost('settings', 'string', null);

            if ($email_agent_settings) {
                if ($user = $this->auth->get_user()) {
                    if (!$email_agent_name) {
                        $response_array['msg'] = 'Morate dati ime svojem Email agentu!';
                    } else {
                        if ($email_agent = UsersEmailAgents::create_new($user, $this->request)) {
                            $response_array['status'] = true;
                        } else {
                            $response_array['msg'] = 'Email agent nije mogao biti stvoren';
                        }
                    }
                } else {
                    $response_array['msg'] = 'Niste prijavljeni';
                    $response_array['data'] = array(
                        'logged_in' => false
                    );
                }
            } else {
                $response_array['msg'] = 'Morate odabrati parametre pretrage da bi spremili vašeg Email agenta';
            }

            $this->response->setJsonContent($response_array);
        }

        return $this->response;
    }

    public function searchAutocompleteAction($search_term = null)
    {
        if ($this->_isAjaxRequest()) {
            $suggestions = SearchQueries::getSuggestions($search_term);
            if (count($suggestions) == 0) {
                $suggestions = array();
            }

            $this->response->setJsonContent($suggestions);
        }

        return $this->response;
    }

    public function searchShopsAutocompleteAction($searchTerm = null)
    {
        if ($this->_isAjaxRequest()) {
            $suggestions = UsersShops::getSuggestions($searchTerm);
            if (count($suggestions) == 0) {
                $suggestions = array();
            }

            $this->response->setJsonContent($suggestions);
        }

        return $this->response;
    }

    public function checkVideoUrlAction()
    {
        if ($this->_isAjaxRequest()) {
            $response_array = array('status' => false);
            $verified_url   = OEmbedUtils::parse_youtube_url($this->request->getPost('url'), $return_html = false);

            if ($verified_url) {
                $response_array['status'] = true;
                $response_array['url']    = $verified_url;
            }

            $this->response->setJsonContent($response_array);
        }

        return $this->response;
    }

    public function shareViaEmailAction()
    {
        if ($this->_isAjaxRequest()) {
            $response_array = array('status' => false);
            $this->check_csrf();

            $abort               = false;
            $validation_messages = null;

            // Anon users need a verified captcha response
            if ($this->auth->logged_in()) {
                $auth_user = $this->auth->get_user();
            }
            if (empty($auth_user)) {
                $g_response = $this->request->getPost('g-recaptcha-response');
                $validation_messages = new \Phalcon\Validation\Message\Group();
                if (empty($g_response)) {
                    $abort = true;
                    $validation_messages->appendMessage(new \Phalcon\Validation\Message('Molimo potvrdite da niste robot', 'captcha-email-share'));
                }
                if (!$abort) {
                    $recaptcha = new \ReCaptcha\ReCaptcha($this->recaptcha_secret);
                    $response  = $recaptcha->verify($g_response, $this->request->getClientAddress(true));
                    if (!$response->isSuccess()) {
                        $abort = true;
                        foreach ($response->getErrorCodes() as $code) {
                            $validation_messages->appendMessage(new \Phalcon\Validation\Message('Captcha greška: ' . $code, 'captcha-email-share'));
                        }
                    }
                }
            }

            // Don't even bother doing our validations if recaptcha failed
            if (!$abort) {
                $validation          = new \Baseapp\Library\Validations\EmailShareForm();
                $validation_messages = $validation->validate($this->request->getPost());
            }

            if (!count($validation_messages)) {
                $sender_email = $this->request->getPost('sender_email', 'email', null);
                $sender_name  = null;
                if ($sender = $this->request->hasPost('sender_id') ? Users::findFirst($this->request->getPost('sender_id')) : null) {
                    $sender_name = $sender->getDisplayName();
                }

                $sendOK = false;
                if ($sender_email) {
                    $recipients = $this->request->getPost('recipients', 'string', null);
                    if ($recipients) {
                        if (strpos($recipients, PHP_EOL)) {
                            $recipients = explode(PHP_EOL, $recipients);
                        } elseif (strpos($recipients, ",")) {
                            $recipients = explode(',', $recipients);
                        } else {
                            $recipients = array($recipients);
                        }

                        if ($ad_id = $this->request->getPost('ad_id', 'int', null)) {
                            if ($ad = Ads::findFirst($ad_id)) {
                                $title = $ad->title;
                                foreach ($recipients as $recipient) {
                                    if ($recipient = trim(str_replace(array("\r", "\n"), '', $recipient))) {
                                        $email = new \Baseapp\Library\Email();
                                        $email->prepare(
                                            $title,
                                            $recipient,
                                            'emailshare',
                                            array(
                                                'title'    => $title,
                                                'type'     => 'ad',
                                                'ad'       => $ad,
                                                'sender' => ($sender_name ? $sender_name . ' (' . $sender_email . ')' : $sender_email)
                                            )
                                        );
                                        if ($email->Send()) {
                                            $sendOK = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if ($sendOK) {
                    $response_array['status'] = true;
                    $response_array['msg'] = 'Poruka uspješno poslana primateljima';
                } else {
                    $response_array['msg'] = 'Slanje poruke nije uspjelo (serverska greška)!';
                }
            } else {
                // Show errors
                $errors = array();
                foreach ($validation_messages as $message) {
                    $errors[] = array(
                        'field' => $message->getField(),
                        'value' => $message->getMessage()
                    );
                }
                $response_array['msg']    = '<h2>Oops!</h2><p>Molimo ispravite uočene greške.</p>';
                $response_array['errors'] = $errors;
            }

            $this->response->setJsonContent($response_array);
        }

        return $this->response;
    }

    public function getNearByAction()
    {
        if ($this->_isAjaxRequest()) {
            $response_array = array('status' => false);
            if ($ad_id = $this->request->getPost('classifiedId', 'int', null)) {
                if ($ad = Ads::findFirst($ad_id)) {
                    if ($bounds = $this->request->getPost('bounds')) {
                        $response_array['status']      = true;
                        $response_array['classifieds'] = $ad->getNearByOnMap(json_decode($bounds));
                    }
                }
            }

            $this->response->setJsonContent($response_array);
        }

        return $this->response;
    }

    /**
     * Get suggested locations based on given term
     */
    public function getSuggestedLocationsAction($term = null)
    {
        if ($this->_isAjaxRequest()) {
            $term           = trim($term);
            $response_array = array('status' => false);
            $locations      = null;

            if ($term) {
                // search locations by specific term
                $locations = Locations::autosuggest($term, 2);
            } else {
                // return list of second level locations (Croatian counties)
                $locations = Locations::findCounties();
            }

            if ($locations) {
                $response_array['status'] = true;
                $response_array['data']   = $locations->toArray();
            }

            $this->response->setJsonContent($response_array);
        }

        return $this->response;
    }

}
