<?php

namespace Baseapp;

use Baseapp\Traits\CommonServiceLoaders;
use Phalcon\Di;
use Phalcon\DiInterface;

/**
 * Console
 */
class Console extends \Phalcon\CLI\Console
{
    use CommonServiceLoaders;

    /**
     * Console constructor - set the dependency Injector
     *
     * @param DiInterface $di
     */
    public function __construct(DiInterface $di)
    {
        mb_internal_encoding('utf-8');

        $this->_di = $di;

        $service_loaders = array(
            'config',
            'loader',
            'timezone',
            'logger',
            'shutdown_manager',
            'messages_collector',
            'error_handling',
            // 'assets',
            'db',
            'dispatcher',
            'events_manager',
            'cache',
            'crypt',
            'security',
            'cookies',
            'session',
            'auth',
            'router',
            'url',
            'filter',
            'models_metadata',
            'view_counts_manager',
            'beanstalk'
        );

        // Register/initialize services
        foreach ($service_loaders as $service) {
            $this->$service();
        }

        // Register modules
        $this->registerModules(array(
            'cli' => array(
                'className' => 'Baseapp\Cli\Module',
                'path' => APP_DIR . '/cli/Module.php'
            ),
        ));

        // Register the app itself as a service
        $this->_di->set('app', $this);

        // Set the dependency Injector
        parent::__construct($this->_di);
    }

    /**
     * Override default HMVC request for the CLI
     *
     * @param array $location Location to run the request against/on
     *
     * @return mixed response
     */
    public function request($location)
    {
        $dispatcher = clone $this->getDI()->get('dispatcher');

        if (isset($location['task'])) {
            $dispatcher->setTaskName($location['task']);
        } else {
            $dispatcher->setTaskName('main');
        }

        if (isset($location['action'])) {
            $dispatcher->setActionName($location['action']);
        } else {
            $dispatcher->setActionName('main');
        }

        if (isset($location['params'])) {
            if (is_array($location['params'])) {
                $dispatcher->setParams($location['params']);
            } else {
                $dispatcher->setParams((array) $location['params']);
            }
        } else {
            $dispatcher->setParams(array());
        }

        $dispatcher->dispatch();
        return $dispatcher->getReturnedValue();
    }

    /**
     * Set the CLI router service, where we're using Phalcon's default CLI router
     *
     * @return void
     */
    protected function router()
    {
        $this->_di->set('router', function() {
            $router = new \Phalcon\CLI\Router();
            return $router;
        });
    }

    /**
     * Handles the command-line arguments
     *
     * @param array|null $arguments
     * @return void
     */
    public function handle(array $arguments = null)
    {
        $params = array();
        switch (count($arguments)) {
            case 1:
                $task = 'main';
                $action = 'main';
                break;
            case 2:
                $task = $arguments[1];
                $action = 'main';
                break;
            case 3:
                $task = $arguments[1];
                $action = $arguments[2];
                break;
            default:
                $task = $arguments[1];
                $action = $arguments[2];
                $params = array_slice($arguments, 3);
                break;
        }
        $arguments = array_merge(array('module' => 'cli', 'task' => $task, 'action' => $action), $params);
        parent::handle($arguments);
    }

    /**
     * Logs the exception, prints the exception message and exits with an error status.
     * Additional exception details are printed if debug flag is set via config
     *
     * @param \Exception $e
     */
    public static function exception(\Exception $e)
    {
        // Log always
        Bootstrap::log($e);

        // Display debug output if desired
        $config = Di::getDefault()->getShared('config');
        if ($config->app->debug) {
            $tpl = "Exception: %s in %s on line %s\n%s";
            $msg = sprintf($tpl, $e->getMessage(), $e->getFile(), $e->getLine(), $e->getTraceAsString());
        } else {
            $msg = $e->getMessage();
        }

        static::error($msg);
    }

    public static function error($msg)
    {
        print $msg . "\n";
        exit(1);
    }
}
