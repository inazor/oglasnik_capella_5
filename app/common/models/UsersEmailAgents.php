<?php

namespace Baseapp\Models;

use Baseapp\Library\Parameters\Parametrizator;
use Baseapp\Library\Utils;
use Phalcon\Di;
use Phalcon\Mvc\View;

class UsersEmailAgents extends BaseModelBlamable
{
    public $id;
    public $user_id;
    public $name;
    public $settings;
    public $active;

    public function initialize()
    {
        parent::initialize();

        $this->belongsTo(
            'user_id', __NAMESPACE__ . '\Users', 'id',
            array(
                'alias'      => 'User',
                'reusable'   => true,
            )
        );
    }

    public function getSource()
    {
        return 'users_email_agents';
    }

    public static function getEmailAgentForUserByName(Users $user, $email_agent_name)
    {
        $email_agent = self::findFirst(array(
            'conditions' => 'user_id = :user_id: AND LOWER(name) = :email_agent_name:',
            'bind' => array(
                'user_id'          => $user->id,
                'email_agent_name' => trim(mb_strtolower($email_agent_name, 'UTF-8'))
            )
        ));

        return $email_agent;
    }

    public static function create_new(Users $user, $request)
    {
        $settings = $request->getPost('settings');

        // first try to get users email agent by its settings... if we find one,
        // basically, we'll only rename it (set its name property) :)
        //
        // this way, we prevent multiple agents with same results!

        $email_agent = self::findFirst(array(
            'conditions' => 'user_id = :user_id: AND settings = :settings:',
            'bind' => array(
                'user_id'  => $user->id,
                'settings' => $settings
            )
        ));

        if (!$email_agent) {
            $email_agent = new self();
        }

        $email_agent->user_id = $user->id;
        $email_agent->name = $request->getPost('name', 'string', null);
        $email_agent->settings = $request->getPost('settings');
        $email_agent->active = 1;

        return $email_agent->save();
    }

    public static function getAllForUser(Users $user)
    {
        $email_agents = null;

        $rows = self::find(array(
            'conditions' => 'user_id = :user_id:',
            'bind' => array(
                'user_id'          => $user->id
            )
        ));

        if ($rows) {
            $email_agents = array();

            // Init the category tree for results decoration (only once in case of repeated calls)
            static $cat_tree = null;
            if (null === $cat_tree) {
                $cat_tree = Di::getDefault()->get(Categories::MEMCACHED_KEY);
            }

            foreach ($rows as $row) {
                $query_string = json_decode($row->settings, true);
                $query_string['ea_id'] = $row->id;
                $category_id = null;
                if (isset($query_string['category_id'])) {
                    $category_id = $query_string['category_id'];
                    unset($query_string['category_id']);
                }
                if ($category_id && $query_string) {
                    $category = Categories::findFirst($category_id);

                    $email_agent = $row->toArray();
                    $email_agent['view_url'] = $category->get_url($query_string);
                    $email_agent['settings'] = '';
                    $email_agent['category'] = null;
                    $cat = isset($cat_tree[$category_id]) ? $cat_tree[$category_id] : null;
                    if ($cat) {
                        $email_agent['category'] = $cat->path['text'];
                    }

                    $email_agents[] = $email_agent;
                }
            }
        }

        return $email_agents;
    }

    public static function process()
    {
        // TODO: we should somehow chunk this entire process so we won't overload the smtp server

        // get all active email agents
        $builder = new \Phalcon\Mvc\Model\Query\Builder();
        $builder->columns(array(
            'email_agent.*',
            'user.*'
        ));
        $builder->addFrom('Baseapp\Models\UsersEmailAgents', 'email_agent');
        $builder->innerJoin('Baseapp\Models\Users', 'email_agent.user_id = user.id', 'user');
        $builder->where('email_agent.active = 1');
        $builder->orderBy('email_agent.id ASC');

        $rows = $builder->getQuery()->execute();

        if ($rows) {
            foreach ($rows as $row) {
                $email_agent = $row->email_agent;
                $user        = $row->user;

                // as email agents will be run every day after the midnight, we
                // should find all new ads (published during the previous day)
                // that fulfill $email_agent's settings

                if ($new_ads = $email_agent->getNewAds()) {

                    // create new queue record
                    $queue = new UsersEmailAgentsQueue();
                    $queue->email_agent_id = $email_agent->id;
                    $queue->recipient_email = $user->email;
                    $queue->subject = 'Oglasnik.hr - Email agent "' . $email_agent->name . '"';
                    $queue->body = self::generateMarkup($user, $email_agent, $new_ads);
                    $queue->create();
                }
            }
        }
    }

    private static function generateMarkup($user, $email_agent, $ads)
    {
        $view = \Phalcon\Di::getDefault()->getShared('view');
        $view->getRender(
            'email',
            'emailagent',
            array(
                'title' => 'Email agent "' . $email_agent->name . '"',
                'user'  => $user,
                'ads'   => $ads
            ),
            function($callback) {
                $callback->setRenderLevel(View::LEVEL_LAYOUT);
            }
        );
        $contents = $view->getContent();

        return $contents;
    }

    private function getParsedSettings()
    {
        $parsed_settings = null;

        $settings = json_decode($this->settings, true);
        if ($settings) {
            if (isset($settings['category_id'])) {
                $category_id = intval($settings['category_id']);
                unset($settings['category_id']);

                if ($category_id && $category = Categories::findFirst($category_id)) {
                    $parametrizator = new Parametrizator();
                    $parametrizator->setCategory($category);
                    $parametrizator->setModule('frontend');
                    $parametrizator->setData($settings);
                    $filters_response = $parametrizator->getFilters();
                    if (isset($filters_response['filters']) && count($filters_response['filters'])) {
                        $parsed_settings = array(
                            'category' => $category,
                            'filters'  => $filters_response['filters']
                        );
                    }
                }
            }
        }

        return $parsed_settings;
    }

    private function getNewAds()
    {
        $ads = null;

        if ($parsed_settings = $this->getParsedSettings()) {
            $category = $parsed_settings['category'];
            $filters  = $parsed_settings['filters'];

            $yesterdays_starting_timestamp = strtotime(date('Y-m-d', strtotime('-1 days')));

            if (count($filters)) {
                $builder = new \Phalcon\Mvc\Model\Query\Builder();
                $columns = array('ad.*');
                $builder->addFrom('Baseapp\Models\Ads', 'ad');
                $builder->innerJoin('Baseapp\Models\Categories', 'ad.category_id = category.id', 'category');

                $where_array  = array(
                    'category.lft >= :category_left: AND category.rght <= :category_right:',
                    'ad.active = 1 AND ad.is_spam = 0' // AND ad.sort_date >= :starting_timestamp:
                );
                $where_params = array(
                    'category_left'      => $category->lft,
                    'category_right'     => $category->rght,
                    //'starting_timestamp' => $yesterdays_starting_timestamp
                );
                $where_types  = array();
                $having_array  = array();

                foreach ($filters as $filter) {
                    if (isset($filter['model'])) {
                        $builder->innerJoin($filter['model'], $filter['alias'].'.ad_id = ad.id', $filter['alias']);
                        if (isset($filter['filter'])) {
                            if (isset($filter['filter']['columns'])) {
                                $columns = array_merge($columns, $filter['filter']['columns']);
                            }
                            if (isset($filter['filter']['where_conditions'])) {
                                $where_array[] = $filter['filter']['where_conditions'];
                                if (isset($filter['filter']['where_params'])) {
                                    $where_params = array_merge($where_params, $filter['filter']['where_params']);
                                }
                                if (isset($filter['filter']['where_types'])) {
                                    $where_types = array_merge($where_types, $filter['filter']['where_types']);
                                }
                            }
                            if (isset($filter['filter']['having_conditions'])) {
                                $having_array[] = $filter['filter']['having_conditions'];
                            }
                        }
                    } else {
                        if (isset($filter['columns'])) {
                            $columns = array_merge($columns, $filter['columns']);
                        }
                        if (isset($filter['where_conditions'])) {
                            $where_array[] = $filter['where_conditions'];
                            if (isset($filter['where_params'])) {
                                $where_params = array_merge($where_params, $filter['where_params']);
                            }
                            if (isset($filter['where_types'])) {
                                $where_types = array_merge($where_types, $filter['where_types']);
                            }
                        }
                        if (isset($filter['having_conditions'])) {
                            $having_array[] = $filter['having_conditions'];
                        }
                    }
                }

                if (count($where_types)) {
                    $builder->where(implode(' AND ', $where_array), $where_params, $where_types);
                } else {
                    $builder->where(implode(' AND ', $where_array), $where_params);
                }
                if (count($having_array)) {
                    $builder->having(implode(' AND ', $having_array));
                }
                $builder->columns($columns);
                $builder->groupBy(array('ad.id'));
                $builder->limit(100);
                $builder->orderBy('ad.sort_date DESC');

                $found_ads = $builder->getQuery()->execute();
                if ($found_ads && count($found_ads)) {
                    $ads = $this->get_prepared_ads_data($found_ads);
                }
            }
        }

        return $ads;
    }

    private function get_prepared_ads_data($rows)
    {
        $results = array();

        if (!$rows->valid()) {
            return $results;
        }

        $media_ids = array();
        $results   = array();

        $thumb_style_details = 'Oglas-140x140';
        $no_pic = (new Media())->getNoImageThumb($thumb_style_details)->getFullSrcForEmail();

        foreach ($rows as $k => $row) {
            /* @var Ads $ad */
            $ad = $row;
            $json_data = trim($ad->json_data) ? json_decode(trim($ad->json_data)) : null;

            $result = array(
                'id'          => $ad->id,
                'url'         => $ad->get_frontend_view_link(),
                'title'       => strip_tags($ad->title),
                'description' => nl2br(Utils::str_truncate(strip_tags(trim($ad->description_tpl) ? trim($ad->description_tpl) : trim($ad->description)), 150)),
                'sort_date'   => date('d.m.Y.', $ad->sort_date),
                'price'       => $ad->getPrices(),
                'media_id'    => null,
                'thumb_pic'   => $no_pic
            );

            // Grab media ids
            if ($json_data && isset($json_data->ad_media_gallery) && count($json_data->ad_media_gallery)) {
                $result['media_id'] = $json_data->ad_media_gallery[0];
                $media_ids[]        = $json_data->ad_media_gallery[0];
            }

            $results[$k] = $result;
        }

        if (!empty($media_ids)) {
            if ($ad_medias = (new Media())->get_multiple_ids($media_ids)) {
                foreach ($results as $k => $ad) {
                    foreach ($ad_medias as $media) {
                        if ($media->id == $ad['media_id']) {
                            $ad['thumb_pic'] = $media->get_thumb($thumb_style_details, false, $ad['category_id'])->getFullSrcForEmail();
                        }
                    }
                    $results[$k] = $ad;
                }
            }
        }

        return $results;
    }

}
