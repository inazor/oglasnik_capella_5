<?php

namespace Baseapp\Models;

use Baseapp\Library\Email;

class UsersEmailAgentsQueue extends BaseModelBlamable
{
    public $id;
    public $email_agent_id;
    public $recipient_email;
    public $subject;
    public $body;

    public $max_emails_per_hour = 100;

    public function initialize()
    {
        parent::initialize();

        $this->belongsTo(
            'email_agent_id', __NAMESPACE__ . '\UsersEmailAgents', 'id',
            array(
                'alias'      => 'UsersEmailAgent',
                'reusable'   => true,
            )
        );

        // Load email config from config.ini
        if ($config = \Phalcon\Di::getDefault()->getShared('config')->email_queue) {
            $max_emails_per_hour = $config->max_emails_per_hour;
        }
    }

    public function getSource()
    {
        return 'users_email_agents_queue';
    }

    private function get_cycle_limit()
    {
        return floor($this->max_emails_per_hour / 60);
    }

    public function process()
    {
        $queue = self::find(array(
            'limit' => $this->get_cycle_limit()
        ));

        if ($queue) {
            $sent_ids = array();

            foreach ($queue as $queue_row) {
                if ($queue_row->sendEmail()) {
                    $sent_ids[] = $queue_row->id;
                }
            }

            if (count($sent_ids)) {
                $sent_queue = self::find(array(
                    'conditions' => 'id IN (' . implode(',', $sent_ids) . ')'
                ));
                if ($sent_queue) {
                    $sent_queue->delete();
                }
            }
        }
    }

    private function sendEmail()
    {
        $email = new Email();
        $email->Subject = $this->subject;
        $email->AddAddress($this->recipient_email);
        // Images are referenced via 'cid:' in layouts/email.volt
        $basedir = ROOT_PATH . '/public/assets/img/';
        $email->addEmbeddedImage($basedir . 'email-logo-header.jpg', 'email-logo-header', 'email-logo-header.jpg');
        $email->addEmbeddedImage($basedir . 'email-logo-footer.jpg', 'email-logo-footer', 'email-logo-footer.jpg');
        $email->MsgHTML($email->rewrite_markup($this->body));
        $email_sent = $email->send();

        return $email_sent;
    }

}
