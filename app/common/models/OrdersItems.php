<?php

namespace Baseapp\Models;

use Baseapp\Library\Products\ProductsInterface;
use Baseapp\Library\Utils;

class OrdersItems extends BaseModelBlamable
{
    public $id;
    public $order_id;
    public $ad_id;

    public $product    = ''; // serialized product class
    public $product_id = ''; // product slug

    public $title = '';
    public $qty   = 1;

    /**
     * Outside code must use getters/setters
     * @access protected
     */
    protected $price = '0'; // string, in minor currency units (lp), unit price
    protected $total = '0'; // string, in minor currency units (lp), $qty * $price

    public $tax_rate = 0;
    public $tax_amount = '0';

    public function initialize()
    {
        parent::initialize();

        $this->belongsTo(
            'order_id', __NAMESPACE__ . '\Orders', 'id',
            array(
                'alias'    => 'Order',
                'reusable' => true,
            )
        );

        $this->belongsTo(
            'ad_id', __NAMESPACE__ . '\Ads', 'id',
            array(
                'alias'    => 'Ad',
                'reusable' => true,
            )
        );
    }

    /**
     * @return ProductsInterface|null
     */
    public function getProduct()
    {
        $product = null;

        if (null !== $this->product) {
            $product = unserialize($this->product);
        }

        return $product;
    }

    public function getTotal()
    {
        return Utils::lp2kn($this->total);
    }

    public function getTotalRaw()
    {
        return $this->total;
    }

    public function setTotal($total = '')
    {
        $this->total = Utils::kn2lp($total);
    }

    public function setTotalRaw($total)
    {
        $this->total = $total;
    }

    public function getPrice()
    {
        return Utils::lp2kn($this->price);
    }

    public function getPriceRaw()
    {
        return $this->price;
    }

    public function setPrice($price = '')
    {
        $this->price = Utils::kn2lp($price);
    }

    public function setPriceRaw($price)
    {
        $this->price = $price;
    }

    // Override magic setter to prevent direct access to 'total' and 'price' properties
    public function __set($property, $value)
    {
        if ('total' === $property) {
            $this->setTotal($value);
            return;
        }

        if ('price' === $property) {
            $this->setPrice($value);
            return;
        }

        parent::__set($property, $value);
    }

    // Override magic getter for 'total' and price properties
    public function __get($property)
    {
        if ('total' === $property) {
            return $this->getTotal();
        }

        if ('price' === $property) {
            return $this->getPrice();
        }

        return parent::__get($property);
    }

    public function getSource()
    {
        return 'orders_items';
    }

//*
    public function onValidationFails()
    {
        var_dump($this->getMessages());
        exit;
    }
//*/
}
