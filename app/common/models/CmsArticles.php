<?php

namespace Baseapp\Models;

use Baseapp\Bootstrap;
use Phalcon\Mvc\Model\Query\Builder;
use Baseapp\Library\Validations\CmsArticles as CmsArticlesValidations;
use Baseapp\Traits\MediaCommonMethods;
use Baseapp\Models\CmsCategories;

/**
 * Cms articles Model
 */
class CmsArticles extends BaseModelBlamable
{
    use MediaCommonMethods;

    private $_media_data = array();

    protected $fields_types_map = array(
        'int'     => array(
            'category_id',
            'sort_order'
        ),
        'boolint' => array(
            'active',
            'full_page_image',
            'zendesk',
            'featured'
        ),
        'date'    => array(
            'publish_date',
            'featured_start',
            'featured_end'
        ),
        'string'  => array(
            'slug',
            'title',
            'excerpt',
            'content',
            'meta_title',
            'meta_description',
            'meta_keywords'
        )
    );

    /**
     * CMS articles initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->belongsTo(
            'category_id', __NAMESPACE__ . '\CmsCategories', 'id',
            array(
                'alias'    => 'Category',
                'reusable' => true
            )
        );

        $this->hasMany(
            'id', __NAMESPACE__ . '\CmsArticlesTags', 'article_id',
            array(
                'alias'    => 'Tags',
                'reusable' => true
            )
        );

        $this->belongsTo(
            'created_by_user_id', __NAMESPACE__ . '\Users', 'id',
            array(
                'alias'    => 'CreatedBy',
                'reusable' => true
            )
        );

        $this->belongsTo(
            'modified_by_user_id', __NAMESPACE__ . '\Users', 'id',
            array(
                'alias'    => 'ModifiedBy',
                'reusable' => true
            )
        );

        $this->hasMany(
            'id', __NAMESPACE__ . '\CmsArticlesMedia', 'article_id',
            array(
                'alias'    => 'Media',
                'reusable' => true
            )
        );
    }

    public function getCategory()
    {
        $category = null;

        if ($this->category_id) {
            $category = CmsCategories::findFirst($this->category_id);
        }

        return $category;
    }

    /**
     * @param \Phalcon\Http\RequestInterface $request
     */
    protected function initialize_tags($request)
    {
        if ($this->Tags) {
            $this->Tags->delete();
            $this->raw_tags = null;

            $tags = $request->getPost('tags');
            if ($tags) {
                $article_tags = array();
                $article_raw_tags = array();
                $POST_tags = explode(',', $tags);

                foreach ($POST_tags as $POST_tag) {
                    $POST_tag = trim($POST_tag);
                    if ($POST_tag && $tag_id = Tags::get_id($POST_tag, true)) {
                        $article_raw_tags[] = $POST_tag;
                        $article_tag = new CmsArticlesTags();
                        $article_tag->tag_id = $tag_id;
                        $article_tags[] = $article_tag;
                    }
                }
                if (count($article_tags)) {
                    $this->Tags = $article_tags;
                    $this->raw_tags = implode(',', $article_raw_tags);
                }
            }
        }
    }

    /**
     * Add a new CmsArticle
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function add_new($request)
    {
        $this->initialize_model_with_post($request);
        $this->initialize_tags($request);
        $this->_handle_article_media($request);

        $validation = new CmsArticlesValidations();
        if ($request->getPost('category_id')) {
            $slug_extra_conditions = array(
                'category_id = :category_id:',
                array(
                    'category_id' => $request->getPost('category_id')
                )
            );
            $slug_validation_message = 'Slug should be unique';
            $validation->add('slug', new \Baseapp\Extension\Validator\Uniqueness(array(
                'model' => '\Baseapp\Models\CmsArticles',
                'extra_conditions' => $slug_extra_conditions,
                'message' => $slug_validation_message,
            )));
        }

        $messages = $validation->validate($request->getPost());

        if ($request->hasPost('featured')) {
            $validation->add('featured_start', new \Phalcon\Validation\Validator\PresenceOf(array(
                'message' => 'Featured start datetime is required'
            )));
            $validation->add('featured_end', new \Phalcon\Validation\Validator\PresenceOf(array(
                'message' => 'Featured end datetime is required'
            )));
        }

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            if (true === $this->create()) {
                return $this;
            } else {
                Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Save CmsArticle data method
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function save_changes($request)
    {
        $validation = new CmsArticlesValidations();
        // avoid validation for certain fields that haven't been changed
        if ($request->getPost('category_id') && $this->slug != $request->getPost('slug')) {
            $slug_extra_conditions = array(
                'category_id = :category_id:',
                array(
                    'category_id' => $request->getPost('category_id')
                )
            );
            $slug_validation_message = 'Slug should be unique';
            $validation->add('slug', new \Baseapp\Extension\Validator\Uniqueness(array(
                'model' => '\Baseapp\Models\CmsArticles',
                'extra_conditions' => $slug_extra_conditions,
                'message' => $slug_validation_message,
            )));
        }

        if ($request->hasPost('featured')) {
            $validation->add('featured_start', new \Phalcon\Validation\Validator\PresenceOf(array(
                'message' => 'Featured start datetime is required'
            )));
            $validation->add('featured_end', new \Phalcon\Validation\Validator\PresenceOf(array(
                'message' => 'Featured end datetime is required'
            )));
        }

        $messages = $validation->validate($request->getPost());

        $this->_handle_article_media($request);
        $this->initialize_model_with_post($request);
        $this->initialize_tags($request);

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            if (true === $this->update()) {
                return $this;
            } else {
                Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Active/Inactive article, depending on the previous state
     *
     * @return bool Whether the update was successful or not
     */
    public function activeToggle()
    {
        $this->disable_notnull_validations();
        $this->active = (int) !$this->active;
        $result = $this->update();
        $this->enable_notnull_validations();
        return $result;
    }

    public function getTags()
    {
        $tags = null;

        $builder = Tags::query();
        $builder
            ->innerJoin('Baseapp\Models\CmsArticlesTags', 'mat.tag_id = Baseapp\Models\Tags.id', 'mat')
            ->where(
                'mat.article_id = :article_id:',
                array(
                    'article_id' => $this->id
                )
            );
        $results = $builder->execute();
        if ($results && count($results) >= 1) {
            $tags = $results;
        }

        return $tags;
    }

    /**
     * Helper method to assign media to current article (using the relationship alias)
     *
     * @param \Phalcon\Http\RequestInterface $request
     */
    private function _handle_article_media($request)
    {
        if ($this->Media->valid()) {
            $this->Media->delete();
        }
        $this->_media_data = $request->getPost('article_media_gallery');

        $media = array();
        if ($this->_media_data && count($this->_media_data)) {
            foreach ($this->_media_data as $index => $id) {
                $article_media = new CmsArticlesMedia();
                $article_media->media_id = $id;
                $article_media->sort_idx = $index + 1;
                $media[] = $article_media;
            }
        }

        $this->Media = $media;
    }

    /**
     * Returns either the complete HTML markup for the article's frontend view
     * or just the URL (which is then used as the "href").
     *
     * Used primarily to keep the link format/definition in a single place since
     * we've started providing better "Article updated/created" success messages
     * in the backend too. Having a link within the message makes it
     * a lot easier to see the frontend version in a new tab/window).
     *
     * @param string $type Optional. Defaults to 'href' (returning just the URL).
     *                     If specified as 'html' returns the complete <a href...> markup.
     *
     * @return string
     */
    public function get_frontend_view_link($type = 'href')
    {
        $link = null;

        if (isset($this->slug)) {
            $url  = '/novosti/' . $this->slug;

            // Use the 'url' resolver service if present, making it more flexible
            $di = $this->getDI();
            if ($di->has('url')) {
                $url = $di->get('url')->get('novosti/clanak/' . $this->slug);
            }

            $link = $url;

            if ('html' === $type) {
                $markup = '<a href="%s" target="_blank"><i class="fa fa-fw fa-eye"></i>%s</a>';
                $link   = sprintf($markup, $url, 'View article');
            }
        }

        return $link;
    }

    public function getURI()
    {
        $url = null;

        if (isset($this->slug)) {
            $cmsTree     = $this->getDI()->get(CmsCategories::MEMCACHED_KEY);
            $cmsCategory = isset($cmsTree[$this->category_id]) ? $cmsTree[$this->category_id] : null;
            unset($cmsTree);

            if ($cmsCategory) {
                $url = $cmsCategory->url . '/' . $this->slug;
            } else {
                $url = $this->slug;
            }
            unset($cmsCategory);

            // Use the 'url' resolver service if present, making it more flexible
            $di = $this->getDI();
            if ($di->has('url')) {
                $url = $di->get('url')->get($url);
            } else {
                $url = '/' . $url;
            }
        }

        return $url;
    }

    public function getCategoryURI()
    {
        $url = null;

        if (isset($this->category_id) && $this->category_id) {
            $cmsTree     = $this->getDI()->get(CmsCategories::MEMCACHED_KEY);
            $cmsCategory = isset($cmsTree[$this->category_id]) ? $cmsTree[$this->category_id] : null;
            unset($cmsTree);

            if ($cmsCategory) {
                $url = $cmsCategory->url;
            }
            unset($cmsCategory);

        }

        return $url;
    }

    /**
     * Enrich given resultset with medias that should be shown on articles/homepage listings.
     *
     * @param $rows Resultset we'll be working on
     * @param $thumb_style_details
     *
     * @return array
     */
    public static function getEnrichedFrontendBasicResultsArray(&$rows, $thumb_style_details = 'CMS-300x200')
    {
        $results = array();

        // Bail if the Resultset ain't valid
        if (!$rows->valid()) {
            return $results;
        }

        $ids = array();

        $no_pic = (new Media())->getNoImageThumb($thumb_style_details);

        foreach ($rows as $k => $row) {
            if (isset($row->article)) {
                $article = $row->article;
            } else {
                $article = $row;
            }

            $ids[]                  = $article->id;
            $result                 = $article->toArray();
            $result['publish_date'] = date('d.m.Y.', strtotime($result['publish_date']));
            $result['frontend_url'] = $article->get_frontend_view_link();
            $result['thumb']        = $no_pic;

            $results[$k] = $result;
        }

        $results = self::enrichResultsWithMedia($results, $ids, $thumb_style_details);

        return $results;
    }

    /**
     * Enrich $results with Media files given in $media_ids array
     *
     * @param array $results             Array of Ads we'll be working on
     * @param array $article_ids         Array of article id's we need medias for
     * @param array $thumb_style_details Thumbnail settings array
     *
     * @return array Enriched $results array
     */
    protected static function enrichResultsWithMedia($results, $article_ids, $thumb_style_details)
    {
        if (!empty($article_ids)) {
            if ($articleMedias = (new CmsArticlesMedia())->getMediaForMultipleArticleIds($article_ids)) {
                foreach ($results as $k => $article) {
                    foreach ($articleMedias as $row) {
                        if (isset($row->media)) {
                            $media = $row->media;
                        } else {
                            $media = $row;
                        }
                        $article_id = $row->article_id;

                        if ($article_id == $article['id']) {
                            $article['thumb'] = $media->get_thumb($thumb_style_details);
                        }
                    }
                    $results[$k] = $article;
                }
            }
        }

        return $results;
    }

    public function getBreadcrumbs()
    {
        $breadcrumbs = null;

        if ($categoryBreadcrumbs = $this->getCategory()->getNodePath()) {
            $breadcrumbs = array();
            $currBc = 0;

            foreach ($categoryBreadcrumbs as $bc) {
                if ($bc->parent_id !== 1) {
                    $breadcrumbs[] = array(
                        'name'         => (0 === $currBc ? 'Novosti' : $bc->name),
                        'frontend_url' => $this->getDI()->get('url')->get('novosti/' . $bc->url)
                    );
                    $currBc++;
                }
            }
            if (empty($breadcrumbs)) {
                $breadcrumbs = null;
            }
        }

        return $breadcrumbs;
    }

}
