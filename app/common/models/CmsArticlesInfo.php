<?php

namespace Baseapp\Models;

use Baseapp\Bootstrap;
use Phalcon\Mvc\Model\Query\Builder;
use Baseapp\Library\Validations\CmsArticles as CmsArticlesValidations;
use Baseapp\Traits\MediaCommonMethods;

/**
 * Cms articles (static pages) Model
 */
class CmsArticlesInfo extends CmsArticles
{
    /**
     * CmsArticlesInfo initialize
     */
    public function initialize()
    {
        parent::initialize();
    }

}
