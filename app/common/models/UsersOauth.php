<?php

namespace Baseapp\Models;


class UsersOauth extends BaseModelBlamable
{

    /**
     * Sets the table name
     */
    public function getSource()
    {
        return 'user_oauth';
    }

    /**
     * Initialize the model
     */
    public function initialize()
    {
        parent::initialize();

        $this->belongsTo('user_id', __NAMESPACE__ . '\Users', 'id', array(
            'alias' => 'User',
            'foreignKey' => true
        ));
    }

} 
