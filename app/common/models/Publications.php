<?php

namespace Baseapp\Models;

use Baseapp\Library\Validations\Publications as PublicationValidations;
use Phalcon\Mvc\Model\Relation as PhRelation;

/**
 * Publications Model
 */
class Publications extends BaseModelBlamable
{
    /**
     * Publications initialize
     */
    public function initialize()
    {
        parent::initialize();
        $this->hasMany('id', __NAMESPACE__ . '\Products', 'publication_id', array(
            'alias' => 'Products',
            'foreignKey' => array(
                'action' => PhRelation::ACTION_CASCADE
            )
        ));
    }

    /**
     * Returns available types (defined as ENUM in the db currently)
     * @return array
     */
    public function getAvailableTypes()
    {
        $options = $this->get_field_options('type');

        // fill array keys with values so that \Phalcon\Select behaves properly...
        $return_options = array();
        foreach ($options as $k => $v) {
            $return_options[$v] = $v;
        }

        return $return_options;
    }

    public function initialize_model_with_post($request)
    {
        $this->type = $request->hasPost('type') ? trim($request->getPost('type')) : 'online';
        $this->name = $request->hasPost('name') ? trim($request->getPost('name')) : '';
        $this->active = $request->hasPost('active') ? 1 : 0;
    }


    /**
     * Add a new Publication
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function add_new($request)
    {
        $this->initialize_model_with_post($request);

        $validation = new PublicationValidations();

        $available_types = $this->getAvailableTypes();
        $validation->add('type', new \Phalcon\Validation\Validator\InclusionIn(array(
            'message' => 'Type must be one of: ' . implode(', ', $available_types),
            'domain' => $available_types
        )));
        $messages = $validation->validate($request->getPost());

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            if (true === $this->create()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Save Publication data method
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function save_changes($request)
    {
        // avoid validation for certain fields that haven't been changed
        $unique_if_changed = array('type');
        $require_unique = array();
        foreach ($unique_if_changed as $field) {
            if ($this->$field != $request->getPost($field)) {
                $require_unique[] = $field;
            }
        }
        $validation = new PublicationValidations();
        $validation->set_unique($require_unique);

        $available_types = $this->getAvailableTypes();
        $validation->add('type', new \Phalcon\Validation\Validator\InclusionIn(array(
            'message' => 'Type must be one of: ' . implode(', ', $available_types),
            'domain' => $available_types
        )));

        $messages = $validation->validate($request->getPost());

        $this->initialize_model_with_post($request);

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            if (true === $this->update()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Active/Inactive publication, depending on the previous state
     *
     * @return bool Whether the update was successful or not
     */
    public function activeToggle()
    {
        $this->disable_notnull_validations();
        $this->active = (int) !$this->active;
        $result = $this->update();
        $this->enable_notnull_validations();
        return $result;
    }

}
