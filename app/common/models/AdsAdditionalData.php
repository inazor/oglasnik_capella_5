<?php

namespace Baseapp\Models;

class AdsAdditionalData extends BaseModelBlamable
{
    const TABLE_NAME = 'ads_additional_data';

    public $id;
    public $ad_id;
    public $import_id;
    public $avus_id;
    public $remark;

    public function initialize()
    {
        $this->belongsTo(
            'ad_id', __NAMESPACE__ . '\Ads', 'id',
            array(
                'alias'      => 'Ad',
                'reusable'   => true
            )
        );

        parent::initialize();
    }

    public function getSource()
    {
        return self::TABLE_NAME;
    }
}
