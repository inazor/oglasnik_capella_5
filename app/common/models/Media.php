<?php

namespace Baseapp\Models;

use Baseapp\Bootstrap;
use Baseapp\Extension\Image;
use Baseapp\Library\Ffmpeg;
use Baseapp\Library\Images\Thumb;
use Baseapp\Library\Images\ThumbNoImageSvgInline;
use Baseapp\Library\PartialFilenameFilter;
use Baseapp\Library\Utils;

class Media extends BaseModelBlamable
{
    const TYPE_IMAGE = 1;
    const TYPE_VIDEO = 2;
    const TYPE_AUDIO = 3;
    const TYPE_FILE  = 4;

    public $id;
    public $type;
    public $filename;
    public $filename_orig;
    public $mimetype;
    public $mimetype_orig;

    public $author = null;
    public $title = null;
    public $description = null;
    public $copyright = null;
    public $keywords = null;

    public $created_at;
    public $modified_at;

    public $created_by_user_id;
    public $modified_by_user_id;

    private $base_dir;
    private $base_uri;

    private $custom_files;

    /**
     * Model init. Called only once per request!
     */
    public function initialize()
    {
        parent::initialize();

        $this->hasMany(
            'id', __NAMESPACE__ . '\AdsMedia', 'media_id',
            array(
                'alias'    => 'Ads',
                'reusable' => true
            )
        );

        $this->hasMany(
            'id', __NAMESPACE__ . '\CmsArticlesMedia', 'media_id',
            array(
                'alias'      => 'Articles',
                'reusable'   => true
            )
        );

        $this->hasMany(
            'id', __NAMESPACE__ . '\MediaCrops', 'media_id',
            array(
                'alias'    => 'Crops',
                'reusable' => true
            )
        );

        $this->belongsTo(
            'created_by_user_id', __NAMESPACE__ . '\Users', 'id',
            array(
                'alias'    => 'CreatedBy',
                'reusable' => true
            )
        );

        $this->belongsTo(
            'modified_by_user_id', __NAMESPACE__ . '\Users', 'id',
            array(
                'alias'    => 'ModifiedBy',
                'reusable' => true
            )
        );
    }

    public function onConstruct()
    {
        $config = $this->getDI()->get('config');

        $this->base_dir = $config->app->uploads_dir;
        $this->base_uri = $config->app->uploads_uri;
    }

    public function getSource()
    {
        return 'media';
    }

    public function get_variations_prefix()
    {
        return '/_var';
    }

    /**
     * Static helper for getByThumb()
     *
     * @param null|Thumb|string $src
     *
     * @return null|\Phalcon\Mvc\Model
     */
    public static function getEntityByThumb($src = null)
    {
        return (new self())->getByThumb($src);
    }

    /**
     * Returns the matching Media entity given a Thumb or a thumbnail src/url
     *
     * @param null|Thumb|string $src
     *
     * @return null|\Phalcon\Mvc\Model
     */
    public function getByThumb($src = null)
    {
        // Check if Thumb or a string is passed
        if ($src instanceof Thumb) {
            $url = $src->getSrc();
        } else {
            $url = $src;
        }

        $entity = null;

        // Parse the url and get the hashed filename part
        $hash = self::get_filename_hash_part($url);

        if ($hash) {
            // Query for the entity by filename matching the hash
            $entity = self::findFirst(array(
                'conditions' => "filename LIKE CONCAT(:filename:, '%')",
                'bind'       => array('filename' => $hash)
            ));
        }

        return $entity;
    }

    public static function get_filename_hash_part($filename)
    {
        $basename  = basename($filename);
        $extension = Utils::get_filename_extension($basename);
        $filename  = str_replace('.' . $extension, '', $basename);

        // Grab the first 40 chars of the filename only, since we're using sha1
        return substr($filename, 0, 40);
    }

    /**
     * Overriding the BaseModel's ident
     *
     * @return mixed|null|string
     */
    public function ident()
    {
        $val = 'Unknown';

        if (isset($this->title) && !empty($this->title)) {
            $val = $this->readAttribute('title');
        } elseif (isset($this->filename_orig)) {
            $val = $this->readAttribute('filename_orig');
        } elseif (isset($this->filename)) {
            $val = $this->readAttribute('filename');
        }

        $val .= sprintf(' [ID: %s]', $this->readAttribute('id'));

        return $val;
    }

    public function get_download_url($is_admin = true)
    {
        $admin = $is_admin ? 'admin/' : '';

        return $this->getDI()->get('url')->get($admin . 'media/download/' . $this->id);
    }

    /**
     * Cleanup the filesystem when the Media record is gone
     */
    public function beforeDelete()
    {
        $success = false;

        $file = $this->get_original_file();
        if (null !== $file && !is_dir($file)) {
            /**
             * Deleting the originally uploaded file (and variations) from disk
             * only if no other media record references the same filename
             * (because, in theory, it could, since the filename is generated
             * based on the sha1 hash of the uploaded file contents, so two
             * identical file uploads point to the same single file on disk)
             */
            $success = true;
            if (!$this->has_filename_duplicates($this->filename)) {
                // Deleting any potentially created variations
                $deleted_something = $this->delete_variations_from_disk();

                // Removing original file
                $success = @unlink($file);

                // Make sure to allow deletion if we actually deleted something
                // handles edge cases for testing etc (created style, removed original, broken/partial
                // previous delete attempts...)
                if (!$success && $deleted_something) {
                    $success = true;
                }
            }
        }

        return $success;
    }

    /**
     * Check if more than 1 record with the specified filename exists.
     * If no specific filename is given, the current record's filename is assumed.
     *
     * @param string|null $filename
     *
     * @return bool
     */
    public function has_filename_duplicates($filename = null)
    {
        if (null === $filename) {
            $filename = $this->filename;
        }

        $sql    = 'SELECT COUNT(*) AS total FROM Baseapp\Models\Media WHERE filename = :filename:';
        $result = $this->getModelsManager()->executeQuery($sql, array('filename' => $filename))->toArray();
        $total  = (int) $result[0]['total'];

        return ($total >= 2);
    }

    public function delete_variations_from_disk()
    {
        $success = false;

        if ($this->type != self::TYPE_IMAGE && $this->type != self::TYPE_VIDEO) {
            // Only video and image types have variations (for now at least)
            return true;
        }

        if ($this->filename) {
            $filepath    = Utils::get_fqpn($this->filename, Utils::fqpn_options());
            $type_prefix = $this->get_storage_type_prefix($this->type);
            $full        = $this->base_dir . '/' . $type_prefix . $this->get_variations_prefix() . $filepath;

            /**
             * Get the filename without the extension and delete
             * any files found withing the variations directory that match
             * the partial filename
             */
            $extension                  = Utils::get_filename_extension($this->filename);
            $filename_without_extension = str_replace('.' . $extension, '', $this->filename);

            // Walk the variations dir and delete anything that matches the partial filename
            $dir_name = pathinfo($full, PATHINFO_DIRNAME);

            $deleted_something = false;
            if (is_dir($dir_name)) {
                $dir_handle = @opendir($dir_name);
                if ($dir_handle) {
                    while (false !== ($file = readdir($dir_handle))) {
                        if ('.' !== $file && '..' !== $file) {
                            if (false !== stripos($file, $filename_without_extension)) {
                                $full_path = $dir_name . '/' . $file;
                                if (!is_dir($full_path)) {
                                    // In theory, all these should succeed...
                                    $success = unlink($full_path);
                                    if ($success) {
                                        $deleted_something = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $success = $deleted_something;
        }

        return $success;
    }

    public function delete_files_with_slug($slug)
    {
        /**
         * This might be tricky - we need to go through *all* the repositories
         * and their _var directories and subdirectories and scan for and find
         * files matching a certain filename pattern and then delete them
         */
        $deleted_something = false;
        $var_prefix        = $this->get_variations_prefix();

        $repos           = array();
        $repos['images'] = $this->base_dir . '/' . $this->get_storage_type_prefix(self::TYPE_IMAGE) . $var_prefix;
        $repos['media']  = $this->base_dir . '/' . $this->get_storage_type_prefix(self::TYPE_VIDEO) . $var_prefix;

        foreach ($repos as $repo => $path) {
            if (is_dir($path)) {
                $it = new \RecursiveDirectoryIterator($path, \RecursiveDirectoryIterator::SKIP_DOTS);
                $it = new PartialFilenameFilter($it, '-' . $slug);
                $it = new \RecursiveIteratorIterator($it);

                foreach ($it as $file) {
                    $removed = unlink($file->getPathname());
                    if ($removed) {
                        $deleted_something = true;
                    }
                }
            }
        }

        return $deleted_something;
    }

    /**
     * Helper method to get multiple media records at once
     *
     * @param array|string|null $ids Comma-separated list of ids or an array of media ids
     *
     * @return Media[]|null
     */
    public function get_multiple_ids($ids = null)
    {
        $media_items = null;

        if ($ids) {
            if (!is_array($ids)) {
                if (strpos($ids, ',') !== false) {
                    $ids = explode(',', $ids);
                } elseif (is_numeric($ids)) {
                    $ids = array((int) $ids);
                }
            }

            if (count($ids)) {
                $queryBuilder = new \Phalcon\Mvc\Model\Query\Builder();
                $media_items = $queryBuilder
                   ->from('Baseapp\Models\Media')
                   ->inWhere('id', $ids)
                   ->orderBy('FIELD(id, ' . implode(',', $ids) . ')')
                   ->getQuery()
                   ->execute();
            }
        }

        return $media_items;
    }

    public function get_type_slug($type = null)
    {
        if (null === $type) {
            $type = $this->type;
        }

        switch ($type) {
            case self::TYPE_IMAGE:
                $slug = 'image';
                break;
            case self::TYPE_VIDEO:
                $slug = 'video';
                break;
            case self::TYPE_AUDIO:
                $slug = 'audio';
                break;
            case self::TYPE_FILE:
            default:
                $slug = 'file';
                break;
        }

        return $slug;
    }

    protected function init_thumb_data($file = null)
    {
        $data = array();

        $data['mime']          = $this->mimetype;
        $data['type']          = $this->get_type_slug();
        $data['filename_orig'] = $this->filename_orig;

        if (null !== $file && 'image' === $data['type']) {
            $size           = getimagesize($file);
            $data['width']  = $size[0];
            $data['height'] = $size[1];
            $data['mime']   = $size['mime'];
        }

        if (false !== strpos($data['mime'], '/')) {
            list($type, $subtype) = explode('/', $data['mime']);
        } else {
            list($type, $subtype) = array($data['mime'], '');
        }

        $data['subtype']     = $subtype;
        $data['orientation'] = 'landscape';

        if (isset($data['height']) && isset($data['width']) && $data['height'] > $data['width']) {
            $data['orientation'] = 'portrait';
        }

        return $data;
    }

    /**
     * @param null $type
     *
     * @return Thumb
     */
    public function get_thumb_icon($type = null)
    {
        $type_slug = $this->get_type_slug($type);

        if ('file' === $type_slug) {
            $type_slug = 'default';
        }

        $url = $this->getDI()->get('url')->get('assets/img/media/' . $type_slug . '.png');

        $preview             = $this->init_thumb_data();
        $preview['src']      = $url;
        $preview['basename'] = basename($url);
        $preview['icon']     = true;
        $preview['tag']      = '<img src="' . $url . '" class="icon">';

        $thumb = new Thumb($preview);

        return $thumb;
    }

    /**
     * Returns a Thumb generated from the specified style
     * (or the original file in case the record is not styleable)
     *
     * @param null|ImageStyles|string|int|array $style
     * @param bool $force_create Force creation even if the file on disk already exists
     * @param Categories|int $category Category id, so that we can have different fallback images per category
     *
     * @return Thumb
     */
    public function get_thumb($style = null, $force_create = false, $category = null)
    {
        $variations_prefix = '';

        // Find the ImageStyles if specified
        $style = $this->styles_adapter($style);

        // If a specific style was requested but it's not styleable return a default icon
        if ($style && !$this->is_styleable()) {
            return $this->get_thumb_icon($this->type);
        }

        if ($category instanceof Categories) {
            $category_id = $category->id;
        } else {
            $category_id = (int) $category;
        }

        $category_fallback_image = Categories::getFallbackImage($category_id);

        // $original is the full pathname to the originally uploaded file
        $original    = $this->get_original_file();
        $type_prefix = $this->get_storage_type_prefix($this->type);

        // Original file might not exist on our filesystem for various reasons, so bail early
        if (null === $original) {
            return self::getNoImageThumb($style, 'Not found', $category_fallback_image);
        }

        // Determine what we're working with
        $filename = $original;
        if ($style) {
            // ImageStyles are stored in a special folder below originally uploaded files
            $variations_prefix = $this->get_variations_prefix();
            $filename          = $style->tweak_filename($original);
        }

        $basename = basename($filename);

        $basedir              = $this->base_dir . '/' . $type_prefix . $variations_prefix;
        $partitioned_filepath = Utils::get_fqpn(
            $basename,
            Utils::fqpn_options(array(
                'basedir'             => $basedir,
                // create any missing directories
                'create_missing_dirs' => true
            ))
        );

        // Check if specified style file exists already (or if forcing creation), if not, create it
        if (!file_exists($partitioned_filepath) || $force_create) {
            // File doesn't yet exists (or forced creation)
            $created = $this->create_style_image($style, $original, $partitioned_filepath);
            if (!$created) {
                // Unable to create the thumbnail, return a default 'no-image' Thumb
                return self::getNoImageThumb($style, null, $category_fallback_image);
            }
        }

        $url_part = str_replace($basedir, '', $partitioned_filepath);
        // $filename starts with a slash due to Utils::fqpn_opttions()
        $url = $this->base_uri . '/' . $type_prefix . $variations_prefix . $url_part;

        // FIXME: edge case possible here: creating the thumb could've failed for some reason...
        // In that case we get a warning/notice from getimagesize() in init_thumb_data()... and
        // a basically useless Thumb object, since the Thumb itself does not exist on disk.
        $preview             = $this->init_thumb_data($partitioned_filepath);
        $preview['src']      = $url;
        $preview['basename'] = basename($url);
        // $preview['path']     = $partitioned_filepath;
        $preview['tag'] = '<img src="' . $preview['src'] . '" alt="">';
        if ($style) {
            if (method_exists($style, 'toArray')) {
                $preview['style'] = $style->toArray();
            } else {
                $preview['style'] = $style;
            }
        }

        $thumb = new Thumb($preview);

        return $thumb;
    }

    /**
     * Returns a new Thumb representing the default 'no-image-available' thumbnail.
     * FIXME: Use our own placeholder image(s)
     *
     * @param $style
     * @param string|null $text Optional. Extra text to put on the image
     * @param string|null $path Optional. Allows overriding the default no-image-placeholder.svg
     *
     * @return Thumb
     */
    public static function getNoImageThumb($style, $text = null, $path = null)
    {
        $media = new self();

        // FIXME: In theory, we end up with a null $style when Media::get_url() is called without
        // specifying a style at all, and the expectation from volt appears to be that it should
        // return the originally uploaded file's url -- we might have to fix that!
        // For now, gonna set some large default width and height without cropping just to avoid fatal errors.
        if (null === $style) {
            $style = ImageStyles::fromArray(array(
                'width'  => 1024,
                'height' => 1024,
                'crop'   => true
            ));
        }

        $style = $media->styles_adapter($style);

        // FIXME/TODO: when called with a specific style which doesn't exist in the database
        // yet for whatever reason, we can get null here. Gonna use a 270x270 "default" for now,
        // so that we don't get a bunch of fatals now in the early dev phase
        if (null === $style) {
            $style = ImageStyles::fromArray(array(
                'width' => 270,
                'height' => 270,
                'crop' => true
            ));
        }

        $thumb_path = 'assets/img/no-image-placeholder.svg';
        if ($path !== null) {
            $thumb_path = $path;
        }

        $thumb_data = array(
            'width'  => $style->width,
            'height' => $style->height,
            'style'  => $style->toArray(),
            'class'  => 'no-thumb no-thumb-svg',
            'src'    => $media->getDI()->get('url')->get($thumb_path)
        );

        if (null !== $text) {
            // $thumb_data['src'] .= '&amp;text=' . $text . '+' . sprintf('%sx%s', $style->width, $style->height);
        }
/*
        // Set the Thumb src based on orientation
        unset($thumb_data['src']);
        $thumb       = new Thumb($thumb_data);
        $orientation = $thumb->getOrientation();
        if ('landscape' === $orientation) {
            $src = $this->getDI()->get('url')->get('assets/img/nema-slike.svg');
        } else {
            $src = $this->getDI()->get('url')->get('assets/img/nema-slike-portrait.svg');
        }
        $thumb->setSrc($src);
*/
/*
        $thumb = new ThumbNoImageSvgInline($thumb_data);
*/
        $thumb = new Thumb($thumb_data);

        return $thumb;
    }

    /**
     * Specialized method to reduce backend template view code duplication and
     * keep backend thumb definition in a single place
     *
     * @return Thumb
     */
    public function get_backend_thumb()
    {
        return $this->get_thumb('GridView');
/*
        return $this->get_thumb(array(
            'width' => 300,
            'height' => 300,
            'crop' => false
        ));
*/
    }

    /**
     * Returns either a specific stored crop definition/coordinates or all of them or false in
     * case a crop record for a specific style is specified and it does not exist.
     *
     * @param bool|null|ImageStyles|string|int|array $style
     *
     * @return array|bool
     */
    public function get_matching_crops($style = false)
    {
        // Cache for repeated calls to the function
        static $crops_keyed = null;

        if (null === $crops_keyed) {
            $crops_keyed = array();
            if ($this->Crops->valid()) {
                $crops       = $this->Crops->toArray();
                $crops_keyed = array();
                foreach ($crops as $crop) {
                    $crops_keyed[$crop['style_id']] = $crop;
                }
                unset($crops);
            }
        }

        // If a specific crop was specified (via style slug), return only that one, otherwise all
        $return = $crops_keyed;
        if (false !== $style) {
            $style  = $this->styles_adapter($style);
            $return = false;
            if ($style) {
                if (isset($crops_keyed[$style->id])) {
                    $return = $crops_keyed[$style->id];
                }
            }
        }

        return $return;
    }

    /**
     * Create and return all the available/defined Thumbs (if the entity is styleable).
     * Can be limited to only returning some Thumbs by specifying them as an array of
     * individual parameters accepted by Media::get_thumb()
     *
     * @param null|ImageStyles[]|int[]|string|string[]|array $styles
     *
     * @return Thumb[]|null An array of Thumb objects or null if the Media is not styleable
     */
    public function get_thumbs($styles = 'all')
    {
        if ($this->is_styleable()) {
            $previews = array();

            if (!is_array($styles) || empty($styles) || 'all' === $styles) {
                $styles = ImageStyles::all();

                // Cached styles collection is keyed on both ids and slugs, which
                // results in duplicate styles being used later on unless we filter them here
                $styles = Utils::array_where($styles, function($k, $v){
                    return is_numeric($k);
                });

            } else {
                $styles = (array) $styles;
            }

            foreach ($styles as $style) {
                $previews[] = $this->get_thumb($style);
            }

            return $previews;
        } else {
            return null;
        }
    }

    public function get_all_thumbs()
    {
        return $this->get_thumbs('all');
    }

    /**
     * Returns the URL for the specified style (or the originally uploaded file).
     *
     * @param null|ImageStyles|string|int|array $style
     * @param bool $force_create Force creation even if the file on disk already exists
     * @param Categories|int $category Category id, so that we can have different fallback images per category
     *
     * @return string
     */
    public function get_url($style = null, $force_create = false, $category = null)
    {
        if (self::TYPE_FILE === $this->type) {
            // Raw files are dangerous. So they go through a wrapper
            return $this->get_download_url();
        } else {
            // Get the preview/thumb and return it's src
            $preview = $this->get_thumb($style, $force_create, $category);

            return $preview->getSrc();
        }
    }

    public function is_styleable()
    {
        return (self::TYPE_IMAGE === $this->type || self::TYPE_VIDEO === $this->type);
    }

    /**
     * Adapts all different kinds of ways to specify an ImageStyles record
     * and returns an ImageStyles instance or null.
     *
     * It can be specified by slug, id, or as an array of key-value pairs that
     * have to match the field names used in the ImageStyles model.
     *
     * The array 'syntax' is useful for testing and backend-only and/or one-off usages.
     *
     * @param null|ImageStyles|string|int|array $style
     *
     * @return ImageStyles|null
     */
    public function styles_adapter($style = null)
    {
        if (null !== $style && !($style instanceof ImageStyles)) {
            if (is_array($style)) {
                $style = ImageStyles::fromArray($style);
            } else {
                $styles = (new ImageStyles())->cachedAll();
                $style  = isset($styles[$style]) ? $styles[$style] : null;
            }
        }

        return $style;
    }

    public static function static_styles_adapter($style = null)
    {
        return (new self())->styles_adapter($style);
    }

    /**
     * @param $style \stdClass|ImageStyles
     * @param $source string Source filepath
     * @param $target string Target filepath
     *
     * @return bool
     */
    public function create_style_image($style, $source, $target)
    {
        /**
         * zyt: 18.08.2016.
         *
         * Wrapped in a try/catch to avoid unhandled exceptions that caused panic on
         * production on several occasions (because the exceptions bubble up into unexpected places since thumbs
         * are now shown pretty much everywhere, and code that shows them explodes -- suddenly even backend ads listing
         * shows error 500 etc).
         *
         * The only way this happens is when someone shoves an empty image on disk and stores its path
         * into our database from outside (such as via their in-house crawlers/importers/whatever).
         * Regularly uploaded files handled by our code never seem to end up being the reason for exceptions here.
         *
         * So there's that.
         */
        try {
            $image = new Image($source, $style->width, $style->height);
        } catch (\Exception $e) {
            Bootstrap::log('Caught exception while opening source image in Media::create_style_image()');
            Bootstrap::log($e);
            return false;
        }

        // Auto rotate as soon as we open it, so anything done further should be fine
        $im     = $image->getInternalImInstance();
        $format = strtolower($im->getImageFormat());
        if ('jpeg' === $format) {
            $image->auto_rotate();
        }
        unset($im, $format);

        // Store original dimensions
        $orig_w = $image->getWidth();
        $orig_h = $image->getHeight();

        // TODO: can handle CMYK crap if it becomes an issue in the future...
        // $image->handle_cmyk();

        // If there's a stored crop definition (or the style specifies a crop)
        $do_crop       = false;
        $existing_crop = $this->get_matching_crops($style->id);
        if ($existing_crop) {
            $do_crop = true;
        } elseif ($style->crop) {
            $do_crop                 = true;
            $existing_crop           = array();
            $existing_crop['width']  = $style->width;
            $existing_crop['height'] = $style->height;
        }

        // Do the cropping if needed
        if ($do_crop) {
            if (isset($existing_crop['x']) && isset($existing_crop['y'])) {
                // Exact coordinates exist
                $image->crop($existing_crop['width'], $existing_crop['height'], $existing_crop['x'], $existing_crop['y']);
            } else {
                // No previous specific crop record, try smart fitting without any upsizing for now
                $image->fit($existing_crop['width'], $existing_crop['height'], function($constraint) {
                    $constraint->upsize();
                });
            }

            // Enforce cropped dimensions in case the result is smaller than what the style dictates
            // NB: Only 'hard-crop' styles enforce this (for now at least), the other possible use-case
            // might come from a stored precise crop definition without the style itself having the
            // crop flag - that's why this if is here...
            if ($style->crop) {
                $actual_w      = $image->getWidth();
                $actual_h      = $image->getHeight();
                $target_width  = ($actual_w < $style->width) ? $style->width : null;
                $target_height = ($actual_h < $style->height) ? $style->height : null;
                if ($target_width || $target_height) {
                    $image->resize_canvas($target_width, $target_height);
                }
            }
        }

        // Now resize the (even potentially cropped) image if needed, hopefully avoiding upscaling in all cases
        $image->resize_down($style->width, $style->height);

        // Apply watermarking strategy if style says so
        if ($style->watermark) {
            $text = null;
            if (!empty($this->copyright)) {
                $text = $this->copyright;
            }

            $logo = ROOT_PATH . '/public/assets/img/16px.png';
            $image->add_default_watermark($logo, $text);
        }

        return $image->save($target);
    }

    public function get_original_file()
    {
        $pathname = null;

        if (isset($this->id) && !empty($this->filename)) {
            $filename    = Utils::get_fqpn($this->filename, Utils::fqpn_options());
            $type_prefix = $this->get_storage_type_prefix($this->type);
            $pathname    = $this->base_dir . '/' . $type_prefix . $filename;
            if (!file_exists($pathname)) {
                $pathname = null;
            }
        }

        return $pathname;
    }

    public function get_original_file_details()
    {
        $details = null;

        $file = $this->get_original_file();
        if (null !== $file) {
            $details                   = array();
            $details['filesize_bytes'] = filesize($file);
            $details['filesize']       = Utils::format_filesize($details['filesize_bytes']);
            $details['filepath']       = $file;
            $details['basename']       = basename($file);
            $details['mimetype']       = $this->mimetype;

            switch ($this->type) {
                case self::TYPE_VIDEO:
                    $movie       = new Ffmpeg($file);
                    $details_map = array(
                        'filetype'         => 'FileType',
                        'duration'         => 'Duration',
                        'bitrate'          => 'BitRate',
                        'audio.codec'      => 'AudioCodec',
                        'audio.frequency'  => 'AudioFrequency',
                        'audio.chantype'   => 'AudioChanType',
                        'audio.bitrate'    => 'AudioBitrate',
                        'video.resolution' => 'VideoResolution',
                        'video.palette'    => 'VideoPalette',
                        'video.width'      => 'FrameWidth',
                        'video.height'     => 'FrameHeight',
                        'video.framerate'  => 'FrameRate',
                        'video.bitrate'    => 'VideoBitRate'
                    );
                    // parse the details tab
                    foreach ($details_map as $k => $method) {
                        $details[$k] = $movie->{'get' . $method}();
                    }
                    $details['dimensions'] = sprintf('%s x %s', $details['video.width'], $details['video.height']);
                    break;
                case self::TYPE_IMAGE:
                    $size                  = getimagesize($file);
                    $details['width']      = $size[0];
                    $details['height']     = $size[1];
                    $details['dimensions'] = sprintf('%s x %s', $size[0], $size[1]);
                    break;
            }
        }

        return $details;
    }

    public function set_defaults()
    {
        $now              = date('Y-m-d H:i:s');
        $this->created_at = $this->modified_at = $now;

        $this->set_created_by();
        $this->set_modified_by();
    }

    public function get_auth_user_id()
    {
        // Cache for potential repeated calls
        static $user = null;
        if (null === $user) {
            $user = $this->getDI()->getShared('auth')->get_user();
        }

        if ($user instanceof Users) {
            return $user->id;
        }

        return null;
    }

    public function set_created_by($id = null)
    {
        if (!isset($this->created_by_user_id)) {
            if (null === $id) {
                $id = $this->get_auth_user_id();
            }

            $this->created_by_user_id = $id;
        }
    }

    public function set_modified_by($id = null)
    {
        if (!isset($this->modified_by_user_id)) {
            if (null === $id) {
                $id = $this->get_auth_user_id();
            }

            $this->modified_by_user_id = $id;
        }
    }

    /**
     * Returns the storage dir/prefix based on media type.
     *
     * @param int $type One of the self::TYPE_* constants
     *
     * @return string The storage path prefix for the specific type
     */
    public function get_storage_type_prefix($type)
    {
        switch ($type) {
            case self::TYPE_IMAGE:
                return 'images';
                break;
            case self::TYPE_VIDEO:
            case self::TYPE_AUDIO:
                return 'media';
                break;
            case self::TYPE_FILE:
            default:
                return 'files';
                break;
        }
    }

    /**
     * Returns the type of media we consider a file to be based on the mime type or file extension.
     *
     * Works primarily on the passed in mime type string, which should always be obtained
     * via Phalcon's File::getRealType(). However, there are certain cases where we might need to
     * override things and really treat things as 'audio' even though the mime type and/or
     * extension is something completely different., overwrite the type since many
     * browsers suck at sending a proper mime type for a file, the second
     * parameter is used as a fallback in some cases (and then files get treated based on the extension,
     * which is not really safe, but it works for now).
     *
     * @param string $mime_type
     * @param string $extension
     *
     * @return string
     */
    public function get_type_from_mimetype($mime_type = '', $extension = '')
    {
        // Generic images
        if (strpos($mime_type, 'image/') !== false) {
            return self::TYPE_IMAGE;
        }
        // Generic audio
        if (strpos($mime_type, 'audio/') !== false) {
            return self::TYPE_AUDIO;
        }
        // Generic video
        if (strpos($mime_type, 'video/') !== false) {
            return self::TYPE_VIDEO;
        }

        /**
         * If an extension and/or mimetype matches something specific we'd like it to be treated as,
         * and we haven't already returned a type above, this is where we can compensate/override...
         */

        $flv_extensions   = array('flv', 'f4v', 'mp4', 'm4v');
        $audio_extensions = array('mp3');

        switch ($mime_type) {
            case 'application/ogg':
                $type = self::TYPE_AUDIO;
                break;
            default:
                if (in_array($extension, $flv_extensions)) {
                    $type = self::TYPE_VIDEO;
                } else {
                    if (in_array($extension, $audio_extensions)) {
                        $type = self::TYPE_AUDIO;
                    } else {
                        // Fallback to generic 'file' type for everything else
                        return self::TYPE_FILE;
                    }
                }
                break;
        }

        return $type;
    }

    /**
     * Saves data for a new entity
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function backend_create($request)
    {
        $file_validator = new \Baseapp\Extension\Validator\File(array(
            'messageEmpty'   => 'No file uploaded',
            'messageIniSize' => 'Uploaded file exceeds max upload limit'
        ));
        $validation     = new \Phalcon\Validation();
        $validation->add('upload', $file_validator);

        // TODO: Create a new/different FileUpload Validator, maybe one that can work with $request->getUploadedFiles()
        $messages = $validation->validate($_FILES);

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            $this->process_request_values_before_save($request);
            $results = $this->handle_upload($request);
            if ($results['ok']) {
                return $this;
            } else {
                Bootstrap::log($results);

                return $this->getMessages();
            }
        }
    }

    /**
     * Saves data for an existing entity
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function backend_save($request)
    {
        $this->process_request_values_before_save($request);

        if (true === $this->update()) {
            return $this;
        } else {
            Bootstrap::log($this->getMessages());

            return $this->getMessages();
        }
    }

    /**
     * @param \Phalcon\Http\RequestInterface $request
     */
    public function process_request_values_before_save($request)
    {
        $fields_strings = array(
            'title',
            'author',
            'description',
            'copyright',
            'keywords'
        );
        foreach ($fields_strings as $field) {
            $this->$field = $request->hasPost($field) ? $request->getPost($field) : null;
            // Reset back to null if empty after trimming
            if (null !== $this->$field) {
                $this->$field = trim($this->$field);
                if (empty($this->$field)) {
                    $this->$field = null;
                }
            }
        }
    }

    /**
     * @param string $filename
     * @param int $type
     *
     * @return bool|null|string
     * @throws \Exception
     */
    public function build_upload_pathname($filename, $type)
    {
        $type_prefix = $this->get_storage_type_prefix($type);
        $base        = $this->base_dir . '/' . $type_prefix;

        $opts     = Utils::fqpn_options(array(
            'basedir'             => $base,
            'create_missing_dirs' => true,
            'remove_accents'      => true
        ));
        $pathname = Utils::get_fqpn($filename, $opts);

        if (false === $pathname) {
            throw new \Exception('Failed creating a valid upload pathname!');
        }

        return $pathname;
    }

    /**
     * Handles the upload shenanigans. Even backend media upload calls it.
     * TODO: simplify/abstract away as much as possible.
     *
     * @param null|$request \Phalcon\Http\RequestInterface
     * @param bool $generate_backend_thumb
     *
     * @return array Hash of messages
     */
    public function handle_upload($request = null, $generate_backend_thumb = true)
    {
        $results = array(
            'ok'    => false,
            'msg'   => 'Nothing uploaded',
            'media' => null
        );

        // Checks the special case of having low `post_max_size` in php.ini
        // which results in $_FILES and $_POST being completely empty
        $post_size_check_results = Utils::is_post_size_exceeded();
        if (false !== $post_size_check_results) {
            $results['msg'] = $post_size_check_results;
        }

        if ($request) {
            if (!$request->hasFiles()) {
                return $results;
            }
            /* @var $files \Phalcon\Http\Request\File[] */
            $files = $request->getUploadedFiles();
        } else {
            if (isset($this->custom_files) && count($this->custom_files)) {
                /* @var $files \Phalcon\Http\Request\File[] */
                $files = $this->custom_files;
            } else {
                return $results;
            }
        }

        $files_data = array();
        $had_errors = false;

        foreach ($files as $file) {
            if (file_exists($file->getTempName())) {
                $this->id = null;
                $file_data = array();

                // Storing locally and returning the data about each file
                $this->set_defaults();
                $mime_real       = $file->getRealType();
                $filename_orig   = $file->getName();
                $temp            = $file->getTempName();
                $extension       = Utils::get_filename_extension($filename_orig);
                $type            = $this->get_type_from_mimetype($mime_real, $extension);
                $hash            = sha1_file($temp);
                $hashed_filename = sprintf('%s.%s', $hash, $extension);
                $target_pathname = $this->build_upload_pathname($hashed_filename, $type);

                /**
                 * Checking if destination already exists, and if so modifying the
                 * filename so it doesn't collide with any existing files (since basing the
                 * filename on sha1_file() produces identical filenames for files that
                 * have identical content
                 */
                if (file_exists($target_pathname)) {
                    $info            = pathinfo($target_pathname);
                    $unique_filename = Utils::build_unique_filename($info['dirname'], $info['basename']);
                    $hashed_filename = $unique_filename;
                    $target_pathname = $info['dirname'] . '/' . $unique_filename;
                }

                $this->mimetype_orig = $file->getType();
                $this->mimetype      = $mime_real;
                $this->type          = $type;
                $this->filename_orig = $filename_orig;
                $this->filename      = $hashed_filename;

                // Try moving the file and then saving
                if ($file->moveTo($target_pathname)) {
                    $saved = $this->create();
                    if ($saved) {
                        $had_errors        = false;
                        $file_data['id']   = $this->id;
                        $file_data['mime'] = $this->mimetype;
                        $file_data['orig'] = $this->filename_orig;
                        $file_data['type'] = $this->get_type_slug($this->type);
                        // TODO: add edit/delete links for admins?
                        /*
                        if ($this->getDI()->get('auth')->logged_in('admin')) {
                            $file_data['edit_link'] = 'admin/media/edit/' . $file_data['id'];
                            $file_data['delete_link'] = 'admin/media/delete/' . $file_data['id'];
                        }
                        //*/
                        // Create the thumb for backend purposes while we're here
                        $file_data['preview'] = $generate_backend_thumb ? $this->get_backend_thumb() : null;

                        // TODO: do we want/need to create all the defined image styles right here? it could
                        // slow down the upload process for the user (but generally upload is slow anyway...)
                        // ... and it might create images on disk that end up never being used...
                    } else {
                        $had_errors = true;

                        // Remove the moved file since we couldn't save it for some reason...
                        unlink($target_pathname);

                        // Phalcon\Validation\Message\Group does not have toArray() or similar...
                        $msgs = array();
                        foreach ($this->getMessages() as $msg) {
                            $msgs[] = $msg->getMessage();
                        }
                        $msg_string       = implode(', ', $msgs);
                        $file_data['msg'] = sprintf('Validation errors: %s', $msg_string);
                    }
                } else {
                    $had_errors = true;
                    $msg        = sprintf('Failed moving file to target location');
                    $debug      = $this->getDI()->get('config')->app->debug;
                    if ($debug) {
                        $msg = sprintf('Failed moving "%s" ("%s") to "%s"', $filename_orig, $temp, $target_pathname);
                    }
                    // throw new \Exception($msg);
                    $file_data['msg'] = $msg;
                }

                $file_data['ok'] = !$had_errors;
                $files_data[]    = $file_data;
            }
        }

        $results['media'] = $files_data;
        $results['ok']    = !$had_errors;

        if ($results['ok']) {
            $results['msg'] = 'Upload(s) completed successfully. ' . count($results['media']) . ' file(s) uploaded.';
        }

        return $results;
    }

    public function setCustomFiles($files_data)
    {
        if (is_array($files_data) && count($files_data)) {
            $this->custom_files = array();
            foreach ($files_data as $file_data) {
                $this->custom_files[] = new \Baseapp\Extension\Request\CustomFile($file_data, 'ad_media');
            }
        }
    }

    public function setCustomFile($file_data)
    {
        if (is_array($file_data)) {
            $this->custom_files = array();
            $this->custom_files[] = new \Baseapp\Extension\Request\CustomFile($file_data, 'ad_media');
        }
    }
}
