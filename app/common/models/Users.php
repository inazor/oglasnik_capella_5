<?php

namespace Baseapp\Models;

use Baseapp\Extension\Validator\BirthDate;
use Baseapp\Library\Email;
use Baseapp\Library\Utils;
use Baseapp\Library\Validations\UsersBackend as UsersValidationsBackend;
use Baseapp\Library\Validations\UsersBackendRelaxed as UsersValidationsBackendRelaxed;
use Baseapp\Bootstrap;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Phalcon\Db\Column;
use Phalcon\Di;
use Phalcon\Escaper;
use Phalcon\Http\RequestInterface;
use Phalcon\Validation as PhValidation;
use Baseapp\Traits\InfractionReportsHelpers;

/**
 * Users Model
 */
class Users extends BaseModelBlamable
{
    use InfractionReportsHelpers;

    const TYPE_PERSONAL = 1;
    const TYPE_COMPANY = 2;

    public $banned = 0;
    public $ban_reason = '';
    public $suspended = 0;
    public $birth_date = null;
    public $phone1_public = 0;
    public $phone2_public = 0;
    public $active = 0;
    public $country_id = 1;
    public $county_id = null;

    protected $reportReasons = array(
        'Predaje lažne/prevarne oglase',
        'Spam oglasima',
        'Spam porukama',
        'Neprimjerena profilna ili cover fotografija',
        'Ostalo'
    );

    /**
     * Users initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->hasMany('id', __NAMESPACE__ . '\Tokens', 'user_id', array(
            'alias'      => 'Tokens',
            'foreignKey' => array(
                'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE
            ),
            'reusable'   => true
        ));
        $this->hasMany('id', __NAMESPACE__ . '\RolesUsers', 'user_id', array(
            'alias'      => 'Roles',
            'foreignKey' => array(
                'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE
            ),
            'reusable'   => true
        ));
        $this->belongsTo('country_id', __NAMESPACE__ . '\Locations', 'id', array(
            'alias'      => 'Country',
            'reusable'   => true
        ));
        $this->belongsTo('county_id', __NAMESPACE__ . '\Locations', 'id', array(
            'alias'      => 'County',
            'reusable'   => true
        ));
        $this->hasMany('id', __NAMESPACE__ . '\Ads', 'user_id', array(
            'alias'      => 'Ads',
            'foreignKey' => array(
                'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE
            ),
            'reusable'   => true
        ));
        $this->hasMany('id', __NAMESPACE__ . '\Media', 'created_by_user_id', array(
            'alias'      => 'MediaCreated',
            'foreignKey' => array(
                'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE
            ),
            'reusable'   => true
        ));
        $this->hasMany('id', __NAMESPACE__ . '\Media', 'modified_by_user_id', array(
            'alias'      => 'MediaModified',
            'foreignKey' => array(
                'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE
            ),
            'reusable'   => true
        ));
        $this->hasOne(
            'id', __NAMESPACE__ . '\UsersShops', 'user_id',
            array(
                'alias'    => 'Shop',
                'reusable' => true
            )
        );
        $this->hasManyToMany(
            'id', __NAMESPACE__ . '\UsersFavoriteAds', 'user_id',
            'ad_id', __NAMESPACE__ . '\Ads', 'id',
            array(
                'alias'    => 'FavoriteAds',
                'reusable' => true
            )
        );
        $this->hasOne(
            'id', __NAMESPACE__ . '\UsersProfileImages', 'user_id',
            array(
                'alias' => 'ProfileImages',
                'reusable' => true
            )
        );
    }

    public function getShop()
    {
        return UsersShops::findFirst('user_id = ' . $this->id);
    }

    public function getUserMessages($return_builder = false)
    {
        $messages = new UsersMessages();
        return $messages->getForUser($this, $return_builder);
    }

    public function getUserMessagesById($message_id)
    {
        $message_ids = array();

        if (is_array($message_id)) {
            $message_ids = $message_id;
        } elseif (strpos($message_id, ',') !== false) {
            $message_ids = explode(',', $message_id);
        } elseif ((int) $message_id) {
            $message_ids[] = (int) $message_id;
        }

        if (count($message_ids)) {
            return UsersMessages::find(array(
                'conditions' => "id IN ({message_ids:array}) AND (user_id = :user_id: OR (user_id IS NULL AND entity = 'notice'))",
                'bind'       => array(
                    'message_ids' => $message_ids,
                    'user_id'     => (int) $this->id
                )
            ));
        }

        return null;
    }

    public function getUserMessagesIDsFromResultset($resultset)
    {
        $message_ids = array();

        if ($resultset) {
            foreach ($resultset as $message) {
                $message_ids[] = $message->id;
            }
        }

        return count($message_ids) ? $message_ids : null;
    }

    public function markMessageAsReadById($message_id)
    {
        $messages = $this->getUserMessagesById($message_id);
        $message_ids = $this->getUserMessagesIDsFromResultset($messages);

        if ($message_ids) {
            return UsersMessagesStates::markAsRead($this->id, $message_ids);
        }

        return false;
    }

    public function getUnreadNotificationsCount()
    {
        $builder = new \Phalcon\Mvc\Model\Query\Builder();
        $builder->addFrom('Baseapp\Models\UsersMessages', 'um');
        $builder->leftJoin('Baseapp\Models\UsersMessagesStates', 'um.id = ums.user_message_id', 'ums');
        $builder->where(
            'um.user_id = :user_id: AND ums.read_at IS NULL',
            array(
                'user_id' => $this->id
            )
        );

        return count($builder->getQuery()->execute());
    }

    public function assignDefaults($extras = array())
    {
        $defaults = array(
            'type' => self::TYPE_PERSONAL,
            'newsletter' => 0,
            'active' => 0,
            'logins' => 0,
            'last_login' => null,
            'banned' => 0,
            'ban_reason' => '',
            'suspended' => 0,
        );
        $data = array_merge($defaults, $extras);

        return $this->assign($data);
    }

    /**
     * Activation method
     */
    public function activation()
    {
        if ($this->get_role('login')) {
            // User already has the login role (activation already completed probably)
            return null;
        } else {
            return $this->insert_login_role_and_set_active();
        }
    }

    /**
     * Checks if the user is active (has the 'login' role).
     *
     * @return bool Returns true if the user has the 'login' role,
     *              which means he can login, and has activated his
     *              account at some point (because the 'login' role is granted
     *              when an e-mail address is confirmed/activated)
     */
    public function is_active_by_having_login_role() {
        return (false !== $this->get_role('login'));
    }

    public function is_active() {
        return $this->active;
    }

    /**
     * Get/check user's role (if assigned). Returns the specified role (if assigned) or null.
     *
     * @param string $role_name Role to check for
     *
     * @return false|array False if role is not assigned (or doesn't exist at all), or the matched role (as an array)
     */
    public function get_role($role_name = 'login')
    {
        $roles = $this->get_roles_joined();

        foreach ($roles as $role) {
            if ($role['name'] === $role_name) {
                return $role;
            }
        }

        return false;
    }

    /**
     * Returns an array (potentially empty) of Roles assigned to the current user.
     *
     * @return array
     */
    public function get_roles_joined()
    {
        $assigned_roles = array();

        if (isset($this->id)) {
            $assigned_roles = $this->getModelsManager()->executeQuery(
                "SELECT r.* " .
                "FROM Baseapp\Models\Roles AS r " .
                "JOIN Baseapp\Models\RolesUsers AS ru " .
                "ON r.id = ru.role_id " .
                "WHERE ru.user_id = " . $this->id);
            $assigned_roles = $assigned_roles->toArray();
        }

        return $assigned_roles;
    }

    private function getValidPhoneNumber($phoneNumber, $country_iso_code = 'HR')
    {
        $validPhoneNumber = null;

        $parsed = Utils::parsePhoneNumber($phoneNumber, $country_iso_code);
        if ($parsed['valid']) {
            $validPhoneNumber = $parsed['international'];
        }

        return $validPhoneNumber;
    }

    public function get_public_phone_numbers($country_iso_code = null)
    {
        $public_phone_numbers = array();

        if (!$country_iso_code) {
            $country_id      = $this->country_id ? $this->country_id : 1;
            $country         = Locations::findFirst($country_id);
            $country_iso_code = $country->iso_code;
        }

        if (!empty($this->phone1) && $this->phone1_public && $validPhoneNumber = $this->getValidPhoneNumber($this->phone1, $country_iso_code)) {
            $public_phone_numbers[] = $validPhoneNumber;
        }
        if (!empty($this->phone2) && $this->phone2_public && $validPhoneNumber = $this->getValidPhoneNumber($this->phone2, $country_iso_code)) {
            $public_phone_numbers[] = $validPhoneNumber;
        }

        $public_phone_numbers = array_pad($public_phone_numbers, 2, '');

        return $public_phone_numbers;
    }

    public function getCountryISOCode()
    {
        $country_id   = $this->country_id ? $this->country_id : 1;
        $country      = Locations::findFirst($country_id);
        return $country->iso_code;
    }

    public function get_public_details()
    {
        $country_id           = $this->country_id ? $this->country_id : 1;
        $country              = Locations::findFirst($country_id);
        $country_name         = $country->name;
        $country_iso_code     = $country->iso_code;
        $public_phone_numbers = $this->get_public_phone_numbers($country_iso_code);
        $location             = '';
        if ($this->county_id) {
            if ($county = Locations::findFirst($this->county_id)) {
                $location = $county->name . ', ' . $country_name;
            } else {
                $location = $country_name;
            }
        } else {
            $location = $country_name;
        }

        $public_details = array(
            'username'       => $this->username,
            'company_name'   => $this->company_name,
            'country_id'     => $this->country_id,
            'county_id'      => $this->county_id,
            'location'       => $location,
            'phone1'         => $public_phone_numbers[0],
            'phone1_details' => Utils::parsePhoneNumber($public_phone_numbers[0], $country_iso_code),
            'phone2'         => $public_phone_numbers[1],
            'phone2_details' => Utils::parsePhoneNumber($public_phone_numbers[1], $country_iso_code),
            'created_date'   => ''
        );
        if ($this->created_at && $this->created_at !== '0000-00-00 00:00:00') {
            $public_details['created_date'] = date('d.m.Y.', strtotime($this->created_at));
        }

        return $public_details;
    }

    /**
     * Updates roles for an existing user by first removing any existing roles
     * and then assigning newly specified ones.
     *
     * This allows the removal of all roles for a given user (by un-checking all of the roles on /admin/users/edit and
     * hitting save without any roles assigned).
     *
     * @param array $roles
     */
    public function update_roles($roles = array())
    {
        // Nuke any existing roles before assigning new ones
        // NB: this allows removal of all roles for a user when editing it and unchecking everything
        if ($this->Roles->valid()) {
            $this->Roles->delete();
        }

        $this->assignRoles($roles);
    }

    /**
     * Attempts to create RolesUsers objects and assign them to the Roles relation alias.
     *
     * Once the role objects are assigned, they're not automatically saved, one still has to
     * call update()/create()/save() explicitly!
     *
     * @param array $roles
     */
    public function assignRoles($roles = array()) {
        $role_objects = $this->build_roles($roles);
        if ($role_objects) {
            // NB: this is using the Roles relation alias and is not "saved" unless create()/update()/save() is called!
            $this->Roles = $role_objects;
        }
    }

    /**
     * Creates and returns an array of RolesUsers objects.
     * The objects are created based on the type of each value given in $roles parameter.
     *
     * If the value is a string, it's treated as a role name and a corresponding role_id is
     * queried/retrieved from the Roles model (if a role with a given name exists).
     *
     * If the value is numeric, it's treated as a role_id, without any further checks (so you can
     * potentially assign a non-existing role that way, but OTOH we avoid an extra query)
     *
     * @param array $roles
     *
     * @return array|null
     */
    protected function build_roles($roles = array())
    {
        if (!is_array($roles)) {
            $roles = (array) $roles;
        }

        $role_objects = null;

        // Go through the passed in array and create actual
        // RolesUsers() objects based on the type of value:
        // - if it's a string, it's treated as a role name and a corresponding role_id is queried for that name
        // - if it's numeric, it's treated as role_id, avoiding the extra query
        if (!empty($roles)) {
            $role_objects = array();
            // For not-yet-saved objects (aka new user creation) we assign null here
            // and avoid a notice about undefined 'id', but Phalcon's relations appear to
            // be assigning things properly later when we call the model save/create methods
            $user_id = !empty($this->id) ? $this->id : null;
            foreach ($roles as $name_or_id) {
                $role = new RolesUsers();
                $role->user_id = $user_id;
                if (is_numeric($name_or_id)) {
                    $role->role_id = (int) $name_or_id;
                } else {
                    $role_id = Roles::findFirst(array('name="' . $name_or_id . '"'))->id;
                    if ($role_id) {
                        $role->role_id = $role_id;
                    }
                }
                // Avoids potentially non-existing roles
                if (isset($role->role_id)) {
                    $role_objects[] = $role;
                }
            }
        }

        return $role_objects;
    }

    public function insert_login_role()
    {
        // Insert login role record
        $inserted = $this->insert_roles(array('login'));
        if ($inserted) {
            return true;
        } else {
            Bootstrap::log($this->getMessages());
            return $this->getMessages();
        }
    }

    public function insert_login_role_and_set_active()
    {
        // Insert login role record
        $inserted = $this->insert_roles(array('login'));
        if ($inserted) {
            // Update active flag
            $this->update(array('active' => 1));
            return true;
        } else {
            Bootstrap::log($this->getMessages());
            return $this->getMessages();
        }
    }

    /**
     * Insert/Create records in roles_users directly (instead of just setting the model relation alias and
     * waiting for update()/create()/save() to be called.
     * Wrapped in a transaction.
     *
     * @param array $roles Array of role names (or ids)
     *
     * @return bool
     */
    public function insert_roles($roles = array())
    {
        $roles = $this->build_roles($roles);
        if ($roles) {
            $db = $this->getDI()->get('db');
            $db->begin();
            foreach ($roles as $role) {
                if (true !== $role->create()) {
                    $db->rollback();
                    return false;
                }
            }
            $db->commit();
            return true;
        }
        return false;
    }

    protected function addExtraFieldValidations(PhValidation &$validation, RequestInterface &$request)
    {
        // Validate birth_date if sent
        $birth_date = trim($request->getPost('birth_date'));
        if (!empty($birth_date)) {
            $validation->add('birth_date', new BirthDate(array(
                'format'  => 'Y-m-d',
                'minYear' => 1901,
                // TODO: this is due to using DATE field for `birth_date` currently, which is limited... we
                // should either switch to DATETIME or maybe even just store it as a string...
                'maxYear' => 2037,
                'message' => 'Molimo unesite valjani datum rođenja',
            )));
        }

        // Validate zip code
        $zip_code = trim($request->getPost('zip_code'));
        if (!empty($zip_code)) {
            $validation->add('zip_code', new PhValidation\Validator\Regex(array(
                'pattern' => '/^\d+$/',
                'message' => 'Neispravan poštanski broj'
            )));
        }
    }

    /**
     * Frontend user sign-up logic
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|PhValidation\Message\Group|void
     */
    public function signup($request)
    {
        $registration_type = $request->getPost('registration_type');

        if (self::TYPE_COMPANY == $registration_type) {
            $validation = new \Baseapp\Library\Validations\UsersFrontendCompany();
        } else {
            $validation = new \Baseapp\Library\Validations\UsersFrontend();
        }

        $validation = $validation->get();

        // if private user has entered OIB during signup process, validate it
        if (self::TYPE_PERSONAL == $registration_type && $request->getPost('oib', 'string', null)) {
            $validation->add('oib', new \Baseapp\Extension\Validator\OibSimple(
                array(
                    'message' => 'Upisani OIB nema traženi broj znamenki',
                    'messageDigit' => 'Upisani OIB trebao bi se sastojati samo od brojki'
                )
            ));
            // Avoids duplicate OIBs
            $validation->add('oib', new \Baseapp\Extension\Validator\Uniqueness(
                array(
                    'model' => '\Baseapp\Models\Users',
                    'message' => 'Već postoji korisnik s upisanim OIB-om'
                )
            ));
        }

        // validate terms as well
        $validation->add('terms', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Niste prihvatili uvjete korištenja'
        )));

        // Validate some extra fields if they're filled in
        $this->addExtraFieldValidations($validation, $request);

        $messages = $validation->validate($request->getPost());

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            $this->assignDefaults(array(
                'username' => $request->getPost('username'),
                'email' => $request->getPost('email'),
                'type' => $registration_type,
                'newsletter' => $request->hasPost('newsletter') ? 1 : 0
            ));

            $this->process_request_values_before_save($request);

            if (true === $this->create()) {
                $this->send_activation_email();
                return $this;
            } else {
                Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Creates a new user account from the backend
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|PhValidation\Message\Group|void
     */
    public function backend_signup($request)
    {
        $this->assignDefaults();
        $this->populate_model_data_with_request_values($request, 'backend');

        $validation = new UsersValidationsBackend();
        $validation = $validation->get();

        // backend sign-up additionally requires a password
        $validation->add('password', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Password cannot be empty'
        )));

        $messages = $validation->validate($request->getPost());

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            // TODO/FIXME: what's the difference between populate_model_data_with_post_values() and this?
            $this->process_request_values_before_save($request);

            if (true === $this->create()) {
                if ('add' === $request->getPost('action') && $request->hasPost('send_email')) {
                    $this->send_activation_email();
                }
                return $this;
            } else {
                Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Saves data for an existing user account (frontend)
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function frontend_save($request)
    {
        // avoid validation for certain fields that haven't been changed
        $unique_if_changed = array('oib');
        $require_unique = array();
        foreach ($unique_if_changed as $field) {
            if ($this->$field != $request->getPost($field)) {
                $require_unique[] = $field;
            }
        }

        if (self::TYPE_COMPANY == $this->type) {
            $validation = new \Baseapp\Library\Validations\UsersFrontendEditCompany();
        } else {
            $validation = new \Baseapp\Library\Validations\UsersFrontendEdit();
        }
        $validation->set_unique($require_unique);
        $validation = $validation->get();

        // Validate extra fields if they're set
        $this->addExtraFieldValidations($validation, $request);

        $messages = $validation->validate($request->getPost());

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            $this->populate_model_data_with_request_values($request, 'frontend');
            if (true === $this->update()) {
                return $this;
            } else {
                Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Saves data for an existing user account (backend)
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function backend_save($request)
    {
        // avoid validation for certain fields that haven't been changed
        $unique_if_changed = array('username', 'email');
        $require_unique = array();
        foreach ($unique_if_changed as $field) {
            if ($this->$field != $request->getPost($field)) {
                $require_unique[] = $field;
            }
        }

        $validation = new UsersValidationsBackendRelaxed();
        $validation->set_unique($require_unique);

        $messages = $validation->validate($request->getPost());

        $this->populate_model_data_with_request_values($request, 'backend');

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            // TODO/FIXME: what's the difference between populate_model_data_with_request_values() and this?
            $this->process_request_values_before_save($request);

            if (true === $this->update()) {
                return $this;
            } else {
                Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Prepares and sends an activation email message.
     *
     * @return bool
     * @throws \phpmailerException depending on Email class config
     */
    public function send_activation_email()
    {
        $title = 'Oglasnik.hr - aktivacija korisničkog računa';
        $email = new Email();
        $email->prepare(
            $title,
            $this->email,
            'activation',
            array(
                'title'    => $title,
                'username' => $this->username,
                'hash'     => md5($this->id . $this->email . $this->password . $this->getDI()->getShared('config')->auth->hash_key)
            )
        );
        $email->Send();
    }

    /**
     * Prepares and sends a welcome email message for oauth signups.
     *
     * @return bool
     * @throws \phpmailerException depending on Email class config
     */
    public function send_oauth_welcome_email()
    {
        $title = 'Oglasnik.hr - otvaranje korisničkog računa';
        $email = new Email();
        $email->prepare(
            $title,
            $this->email,
            'oauthwelcome',
            array(
                'title'    => $title,
                'username' => $this->username
            )
        );
        $email->Send();
    }

    public function send_pwdreset_email($token)
    {
        $title = 'Oglasnik.hr - link za promjenu lozinke';
        $email = new Email();
        $email->prepare(
            $title,
            $this->email,
            'pwdreset',
            array(
                'title'    => $title,
                'username' => $this->username,
                'token'    => $token,
                'url'      => $this->getDI()->getShared('url')->get('user/pwdreset/' . $token)
            )
        );
        $email->send();
    }

    /**
     * Prepares and sends an email message after a successful password change
     */
    public function send_pwdchanged_email()
    {
        $title = 'Oglasnik.hr - Vaša lozinka je uspješno promjenjena';
        $email = new Email();
        $email->prepare(
            $title,
            $this->email,
            'pwdchanged',
            array(
                'title'    => $title,
                'username' => $this->username,
            )
        );
        $email->send();
    }

    /**
     * @param \Phalcon\Http\RequestInterface $request
     */
    public function process_request_values_before_save($request)
    {
        // TODO: password hashing can probably be made automatic by defining a setter
        // on the Users model directly, which could automatically hash the password as soon as it's set,
        // but not sure if we want that currently, and what will be any future implications
        if ($request->hasPost('password')) {
            $pwd = $request->getPost('password');
            if (!empty($pwd)) {
                $this->password = $this->getDI()->getShared('auth')->hash_password($pwd);
            } else {
                // Avoids non-null default validation in case no (or empty) password was submitted
                $this->skipAttributesOnUpdate(array('password'));
            }
        }

        $fields_strings = array(
            'first_name',
            'last_name',
            'oib',
            'birth_date',
            'company_name',
            'phone1',
            'phone2',
            'zip_code',
            'city',
            'address',
            'ban_reason',
        );
        foreach ($fields_strings as $field) {
            $this->$field = $request->hasPost($field) ? $request->getPost($field) : null;
        }

        $fields_int_or_null = array(
            'country_id',
            'county_id',
        );
        foreach ($fields_int_or_null as $field) {
            if ($request->hasPost($field)) {
                $value = (int) $request->getPost($field);
                if (!$value) {
                    $value = null;
                }
                $this->$field = $value;
            }
        }

        if (trim($this->phone1)) {
            $this->phone1_public = $request->hasPost('phone1_public') ? 1 : 0;
        } else {
            $this->phone1_public = 0;
        }
        if (trim($this->phone2)) {
            $this->phone2_public = $request->hasPost('phone2_public') ? 1 : 0;
        } else {
            $this->phone2_public = 0;
        }

        $birth_date = $request->hasPost('birth_date') ? strtotime($request->getPost('birth_date')) : 0;
        $this->birth_date = $birth_date ? date('Y-m-d', $birth_date) : null;

        if ($this->getDI()->getShared('auth')->logged_in(array('admin'))) {
            // Assign the user's roles from $_POST (if none are specified (ie. all unchecked), any existing user's roles are deleted)
            // WARN: using $_POST since Phalcon doesn't currently handle array notation in forms at all!
            $roles = isset($_POST['roles']) ? $_POST['roles'] : null;
            $this->update_roles($roles);
        }

        /**
         * TODO/FIXME: what about other defaults? see backend UsersController also, and differences between
         * backend_signup(), backend_save(), signup()?
         * see also fields in populate_model_data_with_request_values below()
         */
    }

    /**
     * Populates model fields/attributes with request data
     *
     * @param \Phalcon\Http\RequestInterface $request
     * @param string $module
     */
    public function populate_model_data_with_request_values($request, $module = 'frontend')
    {
        if ($request->isPost()) {
            /**
             * The general idea here was to populate model fields with
             * something, if there was something submitted so that it can
             * be displayed inside the views in cases of errors and whatnot
             * slightly easier (without checking $_POST in .volt files etc...
             *
             * This also kind of avoids having to disable non-null validations in certain cases.
             *
             * Unfortunately, it also breaks the possibility of deleting contents
             * of certain fields completely...
             *
             * TODO/FIXME: This needs much more careful planning and execution.
             */
            $metaData = $this->getModelsMetaData();
            $fieldTypes = $metaData->getDataTypes($this);

            foreach ($fieldTypes as $field => $fieldType) {
                if ($request->hasPost($field)) {
                    $value = trim($request->getPost($field));
                    $this->$field = null;
                    if ($value) {
                        $this->$field = $value;

                        // Attempt to automatically fix some troubling date data
                        if ($fieldType === Column::TYPE_DATE) {
                            $date = \DateTime::createFromFormat('d.m.Y', $value);
                            if ($date) {
                                $this->$field = $date->format('Y-m-d');
                            }
                        }
                    }
                }
            }

            if ($this->county_id && !$request->hasPost('county_id')) {
                $this->county_id = null;
            }

            $this->newsletter = $request->hasPost('newsletter') ? 1 : 0;
            $this->phone1_public = 0;
            if (isset($this->phone1)) {
                $phone1 = trim($this->phone1);
                if (!empty($phone1)) {
                    $this->phone1_public = $request->hasPost('phone1_public') ? 1 : 0;
                }
            }
            $this->phone2_public = 0;
            if (isset($this->phone2) && !empty($this->phone2)) {
                $phone2 = trim($this->phone2);
                if (!empty($phone2)) {
                    $this->phone2_public = $request->hasPost('phone2_public') ? 1 : 0;
                }
            }
            if ('backend' === $module) {
                $this->active = $request->hasPost('active') ? 1 : 0;
                $this->banned = $request->hasPost('banned') ? 1 : 0;
            }
        }
    }

    /**
     * Ban/Unban user, depending on the current state
     *
     * @return bool Whether the update was successful or not
     */
    public function banToggle()
    {
        $this->disable_notnull_validations();
        $this->banned = (int) !$this->banned;
        $result = $this->update();
        $this->enable_notnull_validations();
        return $result;
    }

    public function getIconMarkup()
    {
        $icon = ($this->type == self::TYPE_COMPANY) ? 'globe' : 'user';
        if ($this->getShop()) {
            $icon = 'shopping-cart';
        }

        $markup = '<span class="fa fa-' . $icon . ' fa-fw"></span>';

        if (!empty($this->remark)) {
            $escaper = new Escaper();
            $remark = nl2br($escaper->escapeHtmlAttr($this->remark));
            $markup = '<span class="fa fa-warning text-danger fa-fw" data-toggle="tooltip" data-type="html" data-placement="right" title="' . $remark . '"></span>' . $markup;
        }

        return $markup;
    }


    /**
     * Get user's favorite ad (or ads if comma-separated string or array of ad_id's given)
     *
     * @param int|array|string $ad_id
     *
     * @return null|\Baseapp\Models\UsersFavoriteAds|\Baseapp\Models\UsersFavoriteAds[]
     */
    public function get_favorite($ad_id)
    {
        $ad_ids = array();

        if (is_array($ad_id)) {
            $ad_ids = $ad_id;
        } elseif (strpos($ad_id, ',') !== false) {
            $ad_ids = explode(',', $ad_id);
        } elseif ((int) $ad_id) {
            $ad_ids[] = (int) $ad_id;
        }

        if (count($ad_ids)) {
            if (count($ad_ids) == 1) {
                return UsersFavoriteAds::findFirst(array(
                    'conditions' => 'ad_id = :ad_id: AND user_id = :user_id:',
                    'bind'       => array(
                        'ad_id'   => $ad_ids[0],
                        'user_id' => $this->id
                    )
                ));
            } else {
                return UsersFavoriteAds::find(array(
                    'conditions' => 'ad_id IN ({ad_id:array}) AND user_id = :user_id:',
                    'bind'       => array(
                        'ad_id'   => $ad_ids,
                        'user_id' => $this->id
                    )
                ));
            }
        }

        return null;
    }

    /**
     * Makes an ad as user's favorite
     *
     * @param int $ad_id
     * @return \Baseapp\Models\UsersFavoriteAds
     */
    public function make_favorite($ad_id)
    {
        $favorite_ad = $this->get_favorite($ad_id);

        if (!$favorite_ad) {
            $favorite_ad = new UsersFavoriteAds();
            $favorite_ad->user_id = (int) $this->id;
            $favorite_ad->ad_id = (int) $ad_id;
            $favorite_ad->save();
        }

        return $favorite_ad;
    }

    /**
     * Removes ad(s) from user's favorites. $ad_id can also be a comma-separated string or array or ad_id's
     *
     * @param int|string|array $ad_id
     * @return null|bool
     */
    public function remove_favorite($ad_id)
    {
        if ($favorite_ads = $this->get_favorite($ad_id)) {
            return $favorite_ads->delete();
        }

        return null;
    }

    /**
     * Save a note for favorited ad
     *
     * @param int $ad_id
     * @param string $note
     *
     * @return boolean
     */
    public function save_favorite_note($ad_id, $note)
    {
        $favorite_ad = $this->get_favorite($ad_id);
        $favorite_ad->note = trim($note) ? trim($note) : null;
        return $favorite_ad->save();
    }

    public function has_oib()
    {
        return !empty($this->oib);
    }

    public function has_fullName()
    {
        return !empty($this->first_name) && !empty($this->last_name);
    }

    public static function buildInvoiceDetailsArray($source)
    {
        $details = array();

        if ($source instanceof Users) {
            $source = $source->toArray();
        }

        if (self::TYPE_PERSONAL == $source['type']) {
            $parts = array();
            if (!empty($source['first_name'])) {
                $parts[] = $source['first_name'];
            }
            if (!empty($source['last_name'])) {
                $parts[] = $source['last_name'];
            }
            $details['CUSTOMERNAME'] = implode(' ', $parts);
            $details['COMPANY']      = 0;
        } else {
            $details['CUSTOMERNAME'] = $source['company_name'];
            $details['COMPANY']      = 1;
        }

        $details['TAXNUMBER']  = $source['oib'];
        $details['EMAIL']      = $source['email'];
        $details['FIRST_NAME'] = $source['first_name'];
        $details['LAST_NAME']  = $source['last_name'];
        $details['STREET']     = $source['address'];
        $details['ZIPCODE']    = $source['zip_code'];
        $details['CITY']       = $source['city'];

        // Setting 'TAXCOUNTRYCODE' based on user's country_id if available
        $country_id = $source['country_id'];
        if ($country_id) {
            $location = Locations::findFirst('id = ' . (int) $country_id);
            if ($location) {
                $country_iso_code = $location->iso_code;
                if (!empty($country_iso_code)) {
                    $details['TAXCOUNTRYCODE'] = $country_iso_code;
                }
            }
        }

        $phone_no   = !empty($source['phone1']) ? $source['phone1'] : (!empty($source['phone2']) ? $source['phone2'] : null);
        $phone_util = PhoneNumberUtil::getInstance();
        try {
            $phone_data = $phone_util->parse($phone_no, 'HR');

            /**
             * This is so that Oglasnik can dump shitty data into a table in Navision,
             * which seems to be handling phone numbers like its 1980 or something...
             * Area code means nothing, but they must have it, so we produce something, but...
             * Read more here if you care: https://github.com/googlei18n/libphonenumber/issues/46
             */
            $nat_dest_code = null;
            $national_significant_number = $phone_util->getNationalSignificantNumber($phone_data);
            $nat_dest_code_len = $phone_util->getLengthOfNationalDestinationCode($phone_data);
            if ($nat_dest_code_len > 0) {
                $nat_dest_code = substr($national_significant_number, 0, $nat_dest_code_len);
                /**
                 * Zero-padding left side up to 2 or 3 digits (based on gotten length) so that:
                 * - +3851... -> 01
                 * - +38521123456 -> 021
                 * - +38598462602 -> 098
                 * ... etc. It will break eventually.
                 */
                $len = strlen($nat_dest_code);
                if ($len < 2) {
                    $nat_dest_code = str_pad($nat_dest_code, 2, '0', STR_PAD_LEFT);
                } else if ($len <= 3) {
                    $nat_dest_code = str_pad($nat_dest_code, 3, '0', STR_PAD_LEFT);
                }
            }

            $phone_formatted = $phone_util->format($phone_data, PhoneNumberFormat::E164);

            $details['TELAREA']        = $nat_dest_code;
            $details['TELPHONENUMBER'] = $phone_formatted;
        } catch (NumberParseException $e) {
            $details['TELAREA']        = null;
            $details['TELPHONENUMBER'] = $phone_no;
        }

        $details['USERNAME'] = $source['username'];

        return $details;
    }

    public function getInvoiceDetails()
    {
        return self::buildInvoiceDetailsArray(clone $this);
    }

    public static function getInvoiceDetailsByUserId($user_id)
    {
        $user = self::findFirst($user_id);

        return self::buildInvoiceDetailsArray($user);
    }

    /**
     * Get users for autosuggest (filtered based on given search string - and by type, if one specified)
     *
     * @param string $search_str
     * @param null|int $type
     *
     * @return null|\Baseapp\Models\Users[]
     */
    public static function get_suggest_values($search_str, $type = null)
    {
        $users = null;

        $search_str = trim($search_str);
        if ($search_str) {
            $search_conditions = "user.username LIKE CONCAT('%', :username:, '%') OR user.first_name LIKE CONCAT('%', :first_name:, '%') OR user.last_name LIKE CONCAT('%', :last_name:, '%') OR user.company_name LIKE CONCAT('%', :company_name:, '%') OR shop.title LIKE CONCAT('%', :shop_name:, '%')";
            $search_params = array(
                'username'     => $search_str,
                'first_name'   => $search_str,
                'last_name'    => $search_str,
                'company_name' => $search_str,
                'shop_name'    => $search_str,
            );

            $builder = new \Phalcon\Mvc\Model\Query\Builder();
            $builder->addFrom('Baseapp\Models\Users', 'user');
            $builder->leftJoin('Baseapp\Models\UsersShops', 'user.id = shop.user_id', 'shop');
            $builder->columns(array('user.id, user.type, user.username, user.first_name, user.last_name, user.company_name, user.email, user.remark, shop.title'));
            if (intval($type)) {
                $builder->where(
                    'user.type = :type:',
                    array(
                        'type' => intval($type)
                    ),
                    array(
                        'type' => \PDO::PARAM_INT
                    )
                );
                $builder->andWhere($search_conditions, $search_params);
            } else {
                $builder->where($search_conditions, $search_params);
            }
            $builder->orderBy('user.username ASC');
            $builder->limit(50);

            $users = $builder->getQuery()->execute();
        }

        return $users;
    }

    public function getAge() {
        $age = null;

        if (!empty($this->birth_date)) {
            $birth_date = new \DateTime($this->birth_date);
            $today = new \DateTime('now');
            $interval = $birth_date->diff($today);

            $age = $interval->y;
        }

        return $age;
    }

    public function isShop() {
        return $this->getActiveShop();
    }

    public function getActiveShop() {
        if ($active_shop = $this->getShop()) {
            $now = Utils::getRequestTime();
            if ($active_shop->published_at > $now || $active_shop->expires_at < $now) {
                $active_shop = null;
            }
        }

        return $active_shop;
    }

    public function getUrl()
    {
        $link = null;

        if (isset($this->id)) {
            $url = 'korisnik/' . $this->username;

            // Use the 'url' resolver service if present, making it more flexible
            $di = $this->getDI();
            if ($di->has('url')) {
                $url = $di->get('url')->get($url);
            } else {
                $url = '/' . $url;
            }

            $link = $url;
        }

        return $link;
    }

    public function get_ads_page()
    {
        $link = $this->getUrl();

        if ($shop = $this->getActiveShop()) {
            $url = 'trgovina/' . $shop->slug;

            // Use the 'url' resolver service if present, making it more flexible
            $di = $this->getDI();
            if ($di->has('url')) {
                $url = $di->get('url')->get($url);
            } else {
                $url = '/' . $url;
            }

            $link = $url;
        }

        return $link;
    }

    public function getDisplayName()
    {
        $parts = array();
        if (!empty($this->first_name)) {
            $parts[] = $this->first_name;
        }
        if (!empty($this->last_name)) {
            $parts[] = $this->last_name;
        }

        $name = implode(' ', $parts);

        return $name;
    }

    public function canBeImpersonated()
    {
        $can_be_impersonated = true;

        if (empty($this->id)) {
            $can_be_impersonated = false;
        }

        // Current user must have login role
        if (!$this->hasRole('login')) {
            $can_be_impersonated = false;
        }

        // Current user must not have admin or support role (impersonating other admins/support users sounds dangerous)
        if ($this->hasRole(array('admin', 'supersupport', 'support', 'sales'))) {
            $can_be_impersonated = false;
        }

        return $can_be_impersonated;
    }

    /**
     * @param string|array $name String or array of strings representing the role names to check
     *
     * @return bool
     */
    public function hasRole($name)
    {
        $has   = false;
        $roles = $this->getAssignedRoleNames();

        if (is_array($name)) {
            foreach ($name as $n) {
                if (isset($roles[$n])) {
                    $has = true;
                    break;
                }
            }
        } else {
            $has = isset($roles[$name]);
        }

        return $has;
    }

    /**
     * @return array
     */
    public function getAssignedRoleNames()
    {
        static $role_names = null;

        if (null === $role_names) {
            $roles      = $this->get_roles_joined();
            $role_names = array();

            foreach ($roles as $k => $role) {
                // This way we can check via isset or something like in_array(), depending on use-case
                $role_names[$role['name']] = $role['name'];
            }
        }

        return $role_names;
    }

    public static function sanitizeUsername($string)
    {
        $username = str_replace('-', '', Utils::slugify($string));

        return $username;
    }

    /**
     * @param array $ids
     *
     * @return \stdClass[] Array of \stdClass objects each with a 'text' and 'value' properties
     */
    public static function decorateUsersDataForJsonFromIds(array $ids)
    {
        $ids = implode(',', array_map(function($id){
            return (int) trim($id);
        }, $ids));

        /* @var $entities Users[] */
        $condition = 'id IN (' . $ids . ') ORDER BY FIELD(id,' . $ids . ')';
        $entities = self::find($condition);

        $data = array();
        foreach ($entities as $user) {
            $o = new \stdClass();
            $o->text = $user->username;
            $o->value = (string) $user->id;

            $data[] = $o;
        }

        return $data;
    }

    /**
     * Returns the shop name and username (if an active shop exists)
     * for backend-display purposes.
     *
     * DOES AN EXTRA QUERY so avoid using it at all costs without thinking of the implications.
     * Currently used only on ads backend edit page as a work-in-progress-proof-of-concept
     * to see what's really going on with https://trello.com/c/tY3iRemv/466
     *
     * @return string
     */
    public function getExpensiveBackendDisplayName()
    {
        $display_name = $this->username;

        // If the user has an (active?) shop, display it's name followed by the username in brackets
        // TODO/FIXME: what happens when the user has more than one shop?
        $shop = $this->getActiveShop();
        if ($shop) {
            $display_name = sprintf('%s (%s)', $shop->getName(), $this->username);
        }

        return $display_name;
    }

    protected function getUsersProfileImagesInstance()
    {
        $images = $this->ProfileImages;

        if (!$images) {
            $images = new UsersProfileImages();
        }

        return $images;
    }

    /**
     * Returns the user's avatar as a Thumb (if there is one uploaded),
     * or the default "no image" avatar.
     *
     * @return \Baseapp\Library\Images\Thumb
     */
    public function getAvatar()
    {
        $images = $this->getUsersProfileImagesInstance();

        return $images->getAvatarImage();
    }

    public function getProfileBg()
    {
        $images = $this->getUsersProfileImagesInstance();

        return $images->getProfileBgImage();
    }

    /**
     * Helper method to get multiple user records at once
     *
     * @param array|string|null $ids Comma-separated list of ids or an array of user ids
     *
     * @return Users[]|null
     */
    public function get_multiple_ids($ids = null)
    {
        $users_items = null;

        if ($ids) {
            if (!is_array($ids)) {
                if (strpos($ids, ',') !== false) {
                    $ids = explode(',', $ids);
                } elseif (is_numeric($ids)) {
                    $ids = array((int) $ids);
                }
            }

            if (count($ids)) {
                $queryBuilder = new \Phalcon\Mvc\Model\Query\Builder();
                $users_items = $queryBuilder
                   ->columns(array('user.*', 'shop.*'))
                   ->addFrom('Baseapp\Models\Users', 'user')
                   ->leftJoin('Baseapp\Models\UsersShops', 'user.id = shop.user_id', 'shop')
                   ->inWhere('user.id', $ids)
                   ->orderBy('FIELD(user.id, ' . implode(',', $ids) . ')')
                   ->getQuery()
                   ->execute();
            }
        }

        return $users_items;
    }

    /**
     * @param array $details
     *
     * @return null|string
     */
    public static function buildInfoMarkup(array $details = array())
    {
        $markup = null;

        if (!empty($details)) {
            $markup = '';
        }

        if (!empty($details['username'])) {
            $markup .= '<h3 class="no-top-margin">' . $details['username'] . '</h3>';
        }

        if (!empty($details['created_date'])) {
            $markup .= '<span class="djelatnost">Korisnik od: ' . $details['created_date'] . '</span>';
        }

        if (!empty($details['location'])) {
            $markup .= '<p class="ad-details-location margin-top-md">' . $details['location'] . '</p>';
        }

        if (!empty($details['phone1_details']) or !empty($details['phone2_details'])) {
            $markup .= '<p class="ad-details-phone margin-top-md">';
            if (!empty($details['phone1_details'])) {
                $markup .= '<a href="tel:' . $details['phone1_details']['international'] .'">';
                $markup .= $details['phone1_details']['formattedNationalNumber'];
                $markup .= '</a>';
            }
            if (!empty($details['phone2_details'])) {
                $markup .= '<br><a href="tel:' . $details['phone2_details']['international'] .'">';
                $markup .= $details['phone2_details']['formattedNationalNumber'];
                $markup .= '</a>';
            }
            $markup .= '</p>';
        }

        return $markup;
    }
}
