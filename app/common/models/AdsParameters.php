<?php

namespace Baseapp\Models;

/**
 * AdsParameters Model
 */
class AdsParameters extends BaseModelBlamable
{

    /**
     * AdsParameters initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->belongsTo(
            'ad_id', __NAMESPACE__ . '\Ads', 'id',
            array(
                'alias' => 'Ad',
                'foreignKey' => array(
                    'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE
                )
            )
        );

        $this->belongsTo(
            'parameter_id', __NAMESPACE__ . '\Parameters', 'id',
            array(
                'alias' => 'Parameter',
                'foreignKey' => array(
                    'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE
                )
            )
        );
    }

    public static function getByPrimaryKeyOrCreateNew($ad_id, $parameter_id, $level, $item_index)
    {
        $ads_parameter = null;

        if ($ad_id) {
            $ads_parameter = self::findFirst(array(
                'conditions' => 'ad_id = :ad_id: AND parameter_id = :parameter_id: AND level = :level: AND item_index = :item_index:',
                'bind'       => array(
                    'ad_id'        => $ad_id,
                    'parameter_id' => $parameter_id,
                    'level'        => $level,
                    'item_index'   => $item_index
                )
            ));
        }

        if (!$ads_parameter) {
            $ads_parameter = new self();
            $ads_parameter->ad_id = $ad_id;
            $ads_parameter->parameter_id = $parameter_id;
            $ads_parameter->level = $level;
            $ads_parameter->item_index = $item_index;
        }

        return $ads_parameter;
    }
}
