<?php

namespace Baseapp\Models;

use Phalcon\Mvc\Model\Resultset;
use Baseapp\Library\Email;
use Baseapp\Library\Utils;

class UsersMessages extends BaseModelBlamable
{
    public $id;
    public $parent_id = null;
    public $thread_id = null;
    public $user_id   = null;
    public $entity;
    public $entity_id;
    public $sender_name;
    public $sender_email;
    public $title     = null;
    public $message;
    public $received_at;
    public $active    = 1;

    public function initialize()
    {
        parent::initialize();

        $this->belongsTo(
            'user_id', __NAMESPACE__ . '\Users', 'id',
            array(
                'alias'      => 'User',
                'reusable'   => true,
            )
        );

        $this->setSource('users_messages');
    }

    public function getUser()
    {
        return $this->user_id ? Users::findFirst($this->user_id) : null;
    }

    public function getRecipient()
    {
        $recipient = null;

        if (isset($this->thread_id) && $this->thread_id && $thread_msg = self::findFirst($this->thread_id)) {
            // get senders name and email from the thread_id's message
            $recipient        = new \stdClass();
            $recipient->id    = $thread_msg->created_by_user_id;
            $recipient->name  = $thread_msg->sender_name;
            $recipient->email = $thread_msg->sender_email;

            return $recipient;
        }

        switch($this->entity) {
            case 'users':
                $recipient = $this->getUser();
                break;
            case 'shops':
                $shop = UsersShops::findFirst($this->entity_id);
                if ($shop->isActive()) {
                    $recipient = $shop;
                } else {
                    $recipient = $this->getUser();
                }
                break;
            case 'ads':
                $recipient = $this->getUser();
                if ($shop = $recipient->getShop()) {
                    $recipient = $shop;
                }
                break;
        }

        return $recipient;
    }

    public function getSender()
    {
        // notices from the app are sent to all users and therefore the user_id field is null and we have no sender
        if (!$this->user_id) {
            return null;
        }

        $sender = new \stdClass();
        if ($this->created_by_user_id) {
            $sender->id = $this->created_by_user_id;
        }
        $sender->name = $this->sender_name;
        $sender->email = $this->sender_email;

        return $sender;
    }

    public function beforeCreate()
    {
        // save info about the sender (created_by_user_id) if we have a logged in user
        if ($logged_in_user = $this->getDI()->getShared('auth')->get_user()) {
            $this->created_by_user_id = $logged_in_user->id;
        }
    }

    public function getForUser(Users $user, $return_builder = false)
    {
        $builder = new \Phalcon\Mvc\Model\Query\Builder();
        $builder
            ->columns(array(
                'um.id',
                'um.parent_id',
                'um.thread_id',
                'um.entity',
                'um.entity_id',
                'um.sender_name',
                'um.sender_email',
                'um.title',
                'um.message',
                'um.received_at',
                'um.created_at',
                'ums.read_at'
            ))
            ->addFrom('Baseapp\Models\UsersMessages', 'um')
            ->leftJoin('Baseapp\Models\UsersMessagesStates', 'um.id = ums.user_message_id AND ums.user_id = ' . $user->id, 'ums')
            ->where(
                '(um.user_id IS NULL AND um.entity = :entity:) OR um.user_id = :user_id:',
                array(
                    'entity' => 'notice',
                    'user_id' => $user->id
                )
            )
            ->andWhere(
                'um.active = 1 AND um.received_at <= :received_at: AND (ums.deleted = 0 OR ums.deleted IS NULL)',
                array(
                    'received_at' => date('Y-m-d H:i:s')
                )
            );
        $builder->orderBy('um.received_at DESC');

        if ($return_builder) {
            return $builder;
        }
        $messages = $builder->getQuery()->execute();

        return $messages;
    }

    /**
     * Transforms the Resultset into a regular array containing all the needed info already formatted
     * so it could just be echo-ed in the view. Also, resulting array contains main ad images.
     *
     * @param Resultset &$rows
     *
     * @return array
     */
    public static function buildResultsArray(Resultset &$rows)
    {
        $results = array();

        // Bail if the Resultset ain't valid
        if (!$rows->valid()) {
            return $results;
        }

        $results   = array();
        $user_ids  = array();
        $shop_ids  = array();
        $ad_ids    = array();

        foreach ($rows as $k => $row) {
            $msg      = $row;
            $result   = $msg->toArray();
            // format datetimes
            $result['received_at'] = $msg->received_at ? date('d.m.Y', strtotime($msg->received_at)) : '';
            $result['created_at']  = $msg->created_at ? date('d.m.Y \u H:i', strtotime($msg->created_at)) : '';
            $result['read_at']     = $msg->read_at ? date('d.m.Y', strtotime($msg->read_at)) : '';
            $result['read']        = $result['read_at'] ? true : false;
            $result['canReply']    = true;

            switch ($msg->entity) {
                case 'notice':
                    $result['title']        = '<strong class="text-primary">' . $msg->title . '</strong>';
                    $result['sender_name']  = 'Oglasnik d.o.o.';
                    $result['sender_email'] = null;
                    $result['canReply']     = false;
                    break;

                case 'ads':
                    $ad_ids[] = $msg->entity_id;
                    $result['message'] = nl2br(strip_tags($msg->message));
                    $label_text = ($msg->parent_id ? 'Re: ' : '') . 'Upit vezan uz oglas';
                    $result['title'] = '<span>' . $label_text . '</span>';
                    break;

                case 'users':
                    $user_ids[] = $msg->entity_id;
                    $result['message'] = nl2br(strip_tags($msg->message));
                    $label_text = ($msg->parent_id ? 'Re: ' : '') . 'Upit na profilu';
                    $result['title'] = '<span>' . $label_text . '</span>';
                    break;

                case 'shops':
                    $shop_ids[] = $msg->entity_id;
                    $result['message'] = nl2br(strip_tags($msg->message));
                    $label_text = ($msg->parent_id ? 'Re: ' : '') . 'Upit vezan uz trgovinu';
                    $result['title'] = '<span>' . $label_text . '</span>';
                    break;
            }
            $results[$k] = $result;
        }

        // enrich $results with appropriate models information
        $users = !empty($user_ids) ? (new Users())->get_multiple_ids($user_ids) : array();
        $shops = !empty($shop_ids) ? (new UsersShops())->get_multiple_ids($shop_ids) : array();
        $ads   = !empty($ad_ids) ? (new Ads())->get_multiple_ids($ad_ids) : array();
        if (!empty($users) || !empty($shops) || !empty($ads)) {
            foreach ($results as $k => $message) {
                $message_entity_id = $message['entity_id'];
                switch($message['entity']) {
                    case 'ads':
                        // update stuff with ad details
                        foreach ($ads as $ad) {
                            if ($ad->id == $message_entity_id) {
                                $message['title'] .= ' <a href="' . $ad->get_frontend_view_link() . '" rel="nofollow" class="text-bold">' . $ad->title . '</a>';
                                break;
                            }
                        }
                        break;
                    case 'users':
                        // update stuff with user details
                        foreach ($users as $row) {
                            $user = isset($row->user) ? $row->user : $row;
                            if ($user->id == $message_entity_id) {
                                $message['title'] .= ' <a href="' . $user->getUrl() . '" rel="nofollow" class="text-bold">' . $user->username . '</a>';
                                break;
                            }
                        }
                        break;
                    case 'shops':
                        // update stuff with shop details
                        foreach ($shops as $row) {
                            $shop = isset($row->shop) ? $row->shop : $row;
                            if ($shop->id == $message_entity_id) {
                                $message['title'] .= ' <a href="' . $shop->getUrl() . '" rel="nofollow" class="text-bold">' . $shop->getName() . '</a>';
                                break;
                            }
                        }
                        break;
                }
                $results[$k] = $message;
            }
        }

        return $results;
    }

    public function setAsRead()
    {
        $message_state = UsersMessagesStates::getOrCreateNew($this);
        $message_state->read_at = date('Y-m-d H:i:s');
        return $message_state->save();
    }

    public function getPartyDetails($party)
    {
        $partyDetails = null;

        if ($party instanceof Users) {
            $partyDetails = array(
                'entity'    => 'users',
                'entity_id' => $party->id,
                'name'      => ($party->type == 1 ? $party->username : $party->company_name),
                'email'     => $party->email
            );
        } elseif ($party instanceof UsersShops) {
            $partyDetails = array(
                'entity'    => 'shops',
                'entity_id' => $party->id,
                'name'      => $party->getName(),
                'email'     => $party->getEmail()
            );
        } elseif ($party instanceof \stdClass) {
            $partyDetails = (array) $party;
        }

        return $partyDetails;
    }

    public static function send($messageData, $template = 'profilecontact', $entity = null)
    {
        $result = false;

        $user_message = new self();
        $user_message->assign($messageData);
        $user_message->received_at  = date('Y-m-d H:i:s', Utils::getRequestTime());
        $user_message->active       = 1;
        $result = $user_message->create();

        $recipientDetails = $user_message->getPartyDetails($user_message->getRecipient());

        if (!$recipientDetails) {
            return $result;
        }

        $additionalFields = array();
        if ($entity) {
            if ($entity instanceof Ads) {
                $additionalFields['ad_title'] = $entity->title;
                $additionalFields['ad_link']  = $entity->get_frontend_view_link();
            }
        }

        // send an actual email to recipient;
        self::sendEmail(
            $recipientDetails['email'], 
            $messageData['sender_name'], 
            $messageData['sender_email'], 
            $messageData['message'],
            $template,
            $additionalFields
        );

        return $result;
    }

    public function sendReply($replyMessage)
    {
        $result           = false;
        $recipientDetails = $this->getPartyDetails($this->getSender());

        if (!$recipientDetails) {
            return $result;
        }

        $senderDetails = $this->getPartyDetails($this->getRecipient());

        if ($this->created_by_user_id) {
            // reply with native db model also
            $user_message = new self();
            $user_message->parent_id    = $this->id;
            $user_message->thread_id    = isset($this->thread_id) && $this->thread_id ? $this->thread_id : $this->id;
            $user_message->user_id      = $this->created_by_user_id;
            $user_message->entity       = $this->entity;
            $user_message->entity_id    = $this->entity_id;
            $user_message->sender_name  = $senderDetails['name'];
            $user_message->sender_email = $senderDetails['email'];
            $user_message->message      = $replyMessage;
            $user_message->received_at  = date('Y-m-d H:i:s', Utils::getRequestTime());
            $user_message->active       = 1;
            $user_message->create();
        }

        // send an actual email to recipient;
        return self::sendEmail(
            $recipientDetails['email'], 
            $senderDetails['name'], 
            $senderDetails['email'], 
            $replyMessage
        );
    }

    // this is actually soft-delete!
    public function delete()
    {
        $message_state = UsersMessagesStates::getOrCreateNew($this);
        $message_state->deleted = 1;
        return $message_state->save();
    }

    public static function sendEmail($recipientEmail, $senderName, $senderEmail, $message, $template = 'profilecontact', $additionalFields = array())
    {
        $url = \Phalcon\Di::getDefault()->get('url');

        $title = 'Oglasnik.hr - Kontakt upit';
        $email = new Email();
        $email->prepare(
            $title,
            $recipientEmail,
            $template,
            array_merge(
                array(
                    'title'   => $title,
                    'name'    => $senderName,
                    'email'   => $senderEmail,
                    'message' => $message,
                    'remark'  => 'Za odgovor na ovaj upit, <strong><a href="' . $url->get('user/signin') . '" style="color:#009fe3;">prijavite se u svoj profil</a></strong> na Oglasnik.hr.'
                ),
                $additionalFields
            )
        );
        $email->FromName = $senderName;
        $email->ClearReplyTos();
        $email->addReplyTo($senderEmail, $senderName);
        //$email->addReplyTo('NOREPLY@oglasnik.hr', 'Oglasnik.hr');

        return $email->send();
    }

}
