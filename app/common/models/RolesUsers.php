<?php

namespace Baseapp\Models;

/**
 * User roles Model
 */
class RolesUsers extends BaseModelBlamable
{

    /**
     * RolesUsers initialize
     */
    public function initialize()
    {
        parent::initialize();
        $this->belongsTo('user_id', __NAMESPACE__ . '\Users', 'id', array(
            'alias' => 'User',
            'foreignKey' => true
        ));
        $this->belongsTo('role_id', __NAMESPACE__ . '\Roles', 'id', array(
            'alias' => 'Role',
            'foreignKey' => true
        ));
    }

}
