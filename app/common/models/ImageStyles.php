<?php

namespace Baseapp\Models;

use Baseapp\Bootstrap;
use Baseapp\Library\Utils;
use Baseapp\Traits\CachedAllCollection;

class ImageStyles extends BaseModelBlamable
{
    use CachedAllCollection;

    public $id;
    public $slug;
    public $width;
    public $height;
    public $crop;
    public $watermark;

    // TODO: figure out how/when/if we're gonna implement effects and the order to apply them in
    protected $effects = array();

    // Whether to delete old files when a change in the model is detected (defaults to true so it behaves as it did before)
    protected $checkChangesBeforeUpdate = true;

    public function getSource()
    {
        return 'image_styles';
    }

    public function skipCheckingChangesBeforeUpdate()
    {
        $this->checkChangesBeforeUpdate = false;
    }

    public function check_for_changes()
    {
        // See if any important attribute values are about to change
        // and delete any generated files if that's the case
        try {
            $changed = $this->getChangedFields();
            if (!empty($changed)) {
                // Data from the model before changes
                $original_data = $this->getSnapshotData();
                $this->delete_files($original_data['slug']);
            }
        } catch (\Exception $e) {
            Bootstrap::log($e);
        }
    }

    public function beforeUpdate()
    {
        // Check important attribute changes on update only, and only if specified,
        // that way it can be avoided when needed (for CLI etc.) because deletes can
        // take a VERY long time, and then our changes are not saved at all...
        if ($this->checkChangesBeforeUpdate) {
            $this->check_for_changes();
        }
    }

    public function afterDelete()
    {
        // Bust the 'all styles' cache
        $this->cachedAllInvalidate();

        // Nuke created files/thumbnails from disk
        $this->delete_files($this->slug);
    }

    // Model initialize is called once per request!
    public function initialize()
    {
        $this->hasMany(
            'id', __NAMESPACE__ . '\MediaCrops', 'style_id',
            array(
                'alias'      => 'CroppedMedia',
                'reusable'   => true,
            )
        );

        parent::initialize();

        // Cache entire table on initialize
        $this->cachedAll();
    }

    /**
     * Overriding CachedAllCollection\cachedAllBuildCacheData method in order to change
     * the default cache data to be keyed on both the id and the slug
     * (and not converted to a simple array)
     *
     * @inheritdoc
     */
    public function cachedAllBuildCacheData($records)
    {
        $data = array();

        foreach ($records as $record) {
            $data[$record->id] = $data[$record->slug] = $record;
        }

        return $data;
    }

    /**
     * Delete all generated files in order to re-create them when
     * they are next requested. Called automatically when the dimensions
     * and/or other important style parameters have changed.
     *
     * @param $slug
     *
     * @return bool
     */
    public function delete_files($slug)
    {
        return (new Media())->delete_files_with_slug($slug);
    }

    /**
     * Creates and returns a "temporary" ImageStyles object which can
     * be used to create images but should probably not be saved into the database.
     *
     * @param array $style
     *
     * @return ImageStyles
     */
    public static function fromArray($style = array())
    {
        if (!is_array($style)) {
            $style = (array) $style;
        }

        $defaults = array(
            'id' => null,
            'slug' => 'internal',
            'width' => 100,
            'height' => 100,
            'crop' => true,
            'watermark' => false
        );
        $style = array_merge($defaults, $style);

        // If slug was not specified explicitly, make it a hash of
        // specified values - we'll see how this behaves...
        if ('internal' === $style['slug']) {
            $style['slug'] = md5($style['slug'] . $style['width'] . $style['height'] . $style['crop'] . $style['watermark']);
        }

        $o = new self();

        foreach ($style as $k => $v) {
            $o->$k = $style[$k];
        }

        return $o;
    }

    /**
     * Modifies the originally uploaded filename so that it matches this
     * style's specifications and can be specifically targeted later.
     *
     * TODO: incorporate all the specifics of this style, maybe create an md5 hash
     * of all the style-specific parameter values.
     *
     * @param $original
     *
     * @return string
     */
    public function tweak_filename($original)
    {
        $basename = pathinfo($original, PATHINFO_BASENAME);
        $ext = Utils::get_filename_extension($basename);
        $basename_without_ext = basename($basename, '.' . $ext);

        // TODO: incorporate a hash of crop/watermark/other-stuff too somehow?
        // TODO: So that when values change, we know what/how to delete old files and create new ones?
        // TODO: how to be sure we delete what's changed properly?
        // TODO: What about old/existing references to differently named files that are already generated/linked from somewhere? Do they just 404?

        $new_basename = sprintf('%s-%s.%s', $basename_without_ext, $this->slug, $ext);
        $full = str_replace($basename, $new_basename, $original);

        return $full;
    }

    /**
     * Saves data for a new entity
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function backend_create($request)
    {
        $this->process_request_values($request);

        $validation = new \Baseapp\Library\Validations\ImageStyles();
        $validation->set_unique(array('slug' => array()));

        $messages = $validation->validate($this);

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            $created = $this->create();
            if (true === $created) {
                return $this;
            } else {
                Bootstrap::log($created);
                return $this->getMessages();
            }
        }
    }

    /**
     * Saves data for an existing entity
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function backend_save($request)
    {
        $this->process_request_values($request);

        $validation = new \Baseapp\Library\Validations\ImageStyles();

        // Validate that slug is unique across all the records except the current one
        $extra_conditions = array(
            'id != :id:',
            array(
                'id' => $this->id
            )
        );
        $validation->set_unique(array('slug' => array('extra_conditions' => $extra_conditions)));

        $messages = $validation->validate($this);

        if (count($messages)) {
            return $messages;
        } else {
            if (true === $this->update()) {
                return $this;
            } else {
                Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return null|array
     */
    public function process_request_values($request)
    {
        $new_data = null;

        $fields_strings = array(
            'slug',
        );
        foreach ($fields_strings as $field) {
            $new_data[$field] = $request->hasPost($field) ? $request->getPost($field) : null;
            if (null !== $new_data[$field]) {
                $new_data[$field] = trim($new_data[$field]);
                if (empty($new_data[$field])) {
                    // Reset back to null if empty after trimming
                    $new_data[$field] = null;
                } else {
                    if ('slug' === $field) {
                        $new_data[$field] = trim($new_data[$field]);//Utils::slugify($new_data[$field]);
                    }
                }
            }
        }

        $fields_int_or_null = array(
            'width',
            'height',
        );
        foreach ($fields_int_or_null as $field) {
            if ($request->hasPost($field)) {
                $value = $request->getPost($field);
                if (ctype_digit($value)) {
                    $value = (int) $value;
                }
                if (!$value || $value <= 0) {
                    $value = null;
                }
                $new_data[$field] = $value;
            }
        }

        $fields_bool = array(
            'crop',
            'watermark'
        );
        foreach ($fields_bool as $field) {
            $new_data[$field] = $request->hasPost($field) ? 1 : 0;
        }

        // Assign processed data to the model
        $this->assign($new_data);

        return $new_data;
    }
}
