<?php

namespace Baseapp\Models;

use Baseapp\Library\Utils;
use Phalcon\Mvc\Model\Resultset;

/**
 * SearchQueries Model
 */
class SearchQueries extends BaseModel
{
    /**
     * SearchQueries initialize
     */
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * @param string|null $search_term
     *
     * @return array
     */
    public static function getSuggestions($search_term = null)
    {
        $suggestions_array = array();

        $search_term = trim($search_term);

        $search_params = array(
            'columns'    => 'term, slug',
            'order'      => 'term_volume DESC, search_results DESC',
            'hydration'  => Resultset::HYDRATE_ARRAYS,
            'limit'      => 100
        );
        if ($search_term) {
            $search_params['conditions'] = 'LOWER(term) LIKE CONCAT(\'%\', :search_term:, \'%\')';
            $search_params['bind'] = array('search_term' => mb_strtolower($search_term, 'UTF-8'));
        }

        if ($suggestions = self::find($search_params)) {
            $suggestions_array = $suggestions->toArray();
        }

        // Remove any occurences of 'njuskalo' from the suggestion results
        $suggestions_array = array_filter($suggestions_array, function($term_data){
            $slug = $term_data['slug'];
            return (false === strpos($slug, 'njuskal'));
        });

        return $suggestions_array;
    }

    /**
     * Helper method to get existing or return a new SearchQueries model
     *
     * @param string $search_term
     *
     * @return \Baseapp\Models\SearchQueries
     */
    protected function getOrCreateNew($search_term)
    {
        $search_term = trim($search_term);

        $search_query = self::findFirst(array(
            'conditions' => 'term = :term:',
            'bind'       => array(
                'term' => $search_term
            )
        ));
        if (!$search_query) {
            $search_query = new self();
            $search_query->term = $search_term;
            $search_query->slug = Utils::slugify($search_term);
            $search_query->term_volume = 0;
            $search_query->search_results = 0;
        }

        return $search_query;
    }

    /**
     * Set details for searched term
     *
     * @param string $search_term Search term we'll be working on
     * @param int $search_results Number of results that search returned
     */
    public function setSearchTermDetails($search_term, $search_results)
    {
        $search_term = trim($search_term);
        $search_results = intval($search_results);

        if ($search_term && $search_results) {
            $search_query = $this->getOrCreateNew($search_term);
            $search_query->term_volume++;
            $search_query->search_results = $search_results;
            $search_query->save();
        }
    }
}
