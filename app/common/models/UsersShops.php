<?php

namespace Baseapp\Models;

use Baseapp\Backend\Controllers\ShopsController;
use Baseapp\Library\Utils;
use Baseapp\Library\RawDB;
use Baseapp\Library\Validations\UsersShopsFrontend as ShopsValidationsFrontend;
use Baseapp\Library\Validations\UsersShopsBackend as ShopsValidationsBackend;
use Baseapp\Traits\MediaCommonMethods;
use Phalcon\Di;
use Phalcon\Escaper;
use Baseapp\Traits\InfractionReportsHelpers;

/**
 * UsersShops Model
 */
class UsersShops extends BaseModelBlamable
{
    use InfractionReportsHelpers;
    use MediaCommonMethods;

    public $user_id = null;

    protected $fields_types_map = array(
        'int'     => array(
            'user_id'
        ),
        'dateint' => array(
            'published_at',
            'expires_at',
        ),
        'string'  => array(
            'title',
            'slug',
            'subtitle',
            'about',
            'email',
            'web',
            'default_sorting_order'
        )
    );

    protected $reportReasons = array(
        'Predaje lažne/prevarne oglase',
        'Spam oglasima',
        'Spam porukama',
        'Neprimjerena profilna ili cover fotografija',
        'Ostalo'
    );

    /**
     * UsersShops initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->belongsTo(
            'user_id', __NAMESPACE__ . '\Users', 'id',
            array(
                'alias'    => 'User',
                'reusable' => true
            )
        );

        $this->hasMany(
            'id', __NAMESPACE__ . '\UsersShopsFeatured', 'shop_id',
            array(
                'alias'    => 'FeaturedShops',
                'reusable' => true
            )
        );
    }

    public function beforeCreate()
    {
        $this->created_at = Utils::getRequestTime();
    }

    public function beforeUpdate()
    {
        $this->modified_at = Utils::getRequestTime();
    }

    /**
     * Creates a new shop
     *
     * @param string $module
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    protected function add_new($module, $request)
    {
        if ('frontend' === $module) {
            $validation = new ShopsValidationsFrontend();
        } elseif ('backend' === $module) {
            $validation = new ShopsValidationsBackend();
        }
        $messages = $validation->validate($request->getPost());

        $this->initialize_model_with_post($request);

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            if (true === $this->create()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Save an existing shop
     *
     * @param string $module
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    protected function save_changes($module, $request)
    {
        if ('frontend' === $module) {
            $validation = new ShopsValidationsFrontend();
        } elseif ('backend' === $module) {
            $validation = new ShopsValidationsBackend();
        }
        // avoid validation for certain fields that haven't been changed
        $unique_if_changed = array('slug');
        $require_unique = array();
        foreach ($unique_if_changed as $field) {
            if ($this->$field != $request->getPost($field)) {
                $require_unique[] = $field;
            }
        }
        if ($request->getPost('email')) {
            $validation->validate_if_set(array('email'));
        }
        $validation->set_unique($require_unique);
        $messages = $validation->validate($request->getPost());

        $this->initialize_model_with_post($request);

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            if (true === $this->save()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Creates a new shop from the frontend module
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function frontend_add_new($request)
    {
        return $this->add_new('frontend', $request);
    }

    /**
     * Saves data for an existing shop from frontend
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function frontend_save_changes($request)
    {
        return $this->save_changes('frontend', $request);
    }

    /**
     * Creates a new shop from the backend module
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function backend_add_new($request)
    {
        return $this->add_new('backend', $request);
    }

    /**
     * Saves data for an existing shop from backend
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function backend_save_changes($request)
    {
        return $this->save_changes('backend', $request);
    }

    public function getName()
    {
        $title = $this->title;

        // Try some fallbacks for edge-cases from staging/dev setups
        if (empty($title)) {
            $title = $this->getOwner()->company_name;
            if (empty($title)) {
                $title = 'Untitled';
            }
        }

        return strip_tags($title);
    }

    /**
     * @return null|Users
     */
    public function getOwner()
    {
        $owner = null;

        if ($this->user_id) {
            $owner = Users::findFirst($this->user_id);
        }

        return $owner;
    }

    public function getAvatar()
    {
        return $this->getOwner()->getAvatar();
    }

    public function getAddress($lineBreaks = false)
    {
        $address = array();

        if ($owner = $this->getOwner()) {
            if (!empty($owner->address)) {
                $address[] = trim($owner->address);
            }

            if (!empty($owner->city)) {
                if (!empty($owner->zip_code)) {
                    $address[] = trim($owner->zip_code) . ' ' . trim($owner->city);
                } else {
                    $address[] = trim($owner->city);
                }
            }

            if ($userCountry = $owner->Country) {
                $address[] = trim($userCountry->name);
            }
        }

        return count($address) ? implode(',' . ($lineBreaks ? '<br/>' : '') . ' ', $address) : null;
    }

    public function getPhoneNumbers()
    {
        return $this->getOwner()->get_public_phone_numbers();
    }

    public function getEmail()
    {
        if (!empty($this->email)) {
            return trim($this->email);
        }

        return $this->getOwner()->email;
    }

    public function getWeb()
    {
        $web = null;

        if (!empty($this->web)) {
            $web = trim($this->web);

            if (substr($web, 0, 4) !== 'http') {
                $web = 'http://' . $web;
            }
        }

        return $web;
    }

    public function get_public_details()
    {
        $address       = $this->getAddress(true);
        $phone_numbers = $this->getPhoneNumbers();
        $web           = $this->getWeb();
        $email         = $this->getEmail();

        $country              = $this->getOwner()->Country;
        $country_name         = $country->name;
        $location             = '';
        if ($this->getOwner()->county_id) {
            $location = $this->getOwner()->County->name . ', ' . $country_name;
        } else {
            $location = $country_name;
        }

        $public_details = array(
            'location'       => $address ? $address : $location,
            'location_map'   => $address ? $this->getAddress() : null,
            'phone1'         => $phone_numbers[0],
            'phone1_details' => Utils::parsePhoneNumber($phone_numbers[0], $country->iso_code),
            'phone2'         => $phone_numbers[1],
            'phone2_details' => Utils::parsePhoneNumber($phone_numbers[1], $country->iso_code),
            'email'          => $email,
            'web'            => $web,
            'name'           => $this->getName(),
            'subtitle'       => !empty($this->subtitle) ? $this->subtitle : '',
            'about'          => $this->getAbout()
        );

        return $public_details;
    }

    public function getAbout()
    {
        $about = null;

        if (!empty($this->about)) {
            // Checking for empty paragraphs and simple stuff producing "empty" output
            $about_stripped = trim(strip_tags($this->about));
            if (!empty($about_stripped)) {
                // Not really stripping tags because we need basic formatting

                // TODO: we might have to tighten this up and do server-side stripping too, relying on JS to do it is really dangerous!
                $about = $this->about;
            }
        }

        return $about;
    }

    public function getUrl($type = 'href')
    {
        $link = null;

        if (isset($this->slug)) {
            $url = 'trgovina/' . $this->slug;

            // Use the 'url' resolver service if present, making it more flexible
            $di = $this->getDI();
            if ($di->has('url')) {
                $url = $di->get('url')->get($url);
            } else {
                $url = '/' . $url;
            }

            $link = $url;

            if ('html' === $type) {
                $markup = '<a href="%s" target="_blank"><i class="fa fa-fw fa-eye"></i>%s</a>';
                $link   = sprintf($markup, $url, 'View shop');
            }
        }

        return $link;
    }

    public function getShoppingWindows($params = array())
    {
        if (empty($params)) {
            $params = array(
                'order' => 'id DESC'
            );
        }

        // Using the relationship alias
        $records = $this->getFeaturedShops($params); // FIXME: change this alias it makes no fucking sense

        if ($records && $records->valid()) {
            // TODO: create array data for use in views?
        }

        return $records;
    }

    public static function getFeatured($category_id = null)
    {
        $now = Utils::getRequestTime();

        $builder = new \Phalcon\Mvc\Model\Query\Builder();
        $builder->columns(array('izlog.*', 'shop.*'));
        $builder->addFrom('Baseapp\Models\UsersShopsFeatured', 'izlog');
        $builder->innerJoin('Baseapp\Models\UsersShops', 'shop.id = izlog.shop_id', 'shop');
        $builder->where(
            'izlog.published_at <= :now1: AND izlog.expires_at >= :now2: AND active = 1 AND deleted = 0',
            array(
                'now1' => $now,
                'now2' => $now
            )
        );
        $builder->andWhere(
            'FIND_IN_SET(:category_id:, izlog.categories)',
            array(
                'category_id' => intval($category_id)
            ),
            array(
                'category_id' => \PDO::PARAM_INT
            )
        );
        $builder->orderBy('izlog.expires_at DESC');

        $featured = $builder->getQuery()->execute();

        return $featured;
    }

    public static function getShoppingWindowsByIDs(array $ids = array())
    {
        $now = Utils::getRequestTime();

        $builder = new \Phalcon\Mvc\Model\Query\Builder();
        $builder->columns(array('izlog.*', 'shop.*'));
        $builder->addFrom('Baseapp\Models\UsersShopsFeatured', 'izlog');
        $builder->innerJoin('Baseapp\Models\UsersShops', 'shop.id = izlog.shop_id', 'shop');
        $builder->where(
            'izlog.published_at <= :now1: AND izlog.expires_at >= :now2: AND active = 1 AND deleted = 0',
            array(
                'now1' => $now,
                'now2' => $now
            )
        );
        $builder->inWhere('izlog.id', $ids);
        $builder->orderBy('izlog.expires_at DESC');
        $result = $builder->getQuery()->execute();

        return $result;
    }

    /**
     * @param array $category_ids
     *
     * @return mixed
     */
    public static function queryShoppingWindowsForCategories(array $category_ids = array())
    {
        $now = Utils::getRequestTime();

        $builder = new \Phalcon\Mvc\Model\Query\Builder();
        $builder->columns(array('izlog.*', 'shop.*'));
        $builder->addFrom('Baseapp\Models\UsersShopsFeatured', 'izlog');
        $builder->innerJoin('Baseapp\Models\UsersShops', 'shop.id = izlog.shop_id', 'shop');
        $builder->where(
            'izlog.published_at <= :now1: AND izlog.expires_at >= :now2: AND active = 1 AND deleted = 0',
            array(
                'now1' => $now,
                'now2' => $now
            )
        );

        $cnt = 0;
        $finds = array();
        foreach ($category_ids as $category_id) {
            $cnt++;
            $finds[] = 'FIND_IN_SET(' . (int) $category_id . ', izlog.categories)';
        }
        if (!empty($finds)) {
            $finds_sql = implode(' OR ', $finds);
            $builder->andWhere($finds_sql);
        }

        $builder->orderBy('izlog.expires_at DESC');

        $result = $builder->getQuery()->execute();

        return $result;
    }

    /**
     * @param int $limit
     * @param bool $with_images_only
     * @param array $exclude Ad ids to exclude/avoid
     *
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public function getLatestAds($limit = 3, $with_images_only = true, $exclude = array())
    {
        $query = Ads::query();
        $query->where("user_id = :user_id:")
            ->andWhere("active = 1")
            ->andWhere("is_spam = 0")
            ->bind(array('user_id' => $this->user_id))
            ->orderBy('sort_date DESC')
            ->groupBy('Baseapp\Models\Ads.id')
            ->limit($limit);

        if ($with_images_only) {
            $query->innerJoin('Baseapp\Models\AdsMedia', 'Baseapp\Models\AdsMedia.ad_id = Baseapp\Models\Ads.id');
        }

        if (!empty($exclude)) {
            $query->andWhere('Baseapp\Models\Ads.id NOT IN (' . implode(',', array_map('intval', $exclude)) . ')');
        }

        $results = $query->execute();

        return $results;
    }

    /**
     * @param int $limit
     * @param array $exclude Ad ids to exclude/avoid
     *
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public function getLatestAdsWithImages($limit = 3, $exclude = array())
    {
        return $this->getLatestAds($limit, true, $exclude);
    }

    /**
     * Builds backend-specific markup given a list of "named links" and their text values.
     *
     * @param array $links
     *
     * @return null|string
     */
    protected function buildBackendInfoLinksMarkup($links = array())
    {
        $markup = null;

        $markup_elements = array();
        if (isset($links['shop_edit'])) {
            $markup_elements[] = '<a href="' . $links['shop_edit']['href'] . '"><span class="fa fa-edit fa-fw"></span>' . $links['shop_edit']['text'] . '</a>';
        }
        if (isset($links['user_edit'])) {
            $remark_span = '';
            if (isset($links['user_remark'])) {
                $escaper = new Escaper();
                $links['user_remark'] = $escaper->escapeHtmlAttr($links['user_remark']);
                $remark_span = '<span class="fa fa-warning text-danger" data-toggle="tooltip" data-type="html" data-placement="right" title="' . nl2br($links['user_remark']) . '"></span>';
            }
            $markup_elements[] = '<span class="text-muted">[' . $remark_span . '<span class="fa fa-shopping-cart fa-fw"></span><a href="' . $links['user_edit']['href'] . '">' . $links['user_edit']['text'] . '</a> ]</span>';
        }
        if (isset($links['search'])) {
            $markup_elements[] = '&nbsp;<span class="text-muted">[<span class="fa fa-search fa-fw"></span><a href="' . $links['search']['href'] . '">' . $links['search']['text'] . '</a> ]</span>';
        }
        if (isset($links['delete'])) {
            $markup_elements[] = '&nbsp;<span class="text-muted">[<span class="fa fa-trash-o fa-fw text-danger"></span><a href="' . $links['delete']['href'] . '">' . $links['delete']['text'] . '</a> ]</span>';
        }

        if (!empty($markup_elements)) {
            $markup = implode('&nbsp;', $markup_elements);
        }

        return $markup;
    }

    /**
     * @return null|string
     */
    public function getBackendInfoLinksMarkup()
    {
        $markup = null;

        $owner = $this->getOwner();

        $url_service = $this->getDI()->get('url');
        $shop_link   = $url_service->get('admin/shops/edit/' . $this->id);
        $user_link   = $url_service->get('admin/users/edit/' . $this->user_id);

        $links = array(
            'shop_edit' => array('href' => $shop_link, 'text' => $this->getName()),
            'user_edit' => array('href' => $user_link, 'text' => $owner->username)
        );

        if (!empty($owner->remark)) {
            $links['user_remark'] = trim($owner->remark);
        }

        return $this->buildBackendInfoLinksMarkup($links);
    }

    /**
     * @param \Phalcon\Mvc\Model\Row|array $row
     * @param Users|null $user Optional Users instance (if delete links are to be shown, the user must have adequate role)
     *
     * @return null|string
     */
    public function buildBackendInfoLinksMarkupFromModelRow($row, $user = null)
    {
        $markup = null;

        if (!is_array($row) && method_exists($row, 'toArray')) {
            $row = $row->toArray();
        }

        $url_service = $this->getDI()->get('url');

        $shop_link = $url_service->get('admin/shops/edit/' . $row['id']);
        $user_link = $url_service->get('admin/users/edit/' . $row['user_id']);
        $user_ads_search_link = $url_service->get('admin/ads', array(
            'field' => 'user_id',
            'mode'  => 'exact',
            'q'     => $row['user_id']
        ));

        $shop_title = $row['title'];
        if (empty($shop_title)) {
            $shop_title = $row['company_name'];
        }

        $links = array(
            'shop_edit' => array('href' => $shop_link, 'text' => $shop_title),
            'user_edit' => array('href' => $user_link, 'text' => $row['username']),
            'search'    => array('href' => $user_ads_search_link, 'text' => 'Oglasi korisnika')
        );

        // Add the delete link if we're given a user that can do that
        if (null !== $user) {
            if ($user->hasRole(ShopsController::$can_delete_roles)) {
                $delete_link     = $url_service->get('admin/shops/delete/' . $row['id']);
                $links['delete'] = array('href' => $delete_link, 'text' => 'Delete');
            }
        }

        if (!empty($row['remark'])) {
            $links['user_remark'] = trim($row['remark']);
        }

        return $this->buildBackendInfoLinksMarkup($links);
    }

    /**
     * Invokes the specified $callback with/on each element of the passed-by-reference $items array/iterable and
     * stores the results of each invocation back into $items.
     *
     * @param array &$items Passed by reference
     * @param callable $callback Each $item in $items is passed to $callback as the first parameter
     */
    public static function paginatorItemsCallableDecorator(&$items, $callback)
    {
        if (empty($items)) {
            return;
        }

        foreach ($items as $k => $item) {
            $items[$k] = $callback($item);
        }
    }

    public function isActive()
    {
        $now = Utils::getRequestTime();
        $active = false;

        $published_at = isset($this->published_at) ? $this->published_at : $now;
        $expires_at = isset($this->expires_at) ? $this->expires_at : $now;

        if ($now >= $published_at && $now <= $expires_at) {
            $active = true;
        }

        return $active;
    }

    /**
     * @param string|null $searchTerm
     *
     * @return array
     */
    public static function getSuggestions($searchTerm = null)
    {
        $suggestions_array = array();

        $searchTerm = trim(mb_strtolower($searchTerm, 'UTF-8'));
        $now = Utils::getRequestTime();
        $di = Di::getDefault();

        if ($searchTerm) {
            $sql = "SELECT
                    id, title, slug,
                    ((LOWER(title) LIKE '%$searchTerm%') + (LOWER(slug) LIKE '%$searchTerm%')) AS hits
                FROM " . (new self)->getSource() . "
                WHERE (LOWER(title) LIKE '%$searchTerm%' OR LOWER(slug) LIKE '%$searchTerm%') AND published_at <= $now AND expires_at >= $now
                ORDER BY hits DESC, title ASC";
        } else {
            $sql = "SELECT id, title, slug FROM " . (new self)->getSource() . " WHERE published_at <= $now AND expires_at >= $now ORDER BY title ASC";
        }

        $db = new RawDB(array(
            'options' => array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                \PDO::ATTR_EMULATE_PREPARES   => false,
                \PDO::ATTR_STRINGIFY_FETCHES  => false,
            )
        ));

        if ($rows = $db->find($sql)) {
            foreach ($rows as $row) {
                $suggestions_array[] = array(
                    'id'           => (int) $row['id'],
                    'name'         => trim($row['title']),
                    'frontend_url' => $di->get('url')->get('trgovina/' . $row['slug'])
                );        
            }
        }

        return $suggestions_array;
    }

    /**
     * Helper method to get multiple shop records at once
     *
     * @param array|string|null $ids Comma-separated list of ids or an array of shop ids
     *
     * @return Users[]|null
     */
    public function get_multiple_ids($ids = null)
    {
        $shops = null;

        if ($ids) {
            if (!is_array($ids)) {
                if (strpos($ids, ',') !== false) {
                    $ids = explode(',', $ids);
                } elseif (is_numeric($ids)) {
                    $ids = array((int) $ids);
                }
            }

            if (count($ids)) {
                $queryBuilder = new \Phalcon\Mvc\Model\Query\Builder();
                $shops = $queryBuilder
                   ->columns(array('user.*', 'shop.*'))
                   ->addFrom('Baseapp\Models\Users', 'user')
                   ->innerJoin('Baseapp\Models\UsersShops', 'user.id = shop.user_id', 'shop')
                   ->inWhere('shop.id', $ids)
                   ->orderBy('FIELD(shop.id, ' . implode(',', $ids) . ')')
                   ->getQuery()
                   ->execute();
            }
        }

        return $shops;
    }

    /**
     * @param array $details
     *
     * @return null|string
     */
    public static function buildInfoMarkup(array $details = array())
    {
        $markup = null;

        if (!empty($details)) {
            $markup = '';
        }

        if (!empty($details['name'])) {
            $markup .= '<h3 class="no-top-margin convert-to-tab-xs convert-to-tab-sm initial-closed">' . $details['name'] . '<span class="caret hidden-md hidden-lg"></span></h3>';
        }

        if (!empty($details['subtitle'])) {
            $markup .= '<span class="djelatnost">' . $details['subtitle'] . '</span>';
        }
        if (!empty($details['about'])) {
            $markup .= '<div class="excerpt">' . $details['about'] . '</div>';
        }

        if (!empty($details['location'])) {
            $markup .= '<p class="ad-details-location margin-top-md">' . $details['location'];
            if (!empty($details['location_map'])) {
                $markup .= '<br/><a href="#" class="show-on-map" data-toggle="modal" ';
                $markup .= 'data-target="#modal-inspect-user-map" data-address="' . $details['location_map'] . '">Prikaži na karti</a>';
            }
            $markup .= '</p>';
        }

        if (!empty($details['phone1_details']) or !empty($details['phone2_details']) or !empty($details['email']) or !empty($details['web'])) {
            $markup .= '<p class="ad-details-phone margin-top-md">';
            if (!empty($details['phone1_details'])) {
                $markup .= '<a href="tel:' . $details['phone1_details']['international'] .'">';
                $markup .= $details['phone1_details']['formattedNationalNumber'];
                $markup .= '</a>';
            }
            if (!empty($details['phone2_details'])) {
                $markup .= '<br><a href="tel:' . $details['phone2_details']['international'] .'">';
                $markup .= $details['phone2_details']['formattedNationalNumber'];
                $markup .= '</a>';
            }
            if (!empty($details['email'])) {
                $markup .= '<br><a href="mailto:' . $details['email'] . '">' . $details['email'] . '</a>';
            }
            if (!empty($details['web'])) {
                $markup .= '<br><a href="' . $details['web'] . '" class="truncate">' . $details['web'] . '</a>';
            }
            $markup .= '</p>';
        }

        return $markup;
    }
}
