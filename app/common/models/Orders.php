<?php

namespace Baseapp\Models;

use Baseapp\Library\Debug;
use Baseapp\Library\PDF417\Renderers\ImagickImageRenderer;
use Baseapp\Library\PDF417\Renderers\SvgRenderer;
use Baseapp\Library\Products\Online\Pushup;
use Baseapp\Library\Products\ProductsInterface;
use Baseapp\Library\Products\Trgovina;
use Baseapp\Library\Products\Trgovina\Izlog;
use Baseapp\Library\Utils;
use Illuminate\Support\Facades\HTML;
use Phalcon\Mvc\Model\Query\Builder;
use Phalcon\Mvc\Model\Resultset;
use Baseapp\Traits\BarcodeControllerMethods;

class Orders extends BaseModelBlamable
{
    use BarcodeControllerMethods;

    const STATUS_NEW = 1; // Order record created (when "submit order" is clicked), could be payed offline
    const STATUS_COMPLETED = 2; // Order fulfilled/finalized
    const STATUS_CANCELLED = 3; // Order cancelled by the customer (or admin)
    const STATUS_EXPIRED = 4; // Will probably have to be processed via cron or something...

    // TODO: will probably need a few more statuses in order to be able to differentiate
    // between user/admin cancelled, awaiting payment, maybe more...

    //const PBO_PREFIX = '900';
	const PBO_PREFIX = '500';

    const DAYS_VALID = 30; // Order validity (in days), calculated on record creation

    public $id;
    public $user_id;

    // Default status for new records
    public $status = self::STATUS_NEW;

    public $approval_code = null; // Received from IPG for successful CC payments

    public $cart = ''; // Serialized cart contents
    public $ip = '';

    /**
     * Amount in minor currency units (lp), de-normalized (for easier display purposes mostly).
     * Outside code must use getter/setter because we want it to set the amount in major currency unit
     * and to get it back in the same way, but we don't want to store float/double in the DB due to
     * horrible rounding problems that causes sooner or later.
     *
     * @var string
     * @access private
     */
    protected $total = '0';

    public $created_at; // Y-m-d H:i:s
    public $modified_at; // Y-m-d H:i:s

    // Computed on record creation in beforeValidationOnCreate() using self::DAYS_VALID
    public $valid_until; // Y-m-d

    // Possible payment methods
    private static $payment_methods = array(
        'offline',
        'visa',
        'amex',
        'mastercard',
        'diners',
        'discover',
        'maestro'
        // TODO: what about future payment methods? 'wallet'? 'sms'? 'mpay'?
    );

    // Chosen payment method
    public $payment_method = null;

    public function getSource()
    {
        return 'orders';
    }

    // Override magic setter to prevent direct access to 'total' property
    public function __set($property, $value)
    {
        if ('total' === $property) {
            $this->setTotal($value);

            return;
        }

        parent::__set($property, $value);
    }

    // Override magic getter for 'total' property
    public function __get($property)
    {
        if ('total' === $property) {
            return $this->getTotal();
        }

        return parent::__get($property);
    }

    public function ident()
    {
        return sprintf('Order #%s (PBO: %s)', $this->readAttribute('id'), $this->getPbo());
    }

    /**
     * Model init. Called only once per request!
     */
    public function initialize()
    {
        parent::initialize();

        $this->belongsTo(
            'user_id', __NAMESPACE__ . '\Users', 'id',
            array(
                'alias'    => 'User',
                'reusable' => true,
            )
        );

        $this->hasMany(
            'id', __NAMESPACE__ . '\OrdersItems', 'order_id',
            array(
                'alias'    => 'Items',
                'reusable' => true
            )
        );
    }

    public function getTotal()
    {
        return Utils::lp2kn($this->total);
    }

    public function setTotal($total = '')
    {
        $this->total = Utils::kn2lp($total);
    }

    public function getTotalRaw()
    {
        return $this->total;
    }

    public function setTotalRaw($total)
    {
        $this->total = $total;
    }

    // Overriding toArray() in order to set 'total' correctly from minor currency units
    public function toArray($columns = null)
    {
        $data = parent::toArray($columns);

        if (isset($data['total'])) {
            $data['total'] = $this->getTotal();
        }

        return $data;
    }

    public function getStatusText()
    {
        $statuses = static::getAllStatuses();

        return $statuses[$this->status];
    }

    public function getLabeledStatusText()
    {
        $statuses = static::getAllStatuses();
        $data = array(
            Orders::STATUS_NEW       => array(
                'title'     => $statuses[Orders::STATUS_NEW],
                'classname' => 'info'
            ),
            Orders::STATUS_CANCELLED => array(
                'title'     => $statuses[Orders::STATUS_CANCELLED],
                'classname' => 'warning'
            ),
            Orders::STATUS_EXPIRED   => array(
                'title'     => $statuses[Orders::STATUS_EXPIRED],
                'classname' => 'danger'
            ),
            Orders::STATUS_COMPLETED => array(
                'title'     => $statuses[Orders::STATUS_COMPLETED],
                'classname' => 'success'
            )
        );

        $current_label = $data[$this->status];

        return '<span class="label label-' . $current_label['classname'] . '">' . $current_label['title'] . '</span>';
    }


    /**
     * @return array
     */
    public static function getAllStatuses()
    {
        return array(
            self::STATUS_NEW       => 'Created',
            self::STATUS_COMPLETED => 'Completed',
            self::STATUS_CANCELLED => 'Cancelled',
            self::STATUS_EXPIRED   => 'Expired'
        );
    }


    /**
     * @return array
     */
    public static function getAllPaymentMethods()
    {
        $methods = array();

        foreach (static::$payment_methods as $method) {
            $methods[$method] = ucfirst($method);
        }

        return $methods;
    }

    /**
     * Overridden findFirst in order to provide proper return type annotations
     * and to support finding by Pbo transparently
     *
     * @param array|int|string|null $parameters
     *
     * @return bool|Orders
     */
    public static function findFirst($parameters = null)
    {
        if (null !== $parameters) {
            // If we're given just a PBO, lets try acting like it's the same as if we were given an id
            if (is_string($parameters) || is_numeric($parameters)) {
                if (Utils::str_has_pbo_prefix($parameters)) {
                    $parameters = Utils::str_remove_pbo_prefix($parameters);
                }
            }
        }

        return parent::findFirst($parameters);
    }

    /**
     * <pbo> aka OFFERNUMBER?
     *
     * @return string
     */
    public function getPbo()
    {
        return Utils::build_pbo($this->id);
    }

    public function markExpired()
    {
        $sql = "UPDATE `" . $this->getSource() . "` SET `status` = :status_expired WHERE `status` = :status_new AND `valid_until` < NOW()";

        $conn = $this->getWriteConnection();
        $conn->execute($sql, array(':status_expired' => self::STATUS_EXPIRED, ':status_new' => self::STATUS_NEW));

        return $conn->affectedRows();
    }

    /**
     * Transforms the Resultset into a regular array containing a bunch of objects/entities and augments
     * each one with a "details" key which contains an array of one or more matching OrdersItems records
     *
     * @param Resultset &$orders
     *
     * @return array
     */
    public static function buildResultsArray(Resultset &$orders)
    {
        $results = array();

        // Bail if the Resultset ain't valid
        if (!$orders->valid()) {
            return $results;
        }

        $ids     = array();
        $results = $orders->toArray();

        $statuses = self::getAllStatuses();

        foreach ($results as $k => $result) {
            // Grab ids for later
            $ids[] = $result['id'];

            // Fix total to not be in minor currency units
            $result['total_lp'] = $result['total'];
            $result['total']    = Utils::lp2kn($result['total']);

            $result['pbo']         = Utils::build_pbo($result['id']);
            $result['status_text'] = $statuses[$result['status']];
            $results[$k]           = $result;
        }

        $condition = 'order_id IN (' . implode(',', $ids) . ')';
        $details   = OrdersItems::find(array(
            'conditions' => $condition,
            'hydration'  => Resultset::HYDRATE_OBJECTS
        ));

        foreach ($results as $k => $order) {
            $results[$k]['details'] = array();
            foreach ($details as $detail) {
                if ($detail->order_id == $order['id']) {
                    // Break potentially huge strings for display purposes
                    if (!empty($detail->product)) {
                        $detail->product = Utils::soft_break(Utils::esc_html($detail->product));
                    }
                    $results[$k]['details'][] = $detail;
                }
            }
            if (!empty($results[$k]['details'])) {
                $results[$k]['table_markup'] = static::buildOrderTableMarkup($order, $results[$k]['details']);
            }
        }

        return $results;
    }

    /**
     * Make sure certain data is set automatically when creating a new order
     */
    public function beforeValidationOnCreate()
    {
        $this->modified_at = date('Y-m-d H:i:s', time());

        $valid_until       = $this->createUntilDate();
        $this->valid_until = $valid_until->format('Y-m-d');

        // Set ip if not yet set (will be empty if the Order is created via CLI)
        if (!isset($this->ip) || empty($this->ip)) {
            $this->ip = $this->getDI()->getRequest()->getClientAddress(true);
        }
    }

    /**
     * @return \DateTime
     */
    protected function createUntilDate()
    {
        if (isset($this->created_at) && !empty($this->created_at)) {
            $start_from = strtotime($this->created_at);
        } else {
            $start_from = time();
        }

        $valid_until = new \DateTime();
        $valid_until->setTimestamp($start_from + (self::DAYS_VALID * 86400));

        return $valid_until;
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        $expired = ($this->status == self::STATUS_EXPIRED);

        // If it ain't marked as expired already (via cron calls to Orders::markExpired()),
        // check if valid_until date has passed
        if (!$expired) {
            $ts_today       = strtotime(date('Y-m-d'));
            $ts_valid_until = strtotime($this->valid_until);
            $expired        = ($ts_today > $ts_valid_until);
        }

        return $expired;
    }

    /**
     * @return bool
     */
    public function isStatusChangeable()
    {
        return ($this->status == self::STATUS_NEW);
    }

    public function getPaymentMethod()
    {
        return $this->payment_method;
    }

    public function isOfflinePayment()
    {
        return ('offline' === $this->payment_method);
    }

    public function isCcPayment()
    {
        return !$this->isOfflinePayment();
    }

    /**
     * Applies/executes business rules for purchased OrderItems if the order
     * is not yet expired or already fulfilled
     *
     * @param array $extra_data
     *
     * @return bool
     */
    public function finalizePurchase($extra_data = array())
    {
        if ($this->isExpired()) {
            return false;
        }

        if (!$this->isStatusChangeable()) {
            return false;
        }

        // Get all OrdersItems for this order and apply their purchase/activation rules
        $order_items = $this->getItems();
        if ($order_items->valid()) {
            /* @var $order_items OrdersItems[] */
            foreach ($order_items as $item) {
                $this->applyOrderedItemLogic($item);
            }
        } else {
            // TODO: what here? Can we have real orders without line items?
        }

        // TODO: send emails here/now? To whom? With what content exactly?

        // When done set status as completed (if not already set from outside) and return
        if (!isset($extra_data['status'])) {
            $extra_data['status'] = self::STATUS_COMPLETED;
        }

        return $this->save($extra_data);
    }

    /**
     * @param OrdersItems $item
     */
    protected function applyOrderedItemLogic(OrdersItems $item) {
        $product = null;
        if ($item->product) {
            $product = unserialize($item->product);
        }

        /** @var Ads $ad */
        $ad = $item->getAd();
        if ($ad && $product) {
            if ($product instanceof Pushup) {
                // Handle Pushups directly without modifying anything else on the ad
                $ad->pushUp(true); // true means it's a paid pushUp (meaning it came from an Order)
            } else {
                // Handling products that have an Ad to work with
                $ad->modifyWithProductUponPayment($product);
            }
        } else {
            // Handling stuff related to the User/Shop/Window/etc that made the order
            $duration_days = $product->getDuration();
            $now           = Utils::getRequestTime();
            $expires_at    = $now + (86400 * $duration_days); // now + x days worth of seconds
            switch (true) {
                case $product instanceof Izlog:
                    // Find the corresponding shopping window and extend its expiry date
                    $shopping_window_id = $product->getOrderData('shopping_window_id');
                    if ($shopping_window_id) {
                        $shopping_window = UsersShopsFeatured::findFirst($shopping_window_id);
                        if ($shopping_window) {
                            $shopping_window->save(array(
                                'expires_at' => $expires_at,
                                'published_at' => $now
                            ));
                        }
                    }
                    break;
                case $product instanceof Trgovina:
                    // Find the matching shop and extend its expiry date
                    $shop_id = $product->getOrderData('shop_id');
                    if ($shop_id) {
                        $shop = UsersShops::findFirst($shop_id);
                        if ($shop) {
                            $shop->save(array(
                                'expires_at' => $expires_at,
                                'published_at' => $now
                            ));
                        }
                    }
                    break;
            }
        }
    }

    /**
     * @return string
     */
    public function getTableMarkup()
    {
        $clone  = clone $this;
        $markup = static::buildOrderTableMarkup($clone, $clone->getItems());

        return $markup;
    }

    /**
     * Creates HTML table markup given an Order and the OrderDetails (line items) as
     * either an array or actual model instances.
     *
     * @param Orders|array $order
     * @param OrdersItems|Resultset|array $items Orders items (line items)
     *
     * @return string
     */
    public static function buildOrderTableMarkup($order, $items = array())
    {
        if (!is_array($order)) {
            $order = $order->toArray();
        }

        if (!is_array($items)) {
            $items = $items->toArray();
        }

        $order_total = Utils::format_money($order['total'], true);

        $html = '<table class="table table-condensed table-striped table-bordered order-details">';
        $html .= <<<TH
<thead>
    <tr>
        <th class="text-left">Ad</th>
        <th class="product-code">ID</th>
        <th class="title">Naziv</th>
        <th class="text-right price">Cijena</th>
        <th class="text-right qty">Količina</th>
        <th class="text-right value">Ukupno</th>
    </tr>
</thead>
TH;
        $html .= <<<TF
<tfoot>
    <tr class="summary total">
        <td colspan="4">&#160;</td>
        <td class="text-right"><strong>Ukupan iznos</strong></td>
        <td class="text-right value"><strong>{$order_total}</strong></td>
    </tr>
</tfoot>
TF;
        $html .= '<tbody>';
        foreach ($items as &$item) {
            $html .= '<tr>';
            $link = '';
            if ($item->ad_id) {
                $link = sprintf('<a href="/admin/ads/edit/%s">%s</a>', $item->ad_id, $item->ad_id);
            }
            $html .= '<td class="text-left">' . $link . '</td>';
            $html .= '<td class="product-code">' . $item->product_id . '</td>';
            $html .= '<td class="title">' . $item->title . '</td>';
            $html .= '<td class="text-right price">' . Utils::format_money(Utils::lp2kn($item->price), true) . '</td>';
            $html .= '<td class="text-right qty">' . $item->qty . '</td>';
            $html .= '<td class="text-right value">' . Utils::format_money(Utils::lp2kn($item->total), true) . '</td>';
            $html .= '</tr>';
        }
        $html .= '</tbody>';

        $html .= '</table>';

        return $html;
    }

    /**
     * Creates a new (or updates an existing, if specified) Order given a specific Ad.
     * If given a list of explicit ProductsInterface instances they're set as the OrdersItems,
     * otherwise they're retrieved from the specified $ad using `$ad->getProducts()`.
     * Supports passing along optional extra Order data to be set on the Orders model.
     *
     * @param Ads $ad
     * @param Orders|null $order Optional, updates an existing Order or creates a new one (default)
     * @param ProductsInterface[]|null $products Optional, Array of ProductsInterface instances or fetched
     *                                           via `$ad->getProducts()` (default)
     * @param array $extra_data Extra key/value pairs to be set on the resulting Orders model
     *
     * @return Orders|\Phalcon\Mvc\Model\MessageInterface[]|void Returns the new/updated Order if everything is ok
     */
    public static function saveForAd(Ads $ad, Orders $order = null, array $products = null, $extra_data = array())
    {

		
		//IN:
		if ( \Baseapp\Library\libSuva::getContext('n_source_path') != 'suva' ){
			return \Baseapp\Library\libSuva::Orders_saveForAd ($ad, $order, $products, $extra_data);
		}
		
		
		// Grab the Ad's products if they're not explicitly specified/provided
        if (null === $products) {
            $products = $ad->getProducts();
        }
		
		$requiredFreeOrder = false;
		$requiredPaidOrder = false;
		
        foreach ($products as $ordered_product) {
            $cost = $ordered_product->getCost();
			if ($cost > 0) $requiredPaidOrder = true;
			else $requiredFreeOrder = true;
		}

		
		//error_log("requiredFreeOrder  $requiredFreeOrder, requiredPaidOrder $requiredPaidOrder");
		
		if ($requiredFreeOrder) $freeOrder = \Baseapp\Suva\Library\libFrontend::getFreeAdsListForUser($ad->user_id);
		
		
		//error_log("freeOrder ID  $freeOrder->id");
		
		
		if ( null === $order && $requiredPaidOrder ) $paidOrder = $order = new Orders();
		elseif ( $order && $requiredPaidOrder ) $paidOrder = $order;
		
		if (null === $order && $requiredPaidOrder) {
            //$order = new Orders();

            // Set some defaults for new Orders created this way
            $default_data = array(
                'payment_method' => 'offline',
                'status'         => self::STATUS_NEW,
                'user_id'        => $ad->user_id
            );
        } else {
            $default_data = array();
        }

        // Merge any additional data with the defaults above (allowing override)
        $order_data = $default_data;
        if (!empty($extra_data)) {
            $order_data = array_merge($default_data, $extra_data);
        }


        if ($requiredPaidOrder){
			// Setting order data present in the array
			foreach ($order_data as $k => $v) {
				$paidOrder->$k = $v;
			}

			// Clear out any previously set related records
			$paidOrder->Items->delete();
		}

        $new_total_lp = 0;
        if (!isset($order_data['items']) || empty($order_data['items'])) {
            // Add all the given products as order line items
            if ($requiredFreeOrder) $freeItems = array();
			if ($requiredPaidOrder) $paidItems = array();
            foreach ($products as $ordered_product) {
                $cost = $ordered_product->getCost();
                // Skipping products/items with a cost of 0
                // TODO: ask Oglasnik which way should it be in the end...
                /*
                if ($cost <= 0) {
                    continue;
                }
                */

                $qty = 1;

                // TODO: do we separate product "extras" into separate order items or keep it together?
                $item             = new OrdersItems();
                $item->ad_id      = $ad->id;
                $item->product_id = $ordered_product->getId();
                $item->product    = serialize($ordered_product);
                $item->title      = $ordered_product->getBillingTitleWithEverything() . ' [Oglas: ' . $ad->id . ']';
                $item->qty        = $qty;
                $item->setPriceRaw($cost);
                $item->setTotalRaw($cost);

                $new_total_lp += $qty * $cost;

                // TODO: tax_rate? calculated tax_amount in a separate field?

                if ($cost > 0) $paidItems[] = $item;
				else $freeItems[] = $item;
            }
            
			if($requiredFreeOrder) $freeOrder->Items = $freeItems;
			if($requiredPaidOrder) $paidOrder->Items = $paidItems;
        } elseif($requiredPaidOrder) {
            // Add whatever was specified in parameters
            $paidOrder->Items  = $order_data['items'];
        }

		if ($requiredPaidOrder){
			// Update the new de-normalized total field if needed
			$current_total = $paidOrder->getTotal();
			if ($current_total != $new_total_lp) {
				$paidOrder->setTotalRaw($new_total_lp);
			}

			// Finally save the Order
			$paidOrderSaved = $paidOrder->save();

			if (! $paidOrderSaved) {
				return $paidOrder->getMessages();
			}
		}
		if ($requiredFreeOrder){
			// Finally save the Order
			$freeOrderSaved = $freeOrder->save();

			if (!$freeOrderSaved) {
				return $freeOrder->getMessages();
			}
		}
		
		if ($requiredPaidOrder){
			return $paidOrder;
		}
		if ($requiredFreeOrder){
			return $freeOrder;
		}
		
		
    }

    /**
     * Creates a new Order given a list of Pushup Products and $extra_data.
     *
     * @param Pushup[]|null $products Optional, Array of Pushup instances
     * @param array $extra_data Extra key/value pairs to be set on the resulting Orders model
     *
     * @return Orders|\Phalcon\Mvc\Model\MessageInterface[]|void Returns the new Order if everything is ok
     */
    public static function createWithPushups(array $products, $extra_data = array())
    {
        $order = new Orders();

        // Set some defaults for new Orders created this way
        $default_data = array(
            'payment_method' => 'offline',
            'status'         => self::STATUS_NEW,
        );

        // Merge any additional data with the defaults above (allowing override)
        $order_data = $default_data;
        if (!empty($extra_data)) {
            $order_data = array_merge($default_data, $extra_data);
        }

        // Setting order data present in the array
        foreach ($order_data as $k => $v) {
            $order->$k = $v;
        }

        $new_total_lp = 0;
        if (!isset($order_data['items']) || empty($order_data['items'])) {
            // Add all the given products as order line items
            $items = array();
            foreach ($products as $ordered_product) {
                $cost = $ordered_product->getCost();
                // Skipping products/items with a cost of 0
                // TODO: ask Oglasnik which way should it be in the end...
                /*
                if ($cost <= 0) {
                    continue;
                }
                */

                $ad = $ordered_product->getAd();

                $qty = 1;

                // TODO: do we separate product "extras" into separate order items or keep it together?
                $item             = new OrdersItems();
                $item->ad_id      = $ad->id;
                $item->product_id = $ordered_product->getId();
                $item->product    = serialize($ordered_product);
                $item->title      = $ordered_product->getBillingTitleWithEverything() . ' [Oglas: ' . $ad->id . ']';
                $item->qty        = $qty;
                $item->setPriceRaw($cost);
                $item->setTotalRaw($cost);

                $new_total_lp += $qty * $cost;

                // TODO: tax_rate? calculated tax_amount in a separate field?

                $items[] = $item;
            }
            $order->Items = $items;
        } else {
            // Add whatever was specified in parameters
            $order->Items  = $order_data['items'];
        }

        // Update the new de-normalized total field if needed
        $current_total = $order->getTotal();
        if ($current_total != $new_total_lp) {
            $order->setTotalRaw($new_total_lp);
        }

        // Finally create the Order
        $created = $order->create();

        if ($created) {
            return $order;
        } else {
            return $order->getMessages();
        }
    }

    public static function createShoppingWindowOrder(UsersShops $shop, UsersShopsFeatured $shopping_window, $extra_data = null)
    {
        $order = new Orders();

        // Set some defaults for new Orders created this way
        $default_data = array(
            'payment_method' => 'offline',
            'status'         => self::STATUS_NEW,
        );

        // Merge any additional data with the defaults above (allowing override)
        $order_data = $default_data;
        if (!empty($extra_data)) {
            $order_data = array_merge($default_data, $extra_data);
        }

        // Setting order data present in the array
        foreach ($order_data as $k => $v) {
            $order->$k = $v;
        }

        // Create and configure the product instance
        $product = new Izlog();
        $product_order_data = array(
            'shop_id' => $shop->id,
            'shopping_window_id' => $shopping_window->id
        );
        $product->setSelectedOption('30 dana');
        $product->setOrderData($product_order_data);
        /** @var Izlog[] $products */
        $products = array();
        $products[] = $product;

        // Add all the given products as order line items
        $new_total_lp = 0;
        $items = array();
        foreach ($products as $ordered_product) {
            $cost = $ordered_product->getCost();
            $qty = 1;

            $item             = new OrdersItems();
            $item->ad_id      = null;
            $item->product_id = $ordered_product->getId();
            $item->product    = serialize($ordered_product);
            $item->title      = $ordered_product->getBillingTitleWithEverything();
            $item->qty        = $qty;
            $item->setPriceRaw($cost);
            $item->setTotalRaw($cost);

            $new_total_lp += $qty * $cost;

            // TODO: tax_rate? calculated tax_amount in a separate field?

            $items[] = $item;
        }
        $order->Items = $items;

        // Update the new de-normalized total field if needed
        $current_total = $order->getTotal();
        if ($current_total != $new_total_lp) {
            $order->setTotalRaw($new_total_lp);
        }

        // Finally create the Order
        $created = $order->create();

        if ($created) {
            return $order;
        } else {
            return $order->getMessages();
        }
    }

    public static function isAllowedPaymentMethod($method)
    {
        return in_array($method, self::$payment_methods);
    }

    /**
     * @param Ads|int|null $ad
     *
     * @return Orders[]|array|false
     */
    public static function findUnpaidOrdersForAd($ad = null)
    {
        $ad_id = null;

        if (null !== $ad) {
            if ($ad instanceof Ads && !empty($ad->id)) {
                $ad_id = $ad->id;
            } else if (is_numeric($ad)) {
                $ad_id = (int) $ad;
                $ad    = Ads::findFirst($ad_id);
            }
        }

        // ¯\_(ツ)_/¯
        if (!$ad || !$ad_id) {
            return false;
        }

        // Find new/unpaid Orders (and their OrdersItems) for the specified ad
        $builder = new Builder();
        $builder->columns(array(
            'Baseapp\Models\Orders.*'
        ))->from('Baseapp\Models\Orders');
        $builder->innerJoin(
            'Baseapp\Models\OrdersItems',
            'Baseapp\Models\Orders.id = Baseapp\Models\OrdersItems.order_id AND Baseapp\Models\OrdersItems.ad_id = ' . $ad_id
        );
        $builder->andWhere('Baseapp\Models\Orders.status = :status:', array('status' => self::STATUS_NEW));
        $builder->groupBy('Baseapp\Models\Orders.id');

        $results = $builder->getQuery()->execute();

        if (!$results || 0 === count($results)) {
            $results = array();
        }

        return $results;
    }

    public static function findOrdersForAd($ad = null, $limit = null)
    {
        $ad_id = null;

        if (null !== $ad) {
            if ($ad instanceof Ads && !empty($ad->id)) {
                $ad_id = $ad->id;
            } else if (is_numeric($ad)) {
                $ad_id = (int) $ad;
                $ad    = Ads::findFirst($ad_id);
            }
        }

        // ¯\_(ツ)_/¯
        if (!$ad || !$ad_id) {
            return false;
        }

        // Find all Orders (and their OrdersItems) for the specified ad
        $builder = new Builder();
        $builder->columns(array(
            'Baseapp\Models\Orders.*'
        ))->from('Baseapp\Models\Orders');
        $builder->innerJoin(
            'Baseapp\Models\OrdersItems',
            'Baseapp\Models\Orders.id = Baseapp\Models\OrdersItems.order_id AND Baseapp\Models\OrdersItems.ad_id = ' . $ad_id
        );
        $builder->groupBy('Baseapp\Models\Orders.id');
        $builder->orderBy('Baseapp\Models\Orders.id DESC');

        if ($limit && intval($limit)) {
            $builder->limit(intval($limit));
        }

        $results = $builder->getQuery()->execute();

        if (!$results || 0 === count($results)) {
            $results = array();
        }

        return $results;
    }

    /**
     * @param Users|int $user
     *
     * @return array|mixed
     */
    public static function findUnpaidOrdersWithIzlogProductForUser($user)
    {
        if ($user instanceof Users) {
            $user_id = $user->id;
        } else {
            $user_id = (int) $user;
        }

        $builder = new Builder();
        $builder->columns(array(
            'o.*',
            'oi.*',
        ))->from(array('o' => 'Baseapp\Models\Orders'));
        $builder->innerJoin('Baseapp\Models\OrdersItems', 'o.id = oi.order_id', 'oi');
        $builder->where('o.user_id = ' . $user_id);
        $builder->andWhere('o.status = ' . self::STATUS_NEW);
        $builder->andWhere('oi.product_id = "izlog"');

        $results = $builder->getQuery()->execute();

        if (!$results || 0 === count($results)) {
            $results = array();
        }

        return $results;
    }

    /**
     * @param Users|int $user
     * @param array $ad_ids
     *
     * @return array|\Phalcon\Mvc\Model\Resultset\Complex|mixed
     */
    public static function findUnpaidPushUpOrders($user, array $ad_ids = array())
    {
        if ($user instanceof Users) {
            $user_id = $user->id;
        } else {
            $user_id = (int) $user;
        }

        $builder = new Builder();
        $builder->columns(array(
            'o.*',
            'oi.*',
        ))->from(array('o' => 'Baseapp\Models\Orders'));
        $builder->innerJoin('Baseapp\Models\OrdersItems', 'o.id = oi.order_id', 'oi');

        $builder->where('o.user_id = ' . $user_id);
        $builder->andWhere('o.status = ' . self::STATUS_NEW);
        $builder->andWhere('oi.product_id = "pushup"');

        // Search only specific ad ids if specified
        if (!empty($ad_ids)) {
            $builder->andWhere('oi.ad_id IN (' . implode(',', $ad_ids ) . ')');
        }

        $results = $builder->getQuery()->execute();

        if (!$results || 0 === count($results)) {
            $results = array();
        }

        return $results;
    }

    public function getPaymentRecapTableMarkup()
    {
        $clone = clone $this;

        return static::buildPaymentRecapTableMarkup($clone, $clone->getItems());
    }

    /**
     * Creates HTML table markup given an Order and the OrderDetails (line items) as
     * either an array or actual model instances.
     * Similar to `buildOrderTableMarkup()` but with less details and working with
     * OrdersItems transformed into arrays (instead of a list of objects as is the case
     * in `buildOrderTableMarkup()` since it's being called from Orders::buildResultsArray())
     *
     * @param Orders|array $order
     * @param OrdersItems|Resultset|array $items Orders items (line items)
     *
     * @return string
     */
    public static function buildPaymentRecapTableMarkup($order, $items = array())
    {
        if (!is_array($order)) {
            $order = $order->toArray();
        }

        if (!is_array($items)) {
            $items = $items->toArray();
        }

        $order_total = Utils::format_money($order['total'], true);

        $html = '<div class="overflow-hidden border-radius bg-white">';
        $html .= '<table class="table overview-table order-details hidden-xs">';
        $html .= <<<TH
<thead>
    <tr>
        <th class="title">Naziv</th>
        <th class="text-right price">Cijena</th>
        <th class="text-right qty">Količina</th>
        <th class="text-right value">Ukupno</th>
    </tr>
</thead>
TH;
        $html .= <<<TF
<tfoot>
    <tr class="summary total">
        <td colspan="3" class="text-right text-big blue text-nowrap"><strong>Ukupan iznos</strong></td>
        <td class="text-right text-big blue value text-nowrap"><strong>{$order_total} kn</strong></td>
    </tr>
</tfoot>
TF;
        $html .= '<tbody>';
        $xsScreenData = '<div class="payment-recap visible-xs">';
        foreach ($items as &$item) {
            $html .= '<tr>';
            $html .= '<td class="title">' . $item['title'] . '</td>';
            $html .= '<td class="text-right price text-nowrap">' . Utils::format_money(Utils::lp2kn($item['price']), true) . ' kn</td>';
            $html .= '<td class="text-right qty text-nowrap">' . $item['qty'] . '</td>';
            $html .= '<td class="text-right blue value text-nowrap">' . Utils::format_money(Utils::lp2kn($item['total']), true) . ' kn</td>';
            $html .= '</tr>';

            $xsScreenData .= '<dl>';
            $xsScreenData .= '<dt>Naziv:</dt><dd>' . $item['title'] . '</dd>';
            $xsScreenData .= '<dt>Cijena:</dt><dd>' . Utils::format_money(Utils::lp2kn($item['price']), true) . ' kn</dd>';
            $xsScreenData .= '<dt>Količina:</dt><dd>' . $item['qty'] . '</dd>';
            $xsScreenData .= '<dt>Ukupno:</dt><dd>' . Utils::format_money(Utils::lp2kn($item['total']), true) . ' kn</dd>';
            $xsScreenData .= '</dl>';
        }

        $xsScreenData .= '<span class="total text-bold">Ukupan iznos: <span class="text-primary">' . $order_total . ' kn</span></span>';

        $xsScreenData .= '</div>';
        $html .= '</tbody>';

        $html .= '</table>';
        $html .= $xsScreenData;
        $html .= '</div>';

        return $html;
    }

    /**
     * @param array $order_ids
     *
     * @return bool|int
     */
    public function markCancelled(array $order_ids)
    {
        $result = false;

        if (!empty($order_ids)) {
            $ids_sql = implode(', ', $order_ids);
            $sql = "UPDATE `" . $this->getSource() . "` SET `status` = " . self::STATUS_CANCELLED . " WHERE id IN (" . $ids_sql . ") AND `status` = " . self::STATUS_NEW;

            $conn = $this->getWriteConnection();
            $conn->execute($sql);

            $result = $conn->affectedRows();
        }

        return $result;
    }

    public static function cancelAllPendingOrdersForAd($ad)
    {
        $orders = self::findUnpaidOrdersForAd($ad);
        $result = null;

        if ($orders && count($orders)) {
            $order_ids = [];
            foreach ($orders as $order) {
                $order_ids[] = $order->id;
            }

            $result = (new Orders())->markCancelled($order_ids);
        }

        return $result;
    }

    public function handleCancellationOfOrderItems()
    {
        if ($items = $this->getItems()) {
            foreach ($items as $item) {
                if ($ad = $item->Ad) {
                    $ad->latest_payment_state = Ads::PAYMENT_STATE_CANCELED_ORDER;
                    $ad->update();
                }
            }
        }
    }

    /**
     * @return string
     */
    public function get2DBarcodeInlineImage()
    {
        $barcode_image = '';

        $data = $this->buildHub3BarcodeData($this);

        $options = [
            'format'  => 'png',
            'quality' => 90,
            'scale'   => 2,
            'ratio'   => 3,
            'padding' => 0,
            'bgColor' => '#f9f9f9'
        ];

        $img_renderer = new ImagickImageRenderer($options);
        $img = $img_renderer->render($data);
        if ($img) {
            $barcode_image = 'data:image/' . $options['format'] . ';base64,' . base64_encode($img);
        }

        return $barcode_image;
    }

    /**
     * @param bool $base64_encoded Defaults to true
     *
     * @return string
     */
    public function get2DBarcodeInlineSvg($base64_encoded = true)
    {
        $data = $this->buildHub3BarcodeData($this);

        $renderer = new SvgRenderer(array(
            'pretty' => false,
            'scale' => 2,
            'ratio' => 3,
            'color' => 'black',
        ));
        $svg = $renderer->render($data);

        if ($base64_encoded) {
            $svg = 'data:image/svg+xml;base64,' . base64_encode($svg);
        }

        return $svg;
    }

    /**
     * Build a bunch of common vars + order data (formatted total, pbo, barcode image, etc.) used in views and
     * return it all as an array.
     *
     * @param Orders $order
     * @param null|string $barcode_src Optional, returned as the value of 'barcode_src' key in resulting array (if truthy)
     *
     * @return array
     */
    public static function buildCommonDisplayData(Orders $order, $barcode_src = null)
    {
        $data = $order->toArray();

        $data['total_formatted'] = Utils::format_money($order->getTotal(), true);
        $data['pbo'] = $order->getPbo();

        if ($barcode_src) {
            $data['barcode_src'] = $barcode_src;
            $data['barcode_markup'] = '<img class="barcode" src="' . $barcode_src . '">';
        }

        return $data;
    }

    public static function buildPaymentDataTableMarkup(Orders $order, $barcode_src = null)
    {
        // If not already created earlier, create the barcode now
        if (!$barcode_src) {
            $barcode_src = $order->get2DBarcodeInlineSvg();
        }

        $order_data = Orders::buildCommonDisplayData($order, $barcode_src);

        $markup = <<<HTML
<h2>Podaci za uplatu:</h2>
<div class="overflow-hidden border-radius bg-white">
<table id="offline-payment-details-data-{$order->id}" cellpadding="0" cellspacing="0" class="table overview-table order-details order-details-hub-3a">
    <tbody>
    <tr>
        <td class="u-text-left hidden-xs">Primatelj</th>
        <td class="text-right" data-target=".primatelj">
            <div class="text-small text-left visible-xs">
                <div class="text-bold">Primatelj:</div>
                <div class="text-primary">Oglasnik d.o.o., Savska cesta 41, 10000 Zagreb</div>
            </div>
            <div class="hidden-xs">
                Oglasnik d.o.o., Savska cesta 41, 10000 Zagreb
            </div>
        </td>
    </tr>
    <tr>
        <td class="u-text-left hidden-xs">Iznos</th>
        <td class="text-right" data-monospace data-target=".iznos">
            <div class="text-small text-left visible-xs">
                <div class="text-bold">Iznos:</div>
                <div class="text-primary">={$order_data['total_formatted']} kn</div>
            </div>
            <div class="hidden-xs">
                <span class="order-total-formatted">={$order_data['total_formatted']}</span> <abbr title="kune">kn</abbr>
            </div>
        </td>
    </tr>
    <tr>
        <td class="u-text-left hidden-xs">Model</th>
        <td class="text-right" data-monospace data-target=".model">
            <div class="text-small text-left visible-xs">
                <div class="text-bold">Model:</div>
                <div class="text-primary">HR00</div>
            </div>
            <div class="hidden-xs">
                HR00
            </div>
        </td>
    </tr>
    <tr>
        <td class="u-text-left hidden-xs">Poziv na broj primatelja</th>
        <td class="text-right" data-monospace data-target=".poziv-na-broj">
            <div class="text-small text-left visible-xs">
                <div class="text-bold">Poziv na broj primatelja:</div>
                <div class="text-primary">{$order_data['pbo']}</div>
            </div>
            <div class="hidden-xs">
                {$order_data['pbo']}
            </div>
        </td>
    </tr>
    <tr>
        <td class="u-text-left hidden-xs">IBAN ili broj računa primatelja</th>
        <td class="text-right" data-monospace data-target=".broj-racuna">
            <div class="text-small text-left visible-xs">
                <div class="text-bold">IBAN ili broj računa primatelja:</div>
                <div class="text-primary">HR1624840081100153885</div>
            </div>
            <div class="hidden-xs">
                HR1624840081100153885
            </div>
        </td>
    </tr>
    <tr>
        <td class="u-text-left hidden-xs">Opis plaćanja</th>
        <td class="text-right" data-target=".opis-placanja">
            <div class="text-small text-left visible-xs">
                <div class="text-bold">Opis plaćanja:</div>
                <div class="text-primary">Plaćanje po narudžbi {$order_data['pbo']}</div>
            </div>
            <div class="hidden-xs">
                Plaćanje po narudžbi {$order_data['pbo']}
            </div>
        </td>
    </tr>
HTML;
        // Add barcode img
        if (isset($order_data['barcode_markup'])) {
            $markup .= <<<HTML
    <tr>
        <td data-target=".barcode" colspan="2">{$order_data['barcode_markup']}</td>
    </tr>
HTML;
        }

        $href = '/moj-kutak/uplatnica/' . $order_data['id'];
        $markup .= <<<HTML
    <tr>
        <td colspan="2">
            <a href="{$href}" target="_blank"><span class="fa fa-fw fa-print"></span>Ispiši uplatnicu</a>
            <!--a href="{$href}?pdf" target="_blank"><span class="fa fa-fw fa-file-pdf-o"></span>Preuzmi uplatnicu</a-->
        </td>
    </tr>
HTML;

        // Close up tags
        $markup .= <<<HTML
    </tbody>
</table>
</div>
HTML;

        return $markup;
    }

    public static function buildBankSlipMarkup(Orders $order, $barcode_src = null)
    {
        $order_data = Orders::buildCommonDisplayData($order, $barcode_src);
        $receiver = TransactionReceiverData::createOglasnikReceiverObject($order_data['pbo']);

        $monospace_fmt = '<b class="character">%s</b>';

        $model_text = 'HR' . $receiver->model;
        $model_markup = Orders::sprintfLetters($model_text, $monospace_fmt);

        $receiver_address = $receiver->name . ', ' . $receiver->street . ', ' . $receiver->place;

        $monospace_data = array(
            'total' => Orders::sprintfLetters('=' . $order_data['total_formatted'], $monospace_fmt),
            'iban' => Orders::sprintfLetters($receiver->iban, $monospace_fmt),
            'pbo' => Orders::sprintfLetters($order_data['pbo'], $monospace_fmt)
        );

        $markup = <<<HTML
<div class="hub-3a-slip">
    <img id="uplatnica-{$order->id}" class="uplatnica" src="/assets/img/uplatnica-hub-3a.jpg" alt="HUB-3A uplatnica">
    <span class="valuta faux-monospace"><b class="character">H</b><b class="character">R</b><b class="character">K</b></span>
    <span class="iznos faux-monospace">{$monospace_data['total']}</span>
    <span class="model faux-monospace">{$model_markup}</span>
    <span class="platitelj"></span>
    <span class="primatelj">{$receiver_address}</span>
    <span class="broj-racuna faux-monospace">{$monospace_data['iban']}</span>
    <span class="poziv-na-broj faux-monospace">{$monospace_data['pbo']}</span>
    <span class="opis-placanja">Plaćanje po narudžbi {$order_data['pbo']}</span>
HTML;
        if (isset($order_data['barcode_markup'])) {
            $markup .= <<<HTML
    <span class="barcode">{$order_data['barcode_markup']}</span>
HTML;
        }
        $markup .= <<<HTML
    <div class="talon">
        <span class="talon-row valuta-i-iznos">HRK={$order_data['total_formatted']}</span>
        <span class="talon-row iban-primatelja">HR1624840081100153885</span>
        <span class="talon-row model-i-pbo-primatelja">{$model_text} {$order_data['pbo']}</span>
        <span class="talon-row opis-placanja">Plaćanje po narudžbi {$order_data['pbo']}</span>
    </div>
</div>
HTML;

        return $markup;
    }

    public static function sprintfLetters($str, $format = '<span>%s</span>')
    {
        $chars = preg_split('//', $str, -1, PREG_SPLIT_NO_EMPTY);

        $result = '';
        foreach ($chars as $char) {
            // Skip over '.' and ',' chars completely
            if ('.' !== $char && ',' !== $char) {
                $result .= sprintf($format, $char);
            }/* else {
                $result .= $char;
            }*/
        }

        return $result;
    }

    public static function buildOfflineConfirmButtonMarkup($which, $button_text = 'Predaj oglas')
    {
        $markup = '<form id="frm_payment_confirm" method="post">';
        $markup .= '<button id="' . $which . '-submit" class="btn btn-primary btn-lg" type="submit" name="next" value="finish">';
        $markup .= $button_text . ' <span class="fa fa-arrow-circle-right fa-fw"></span></button>';
        $markup .= '</form>';

        return $markup;
    }
}
