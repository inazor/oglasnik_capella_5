<?php

namespace Baseapp\Models;

use \Baseapp\Library\Behavior\Blamable;

/**
 * Implements a Base Model which has Blamable Behavior attached, and any model which
 * wishes to use auditing capabilities can inherit from it.
 *
 * When a model inherits from BaseModelBlamable it must call parent::initialize() in cases
 * when it defines it's own initialize() method!
 *
 * A way around that could be to use Traits, but we'll see about that
 */
abstract class BaseModelBlamable extends BaseModel
{
    protected $auditing_enabled = true;

    public function initialize()
    {
        $this->keepSnapshots(true);
        $this->addBehavior(new Blamable());
        parent::initialize();
    }

    public function disable_auditing()
    {
        $this->auditing_enabled = false;
    }

    public function enable_auditing()
    {
        $this->auditing_enabled = true;
    }

    public function set_auditing_enabled($enabled)
    {
        $this->auditing_enabled = $enabled;
    }

    public function is_auditing_enabled()
    {
        return $this->auditing_enabled;
    }

    public function is_auditing_disabled()
    {
        return !$this->auditing_enabled;
    }
}
