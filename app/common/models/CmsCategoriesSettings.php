<?php

namespace Baseapp\Models;

/**
 * Cms categories settings Model
 */
class CmsCategoriesSettings extends BaseModelBlamable
{
    /**
     * CategoriesSettings initialize
     */
    public function initialize()
    {
        parent::initialize();

        // Every Cms categories settings belongs to a Cms category
        $this->belongsTo(
            'category_id', __NAMESPACE__ . '\CmsCategories', 'id', array(
                'alias' => 'Category'
            )
        );
    }

}
