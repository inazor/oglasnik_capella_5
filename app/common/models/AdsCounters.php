<?php

namespace Baseapp\Models;

/**
 * AdsCounter Model
 */
class AdsCounters extends BaseModelBlamable
{
    /**
     * AdsCounters initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->belongsTo(
            'ad_id', __NAMESPACE__ . '\Ads', 'id',
            array(
                'alias'      => 'Ad',
                'reusable'   => true
            )
        );
    }
}
