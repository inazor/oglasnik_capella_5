<?php

namespace Baseapp\Models;

class TransactionSenderData
{
    public $name;
    public $street;
    public $place;

    public static function fromObject($object)
    {
        $instance         = new self();

        if (isset($object->name)) {
            $instance->name = $object->name;
        }

        if (isset($object->street)) {
            $instance->street = $object->street;
        }

        if (isset($object->place)) {
            $instance->place = $object->place;
        }

        return $instance;
    }
}
