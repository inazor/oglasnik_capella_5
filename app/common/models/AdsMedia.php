<?php

namespace Baseapp\Models;

// class AdsMedia extends BaseModel
class AdsMedia extends BaseModelBlamable
{
    public $id;
    public $ad_id;
    public $media_id;
    public $sort_idx;

    public function initialize()
    {
        $this->belongsTo(
            'media_id', __NAMESPACE__ . '\Media', 'id',
            array(
                'alias'      => 'Media',
                'reusable'   => true
            )
        );

        $this->belongsTo(
            'ad_id', __NAMESPACE__ . '\Ads', 'id',
            array(
                'alias'      => 'Ad',
                'reusable'   => true
            )
        );

        parent::initialize();
    }

    public function getSource()
    {
        return 'ads_media';
    }

    public static function getByAdMediaIdOrCreateNew($ad_id, $media_id)
    {
        $ad_media = null;

        if ($ad_id) {
            $ad_media = self::findFirst(array(
                'conditions' => 'ad_id = :ad_id: AND media_id = :media_id:',
                'bind'       => array(
                    'ad_id'    => $ad_id,
                    'media_id' => $media_id
                )
            ));
        }

        if (!$ad_media) {
            $ad_media = new self();
            $ad_media->ad_id = $ad_id;
            $ad_media->media_id = $media_id;
        }

        return $ad_media;
    }

}
