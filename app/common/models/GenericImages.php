<?php

namespace Baseapp\Models;

use Baseapp\Extension\Image;
use Baseapp\Library\Images\Thumb;
use Baseapp\Library\Utils;
use Phalcon\Di;

class GenericImages extends BaseModelBlamable
{
    protected $id;

    public $model_name;
    public $model_pk_val;
    public $model_field;

    public $image;
    public $image_details;

    static $base_dir;
    static $base_uri;

    public function initialize()
    {
        parent::initialize();

        $this->setSource('generic_images');
    }

    public function onConstruct()
    {
        $config = $this->getDI()->get('config');

        self::$base_dir = $config->app->uploads_dir;
        self::$base_uri = $config->app->uploads_uri;
    }

    public function beforeDelete()
    {
        if (!empty($this->image)) {
            $details = json_decode($this->image_details, true);
            if (!empty($details['path'])) {
                $deleted = unlink($details['path']);
                if (!$deleted) {
                    if (isset($details['path_root'])) {
                        $deleted = unlink(ROOT_PATH . $details['path_root']);
                    }
                }
            }
        }

        return true;
    }

    public static function getUploadsUri()
    {
        self::checkConfig();

        return self::$base_uri . '/generic';
    }

    public static function getUploadsDir()
    {
        self::checkConfig();

        return self::$base_dir . '/generic';
    }

    public static function checkConfig()
    {
        static $config = null;

        if (null === $config) {
            $config = Di::getDefault()->getShared('config');

            if (empty(self::$base_dir)) {
                self::$base_dir = $config->app->uploads_dir;
            }
            if (empty(self::$base_uri)) {
                self::$base_uri = $config->app->uploads_uri;
            }
        }
    }

    public function assignNew($filename, array $details = array())
    {
        if (!isset($details['path'])) {
            throw new \Exception('Missing image path details');
        }

        $created = $this->resizeDefault(ROOT_PATH . $details['path_root']);
        if ($created) {
            $this->image = $filename;
        }

        if (!empty($details)) {
            $this->image_details = json_encode($details);
        }

        return $this->save();
    }

    public function resizeDefault($path)
    {
        $style = $this->getDefaultStyle();
        $image = new Image($path, $style->width, $style->height);

        // Auto rotate as soon as we open it, so anything done further should be fine
        $im     = $image->getInternalImInstance();
        $format = strtolower($im->getImageFormat());
        if ('jpeg' === $format) {
            $image->auto_rotate();
        }
        unset($im, $format);

        // Store original dimensions
        $orig_w = $image->getWidth();
        $orig_h = $image->getHeight();

        // TODO: can handle CMYK crap if it becomes an issue in the future...
        // $image->handle_cmyk();

        // If there's a stored crop definition (or the style specifies a crop)
        $do_crop       = false;
        if ($style->crop) {
            $do_crop                 = true;
            $existing_crop           = array();
            $existing_crop['width']  = $style->width;
            $existing_crop['height'] = $style->height;
        }

        // Do the cropping if needed
        if ($do_crop) {
            if (isset($existing_crop['x']) && isset($existing_crop['y'])) {
                // Exact coordinates exist
                $image->crop($existing_crop['width'], $existing_crop['height'], $existing_crop['x'], $existing_crop['y']);
            } else {
                // No previous specific crop record, try smart fitting without any upsizing for now
                $image->fit($existing_crop['width'], $existing_crop['height'], function($constraint) {
                    $constraint->upsize();
                });
            }

            // Enforce cropped dimensions in case the result is smaller than what the style dictates
            // NB: Only 'hard-crop' styles enforce this (for now at least), the other possible use-case
            // might come from a stored precise crop definition without the style itself having the
            // crop flag - that's why this if is here...
            if ($style->crop) {
                $actual_w      = $image->getWidth();
                $actual_h      = $image->getHeight();
                $target_width  = ($actual_w < $style->width) ? $style->width : null;
                $target_height = ($actual_h < $style->height) ? $style->height : null;
                if ($target_width || $target_height) {
                    $image->resize_canvas($target_width, $target_height);
                }
            }
        }

        // Now resize the (even potentially cropped) image if needed, hopefully avoiding upscaling in all cases
        $image->resize_down($style->width, $style->height);

        return $image->save($path);
    }

    /**
     * @return Thumb
     */
    public function getThumb()
    {
        $thumb = null;

        if (!empty($this->image)) {
            $details = json_decode($this->image_details, true);
            $image_path = $details['path'];
            if (!file_exists($image_path)) {
                $image_path = isset($details['path_root']) ? ROOT_PATH . $details['path_root'] : null;
            }
            if ($image_path && file_exists($image_path)) {
                $info = getimagesize($image_path);
                $src = $this->buildUrl($this->image);
                $thumb = new Thumb();
                $thumb->setSrc($src);
                $thumb->setWidth($info[0]);
                $thumb->setHeight($info[1]);
            }
        }

        if (!$thumb) {
            $thumb = Media::getNoImageThumb($this->getDefaultStyle());
        }

        return $thumb;
    }

    public function buildUrl($filename)
    {
        $basename = basename($filename);

        $partitioned_filepath = Utils::get_fqpn(
            $basename,
            Utils::fqpn_options(array(
                'basedir' => $this->getUploadsDir(),
            ))
        );

        $url_part = str_replace($this->getUploadsDir(), '', $partitioned_filepath);
        // $url_parts starts with a slash due to Utils::fqpn_options()
        $url = $this->getUploadsUri() . $url_part;

        return $url;
    }

    public function getDefaultStyle()
    {
        static $style = null;

        if (null === $style) {
            $style = ImageStyles::fromArray(array(
                'slug'      => 'generic',
                'width'     => 600,
                'height'    => 600,
                'crop'      => false,
                'watermark' => false
            ));
        }

        return $style;
    }

    public static function findFirstRelated($model_name, $model_pk_val, $model_field = null)
    {
        $sql = array(
            'model_name = :model_name:',
            'model_pk_val = :model_pk_val:'
        );
        $bindings = array(
            'model_name' => $model_name,
            'model_pk_val' => $model_pk_val
        );
        if ($model_field) {
            $sql[] = 'model_field = :model_field:';
            $bindings['model_field'] = $model_field;
        }

        return self::findFirst(array(
            implode(' AND ', $sql),
            'bind' => $bindings
        ));
    }

    public static function findRelated($model_name, $model_pk_vals, $model_field = null)
    {
        $model_ids = array();

        if (is_array($model_pk_vals)) {
            $model_ids = $model_pk_vals;
        } elseif (false !== strpbrk($model_pk_vals, ',')) {
            $model_ids = explode(',', $model_pk_vals);
        } elseif ((int) $model_pk_vals == trim($model_pk_vals)) {
            $model_ids[] = (int) $model_pk_vals;
        }

        if (empty($model_ids)) {
            return null;
        }

        $sql = array(
            'model_name = :model_name:',
            'model_pk_val IN ('.implode(',', $model_ids).')'
        );
        $bindings = array(
            'model_name' => $model_name
        );
        if ($model_field) {
            $sql[] = 'model_field = :model_field:';
            $bindings['model_field'] = $model_field;
        }

        return self::find(array(
            implode(' AND ', $sql),
            'bind' => $bindings
        ));
    }

}
