<?php

namespace Baseapp\Extension;

/**
 * Extends default \Phalcon\Tag to provide our customizations.
 */

class Tag extends \Phalcon\Tag
{
    public static function form($parameters = array())
    {
        if (is_array($parameters)) {
            /*if (isset($parameters['parameters'])) {
                if (!isset($parameters['parameters']['autocomplete'])) {
                    $parameters['parameters']['autocomplete'] = 'off';
                }
            }*/
            if (!isset($parameters['autocomplete'])) {
                $parameters['autocomplete'] = 'off';
            }
        }

        return parent::form($parameters);
    }

    /**
     * Overriding form closing to insert stuff we'd like to have inserted
     * automatically on every form. This is a really dirty approach.
     *
     * @return string
     */
/*
    public static function endForm()
    {
        $markup = '</form>';
        return $markup;
    }
*/

    /**
     * @param array $parameters
     * @param null $data
     *
     * @return string
     * @throws \Phalcon\Tag\Exception
     */
    public static function selectStatic($parameters, $data = null)
    {
        return \Baseapp\Extension\Tag\Select::selectField($parameters, $data);
    }

    /**
     * @param array $parameters
     * @param null $data
     *
     * @return string
     * @throws \Phalcon\Tag\Exception
     */
    public static function select($parameters, $data = null)
    {
        return \Baseapp\Extension\Tag\Select::selectField($parameters, $data);
    }

} 
