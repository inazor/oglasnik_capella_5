<?php

namespace Baseapp\Extension\Logger\Adapter\File;

/**
 * Extends the default \Phalcon\Logger\Adapter\File implementation to
 * automatically use logger transactions (which has the effect of messages being
 * queued internally and only being written to file when commit() is called).
 *
 * So we call commit() in the destructor.
 *
 * This speeds up logging measurably when you log() a lot, but it can be
 * deadly in the sense that there's no guarantees that the logs will actually be written.
 * Hence the name :)
 *
 * Use it by specifying the full classname in config.ini:
 * [logger]
 * adapter = "\Baseapp\Extension\Logger\Adapter\File\Volatile"
 */
class Volatile extends \Phalcon\Logger\Adapter\File
{

    public function __construct($name, $options = null)
    {
        parent::__construct($name, $options);
        $this->begin();
    }

    public function __destruct()
    {
        $this->commit();
    }

} 
