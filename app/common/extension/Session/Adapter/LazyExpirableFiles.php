<?php
/**
 * Lazy session adapter for Phalcon, which can start the session only when needed.
 */

namespace Baseapp\Extension\Session\Adapter;

use Baseapp\Bootstrap;
use Baseapp\Library\Debug;

class LazyExpirableFiles extends \Phalcon\Session\Adapter\Files
{

    const DEFAULT_OPTION_LIFETIME = 900;
    const METADATA_KEY = '__meta';
    const DEBUG = false;

    /**
     * Whether or not the session id has been regenerated this request.
     *
     * Id regeneration state
     * <0 - regenerate requested when session is started
     * 0  - do nothing
     * >0 - already called session_regenerate_id()
     *
     * @var int
     */
    private static $_regenerate_id_state = 0;

    /**
     * Whether or not write close has been performed.
     *
     * @var bool
     */
    private static $_write_closed = false;

    public function __construct($options = null)
    {
        if (!isset($options['lifetime'])) {
            $options['lifetime'] = self::DEFAULT_OPTION_LIFETIME;
        }

        // TODO: these should ALL be set via php.ini really...
        $session_options = array(
            'use_only_cookies',
            'cache_limiter',
            'cookies_httponly',
            'name'
        );
        foreach ($session_options as $option_name) {
            if (isset($options[$option_name])) {
                ini_set('session.' . $option_name, $options[$option_name]);
            }
        }

        parent::__construct($options);

        if (ini_get('session.auto_start')) {
            $this->start();
        }
    }

    public function start()
    {
        // Check to see if we've been passed an invalid session ID via set_id()
        $id = $this->get_id();
        if ($id && !$this->check_id($id)) {
            // Generate a valid, temporary replacement
            self::set_id(md5($id));
            // Force a regenerate after session is started
            self::$_regenerate_id_state = -1;
        }

        // FIXME: spotted an occasional notice in the logs about the session already being started:
        // [Sun, 28 Jun 15 20:19:20 +0200][ERROR] [109.60.100.240 /user/signout] Notice: A session had already been
        // started - ignoring session_start() in C:\Users\zytzagoo\Dropbox\webs\oglasnik.hr\phalcon-base\app\common\extension\Session\Adapter\LazyExpirableFiles.php on line 74
        // Investigate how/why exactly does this happen.
        // I'm suspecting an expired session tries to auto-login or something (due to logged_in() check in /user/signout, but haven't checked)

        // Let the parent actually start our session
        if (self::DEBUG) {
            Bootstrap::log(__METHOD__ . '()');
            // Bootstrap::log(Debug::trace());
            // Bootstrap::log("Trace:\n" . Debug::backtrace());
        }
        $started = parent::start();

        // Update session activity and check if it expired
        $metadata = $this->get(self::METADATA_KEY, false);
        if (!$metadata) {
            $this->init();
        } else {
            // Check if our session expired and destroy it if it did
            if ($metadata['activity']) {
                $seconds_ago = (time() - $metadata['activity']);
                if ($seconds_ago > $this->get_option('lifetime')) {
                    if (self::DEBUG) {
                        Bootstrap::log('Session lifetime reached, calling session destroy() from start()');
                    }
                    $this->destroy(true);
                }
            }

            // Update activity for next time...
            $metadata['activity'] = time();
            $this->set(self::METADATA_KEY, $metadata);
        }

        // Check if we need to regenerate
        if (-1 === self::$_regenerate_id_state) {
            $this->regenerate_id();
        }
    }

    private function init()
    {
        $now = time();
        $metadata = array(
            // TODO: make sure IP is fetched properly in all cases
            'ip'       => $_SERVER['REMOTE_ADDR'],
            'name'     => $this->get_option('name'),
            'created'  => $now,
            'activity' => $now,
        );
        $this->set(self::METADATA_KEY, $metadata);
        return true;
    }

    /**
     * Regenerate the session id. Best practice is to call this after
     * session is started. If called prior to session starting, session id will be regenerated
     * at start time.
     *
     * @throws \Phalcon\Session\Exception
     * @return void
     */
    public function regenerate_id()
    {
        if (headers_sent($filename, $linenum)) {
            throw new \Phalcon\Session\Exception('You must call ' . __CLASS__ . '::' . __FUNCTION__ .
                "() before any output has been sent to the browser; output started in {$filename}/{$linenum}");
        }
        if (!$this->isStarted()) {
            self::$_regenerate_id_state = -1;
        } else {
            session_regenerate_id(true);
            self::$_regenerate_id_state = 1;
        }
    }

    public function regenerateId($deleteOldSession = null)
    {
        $this->regenerate_id();
    }

    /**
     * Convenience method to determine if session_regenerate_id()
     * has been called during this request by us.
     *
     * @return bool
     */
    public function is_regenerated()
    {
        return ((self::$_regenerate_id_state > 0) ? true : false);
    }

    /**
     * Perform a hash-bits check on the session ID
     *
     * @param string $id Session ID
     * @return bool
     */
    protected function check_id($id)
    {
        $hash_bits_per_char = ini_get('session.hash_bits_per_character');
        if (!$hash_bits_per_char) {
            $hash_bits_per_char = 5; // the default value
        }

        switch ($hash_bits_per_char) {
            case 4: $pattern = '^[0-9a-f]*$'; break;
            case 5: $pattern = '^[0-9a-v]*$'; break;
            case 6: $pattern = '^[0-9a-zA-Z-,]*$'; break;
        }

        return preg_match('#' . $pattern . '#', $id);
    }

    /**
     * Set the session identifier to a user specified id
     *
     * @param $id
     *
     * @throws \Phalcon\Session\Exception
     */
    public function set_id($id)
    {
        if (defined('SID')) {
            throw new \Phalcon\Session\Exception('The session has already been started. The session id must be set before that.');
        }

        if (headers_sent($filename, $linenum)) {
            throw new \Phalcon\Session\Exception('You must call ' . __CLASS__ . '::' . __FUNCTION__ .
                "() before any output has been sent to the browser; output started in {$filename}/{$linenum}");
        }

        if (!is_string($id) || $id === '') {
            throw new \Phalcon\Session\Exception('You must provide a non-empty string as a session identifier.');
        }

        session_id($id);
    }

    public function write_close()
    {
        if (self::$_write_closed) {
            return;
        }
        session_write_close();
        self::$_write_closed = true;
    }

    /**
     * Returns true if a session is active already. Very hacky!
     *
     * @link http://stackoverflow.com/questions/3788369/how-to-tell-if-a-session-is-active/7656468#7656468
     *
     * @return bool
     * @throws \UnexpectedValueException
     */
    public static function is_active()
    {
        $setting = 'session.use_trans_sid';
        $current = ini_get($setting);
        if (false === $current) {
            throw new \UnexpectedValueException(sprintf('Setting %s does not exists.', $setting));
        }

        $testate = "mix$current$current";
        // Even though $old seems unused, it's actually used and mustn't be removed (due to the way ini_set() behaves)
        $old    = @ini_set($setting, $testate);
        $peek   = @ini_set($setting, $current);
        $result = ($peek === $current || false === $peek);

        return $result;
    }

    /**
     * Write session data to store and close the session.
     */
    public function commit()
    {
        session_commit();
    }

    public function start_on_demand()
    {
        if (self::DEBUG) {
            Bootstrap::log(__METHOD__ . '(): exists=' . var_export($this->exists_in_current_request(), true));
        }

        if ($this->exists_in_current_request()) {
            $this->start();
        }
    }

    /**
     * Whether or not a session exists for/in the current request
     *
     * @return bool
     */
    public function exists_in_current_request()
    {
        $exists = false;

        $session_use_cookies = (bool) ini_get('session.use_cookies');

        if (true === $session_use_cookies && isset($_COOKIE[$this->get_option('name')])) {
            $exists = true;
        } elseif (false === (bool) ini_get('session.use_only_cookies') && isset($_REQUEST[$this->get_option('name')])) {
            $exists = true;
        }

        return $exists;
    }

    public function get_option($key)
    {
        if (isset($this->_options[$key])) {
            return $this->_options[$key];
        }

        if ('name' === $key) {
            return $this->get_name();
        }

        return null;
    }

    public function get_name()
    {
        return session_name();
    }

    public function get_id()
    {
        return session_id();
    }

    /**
     * Overriding Phalcon's set() to start the session if it hasn't been started yet
     * which allows lazy set() behaviour
     *
     * @param string $index
     * @param string $value
     */
    public function set($index, $value)
    {
        if (!$this->isStarted()) {
            $this->start();
        }

        if (self::DEBUG) {
            Bootstrap::log(__METHOD__ . '(), session started=' . var_export($this->isStarted(), true) . ', $index=' . var_export($index, true));
        }

        return parent::set($index, $value);
    }

    /**
     * Overriding destroy to allow removing the session cookie as well if desired
     *
     * @param bool $remove_cookie
     *
     * @return bool|void
     */
    public function destroy($remove_cookie = true)
    {
        parent::destroy();

        // TODO: could this throw notices/warnings if headers have been sent already?
        if ($remove_cookie) {
            $params = session_get_cookie_params();
            setcookie(self::get_name(), '', time() - 42000,
                $params['path'], $params['domain'],
                $params['secure'], $params['httponly']
            );
        }
    }
}
