<?php

namespace Baseapp\Extension\Cache\Backend;

use Baseapp\Bootstrap;
use Phalcon\Cache\Backend\Memcache as PhMemcache;
use Phalcon\Cache\Exception;

class MyMemcache extends PhMemcache
{
    const GENERATE_DATA = 1;
    const NOT_FOUND     = 2;

    const LOCK_KEY_SUFFIX = '.lock';

    /**
     * @var int
     */
    private $result_code = self::NOT_FOUND;

    /**
     * Returns true if we're connected to the underlying Memcached daemon.
     * It attempts to connect (once) in case we're currently not connected.
     *
     * Unfortunately, this does not guarantee anything, nor does it cover all possible cases
     * of how/when the underlying daemon goes away. It can disappear at any point in time
     * between the connection attempt and any further get/set calls. It can even be currently
     * connected and then disappear right afterwards (but before any further get/set calls are attempted).
     *
     * But it's better than nothing for now.
     *
     * That's why it's really important to guard any get/set/etc calls with try/catch blocks and to handle ALL
     * possible cases in critical parts of code that rely on Memcached. Literally, ALL POSSIBLE CASES.
     *
     * @return bool
     */
    public function isConnected()
    {
        $memcache = $this->_memcache;

        if (!is_object($memcache)) {
            try {
                $this->_connect();
                $memcache = $this->_memcache;
            } catch (Exception $e) {
                Bootstrap::log('Failed to connect in MyMemcache::isConnected()');
                Bootstrap::log($e);
            }
        }

        return is_object($memcache);
    }

    /**
     * @param string|null $key
     * @param mixed $value
     * @param int $flag
     * @param int|null $expire
     *
     * @return bool
     */
    public function add($key = null, $value = null, $flag = null, $expire = null)
    {
        $memcache = $this->_memcache;

        if (!is_object($memcache)) {
            $this->_connect();
            $memcache = $this->_memcache;
        }

        return $memcache->add($key, $value, $flag, $expire);
    }

    /**
     * Get key from cache using a ghetto lock (which can be used to prevent multiple clients
     * from doing the same expensive compute operation simultaneously).
     *
     * The burden is on the API consumer to check results of MyMemcache::getResultCode() in order
     * to determine what they want to do next (and how).
     *
     * Example code:
     * ```
     * $get_result = $this->memcached->getWithLock($key, $lifetime);
     * $result_code = $this->memcached->getResultCode();
     * $generate_data = (MyMemcache::GENERATE_DATA == $result_code);
     * ```
     *
     * @param string $key
     * @param int $lifetime
     *
     * @return bool|mixed
     */
    public function getWithLock($key, $lifetime)
    {
        $result = parent::get($key, $lifetime);

        // If we have what we came for, return it
        if ($result) {
            return $result;
        }

        // If not, attempt to set a "lock" (which is just another memcached key really) which is
        // supposed to be used to guarantee only one consumer is actively rebuilding data.
        // This shifts the burden of dealing with a 'miss' back to the API consumer and
        // they then have to check getResultCode() to determine what they want to do further and how.

        // Adding 20 seconds to the specified $lifetime in order to provide leeway
        // for the expensive data to be generated. If the expensive compute operation
        // takes longer than that, this HAS TO BE MODIFIED.
        $lock_key = $key . self::LOCK_KEY_SUFFIX;
        $lock_value = mt_rand(1, 1000000000);
        $added = $this->add($lock_key, $lock_value, null, $lifetime + 20);

        if (true === $added) {
            $this->setResultCode(self::GENERATE_DATA);
            return false;
        }

        $this->setResultCode(self::NOT_FOUND);
        return false;
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function deleteLock($key)
    {
        return $this->delete($key . self::LOCK_KEY_SUFFIX);
    }

    /**
     * Returns true if the lock exists
     *
     * @param string $key
     *
     * @return bool
     */
    public function lockExists($key)
    {
        $lock   = $this->get($key . self::LOCK_KEY_SUFFIX);
        $result = (false !== $lock);

        return $result;
    }

    /**
     * @return int
     */
    public function getResultCode()
    {
        return $this->result_code;
    }

    /**
     * @param int $code
     */
    public function setResultCode($code)
    {
        $this->result_code = $code;
    }
}
