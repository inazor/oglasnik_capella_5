<?php
/**
 * Simple OIB validator.
 *
 * Example usage:
 * $validation->add('oib', new \Baseapp\Extension\Validator\Oib(array(
 *     'messageDigit' => 'Upisani OIB trebao bi se sastojati samo od brojki',
 *     'message' => 'Upisani OIB nema traženi broj znamenki'
 * )));
 *
 * @atuhor zytzagoo
 */

namespace Baseapp\Extension\Validator;

use Phalcon\Validation;
use Phalcon\Validation\Validator;
use Phalcon\Validation\ValidatorInterface;

class OibSimple extends Validator implements ValidatorInterface
{
    public function validate(Validation $validator, $attribute)
    {
        $value = $validator->getValue($attribute);
        $label = $this->getOption('label');

        // prep label
        if (empty($label)) {
            $label = $validator->getLabel($attribute);

            if (empty($label)) {
                $label = $attribute;
            }
        }

        // not numeric
        if (!is_numeric($value)) {
            $message = $this->getOption('messageDigit');
            $replace_pairs = array(':field' => $label);

            if (empty($message)) {
                $message = $validator->getDefaultMessage('Digit');
            }

            $validator->appendMessage(new \Phalcon\Validation\Message(strtr($message, $replace_pairs), $attribute, 'Digit'));
            return false;
        }

        // invalid len
        if (strlen($value) !== 11) {
            $message = $this->getOption('message');
            $replace_pairs = array(':field' => $label);

            if (empty($message)) {
                $message = $validator->getDefaultMessage('InvalidLen');
            }

            $validator->appendMessage(new \Phalcon\Validation\Message(strtr($message, $replace_pairs), $attribute, 'InvalidLen'));
            return false;
        }

        return true;
    }
}
