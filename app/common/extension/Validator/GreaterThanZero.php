<?php
/**
 * Custom number validator (values greater than zero).
 *
 * Example usage:
 * $validation->add('parameter_name', new \Baseapp\Extension\Validator\GreaterThanZero(array(
 *     'messageDigit' => 'Upisana kvadratura bi se trebala sastojati isključivo od brojki i decimalnog zareza',
 *     'message'      => 'Upisana kvadratura nije ispravna'
 * )));
 */

namespace Baseapp\Extension\Validator;

use Phalcon\Validation\Validator;
use Phalcon\Validation\ValidatorInterface;

class GreaterThanZero extends Validator implements ValidatorInterface
{
    public function validate(\Phalcon\Validation $validator, $attribute)
    {
        $value = $validator->getValue($attribute);
        $label = $this->getOption('label');

        // prep label
        if (empty($label)) {
            $label = $validator->getLabel($attribute);

            if (empty($label)) {
                $label = $attribute;
            }
        }

        // not numeric
        if (!is_numeric($value)) {
            $message = $this->getOption('messageDigit');
            $replace_pairs = array(':field' => $label);

            if (empty($message)) {
                $message = $validator->getDefaultMessage('Digit');
            }

            $validator->appendMessage(new \Phalcon\Validation\Message(strtr($message, $replace_pairs), $attribute, 'Digit'));
            return false;
        }

        $valid = $value > 0;

        if (!$valid) {
            $message = $this->getOption('message');
            $replace_pairs = array(':field' => $label);

            $validator->appendMessage(new \Phalcon\Validation\Message(strtr($message, $replace_pairs), $attribute));
            return false;
        }

        return true;
    }

}
