<?php
/**
 * Custom Interval validator.
 *
 * Example usage:
 * $validation->add('ad_params_<ID>', new \Baseapp\Extension\Validator\Interval(array(
 *     'messageInvalidData' => 'Invalid data',
 *     'messageInvalidRange' => 'Invalid range',
 *     'message' => 'At least one field should be filled'
* )));
 *
 */

namespace Baseapp\Extension\Validator;

use Phalcon\Validation;
use Phalcon\Validation\Validator;
use Phalcon\Validation\ValidatorInterface;

class Interval extends Validator implements ValidatorInterface
{
    public function validate(Validation $validator, $attribute)
    {
        $value_from = trim($validator->getValue($attribute . '_from'));
        $value_to = trim($validator->getValue($attribute . '_to'));
        $label = $this->getOption('label');
        // prep label
        if (empty($label)) {
            $label = $validator->getLabel($attribute);

            if (empty($label)) {
                $label = $attribute;
            }
        }

        // -- step 1

        // check if at least one field is filled
        if ('' === $value_from && '' === $value_to) {
            $message = $this->getOption('message');
            $replace_pairs = array(':field' => $label);
            if (empty($message)) {
                $message = 'At least one field should be filled';
            }

            $validator->appendMessage(new \Phalcon\Validation\Message(strtr($message, $replace_pairs), $attribute));
            return false;
        }

        // -- step 2

        if (is_numeric($value_from) || $value_from == '') {
            $value_from = intval($value_from);
        }
        if (is_numeric($value_to) || $value_to == '') {
            $value_to = intval($value_to);
        }
        // check if values can be represented ad integers?
        if (0 === $value_from && 0 === $value_to) {
            $message = $this->getOption('messageInvalidData');
            $replace_pairs = array(':field' => $label);
            if (empty($message)) {
                $message = 'Invalid data';
            }

            $validator->appendMessage(new \Phalcon\Validation\Message(strtr($message, $replace_pairs), $attribute, 'InvalidData'));
            return false;
        }

        // -- step 3

        if ($value_from && $value_to) {
            if ($value_to < $value_from) {
                $message = $this->getOption('messageInvalidRange');
                $replace_pairs = array(':field' => $label);
                if (empty($message)) {
                    $message = 'Invalid range';
                }

                $validator->appendMessage(new \Phalcon\Validation\Message(strtr($message, $replace_pairs), $attribute, 'InvalidRange'));
                return false;
            }
        }

        return true;
    }
}
