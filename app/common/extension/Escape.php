<?php

namespace Baseapp\Extension;

/**
 * Escape filter - convert characters to HTML entities
 */
class Escape
{

    /**
     * Add the new filter
     *
     * @param string $string string to filtering
     *
     * @return string filtered string
     */
    public function filter($string)
    {
        return htmlspecialchars((string) $string, ENT_QUOTES, 'UTF-8');
    }

}
