<?php

namespace Baseapp\Library\Products;

use Baseapp\Library\Products\Online\Pushup;
use Baseapp\Library\Utils;

class PushupProductsChooser
{
    /**
     * @var ProductsSelector
     */
    protected $selector;

    /**
     * @param ProductsSelector $selector
     */
    public function __construct(ProductsSelector $selector)
    {
        $this->selector = $selector;
    }

    /**
     * @param array $post_data
     *
     * @return string
     */
    public function getMarkup($post_data = array())
    {
        $markup = '<table class="table table-striped table-bordered table-responsive">';

        $markup .= '<thead>';
        $markup .= '<tr>';
        $markup .= '<th class="text-left">Oglas</th>';
        // $markup .= '<th class="text-center">Do besplatnog</th>';
        $markup .= '<th class="text-left">Usluga</th>';
        $markup .= '<th class="text-right">Cijena</th>';
        $markup .= '</tr>';
        $markup .= '</thead>';

        $markup .= '<tbody>';
        foreach ($this->selector->getProducts() as $product) {
            /* @var $product Pushup|ProductsInterface */

            $cost_lp = $product->getCost();

            $markup .= '<tr>';
            $markup .= '<td class="text-left">' . Utils::esc_html($product->getAd()->ident()) . '</td>';
            $markup .= '<td class="text-left">' . $product->getTitle() . '</td>';
            $markup .= '<td class="text-right" data-price="' . $cost_lp  . '">' . $product->formatDisplayPrice($cost_lp) . '</td>';
            $markup .= '</tr>';

            unset($product);
        }
        $markup .= '</tbody>';

        $markup .= '<tfoot>';
        $markup .= '<tr>';
        $markup .= '<td class="text-right" colspan="2"><strong>Ukupan iznos</strong></td>';
        $markup .= '<td class="text-right price"><strong>' . $this->getChosenProductsTotalCostDisplay() . '</strong></td>';
        $markup .= '</tr>';
        $markup .= '</tfoot>';

        $markup .= '</table>';

        return $markup;
    }

    /**
     * @return int
     */
    public function getChosenProductsTotalCost()
    {
        $total = 0;

        $products = $this->getChosenProducts();
        foreach ($products as &$product) {
            $total += $product->getCost();
        }
        unset($product); // http://php.net/manual/en/control-structures.foreach.php

        return $total;
    }

    /**
     * @return string
     */
    public function getChosenProductsTotalCostDisplay()
    {
        $amount_in_lp = $this->getChosenProductsTotalCost();
        $price    = Utils::format_money(Utils::lp2kn($amount_in_lp), true);
        $currency = 'kn';
        $price    = sprintf('%s %s', $price, $currency);
        return $price;
    }

    /**
     * @return array|ProductsInterface[]
     */
    public function getChosenProducts()
    {
        return $this->selector->getChosenProductsFlat();
    }

    /**
     * @return ProductsSelector
     */
    public function getProductsSelector()
    {
        return $this->selector;
    }
}
