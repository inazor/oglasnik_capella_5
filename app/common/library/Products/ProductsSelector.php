<?php

namespace Baseapp\Library\Products;

use Baseapp\Models\Categories;
use Baseapp\Models\Users;

/**
 * Class ProductsSelector provides a grouped and/or flat list of ProductsInterface instances
 *
 * @package Baseapp\Library\Products
 */
class ProductsSelector
{
    protected $products = array(
        'grouped' => array(),
        'flat'    => array()
    );

    protected $chosen_products = array(
        'grouped' => array(),
        'flat'    => array()
    );

    protected $category  = null;
    protected $user      = null;
    protected $post_data = null;

    public function __construct(Categories $category = null, Users $user = null, array &$post_data = null)
    {
        $this->category  = $category;
        $this->user      = $user;
        $this->post_data = $post_data;

        $this->process();

        return $this;
    }

    public function getPossibleList()
    {
        $product_list = array(
            'Online' => array(
                'Osnovni',
                'IstaknutiOnline',
                'IstaknutiNoExtras',
                'Premium',
                'PremiumNoExtras',
                'Platinum',
                'Pinky'
            ),
            'Offline' => array(
                'MaliPrivatni',
                'MaliFoto',
                'MaliBold',
                'IstaknutiOffline',
                'IstaknutiBoja'
            )
        );

        // prevent the notice in case we still don't know the user we're working with. the case when this happens is
        // when the products are being presented in the backoffice and we just started with process of creating a new
        // ad. in that specific moment, we know nothing about the user this ad will be related with!
        $user_type = !empty($this->user->type) ? $this->user->type : null;

        // Modifying offline group's first product for company users as they
        // should not be allowed to not-pay for stuff basically...
        if (Users::TYPE_COMPANY == $user_type) {
            $product_list['Offline'][0] = 'MaliPoslovni';
        }

        return $product_list;
    }

    protected function process()
    {
        foreach ($this->getPossibleList() as $group => $classes) {
            $group_lowercase = strtolower($group);
            foreach ($classes as $class_name) {
                $product = $this->createProduct($group, $class_name);
                $this->addProduct($product, $group_lowercase);
                if ($product->isChosen()) {
                    $this->addChosenProduct($product, $group_lowercase);
                }
            }
        }

        $this->ensureAtLeastOneChosenProduct();
    }

    /**
     * @param ProductsInterface $product
     * @param null|string $group
     *
     * @return bool
     */
    protected function addProduct(ProductsInterface $product, $group = null)
    {
        // Creating a list of all products
        if (null !== $group) {
            $this->products['grouped'][$group][] = $product;
        }

        $this->products['flat'][] = $product;

        return true;
    }

    /**
     * @param ProductsInterface $product
     * @param null|string $group
     *
     * @return bool
     */
    protected function addChosenProduct(ProductsInterface $product, $group = null)
    {
        if (null !== $group) {
            $this->chosen_products['grouped'][$group][] = $product;
        }

        $this->chosen_products['flat'][] = $product;

        return true;
    }

    /**
     * Return flat array/list of chosen products
     *
     * @return ProductsInterface[]
     */
    public function getChosenProductsFlat()
    {
        return $this->chosen_products['flat'];
    }

    /**
     * Return grouped array of chosen products
     *
     * @return array Associative array of groups and their corresponding ProductsInterface instances, i.e.
     *              ['online' => ProductsInterface[], 'offline' => ProductsInterface[]]
     */
    public function getChosenProductsGrouped()
    {
        return $this->chosen_products['grouped'];
    }

    /**
     * @param string $group
     * @param string $class_name
     *
     * @return ProductsInterface
     */
    protected function createProduct($group, $class_name)
    {
        $fq_class_name = $this->buildFullClassName($group, $class_name);
        /* @var $product ProductsInterface */
        $product = new $fq_class_name($this->category, $this->user, $this->post_data);

        return $product;
    }

    /**
     * @return ProductsInterface[]
     */
    public function getProducts()
    {
        return $this->products['flat'];
    }

    /**
     * @return array Associative array of groups and their corresponding ProductsInterface instances, i.e.
     *              ['online' => ProductsInterface[], 'offline' => ProductsInterface[]]
     */
    public function getGrouped()
    {
        return $this->products['grouped'];
    }

    public function buildFullClassName($group, $class_name)
    {
        $fq_class_name = '\Baseapp\Library\Products\\' . $group . '\\' . $class_name;

        return $fq_class_name;
    }

    /**
     * @return array|null
     */
    public function getPostData()
    {
        return $this->post_data;
    }

    /**
     * Returns true if $this->post_data should be considered "empty" (which
     * is not actually the case when only a single element is in there (the 'offline-products' checkbox), but we
     * really want it to be considered as empty in those cases too.
     *
     * @return bool
     */
    public function isEmptyPostData()
    {
        $empty = true;

        $data = $this->post_data;
        if (!empty($data)) {
            // Exception: if there's only one element in the array, and it's the 'offline-products' checkbox,
            // consider that as empty with regards to setting any other post data from it etc.
            $cnt = count($data);
            if (1 === $cnt && 'offline-products' === key($data)) {
                $empty = true;
            } else {
                $empty = false;
            }
        }

        return $empty;
    }

    /**
     * If no products are chosen explicitly either through post data or by presetting them
     * or via calling addChosenProduct(), we set the first one from the flat list of available products
     * as the chosen one.
     */
    protected function ensureAtLeastOneChosenProduct()
    {
        $chosen = $this->getChosenProductsFlat();
        if (empty($chosen)) {
            $products = $this->getProducts();
            $product = reset($products);
            $product->isChosen(true);
            $this->addChosenProduct($product, $product->getType());
        }
    }
}
