<?php

namespace Baseapp\Library\Products\Offline;

use Baseapp\Library\Products\Offline;

class MaliFoto extends Offline
{
    protected $id = 'mali-foto';
    protected $code = 'FotoPT';
    protected $title = 'Mali foto oglas';
    protected $description = '<p>Čak 58% ljudi prvo gleda oglase s fotografijom. Znatno si povećajte šanse za rješavanjem potrebe u što kraćem roku.</p>';

    protected $avus_adformat_single = 'Fotooglas single T';
    protected $avus_adlayout_single = 'PhotoAd_ORI';
    protected $avus_adformat_multi  = 'Fotooglas pack T';
    protected $avus_adlayout_multi  = 'PhotoAd_ORI';

    protected $options = array(
        '1 objava' => '4200',
        '2 objave' => '8200',
        '4 objave' => '15000',
        '8 objava' => '30000'
    );
}
