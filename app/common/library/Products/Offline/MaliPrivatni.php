<?php

namespace Baseapp\Library\Products\Offline;

use Baseapp\Library\Products\Offline;
use Baseapp\Models\Users;

class MaliPrivatni extends Offline
{
    protected $id = 'mali-privatni';
    protected $code = 'MaliPT';
    protected $title = 'Mali privatni oglas';
    protected $description = '<p>Mali besplatni oglas za oglašavanje u privatne svrhe i prodaju privatne imovine. Ukoliko želite rezervirati svoje mjesto za više izdanja, možete zakupiti pakete objava.</p>';

    protected $avus_adformat_single = 'Mali besplatniT';
    protected $avus_adlayout_single = 'Obični';
    protected $avus_adformat_multi  = 'Mali privatniT';
    protected $avus_adlayout_multi  = 'Obični';

    protected $options = array(
        '1 objava' => '0',
        '2 objave' => '3300',
        '4 objave' => '6200',
        '8 objava' => '12400'
    );

    protected $extras = false;

    protected $payment_required_before_publication = false;

    protected function configureByUser(Users $user)
    {
        if ($user->type == $user::TYPE_COMPANY) {
            $this->isDisabled(true);
        }
    }
}
