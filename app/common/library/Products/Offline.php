<?php

namespace Baseapp\Library\Products;

use Baseapp\Library\Utils;
use Baseapp\Models\Categories;

abstract class Offline extends Base
{
    protected $type = 'offline';

    // avus identifiers
    protected $avus_adformat_single = null;
    protected $avus_adlayout_single = null;
    protected $avus_adformat_multi  = null;
    protected $avus_adlayout_multi  = null;

    // Offline products have no extras by default
    protected $extras = false;

    // Offline products have a different default duration
    protected $default_duration = 1;

    protected $has_special_prices = false;

    public function configureByCategory(Categories $category)
    {
        $reconfigured = parent::configureByCategory($category);

        // TODO: move this into settings as well? This feels different than 'is category free/does category allow free ads'
        // since it's currently used to signify adding '+' to avus code basically... which in the end might go away
        // completely?
        $this->has_special_prices = $reconfigured;

        return $this->has_special_prices;
    }

    public function getConfigForm()
    {
        $product_id = $this->getId();

        $markup = '';
        $markup .= '<div class="panel product panel-info" data-product-id="' . $product_id . '">';

        $markup .= '<div class="panel-heading">';
        $markup .= '<div class="pull-right"><div class="checkbox no-margin"><label>';
        $markup .= '<input type="checkbox" class="product-disabler-checkbox" name="' . $product_id . '[disabled]"';
        $checkbox_checked = $this->isDisabled() ? ' checked="checked"' : '';
        $markup .= $checkbox_checked . ' value="1"> Disabled</label>';
        $markup .= '</div></div>';
        $markup .= '<h3 class="panel-title">' . $this->getTitle() . '</h3>';
        $markup .= '</div>';

        $markup .= '<div class="panel-body">';
        $markup .= '<div class="row">';

        $options = $this->getOptions();
        foreach ($options as $option_key => $option_value) {
            $option_id = $product_id . '-' . $option_key;
            $option_value_display = Utils::lp2kn($option_value);
            $option_repeat = $this->str_first_word($option_key);

            $markup .= '<div class="col-md-3 col-lg-3">';
            $markup .= '<div class="form-group product-option">';
            $markup .= '<label class="control-label" for="' . $option_id .'">' . $option_key . '</label>';
            $markup .= '<div class="input-group">';
            $markup .= '<input class="form-control text-right price" type="text" id="' . $option_id . '"';
            $markup .= ' name="' . $product_id . '[options][' . $option_key . '] placeholder="0" data-repeat="' . $option_repeat . '"';
            $markup .= ' value="' . $option_value_display . '">';
            $markup .= '<span class="input-group-addon">HRK</span>';
            $markup .= '</div>';
            $markup .= '</div>';
            $markup .= '</div>';
        }

        $markup .= '</div></div></div>';

        return $markup;
    }

    public function get_avus_adformat()
    {
        if ((int) $this->getDuration() == 1) {
            return $this->avus_adformat_single;
        } else {
            return $this->avus_adformat_multi;
        }
    }

    public function get_avus_adlayout()
    {
        if ((int) $this->getDuration() == 1) {
            return $this->avus_adlayout_single;
        } else {
            return $this->avus_adlayout_multi;
        }
    }

}
