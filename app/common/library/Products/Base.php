<?php

namespace Baseapp\Library\Products;

use Baseapp\Models\Categories;
use Baseapp\Library\Utils;
use Baseapp\Models\Users;

abstract class Base implements ProductsInterface
{
    // NOTE: id needs to be unique across all the product classes!
    // If two classes have the same id there's no way to differentiate whose data is exactly submitted via post!
    protected $id = '';
    protected $code;
    protected $type = '';
    protected $description = '';
    protected $title = '';

    /* @var $options false|array List of product options and their prices (if set) */
    protected $options = array();

    /* @var $extras false|array List of extra options and their prices (if set) */
    protected $extras = array();

    /* @var $push_up bool|string|int Push-up price in lipas (if set) */
    protected $push_up = false;

    protected $selected = null;
    protected $selected_extras = null;

    protected $selected_backup = null;

    /* @var $category Categories */
    protected $category = null;

    /* @var $user Users */
    protected $user = null;

    protected $disabled = false;
    protected $chosen = false;

    protected $orderable = true;
    protected $upgradable = true;

    /**
     * Used to assign the sort_order field in the 'ads' table at the same
     * time that expiry time and other stuff is calculated. Ad listings are
     * displayed ordered by something like `product_sort ASC, date_sort DESC`, so
     * lower sort_idx = higher position in the listings.
     * Overridden in various subclasses.
     *
     * @var $sort_idx int|null
     */
    protected $sort_idx = null;

    /**
     * TODO/FIXME: Not sure if we want this!
     * Used in getDuration() if/when parsing the selected option fails.
     * This means there could be weird edge cases happening if certain checks
     * are not properly done before assigning a product to an ad.
     *
     * @var int
     */
    protected $default_duration = 30;

    /**
     * @var bool Certain products can be published immediately, others require order/payment fulfillment beforehand by default
     */
    protected $payment_required_before_publication = true;

    public function __construct(Categories $category = null, Users $user = null, array &$post_data = null)
    {
        if (null !== $category) {
            $this->setCategory($category);
            $this->configureByCategory($category);
        }

        if (null !== $user) {
            $this->setUser($user);
            $this->configureByUser($user);
        }

        // Pre-sets options, extras and the product itself if the required data is found within POST
        if (null !== $post_data) {
            $this->selectViaPost($post_data);
        }
    }

    /**
     * @param Categories $category
     *
     * @return bool Returns a boolean indicating whether db-configured prices were applied or not
     */
    public function configureByCategory(Categories $category)
    {
        // For products that don't have a category specified (due to early bugs),
        // re-assign them a proper category once this is called... otherwise stuff blows up
        $this->setCategory($category);

        $reconfigured = false;

        $settings = $this->getCategorySettings('products_cfg_json', $this->getType(), $this->getFqcn());

        if ($settings) {
            if (isset($settings['disabled'])) {
                $this->isDisabled($settings['disabled']);
            }

            /*if (!empty($settings['title'])) {
                $this->setTitle($settings['title']);
            }*/

            if (isset($settings['options'])) {
                $this->setOptions($settings['options']);
                $reconfigured = true;
            }

            if (isset($settings['extras'])) {
                $this->setExtras($settings['extras']);
                $reconfigured = true;
            }

            if ($this->hasPushUp()) {
                if (isset($settings['push_up'])) {
                    // TODO: do we want a setter method to handle this?
                    $this->push_up = $settings['push_up'];
                    $reconfigured = true;
                }
            }
        }

        return $reconfigured;
    }

    /**
     * @param Users $user
     */
    protected function configureByUser(Users $user)
    {
        // no-op by default since only one specific Offline product deals
        // with this case for now (and this method is overridden there)
    }

    public function selectViaPost(array &$post_data = null)
    {
        $product_id = $this->getId();

        // Scan options POST data ($_POST['options'][$product_id])
        $chosen = $this->findInPost($post_data, 'options', $product_id);
        if (null !== $chosen) {
            $this->setSelectedOption($chosen);
        }

        // Scan extras POST data ($_POST['extras'][$product_id][$group])
        $extras = $this->getExtras();
        if ($extras) {
            foreach (array_keys($extras) as $group) {
                $chosen_extra = $this->findInPost($post_data, 'extras', array($product_id, $group));
                if (null !== $chosen_extra) {
                    $this->setSelectedExtras($group, $chosen_extra);
                }
            }
        }

        // Scan POST for the product itself and set internal chosen flag
        if ($this->existsInPost($post_data)) {
            $this->isChosen(true);
        }
    }

    private function findInPost(array &$post_data = null, $bucket = 'options', $search_key = null)
    {
        if (!is_array($post_data) || empty($post_data)) {
            return null;
        }

        if (!array_key_exists($bucket, $post_data)) {
            return null;
        }

        $chosen = $post_data[$bucket];
        $found = false;

        // Further check if the data stored under the key we just got is an array
        // If so, and if a $search_key is specified return the value(s) belonging
        // to the specified search key within the bucket
        if (is_array($chosen)) {
            if (null !== $search_key) {
                if (is_string($search_key)) {
                    // If $search_key is a string, just get crap stored under it if it exists within the bucket
                    if (isset($chosen[$search_key]) && !empty($chosen[$search_key])) {
                        $chosen = $chosen[$search_key];
                        $found = true;
                    }
                } elseif (is_array($search_key)) {
                    // If $search_key is an array, first element is the sub-bucket name
                    // and the second element is the search value
                    $sub_bucket = $search_key[0];
                    if (isset($chosen[$sub_bucket]) && !empty($chosen[$sub_bucket])) {
                        $chosen_bucket = $chosen[$sub_bucket];
                        $search_key = $search_key[1];
                        if (isset($chosen_bucket[$search_key]) && !empty($chosen_bucket[$search_key])) {
                            $chosen = $chosen_bucket[$search_key];
                            $found = true;
                        }
                    }
                }
            }
        }

        // If we didn't find anything reset the return value back to null (or we end up returning the entire bucket)
        if (!$found && null !== $chosen) {
            $chosen = null;
        }

        return $chosen;
    }

    public function existsInPost(array &$post_data = array())
    {
        $chosen = false;

        if (!$post_data) {
            return $chosen;
        }

        $search_key = 'products-' . $this->getType();

        if (!isset($post_data[$search_key])) {
            return $chosen;
        }

        // Look for the matching value in post data (which could be a simple string or an array)
        $search_value = $this->getId();
        if (is_array($post_data[$search_key])) {
            if (in_array($search_value, array_values($post_data[$search_key]))) {
                $chosen = true;
            }
        } else {
            if ($post_data[$search_key] === $search_value) {
                $chosen = true;
            }
        }

        return $chosen;
    }

    public function getFqcn()
    {
        return get_class($this);
    }

    /**
     * Returns true if the product has a push_up price specified
     *
     * @return bool
     */
    public function hasPushUp()
    {
        return (isset($this->push_up) && false !== $this->push_up);
        /*
        $extras = $this->getExtras();
        return ($extras && isset($extras['Push up']) && $extras['Push up'] > 0);
        */
    }

    /**
     * Returns the defined push_up price if the product has it as an option
     *
     * @return bool|int|null|string
     */
    public function getPushUpPrice()
    {
        $price = null;

        if ($this->hasPushUp()) {
            $price = $this->push_up;
        }

        return $price;
    }

    /**
     * Getter/Setter for $this->chosen depending on the optional param.
     *
     * @param null|bool $chosen Optional. Defaults to null, in which case the method acts as a getter. Otherwise it's a setter
     *
     * @return bool Current value of the chosen attribute
     */
    public function isChosen($chosen = null)
    {
        if (null !== $chosen) {
            $this->chosen = (bool) $chosen;
        }

        return $this->chosen;
    }

    public function isDisabled($disabled = null)
    {
        if (null !== $disabled) {
            $this->disabled = (bool) $disabled;
        }

        return $this->disabled;
    }

    public function isOrderable($orderable = null)
    {
        if (null !== $orderable) {
            $this->orderable = (bool) $orderable;
        }

        return $this->orderable;
    }

    public function isUpgradable($upgradable = null)
    {
        if (null !== $upgradable) {
            $this->upgradable = (bool) $upgradable;
        }

        return $this->upgradable;
    }

    public function getChooserMarkup()
    {
        $product_id = $this->getId();
        $radio_value = $product_id;
        $radio_name = 'products-' . $this->getType();
        $radio_id = 'products-' . $this->getType() . '-' . $radio_value;

        $selected_class = '';
        $radio_checked = '';
        if ($this->isChosen()) {
            $selected_class = ' on';
            $radio_checked = ' checked="checked"';
        }

        $radio_disabled = $this->isDisabled() ? ' disabled="disabled"' : '';

        $html = sprintf(
            '<div class="bg-white border-radius margin-bottom-sm product-box %s%s" data-id="%s" data-type="%s" data-currency-prefix data-currency-suffix="kn">',
            $product_id,
            $selected_class,
            $product_id,
            $this->getType()
        );

        $html .= '  <div class="box-body">';
        $html .= '      <div class="form-group radio">';
        $html .= '        <input class="radio-light-blue" type="radio"' . $radio_checked . ' name="' . $radio_name . '" id="' . $radio_id . '" value="' . $radio_value . '"' . $radio_disabled . '><label for="' . $radio_id . '">' . $this->getTitle() . '</label>';
        $html .= '      </div>';

        $html .= '      <div class="product-description">';
        $html .= ' ' . $this->getDescription();
        $html .= '      </div>';

        $html .= '      <div class="product-options">';
        $html .= '<div class="row"><div class="col-md-3 col-md-offset-9 col-sm-4 col-sm-offset-8 text-right">';
        $html .= '<div class="icon_dropdown">' . $this->buildOptionsSelect() . '</div>';
        $html .= '</div></div>';
        $html .= '      </div>';

        $extras_chooser = $this->buildExtrasChooser();
        if (!empty($extras_chooser)) {
            $html .= $extras_chooser;
        }

        $html .= sprintf('  %s', $this->getPriceMarkupPlaceholder());

        $html .= '  </div>'; // ./box-body

        $html .= '</div>'; // ./product-box

        return $html;
    }

    public function getBackendChooserMarkup()
    {
        $product_id = $this->getId();
        $radio_value = $product_id;
        $radio_name = 'products-' . $this->getType();
        $radio_id = 'products-' . $this->getType() . '-' . $radio_value;

        $selected_class = '';
        $radio_checked = '';
        if ($this->isChosen()) {
            $selected_class = ' on';
            $radio_checked = ' checked="checked"';
        }

        $radio_disabled = $this->isDisabled() ? ' disabled="disabled"' : '';

        $html = sprintf(
            '<div class="product-box-backend product-box %s%s" data-id="%s" data-type="%s" data-currency-prefix data-currency-suffix="kn">',
            $product_id,
            $selected_class,
            $product_id,
            $this->getType()
        );

        $html .= '  <div>';
        $html .= '    <div class="radio radio-primary radio-big">';
        $html .= '      <input type="radio"' . $radio_checked . ' name="' . $radio_name . '" id="' . $radio_id . '" value="' . $radio_value . '"' . $radio_disabled . '>';
        $html .= '      <label for="' . $radio_id . '"><span class="text-primary text-bold">' . $this->getTitle() . '</span></label>';
        $html .= '    </div>';
        $html .= '    <div class="product-options"><div class="icon_dropdown pull_right">';
        $html .= $this->buildOptionsSelect();
        $html .= '    </div></div>';
        $html .= '    <div class="clearfix"></div>';
        $html .= '  </div>';

        $html .= '  <div class="product-description">';
        $extras_chooser = $this->buildBackendExtrasChooser();
        if (!empty($extras_chooser)) {
            $html .= $extras_chooser;
        }
        $html .= '  </div>';

        $html .= '</div>';
        return $html;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function getCategorySettings($field = null, $key = null, $sub_key = null)
    {
        $settings_data = null;

        $cat = $this->getCategory();
        $settings = $cat->getSettings();

        if ($settings) {
            // Return the entire model object when no specific field is requested
            $settings_data = $settings;

            if (null !== $field) {
                if (!property_exists($settings, $field)) {
                    throw new \InvalidArgumentException('Field/property `' . $field . '` not found in `$settings` (CategorySettings)!');
                }
                // We have no easy way of knowing if the specific field really is json encoded in the first place
                // but since all products-related fields currently do, we're always decoding here (using assoc arrays)
                $settings_data = json_decode($settings->{$field}, true);
            }

            // Allows retrieving data for a single nested key from within the field's entire json_encoded array
            if (null !== $key && isset($settings_data[$key])) {
                $settings_data = $settings_data[$key];

                // Check second level if first level had something
                if (null !== $sub_key && isset($settings_data[$sub_key])) {
                    $settings_data = $settings_data[$sub_key];
                }
            }
        }

        return $settings_data;
    }

    public function saveCategorySettings($field, $data)
    {
        $cat = $this->getCategory();
        $settings = $cat->getSettings();

        if ($settings) {
            return $settings->save(array($field => json_encode($data)));
        }

        return false;
    }

    public function saveConfigForm($data)
    {
        $saved = false;

        if (!empty($data)) {
            // One field in the db holds json_encoded data for all the potential
            // products, first keyed by product type, then keyed on the fully qualified class name,
            // so first get the existing data stored there, and update/insert into that
            // specific key and save the entire json structure again
            $type = $this->getType();
            $key = $this->getFqcn();
            $settings = $this->getCategorySettings('products_cfg_json', $type, $key);
            if (!is_array($settings)) {
                $settings = array();
            }

            $settings[$type][$key] = $data;
            $saved = $this->saveCategorySettings('products_cfg_json', $settings);
        }

        return $saved;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getShoppingCartInfo()
    {
        $info = $this->getBillingIdentifier();

        $info .= sprintf(' (%s)', $this->formatDisplayPrice($this->getCost()));

        return $info;
    }

    public function getBillingIdentifier()
    {
        $code   = $this->getCode();
        $format = '%s';
        $args   = array($code);

        $duration = $this->getDuration();
        if ($duration) {
            $args[] = $duration;
            $format = '%s %sx';
        }

        return vsprintf($format, $args);
    }

    public function getDuration()
    {
        $duration = $this->getFirstWordOfSelectedOption();

        if (null === $duration) {
            $duration = $this->default_duration;
        }

        return $duration;
    }

    public function getExtrasDuration()
    {
        $duration = $this->getFirstWordOfSelectedExtraOption();

        if (null === $duration) {
            $duration = $this->default_duration;
        }

        return $duration;
    }

    // TODO: Maybe we should look for the first digit instead of just the first word? This works for now...
    public function getFirstWordOfSelectedOption()
    {
        $first = null;

        if (null !== $this->getOptions()) {
            $selected_option = $this->getSelectedOption();
            if ($selected_option) {
                // Handling special case of purchasing/upgrading extras on existing products
                if ('Do isteka' === $selected_option) {
                    $first = 0;
                } else {
                    $first = (int) $this->str_first_word($selected_option);
                    if ($first <= 0) {
                        $first = null;
                    }
                }
            }
        }

        return $first;
    }

    // TODO: Maybe we should look for the first digit instead of just the first word? This works for now...
    public function getFirstWordOfSelectedExtraOption()
    {
        $first = null;

        if ($this->hasExtras()) {
            $selected_extras = $this->getSelectedExtras();
            if ($selected_extras) {
                $first_extra_value = reset($selected_extras);
                $first = (int) $this->str_first_word($first_extra_value);
                if ($first <= 0) {
                    $first = null;
                }
            }
        }

        return $first;
    }

    // TODO: I have a feeling there's a better/faster way to do this
    public function str_first_word($str) {
        $first = null;

        $arr = explode(' ', $str);
        if (count($arr)) {
            $first = array_shift($arr);
        }

        return $first;
    }

    /**
     * @param $str
     *
     * @return int|mixed|null
     */
    public function getDurationDaysFromString($str)
    {
        $first = null;

        $arr = explode(' ', $str);
        if (count($arr)) {
            $first = array_shift($arr);
            if (!ctype_digit($first)) {
                $first = null;
            } else {
                $first = (int) $first;
            }
        }

        return $first;
    }

    public function isOfflineType()
    {
        return 'offline' === $this->type;
    }

    public function isOnlineType()
    {
        return 'online' === $this->type;
    }

    public function isPaymentRequiredBeforePublication()
    {
        $payment_required = $this->payment_required_before_publication;
        $cost = $this->getCost();

        if (false === $payment_required) {
            // If payment is not required by default, make sure it is required when needed
            $payment_required = ($cost > 0);
        } else {
            // If payment is required by default, make sure it's not required if cost is 0
            if ($cost <= 0) {
                $payment_required = false;
            }
        }

        return $payment_required;
    }

    public function isUserType()
    {
        return 'user' === $this->type;
    }

    public function getExtras()
    {
        return $this->extras;
    }

    public function hasExtras()
    {
        $extras = $this->getExtras();

        return !empty($extras);
    }

    public function insertOptionIfNotExists($label, $cost, $selected = false)
    {
        $options = $this->getOptions();
        if (!array_key_exists($label, $options)) {
            $options[$label] = $cost;
            $this->setOptions($options);
            if ($selected) {
                // Backup old selected option in case we're overriding
                $this->backupSelectedOption();
                // Mark the new option as selected
                $this->setSelectedOption($label);
            }
        }
    }

    public function restoreBackedUpSelectedOption()
    {
        $backup = $this->getSelectedOptionBackup();
        if (null !== $backup) {
            $this->selected = $backup;
        }
    }

    public function backupSelectedOption()
    {
        $selected = $this->getSelectedOption();
        if (null !== $selected) {
            $this->setSelectedOptionBackup($selected);
        }
    }

    public function getSelectedOptionBackup()
    {
        return $this->selected_backup;
    }

    public function setSelectedOptionBackup($option)
    {
        $this->selected_backup = $option;
    }

    public function getSelectedOption()
    {
        // Preventing having nothing selected since all the products that actually have
        // options need to have one selected (at least for now). If/when that changes,
        // one can override specific product's getSelectedOption() to change this.
        if (null === $this->selected) {
            $options = $this->getOptions();
            // If the product has options, set the first one as selected if nothing
            // was selected yet
            if (!empty($options)) {
                reset($options);
                $first_option_key = key($options);
                $this->setSelectedOption($first_option_key);
            }
        }

        return $this->selected;
    }

    public function getCost()
    {
        $amount = $this->getSelectedOptionCost();
        $amount += $this->getSelectedExtrasCost();

        return $amount;
    }

    public function getSelectedOptionCost()
    {
        $options = $this->getOptions();
        $amount = $options[$this->getSelectedOption()];

        return $amount;
    }

    public function getSelectedExtrasCost()
    {
        $amount = 0;

        $extras = $this->getExtras();
        if (!empty($extras)) {
            $selected = $this->getSelectedExtras();
            if (null !== $selected) {
                foreach ($selected as $which => $value) {
                    if (isset($extras[$which])) {
                        $extra_amount = $extras[$which];
                        // FIXME: special case of stupid default checkbox handling taken care here
                        if (is_string($value)) {
                            // see above
                            if ('on' !== $value) {
                                if (isset($extras[$which][$value])) {
                                    $extra_amount = $extras[$which][$value];
                                }
                            }
                        }
                        $amount += $extra_amount;
                    }
                }
            }
        }

        return $amount;
    }

    /**
     * Returns an array representing various billing details about
     * the product and its chosen options/extras for easier use in views
     * (payment forms, billing recap...)
     *
     * Array structure is currently:
     * [
     *    'title'        => 'Online Istaknuti oglas',
     *    'description'  => '',
     *    'total_cost'   => '450,00 Kn',
     *    'total_amount' => '45000',
     *    'items'        =>
     *    [
     *         [
     *            'title'        => 'Online Istaknuti oglas',
     *            'duration'     => '5 dana',
     *            'price'        => '100,00 kn',
     *            'price_amount' => '10000',
     *         ],
     *         [
     *            'title'        => 'Objava na naslovnici',
     *            'duration'     => '7 dana',
     *            'price'        => '350,00 kn',
     *            'price_amount' => '35000'
     *         ]
     *    ]
     * ]
     *
     * @return array
     */
    public function getBillingDetails()
    {
        $details = array();

        $cost_amount = $this->getCost();

        $details['title']        = $this->getBillingTitle();
        $details['description']  = $this->getDescription();
        $details['total_cost']   = $this->formatDisplayPrice($cost_amount);
        $details['total_amount'] = $cost_amount;

        $cost_selected_option_amount = $this->getSelectedOptionCost();
        $details['items'] = array(
            array(
                'title'        => $this->getBillingTitleWithSelectedOption(),
                'price'        => $this->formatDisplayPrice($cost_selected_option_amount),
                'price_amount' => $cost_selected_option_amount
            )
        );

        // Merge extras billing info if it exists
        // TODO: this should probably be removed when Payment/Order recap is finalized...
        $extras_billing_details = $this->getSelectedExtrasBillingInfo();
        if (!empty($extras_billing_details)) {
            $details['items'] = array_merge($details['items'], $extras_billing_details);
        }

        return $details;
    }

    /**
     * @return string
     */
    public function getBillingTitle()
    {
        $title = sprintf('%s %s', ucfirst($this->getType()), $this->getTitle());

        return $title;
    }

    /**
     * @return string
     */
    public function getBillingTitleWithSelectedOption()
    {
        $title = $this->getBillingTitle();

        // Include any additional selected options/duration in the title
        $selected_option = $this->getSelectedOption();
        if (!empty($selected_option)) {
            $title .= ' - ' . $selected_option;
        }

        return $title;
    }

    /**
     * @return string
     */
    public function getBillingTitleWithEverything()
    {
        $title = $this->getBillingTitleWithSelectedOption();

        // Include all selected extras (names and durations for now) in the title as well
        $extras          = $this->getExtras();
        $selected_extras = $this->getSelectedExtras();
        if (null !== $selected_extras) {
            foreach ($selected_extras as $which => $value) {
                if (isset($extras[$which])) {
                    $title .= ', ' . $which . ' - ' . $value;
                }
            }
        }

        return $title;
    }

    public function getSelectedExtrasBillingInfo()
    {
        $billing_info = array();

        $extras = $this->getExtras();
        if (!empty($extras)) {
            $selected_extras = $this->getSelectedExtras();
            if (null !== $selected_extras) {
                foreach ($selected_extras as $which => $value) {
                    if (isset($extras[$which])) {
                        $billing_row = array(
                            'title'        => $which . ' - ' . $value,
                            'price'        => null,
                            'price_amount' => null,
                        );

                        // FIXME: special case of stupid default checkbox handling taken care here
                        if (is_string($value)) {
                            // see above
                            if ('on' !== $value) {
                                if (isset($extras[$which][$value])) {
                                    $billing_row['price']        = $this->formatDisplayPrice($extras[$which][$value]);
                                    $billing_row['price_amount'] = $extras[$which][$value];
                                }
                            }
                        }

                        $billing_info[] = $billing_row;
                    }
                }
            }
        }

        return $billing_info;
    }

    public function getDescription()
    {
        $desc = $this->description;

        if (empty($desc)) {
            $desc = 'Default opis proizvoda. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
            laboris nisi ut aliquip ex ea commodo consequat.';
        }

        return $desc;
    }

    public function getSelectedExtras($group = null)
    {
        $ret = $this->selected_extras;

        if (null !== $group && isset($ret[$group])) {
            $ret = $ret[$group];
        }

        return $ret;
    }

    /**
     * @return int|null
     */
    public function getSortIdx()
    {
        return $this->sort_idx;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return null|Users
     */
    public function getUser()
    {
        return $this->user;
    }

    public function buildOptionsSelect()
    {
        $id          = $this->getId();
        $select_id   = 'options-' . $id;
        $select_name = 'options[' . $id . ']';

        $html = sprintf('<select class="select-product-options" id="%s" name="%s">', $select_id, $select_name);

        $selected = $this->getSelectedOption();
        foreach ($this->getOptions() as $key => $price_in_lipas) {
            $selected_attr = $selected === $key ? 'selected="selected" ' : '';
            $price         = $this->formatDisplayPrice($price_in_lipas);
            $display       = $key . ' / ' . $price;
            $duration      = $this->getDurationDaysFromString($key);
            $html .= sprintf(
                '<option %svalue="%s" data-price="%s" data-duration="%s">%s</option>',
                $selected_attr, $key, $price_in_lipas, $duration, $display
            );
        }

        $html .= '</select>';

        return $html;
    }

    public function buildExtrasChooser()
    {
        $markup = null;

        $extras = $this->getExtras();
        if (!empty($extras)) {
            $markup = '<div class="additional-options product-extras margin-top-20px padding-top-20px border-top-1px-solid blue-shade-04-border">';

            // extras have an additional level of nesting in theory, since
            // we can have more than one group of extras, each one having more than one possible price
            $extras_cnt = 0;
            $id = $this->getId();
            foreach ($extras as $group => $prices) {
                // Each $extra has one or more prices
                // in case prices is not an array, we generate slightly different markup

                $extras_cnt++;
                // $markup_id = 'product-extras-' . Utils::sanitize_filename($extra_display);
                $markup_id = 'extras-' . $id . '-' . $extras_cnt;
                $var_name = 'extras[' . $id . '][' . $group . ']';

                $selected = $this->getSelectedExtras($group);

                $markup .= '<div class="row product-extra">';
                $markup .= '  <div class="col-md-9 col-sm-8">';
                $markup .= '    <div class="checkbox checkbox-primary">';

                // In case a single price is defined for this group of extras, we build a single
                // checkbox specifying the price for that extra option.
                $checkbox_checked = (null !== $selected) ? ' checked="checked"' : '';
                $checkbox_data_attrs = ' data-target="#' . $markup_id . '"';
                $checkbox_label = $group;
                if (!is_array($prices)) {
                    $checkbox_data_attrs .= ' data-price="' . $prices . '"';
                    $price = $this->formatDisplayPrice($prices, true);
                    if (null !== $price) {
                        $checkbox_label .= sprintf(' <span class="labelprice"> - <span data-price="%s" class="value">%s</span></span>', $prices, $price);
                    }
                }

                $markup .= '      <input' . $checkbox_checked . ' class="extras-dropdown-toggle"' . $checkbox_data_attrs;
                $markup .= ' type="checkbox" name="' . $var_name . '" id="' . $markup_id. '-enabler">';
                $markup .= '      <label for="' . $markup_id . '-enabler"><span class="text-primary">' . $checkbox_label . '</span></label>';
                $markup .= '    </div>';
                $markup .= '  </div>';

                // If there are multiple prices, we generate a select dropdown for them
                if (is_array($prices)) {
                    $markup .= '  <div class="extras-prices col-md-3 col-sm-4 text-right">';
                    $markup .= '    <div class="icon_dropdown">';
                    $select_id = 'extras-' . $id . '-options-' . $extras_cnt;
                    $markup .= sprintf('<select name="%s" id="%s" class="select-product-extras-options">', $var_name, $select_id);
                    foreach ($prices as $price_key => $price_in_lipas) {
                        $selected_attr = $selected === $price_key ? 'selected="selected" ' : '';
                        $price         = $this->formatDisplayPrice($price_in_lipas);
                        $display       = $price_key . ' / ' . $price;
                        $duration      = $this->getDurationDaysFromString($price_key);
                        $markup .= sprintf(
                            '<option %svalue="%s" data-price="%s" data-duration="%s">%s</option>',
                            $selected_attr, $price_key, $price_in_lipas, $duration, $display
                        );
                    }
                    $markup .= '</select>';
                    $markup .= '    </div>';
                    $markup .= '  </div>';
                }

                // $markup .= '  <div class="clearfix"></div>';

                $markup .= '</div>';
            }

            $markup .= '</div>';
        }

        return $markup;
    }

    /**
     * Builds slightly different markup when generating the extras chooser for backend purposes
     *
     * @return null|string
     */
    public function buildBackendExtrasChooser()
    {
        $markup = null;

        $extras = $this->getExtras();
        if (!empty($extras)) {
            $markup = '<div class="additional-options product-extras margin-top-20px padding-top-20px border-top-1px-solid blue-shade-04-border">';

            // extras have an additional level of nesting in theory, since
            // we can have more than one group of extras, each one having more than one possible price
            $extras_cnt = 0;
            $id = $this->getId();
            foreach ($extras as $group => $prices) {
                // Each $extra has one or more prices
                // in case prices is not an array, we generate slightly different markup

                $extras_cnt++;
                // $markup_id = 'product-extras-' . Utils::sanitize_filename($extra_display);
                $markup_id = 'extras-' . $id . '-' . $extras_cnt;
                $var_name = 'extras[' . $id . '][' . $group . ']';

                $selected = $this->getSelectedExtras($group);

                $markup .= '<div class="product-extra">';
                //$markup .= '  <div class="col-md-9 col-sm-8">';
                $markup .= '    <div class="checkbox checkbox-primary">';

                // In case a single price is defined for this group of extras, we build a single
                // checkbox specifying the price for that extra option.
                $checkbox_checked = (null !== $selected) ? ' checked="checked"' : '';
                $checkbox_data_attrs = ' data-target="#' . $markup_id . '"';
                $checkbox_label = $group;
                if (!is_array($prices)) {
                    $checkbox_data_attrs .= ' data-price="' . $prices . '"';
                    $price = $this->formatDisplayPrice($prices, true);
                    if (null !== $price) {
                        $checkbox_label .= sprintf(' <span class="labelprice"> - <span data-price="%s" class="value">%s</span></span>', $prices, $price);
                    }
                }

                $markup .= '      <input' . $checkbox_checked . ' class="extras-dropdown-toggle"' . $checkbox_data_attrs;
                $markup .= ' type="checkbox" name="' . $var_name . '" id="' . $markup_id. '-enabler">';
                $markup .= '      <label for="' . $markup_id . '-enabler"><span class="text-primary">' . $checkbox_label . '</span></label>';
                // $markup .= '    </div>';
                $markup .= '  </div>';

                // If there are multiple prices, we generate a select dropdown for them
                if (is_array($prices)) {
                    $markup .= '  <div class="extras-prices text-right">';
                    $markup .= '    <div class="icon_dropdown">';
                    $select_id = 'extras-' . $id . '-options-' . $extras_cnt;
                    $markup .= sprintf('<select name="%s" id="%s" class="select-product-extras-options">', $var_name, $select_id);
                    foreach ($prices as $price_key => $price_in_lipas) {
                        $selected_attr = $selected === $price_key ? 'selected="selected" ' : '';
                        $price         = $this->formatDisplayPrice($price_in_lipas);
                        $display       = $price_key . ' / ' . $price;
                        $duration      = $this->getDurationDaysFromString($price_key);
                        $markup .= sprintf(
                            '<option %svalue="%s" data-price="%s" data-duration="%s">%s</option>',
                            $selected_attr, $price_key, $price_in_lipas, $duration, $display
                        );
                    }
                    $markup .= '</select>';
                    $markup .= '    </div>';
                    $markup .= '  </div>';
                }

                // $markup .= '  <div class="clearfix"></div>';

                $markup .= '</div>';
            }

            $markup .= '</div>';
        }

        return $markup;
    }

    public function getConfigForm()
    {
        return null;
    }

    public function prepPostedConfigData($post_data = array())
    {
        $data = null;

        // Re-formatting posted form data belonging to this product into an array keyed with the fqcn (fully qualified class name)
        $product_id = $this->getId();

        $post_bucket = isset($post_data[$product_id]) && is_array($post_data[$product_id]) ? $post_data[$product_id] : array();

        $posted_options = isset($post_bucket['options']) && is_array($post_bucket['options']) ? $post_bucket['options'] : array();
        $posted_extras = isset($post_bucket['extras']) && is_array($post_bucket['extras']) ? $post_bucket['extras'] : false;
        $posted_title = isset($post_bucket['title']) ? $post_bucket['title'] : $this->getTitle();

        $posted_push_up = null;
        if ($this->hasPushUp()) {
            $posted_push_up = isset($post_bucket['push_up']) ? $post_bucket['push_up'] : $this->push_up;
        }

        // Transform nicely formatted prices in kunas with separators and whatnot into
        // a simple string representing the amount in lipas
        foreach ($posted_options as $k => $v) {
            $posted_options[$k] = Utils::kn2lp($v);
        }

        if (false !== $posted_extras && is_array($posted_extras)) {
            foreach ($posted_extras as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $kk => $vv) {
                        $posted_extras[$k][$kk] = Utils::kn2lp($vv);
                    }
                } else {
                    $posted_extras[$k] = Utils::kn2lp($v);
                }
            }
        }

        if (!empty($post_bucket)) {
            $data = array();
            $data['fqcn'] = $this->getFqcn();
            $data['title'] = $posted_title;
            $data['options'] = $posted_options;
            $data['extras'] = $posted_extras;
            if (null !== $posted_push_up) {
                $data['push_up'] = Utils::kn2lp($posted_push_up);
            }
            $data['disabled'] = false;
            if (isset($post_bucket['disabled']) && $post_bucket['disabled']) {
                $data['disabled'] = true;
            }
        }

        return $data;
    }

    /**
     * @param $amount_in_lp
     * @param bool $null_if_free Optional. Defaults to false. If true, returns null in case the amount is <= 0
     *
     * @return string|null
     */
    public function formatDisplayPrice($amount_in_lp, $null_if_free = false)
    {
        if (!$amount_in_lp || $amount_in_lp <= 0) {
            $price = 'besplatno';
            if ($null_if_free) {
                $price = null;
            }
        } else {
            $price    = Utils::format_money(Utils::lp2kn($amount_in_lp), true);
            $currency = 'kn';
            $price    = sprintf('%s %s', $price, $currency);
        }

        return $price;
    }

    public function getPaymentRequiredBeforePublication()
    {
        return $this->payment_required_before_publication;
    }

    public function getPriceMarkupPlaceholder()
    {
        $html = '<h4 class="total-product-price text-right">cijena: ';
        $html .= ' <span class="text-primary has-price"><span class="price"></span> kn</span>';
        $html .= ' <span class="text-primary no-price" style="display:none;">besplatno</span>';
        $html .= '</h4>';

        return $html;
    }

    /**
     * Setters
     */

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @param bool|array $extras
     */
    public function setExtras($extras)
    {
        $this->extras = $extras;
    }

    public function setPaymentRequiredBeforePublication($required)
    {
        $this->payment_required_before_publication = (bool) $required;
    }

    /**
     * @param $option_key
     */
    public function setSelectedOption($option_key)
    {
        $keys = array_keys($this->getOptions());

        if (!in_array($option_key, $keys)) {
            throw new \InvalidArgumentException('Invalid option `' . $option_key . '` (valid options: `' . implode('`, `', $keys) . '`)');
        }

        $this->selected = $option_key;
    }

    /**
     * @param $key
     * @param null|string $opt_choice
     */
    public function setSelectedExtras($key, $opt_choice = null)
    {
        $extras = $this->getExtras();

        // Check key validity
        $valid_keys = array_keys($extras);
        if (!in_array($key, $valid_keys)) {
            throw new \InvalidArgumentException('Invalid key `' . $key . '` (valid keys: ' . print_r($valid_keys, true) . ')');
        }

        // Check choice (if needed)
        if (isset($extras[$key]) && is_array($extras[$key])) {
            // grouped extras require a specific choice
            if (null === $opt_choice) {
                throw new \InvalidArgumentException('Missing `$opt_choice` value for key `' . $key . '`');
            }

            $valid_choices = array_keys($extras[$key]);
            if (!in_array($opt_choice, $valid_choices)) {
                throw new \InvalidArgumentException('Invalid choice `' . $opt_choice. '` (valid choices: ' . print_r($valid_choices, true) . ')');
            }
        }

        // Set what was given
        if (null !== $opt_choice) {
            $this->selected_extras[$key] = $opt_choice;
        } else {
            $this->selected_extras[$key] = true;
        }
    }

    public function unsetSelectedExtras($key, $opt_choice)
    {
        unset($this->selected_extras[$key]);
    }

    /**
     * @param Categories $category
     */
    public function setCategory(Categories $category)
    {
        $this->category = $category;
    }

    /**
     * @param array $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * @param Users $user
     */
    public function setUser(Users $user)
    {
        $this->user = $user;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
}
