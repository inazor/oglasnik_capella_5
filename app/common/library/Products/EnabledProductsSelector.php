<?php

namespace Baseapp\Library\Products;

/**
 * Class EnabledProductsSelector provides a grouped and/or flat list of non-disabled ProductsInterface instances
 *
 * @package Baseapp\Library\Products
 */
class EnabledProductsSelector extends ProductsSelector
{
    /**
     * Overridden parent method prevents adding disabled products to the list
     *
     * @param ProductsInterface $product
     * @param null|string $group
     *
     * @return bool
     */
    protected function addProduct(ProductsInterface $product, $group = null)
    {
        if ($product->isDisabled()) {
            return false;
        }

        return parent::addProduct($product, $group);
    }
}
