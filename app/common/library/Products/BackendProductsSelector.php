<?php

namespace Baseapp\Library\Products;

/**
 * Class BackendProductsSelector provides a way to get all possible products so they could be configured in the backend
 *
 * @package Baseapp\Library\Products
 */
class BackendProductsSelector extends ProductsSelector
{
    /**
     * Overridden parent method displays all products
     * (no matter the user's type or if they're enabled or not etc.)
     *
     * @return array
     */
    public function getPossibleList()
    {
        return array(
            'Online' => array(
                'Osnovni',
                'IstaknutiOnline',
                'IstaknutiNoExtras',
                'Premium',
                'PremiumNoExtras',
                'Platinum',
                'Pinky'
            ),
            'Offline' => array(
                'MaliPrivatni',
                'MaliPoslovni',
                'MaliFoto',
                'MaliBold',
                'IstaknutiOffline',
                'IstaknutiBoja'
            )
        );
    }

    /**
     * Helper method to get a list of all products in an array so we can
     * populate a select with them
     *
     * @return array
     */
    public function getPossibleIdsList()
    {
        $possible_list     = $this->getPossibleList();
        $possible_ids_list = array();

        foreach ($possible_list as $group => $classes) {
            foreach ($classes as $class_name) {
                $product = $this->createProduct($group, $class_name);
                $possible_ids_list[$group . '_' . $product->getId()] = $group . '\\' . $class_name;
            }
        }

        return $possible_ids_list;
    }
}
