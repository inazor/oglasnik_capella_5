<?php

namespace Baseapp\Library\Products;

#use Baseapp\Models\Users;

class Trgovina extends Online
{
    protected $type = 'trgovina';

    protected $id          = 'trgovina';
    protected $title       = 'Trgovina';
    protected $description = 'Trgovina omogućuje automatski import oglasa.';

    protected $push_up = false;
    protected $extras  = false;
    protected $payment_required_before_publication = false;

    /**
     * @var array Holds extra data that might be needed when fulfilling/finalizing payment for this product
     */
    protected $order_data = array();

    public function getChooserMarkup()
    {
        return false;
    }

    public function getBackendChooserMarkup()
    {
        return false;
    }

    /**
     * @param null|string $key
     *
     * @return array|mixed
     */
    public function getOrderData($key = null)
    {
        $data = $this->order_data;

        // Check if only a specific piece of data was specified
        if (null !== $key) {
            if (isset($data[$key])) {
                $data = $data[$key];
            }
        }

        return $data;
    }

    /**
     * @param array $data
     */
    public function setOrderData(array $data)
    {
        $this->order_data = $data;
    }
}
