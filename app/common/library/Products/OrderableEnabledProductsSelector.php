<?php

namespace Baseapp\Library\Products;

/**
 * Class OrderableEnabledProductsSelector provides a grouped and/or flat list of orderable ProductsInterface instances
 */
class OrderableEnabledProductsSelector extends EnabledProductsSelector
{
    /**
     * Overridden parent method prevents adding non-purchasable products to the list
     *
     * @param ProductsInterface $product
     * @param null|string $group
     *
     * @return bool
     */
    protected function addProduct(ProductsInterface $product, $group = null)
    {
        if (!$product->isOrderable()) {
            return false;
        }

        return parent::addProduct($product, $group);
    }
}
