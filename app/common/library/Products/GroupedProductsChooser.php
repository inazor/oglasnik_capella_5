<?php

namespace Baseapp\Library\Products;

use Baseapp\Library\Utils;

class GroupedProductsChooser
{
    /**
     * @var ProductsSelector
     */
    protected $selector;

    /**
     * @param ProductsSelector|null $selector
     */
    public function __construct(ProductsSelector $selector = null)
    {
        if (null === $selector) {
            $selector = new ProductsSelector();
        }

        $this->selector = $selector;
    }

    /**
     * @param array $post_data
     *
     * @return string
     */
    public function getMarkup($post_data = array())
    {
        $markup = '';

        foreach ($this->selector->getGrouped() as $group => $group_products) {
            $group_hidden = '';

            // Add a toggling checkbox before the offline group
            if ('offline' === $group) {
                $checkbox_checked = isset($post_data['offline-products']) ? ' checked="checked"' : '';
                if (empty($checkbox_checked)) {
                    $group_hidden = ' hidden';
                }
                $markup .= '
                    <div class="checkbox-toggle">
                        <div class="checkbox checkbox-primary">
                            <input type="checkbox"' . $checkbox_checked . ' class="target-toggler" data-toggle-target=".products-group-offline" name="offline-products" id="offline-products">
                            <label for="offline-products"><span class="text-primary">Želim oglas za tiskano izdanje</span></label>
                        </div>
                    </div>';
            }

            $markup .= '<div class="products-group products-group-' . $group . $group_hidden . '">';
            $markup .= '<p class="margin-top-0 margin-bottom-30px blue-shade-03">Za ' . ('offline' === $group ? 'tiskano' : 'internet') . ' izdanje - odaberite</p>';

            foreach ($group_products as $k => &$product) {
                /* @var $product ProductsInterface */
                $markup .= $product->getChooserMarkup();
            }
            unset($product);

            $markup .= '</div>';
        }

        return $markup;
    }

    /**
     * @return int
     */
    public function getChosenProductsTotalCost()
    {
        $total = 0;

        $products = $this->getChosenProducts('flat');
        foreach ($products as &$product) {
            $total += $product->getCost();
        }
        unset($product); // http://php.net/manual/en/control-structures.foreach.php

        return $total;
    }

    /**
     * @return string
     */
    public function getChosenProductsTotalCostDisplay()
    {
        $amount_in_lp = $this->getChosenProductsTotalCost();
        $price    = Utils::format_money(Utils::lp2kn($amount_in_lp), true);
        $currency = 'kn';
        $price    = sprintf('%s %s', $price, $currency);
        return $price;
    }

    /**
     * @param string $grouped_or_flat
     *
     * @return array|ProductsInterface[]
     */
    public function getChosenProducts($grouped_or_flat = 'grouped')
    {
        if ('grouped' === $grouped_or_flat) {
            return $this->selector->getChosenProductsGrouped();
        } else {
            return $this->selector->getChosenProductsFlat();
        }
    }

    /**
     * @return ProductsSelector
     */
    public function getProductsSelector()
    {
        return $this->selector;
    }

    public function getPostData()
    {
        return $this->getProductsSelector()->getPostData();
    }
}
