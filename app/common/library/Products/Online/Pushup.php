<?php

namespace Baseapp\Library\Products\Online;

use Baseapp\Library\Products\Online;
use Baseapp\Library\Products\ProductsInterface;
use Baseapp\Models\Ads;

class Pushup extends Online
{
    protected $id          = 'pushup';
    protected $title       = 'Skok na vrh';
    protected $description = 'Skok na vrh unutar kategorije.';

    protected $options          = false;
    protected $default_duration = null;

    /**
     * @var Ads|null
     */
    protected $ad = null;

    // Default cost is 5kn
    protected $cost = '500';

    public function getDuration()
    {
        return 0;
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function setCost($amount)
    {
        $this->cost = $amount;
    }

    public function setCostFromProduct(ProductsInterface $product)
    {
        if ($product->hasPushUp()) {
            $this->setCost($product->getPushUpPrice());
        }
    }

    /**
     * @param Ads $ad
     */
    public function setAd(Ads $ad)
    {
        $this->ad = $ad;
    }

    /**
     * @return Ads|null
     */
    public function getAd()
    {
        return $this->ad;
    }
}
