<?php

namespace Baseapp\Library\Products;

use Baseapp\Models\Categories;
use Baseapp\Models\Users;

interface ProductsInterface
{
    public function __construct(Categories $category = null, Users $user = null, array &$post_data = null);

    public function existsInPost(array &$post_data = null);

    public function formatDisplayPrice($amount_in_lp, $null_if_free = false);

    public function configureByCategory(Categories $category);

    public function getBillingIdentifier();
    public function getBillingDetails();
    public function getBillingTitle();
    public function getBillingTitleWithSelectedOption();
    public function getBillingTitleWithEverything();
    public function getShoppingCartInfo();
    public function getCategory();
    public function getCategorySettings($field = null, $key = null);
    public function saveCategorySettings($field, $data);
    public function saveConfigForm($data);
    public function getChooserMarkup();
    public function getCode();
    public function getConfigForm();
    public function getCost();
    public function getFqcn();
    public function getDescription();
    public function getDuration();
    public function getId();
    public function getOptions();
    public function getExtras();
    public function hasExtras();
    public function getPaymentRequiredBeforePublication();
    public function getSelectedOption();
    public function getSelectedOptionCost();
    public function getSelectedExtras($group = null);
    public function getSelectedExtrasBillingInfo();
    public function getSortIdx();
    public function getTitle();
    public function getType();
    public function getUser();

    public function hasPushUp();
    public function getPushUpPrice();

    public function insertOptionIfNotExists($label, $cost, $selected = false);
    public function backupSelectedOption();
    public function getSelectedOptionBackup();
    public function setSelectedOptionBackup($option);
    public function restoreBackedUpSelectedOption();
    public function isOnlineType();
    public function isOfflineType();
    public function isUserType();

    public function isChosen($chosen = null);
    public function isDisabled($disabled = null);
    public function isOrderable($orderable = null);
    public function isPaymentRequiredBeforePublication();
    public function isUpgradable($upgradable = null);

    public function prepPostedConfigData($post_data = array());
    public function selectViaPost(array &$post_data = null);

    public function setCategory(Categories $category);
    public function setCode($code);
    public function setId($id);
    public function setOptions($options);
    public function setSelectedOption($option_key);
    public function setSelectedExtras($key, $opt_choice = null);
    public function setTitle($title);
    public function setPaymentRequiredBeforePublication($required);
}
