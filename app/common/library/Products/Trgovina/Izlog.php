<?php

namespace Baseapp\Library\Products\Trgovina;

use Baseapp\Library\Products\Trgovina;

class Izlog extends Trgovina
{
    protected $id          = 'izlog';
    protected $title       = 'Izlog';
    protected $description = 'Izlog za promociju trgovine kroz maksimalno 3 kategorije.';

    protected $options = array(
        '30 dana' => '150000'
    );
}
