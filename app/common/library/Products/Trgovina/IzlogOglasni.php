<?php

namespace Baseapp\Library\Products\Trgovina;

use Baseapp\Library\Products\Trgovina;

class IzlogOglasni extends Trgovina
{
    protected $id          = 'izlog-oglasni';

    protected $title       = 'Oglasni izlog';
    protected $description = 'Opis oglasnog izloga.';

    protected $options = array(
        '30 dana' => '150000'
    );
}
