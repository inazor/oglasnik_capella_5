<?php

namespace Baseapp\Library\Products\Trgovina;

use Baseapp\Library\Products\Trgovina;

class IzlogIntro extends Trgovina
{
    protected $id          = 'izlog-intro';

    protected $title       = 'Intro izlog';
    protected $description = 'Opis intro izloga.';

    protected $options = array(
        '30 dana' => '150000'
    );
}
