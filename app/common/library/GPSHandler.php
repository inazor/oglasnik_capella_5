<?php

namespace Baseapp\Library;

/**
 * GPSHandler Library
 */
class GPSHandler
{
    private $north     = null;
    private $south     = null;
    private $west      = null;
    private $east      = null;
    private $latitude  = null;
    private $longitude = null;
    private $zoomLevel = 16;

    public function __construct($latitude = null, $longitude = null)
    {
        if (is_array($latitude) && 4 === count($latitude) && $this->setBounds($latitude)) {
            $longitude = null;
        }

        $this->setPoint($latitude, $longitude);
    }

    public function setBounds($south = null, $west = null, $north = null, $east = null)
    {
        $return = false;

        if (is_array($south) && 4 === count($south)) {
            if (isset($south['north']) && isset($south['south']) && isset($south['west']) && isset($south['east'])) {
                $this->south = $south['south'];
                $this->west  = $south['west'];
                $this->north = $south['north'];
                $this->east  = $south['east'];
            } else {
                $this->south = $south[0];
                $this->west  = $south[1];
                $this->north = $south[2];
                $this->east  = $south[3];
            }
            $return = true;
        }

        if (!$return && $south && $west && $north && $east) {
            $this->south = $south;
            $this->west  = $west;
            $this->north = $north;
            $this->east  = $east;
            $return = true;
        }

        return $return;
    }

    public function setZoomLevel($zoomLevel = 16)
    {
        $this->zoomLevel = (int) $zoomLevel;
    }

    public function setPoint($latitude = null, $longitude = null)
    {
        $return = false;

        if ($latitude && (is_array($latitude) || false !== strpos($latitude, ','))) {
            if (is_array($latitude)) {
                if (2 === count($latitude)) {
                    $this->latitude  = (float) trim($latitude[0]);
                    $this->longitude = (float) trim($latitude[1]);
                    $return          = true;
                } else {
                    $latitude = null;
                }
            } else {
                $point = explode(',', $latitude);
                if (2 === count($point)) {
                    $this->latitude  = (float) trim($point[0]);
                    $this->longitude = (float) trim($point[1]);
                    $return          = true;
                }
            }
        }

        if (!$return && $latitude && $longitude) {
            $this->latitude  = (float) trim($latitude);
            $this->longitude = (float) trim($longitude);
            $return = true;
        }

        return $return;
    }

    public function getBounds($boundsType = null)
    {
        $bounds = null;
        if ($this->south && $this->west && $this->north && $this->east) {
            switch ($boundsType) {
                case 'array':
                    $bounds = array(
                        'south' => $this->south,
                        'west'  => $this->west,
                        'north' => $this->north,
                        'east'  => $this->east
                    );
                    break;
                /**
                 * Google Map's bounds' object is formated with it's properties in following order
                 * Object { 
                 *     south: 45.79433098150818,
                 *     west: 15.91075992902222,
                 *     north: 45.81494351180359,
                 *     east: 15.931573870977786
                 * }
                 *
                 * So, when google map's native method toUrlString() is called, it basically combines all these 
                 * properties in one string string glued with comma (,), but their order is as in it's object
                 * representation
                 *
                 * i.e. "45.79433098150818,15.91075992902222,45.81494351180359,15.931573870977786"
                 */
                case 'googleMapString':
                    $bounds = implode(',', array(
                        $this->south,
                        $this->west,
                        $this->north,
                        $this->east
                    ));
                    break;
                default:
                    $bounds        = new \stdClass();
                    $bounds->south = $this->south;
                    $bounds->west  = $this->west;
                    $bounds->north = $this->north;
                    $bounds->east  = $this->east;
            }
        }

        return $bounds;
    }

    public function getGoogleMapsBoundsString()
    {
        return $this->getBounds('googleMapString');
    }
}
