<?php

namespace Baseapp\Library;

use Baseapp\Models\Ads;
use Baseapp\Library\Email;

class Moderation
{
    const MODERATION_STATUS_WAITING  = 'waiting';
    const MODERATION_STATUS_OK       = 'ok';
    const MODERATION_STATUS_NOK      = 'nok';
    const MODERATION_STATUS_REPORTED = 'reported';

    /* @var $ad Ads */
    protected $ad = null;

    public function __construct(Ads &$ad = null)
    {
        if (null !== $ad) {
            $this->setAd($ad);
        }
    }

    /**
     * @param Ads $ad
     */
    public function setAd(Ads $ad)
    {
        $this->ad = $ad;
    }

    public function getPossibleModerationStatuses() {
        $statuses = array(
            self::MODERATION_STATUS_WAITING  => 'Awaiting moderation',
            self::MODERATION_STATUS_OK       => 'Moderation OK',
            self::MODERATION_STATUS_NOK      => 'Moderation NOK',
            self::MODERATION_STATUS_REPORTED => 'Reported ad',
        );

        return $statuses;
    }

    public function getModerationReasonsData($status = 'all')
    {
        $data = array();

        $data[self::MODERATION_STATUS_OK] = array(
            'ok-oglas-prosao-moderaciju' => array(
                'title'   => 'Vaš oglas je prošao moderaciju (default)',
                'subject' => 'Oglasnik.hr obavijest - Vaš oglas je prošao moderaciju',
                'body'    => ''
            ),
            'ok-navedeno-vise-proizvoda' => array(
                'title'   => '(default) + Navedeno više proizvoda',
                'subject' => 'Oglasnik.hr obavijest - Vaš oglas je prošao moderaciju',
                'body'    => 'U predanom oglasu naveli ste više proizvoda pa Vam, za bolju preglednost, savjetujemo objavu svakog predmeta/proizvoda ili usluge u zasebnom oglasu i u pripadajućom kategoriji. Što više detalja o proizvodu (cijena, količina, dimenzije, perfomanse, slike, video...) ponudite, to će veću pažnju privući vaš oglas. Rubrika za koju ste predali oglas nema ograničenja u broju dopuštenih besplatnih oglasa. Imate li dodatnih pitanja nazovite naš odjel Korisničke podrške na tel. 01/6102-885 radnim danom 8-17h ili pošaljite upit na e-mail <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a>.'
            ),
            'ok-neuspjesna-provjera-kontakta-oglasivaca' => array(
                'title'   => '(default) + Neuspješna provjera kontakta oglašivača',
                'subject' => 'Oglasnik.hr obavijest - Vaš oglas je prošao moderaciju',
                'body'    => 'Oglasnik zadržava pravo provjere kontakt podataka iz oglasa, a prije objavljivanja u tiskanom ili online izdanju Oglasnika. Vaš oglas nismo uspjeli provjeriti. Molimo da se javite našem odjelu Korisničke podrške na tel. 01/6102-885 radnim danom 8-17 h, kako bi smo aktivirali oglas za objavu.'
            ),
            'ok-dodati-fotografije' => array(
                'title'   => '(default) + Dodati fotografije',
                'subject' => 'Oglasnik.hr obavijest - Vaš oglas je prošao moderaciju',
                'body'    => 'Kako biste povećali broj posjeta i privukli pažnju potencijalnih kupaca, dodajte do 20 fotografija uz Vaš oglas ili video. Dodavanje fotografija/videa se NE naplaćuje. Trebate li pomoć, nazovite naš odjel Korisničke podrške na tel. 01/6102-885 radnim danom 8-17h ili pošaljite upit na e-mail <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a>.'
            ),
            'ok-nedovoljno-kvalitetna-fotografija-za-tisak' => array(
                'title'   => '(default) + Nedovoljno kvalitetna fotografija za tisak',
                'subject' => 'Oglasnik.hr obavijest - Vaš oglas je prošao moderaciju',
                'body'    => 'Fotografija u oglasu kojeg želite objaviti, nažalost, nije dovoljno kvalitetna za objavljivanje u tisku. Molimo, obratite se našem odjelu Korisničke podrške na tel. 01/6102-885 radnim danom 8-17h ili pošaljite upit na e-mail <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a>.'
            ),
            'ok-rubrika-nema-ogranicenja-broja-oglasa' => array(
                'title'   => '(default) + Rubrika nema ograničenja broja oglasa',
                'subject' => 'Oglasnik.hr obavijest - Vaš oglas je prošao moderaciju',
                'body'    => 'Rubrika za koju ste predali oglas nema ograničenja u broju dopuštenih besplatnih oglasa. Savjetujemo objavu svakog predmeta/proizvoda u zasebnom oglasu i u pripadajućoj kategoriji. Što više detalja o proizvodu (cijena, količina, dimenzije, perfomanse, slike, video....) ponudite, to će veću pažnju privući vaš oglas. Dodavanje fotografija/videa se NE naplaćuje. Imate li dodatnih pitanja, nazovite naš odjel Korisničke podrške na tel. 01/6102-885 radnim danom 8-17h ili pošaljite upit na e-mail <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a>.'
            ),
            'ok-fotografija-ne-odgovara-tekstu-oglasa' => array(
                'title'   => '(default) + Fotografija ne odgovara tekstu oglasa',
                'subject' => 'Oglasnik.hr obavijest - Vaš oglas je prošao moderaciju',
                'body'    => 'Poštovani, fotografija koju ste pridodali oglasu ne odgovara tekstu oglasa. Imate li dodatnih pitanja, nazovite naš odjel Korisničke podrške na tel. 01/6102-885 radnim danom 8-17h ili pošaljite upit na e-mail <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a>.'
            ),
            'ok-navesti-kolicinu-povrsinu' => array(
                'title'   => '(default) + Navesti količinu/površinu',
                'subject' => 'Oglasnik.hr obavijest - Vaš oglas je prošao moderaciju',
                'body'    => 'U oglasu koji ste predali nije navedena količina/površina koju oglašavate. Što više detalja o proizvodu (cijena, količina, dimenzije, perfomanse, slike, video....) ponudite, to će veću pažnju privući vaš oglas. Dodavanje fotografija/videa se NE naplaćuje. Imate li dodatnih pitanja, nazovite naš odjel Korisničke podrške na tel. 01/6102-885 radnim danom 8-17h ili pošaljite upit na e-mail <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a>.'
            ),
            'ok-nedozvoljena-fotografija-uz-besplatni-oglas' => array(
                'title'   => '(default) + Nedozvoljena fotografija uz besplatni oglas',
                'subject' => 'Oglasnik.hr obavijest - Vaš oglas je prošao moderaciju',
                'body'    => 'Fotografija uz oglas koji ste predali ne može biti objavljena uz besplatan oglas. Logo, web-domene, naziv tvrtke i ostali slični podaci dio su plaćenog oglasa. Molimo, obratite se našem odjelu Korisničke podrške na tel. 01/6102-885 radnim danom 8-17h ili pošaljite upit e-mail <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a>.'
            )
        );

        $data[self::MODERATION_STATUS_NOK] = array(
            'nok-dupli-oglas' => array(
                'title'   => 'Dupli oglas',
                'subject' => 'Oglasnik.hr obavijest - Vaš oglas NIJE prošao moderaciju',
                'body'    => 'Trenutno imate identičan oglas u objavi, kako bi izbjegli ponavljanje istog oglasa, Vaš oglas neće biti objavljen ponovno. Imate li dodatnih pitanja nazovite naš odjel Podrške na tel. 01/6102-885 radnim danom 8-17h ili pošaljite upit na e-mail <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a>.'
            ),
            'nok-kriva-rubrika-nedovoljno-podataka' => array(
                'title'   => 'Kriva rubrika/Nedovoljno podataka',
                'subject' => 'Oglasnik.hr obavijest - Vaš oglas NIJE prošao moderaciju',
                'body'    => 'Odabrali ste krivu rubriku ili vaš oglas nema sve potrebne informacije i ispunjena polja. Molimo vas da ponovo predate oglas u odgovarajuću rubriku, te ispunite sva obavezna polja. Imate li dodatnih pitanja, nazovite naš odjel Podrške na tel. 01/6102-885 radnim danom 8-17h ili pošaljite upit na e-mail <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a>.'
            ),
            'nok-mora-biti-placeni-oglas' => array(
                'title'   => 'Mora biti plaćeni oglas',
                'subject' => 'Oglasnik.hr obavijest - Vaš oglas NIJE prošao moderaciju',
                'body'    => 'Oglasi u kojima se oglašava usluga, ponuda zaposlenja, turistička ponuda, veleprodaja, proizvodnja, odvozi, otkupi, prodaja na kredite, kartice i čekove i slično, oglašava web stranica tvrtke, spadaju u kategoriju plaćenih oglasa. Za detalje o mogućnostima oglašavanja u tiskanom ili internetskom izdanju Oglasnika nazovite naš odjel Podrške na tel. 01/6102-885 radnim danom 8-17h ili pošaljite upit na e-mail <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a>.'
            ),
            'nok-nepotpuni-kontakt-podaci' => array(
                'title'   => 'Nepotpuni kontakt podaci',
                'subject' => 'Oglasnik.hr obavijest - Vaš oglas NIJE prošao moderaciju',
                'body'    => 'U oglasu kojeg želite objaviti upisani su nepotpuni kontakt podaci. Molimo, predajte oglas ponovno s ispravljenim podacima. Imate li dodatnih pitanja nazovite naš odjel Podrške na tel. 01/6102-885 radnim danom 8-17h ili pošaljite upit na e-mail <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a>.'
            ),
            'nok-neprimjereni-tekst-oglasa' => array(
                'title'   => 'Neprimjereni tekst oglasa',
                'subject' => 'Oglasnik.hr obavijest - Vaš oglas NIJE prošao moderaciju',
                'body'    => 'Poštovani, tekst oglasa nije primjeren za objavu. Provjerite da li je sve napisano kako ste željeli objaviti i da li je odabrana dobra rubrika. Imate li dodatnih pitanja, nazovite naš odjel Podrške na tel. 01/6102-885 radnim danom 8-17h ili pošaljite upit na e-mail <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a>.'
            ),
            'nok-oglasavanje-lijekova-organa-netrpeljivi-oglasi' => array(
                'title'   => 'Oglašavanje lijekova, organa, netrpeljivi oglasi',
                'subject' => 'Oglasnik.hr obavijest - Vaš oglas NIJE prošao moderaciju',
                'body'    => 'Poštovani, oglase u kojima se oglašava prodaja lijekova, organa, poziva na uplate na privatne račune građana, narušava ljudsko dostojanstvo, naglašava rasna, seksualna ili etnička diskriminacija, te vjerska ili politička netrpeljivost Oglasnik može odbiti za objavu.'
            ),
            'nok-neuspjesna-provjera-kontakta-oglasivaca' => array(
                'title'   => 'Neuspješna provjera kontakta oglašivača',
                'subject' => 'Oglasnik.hr obavijest - Vaš oglas NIJE prošao moderaciju',
                'body'    => 'Oglasnik zadržava pravo provjere kontakt podataka iz oglasa, a prije objavljivanja u tiskanom ili online izdanju Oglasnika. Vaš oglas nismo uspjeli provjeriti. Molimo da se javite našem odjelu Korisničke podrške na tel. 01/6102-885 radnim danom 8-17 h, kako bi smo aktivirali oglas za objavu.'
            ),
            'nok-nedovoljno-kvalitetna-fotografija-za-tisak' => array(
                'title'   => 'Nedovoljno kvalitetna fotografija za tisak',
                'subject' => 'Oglasnik.hr obavijest - Vaš oglas NIJE prošao moderaciju',
                'body'    => 'Fotografija u oglasu kojeg želite objaviti, nažalost, nije dovoljno kvalitetna za objavljivanje u tisku. Molimo, obratite se našem odjelu Korisničke podrške na tel. 01/6102-885 radnim danom 8-17h ili pošaljite upit na e-mail <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a>.'
            ),
            'nok-fotografija-ne-odgovara-tekstu-oglasa' => array(
                'title'   => 'Fotografija ne odgovara tekstu oglasa',
                'subject' => 'Oglasnik.hr obavijest - Vaš oglas NIJE prošao moderaciju',
                'body'    => 'Poštovani, fotografija koju ste pridodali oglasu ne odgovara tekstu oglasa. Imate li dodatnih pitanja, nazovite odjel Korisničke podrške na tel. 01/6102-885 radnim danom 8-17h ili pošaljite upit na e-mail <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a>.'
            ),
            'nok-nedozvoljena-fotografija-uz-besplatni-oglas' => array(
                'title'   => 'Nedozvoljena fotografija uz besplatni oglas',
                'subject' => 'Oglasnik.hr obavijest - Vaš oglas NIJE prošao moderaciju',
                'body'    => 'Fotografija uz oglas koji ste predali ne može biti objavljena uz besplatan oglas. Logo, web-domene, naziv tvrtke i ostali slični podaci dio su plaćenog oglasa. Molimo, obratite se našem odjelu Korisničke podrške na tel. 01/6102-885 radnim danom 8-17h ili pošaljite upit e-mail <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a>.'
            ),
            'nok-oglasivac-odustao-od-oglasavanja' => array(
                'title'   => 'Oglašivač odustao od oglašavanja',
                'subject' => 'Oglasnik.hr obavijest - Vaš oglas NIJE prošao moderaciju',
                'body'    => 'Na Vaš zahtjev, izbrisali smo vaš oglas. Imate li dodatnih pitanja, nazovite odjel Korisničke podrške na tel. 01/6102-885 radnim danom 8-17h ili pošaljite upit na e-mail <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a>.'
            ),
            'nok-oglas-za-tisak-mora-biti-komercijalnog-tipa' => array(
                'title'   => 'Oglas za tisak mora biti komercijalnog tipa',
                'subject' => 'Oglasnik.hr obavijest - Vaš oglas NIJE prošao moderaciju',
                'body'    => 'Oglasi u kojima se oglašava usluga, veleprodaja, proizvodnja i slično; oglasi u kojima se oglašava internetska stranica tvrtke, ne mogu se oglasiti kao besplatni mali oglasi. Za tiskano izdanje potrebno je odabrati jedan od modela istaknutih plaćenih oglasa. Za detalje o mogućnostima oglašavanja u tiskanom i internetskom izdanju Oglasnika, nazovite odjel Korisničke podrške na tel. 01/6102-885 radnim danom 8-17h ili pošaljite upit na e-mail <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a>.'
            ),
            'nok-predati-ponovno-razdvojeno' => array(
                'title'   => 'Predati ponovno razdvojeno',
                'subject' => 'Oglasnik.hr obavijest - Vaš oglas NIJE prošao moderaciju',
                'body'    => 'U predanom oglasu naveli ste više proizvoda, molimo, predajte zasebne oglase. Što više detalja o proizvodu (cijena, količina, dimenzije, perfomanse, slike, video....) ponudite, to će veću pažnju privući vaš oglas. Oglasu možete besplatno pridružiti do 20 fotografija. Imate li dodatnih pitanja nazovite naš odjel Korisničke podrške na tel. 01/6102-885 radnim danom 8-17h ili pošaljite upit na e-mail <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a>.'
            )
        );

        // Allow returing only a subset of data if specifed
        if ('all' !== $status && isset($data[$status])) {
            $data = $data[$status];
        }

        return $data;
    }

    public function getModerationMessage($status, $reason = null)
    {
        if (!$reason) {
            $reason = 'ok-oglas-prosao-moderaciju';
        }
        $data = $this->getModerationReasonsData($status);
        $message = isset($data[$reason]) ? $data[$reason] : null;
        return $message;
    }

    public function sendModerationMessage($status, $reason = null)
    {
        $message = $this->getModerationMessage($status, $reason);
        if ($message) {
            $email = new Email();
            $email->prepare(
                $message['subject'],
                $this->ad->getUser()->email,
                'ad-moderation-' . $status,
                array(
                    'title' => $message['subject'],
                    'ad' => $this->ad,
                    'ad_products' => $this->ad->getProducts(),
                    'moderation_reason' => $message['body']
                )
            );

            return $email->Send();
        }

        return false;
    }

    /**
     * Sets moderation status and reason
     *
     * @param \Phalcon\Http\RequestInterface $request
     * @return bool Moderation status has been changed?
     */
    public function setModerationStatusAndReason($request)
    {
        // detect initial ad's moderation status
        $initial_moderation_status = (string)$this->ad->moderation . (string)$this->ad->moderation_reason;
        $moderation_status = $request->getPost('moderation', 'string');
        $this->ad->moderation = $moderation_status;
        switch ($moderation_status) {
            case Moderation::MODERATION_STATUS_OK:
                $moderation_reason = trim($request->getPost('moderation_reason_ok', 'string', 'ok-oglas-prosao-moderaciju'));
                $this->ad->moderation_reason = $moderation_reason;
                break;
            case Moderation::MODERATION_STATUS_NOK:
                $this->ad->moderation_reason = trim($request->getPost('moderation_reason_nok', 'string'));
                break;
            default:
                $this->ad->moderation_reason = null;
        }

        return $initial_moderation_status != ((string)$this->ad->moderation . (string)$this->ad->moderation_reason);
    }

}
