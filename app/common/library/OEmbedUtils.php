<?php

namespace Baseapp\Library;

class OEmbedUtils
{
    private static function get_url($url)
    {
        $json = null;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $json_string = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        if (!empty($json_string) && $httpcode >= 200 && $httpcode < 300) {
            $json = json_decode($json_string);
        }

        return $json;
    }

    public static function parse_youtube_url($url, $return_html = true)
    {
        $youtube_url = null;

        if ($youtube_id = self::get_youtube_id_from_url($url)) {
            $youtube_url = 'http://www.youtube.com/embed/' . $youtube_id;
        } else {
            $oembed_url = 'http://www.youtube.com/oembed?url=' . rawurlencode($url) . '&format=json';
            if ($json = self::get_url($oembed_url)) {
                $youtube_url = $json->html;

                if (false === $return_html) {
                    $dom = Utils::html2dom($youtube_url);
                    $iframes = $dom->getElementsByTagName('iframe');
                    foreach ($iframes as $iframe) {
                        $src = trim($iframe->getAttribute('src'));
                        if ($src) {
                            $youtube_url = $src;
                        }
                    }
                }
            }
        }

        return $youtube_url;
    }

    protected static function get_youtube_id_from_url($url)
    {
        $youtube_id = null;

        preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);

        if (!empty($matches) && !empty($matches[1])) {
            $youtube_id = $matches[1];
        }

        return $youtube_id;
    }


    //http://vimeo.com/api/oembed.json?url=http%3A//vimeo.com/7100569
}

