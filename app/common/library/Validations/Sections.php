<?php

namespace Baseapp\Library\Validations;

class Sections
{

    protected $unique = array('url');

    public function __construct() {
        $this->validation = new \Baseapp\Extension\Validation();
        $this->setup();
    }

    public function setup()
    {
        $this->validation->add('name', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Name cannot be empty'
        )));

        $this->validation->add('url', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'URL cannot be empty'
        )));

        $this->validation->add('category_ids', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Select at least one category'
        )));

        $this->validation->setLabels(array(
            'name' => 'Name',
            'url' => 'URL',
            'category_ids' => 'Categories',
            'location' => 'Location'
        ));
    }

    public function add($field, $validator)
    {
        $this->validation->add($field, $validator);
    }

    public function set_unique($unique) {
        $this->unique = $unique;
    }

    public function get()
    {
        $this->add_uniques();
        return $this->validation;
    }

    private function add_uniques() {
        if (is_array($this->unique) && !empty($this->unique)) {
            foreach ($this->unique as $field_name) {
                $this->validation->add($field_name, new \Baseapp\Extension\Validator\Uniqueness(array(
                    'model' => '\Baseapp\Models\Sections',
                    'message' => ucfirst($field_name) . ' already exists'
                )));
            }
        }
    }

    public function validate($data)
    {
        return $this->get()->validate($data);
    }

    public function getMessages() {
        return $this->validation->getMessages();
    }

}
