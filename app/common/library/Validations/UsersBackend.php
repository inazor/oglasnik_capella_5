<?php

namespace Baseapp\Library\Validations;


class UsersBackend extends Users
{
    protected $unique = array('username', 'email');

    public function setup()
    {
        $this->validation->add('username', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Username cannot be empty'
        )));
        $this->validation->add('username', new \Phalcon\Validation\Validator\StringLength(array(
            'min' => 3,
            'max' => 24,
            'messageMaximum' => 'Username cannot be longer than 24 chars',
            'messageMinimum' => 'Username cannot be shorter than 3 chars'
        )));
        $this->validation->add('username', new \Phalcon\Validation\Validator\Regex(array(
            'pattern' => self::USERNAME_REGEX,
            'message' => 'Username can contain only letters, numbers, - (dash) and _ (underscore) and should begin with a letter'
        )));

        $this->validation->add('email', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Email cannot be empty'
        )));
        $this->validation->add('email', new \Phalcon\Validation\Validator\Email(array(
            'message' => 'Email incorrect'
        )));
        $this->validation->setLabels(array(
            'username' => 'Username',
            'email' => 'E-mail',
        ));
    }

    public function set_unique($unique) {
        $this->unique = $unique;
    }

    public function get()
    {
        $this->add_uniques();
        return $this->validation;
    }

    private function add_uniques() {
        if (is_array($this->unique) && !empty($this->unique)) {
            foreach ($this->unique as $field_name) {
                $this->validation->add($field_name, new \Baseapp\Extension\Validator\Uniqueness(array(
                    'model' => self::MODEL,
                    'message' => ucfirst($field_name) . ' already exists'
                )));
            }
        }
    }
}
