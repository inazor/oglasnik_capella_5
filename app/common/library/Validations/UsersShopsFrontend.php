<?php

namespace Baseapp\Library\Validations;


class UsersShopsFrontend extends UsersShops
{
    public function setup()
    {
        $this->validation->add('title', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Naziv mora biti popunjen'
        )));
        $this->validation->add('slug', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Slug mora biti popunjen'
        )));
        $this->validation->add('slug', new \Phalcon\Validation\Validator\Regex(array(
            'pattern' => '/^([a-zA-Z0-9-]){3,64}$/',
            'message' => 'Minimalno 3, maksimalno 64 znakova, bez razmaka (dozvoljena samo crtica "-")'
        )));
        $this->validation->add('about', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Kratki tekst o trgovini mora biti popunjen'
        )));
        $this->validation->setLabels(array(
            'title' => 'Naziv',
            'slug' => 'Slug',
            'about' => 'O nama'
        ));
    }

    public function validate_if_set($fields = array())
    {
        if (isset($fields['email'])) {
            $this->validation->add('email', new \Phalcon\Validation\Validator\Email(array(
                'message' => 'Neispravna e-mail adresa'
            )));
        }
    }

    protected function add_uniques() {
        if (is_array($this->unique) && !empty($this->unique)) {
            foreach ($this->unique as $field_name) {
                $this->validation->add($field_name, new \Baseapp\Extension\Validator\Uniqueness(array(
                    'model' => self::MODEL,
                    'message' => ucfirst($field_name) . ' već postoji'
                )));
            }
        }
    }
}
