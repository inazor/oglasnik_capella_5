<?php

namespace Baseapp\Library\Validations;


class UsersFrontend extends Users
{
    public function setup()
    {
        $this->validation->add('username', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Molimo upišite željeno korisničko ime'
        )));
        $this->validation->add('username', new \Baseapp\Extension\Validator\Uniqueness(array(
            'model' => '\Baseapp\Models\Users',
            'message' => 'Nažalost, željeno korisničko ime je zauzeto'
        )));
        $this->validation->add('username', new \Phalcon\Validation\Validator\StringLength(array(
            'min' => 3,
            'max' => 24,
            'messageMaximum' => 'Molimo upišite korisničko ime kraće od 24 znaka',
            'messageMinimum' => 'Molimo upišite korisničko ime duže od 3 znaka'
        )));
        $this->validation->add('username', new \Phalcon\Validation\Validator\Regex(array(
            'pattern' => self::USERNAME_REGEX,
            'message' => 'Korisničko ime smije sadržavati samo slova, brojke, - (minus) i _ (donju crtu) te mora početi slovom'
        )));
        $this->validation->add('password', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Molimo upišite željenu lozinku'
        )));
        $this->validation->add('password', new \Phalcon\Validation\Validator\StringLength(array(
            'min'            => 6,
            'messageMinimum' => 'Lozinka je prekratka, mora imati barem 6 znakova'
        )));
        $this->validation->add('repeatPassword', new \Phalcon\Validation\Validator\Confirmation(array(
            'with' => 'password',
            'message' => '"Ponovljena lozinka" ne odgovara onoj u polju "Lozinka"'
        )));
        $this->validation->add('first_name', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Ime je neophodno polje'
        )));
        $this->validation->add('last_name', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Prezime je neophodno polje'
        )));
        $this->validation->add('email', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Molimo upišite vašu e-mail adresu'
        )));
        $this->validation->add('email', new \Phalcon\Validation\Validator\Email(array(
            'message' => 'Neispravna e-mail adresa'
        )));
        $this->validation->add('email', new \Baseapp\Extension\Validator\Uniqueness(array(
            'model' => '\Baseapp\Models\Users',
            'message' => 'Već postoji korisnički račun s ovom e-mail adresom'
        )));

        $this->validation->setLabels(array(
            'username' => 'Korisničko ime',
            'password' => 'Lozinka',
            'repeatPassword' => 'Ponovljena lozinka',
            'first_name' => 'Ime',
            'last_name' => 'Prezime',
            'email' => 'E-mail adresa',
            'repeatEmail' => 'Ponovljena email adresa'
        ));
    }
}
