<?php

namespace Baseapp\Library\Validations;

use Phalcon\Validation\Validator\PresenceOf;

class UsersShopsFeaturedBackend extends UsersShopsFeatured
{
    public function setup()
    {
        $this->validation->add('categories', new PresenceOf(array(
            'message' => 'You have to choose at least one category'
        )));
    }

    public function validate_if_set($field = null)
    {
        switch ($field) {
            case 'ads':
                $this->validation->add('ads', new PresenceOf(array(
                    'message' => 'You have to choose at least one ad'
                )));
                break;

            case 'intro':
                $this->validation->add('intro', new PresenceOf(array(
                    'message' => 'Intro text cannot be empty'
                )));
                break;
        }
    }

}
