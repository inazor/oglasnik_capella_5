<?php

namespace Baseapp\Library\Validations;

use Baseapp\Extension\Validation;

abstract class Users
{
    const MODEL = '\Baseapp\Models\Users';
    const USERNAME_REGEX = '/^[a-zA-Z]([a-zA-Z0-9-_]){1,22}[a-zA-Z0-9]$/';

    public function __construct() {
        $this->validation = new Validation();
        $this->setup();
    }

    // validation implementations in inherited/concrete classes should reside here
    abstract public function setup();

    /**
     * @return \Baseapp\Extension\Validation
     */
    public function get()
    {
        return $this->validation;
    }

    /**
     * @return \Phalcon\Validation\Message\Group
     */
    public function getMessages()
    {
        return $this->get()->getMessages();
    }

    /**
     * @param $data
     *
     * @return \Phalcon\Validation\Message\Group
     */
    public function validate($data)
    {
        return $this->get()->validate($data);
    }
}
