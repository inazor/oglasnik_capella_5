<?php

namespace Baseapp\Library\Validations;


class InfractionReports
{
    public function __construct() {
        $this->validation = new \Baseapp\Extension\Validation();
        $this->setup();
    }

    public function setup()
    {
        $this->validation->add('report_reason', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Odaberite razlog prijave'
        )));

        $this->validation->setLabels(array(
            'report_reason' => 'Razlog prijave'
        ));
    }

    public function add($field, $validator)
    {
        $this->validation->add($field, $validator);
    }

    public function get()
    {
        return $this->validation;
    }

    public function validate($data)
    {
        return $this->get()->validate($data);
    }

    public function getMessages() {
        return $this->validation->getMessages();
    }

}
