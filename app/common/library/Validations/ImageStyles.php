<?php

namespace Baseapp\Library\Validations;

use \Phalcon\Validation\Validator\PresenceOf;
use \Baseapp\Extension\Validator\Uniqueness as UniquenessValidator;

class ImageStyles
{
    protected $unique = array();

    public function __construct() {
        $this->validation = new \Baseapp\Extension\Validation();
        $this->setup();
    }

    public function setup()
    {
        $this->validation->add('slug', new PresenceOf(array(
            'message' => 'Slug cannot be empty'
        )));
        $this->validation->add('width', new PresenceOf(array(
            'message' => 'Width must be defined'
        )));
        $this->validation->add('height', new PresenceOf(array(
            'message' => 'Height must be defined'
        )));

        $this->validation->setLabels(array(
            'slug' => 'Slug',
            'width' => 'Width',
            'height' => 'Height',
        ));
    }

    public function add($field, $validator)
    {
        $this->validation->add($field, $validator);
    }

    public function set_unique($unique) {
        $this->unique = $unique;
    }

    public function get()
    {
        $this->add_uniques();
        return $this->validation;
    }

    private function add_uniques() {
        if (is_array($this->unique) && !empty($this->unique)) {
            foreach ($this->unique as $field_name => $options) {
                $defaults = array(
                    'model' => '\Baseapp\Models\ImageStyles',
                    'message' => ucfirst($field_name) . ' already exists'
                );

                if (is_array($options) && !empty($options)) {
                    $options = array_merge($defaults, $options);
                } else {
                    $options = $defaults;
                }

                $this->validation->add($field_name, new UniquenessValidator($options));
            }
        }
    }

    public function validate($data)
    {
        return $this->get()->validate($data);
    }

    public function getMessages() {
        return $this->validation->getMessages();
    }

}

