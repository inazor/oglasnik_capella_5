<?php

namespace Baseapp\Library\Validations;

use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex;

class UsersShopsBackend extends UsersShops
{
    public function setup()
    {
        $this->validation->add('user_id', new PresenceOf(array(
            'message' => 'User cannot be empty'
        )));
        $this->validation->add('title', new PresenceOf(array(
            'message' => 'Title cannot be empty'
        )));
        $this->validation->add('slug', new PresenceOf(array(
            'message' => 'Slug cannot be empty'
        )));
        $this->validation->add('slug', new Regex(array(
            'pattern' => '/^([a-zA-Z0-9-]){3,64}$/',
            'message' => 'Between 3 and 64 chars with no spaces (only dash is alowed "-")'
        )));
        $this->validation->add('about', new PresenceOf(array(
            'message' => 'This field cannot be empty'
        )));
        $this->validation->setLabels(array(
            'title' => 'Title',
            'slug' => 'Slug',
            'about' => 'About'
        ));
    }

    public function validate_if_set($fields = array())
    {
        if (in_array('email', $fields)) {
            $this->validation->add('email', new \Phalcon\Validation\Validator\Email(array(
                'message' => 'Email incorrect'
            )));
        }
    }

    protected function add_uniques() {
        if (is_array($this->unique) && !empty($this->unique)) {
            foreach ($this->unique as $field_name) {
                $this->validation->add($field_name, new \Baseapp\Extension\Validator\Uniqueness(array(
                    'model' => self::MODEL,
                    'message' => ucfirst($field_name) . ' already exists'
                )));
            }
        }
    }
}
