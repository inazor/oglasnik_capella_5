<?php

namespace Baseapp\Library\Validations;

use Baseapp\Extension\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\PresenceOf;

class ContactForm
{
    public function __construct() {
        $this->validation = new Validation();
        $this->setup();
    }

    public function setup()
    {
        $this->validation->add('name', new PresenceOf(array(
            'message' => 'Molimo popunite svoje ime i prezime'
        )));

        $this->validation->add('email', new PresenceOf(array(
            'message' => 'Molimo popunite svoju e-mail adresu'
        )));

        $this->validation->add('email', new EmailValidator(array(
            'message' => 'Neispravna e-mail adresa'
        )));

        $this->validation->add('message', new PresenceOf(array(
            'message' => 'Molimo popunite tekst poruke'
        )));

        $this->validation->setLabels(array(
            'name'    => 'Ime i prezime',
            'email'   => 'E-mail adresa',
            'message' => 'Poruka',
        ));
    }

    public function add($field, $validator)
    {
        $this->validation->add($field, $validator);
    }

    public function get()
    {
        return $this->validation;
    }

    public function validate($data)
    {
        return $this->get()->validate($data);
    }

    public function getMessages() {
        return $this->validation->getMessages();
    }
}
