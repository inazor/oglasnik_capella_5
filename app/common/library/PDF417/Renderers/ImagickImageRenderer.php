<?php

namespace Baseapp\Library\PDF417\Renderers;

use Baseapp\Library\Images\Color;
use Baseapp\Library\Images\Size;
use Baseapp\Library\PDF417\BarcodeData;

class ImagickImageRenderer extends AbstractRenderer
{
    // Supported image formats and corresponding MIME types
    protected $formats = [
        'jpg' => 'image/jpeg',
        'png' => 'image/png',
        'gif' => 'image/gif',
    ];

    protected $options = [
        'format'  => 'png',
        'quality' => 90,
        'scale'   => 3,
        'ratio'   => 3,
        'padding' => 20,
        'color'   => '#000000',
        'bgColor' => '#ffffff',
    ];

    /**
     * {@inheritdoc}
     */
    public function validateOptions()
    {
        $errors = [];
        $format = $this->options['format'];

        if (!isset($this->formats[$format])) {
            $formats = implode(', ', array_keys($this->formats));
            $errors[] = 'Invalid option `format`: ' . $format . '. Expected one of: ' . $formats;
        }

        $scale = $this->options['scale'];
        if (!is_numeric($scale) || $scale < 1 || $scale > 20) {
            $errors[] = 'Invalid option `scale`: ' . $scale . '. Expected an integer between 1 and 20.';
        }

        $ratio = $this->options['ratio'];
        if (!is_numeric($ratio) || $ratio < 1 || $ratio > 10) {
            $errors[] = 'Invalid option `ratio`: ' . $ratio . '. Expected an integer between 1 and 10.';
        }

        $padding = $this->options['padding'];
        if (!is_numeric($padding) || $padding < 0 || $padding > 50) {
            $errors[] = 'Invalid option `padding`: ' . $padding . '. Expected an integer between 0 and 50.';
        }

        $quality = $this->options['quality'];
        if (!is_numeric($quality) || $quality < 0 || $quality > 100) {
            $errors[] = 'Invalid option `quality`: ' . $quality . '. Expected an integer between 0 and 100.';
        }

        // Check colors by trying to parse them
        $color = $this->options['color'];
        $bgColor = $this->options['bgColor'];
        $gdColor = new Color();
        try {
            $gdColor->parse($color);
        } catch (\Exception $ex) {
            $errors[] = 'Invalid option `color`: '. $color . '. Supported color formats: "#000000", "rgb(0,0,0)", or "rgba(0,0,0,0)"';
        }
        try {
            $gdColor->parse($bgColor);
        } catch (\Exception $ex) {
            $errors[] = 'Invalid option `bgColor`: ' . $bgColor . '. Supported color formats: "#000000", "rgb(0,0,0)", or "rgba(0,0,0,0)"';
        }

        return $errors;
    }

    /**
     * {@inheritdoc}
     */
    public function getContentType()
    {
        $format = $this->options['format'];

        return $this->formats[$format];
    }

    /**
     * {@inheritdoc}
     */
    public function render(BarcodeData $data)
    {
        $pixelGrid = $data->getPixelGrid();
        $height    = count($pixelGrid);
        $width     = count($pixelGrid[0]);

        // Extract options
        $bgColor = $this->options['bgColor'];
        $color   = $this->options['color'];
        $format  = $this->options['format'];
        $padding = $this->options['padding'];
        $quality = $this->options['quality'];
        $ratio   = $this->options['ratio'];
        $scale   = $this->options['scale'];

        // Create a new image
        $image_obj = new \Imagick();
        $color_obj = new Color($color);
        $pixel = $color_obj->getPixel();

        $bg_color_obj = new Color($bgColor);

        $image_obj->newImage($width, $height, $bg_color_obj->getPixel(), $format);
        foreach ($pixelGrid as $y => $row) {
            foreach ($row as $x => $value) {
                if ($value) {
                    $draw = new \ImagickDraw();
                    $draw->setFillColor($pixel);
                    $draw->point($x, $y);
                    $image_obj->drawImage($draw);
                }
            }
        }

        // Apply scaling & aspect ratio
        $width  *= $scale;
        $height *= $scale * $ratio;

        // $image_obj->resizeImage($width, $height, \Imagick::FILTER_LANCZOS, 1, false);
        $image_obj->scaleImage($width, $height, false);

        // Add padding
        $width += 2 * $padding;
        $height += 2 * $padding;

        $image_obj = $this->resizeCanvas($image_obj, $width, $height, 'center', false);

        // $img->resizeCanvas($width, $height, 'center', false, $bgColor);
        $image_obj->setFormat($format);
        $image_obj->setImageFormat($format);
        $compression = \IMagick::COMPRESSION_UNDEFINED;
        $format      = strtolower($image_obj->getImageFormat());

        if ('png' === $format) {
            $compression = \Imagick::COMPRESSION_ZIP;
            $image_obj->setInterlaceScheme(\IMagick::INTERLACE_PLANE);
        } elseif ('jpeg' === $format) {
            $compression = \Imagick::COMPRESSION_JPEG;
            $image_obj->setInterlaceScheme(\IMagick::INTERLACE_LINE);
        }

        $image_obj->setCompression($compression);
        $image_obj->setImageCompression($compression);

        $image_obj->setCompressionQuality($quality);
        $image_obj->setImageCompressionQuality($quality);

        // Strip data
        $image_obj->stripImage();

        return $image_obj->getImagesBlob();
    }

    /**
     * @param \Imagick $im
     * @param null $width
     * @param null $height
     * @param string $anchor
     * @param bool $relative
     * @param string $bgcolor
     *
     * @return \Imagick
     */
    protected function resizeCanvas(\Imagick $im, $width = null, $height = null, $anchor = 'center', $relative = false, $bgcolor = 'none')
    {
        $original_width = $im->getImageWidth();
        $original_height = $im->getImageHeight();

        // Check if only width or height is set
        $width = is_null($width) ? $original_width : intval($width);
        $height = is_null($height) ? $original_height : intval($height);

        // Check if relative width/height
        if ($relative) {
            $width = $original_width + $width;
            $height = $original_height + $height;
        }

        // Check for negative width/height
        $width = ($width <= 0) ? $width + $original_width : $width;
        $height = ($height <= 0) ? $height + $original_height : $height;

        $canvas = new \Imagick();
        $canvas->newImage($width, $height, $bgcolor);

        // set copy position
        $canvas_size = (new Size($width, $height))->align($anchor);
        $image_size = (new Size($original_width, $original_height))->align($anchor);
        $canvas_pos = $image_size->relativePosition($canvas_size);
        $image_pos = $canvas_size->relativePosition($image_size);

        if ($width <= $original_width) {
            $dst_x = 0;
            $src_x = $canvas_pos->x;
            $src_w = $canvas_size->width;
        } else {
            $dst_x = $image_pos->x;
            $src_x = 0;
            $src_w = $original_width;
        }
        if ($height <= $original_height) {
            $dst_y = 0;
            $src_y = $canvas_pos->y;
            $src_h = $canvas_size->height;
        } else {
            $dst_y = $image_pos->y;
            $src_y = 0;
            $src_h = $original_height;
        }

        // Make image area transparent to keep transparency even if background-color is set
        $pixel = $canvas->getImagePixelColor(0, 0);
        $color = new Color($pixel);
        $fill = $color->getHex();
        $fill = $fill == '#ff0000' ? '#00ff00' : '#ff0000';

        $rect = new \ImagickDraw();
        $rect->setFillColor($fill);
        $rect->rectangle($dst_x, $dst_y, $dst_x + $src_w - 1, $dst_y + $src_h - 1);
        $canvas->drawImage($rect);
        $canvas->transparentPaintImage($fill, 0, 0, false);

        // Crop original to calculated dimensions
        // $im->cropImage($src_w, $src_h, $src_x, $src_y);

        // Copy image into new canvas
        $canvas->compositeImage($im, \Imagick::COMPOSITE_DEFAULT, $dst_x, $dst_y);
        $canvas->setImagePage(0, 0, 0, 0);

        // Set same format as original
        $canvas->setImageFormat($im->getImageFormat());

        // Replace existing image with the newly created one
        // $im->setImageExtent($canvas->getImageWidth(), $canvas->getImageHeight());
        $im->setImage($canvas);

        return $im;
    }
}
