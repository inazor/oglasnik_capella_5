<?php

namespace Baseapp\Library;
use Baseapp\Bootstrap;

/**
 * Tool Library
 *
 * @package base-app
 * @category Library
 * @version 2.0
 */
class Tool
{

    /**
     * Minify css and js collection
     *
     * @return void
     */
    public static function assetsMinification()
    {
        $di = \Phalcon\Di::getDefault();
        $config = $di->getShared('config');

        $manager = $di->getShared('assets');

        foreach (array('Css', 'Js') as $asset_type) {
            $get = 'get' . $asset_type;
            $filter = '\Phalcon\Assets\Filters\\' . $asset_type . 'min';

            foreach ($manager->$get() as $resource) {
                $min = new $filter();
                $resource->setSourcePath(ROOT_PATH . '/public/' . $resource->getPath());
                $resource->setTargetUri('min/' . $resource->getPath());

                // TODO: http://docs.phalconphp.com/en/latest/reference/assets.html

                if ('production' !== $config->app->env) {
                    // Create non-existing directories
                    if (!is_dir(dirname(ROOT_PATH . '/public/min/' . $resource->getPath()))) {
                        $old = umask(0);
                        mkdir(dirname(ROOT_PATH . '/public/min/' . $resource->getPath()), 0777, true);
                        umask($old);
                    }

                    if ('development' === $config->app->env || !file_exists(ROOT_PATH . '/public/min/' . $resource->getPath())) {
                        file_put_contents(ROOT_PATH . '/public/min/' . $resource->getPath(), $min->filter($resource->getContent()));
                    } elseif (sha1($min->filter($resource->getContent())) != sha1_file(ROOT_PATH . '/public/min/' . $resource->getPath())) {
                        file_put_contents(ROOT_PATH . '/public/min/' . $resource->getPath(), $min->filter($resource->getContent()));
                    }
                }
            }
        }
    }

    /**
     * Replace CamelCase and under_scores to spaces
     *
     * @param string $str string to replace to human readable
     * @param string $char default spacer
     *
     * @return string
     */
    public static function label($str, $char = ' ')
    {
        $str = preg_replace('/(?<=\\w)(?=[A-Z])/', $char . "$1", $str);
        return $char === ' ' ? ucfirst(trim(str_replace('_', ' ', strtolower($str)))) : $str;
    }

    /**
     * @param string|null $lang 'en' or 'hr' for specified language data, or null for the whole array (default)
     *
     * @return array|mixed
     */
    public static function getPaginationLanguageData($lang = null)
    {
        $lang_array = array(
            'en' => array(
                'showing'        => 'Showing',
                'of'             => 'of',
                'first'          => '<span class="fa fa-angle-double-left"></span> First',
                'previous'       => '<span class="fa fa-angle-left fa-fw"></span>',
                'previous_title' => 'Previous',
                'next'           => '<span class="fa fa-angle-right fa-fw"></span>',
                'next_title'     => 'Next',
                'last'           => 'Last <span class="fa fa-angle-double-right"></span>'
            ),
            'hr' => array(
                'showing'        => 'Prikaz',
                'of'             => 'od',
                'first'          => '<span class="fa fa-angle-double-left fa-fw"></span>',
                'previous'       => '<span class="fa fa-angle-left fa-fw"></span>',
                'previous_title' => 'Prethodna',
                'next'           => '<span class="fa fa-angle-right fa-fw"></span>',
                'next_title'     => 'Sljedeća',
                'last'           => '<span class="fa fa-angle-double-right fa-fw"></span>'
            ),
        );

        if (null !== $lang && !isset($lang_array[$lang])) {
            throw new \InvalidArgumentException('Unknown pagination language specified');
        }

        if (null === $lang) {
            // Return entire array
            return $lang_array;
        } else {
            // Return only the specified language
            return $lang_array[$lang];
        }
    }

    /**
     * @param string|null $url
     *
     * @return array
     */
    public static function preparePaginationVariables($url = null)
    {
        $di = \Phalcon\Di::getDefault();

        // Detect URL
        $query = $di->getShared('request')->getQuery();
        $url = $url ? $url : substr($query['_url'], 1);
        unset($query['_url']);

        // If 'sort' parameter is present in the current url and it matches
        // the default value, do not propagate the sort since it creates an extra
        // url with the same results (similar to ?page=1 requests)
        $sort_default = $di->get('config')->adSorting->default_value;
        if (isset($query['sort']) && $query['sort'] === $sort_default) {
            unset($query['sort']);
        }

        return array(
            'query' => $query,
            'url'   => $url
        );
    }

    /**
     * Prepare HTML pagination for frontend purposes. In Croatian.
     *
     * @param object $pagination Phalcon Paginator object
     * @param mixed $url URL with pagination
     * @param string $class CSS class to adding to div
     * @param int $countOut Number of page links in the begin and end of whole range
     * @param int $countIn Number of page links on each side of current page
     * @param string $lang 'hr' or 'en', defaults to 'hr'
     * @return  string
     */
    public static function pagination($pagination, $url = null, $class = 'pagination', $countOut = 0, $countIn = 2, $lang = 'hr')
    {
        // Don't even bother if there's nothing to paginate
        if ($pagination->total_pages < 2) {
            return;
        }

        $lang_array = self::getPaginationLanguageData($lang);

        // Beginning group of pages: $n1...$n2
        $n1 = 1;
        $n2 = min($countOut, $pagination->total_pages);

        // Ending group of pages: $n7...$n8
        $n7 = max(1, $pagination->total_pages - $countOut + 1);
        $n8 = $pagination->total_pages;

        // Middle group of pages: $n4...$n5
        $n4 = max($n2 + 1, $pagination->current - $countIn);
        $n5 = min($n7 - 1, $pagination->current + $countIn);
        $useMiddle = ($n5 >= $n4);

        // build mobile pages
        $mobile_countIn = 1;
        $mobile_links = array();
        if ($mobile_countIn > 0) {
            $m1 = max(1, $pagination->current - $mobile_countIn);
            $m2 = min($n5, $pagination->current + $mobile_countIn);

            for($i = $m1; $i <= $m2; $i++) {
                $mobile_links[$i] = $i;
            }
        } else {
            $mobile_links[$pagination->current] = $pagination->current;
        }

        // Point $n3 between $n2 and $n4
        $n3 = (int) (($n2 + $n4) / 2);
        $useN3 = ($useMiddle && (($n4 - $n2) > 1));

        // Point $n6 between $n5 and $n7
        $n6 = (int) (($n5 + $n7) / 2);
        $useN6 = ($useMiddle && (($n7 - $n5) > 1));

        // Links to display as array(page => content)
        $links = array();

        // Generate links data in accordance with calculated numbers
        for ($i = $n1; $i <= $n2; $i++) {
            $links[$i] = $i;
        }

        if ($useN3) {
            $links[$n3] = '&hellip;';
        }

        for ($i = $n4; $i <= $n5; $i++) {
            $links[$i] = $i;
        }

        if ($useN6) {
            $links[$n6] = '&hellip;';
        }

        for ($i = $n7; $i <= $n8; $i++) {
            $links[$i] = $i;
        }

        $di = \Phalcon\Di::getDefault();
        $edge_items_class = 'edge';

        // Prepare some variables for later use
        $tag   = $di->getShared('tag');
        $vars  = self::preparePaginationVariables($url);
        $url   = $vars['url'];
        $query = $vars['query'];

        // Prepare list
        $html  = '<nav>';
        $html .= '<ul class="' . $class . '">';

        // Prepare Previous button
        if ('' !== trim($lang_array['previous'])) {
            if ($pagination->current > $pagination->before) {
                if (1 != $pagination->before) {
                    $query['page'] = $pagination->before;
                } else {
                    unset($query['page']);
                }
                $html .= '<li data-previous class="' . $edge_items_class . '" data-page="' . $pagination->before . '">' . $tag->linkTo(array($url, 'query' => $query, 'title' => $lang_array['previous_title'], $lang_array['previous'])) . '</li>';
            } else {
                $html .= '<li data-previous class="' . $edge_items_class . ' disabled"><span>' . $lang_array['previous'] . '</span></li>';
            }
        }

        // Prepare Pages
        $paginations = array();
        $items_cnt = 0;
        foreach ($links as $number => $content) {
            $mobile_hidden = !in_array($number, $mobile_links);

            $item_class = array();
            if ($mobile_hidden) {
                $item_class[] = 'hidden-sm';
                $item_class[] = 'hidden-xs';
            }

            $items_cnt++;
            $data_page_attr = ' data-pages data-page="' . $number . '"';
            if ((int) $number === (int) $pagination->current) {
                $item_class[] = 'active';
                $paginations[] = '<li' . $data_page_attr . ' class="' . implode(' ', $item_class) . '"><span>' . $content . '</span></li>';
            } else {
                if (1 != $number) {
                    $query['page'] = $number;
                } else {
                    unset($query['page']);
                }
                if ($content == '&hellip;') {
                    $item_class[] = 'disabled';
                }
                $paginations[] = '<li' .$data_page_attr . (count($item_class) ? ' class="' . implode(' ', $item_class) . '"' : '') . '>' . $tag->linkTo(array($url, 'query' => $query, $content)) . '</li>';
            }
        }

        $html .= implode('', $paginations);

        // Prepare Next button
        if ('' !== trim($lang_array['next'])) {
            if ($pagination->current < $pagination->next) {
                $query['page'] = $pagination->next;
                $html .= '<li data-next class="' . $edge_items_class . '" data-page="' . $pagination->next . '">' . $tag->linkTo(array($url, 'query' => $query, 'title' => $lang_array['next_title'], $lang_array['next'], 'data-page' => $pagination->next)) . '</li>';
            } else {
                $html .= '<li data-next class="' . $edge_items_class . ' disabled"><span>' . $lang_array['next'] . '</span></li>';
            }
        }

        // Close list
        $html .= '</ul>';
        $html .= '</nav>';

        return $html;
    }

    /**
     * Build HTML markup for backend pagination purposes. It's always in english (for now at least),
     * and it always shows additional info about which pages are currently being shown.
     *
     * First Previous 1 2 3 ... 22 23 24 25 26 [27] 28 29 30 31 32 ... 48 49 50 Next Last
     *
     * @param object $pagination Phalcon Paginator object
     * @param mixed $url URL with pagination
     * @param string $class CSS class to adding to div
     * @param int $countOut Number of page links in the begin and end of whole range
     * @param int $countIn Number of page links on each side of current page
     *
     * @return  string
     */
    public static function paginationAdmin($pagination, $url = null, $class = 'pagination', $countOut = 0, $countIn = 2)
    {
        // Don't even bother if there's nothing to paginate
        if ($pagination->total_pages < 2) {
            return;
        }

        $lang_array = self::getPaginationLanguageData('en');

        // Beginning group of pages: $n1...$n2
        $n1 = 1;
        $n2 = min($countOut, $pagination->total_pages);

        // Ending group of pages: $n7...$n8
        $n7 = max(1, $pagination->total_pages - $countOut + 1);
        $n8 = $pagination->total_pages;

        // Middle group of pages: $n4...$n5
        $n4 = max($n2 + 1, $pagination->current - $countIn);
        $n5 = min($n7 - 1, $pagination->current + $countIn);
        $useMiddle = ($n5 >= $n4);

        // build mobile pages
        $mobile_countIn = 1;
        $mobile_links = array();
        if ($mobile_countIn > 0) {
            $m1 = max(1, $pagination->current - $mobile_countIn);
            $m2 = min($n5, $pagination->current + $mobile_countIn);

            for($i = $m1; $i <= $m2; $i++) {
                $mobile_links[$i] = $i;
            }
        } else {
            $mobile_links[$pagination->current] = $pagination->current;
        }

        // Point $n3 between $n2 and $n4
        $n3 = (int) (($n2 + $n4) / 2);
        $useN3 = ($useMiddle && (($n4 - $n2) > 1));

        // Point $n6 between $n5 and $n7
        $n6 = (int) (($n5 + $n7) / 2);
        $useN6 = ($useMiddle && (($n7 - $n5) > 1));

        // Links to display as array(page => content)
        $links = array();

        // Generate links data in accordance with calculated numbers
        for ($i = $n1; $i <= $n2; $i++) {
            $links[$i] = $i;
        }

        if ($useN3) {
            $links[$n3] = '&hellip;';
        }

        for ($i = $n4; $i <= $n5; $i++) {
            $links[$i] = $i;
        }

        if ($useN6) {
            $links[$n6] = '&hellip;';
        }

        for ($i = $n7; $i <= $n8; $i++) {
            $links[$i] = $i;
        }

        $di = \Phalcon\Di::getDefault();
        $edge_items_class = 'edge';

        // Prepare some variables for later use
        $tag   = $di->getShared('tag');
        $vars  = self::preparePaginationVariables($url);
        $url   = $vars['url'];
        $query = $vars['query'];

        // Prepare list
        $html  = '<nav>';
        $html .= '<ul class="' . $class . '">';

        // Build info about the current pagination set, if specified
        $per_page = count($pagination->items);
        $total    = $pagination->total_items;
        $end      = $pagination->current * $per_page;
        $start    = ($end - $per_page) + 1;
        $end      = ($end > $total) ? $total : $end;
        // Fix last page start/end since $per_page does not really
        // match actually limited number of items per page (unless the full total evenly divides)
        if ($pagination->current == $pagination->last) {
            $end   = $total;
            $start = $total - $per_page;
        }

        $html .= '<li class="disabled pagination-info hidden-sm hidden-xs"><span>';
        $html .= $lang_array['showing'].' ' . $start . '-' . $end . ' ' . $lang_array['of'] . ' <span class="total-items">' . $pagination->total_items . '</span>';
        $html .= '</span></li>';

        // Prepare First button
        if ('' !== trim($lang_array['first'])) {
            if ($pagination->current != $pagination->first) {
                unset($query['page']);
                $html .= '<li data-first class="' . $edge_items_class . '" data-page="' . $pagination->first . '">' . $tag->linkTo(array($url, 'query' => $query, $lang_array['first'])) . '</li>';
            } else {
                $html .= '<li data-first class="' . $edge_items_class . ' disabled"><span>' . $lang_array['first'] . '</span></li>';
            }
        }

        // Prepare Previous button
        if ('' !== trim($lang_array['previous'])) {
            if ($pagination->current > $pagination->before) {
                if (1 != $pagination->before) {
                    $query['page'] = $pagination->before;
                } else {
                    unset($query['page']);
                }
                $html .= '<li data-previous class="' . $edge_items_class . '" data-page="' . $pagination->before . '">' . $tag->linkTo(array($url, 'query' => $query, 'title' => $lang_array['previous_title'], $lang_array['previous'])) . '</li>';
            } else {
                $html .= '<li data-previous class="' . $edge_items_class . ' disabled"><span>' . $lang_array['previous'] . '</span></li>';
            }
        }

        // Prepare Pages
        $paginations = array();
        $items_cnt = 0;
        foreach ($links as $number => $content) {
            $mobile_hidden = !in_array($number, $mobile_links);

            $item_class = array();
            if ($mobile_hidden) {
                $item_class[] = 'hidden-sm';
                $item_class[] = 'hidden-xs';
            }

            $items_cnt++;
            $data_page_attr = ' data-pages data-page="' . $number . '"';
            if ((int) $number === (int) $pagination->current) {
                $item_class[] = 'active';
                $paginations[] = '<li' . $data_page_attr . ' class="' . implode(' ', $item_class) . '"><span>' . $content . '</span></li>';
            } else {
                if (1 != $number) {
                    $query['page'] = $number;
                } else {
                    unset($query['page']);
                }
                if ($content == '&hellip;') {
                    $item_class[] = 'disabled';
                }
                $paginations[] = '<li' .$data_page_attr . (count($item_class) ? ' class="' . implode(' ', $item_class) . '"' : '') . '>' . $tag->linkTo(array($url, 'query' => $query, $content)) . '</li>';
            }
        }

        $html .= implode('', $paginations);

        // Prepare Next button
        if ('' !== trim($lang_array['next'])) {
            if ($pagination->current < $pagination->next) {
                $query['page'] = $pagination->next;
                $html .= '<li data-next class="' . $edge_items_class . '" data-page="' . $pagination->next . '">' . $tag->linkTo(array($url, 'query' => $query, 'title' => $lang_array['next_title'], $lang_array['next'], 'data-page' => $pagination->next)) . '</li>';
            } else {
                $html .= '<li data-next class="' . $edge_items_class . ' disabled"><span>' . $lang_array['next'] . '</span></li>';
            }
        }

        // Prepare Last button
        if ('' !== trim($lang_array['last'])) {
            if ($pagination->current != $pagination->last) {
                $query['page'] = $pagination->last;
                $html .= '<li data-last class="' . $edge_items_class . '" data-page="' . $pagination->last . '">' . $tag->linkTo(array($url, 'query' => $query, $lang_array['last'])) . '</li>';
            } else {
                $html .= '<li data-last class="' . $edge_items_class . ' disabled"><span>' . $lang_array['last'] . '</span></li>';
            }
        }

        // Close list
        $html .= '</ul>';
        $html .= '</nav>';

        return $html;
    }

    /**
     * Register the Volt engines
     *
     * @param object $view Phalcon\Mvc\View
     * @param object $di Dependency Injection
     *
     * @return array array of template engines
     */
    public static function registerEngines($view, $di)
    {
        $config = $di->getShared('config');

        $volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);
        $volt->setOptions(array(
            'stat' => true,
            'compileAlways' => ('development' === $config->app->env || 'testing' === $config->app->env) ? true : false,
            'compiledPath' => function($templatePath) {
                list($junk, $path) = explode(ROOT_PATH, $templatePath);
                $dir = dirname($path);
                $file = basename($path, '.volt');
                $cache_dir = ROOT_PATH . '/cache/volt' . $dir;

                if (!is_dir($cache_dir)) {
                    $old = umask(0);
                    mkdir($cache_dir, 0777, true);
                    umask($old);
                }
                return $cache_dir . '/' . $file . '.phtml';
            }
        ));

        $compiler = $volt->getCompiler();
        $compiler->addExtension(new \Baseapp\Extension\VoltStaticFunctions());
        $compiler->addExtension(new \Baseapp\Extension\VoltPHPFunctions());

        return array(
            // Try to load .phtml file from ViewsDir first,
            '.phtml' => 'Phalcon\Mvc\View\Engine\Php',
            '.volt' => $volt,
        );
    }

    public static function getFromWeb($url, $format = 'json')
    {
        $downloadedFile      = null;
        $format              = trim(mb_strtolower($format));
        $acceptedFileFormats = array('json', 'html', 'text');

        if (in_array($format, $acceptedFileFormats)) {
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $downloadedString = curl_exec($curl);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);
            if (!empty($downloadedString) && $httpcode >= 200 && $httpcode < 300) {
                $downloadedFile = $downloadedString;

                if ('json' === $format) {
                    $downloadedFile = json_decode($downloadedFile);
                }
            }
        }

        return $downloadedFile;
    }
}
