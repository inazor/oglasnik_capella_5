<?php

namespace Baseapp\Library;

use Phalcon\Cache\Exception as PhCacheException;
use Phalcon\Mvc\User\Component;
use Baseapp\Bootstrap;
use Baseapp\Extension\Cache\Backend\MyMemcache;

class StatsFetcher extends Component
{
    const URL = 'http://www.posao.hr/files/download/Oglasnik.json';

    protected $caching = true;
    protected $lifetime = 300;
    protected $prefix  = 'home-stats';

    public function __construct()
    {
    }

    /**
     * @return bool|mixed|null
     */
    protected function getWithCaching()
    {
        $result = null;
        $do_slow_operation = false;

        // Try getting from memcached first
        $hash = md5(self::URL);
        $key  = $this->prefix . '-' . $hash;
        $result_code = null;
        $memcached = $this->getMemcache();

        // Determine when it's safe to actually go get the data (taking care of preventing cache stampedes
        // when the cache is actually being used)
        if ($memcached && $this->caching) {
            try {
                // When caching is used, things get somewhat more complex...

                $result = $memcached->getWithLock($key, $this->lifetime);
                $result_code = $memcached->getResultCode();

                /**
                 * N.B:
                 * When a lock is not acquired, we get back a NOT_FOUND result in $result_code,
                 * which means someone else should be building the data currently (no guarantees though)...
                 * And so we're kind of stuck here with having to choose one of:
                 * - show nothing since we got nothing (bad UX for visitors maybe, maybe not)
                 * - do slow operation to get data and hope it's not that slow and not many other process end up in the same state
                 * - retry from the cache several times and then decide which of the above two options to choose again
                 */

                if (!$result) {
                    // Need to check we're the ones that should be doing the expensive query/data
                    $do_slow_operation = (MyMemcache::GENERATE_DATA == $result_code);

                    // If we're not, let's try fetching from the cache again with a few retries until someone
                    // else finishes caching the data hopefully...
                    if (!$do_slow_operation && $memcached->lockExists($key)) {
                        // TODO/FIXME: abstract this away
                        $maxBackoffSeconds = 5;
                        $maxRetries        = 3;
                        $attemptCnt        = 0;
                        while ($attemptCnt < $maxRetries) {
                            $attemptCnt++;
                            $result = $memcached->get($key, $this->lifetime);
                            if ($result) {
                                $do_slow_operation = false;
                                break;
                            }
                            // Randomize the delay somewhat so not all clients wait for the exact same amount of time,
                            // while still utilizing this ghetto version of the truncated exponential backoff algorithm
                            $randMS          = mt_rand(1, 1000);
                            $randS           = $randMS / 1000;
                            $waitSeconds     = pow(2, $attemptCnt - 1) + $randS;
                            $waitTimeSeconds = min($waitSeconds, $maxBackoffSeconds);
                            $waitTimeUSec    = $waitTimeSeconds * 1000000;
                            // Sleep for the calculated amount of time (never longer than $maxBackoffSeconds)
                            // Bootstrap::log('Attempt ' . $attemptCnt . ' failed, sleeping for ' . $waitTimeSeconds . ' seconds...');
                            usleep($waitTimeUSec);
                        }

                        // TODO/FIXME:
                        // If after several attempts above we still have nothing, and the lock still
                        // exists, what do we do?
                        if (!$result && $memcached->lockExists($key)) {
                            $do_slow_operation = true;
                            Bootstrap::log('Reached Memcached max_retries without results and lock still exists, cache key: ' . $key);
                        }
                    }
                }
            } catch (PhCacheException $e) {
                $do_slow_operation = true;
                Bootstrap::log($e);
            }
        } else {
            // When no cache is used, we have no choice really
            $do_slow_operation = true;
        }

        // If we didn't get stuff back from cache and we've been cleared to get
        // the data from DB (lock was set, build and store for later)
        if ($do_slow_operation) {
            $result = $this->getRemoteData();
        }

        // If we've done the slow op, we probably need to cache whatever we got back.
        // Or else we'll go get it from the slow source again in the future.
        // Another VERY important thing is we need to delete the lock even if we did the slow op,
        // otherwise others will keep waiting for it until it expires, and it's slowing down other requests needlessly.
        if ($do_slow_operation && $memcached && $this->caching) {
            try {
                // Whatever results we've gotten back from slow source should probably be cached too.
                // TODO/FIXME: investigate impact of saving $result only if its trueish...
                //if ($result) {
                $memcached->save($key, $result, $this->lifetime);
                //}
            } catch (PhCacheException $e) {
                Bootstrap::log($e);
            }
            // Always deleting the lock that was acquired since we've done the slow op, and even in case
            // the save failed, if we don't delete the lock, other threads cannot attempt their own slow op and save at all...
            $memcached->deleteLock($key);
        }

        return $result;
    }

    /**
     * @return bool|mixed|null
     */
    protected function getData()
    {
        if ($this->caching) {
            $data = $this->getWithCaching();
        } else {
            $data = $this->getRemoteData();
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getHomepageBoxes()
    {
        return $this->transformRemoteData($this->getData());
    }

    /**
     * Static helper to simplify code when using it from controllers
     *
     * @return array
     */
    public static function getHomepageStatsBoxes()
    {
        return (new self())->getHomepageBoxes();
    }

    /**
     * Transforms string/JSON data taken from self::URL which currently looks like this:
     * {
     *   box1_slike: "icn_korsnika_white.png",
     *   box1_broj: 330.204,
     *   box1_text: "Ukupno korisnika oglasnik.hr",
     *   box2_slike: "icn_posjetitelja_white.png",
     *   box2_broj: 725.029,
     *   box2_text: "Mjesečno posjetitelja",
     *   box3_slike: "icn_verificiranih_prodavaca_white.png",
     *   box3_broj: 4.842,
     *   box3_text: "Verificiranih prodavatelja",
     *   box4_slike: "icn_pogleda_white.png",
     *   box4_text: "Dnevno pogledanih oglasa"
     *   box4_broj: 28.252,
     * }
     * The above is `json_decode()`-ed automatically in Tool::getFromWeb() into a \stdClass instance.
     *
     * It is then transformed by this method into something more usable like this:
     *
     * $boxes = array(
     *     array(
     *         'icon' => 'filename.svg',
     *         'num' => '1234',
     *         'text' => 'Stat description'
     *     ),
     *     array(
     *         'icon' => 'filename.svg',
     *         'num' => '1234',
     *         'text' => 'Stat description'
     *     )
     * );
     *
     * @param \stdClass
     *
     * @return array
     */
    protected function transformRemoteData(\stdClass $data = null)
    {
        $boxes = array();

        if (!empty($data)) {
            for ($i = 1; $i <= 4; $i++) {
                $mapping = array(
                    'box' . $i . '_slike' => 'icon',
                    'box' . $i . '_broj'  => 'num',
                    'box' . $i . '_text'  => 'text'
                );
                foreach ($mapping as $key_name_remote => $key_name_local) {
                    if (isset($data->$key_name_remote)) {
                        $boxes[($i-1)][$key_name_local] = $data->$key_name_remote;
                    }
                }
            }
        }

        return $boxes;
    }

    /**
     * @return mixed|null
     */
    protected function getRemoteData()
    {
        return Tool::getFromWeb(self::URL, 'json');
    }

    /**
     * @return MyMemcache|null
     */
    private function getMemcache()
    {
        $cache = null;

        if ($this->getDI()->has('memcache')) {
            try {
                /* @var $cache \Baseapp\Extension\Cache\Backend\MyMemcache */
                $cache = $this->getDI()->getShared('memcache');
            } catch (PhCacheException $e) {
                Bootstrap::log($e);
            }
        }

        return $cache;
    }
}
