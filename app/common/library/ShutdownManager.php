<?php

namespace Baseapp\Library;

/**
 * ShutdownManager class is a wrapper for registering multiple callbacks/shutdown functions with
 * the ability to prevent their execution if/when needed.
 *
 * Shutdown functions will not be executed if the process is killed with a SIGTERM or SIGKILL signal.
 * So, if you ever need to avoid shutdown-registerd functions, use: posix_kill(posix_getpid(), SIGTERM);
 */
class ShutdownManager
{

    protected $callbacks = array();
    protected $enabled = true;
    protected $registered = false;

    public function __construct($auto_register = true)
    {
        if ($auto_register) {
            $this->register();
        }
    }

    public function register()
    {
        if (!$this->registered) {
            register_shutdown_function(array($this, 'shutdown'));
        }
    }

    public function enabled($value = null)
    {
        if (null !== $value) {
            $this->enabled = $value;
        }

        return $this->enabled;
    }

    public function disable()
    {
        return $this->enabled(false);
    }

    public function clear()
    {
        $this->callbacks = array();
    }

    public function append($callback, $param = null)
    {
        $callback = func_get_args();

        if (empty($callback)) {
            throw new \InvalidArgumentException('No callback specified');
        }

        if (!is_callable($callback[0])) {
            throw new \InvalidArgumentException('Invalid callback passed to ' . __METHOD__);
        }

        $this->callbacks[] = $this->parse_args($callback);
    }

    public function prepend($callback, $param = null)
    {
        $callback = func_get_args();

        if (empty($callback)) {
            throw new \InvalidArgumentException('No callback specified');
        }

        if (!is_callable($callback[0])) {
            throw new \InvalidArgumentException('Invalid callback passed to ' . __METHOD__);
        }

        array_unshift($this->callbacks, $this->parse_args($callback));
    }

    public function parse_args($args)
    {
        return array($args[0], array_slice($args, 1));
    }

    public function shutdown()
    {
        if (!$this->enabled()) {
            return;
        }

        foreach ($this->callbacks as $arguments) {
            $callback = array_shift($arguments);
            call_user_func_array($callback, $arguments);
        }
    }

}
