<?php

namespace Baseapp\Library;

use Phalcon\Cache\Exception as PhCacheException;
use Phalcon\Mvc\User\Component;
use Baseapp\Bootstrap;
use Baseapp\Extension\Cache\Backend\MyMemcache;

class PosaoHR extends Component
{
    protected $jsonURL       = 'http://www.posao.hr/affiliate/oglasnik.json';
    protected $error         = false;
    protected $json          = null;
    protected $classifieds   = null;
    protected $cacheResults  = true;
    protected $cacheLifeTime = 300;
    protected $cachePrefix   = 'posaoHR';

    public function __construct($jsonURL = null, $cacheResults = null)
    {
        $this->jsonURL($jsonURL);
        $this->cacheResults($cacheResults);
    }

    /**
     * @return MyMemcache|null
     */
    private function getMemcache()
    {
        $cache = null;

        if ($this->getDI()->has('memcache')) {
            try {
                /* @var $cache \Baseapp\Extension\Cache\Backend\MyMemcache */
                $cache = $this->getDI()->getShared('memcache');
            } catch (PhCacheException $e) {
                Bootstrap::log($e);
            }
        }

        return $cache;
    }


    public function jsonURL($jsonURL = null)
    {
        if (null !== $jsonURL) {
            $this->jsonURL = trim($jsonURL);
        }

        return $this->jsonURL;
    }

    public function cacheResults($cache = null)
    {
        if (null !== $cache) {
            $this->cacheResults = (bool) $cache;
        }

        return $this->cacheResults;
    }

    public function cacheLifeTime($lifeTime = null)
    {
        if (null !== $lifeTime) {
            $this->cacheLifeTime = (int) $lifeTime;
        }

        return $this->cacheLifeTime;
    }

    public function cachePrefix($prefix = null)
    {
        if (null !== $prefix && trim($prefix)) {
            $this->cachePrefix = trim($prefix);
        }

        return $this->cachePrefix;
    }

    public function hasError()
    {
        return $this->error;
    }

    public function get($property)
    {
        $result = null;

        if ('footer' == $property) {
            $result = $this->getFooter();
        } elseif (isset($this->json->$property)) {
            $result = $this->json->$property;
        }

        if ($result) {
            $result = $this->handlePlaceholders($result);
        }

        return $result;
    }

    private function parsePlaceholders($text)
    {
        // Example
        //
        // Thank you for your action. In order to continue you have to press {{jsonKey}}.
        // If you choose to cancel, please click on {{jsonKey2}} button.

        preg_match_all("/(\{\{([^\}]+)\}\})/uU", $text, $matches, PREG_PATTERN_ORDER);

        if (!isset($matches[2])) {
            $matches = null;
        }

        $placeholders = array();
        $properties   = array();
        $placeholderCount = count($matches[2]);

        for ($i = 0; $i < $placeholderCount; $i++) {
            $placeholders[] = trim($matches[0][$i]);
            $properties[]   = trim($matches[2][$i]);
        }

        $matches          = null;
        $placeholderCount = null;
        unset($matches, $placeholderCount);

        if (count($placeholders)) {
            return array(
                'placeholders' => $placeholders,
                'properties'   => $properties
            );
        }

        return null;
    }

    private function handlePlaceholders($text)
    {
        if ($parsed = $this->parsePlaceholders($text)) {
            for ($i = 0; $i < count($parsed['placeholders']); $i++) {
                $propertyValue = isset($this->json->$parsed['properties'][$i]) ? trim($this->json->$parsed['properties'][$i]) : null;

                if ($propertyValue) {
                    $text = str_replace($parsed['placeholders'][$i], $propertyValue, $text);
                }
            }
        }

        return $text;
    }

    private function getFooter()
    {
        $footer = null;

        if (isset($this->json->footer_text) && trim($this->json->footer_text)) {
            if (isset($this->json->footer_link) && trim($this->json->footer_link)) {
                $footer = '<a href="' . trim($this->json->footer_link) . '" rel="external" class="learn-more">' . trim($this->json->footer_text) . '</a>';
            } else {
                $footer = trim($this->json->footer_text);
            }
        }

        return $footer;
    }

    private function getCachedJson()
    {
        $json = null;

        // Try getting from memcached first
        $hash            = md5($this->jsonURL());
        $cacheKey        = $this->cachePrefix() . '-' . $hash;
        $memcached       = $this->getMemcache();
        $memcachedResult = null;
        if ($memcached) {
            try {
                $json    = $memcached->getWithLock($cacheKey, $this->cacheLifeTime());
                $memcachedResult = $memcached->getResultCode();
            } catch (PhCacheException $e) {
                Bootstrap::log($e);
            }
        }

        // Determine when it's safe to actually go get the data (taking care of preventing cache stampedes
        // when the cache is actually being used)
        $getFromWeb = false;
        if (!$memcached) {
            // When no cache is used, we have no choice really
            $getFromWeb = true;
        } else {
            // When caching is used, things get somewhat more complex...

            /**
             * N.B:
             * When a lock is not acquired, we get back a NOT_FOUND result in $memcached_result,
             * which means someone else should be building the data currently (no guarantees though)...
             * And so we're kind of stuck here with having to choose one of:
             * - show nothing since we got nothing (bad UX for visitors maybe, maybe not)
             * - get non-cached data from db and pray it's not that slow and not many other process end up in the same sate
             * - retry from the cache several times and then decide which of the above two options to choose again
             */

            if (!$json || empty($json)) {
                // Need to check we're the ones that should be doing the expensive query/data
                $getFromWeb = (MyMemcache::GENERATE_DATA == $memcachedResult);
                // If we're not, let's try fetching from the cache again with a few retries until someone
                // else finishes caching the data hopefully...
                if (!$getFromWeb && $memcached->lockExists($cacheKey)) {
                    // TODO/FIXME: abstract this away
                    $maxBackoffSeconds = 5;
                    $maxRetries = 3;
                    $attemptCnt = 0;
                    while ($attemptCnt < $maxRetries) {
                        $attemptCnt++;
                        $json = $memcached->get($cacheKey, $this->cacheLifeTime());
                        if ($json) {
                            break;
                        }
                        // Randomize the delay somewhat so not all clients wait for the exact same amount of time,
                        // while still utilizing this ghetto version of the truncated exponential backoff algorithm
                        $randMS          = mt_rand(1, 1000);
                        $randS           = $randMS / 1000;
                        $waitSeconds     = pow(2, $attemptCnt - 1) + $randS;
                        $waitTimeSeconds = min($waitSeconds, $maxBackoffSeconds);
                        $waitTimeUSec    = $waitTimeSeconds * 1000000;
                        // Sleep for the calculated amount of time (never longer than $max_backoff_seconds)
                        usleep($waitTimeUSec);
                    }

                    // TODO/FIXME:
                    // If after several attempts above we still have nothing, and the lock still
                    // exists, what do we do?
                    if (!$json && $memcached->lockExists($cacheKey)) {
                        Bootstrap::log('Reached max_retries without results on fetching PosaoHR data and lock still exists, cache key: ' . $cacheKey);
                    }
                }
            }
        }

        // If we've been cleared to get the data from DB (lock was set), fetch and store for later
        // $getFromWeb = true;
        if ($getFromWeb) {
            $json = $this->fetchJson();
            // Cache for later if we have stuff to cache and the cache service is available
            if (!empty($json) && $memcached) {
                // Cache save can still fail for all sorts of reasons...
                // The bigger potential problem when that happens is that the lock is not released, and others will
                // still wait and try to get from the cache several times, but they get back nothing, since
                // we haven't saved it :/
                try {
                    $memcached->save($cacheKey, $json, $this->cacheLifeTime());
                } catch (PhCacheException $e) {
                    Bootstrap::log($e);
                }
                // Always delete the lock, even when cache save fails, that way other threads might get a chance
                // to try the same thing on their own...
                $memcached->deleteLock($cacheKey);
            }
        }

        return $json;
    }

    /** Fetch JSON file from web */
    private function fetchJson()
    {
        return Tool::getFromWeb($this->jsonURL(), 'json');
    }

    private function setupJson($json)
    {
        $this->json        = $json;
        $this->classifieds = null;
        $this->error       = false;

        if ($json) {
            $this->error       = isset($json->ads) && count($json->ads) ? false : error;
            $this->classifieds = isset($json->ads) ? $json->ads : null;
        }
    }

    public function getClassifieds()
    {
        if ($this->cacheResults()) {
            $json = $this->getCachedJson();
        } else {
            $json = $this->fetchJson();
        }

        $this->setupJson($json);

        return $this->classifieds;
    }
}
