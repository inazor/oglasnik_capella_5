<?php

namespace Baseapp\Library\Assets;

use Phalcon\Assets\Resource;
use Baseapp\Library\Utils;

/**
 * Class VersionedPathAssetsManager appends a version string (given to the constructor
 * as an options key named `version`) after the `assets/` part of the path (for local resources only!).
 *
 * @package Baseapp\Library\Assets
 */
class VersionedPathAssetsManager extends \Phalcon\Assets\Manager
{
    private function getVersionedPathPart()
    {
        $config = \Phalcon\Di::getDefault()->get('config');

        $versionedPathPart = $this->_options['version'];
        if ('development' === $config->app->env) {
            $versionedPathPart = sha1(Utils::getRequestTime());
        }

        return $versionedPathPart;
    }

    public function addCss($path, $local = true, $filter = true, $attributes = null)
    {
        if ($local) {
            $path = str_replace('assets/', 'assets/' . $this->getVersionedPathPart() . '/', $path);
        }

        $this->addResourceByType('css', new Resource\Css($path, $local, $filter, $attributes));
        return $this;
    }

    public function addJs($path, $local = true, $filter = true, $attributes = null)
    {
        if ($local) {
            $path = str_replace('assets/', 'assets/' . $this->getVersionedPathPart() . '/', $path);
        }

        $this->addResourceByType('js', new Resource\Js($path, $local, $filter, $attributes));
        return $this;
    }
}
