<?php

namespace Baseapp\Library\Assets;

use \Phalcon\Assets\Collection;
use \Phalcon\Assets\Filters\Cssmin;
use \Phalcon\Assets\Filters\Jsmin;
use \Phalcon\Assets\Filters\None;
use \Phalcon\Assets\Manager as PhManager;
use \Phalcon\Cache\Backend;
use \Phalcon\Config;
use \Phalcon\Di;
use \Phalcon\DiInterface;
use Baseapp\Extension\Tag;

/**
 * Customized Assets Manager.
 */
class Manager extends PhManager
{
    const
        /**
         * Style file name in url.
         */
        FILENAME_PATTERN_CSS = '%u.styles.css',

        /**
         * Javascript file name in url.
         */
        FILENAME_PATTERN_JS = '%u.scripts.js';

    const
        /**
         * Javascript default collection name.
         */
        DEFAULT_COLLECTION_JS = 'js',

        /**
         * CSS default collection name.
         */
        DEFAULT_COLLECTION_CSS = 'css';

    const
        /**
         * Generated path for files that will be merged and minified.
         */
        GENERATED_STORAGE_PATH = 'min/';

    /**
     * Application config.
     *
     * @var Config
     */
    protected $_config;

    /**
     * Cache.
     *
     * @var Backend
     */
    protected $_cache;

    /**
     * Inline <head> code.
     *
     * @var array
     */
    protected $_inline = [];

    public $nonFreeFiltersAvailable = true;

    /**
     * Initialize assets manager.
     *
     * @param DiInterface $di Dependency injection.
     * @param bool $prepare Prepare manager (install assets if in debug and create default collections).
     */
    public function __construct($di, $prepare = true)
    {
        if (null === $di) {
            $di = Di::getDefault();
        }

        $this->_config = $di->get('config');
        $this->_cache = $di->get('cache');

        $this->nonFreeFiltersAvailable = $this->nonFreeFiltersAvailable();

        // $this->useImplicitOutput(false);
/*
        if ($prepare) {
            $this->set(self::DEFAULT_COLLECTION_CSS, $this->getEmptyCssCollection());
            $this->set(self::DEFAULT_COLLECTION_JS, $this->getEmptyJsCollection());
        }
//*/
    }

    /**
     * Clear assets cache.
     *
     * @return void
     */
/*
    public function clear()
    {
        $location = $this->getLocation();
        $files = Utils::fsRecursiveGlob($location, '*'); // get all file names
        // iterate files
        foreach ($files as $file) {
            if (is_file($file)) {
                @unlink($file); // delete file
            }
        }
    }
//*/
    /**
     * Get empty JS collection.
     *
     * @return Collection
     */
    public function getEmptyJsCollection()
    {
        $collection = new Collection();

        // $remote = $this->_config->app->static_uri;
        $remote = false;
        if ($remote) {
            $collection->setPrefix($remote)->setLocal(false);
        }

        $join = !$this->_config->app->debug;
        return $collection->addFilter(new Jsmin())->join($join);
    }

    /**
     * Get empty CSS collection.
     *
     * @return Collection
     */
    public function getEmptyCssCollection()
    {
        $collection = new Collection();

        // $remote = $this->_config->app->static_uri;
        $remote = false;
        if ($remote) {
            $collection->setPrefix($remote)->setLocal(false);
        }

        $join = !$this->_config->app->debug;
        return $collection->addFilter(new Cssmin())->join($join);
    }

    /**
     * Add <head> inline code.
     *
     * @param string $name Identification
     * @param string $code Code to add to <head> tag
     *
     * @return $this
     */
    public function addInline($name, $code)
    {
        $this->_inline[$name] = $code;
        return $this;
    }

    /**
     * Remove inline code.
     *
     * @param string $name Identification
     *
     * @return $this
     */
    public function removeInline($name)
    {
        unset($this->_inline[$name]);
        return $this;
    }

    /**
     * Get <head> tag inline code.
     *
     * @return string
     */
    public function outputInline()
    {
        return implode("\n", $this->_inline);
    }

    public function nonFreeFiltersAvailable()
    {
        $test = new \Phalcon\Assets\Filters\Cssmin();
        try {
            $test->filter('.zytzagoo { color:#c0ffee; }');
            return true;
        } catch (\Phalcon\Assets\Exception $e) {
            return false;
        }
    }

    /**
     * Prints the HTML for JS resources.
     *
     * @param string $collectionName the name of the collection
     *
     * @return string
     **/
    public function outputJs($collectionName = self::DEFAULT_COLLECTION_JS)
    {
        // $remote = $this->_config->app->assets->get('remote');
        $remote = false;
        $collection = $this->collection($collectionName);

        if (!$remote && $collection->getJoin()) {
            // $local = $this->_config->app->assets->get('local');
            // $local = 'assets/';
            // $local = $this->_config->app->static_uri;
            $local = '';
            $lifetime = $this->_config->cacheFrontendData->options->get('lifetime', 0);

            // Add our filters if there aren't any added yet
            $filters = $collection->getFilters();
            if (empty($filters)) {
                // TODO: find a way to easily enable Jsmin() and Cssmin() filters on windows... or stop using windows :)
                if ($this->nonFreeFiltersAvailable) {
                    $collection->addFilter(new Jsmin());
                } else {
                    $collection->addFilter(new None());
                }
            }

            // TODO: try to create a cache key that'll force a cache bust when the
            // contents of the collection are changed or something... But again that
            // might need another caching layer in order to not bother checking the contents on every request?

            $filepath = $local . self::GENERATED_STORAGE_PATH . $filename = $filename =
                    $this->getCollectionFileName($collection, self::FILENAME_PATTERN_JS);

            $collection->setTargetPath($filepath)->setTargetUri($filepath);

            if ($this->_cache->exists($filename)) {
                return Tag::javascriptInclude($collection->getTargetUri());
            }

            $res = parent::outputJs($collectionName);
            $this->_cache->save($filename, true, $lifetime);

            return $res;
        }

        return parent::outputJs($collectionName);
    }

    /**
     * Prints the HTML for CSS resources.
     *
     * @param string $collectionName the name of the collection
     *
     * @return string
     **/
    public function outputCss($collectionName = self::DEFAULT_COLLECTION_CSS)
    {
        // $remote = $this->_config->app->assets->get('remote');
        $remote = false;
        $collection = $this->collection($collectionName);

        if (!$remote && $collection->getJoin()) {
            // $local = $this->_config->app->assets->get('local');
            // $local = 'assets/';
            // $local = $this->_config->app->static_uri;
            $local = '';
            $lifetime = $this->_config->cacheFrontendData->options->get('lifetime', 0);

            // Add our filters if there aren't any added yet
            $filters = $collection->getFilters();
            if (empty($filters)) {
                if ($this->nonFreeFiltersAvailable) {
                    $collection->addFilter(new Cssmin());
                } else {
                    $collection->addFilter(new None());
                }
            }

            // TODO: try to create a cache key that'll force a cache bust when the
            // contents of the collection are changed or something... But again that
            // might need another caching layer in order to not bother checking the contents on every request?

            $filepath = $local . self::GENERATED_STORAGE_PATH . $filename = $filename =
                    $this->getCollectionFileName($collection, self::FILENAME_PATTERN_CSS);

            $collection->setTargetPath($filepath)->setTargetUri($filepath);

            if ($this->_cache->exists($filename)) {
                return Tag::stylesheetLink($collection->getTargetUri());
            }

            $res = parent::outputCss($collectionName);
            $this->_cache->save($filename, true, $lifetime);

            return $res;
        }

        return parent::outputCss($collectionName);
    }

    /**
     * Get filename for a collection based on a pattern.
     *
     * @param Collection $collection Assets collection
     * @param string $pattern Filename pattern
     *
     * @return string
     */
    public function getCollectionFileName(Collection $collection, $pattern)
    {
        return sprintf($pattern, crc32(serialize($collection)));
    }

    public function getCollectionCacheKey(Collection $collection)
    {
        $key = '';

        foreach ($collection as $resource) {
            $key .= $resource->getRealSourcePath();
            $key .= $resource->getTargetPath();
            // $key .= filemtime($resource->getRealSourcePath());
            $key .= sha1_file($resource->getRealSourcePath());
        }

        return md5($key);
    }

    /**
     * Get location according to params. If no params are specified returns the full path to assets directory.
     *
     * @param null|string $filename Filename (appended to assets path).
     *
     * @return string
     */
    protected function getLocation($filename = null)
    {
        $location = ROOT_PATH . '/public/assets';

        if (!$filename) {
            return $location;
        }

        return $location . '/' . $filename;
    }
}
