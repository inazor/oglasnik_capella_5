<?php

namespace Baseapp\Library;

class PartialFilenameFilter extends \RecursiveFilterIterator
{
    protected $pattern;
    protected $part;

    public function __construct($iterator, $part)
    {
        $this->part = $part;
        $this->pattern = '/(.*)' . preg_quote($this->part) . '($|\..+?$)/i';
        parent::__construct($iterator);
    }

    public function accept()
    {
        return $this->hasChildren() || ($this->current()->isFile() && preg_match($this->pattern, $this->current()));
    }

    public function getChildren()
    {
        return new self($this->getInnerIterator()->getChildren(), $this->part);
    }

    public function __toString()
    {
        return $this->current()->getFilename();
    }
}
