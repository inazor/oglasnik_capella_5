<?php

namespace Baseapp\Library\Uploader;

use Baseapp\Library\Utils;
use \Phalcon\Http\Request;

class Uploader
{
    /**
     * @var \Phalcon\Http\Request $request
     */
    private $request;

    /**
     * @var \Phalcon\Http\Request\File[] $files
     */
    private $files;

    private $fields;

    /**
     * Validation Rules
     *
     * @var array $rules
     */
    private $rules = [];

    /**
     * Uploaded files array
     *
     * @var array $info
     */
    private $info;

    /**
     * Validator
     *
     * @var Validator
     */
    private $validator;

    protected $storageOptions = [
        'basedir'             => '',
        'num_partitions'      => 2,
        'partition_length'    => 1,
        'create_missing_dirs' => false,
        'sanitize_filename'   => false,
        'remove_accents'      => false
    ];

    /**
     * Uploader constructor.
     *
     * @param array $rules
     * @param Validator|null $validator
     */
    public function __construct($rules = [], Validator $validator = null, $formFields = [])
    {
        if (!empty($rules)) {
            $this->setRules($rules);
        }

        if (null === $validator) {
            $validator = new Validator();
            $this->setValidator($validator);
        }

        $this->request = new Request();

        $this->setFormFields($formFields);
    }

    /**
     * Set form fields for uploaded files we will use
     *
     * @param array $formFields
     *
     * @return Uploader
     */
    public function setFormFields(array $formFields)
    {
        if (!empty($formFields)) {
            $uploadedFiles = $this->request->getUploadedFiles();
            $this->files = array();
            foreach ($uploadedFiles as $uploadedFile) {
                if (in_array($uploadedFile->getKey(), $formFields)) {
                    $this->files[] = $uploadedFile;
                }
            }
        } else {
            $this->files = $this->request->getUploadedFiles();
        }

        return $this;
    }

    /**
     * Set rules for uploaded files
     *
     * @param array $rules
     *
     * @return Uploader
     */
    public function setRules(array $rules)
    {
        foreach ($rules as $key => $values) {
            if ((is_array($values) && !empty($values)) || is_callable($values)) {
                $this->rules[$key] = $values;
            } else {
                $this->rules[$key] = trim($values);
            }
        }

        return $this;
    }

    public function setValidator(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Check if all uploaded files are valid
     *
     * @return bool
     */
    public function isValid()
    {
        if (count($this->files) > 0) {
            foreach ($this->files as $n => $file) {
                $core_ok = $this->validator->checkPhpErrors($file);
                // Don't even bother with other validation rules if a core php error was detected
                if (!$core_ok) {
                    continue;
                }

                // Apply all the validation rules for each file
                foreach ($this->rules as $key => $rule) {
                    $method = 'check' . ucfirst($key);
                    if (method_exists($this->validator, $method)) {
                        $ok = $this->validator->$method($file, $rule);
                        // Stop processing further rules as soon as one fails...
                        /*
                        if (!$ok) {
                            break;
                        }
                        */
                    }
                }
            }
        }

        $errors = $this->getErrors();

        return (bool) empty($errors);
    }

    /**
     * Return error messages
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->validator->errors;
    }

    /**
     * Get info about each uploaded file
     *
     * @return array
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Moves uploaded files to their defined location and returns
     * an array of details for each moved file.
     *
     * @return array
     */
    public function move()
    {
        foreach ($this->files as $n => $file) {
            /* @var Phalcon\Http\Request\File $file */
            $filename_original = $file->getName();

            $ext = strtolower($file->getExtension());
            // $ext = str_replace('jpeg', 'jpg', $ext);

            $base = rtrim($this->rules['directory'], '/\\');

            $filename = Utils::random_uuid() . '.' . $ext;
            $fullpath = $base . '/' . $filename;

            // Makes sure all the folders upto $fullpath exist
            $opts     = Utils::fqpn_options(array(
                'basedir'             => $base,
                'create_missing_dirs' => true
            ));
            $pathname = Utils::get_fqpn($fullpath, $opts);

            $root_pathname = str_replace(ROOT_PATH, '', $pathname);

            /**
             * Checking if destination already exists and modifying the filename if it does
             * so it doesn't collide with an existing file).
             * But this is should not really happen when we're using v4 uuids to name the files...
             * However... If it ever starts happening, we should be fine with just uncommenting this...
             */
            /*
            if (file_exists($pathname)) {
                $info            = pathinfo($pathname);
                $unique_filename = Utils::build_unique_filename($info['dirname'], $info['basename']);
                $pathname = $info['dirname'] . '/' . $unique_filename;
            }
            */

            // move file to target directory
            $uploaded = $file->moveTo($pathname);

            if ($uploaded) {
                $this->info[] = [
                    'path'      => $pathname,
                    'path_root' => $root_pathname,
                    'filename'  => $filename,
                    'filename_orig' => $filename_original,
                    'size'      => $file->getSize(),
                    'extension' => $ext,
                ];
            }
        }

        return $this->getInfo();
    }

    /**
     * Delete uploaded files
     */
    public function truncate()
    {
        if (!empty($this->info)) {
            foreach ($this->info as $n => $file) {
                if (file_exists($file['path'])) {
                    unlink($file['path']);
                }
            }
        }
    }
}
