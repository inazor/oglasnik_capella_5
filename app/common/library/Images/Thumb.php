<?php

namespace Baseapp\Library\Images;
use Baseapp\Library\Utils;

/**
 * Thumb encapsulates a "thumbnail" generated from a Media entity based
 * on a certain ImageStyle specification
 *
 * @package Baseapp\Library\Images
 */
class Thumb
{
    public $orientation = '';
    public $width = 0;
    public $height = 0;
    public $src = '';
    public $tag = '';
    public $style = array();
    public $class = '';
    public $alt = '';
    public $type = '';
    public $subtype = '';
    public $filename_orig = '';

    /* @var $size Size */
    public $size;

    protected $getters_map = array(
        'orientation'   => 'getOrientation',
        'width'         => 'getWidth',
        'height'        => 'getHeight',
        'src'           => 'getSrc',
        'tag'           => 'getTag',
        'style'         => 'getStyle',
        'size'          => 'getSize',
        'class'         => 'getClass',
        'alt'           => 'getAlt',
        'type'          => 'getType',
        'subtype'       => 'getSubtype',
        'filename_orig' => 'getFilenameOrig',
    );

    protected $setters_map = array(
        'orientation'   => 'setOrientation',
        'width'         => 'setWidth',
        'height'        => 'setHeight',
        'src'           => 'setSrc',
        'tag'           => 'setTag',
        'style'         => 'setStyle',
        'size'          => 'setSize',
        'class'         => 'setClass',
        'alt'           => 'setAlt',
        'type'          => 'setType',
        'subtype'       => 'setSubtype',
        'filename_orig' => 'setFilenameOrig',
    );

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        if (!empty($data)) {
            $this->setData($data);
        }
    }

    /**
     * @param array $data
     *
     * @return $this
     */
    public function setData(array $data = array())
    {
        if (!empty($data)) {
            foreach ($this->setters_map as $k => $setter) {
                if (isset($data[$k])) {
                    $this->$setter($data[$k]);
                }
            }
        }

        return $this;
    }

    /**
     * Returns available Thumb data as an array
     *
     * @param array|null $specific_fields Specify only certain fields/getters to be returned/called instead of everything specified in $this->getters_map
     *
     * @return array
     */
    public function getData(array $specific_fields = null)
    {
        $data = array();

        // Return all by default
        $which = $this->getters_map;

        // Allow returning only certain data if specified
        if (null !== $specific_fields && is_array($specific_fields) && !empty($specific_fields)) {
            $which = $specific_fields;

            // Make sure only valid fields/getters are being called and transform
            // the incoming array to match $this->getters_map format (field => getter)
            foreach ($which as $k => $field_name) {
                unset($which[$k]);
                if (isset($this->getters_map[$field_name])) {
                    $which[$field_name] = $this->getters_map[$field_name];
                }
            }
        }

        foreach ($which as $k => $getter) {
            $data[$k] = $this->$getter();
        }

        return $data;
    }

    /**
     * Returns the Thumb as an <img> tag
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getTag();
    }

    /**
     * Encodes Thumb data as a JSON string
     *
     * @return string Json representation
     */
    public function toJson()
    {
        return json_encode($this->getData());
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->getData();
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return (int) $this->height;
    }

    /**
     * @param int|string $height
     *
     * @return $this
     */
    public function setHeight($height)
    {
        $this->height = (int) $height;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrientation()
    {
        if (empty($this->orientation)) {
            $orientation = 'landscape';
            if ($this->getHeight() > $this->getWidth()) {
                $orientation = 'portrait';
            }
            $this->setOrientation($orientation);
        }

        return $this->orientation;
    }

    /**
     * @param mixed $orientation
     *
     * @return $this
     */
    public function setOrientation($orientation)
    {
        $this->orientation = $orientation;

        return $this;
    }

    /**
     * @param string|bool|null $cache_buster Optional cache-busting param. Appended to value of $this->src after a question mark.
     *                                      If a non-null falsy value is specified it uses the current request time as the cache buster.
     *                                      If a non-null truthy value is specified it is cast to string and used as the cache buster.
     *
     * @return string
     */
    public function getSrc($cache_buster = null)
    {
        $src = $this->src;

        if (null !== $cache_buster) {
            // If not explicitly provided, use current request timestamp
            if (false === $cache_buster || !$cache_buster) {
                $cache_buster = Utils::getRequestTime();
            }

            if ($cache_buster) {
                // Check if a '?' is already there, and if so use an ampersand instead
                if (false === strpos($src, '?')) {
                    // no question mark
                    $src .= '?';
                } else {
                    // already exists, use an ampersand
                    $src .= '&';
                }

                // Append our cache-buster
                $src .= (string) $cache_buster;
            }
        }

        return $src;
    }

    /**
     * This method generates full URLs (for now with hardcoded HTTP protocol) which are needed
     * during generation of email messages which have pictures in them. Protocol-less URLs are
     * not recognized in some email clients (ie. Mozilla Thunderbird).
     *
     * @return string
     */
    public function getFullSrcForEmail()
    {
        $src = null;

        if ($this->src) {
            if (substr($this->src, 0, 2) == '//') {
                $src = 'http:' . $this->src;
            } else {
                $di = \Phalcon\Di::getDefault();
                $config = $di->getShared('config');

                if (substr($this->src, 0, 1) == '/') {
                    $src = $config->app->base_uri . substr($this->src, 1);
                } else {
                    $url = $di->getShared('url');
                    $src = $url->get($this->src);
                }
            }
        }

        return $src;
    }

    /**
     * @param string $src
     *
     * @return $this
     */
    public function setSrc($src)
    {
        $this->src = $src;

        return $this;
    }

    /**
     * @return array
     */
    public function getStyle()
    {
        return $this->style;
    }

    /**
     * @param array $style
     *
     * @return $this
     */
    public function setStyle($style)
    {
        $this->style = $style;

        return $this;
    }

    /**
     * Returns the complete `<img src="..." width="..." height="..." alt="..." class="...">` HTML tag
     *
     * @param string|bool|null $cache_buster Optional cache-busting param (passed on to `getSrc()`)
     *
     * @return string
     */
    public function getTag($cache_buster = null)
    {
        // If a cache-buster is specified, clear out the tag if it was already created earlier
        // so that it has the fresh/new/cache-busted src (and other attribs too potentially)
        if (null !== $cache_buster) {
            if (!empty($this->tag)) {
                $this->tag = '';
            }
        }

        // Build the tag if not already set
        if (empty($this->tag)) {
            $attribs = array(
                'src'    => $this->getSrc($cache_buster),
                'width'  => $this->getWidth(),
                'height' => $this->getHeight(),
                'alt'    => $this->getAlt(),
                'class'  => $this->getClass()
            );

            $tag      = '<img';
            $kv_pairs = array();
            foreach ($attribs as $k => $v) {
                if (!empty($v)) {
                    $kv_pairs[] = $k . '="' . $v . '"';
                }
            }
            if (!empty($kv_pairs)) {
                $tag .= ' ' . implode(' ', $kv_pairs);
            }
            $tag .= '>';

            $this->tag = $tag;
        }

        return $this->tag;
    }

    /**
     * Returns the `<img src="...">` HTML tag (without any other attributes such as width, height, etc.).
     *
     * @param string|bool|null $cache_buster Optional cache-busting param (passed on to `getSrc()`)
     *
     * @return string
     */
    public function getSrcOnlyTag($cache_buster = null)
    {
        $tag = '<img src="' . $this->getSrc($cache_buster) . '">';

        return $tag;
    }

    /**
     * @param string $tag
     *
     * @return $this
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return (int) $this->width;
    }

    /**
     * @param int|string $width
     *
     * @return $this
     */
    public function setWidth($width)
    {
        $this->width = (int) $width;

        return $this;
    }

    /**
     * @param Size|null $size
     *
     * @return $this
     */
    public function setSize(Size $size = null)
    {
        if (null === $size) {
            $size = new Size($this->getWidth(), $this->getHeight());
        }

        $this->size = $size;

        return $this;
    }

    /**
     * @return Size
     */
    public function getSize()
    {
        if (empty($this->size)) {
            $this->setSize();
        }

        return $this->size;
    }

    /**
     * @param string $class
     *
     * @return $this
     */
    public function setClass($class = null)
    {
        if (null !== $class) {
            $this->class = $class;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param string $alt
     *
     * @return $this
     */
    public function setAlt($alt = null)
    {
        if (null !== $alt) {
            $this->alt = $alt;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getSubtype()
    {
        return $this->subtype;
    }

    /**
     * @param string $subtype
     */
    public function setSubtype($subtype)
    {
        $this->subtype = $subtype;
    }

    /**
     * @return string
     */
    public function getFilenameOrig()
    {
        return $this->filename_orig;
    }

    /**
     * @param string $filename_orig
     */
    public function setFilenameOrig($filename_orig)
    {
        $this->filename_orig = $filename_orig;
    }

    /**
     * Add a classname without overwriting any already set classnames
     *
     * @param string $class
     */
    public function addClass($class)
    {
        $current = $this->getClass();
        if (!empty($current)) {
            $class = $current . ' ' . $class;
        }

        $this->setClass($class);
    }

    public function getTransPixelSrc()
    {
        return 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
    }
}
