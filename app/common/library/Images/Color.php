<?php

namespace Baseapp\Library\Images;

class Color extends AbstractColor
{
    /**
     * ImagickPixel containing current color information
     *
     * @var \ImagickPixel
     */
    public $pixel;

    /**
     * Initiates color object from int
     *
     * @param int $value
     */
    public function initFromInteger($value)
    {
        $a = ($value >> 24) & 0xFF;
        $r = ($value >> 16) & 0xFF;
        $g = ($value >> 8) & 0xFF;
        $b = $value & 0xFF;
        $a = $this->rgb2alpha($a);
        $this->setPixel($r, $g, $b, $a);
    }

    /**
     * Initiates color object from an array
     *
     * @param array $array
     */
    public function initFromArray($array)
    {
        $array = array_values($array);
        $r = isset($array[0]) ? $array[0] : 0;
        $g = isset($array[1]) ? $array[1] : 0;
        $b = isset($array[2]) ? $array[2] : 0;
        $a = isset($array[3]) ? $array[3] : 1;
        $this->setPixel($r, $g, $b, $a);
    }

    /**
     * Initiates color object from a string
     *
     * @param string $value
     */
    public function initFromString($value)
    {
        if ($color = $this->rgbaFromString($value)) {
            $this->setPixel($color[0], $color[1], $color[2], $color[3]);
        }
    }

    /**
     * Initiates color object from an \ImagickPixel object
     *
     * @param \ImagickPixel $value
     */
    public function initFromObject($value)
    {
        if ($value instanceof \ImagickPixel) {
            $this->pixel = $value;
        }
    }

    /**
     * Initiates color object from given R, G and B values
     *
     * @param int $r
     * @param int $g
     * @param int $b
     */
    public function initFromRgb($r, $g, $b)
    {
        $this->setPixel($r, $g, $b);
    }

    /**
     * Initiates color object from given R, G, B and A values
     *
     * @param int $r
     * @param int $g
     * @param int $b
     * @param float $a
     */
    public function initFromRgba($r, $g, $b, $a)
    {
        $this->setPixel($r, $g, $b, $a);
    }

    /**
     * Returns the current color's integer value
     *
     * @return int
     */
    public function getInt()
    {
        $r = $this->getRedValue();
        $g = $this->getGreenValue();
        $b = $this->getBlueValue();
        $a = intval(round($this->getAlphaValue() * 255));

        return intval(($a << 24) + ($r << 16) + ($g << 8) + $b);
    }

    /**
     * Returns the current color's hexadecimal value
     *
     * @param string $prefix
     *
     * @return string
     */
    public function getHex($prefix = '')
    {
        return sprintf('%s%02x%02x%02x', $prefix,
            $this->getRedValue(),
            $this->getGreenValue(),
            $this->getBlueValue()
        );
    }

    /**
     * Returns the current color's RGB(A) values as an array
     *
     * @return array
     */
    public function getArray()
    {
        return array(
            $this->getRedValue(),
            $this->getGreenValue(),
            $this->getBlueValue(),
            $this->getAlphaValue()
        );
    }

    /**
     * Returns the current color's RGBA string
     *
     * @return string
     */
    public function getRgba()
    {
        return sprintf('rgba(%d, %d, %d, %.2f)',
            $this->getRedValue(),
            $this->getGreenValue(),
            $this->getBlueValue(),
            $this->getAlphaValue()
        );
    }

    /**
     * Determines if current color is different from given color
     *
     * @param AbstractColor $color
     * @param int $tolerance
     * @return bool
     */
    public function differs(AbstractColor $color, $tolerance = 0)
    {
        $color_tolerance = round($tolerance * 2.55);
        $alpha_tolerance = round($tolerance);
        $delta = array(
            'r' => abs($color->getRedValue() - $this->getRedValue()),
            'g' => abs($color->getGreenValue() - $this->getGreenValue()),
            'b' => abs($color->getBlueValue() - $this->getBlueValue()),
            'a' => abs($color->getAlphaValue() - $this->getAlphaValue())
        );
        return (
            $delta['r'] > $color_tolerance or
            $delta['g'] > $color_tolerance or
            $delta['b'] > $color_tolerance or
            $delta['a'] > $alpha_tolerance
        );
    }

    /**
     * Returns current color's red value
     *
     * @return int
     */
    public function getRedValue()
    {
        return (int) round($this->pixel->getColorValue(\Imagick::COLOR_RED) * 255);
    }

    /**
     * Returns current color's green value
     *
     * @return int
     */
    public function getGreenValue()
    {
        return (int) round($this->pixel->getColorValue(\Imagick::COLOR_GREEN) * 255);
    }

    /**
     * Returns current color's blue value
     *
     * @return int
     */
    public function getBlueValue()
    {
        return (int) round($this->pixel->getColorValue(\Imagick::COLOR_BLUE) * 255);
    }

    /**
     * Returns current color's alpha value
     *
     * @return float
     */
    public function getAlphaValue()
    {
        return round($this->pixel->getColorValue(\Imagick::COLOR_ALPHA), 2);
    }

    /**
     * Initiates ImagickPixel from given RGBA values
     *
     * @param $r
     * @param $g
     * @param $b
     * @param null $a
     *
     * @return \ImagickPixel
     */
    private function setPixel($r, $g, $b, $a = null)
    {
        $a = is_null($a) ? 1 : $a;
        return $this->pixel = new \ImagickPixel(
            sprintf('rgba(%d, %d, %d, %.2f)', $r, $g, $b, $a)
        );
    }

    /**
     * Get current color's \ImagickPixel object
     *
     * @return \ImagickPixel
     */
    public function getPixel()
    {
        return $this->pixel;
    }

    /**
     * Returns an RGB int value as a float
     *
     * @param  int $value
     * @return float
     */
    private function rgb2alpha($value)
    {
        // (255 -> 1.0) / (0 -> 0.0)
        return (float) round($value / 255, 2);
    }
}
