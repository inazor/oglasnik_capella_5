<?php

namespace Baseapp\Library\Mapping;

use Baseapp\Console;
use Baseapp\Library\Utils;
use Baseapp\Library\RawDB;
use Baseapp\Models\Categories as Category;
use Baseapp\Models\CategoriesSettings as CategorySettigns;
use Baseapp\Library\Email;
use Phalcon\Mvc\User\Component;

class MigrateCategorySettings extends Component
{
    private $di;
    private $import_first_started = 0;
    private $import_started = 0;
    private $import_ended = 0;
    private $source_db;
    private $target_db;
    private $current_timestamp;
    private $sql_limit = 250;
    private $processed = 0;
    private $remaining = null;
    private $use_pcntl = 1;
    private $last_import_id = 0;
    private $order = 'ASC';
    private $counters_total = array(
        'error'     => 0,
        'fixed'     => 0
    );
    private $counters = array(
        'error'     => 0,
        'fixed'     => 0
    );
    private $actionName = 'avusMappingFix';
    private $log2email = null;
    private $log = array(
        'msgs'      => array(),
        'errors'    => array()
    );

    public function __construct($options = null)
    {
        $this->import_started = isset($_SERVER['REQUEST_TIME_FLOAT']) ? $_SERVER['REQUEST_TIME_FLOAT'] : microtime(true);
        $this->import_first_started = $this->import_started;
        $this->di = $this->getDI();
        $this->current_timestamp = Utils::getRequestTime();
        // configure and initialize source db
        $source_db_config = array(
            'dbname'   => 'oglasnik_old',
            'options'  => array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                \PDO::ATTR_EMULATE_PREPARES   => false,
                \PDO::ATTR_STRINGIFY_FETCHES  => false,
            )
        );
        $this->source_db = new RawDB($source_db_config);
        // configure and initialize target db
        $target_db_config = array(
            'options'      => array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                \PDO::ATTR_EMULATE_PREPARES   => false,
                \PDO::ATTR_STRINGIFY_FETCHES  => false,
            )
        );
        $this->target_db = new RawDB($target_db_config);

        if (!empty($options)) {
            if (!empty($options['import_first_started'])) {
                $this->import_first_started = trim($options['import_first_started']);
            }
            $first_time = empty($options['remaining']);

            if ($first_time) {
                $this->add2Log('-- Setting up options..');
                $this->add2Log('--');
            }
            $this->options = $options;
            if (!empty($options['log2email'])) {
                $this->log2email = trim($options['log2email']);
                if ($first_time) {
                    $this->add2Log('-- log2email => ' . $this->log2email);
                }
            }
            if (!empty($options['actionName'])) {
                $this->actionName = trim($options['actionName']);
                if ($first_time) {
                    $this->add2Log('-- actionName => ' . $this->actionName);
                }
            }
            if (!empty($options['sql_limit'])) {
                $this->sql_limit = intval($options['sql_limit']);
                if ($first_time) {
                    $this->add2Log('-- sql_limit => ' . $this->sql_limit);
                }
            }
            if (!empty($options['processed'])) {
                $this->processed = intval($options['processed']);
            }
            if (!empty($options['remaining'])) {
                $this->remaining = intval($options['remaining']);
            }
            if (!empty($options['pcntl'])) {
                $this->use_pcntl = intval($options['pcntl']);
            }
            if (!empty($options['last_import_id'])) {
                $this->last_import_id = intval($options['last_import_id']);
            }
            if (!empty($options['order'])) {
                $this->order = trim($options['order']);
            }
            if (!empty($options['counters'])) {
                $counters_arr = explode('_', trim($options['counters']));
                $this->counters_total = array(
                    'error'     => !empty($counters_arr[2]) ? (int)$counters_arr[2] : 0,
                    'fixed'     => !empty($counters_arr[3]) ? (int)$counters_arr[3] : 0,
                );
            }
            if ($first_time) {
                $this->add2Log('--------------------------------------------------------');
            }
        }
    }

    private function get_run_argv()
    {
        $run_argv = array(
            ROOT_PATH . '/private/index.php',
            'import',
            $this->actionName
        );

        if ($this->log2email) {
            $run_argv[] = 'log2email=' . $this->log2email;
        }
        if ($this->import_first_started) {
            $run_argv[] = 'import_first_started=' . $this->import_first_started;
        }
        if ($this->sql_limit) {
            $run_argv[] = 'sql_limit=' . $this->sql_limit;
        }
        if ($this->processed) {
            $run_argv[] = 'processed=' . $this->processed;
        }
        if ($this->remaining) {
            $run_argv[] = 'remaining=' . $this->remaining;
        }
        if ($this->use_pcntl == 0) {
            $run_argv[] = 'pcntl=false';
        }
        if ($this->last_import_id > 0) {
            $run_argv[] = 'last_import_id=' . $this->last_import_id;
        }
        if ($this->order == 'DESC') {
            $run_argv[] = 'order=DESC';
        }
        if ($this->counters_total['error'] || $this->counters_total['fixed']) {
            $run_argv[] = 'counters=' . $this->counters_total['error'] . '_' . $this->counters_total['fixed'];
        }

        return $run_argv;
    }

    private function get_run_command()
    {
        $run_command = $_SERVER['_'] . ' ' . ROOT_PATH . '/private/index.php import ' . $this->actionName;

        if ($this->log2email) {
            $run_command .= ' log2email=' . $this->log2email;
        }
        if ($this->import_first_started) {
            $run_command .= ' import_first_started=' . $this->import_first_started;
        }
        if ($this->sql_limit) {
            $run_command .= ' sql_limit=' . $this->sql_limit;
        }
        if ($this->processed) {
            $run_command .= ' processed=' . $this->processed;
        }
        if ($this->remaining) {
            $run_command .= ' remaining=' . $this->remaining;
        }
        if ($this->use_pcntl == 0) {
            $run_command .= ' pcntl=0';
        }
        if ($this->last_import_id > 0) {
            $run_command .= ' last_import_id=' . $this->last_import_id;
        }
        if ($this->order == 'DESC') {
            $run_command .= ' order=DESC';
        }
        if ($this->counters_total['error'] || $this->counters_total['fixed']) {
            $run_command .= ' counters=' . $this->counters_total['error'] . '_' . $this->counters_total['fixed'];
        }

        return $run_command;
    }

    public function sendMail()
    {
        if ($this->log2email) {
            $this->import_ended = microtime(true);
            $time_spent = $this->import_ended - $this->import_started;
            $time_spent_min = (int)($time_spent/60);
            $time_spent_sec = $time_spent - ($time_spent_min*60);

            $time_spent_total = $this->import_ended - $this->import_first_started;
            $time_spent_total_arr = explode('.', $time_spent_total);
            $time_spent_total = date('H:i:s', $time_spent_total_arr[0]) . '.' . $time_spent_total_arr[1];

            $esitmated_import_end = microtime(true) + round(($time_spent * $this->remaining)/1000);

            $body  = 'TOTALS:' . PHP_EOL;
            $body .= '========================================================' . PHP_EOL;
            $body .= 'Imported so far  : ' . $this->processed . '(' . round(($this->processed/($this->processed + $this->remaining))*100, 2) . '%)' .  PHP_EOL;
            $body .= 'Remaining        : ' . $this->remaining . PHP_EOL;
            $body .= 'Time spent       : ' . $time_spent_total . PHP_EOL;
            $body .= 'Estimated finish : ' . date('H:i:s', $esitmated_import_end) . PHP_EOL;
            $body .= '--------------------------------------------------------' . PHP_EOL;
            $body .= ' This cycle: ' . PHP_EOL;
            $body .= ' - Fixed         : ' . $this->counters['fixed'] . PHP_EOL;
            $body .= ' - Errored       : ' . $this->counters['error'] . PHP_EOL;
            $body .= ' - Time spent    : ' . sprintf("%02d", $time_spent_min) . 'm ' . sprintf("%02d", $time_spent_sec). 's ' . PHP_EOL;
            $body .= ' - Average speed : ' . round(($time_spent / intval($this->counters['fixed'])), 4) . 's / ad' . PHP_EOL;
            if (count($this->log['errors'])) {
                $body .= '-[ Errors ]---------------------------------------------' . PHP_EOL;
                $body .= PHP_EOL . implode(PHP_EOL, $this->log['errors']) . PHP_EOL;
            }
            $body .= '-[ Messages ]-------------------------------------------' . PHP_EOL;
            $body .= PHP_EOL . implode(PHP_EOL, $this->log['msgs']) . PHP_EOL;
            $body .= '========================================================' . PHP_EOL;

            @mail(
                $this->log2email,
                '[' . $this->processed . '/' . ($this->processed + $this->remaining) . '] - Oglasnik.hr ' . $this->actionName,
                $body
            );
        }
    }

    private function add2Log($msg, $type = 'info')
    {
        $msg = trim($msg);
        if ($type == 'info') {
            $this->log['msgs'][] = date('d.m.Y @ H:i:s') . ' ' . $msg;
            echo $msg . PHP_EOL;
        } else {
            $this->log['errors'][] = date('d.m.Y @ H:i:s') . ' ' . $msg;
        }
    }

    public function avusMappingFix()
    {
        // test if categories have the same id...
        $we_are_fine = false;
        $source_cat_ids_rows = $this->source_db->find('SELECT id FROM categories WHERE transaction_type_id IS NOT NULL ORDER BY id ASC');
        $target_cat_ids_rows = $this->target_db->find('SELECT id FROM categories WHERE transaction_type_id IS NOT NULL ORDER BY id ASC');
        $source_cat_ids = array();
        $target_cat_ids = array();
        if (count($source_cat_ids_rows) == count($target_cat_ids_rows)) {
            foreach ($source_cat_ids_rows as $cat) {
                $source_cat_ids[] = $cat['id'];
            }
            foreach ($target_cat_ids_rows as $cat) {
                $target_cat_ids[] = $cat['id'];
            }

            if (implode(',', $source_cat_ids) == implode(',', $target_cat_ids)) {
                $we_are_fine = true;
            }
        }

        if ($we_are_fine) {
            $fields = array(
                'category_id',
                'publication_print_mapping'
            );
            $src_cs = $this->source_db->find('SELECT ' . implode(', ', $fields) . ' FROM categories_settings');

            $found_cs = $src_cs && count($src_cs) ? count($src_cs) : 0;
            if ($found_cs) {
                $this->add2Log('Fixing ' . $found_cs . ' categories\' settings');
                $this->add2Log('--------------------------------------------------------');

                foreach ($src_cs as $cs) {
                    $target_category = $this->target_db->findFirst(
                        'SELECT * FROM categories WHERE id = :category_id',
                        array(
                            'category_id' => $cs['category_id']
                        )
                    );
                    if ($target_category && !empty($target_category['transaction_type_id'])) {
                        $cs_avus_mapping = !empty($cs['publication_print_mapping']) ? $cs['publication_print_mapping'] : null;

                        $msg = '';
                        $update_cs_sql = "UPDATE categories_settings SET publication_print_mapping = :ppm WHERE category_id = :category_id";
                        $updated_cs = $this->target_db->update(
                            $update_cs_sql,
                            array(
                                'category_id' => $target_category['id'],
                                'ppm'         => $cs_avus_mapping
                            )
                        );
                        if ($updated_cs) {
                            $msg = 'Updated Category settings for [' . $target_category['url'] . ']';
                        }

                        if ($msg) {
                            $this->counters['fixed']++;
                            $this->add2Log($msg);
                        } else {
                            $this->counters['error']++;
                            $this->add2Log('Error fixing Category [' . $target_category['url'] . ']');
                            $this->add2Log(print_r(array(
                                'ocs' => $cs,
                                'tcs' => $target_category
                            )), 'error');
                        }
                    }
                }
            }

            return $this->counters;
        } else {
            echo 'Source Categories do not match target ones!' . PHP_EOL;
        }
    }

}
