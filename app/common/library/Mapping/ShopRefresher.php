<?php

namespace Baseapp\Library\Mapping;

use Baseapp\Console;
use Baseapp\Library\Utils;
use Baseapp\Library\RawDB;
use Baseapp\Library\Parameters\Parametrizator;
use Baseapp\Models\Users as User;
use Baseapp\Models\Categories as Category;
use Baseapp\Models\CategoriesFieldsetsParameters as FieldsetParameter;
use Baseapp\Models\Ads as Ad;
use Baseapp\Models\Media;
use Baseapp\Models\Dictionaries as Dictionary;
use Baseapp\Library\Email;
use Phalcon\Mvc\User\Component;

class ShopRefresher extends Component
{
    use MigrationTrait;

    private $di;
    private $import_first_started = 0;
    private $import_started = 0;
    private $import_ended = 0;
    private $source_db;
    private $target_db;
    private $current_timestamp;
    private $today_timestamp;
    private $yesterday_timestamp;
    private $sql_limit = 1000;
    private $processed = 0;
    private $remaining = null;
    private $use_pcntl = 1;
    private $last_import_id = 0;
    private $order = 'ASC';
    private $activeShopUserIds = array();
    private $users = array();
    private $counters_total = array(
        'imported' => 0,
        'expired'  => 0,
        'error'    => 0,
        'fixed'    => 0
    );
    private $counters = array(
        'imported' => 0,
        'expired'  => 0,
        'error'    => 0,
        'fixed'    => 0
    );
    private $countries = array();
    private $locations = array();
    private $php_path = null;
    private $actionName = 'refreshShopAdsFromAvus';
    private $avus_user = null;
    private $log2email = null;
    private $log = array(
        'msgs'   => array(),
        'errors' => array()
    );

    public function __construct($options = null)
    {
        $this->php_path = $_SERVER['_'];
        if (false === strpos($this->php_path, 'php')) {
            $this->php_path = exec('which php');
        }

        $this->import_started = isset($_SERVER['REQUEST_TIME_FLOAT']) ? $_SERVER['REQUEST_TIME_FLOAT'] : microtime(true);
        $this->import_first_started = $this->import_started;
        $this->di = $this->getDI();
        $this->current_timestamp = Utils::getRequestTime();
        $this->today_timestamp = strtotime(date('Y-m-d 00:00:00', $this->current_timestamp));
        $this->yesterday_timestamp = mktime(0, 0, 0, date('m', $this->today_timestamp), intval(date('d', $this->today_timestamp)) - 1, date('Y', $this->today_timestamp));

        // configure and initialize source db
        $source_db_config = array(
            'host'     => 'sql.oglasnik.hr',
            'username' => 'npongrac',
            'password' => 'fief7eej',
            'dbname'   => 'oglasnik',
            'options'  => array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES latin1',
                \PDO::ATTR_EMULATE_PREPARES   => false,
                \PDO::ATTR_STRINGIFY_FETCHES  => false,
            )
        );
        $this->source_db = new RawDB($source_db_config);
        // configure and initialize target db
        $target_db_config = array(
            'options'      => array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                \PDO::ATTR_EMULATE_PREPARES   => false,
                \PDO::ATTR_STRINGIFY_FETCHES  => false,
            )
        );
        $this->target_db = new RawDB($target_db_config);

        $this->avus_user = $this->target_db->findFirst("SELECT * FROM users WHERE username = :username", array('username' => 'avus'));
        $countries = $this->target_db->find("SELECT id, iso_code FROM location WHERE level = 1");
        if ($countries) {
            foreach ($countries as $location) {
                $this->countries['id'][(string)$location['id']] = (string)$location['iso_code'];
                $this->countries['iso_code'][(string)$location['iso_code']] = (string)$location['id'];
            }
        }

        $locations = $this->target_db->find("SELECT id, name FROM location");
        if ($locations) {
            foreach ($locations as $location) {
                $this->locations[(string)$location['id']] = (string)$location['name'];
            }
        }
        $this->initActiveShopUserIds();

        if (!empty($options)) {
            if (!empty($options['import_first_started'])) {
                $this->import_first_started = trim($options['import_first_started']);
            }
            $first_time = empty($options['remaining']);

            if ($first_time) {
                $this->add2Log('-- Setting up options..');
                $this->add2Log('--');
            }
            $this->options = $options;
            if (!empty($options['log2email'])) {
                $this->log2email = trim($options['log2email']);
                if ($first_time) {
                    $this->add2Log('-- log2email => ' . $this->log2email);
                }
            }
            if (!empty($options['actionName'])) {
                $this->actionName = trim($options['actionName']);
                if ($first_time) {
                    $this->add2Log('-- actionName => ' . $this->actionName);
                }
            }
            if (!empty($options['sql_limit'])) {
                $this->sql_limit = intval($options['sql_limit']);
                if ($first_time) {
                    $this->add2Log('-- sql_limit => ' . $this->sql_limit);
                }
            }
            if (!empty($options['processed'])) {
                $this->processed = intval($options['processed']);
            }
            if (!empty($options['remaining'])) {
                $this->remaining = intval($options['remaining']);
            }
            if (!empty($options['pcntl'])) {
                $this->use_pcntl = intval($options['pcntl']);
            }
            if (!empty($options['last_import_id'])) {
                $this->last_import_id = intval($options['last_import_id']);
            }
            if (!empty($options['order'])) {
                $this->order = trim($options['order']);
            }
            if (!empty($options['counters'])) {
                $counters_arr = explode('_', trim($options['counters']));
                $this->counters_total = array(
                    'imported' => !empty($counters_arr[0]) ? (int)$counters_arr[0] : 0,
                    'expired'  => !empty($counters_arr[1]) ? (int)$counters_arr[1] : 0,
                    'error'    => !empty($counters_arr[2]) ? (int)$counters_arr[2] : 0,
                    'fixed'    => !empty($counters_arr[3]) ? (int)$counters_arr[3] : 0,
                );
            }
            if ($first_time) {
                $this->add2Log('--------------------------------------------------------');
            }
        }
    }

    private function initActiveShopUserIds()
    {
        $this->activeShopUserIds = array();
        $this->users = array();

        $rows = $this->target_db->find(
/*
            'SELECT u.id, u.import_id FROM users_shops us INNER JOIN users u ON u.id = us.user_id AND u.import_id IS NOT NULL WHERE us.published_at <= :now_1 AND us.expires_at >= :now_2',
            array(
                'now_1' => $this->today_timestamp,
                'now_2' => $this->today_timestamp
            )
*/
            'SELECT u.id, u.import_id FROM users_shops us INNER JOIN users u ON u.id = us.user_id AND u.import_id IS NOT NULL',
            array()
        );
        if ($rows && count($rows)) {
            foreach ($rows as $row) {
                $this->activeShopUserIds[] = intval($row['import_id']);
                $this->users[intval($row['import_id'])] = intval($row['id']);
            }
        }

        $this->activeShopUserIds = array_unique($this->activeShopUserIds);
    }

    private function getUserIdByImportId($import_id)
    {
        return isset($this->users[(int)$import_id]) ? $this->users[(int)$import_id] : null;
    }

    private function get_location_name_by_id($location_id)
    {
        $name = null;

        if (!empty($this->locations)) {
            $name = !empty($this->locations[(string)$location_id]) ?
                    $this->locations[(string)$location_id] :
                    null;
        }

        if (!$name) {
            $name = (string)$this->target_db->findFirst(
                "SELECT name FROM location WHERE id = :location_id",
                array(
                    'location_id' => $location_id
                ),
                true
            );
            if (!$name) {
                $name = null;
            } elseif (!empty($this->locations)) {
                $this->locations[(string)$location_id] = $name;
            }
        }

        return $name;
    }

    private function get_country_id_by_iso_code($iso_code)
    {
        $country_id = null;

        if (!empty($this->countries['iso_code'])) {
            $country_id = !empty($this->countries['iso_code'][(string)$iso_code]) ?
                           $this->countries['iso_code'][(string)$iso_code] :
                           null;
        }

        if (!$country_id) {
            $country_id = (int)$this->target_db->findFirst(
                "SELECT id FROM location WHERE level = 1 AND iso_code = :iso_code",
                array(
                    'iso_code' => trim($iso_code)
                ),
                true
            );
            if (!$country_id) {
                $country_id = null;
            } elseif (!empty($this->countries['iso_code'])) {
                $this->countries['iso_code'][(string)$iso_code] = (string)$country_id;
            }
        }

        return $country_id;
    }

    private function get_iso_code_by_country_id($country_id)
    {
        $iso_code = null;

        if (!empty($this->countries['id'])) {
            $iso_code = !empty($this->countries['id'][(string)$country_id]) ?
                        $this->countries['id'][(string)$country_id] :
                        null;
        }

        if (!$iso_code) {
            $iso_code = (int)$this->target_db->findFirst(
                "SELECT iso_code FROM location WHERE id = :id",
                array(
                    'id' => trim($country_id)
                ),
                true
            );
            if (!$iso_code) {
                $iso_code = null;
            } elseif (!empty($this->countries['id'])) {
                $this->countries['id'][(string)$country_id] = (string)$iso_code;
            }
        }

        return $iso_code;
    }

    private function get_run_argv()
    {
        $run_argv = array(
            ROOT_PATH . '/private/index.php',
            'cron',
            $this->actionName
        );

        if ($this->log2email) {
            $run_argv[] = 'log2email=' . $this->log2email;
        }
        if ($this->import_first_started) {
            $run_argv[] = 'import_first_started=' . $this->import_first_started;
        }
        if ($this->sql_limit) {
            $run_argv[] = 'sql_limit=' . $this->sql_limit;
        }
        if ($this->processed) {
            $run_argv[] = 'processed=' . $this->processed;
        }
        if ($this->remaining) {
            $run_argv[] = 'remaining=' . $this->remaining;
        }
        if ($this->use_pcntl == 0) {
            $run_argv[] = 'pcntl=false';
        }
        if ($this->last_import_id > 0) {
            $run_argv[] = 'last_import_id=' . $this->last_import_id;
        }
        if ($this->order == 'DESC') {
            $run_argv[] = 'order=DESC';
        }
        if ($this->counters_total['imported'] || $this->counters_total['expired'] || $this->counters_total['error'] || $this->counters_total['fixed']) {
            $run_argv[] = 'counters=' . $this->counters_total['imported'] . '_' . $this->counters_total['expired'] . '_' . $this->counters_total['error'] . '_' . $this->counters_total['fixed'];
        }

        return $run_argv;
    }

    private function get_run_command()
    {
        $run_command = $this->php_path . ' ' . ROOT_PATH . '/private/index.php cron ' . $this->actionName;

        if ($this->log2email) {
            $run_command .= ' log2email=' . $this->log2email;
        }
        if ($this->import_first_started) {
            $run_command .= ' import_first_started=' . $this->import_first_started;
        }
        if ($this->sql_limit) {
            $run_command .= ' sql_limit=' . $this->sql_limit;
        }
        if ($this->processed) {
            $run_command .= ' processed=' . $this->processed;
        }
        if ($this->remaining) {
            $run_command .= ' remaining=' . $this->remaining;
        }
        if ($this->use_pcntl == 0) {
            $run_command .= ' pcntl=0';
        }
        if ($this->last_import_id > 0) {
            $run_command .= ' last_import_id=' . $this->last_import_id;
        }
        if ($this->order == 'DESC') {
            $run_command .= ' order=DESC';
        }
        if ($this->counters_total['imported'] || $this->counters_total['expired'] || $this->counters_total['error'] || $this->counters_total['fixed']) {
            $run_command .= ' counters=' . $this->counters_total['imported'] . '_' . $this->counters_total['expired'] . '_' . $this->counters_total['error'] . '_' . $this->counters_total['fixed'];
        }

        return $run_command;
    }

    /**
     * Validate import category id
     *
     * Import category id (as far as we know) is a string with even number
     * of characters (digits), and every category/subcategory is
     * represented with a two-digit code..
     *
     * @param string $import_category_id
     * @return bool
     */
    protected function valid_import_category_id($import_category_id)
    {
        $import_category_id = trim($import_category_id);

        if (strlen($import_category_id) % 2 == 0) {
            return true;
        }
        $this->add2Log('Import category: ' . $import_category_id . ' is not valid?', 'error');

        return false;
    }

    protected function getAllCategoriesFrom($starting_category_id = null)
    {
        $curr_starting_index = $starting_category_id ? array_search($starting_category_id[0], array_keys($this->cat_ids)) : 0;

        $categories = array();

        $curr_index = 0;
        foreach ($this->cat_ids as $cat_id => $cat_data) {
            if ($curr_index >= $curr_starting_index) {
                $categories[$cat_id] = $cat_data;
            }
            $curr_index++;
        }

        return $categories;
    }

    public function get($import_category_id, $src_ad_content)
    {
        $import_category_id = trim($import_category_id);

        $matching_category_data = null;

        if ($this->valid_import_category_id($import_category_id)) {
            $matching_category_data = array(
                'name'     => array(),
                'settings' => array(
                    'online_category_id' => null,
                    'parameters'         => array(),
                    'extract_data'       => null
                )
            );
            $curr_category_id = '';
            $curr_category = null;

            // split the import category id into an array with two-digit keys
            $categories = str_split($import_category_id, 2);
            $last_category_parameter = null;
            foreach ($categories as $i => $category_chunk) {
                // construct current category's id
                $curr_category_id .= $category_chunk;
                // get the category array
                if ($curr_category == null) {
                    $curr_category = isset($this->import_categories[$curr_category_id]) ? $this->import_categories[$curr_category_id] : null;
                } else {
                    $curr_category = isset($curr_category['items'][$curr_category_id]) ? $curr_category['items'][$curr_category_id] : null;
                }

                if ($curr_category) {
                    // this is for breadcrumb construction so we can see which category we're working on
                    if (isset($curr_category['name'])) {
                        $matching_category_data['name'][] = trim($curr_category['name']);
                    } elseif (is_string($curr_category)) {
                        $matching_category_data['name'][] = trim($curr_category);
                    }

                    // in every category we can have multiple elements with mapping data:
                    // online_id, parameter(s), items, ...


                    // online_id is a specific field. We can only have one online_id at the end but, in the nested array
                    // we can have an online_id field on every level. It is like that so we can override (or at least have
                    // a fallback id in case we don't find a match). So, this field (if found) is simply overwritten!
                    if (isset($curr_category['online_id'])) {
                        // this field can have multiple different types.. it can be a simple integer value,
                        // but it can have an array (array is used when we don't know for sure where to map current category
                        // so we have to run a custom method in order to determinate the right category_id)
                        if (is_array($curr_category['online_id'])) {
                            $custom_method = $curr_category['online_id']['method'];
                            $fallback_id = isset($curr_category['online_id']['fallback_id']) ? $curr_category['online_id']['fallback_id'] : null;
                            if (null === $fallback_id && intval($matching_category_data['settings']['online_category_id'])) {
                                // see if we have online_id from most recent parent? if so, use it as a fallback_id
                                $fallback_id = intval($matching_category_data['settings']['online_category_id']);
                            }
                            $matching_category_data['settings']['online_category_id'] = $this->$custom_method(
                                $src_ad_content,
                                $curr_category['online_id']['keyword'],
                                $fallback_id
                            );
                        } elseif (is_numeric($curr_category['online_id']) && intval($curr_category['online_id'])) {
                            $matching_category_data['settings']['online_category_id'] = intval($curr_category['online_id']);
                        }
                    }

                    // we have two different parameter settings... one is an array of multiple parameters that could be
                    // applied to the ad, the other one is one specific parameter that should be applied to the ad.
                    //
                    // it is important to note that they wont come both at the same time (as it makes no sense!) - it
                    // will be one or another! also, it's important to know that 'parameter' (single one) could be
                    // transferred in the next iteration of the loop (so we can filter subitems, if we want so!)
                    if (isset($curr_category['parameters']) && count($curr_category['parameters'])) {
                        $matching_category_data['settings']['parameters'] = array_merge(
                            $matching_category_data['settings']['parameters'],
                            $curr_category['parameters']
                        );
                    } elseif (isset($curr_category['parameter']) && is_array($curr_category['parameter'])) {
                        if (isset($curr_category['parameter']['slug']) && isset($curr_category['parameter']['type'])) {
                            $latest_parameter = $curr_category['parameter'];

                            // we have multiple type of parameter settings
                            //  - match_str -> we'll be looking for the value in next loop's iteration, so we'll be
                            //    transferring the parameter to next loop
                            if ($curr_category['parameter']['type'] == 'match_str') {
                                $latest_parameter = $curr_category['parameter'];
                            } else {
                                // for now we have only one more parameter type, but we'll clear the $latest_parameter
                                // as we don't need it anymore (as it's valid only for next levels items)..
                                $latest_parameter = null;

                                // check if we have any value set? if yes, add the parameter to stack!
                                if (isset($curr_category['parameter']['value'])) {
                                    $matching_category_data['settings']['parameters'][$curr_category['parameter']['slug']] = array(
                                        'type' => $curr_category['parameter']['type'],
                                        'value' => $curr_category['parameter']['value']
                                    );
                                }
                            }
                        } elseif ($latest_parameter) {
                            // it could happen that we have a parameter from earlier level, so we need to reset it!
                            $latest_parameter = null;
                        }
                    } elseif (isset($latest_parameter)) {
                        if ($latest_parameter['type'] == 'match_str' && trim($curr_category['name'])) {
                            $matching_category_data['settings']['parameters'][$latest_parameter['slug']] = array(
                                'type' => 'match_str',
                                'name' => trim($curr_category['name'])
                            );
                        }

                        // we're done with this level, so clear the $latest_parameter variable...
                        $latest_parameter = null;
                    }

                    // we can have some custom method we'd like to run in order to try to extract some specific data
                    // from the ad located in current category... if 'extract_data' property is set, that's the name of
                    // the custom method we need to call later in the process...
                    if (isset($curr_category['extract_data']) && trim($curr_category['extract_data'])) {
                        $matching_category_data['settings']['extract_data'] = trim($curr_category['extract_data']);
                    }
                } else {
                    $this->add2Log('[' . $import_category_id . '][' . $src_ad_content . '] for some reason we couldn\'t find the category ... break the loop?', 'error');
                    // for some reason we couldn't find the category ... break the loop?
                    //break;
                }
            }

            // handle the breadcrump part we talked about at the beginning of this method :)
            $matching_category_data['name'] = implode(' › ', $matching_category_data['name']);
        }

        if ($matching_category_data['settings']['online_category_id']) {
            $db_cat = $this->target_db->findFirst(
                "SELECT * FROM categories WHERE id = :id",
                array(
                    'id' => $matching_category_data['settings']['online_category_id']
                )
            );
            if ($db_cat) {
                return $matching_category_data;
            }
        }

        return null;
    }


    /**
     * Helper method for matching a string in first level of a specific parameters dictionary (if dict exists!)
     *
     * @param int $category_id Category where to look for parameter_slug
     * @param string $parameter_slug Parameter slug where to look for dictionary match
     * @param string $match_str String to match
     * @param null|string $level2_possible_match_str String to match in second level
     * @return null|int|array
     */
    protected function match_string_in_parameters_dictionary($category_id, $parameter_slug, $match_str, $level2_possible_match_str = null)
    {
        $matching_id = null;

        $working_dictionary_id = $this->target_db->findFirst(
            "SELECT dictionary_id FROM parameters p INNER JOIN categories_fieldsets_parameters cfp ON p.id = cfp.parameter_id AND cfp.category_id = :category_id WHERE cfp.parameter_slug = :parameter_slug",
            array(
                'category_id' => (int)$category_id,
                'parameter_slug' => trim($parameter_slug)
            ),
            true
        );

        if ($working_dictionary_id) {
            $match_id = $this->target_db->findFirst(
                "SELECT id FROM dictionaries WHERE root_id = :root_id AND level = 2 AND name LIKE CONCAT(:name, '%')",
                array(
                    'root_id' => $working_dictionary_id,
                    'name'    => trim($match_str)
                ),
                true
            );

            if ($match_id) {
                $matching_id = $match_id;

                // try findind id for second level?
                if ($level2_possible_match_str) {
                    $exploded_string = explode(',', $level2_possible_match_str);
                    if (count($exploded_string)) {
                        $first_word = trim($exploded_string[0]);

                        $level2_match_id = $this->target_db->findFirst(
                            "SELECT id FROM dictionaries WHERE root_id = :root_id AND parent_id = :parent_id AND name LIKE CONCAT(:name, '%')",
                            array(
                                'root_id'   => $working_dictionary_id,
                                'parent_id' => $matching_id,
                                'name'      => trim($match_str)
                            ),
                            true
                        );
                        if ($level2_match_id) {
                            $matching_id = array(
                                $matching_id,
                                $level2_match_id
                            );
                        } else {
                            $exploded_first_word = explode(' ', $first_word);
                            $first_word = trim($exploded_first_word[0]);

                            $level2_match_id = $this->target_db->findFirst(
                                "SELECT id FROM dictionaries WHERE root_id = :root_id AND level = 3 AND parent_id = :parent_id AND name LIKE CONCAT(:name, '%')",
                                array(
                                    'root_id'   => $working_dictionary_id,
                                    'parent_id' => $matching_id,
                                    'name'      => trim($first_word)
                                ),
                                true
                            );
                            if ($level2_match_id) {
                                $matching_id = array(
                                    $matching_id,
                                    $level2_match_id
                                );
                            }
                        }
                    }
                }
            }
        } else {
            $this->add2Log("match_string_in_parameters_dictionary($category_id, $parameter_slug, $match_str, $level2_possible_match_str) -> NO working_dictionary_id", 'error');
        }

        if (!$matching_id) {
            $this->add2Log("match_string_in_parameters_dictionary($category_id, $parameter_slug, $match_str, $level2_possible_match_str) -> NO match for dictionary id", 'error');
        }

        return $matching_id;
    }

    protected function getLocationByRegion($region_id)
    {
        $data = array(
            '0'  => array('country_id' => 1),                      // Hrvatska
            '1'  => array('country_id' => 1),                      // Hrvatska
            '2'  => array('country_id' => 1, 'county_id' => 232),  // Bjelovarsko-bilogorska županija
            '3'  => array('country_id' => 1, 'county_id' => 588),  // Brodsko-posavska županija
            '4'  => array('country_id' => 1, 'county_id' => 842),  // Dubrovačko-neretvanska županija
            '5'  => array('country_id' => 1, 'county_id' => 7442), // Grad Zagreb
            '6'  => array('country_id' => 1, 'county_id' => 1136), // Istarska županija
            '7'  => array('country_id' => 1, 'county_id' => 1867), // Karlovačka županija
            '8'  => array('country_id' => 1, 'county_id' => 2682), // Koprivničko-križevačka županija
            '9'  => array('country_id' => 1, 'county_id' => 3002), // Krapinsko-zagorska županija
            '10' => array('country_id' => 1, 'county_id' => 3460), // Ličko-senjska županija
            '11' => array('country_id' => 1, 'county_id' => 3755), // Međimurska županija
            '12' => array('country_id' => 1, 'county_id' => 3920), // Osječko-baranjska županija
            '13' => array('country_id' => 1, 'county_id' => 4242), // Požeško-slavonska županija
            '14' => array('country_id' => 1, 'county_id' => 4559), // Primorsko-goranska županija
            '15' => array('country_id' => 1, 'county_id' => 6174), // Šibensko-kninska županija
            '16' => array('country_id' => 1, 'county_id' => 5188), // Sisačko-moslavačka županija
            '17' => array('country_id' => 1, 'county_id' => 5677), // Splitsko-dalmatinska županija
            '18' => array('country_id' => 1, 'county_id' => 6416), // Varaždinska županija
            '19' => array('country_id' => 1, 'county_id' => 6766), // Virovitičko-podravska županija
            '20' => array('country_id' => 1, 'county_id' => 6985), // Vukovarsko-srijemska županija
            '21' => array('country_id' => 1, 'county_id' => 7149), // Zadarska županija
            '22' => array('country_id' => 1, 'county_id' => 7760), // Zagrebačka županija
            '23' => array('country_id' => 1, 'county_id' => 8571)  // Hrvatska -> Inozemstvo
        );

        return (isset($data[$region_id]) ? $data[$region_id] : null);
    }

    protected function downloadUrlToFile($url)
    {
        $outFileName = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'tmp-download-'.md5($url);
        $httpcode = null;
        $result = false;

        if (is_file($url)) {
            $result = copy($url, $outFileName);
        } else {
            $options = array(
              CURLOPT_FILE    => fopen($outFileName, 'w'),
              CURLOPT_TIMEOUT => 28800, // set this to 8 hours so we dont timeout on big files
              CURLOPT_URL     => $url
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            $result = ($httpcode>=200 && $httpcode<300) ? true : false;
        }

        if ($result) {
            return $outFileName;
        } else {
            $this->add2Log("downloadUrlToFile($url) -> HTTP CODE $httpcode", 'error');
        }

        return null;
    }

    protected function get_media_files($media_files)
    {
        $media = array();
        foreach ($media_files as $file) {
            if (isset($file['photo']) && trim($file['photo'])) {

                $tmp_file = $this->downloadUrlToFile('http://slike.oglasnik.hr' . trim($file['photo']));
                if ($tmp_file && file_exists($tmp_file)) {
                    $finfo = @finfo_open(FILEINFO_MIME_TYPE);
                    if ($finfo) {
                        $mime = @finfo_file($finfo, $tmp_file);
                        if ($mime) {
                            $type = (explode('/', $mime)[1]);
                            $filesize = filesize($tmp_file);

                            $media[] = array(
                                'name'     => 'ad-picture-' . $file['id'] . '.' . $type,
                                'type'     => $mime,
                                'tmp_name' => $tmp_file,
                                'error'    => UPLOAD_ERR_OK,
                                'size'     => $filesize
                            );
                        }
                    }
                }
            }
        }

        return $media;
    }

    protected function addMediaFiles($media_files, $user_id = null)
    {
        $pictures = null;

        $media = new Media();
        if ($user_id) {
            $media->created_by_user_id  = $user_id;
            $media->modified_by_user_id = $user_id;
        }
        $media->setCustomFiles($media_files);
        $results = $media->handle_upload(null, false);
        if (isset($results['media'])) {
            $pictures = array();
            foreach ($results['media'] as $media_row) {
                if (isset($media_row['id'])) {
                    $pictures[] = intval($media_row['id']);
                }
            }
        }

        return $pictures;
    }

    protected function toUTF8($str)
    {
        $str = iconv('ISO-8859-2', 'UTF-8', $str);
        $str = str_replace('&#8364;', '€', $str);
        return $str;
    }

    protected function generateTitle($original_title = '', $original_ad_content = '')
    {
        $original_title = trim($original_title);
        $original_body  = trim($this->fixTextFormatting(strip_tags($original_ad_content, '<br><br/>')));

        if (!$original_title || mb_substr($original_body, 0, mb_strlen($original_title, 'UTF-8'), 'UTF-8') == $original_title) {
            $ad_content = str_ireplace(array("<br>", "<br/>", "<br />"), "\n", $original_ad_content);
            if (strpos($ad_content, "\n") !== false) {
                $ad_content = strtok($ad_content, "\n");
            }

            $original_title = $this->fixTitleFormatting(Utils::str_truncate(strip_tags($ad_content), 120));
        }

        return $original_title;
    }

    protected function fixTitleFormatting($title)
    {
        $normalized_title = trim($title);

        if ($normalized_title) {
            $normalized_title = str_replace(array("\r\n", "\r"), " ", $normalized_title);
            $normalized_title = preg_replace("/ {2}/", " ", $normalized_title);

            // fix wrong space near ',' or '.' chars..
            $normalized_title = str_replace("…", "...", $normalized_title);
            $normalized_title = str_replace(" ,", ", ", $normalized_title);
            $normalized_title = str_replace(" .", ". ", $normalized_title);
            $normalized_title = preg_replace("/ {2,}/", " ", $normalized_title);


            // remove multiple occurances of '.' at the end of the string
            $normalized_title = preg_replace("/\.{2,}$/", "", $normalized_title);
        }

        return trim(strip_tags($normalized_title));
    }

    protected function fixTextFormatting($content, $trim_new_lines = false)
    {
        $normalized_content = trim($content);

        if ($normalized_content) {
            $normalized_content = str_replace(array("\r\n", "\r"), "\n", $normalized_content);
            $normalized_content = preg_replace("/ {3,}/", "\n\n", $normalized_content);
            $normalized_content = preg_replace("/ {2}/", "\n", $normalized_content);
            if ($trim_new_lines) {
                $normalized_content = str_replace("\n", ' ', $normalized_content);
            }

            $normalized_content = str_replace("…", "...", $normalized_content);
            // fix wrong space near ',' or '.' chars..
            $normalized_content = preg_replace("/\s?:\s?/", ": ", $normalized_content);
            $normalized_content = str_replace(" ,", ", ", $normalized_content);
            $normalized_content = str_replace(" .", ". ", $normalized_content);

            $normalized_content = preg_replace("/ {2,}/", " ", $normalized_content);

            // remove multiple occurances of '.' at the end of the string
            $normalized_content = preg_replace("/\.{2,}$/", ".", $normalized_content);
        }

        return trim($normalized_content);
    }

    protected function refreshAd(array $trg_ad, array $src_ad)
    {
        if (isset($trg_ad['id']) && intval($trg_ad['id'])) {
            $ad_content = trim($this->toUTF8($src_ad['body']));
            if (isset($src_ad['print_body']) && trim($src_ad['print_body'])) {
                $ad_content = trim($this->toUTF8($src_ad['print_body']));
            }
            $user_id = $trg_ad['user_id'];//$this->getUserIdByImportId($src_ad['submitted_by']);
            if ($user_id && $src_category = $this->get($src_ad['inet_kat'], $ad_content)) {
                $ad_content = strip_tags($ad_content, '<br><br/><br />');

                if (isset($src_category['settings']['online_category_id']) && intval($src_category['settings']['online_category_id'])) {
                    $ad_category = Category::findFirst($src_category['settings']['online_category_id']);
                    if ($ad_category) {
                        $curr_datetime = Utils::getRequestTime();
                        $location = $this->getLocationByRegion($src_ad['region']);

                        $ad_title = trim(strip_tags($src_ad['title'])) ? trim(strip_tags($this->toUTF8(trim($src_ad['title'])))) : null;
                        $ad_body = trim($this->fixTextFormatting(strip_tags($ad_content), '<br><br/>'));

                        // if ad's title is not present or it's present in the body
                        // also, we generate the new title (with more chars) than it
                        // has now, and is better trimmed...
                        if (!$ad_title/* || mb_substr($ad_body, 0, mb_strlen($ad_title, 'UTF-8'), 'UTF-8') == $ad_title*/) {
                            $ad_title = trim(strip_tags(Utils::str_truncate($this->toUTF8($src_ad['body']), 120)));
                        }

                        $ad_phone1 = null;
                        if (trim($src_ad['phone1'])) {
                            $ad_phone1 = trim($src_ad['phone1']);
                        }
                        $ad_phone2 = null;
                        if (trim($src_ad['phone2'])) {
                            $ad_phone2 = trim($src_ad['phone2']);
                        }

                        $modified_at        = trim($src_ad['updated']) ? strtotime($src_ad['updated']) : null;
                        $expires_at         = isset($src_ad['adr_expires']) && !empty($src_ad['adr_expires']) ? strtotime(trim($src_ad['adr_expires'])) : null;
                        if (!$expires_at) {
                            $expires_at = $this->current_timestamp;
                        }

                        if (trim($ad_title)) {
                            $ad = new Ad();
                            $ad->category_id        = $ad_category->id;
                            $ad->user_id            = $user_id;
                            $ad->created_by_user_id = $user_id;
                            $ad->title              = $ad_title;
                            $ad->description        = $ad_body;
                            $ad->description_tpl    = $ad_body;
                            $ad->phone1             = $ad_phone1;
                            $ad->phone2             = $ad_phone2;
                            $ad->modified_at        = $modified_at;
                            $ad->expires_at         = $expires_at;
                            $ad->active             = ($expires_at && $expires_at > $this->current_timestamp ? 1 : 0);

                            $ad->import_id = $src_ad['id'];

                            $parametrizator = new Parametrizator();
                            $parametrizator->setModule('import');

                            // turn off processing of description_tpl and description_offline for imported ads
                            $parametrizator->process_description_tpl_placeholders = false;
                            $parametrizator->process_description_offline_placeholders = false;

                            $parametrizator->setCategory($ad_category);
                            $parametrizator->setAd($ad);

                            // add description parameter
                            $parametrizator->addParameterBySlug('ad_description', $ad_body);

                            // add the title parameter only if it current ad's description doesn't contain its title!
                            //
                            // if this condition doesn't fulfill, the imported ad will still have its
                            // title set (we did this with $ad->title), but adding the parameter's value via addParameterBySlug
                            // method adds parameter's value to the json_data field also, and in later processing, we use
                            // json_data to prepare search_terms... so if we found the substring of title in
                            // description, then there's no need to double information in ads_search_terms table!
                            if (mb_stripos($ad_body, $ad_title, 0, 'UTF-8') === false) {
                                $parametrizator->addParameterBySlug('ad_title', $ad_title);
                            }

                            // add price parameter
                            if (isset($src_ad['cijena']) && intval($src_ad['cijena']) > 0) {
                                $ad->price       = (int)$src_ad['cijena'];
                                $ad->currency_id = intval($src_ad['valuta_id']) ? intval($src_ad['valuta_id']) : 1;
                            } else {
                                $ad->price       = 0;
                                $ad->currency_id = 1;
                            }
                            $parametrizator->addParameterBySlug(
                                'ad_price',
                                array(
                                    'value'       => (int)$ad->price,
                                    'currency_id' => $ad->currency_id
                                )
                            );

                            $location_set_via_parameters = false;
                            // set values for found parameters...
                            if (isset($src_category['settings']['parameters']) && count($src_category['settings']['parameters'])) {
                                foreach ($src_category['settings']['parameters'] as $parameter_slug => $parameter_data) {
                                    if ((isset($parameter_data['name']) && trim($parameter_data['name'])) || isset($parameter_data['value'])) {
                                        $parameter_value = null;

                                        switch ($parameter_data['type']) {
                                            case 'field':
                                                if (isset($src_ad[trim($parameter_data['name'])]) && trim($src_ad[trim($parameter_data['name'])])) {
                                                    $parameter_value = trim($src_ad[trim($parameter_data['name'])]);
                                                }
                                                break;
                                            case 'match_str':
                                                $parameter_value = $this->match_string_in_parameters_dictionary(
                                                    $ad_category->id,
                                                    $parameter_slug,
                                                    trim($parameter_data['name']),
                                                    trim($this->toUTF8($src_ad['body']))
                                                );
                                                break;
                                            case 'value':
                                                $parameter_value = isset($parameter_data['value']) ? $parameter_data['value'] : null;
                                                break;
                                        }

                                        if ($parameter_value) {
                                            if ($parameter_slug == 'ad_location') {
                                                if (count($parameter_value) < count($location)) {
                                                    $parameter_value = $location;
                                                }
                                                $location_set_via_parameters = true;
                                            }
                                            $parametrizator->addParameterBySlug($parameter_slug, $parameter_value);
                                        }
                                    }
                                }
                            }

                            if ($src_category['settings']['extract_data']) {
                                $method_name = $src_category['settings']['extract_data'];
                                $extracted_data = $this->$method_name($ad_content);

                                if (count($extracted_data)) {
                                    foreach ($extracted_data as $parameter_slug => $parameter_value_raw) {
                                        $parameter_value = $parameter_value_raw;
                                        if ($parameter_slug == 'ad_location') {
                                            if (count($parameter_value) < count($location)) {
                                                $parameter_value = $location;
                                            }
                                            $location_set_via_parameters = true;
                                        }
                                        $parametrizator->addParameterBySlug($parameter_slug, $parameter_value);
                                    }
                                }
                            }

                            $ads_pictures_count = 0;
                            if ($src_ad['pictures']) {
                                $media_files = $this->get_media_files($src_ad['pictures']);
                                if (count($media_files)) {
                                    $pictures_array = $this->addMediaFiles($media_files, $user_id);

                                    if (is_array($pictures_array) && count($pictures_array)) {
                                        $ads_pictures_count = count($pictures_array);
                                        $parametrizator->addParameterBySlug('ad_media', $pictures_array);
                                    }
                                }
                            }

                            if (!$location_set_via_parameters && isset($location) && count($location)) {
                                $parametrizator->addParameterBySlug('ad_location', $location);
                            }

                            $parametrizator->parametrize_imported();

                            // handle ad's pictures in case they were not changed
                            $old_trg_ad_json_data = json_decode($trg_ad['json_data']);
                            $old_trg_ad_media_gallery = isset($old_trg_ad_json_data->ad_media_gallery) ? $old_trg_ad_json_data->ad_media_gallery : null;
                            $trg_ad_json_data = json_decode($ad->json_data);
                            if (!isset($trg_ad_json_data->ad_media_gallery)) {
                                if ($old_trg_ad_media_gallery) {
                                    $trg_ad_json_data->ad_media_gallery = $old_trg_ad_media_gallery;
                                    $ad->json_data = json_encode($trg_ad_json_data);
                                }
                            }

                            // generate AD insert SQL...
                            $ad_array = array(
                                'category_id' => $ad->category_id,
                                'title' => $ad->title,
                                'description' => $ad->description,
                                'description_tpl' => $ad->description_tpl,
                                'description_offline' => isset($ad->description_offline) && !empty($ad->description_offline) ? $ad->description_offline : null,
                                'price' => $ad->price,
                                'currency_id' => $ad->currency_id,
                                'phone1' => $ad->phone1,
                                'phone2' => $ad->phone2,
                                'modified_at' => $ad->modified_at,
                                'expires_at' => $ad->expires_at,
                                'country_id' => $ad->country_id,
                                'county_id' => $ad->county_id,
                                'city_id' => $ad->city_id,
                                'municipality_id' => $ad->municipality_id,
                                'lat' => $ad->lat,
                                'lng' => $ad->lng,
                                'json_data' => $ad->json_data,
                                'active' => $ad->active
                            );

                            $fields = array();
                            $params = array();
                            foreach ($ad_array as $field => $val) {
                                $fields[] = $field . ' = :' . $field;
                                $params[':' . $field] = $val;
                            }
                            $params[':ad_id'] = intval($trg_ad['id']);

                            $ad_update_sql = "UPDATE ads SET " . implode(', ', $fields) . " WHERE id = :ad_id";

                            // update the ad in DB
                            $updated_ad = $this->target_db->update($ad_update_sql, $params);
                            if ($updated_ad) {
                                $ad_array['id'] = intval($trg_ad['id']);
                                // first, remove all old ads_parameters for this ad
                                $this->target_db->update('DELETE FROM ads_parameters WHERE ad_id = :ad_id', array('ad_id' => intval($trg_ad['id'])));
                                // add assigned parameters if present
                                if (!empty($parametrizator->SQL_ad_parameters)) {
                                    $ads_parameters_sql = str_ireplace('[AD_ID]', intval($trg_ad['id']), "INSERT INTO ads_parameters (ad_id, parameter_id, level, item_index, value) VALUES " . implode(', ', $parametrizator->SQL_ad_parameters));
                                    $inserted_ad_params = $this->target_db->insert($ads_parameters_sql);
                                }

                                if ($src_ad['pictures']) {
                                    // first, remove all old medias for this ad
                                    $this->target_db->update('DELETE FROM ads_media WHERE ad_id = :ad_id', array('ad_id' => intval($trg_ad['id'])));
                                    // add assigned medias if present
                                    if (!empty($parametrizator->SQL_ad_media)) {
                                        $ads_media_sql = str_ireplace('[AD_ID]', intval($trg_ad['id']), "INSERT INTO ads_media (ad_id, media_id, sort_idx) VALUES " . implode(', ', $parametrizator->SQL_ad_media));
                                        $inserted_ad_media = $this->target_db->insert($ads_media_sql);
                                    }
                                }

                                // add assigned search terms if present
                                $SQL_ad_search_terms = isset($src_ad['adr_search_text']) && !empty($src_ad['adr_search_text']) ? trim($this->toUTF8($src_ad['adr_search_text'])) : '';
                                if ($parametrizator->SQL_ad_search_terms) {
                                    $SQL_ad_search_terms .= ' ' . $parametrizator->SQL_ad_search_terms;
                                    $inserted_ad_search_terms = $this->target_db->insert(
                                        "REPLACE INTO ads_search_terms (ad_id, search_data, search_data_unaccented) VALUES (:ad_id, :search_data, :search_data_unaccented)",
                                        array(
                                            'ad_id'                  => intval($trg_ad['id']),
                                            'search_data'            => trim($SQL_ad_search_terms),
                                            'search_data_unaccented' => trim(Utils::remove_accents($SQL_ad_search_terms))
                                        )
                                    );
                                }

                                return array(
                                    'ad'       => $ad_array,
                                    'pictures' => $ads_pictures_count
                                );
                            } else {
                                $this->add2Log($ad_update_sql, 'error');
                            }
                        }
                    } else {
                        $this->add2Log("refreshAd(".$src_ad['id'].") -> NO target category", 'error');
                    }
                }
            } else {
                $this->add2Log("refreshAd(".$src_ad['id'].") -> NO src_category(".$src_ad['inet_kat'].")", 'error');
            }
        }

        return null;
    }

/******************************************************************************/
    public function sendMail()
    {
        if ($this->log2email && !is_bool($this->log2email)) {
            $this->import_ended = microtime(true);
            $time_spent = $this->import_ended - $this->import_started;
            $time_spent_min = (int)($time_spent/60);
            $time_spent_sec = $time_spent - ($time_spent_min*60);

            $time_spent_total = $this->import_ended - $this->import_first_started;
            $time_spent_total_arr = explode('.', $time_spent_total);
            $time_spent_total = date('H:i:s', $time_spent_total_arr[0]) . '.' . $time_spent_total_arr[1];

            $esitmated_import_end = microtime(true) + round(($time_spent/1000) * $this->remaining);

            $body  = 'TOTALS:' . PHP_EOL;
            $body .= '========================================================' . PHP_EOL;
            $body .= 'Refreshed so far : ' . $this->processed . '(' . round(($this->processed/($this->processed+$this->remaining))*100, 2) . '%)' .  PHP_EOL;
            $body .= 'Remaining        : ' . $this->remaining . PHP_EOL;
            $body .= 'Time spent       : ' . $time_spent_total . PHP_EOL;
            $body .= 'Estimated finish : ' . date('H:i:s', $esitmated_import_end) . PHP_EOL;
            $body .= '--------------------------------------------------------' . PHP_EOL;
            $body .= ' This cycle: ' . PHP_EOL;
            $body .= ' - Imported      : ' . $this->counters['imported'] . PHP_EOL;
            $body .= ' - Errored       : ' . $this->counters['error'] . PHP_EOL;
            $body .= ' - Time spent    : ' . sprintf("%02d", $time_spent_min) . 'm ' . sprintf("%02d", $time_spent_sec). 's ' . PHP_EOL;
            $body .= ' - Average speed : ' . round(($time_spent / (intval($this->counters['imported']) ? intval($this->counters['imported']) : 1)), 4) . 'ms / ad' . PHP_EOL;
            if (count($this->log['errors'])) {
                $body .= '-[ Errors ]---------------------------------------------' . PHP_EOL;
                $body .= PHP_EOL . implode(PHP_EOL, $this->log['errors']) . PHP_EOL;
            }
            $body .= '-[ Messages ]-------------------------------------------' . PHP_EOL;
            $body .= PHP_EOL . implode(PHP_EOL, $this->log['msgs']) . PHP_EOL;
            $body .= '========================================================' . PHP_EOL;

            @mail(
                $this->log2email,
                '[' . $this->processed . '/' . ($this->processed+$this->remaining) . '] - Oglasnik.hr ' . $this->actionName,
                $body
            );
        }
    }

    private function add2Log($msg, $type = 'info')
    {
        $msg = trim($msg);
        if ($type == 'info') {
            $this->log['msgs'][] = date('d.m.Y @ H:i:s') . ' ' . $msg;
        } else {
            $this->log['errors'][] = date('d.m.Y @ H:i:s') . ' ' . $msg;
        }

        if ($this->log2email) {
            echo $msg . PHP_EOL;
        }
    }

    public function refreshAds()
    {
        if (!$this->last_import_id) {
            $this->last_import_id = 0;
        }

        if (!$this->processed) {
            $this->processed = 0;
        }

        $fields = array(
            'ad.id',
            'ad.submitted_by',
            'ad.expires',
            'ad.inet_kat',
            'ad.title',
            'ad.papir_bold_title',
            'ad.body',
            'ad.papir_body',
            'ad.phone1',
            'ad.phone2',
            'ad.updated',
            'ad.submitted',
            'ad.cijena',
            'ad.valuta_id',
            'ad.views',
            'ad.inet_objavi',
            'ad.pbl_date',
            'ad.region',
            'ad.youtube',
            'ad.avus_id',
            'ar.picture_changed',
            'adr.expires AS adr_expires',
            'adr.pbl_date AS adr_pbl_date',
            'adr.search_text AS adr_search_text',
            'adr.sa_photo AS adr_sa_photo',
            'adr.sa_photo2 AS adr_sa_photo2'
        );

        $ads_sql_query = 'SELECT ' . implode(', ', $fields) . ' FROM ads AS ad INNER JOIN ads_refresh AS ar ON ad.id = ar.id LEFT JOIN ads_read AS adr ON ad.id = adr.id ORDER BY ad.id ASC LIMIT 0,' . $this->sql_limit;
        $ads_remaining = 'SELECT COUNT(*) AS total FROM ads AS ad INNER JOIN ads_refresh AS ar ON ad.id = ar.id LEFT JOIN ads_read AS adr ON ad.id = adr.id';
//        $ads_sql_query = 'SELECT ' . implode(', ', $fields) . ' FROM ads AS ad INNER JOIN ads_refresh AS ar ON ad.id = ar.id AND ar.refreshed IS NULL LEFT JOIN ads_read AS adr ON ad.id = adr.id ORDER BY ad.id ASC LIMIT 0,' . $this->sql_limit;
//        $ads_remaining = 'SELECT COUNT(*) AS total FROM ads AS ad INNER JOIN ads_refresh AS ar ON ad.id = ar.id AND ar.refreshed IS NULL LEFT JOIN ads_read AS adr ON ad.id = adr.id';

        $ads_sql_param = array();

        $this->add2Log('--------------------------------------------------------');
        $printable_query = $ads_sql_query;
        foreach ($ads_sql_param as $key => $val) {
            $printable_query = str_ireplace(':' . $key, $val, $printable_query);
        }
        $this->add2Log($printable_query);
        $this->add2Log('--------------------------------------------------------');

        // count remaining ads for import...
        $this->remaining = (int)$this->source_db->findFirst($ads_remaining, $ads_sql_param, true);
        $src_ads = $this->source_db->find($ads_sql_query, $ads_sql_param);

        $found_ads = $src_ads && count($src_ads) ? count($src_ads) : 0;
        if ($found_ads) {
            $this->add2Log('Refreshing ' . $found_ads . ' ad' . ($found_ads > 1 ? 's' : '') . ($this->remaining ? ' out of ' . $this->remaining . ' remaining!' : ''));
            $this->add2Log('--------------------------------------------------------');

            foreach ($src_ads as $src_ad) {
                $trg_ad = null;
                if ($src_ad) {
                    $this->last_import_id = $src_ad['id'];
                    $trg_ad = $this->target_db->findFirst(
                        'SELECT * FROM ads WHERE import_id = :import_id LIMIT 0, 1',
                        array(
                            'import_id' => $src_ad['id']
                        )
                    );

                    if ($trg_ad) {
                        if ($src_ad['adr_expires'] !== null) {
                            $src_ad['pictures'] = null;
                            if (intval($src_ad['picture_changed']) == 1) {
                                $src_ad_pictures = $this->source_db->find(
                                    'SELECT * FROM ads_photos WHERE ad_id = :ad_id',
                                    array(
                                        'ad_id' => intval($src_ad['id'])
                                    )
                                );
                                if ($src_ad_pictures) {
                                    $src_ad['pictures'] = $src_ad_pictures;
                                }
                                if (trim($src_ad['adr_sa_photo']) || trim($src_ad['adr_sa_photo2'])) {
                                    if (trim($src_ad['adr_sa_photo'])) {
                                        if (!$src_ad['pictures']) {
                                            $src_ad['pictures'] = array();
                                        }
                                        $sa_photo = str_replace('_sa.', '.', trim($src_ad['adr_sa_photo']));
                                        $src_ad['pictures'][] = array('id' => 1, 'photo' => $sa_photo);
                                    }
                                    if (trim($src_ad['adr_sa_photo2'])) {
                                        if (!$src_ad['pictures']) {
                                            $src_ad['pictures'] = array();
                                        }
                                        $sa_photo2 = str_replace('_sa.', '.', trim($src_ad['adr_sa_photo2']));
                                        $src_ad['pictures'][] = array('id' => 2, 'photo' => $sa_photo2);
                                    }
                                }
                            }

                            if ($ad_data = $this->refreshAd($trg_ad, $src_ad)) {
                                $trg_ad   = $ad_data['ad'];
                                $pictures = $ad_data['pictures'];

                                $this->counters['imported']++;
                                $this->counters_total['imported']++;
                                $ad_pics = '';
                                if ($ads_pics = count($src_ad['pictures'])) {
                                    $ad_pics = ' + ' . $pictures . '/' . $ads_pics . ' pic(s)';
                                }

                                $this->add2Log('Refreshed ad ' . $src_ad['id'] . ' -> ' . $trg_ad['id'] . $ad_pics);
                            } else {
                                $this->counters['error']++;
                                $this->counters_total['error']++;
                                $this->add2Log('Error refreshing ad ' . $src_ad['id']);
                                $this->add2Log(print_r(array(
                                    'msg'    => 'Refresh Error',
                                    'src_ad' => $src_ad,
                                    'trg_ad' => $trg_ad
                                )), 'error');
                            }
                        } else {
                            $updated = $this->target_db->update(
                                'UPDATE ads SET expires_at = :expires_at WHERE id = :id',
                                array(
                                    'id'         => $trg_ad['id'],
                                    'expires_at' => $this->current_timestamp
                                )
                            );
                            if ($updated) {
                                $this->counters['expired']++;
                                $this->counters_total['expired']++;
                                $this->add2Log('Refresh ad ID:' . $src_ad['id'] . ' expired in target DB');
                            } else {
                                $this->counters['error']++;
                                $this->counters_total['error']++;
                                $this->add2Log('Refresh ad ID:' . $src_ad['id'] . ' could not be expired in target DB');
                            }
                        }
                    } else {
                        $this->counters['error']++;
                        $this->counters_total['error']++;
                        $this->add2Log('Refresh ad ID:' . $src_ad['id'] . ' could not be found in target DB');
                    }
                    // Delete the row from ads_refresh table
                    $src_deleted = $this->source_db->update(
//                        'UPDATE ads_refresh SET refreshed = 1 WHERE id = :id',
                        'DELETE FROM ads_refresh WHERE id = :id',
                        array(
                            'id' => $src_ad['id']
                        )
                    );
                    $this->processed++;
                    $this->remaining--;
                }
            }
        }

        if ($this->remaining > 0) {
            $this->add2Log('--------------------------------------------------------');
            $this->add2Log('-- RESTARTING REFRESH TASK -> ' . $this->remaining . ' ads remaining!');
            $this->add2Log('--------------------------------------------------------');
            $this->sendMail();
            sleep(1);

            \pcntl_exec($this->php_path, $this->get_run_argv());
        }

        return $this->counters_total;
    }

    public static function refreshShopsAds($in_params = array())
    {
        $result = null;

        $options = array();
        if (!empty($in_params)) {
            foreach ($in_params as $param) {
                if (strpos($param, '=')) {
                    $param_arr = explode('=', $param);
                    $options[(string)trim($param_arr[0])] = (string)trim($param_arr[1]);
                }
            }
        }

        $migration = new \Baseapp\Library\Mapping\ShopRefresher($options);

        $ads = $migration->refreshAds();
        if ($ads) {
            $cummulative_counters = array();
            if (isset($ads['imported']) && intval($ads['imported'])) {
                $cummulative_counters[] = 'Succesfull: ' . intval($ads['imported']);
            }
            if (isset($ads['expired']) && intval($ads['expired'])) {
                $cummulative_counters[] = 'Expired: ' . intval($ads['expired']);
            }
            if (isset($ads['error']) && intval($ads['error'])) {
                $cummulative_counters[] = 'Errored: ' . intval($ads['error']);
            }

            if (!empty($cummulative_counters)) {
                $result = implode('; ', $cummulative_counters);
            }
        }

        return $result;
    }

}
