<?php

namespace Baseapp\Library\Parameters;

use Baseapp\Models\Locations as Location;
use Phalcon\Di;

class LocationParameter extends BaseParameter
{
    protected $group_type = 'standard';
    protected $settings_location = 'levels';
    protected $type = 'LOCATION';
    protected $accept_dictionary = false;
    protected $can_default = true;
    protected $can_default_type = 'dropdown';
    protected $can_other = false;
    protected $can_prefix_suffix = false;
    protected $is_dependable = true;
    protected $is_required = true;
    public $is_searchable = true;
    public $is_searchable_type = array('DEPENDABLE_DROPDOWN');

    public function setValue($values)
    {
        if (is_array($values)) {
            $json_data = array();
            foreach ($values as $level => $value) {
                $json_level_data = array(
                    'value' => $value
                );
                if ($level !== 'latlng') {
                    $location = Location::findFirst($value);
                    if ($location) {
                        $json_level_data['text_value'] = $location->name;
                    }
                }
                $json_data[$level] = $json_level_data;
            }

            if (count($json_data)) {
                $this->setPreparedDataJson(
                    'location',
                    $json_data
                );

                $this->ad->country_id      = 0;
                $this->ad->county_id       = 0;
                $this->ad->city_id         = 0;
                $this->ad->municipality_id = 0;

                if (isset($json_data['country_id']['value']) && !empty($json_data['country_id']['value'])) {
                    $this->ad->country_id = (int) $json_data['country_id']['value'];
                }
                if (isset($json_data['county_id']['value']) && !empty($json_data['county_id']['value'])) {
                    $this->ad->county_id = (int) $json_data['county_id']['value'];
                }
                if (isset($json_data['city_id']['value']) && !empty($json_data['city_id']['value'])) {
                    $this->ad->city_id = (int) $json_data['city_id']['value'];
                }
                if (isset($json_data['municipality_id']['value']) && !empty($json_data['municipality_id']['value'])) {
                    $this->ad->municipality_id = (int) $json_data['municipality_id']['value'];
                }

                $this->ad->lat = null;
                $this->ad->lng = null;
                if (isset($json_data['latlng'])) {
                    $tmp_point_lat_lng = explode(',', trim($json_data['latlng']['value']));
                    if ($tmp_point_lat_lng && count($tmp_point_lat_lng) == 2) {
                        $this->ad->lat = $tmp_point_lat_lng[0];
                        $this->ad->lng = $tmp_point_lat_lng[1];
                    }
                }
            }
        }
    }

    public function process()
    {
        $curr_level = 1;

        $json_data = array();
        // we can have a situaction where user selects the value on one level, and there are no more
        // levels after it, in that case we have to remove all 'required' validations on those levels
        // ----
        // the simplest way is to put a global tracker which will remove all level validations after
        // it has its' value 'true'
        $remove_child_validations = false;
        $last_level_location_id = null;

        foreach ($this->parameter_settings as $level) {
            $db_column_name = Location::getDBColumnName($curr_level);
            if ($db_column_name) {
                if ($db_column_name !== 'latlng') {
                    $verify_if_needed = false;
                    $verify_locations_children = false;
                    if (isset($this->data['ad_location_' . $curr_level])) {
                        $curr_param_level_value = intval(trim($this->data['ad_location_' . $curr_level]));

                        if ($curr_param_level_value > 0) {
                            if (!$remove_child_validations) {
                                $json_level_data = array(
                                    'value' => $curr_param_level_value
                                );
                                $location = Location::findFirst($curr_param_level_value);
                                if ($location) {
                                    $last_level_location_id = $curr_param_level_value;
                                    $json_level_data['text_value'] = $location->name;
                                    // verify that this level has children
                                    if (!$location->hasChildren()) {
                                        $remove_child_validations = true;
                                    }
                                }
                                $json_data[$db_column_name] = $json_level_data;
                            }
                            $verify_if_needed = true;
                        } else {
                            $verify_locations_children = true;
                        }
                    } else {
                        // the post was not set for this level... most probably the control was disabled
                        // via javascript and it means that there are no levels in locations but, we have to check
                        $verify_locations_children = true;
                    }
                    if ($verify_locations_children) {
                        $location = Location::findFirst($last_level_location_id);
                        if ($location) {
                            if (!$location->hasChildren()) {
                                $remove_child_validations = true;
                            } else {
                                $verify_if_needed = true;
                            }
                        }
                    }
                    if ($verify_if_needed && $level->is_required) {
                        $this->validation->add('ad_location_' . $curr_level, new \Phalcon\Validation\Validator\PresenceOf(array(
                            'message' => '\'' . $level->label_text . '\' ' . $this->getValidationString('cannot_be_empty')
                        )));
                    }
                } else {
                    if (isset($this->data['ad_location_' . $curr_level])) {
                        $curr_param_level_value = trim($this->data['ad_location_' . $curr_level]);
                        $json_level_data = array(
                            'value' => $curr_param_level_value
                        );
                        $json_data[$db_column_name] = $json_level_data;
                    }

                    if ($level->is_required) {
                        $this->validation->add('ad_location_' . $curr_level, new \Phalcon\Validation\Validator\PresenceOf(array(
                            'message' => '\'' . $level->label_text . '\' ' . $this->getValidationString('location_should_be_defined')
                        )));
                    }
                }
            }
            $curr_level++;
        }

        if (count($json_data)) {
            $this->setPreparedDataJson(
                'location',
                $json_data
            );

            $this->ad->country_id = isset($json_data['country_id']['value']) ? (int) $json_data['country_id']['value'] : 0;
            $this->ad->county_id = isset($json_data['county_id']['value']) ? (int) $json_data['county_id']['value'] : 0;
            $this->ad->city_id = isset($json_data['city_id']['value']) ? (int) $json_data['city_id']['value'] : 0;
            $this->ad->municipality_id = isset($json_data['municipality_id']['value']) ? (int) $json_data['municipality_id']['value'] : 0;

            $this->ad->lat = null;
            $this->ad->lng = null;
            if (isset($json_data['latlng'])) {
                $tmp_point_lat_lng = explode(',', trim($json_data['latlng']['value']));
                if ($tmp_point_lat_lng && count($tmp_point_lat_lng) == 2) {
                    $this->ad->lat = $tmp_point_lat_lng[0];
                    $this->ad->lng = $tmp_point_lat_lng[1];
                }
            }
        }
    }

    public function render_input()
    {
        parent::render_input();

        $render_data = array(
            'row'  => true,
            'html' => '',
            'js'   => ''
        );

        if (!$this->is_hidden) {
            if ($this->parameter_settings) {
                $max_location_level = count($this->parameter_settings);
                $first_location_parameter_id = null;
                if ($max_location_level) {
                    $curr_level = 1;
                    $render_data['js'] = <<<JS
    promise_parameter_location = [];

    function parameter_location_handle_needed_levels(last_needed_level) {
        var max_location_level = $max_location_level;
        for (var i = 1; i <= max_location_level; i++) {
            var \$current_level_select = \$('#parameter_location_level_' + i);
            if ('undefined' !== typeof \$current_level_select && 1 === \$current_level_select.length) {
                var current_level_type = \$current_level_select.data('type');
                var \$current_level_form_group = \$current_level_select.closest('.form-group');
                if (i <= last_needed_level) {
                    \$current_level_form_group.show();
                    \$current_level_select.prop('disabled', false)
                    if (current_level_type !== 'point') {
                        \$current_level_select.selectpicker('refresh');
                    }
                } else {
                    \$current_level_form_group.hide();
                    if (current_level_type !== 'point') {
                        \$current_level_select.prop('disabled', true).selectpicker('refresh');
                    }
                }
            }
        }
    }

    function parameter_location_onChange(\$source, \$target) {
        if ('undefined' != typeof \$source.val()) {
            var last_location_needed_level = parseInt(\$source.attr('id').replace('parameter_location_level_', ''));
            if ('' != \$.trim(\$source.val())) {
                var \$selected_option = \$source.find(':selected');
                if (\$target.data('type') !== 'point') {
                    promise_parameter_location[\$source.attr('id')] = \$.ajax({
                        url: '/ajax/location/' + \$source.val() + '/values',
                        dataType: 'json',
                        success: function(data) {
                            \$target.find('option').remove();
                            if (data.length) {
                                last_location_needed_level++;
                                \$target.append(
                                    \$('<option/>').attr('value', '').text('')
                                );
                                \$.each(data, function(idx, data){
                                    \$target.append(
                                        \$('<option/>').attr('value', data.id).text(data.name).data('lat', data.lat).data('lng', data.lng).data('zoom', data.zoom)
                                    );
                                });
                                \$target.selectpicker('refresh');
                            }
                            parameter_location_handle_needed_levels(last_location_needed_level);
                        }
                    });
                } else {
                    last_location_needed_level++;
                    parameter_location_handle_needed_levels(last_location_needed_level);
                    var \$google_map_container = \$('#' + \$target.attr('id') + '_map');
                    if ('undefined' !== typeof \$google_map_container) {
                        \$google_map_container.attr('data-lat', \$selected_option.data('lat')).data('lat', \$selected_option.data('lat'));
                        \$google_map_container.attr('data-lng', \$selected_option.data('lng')).data('lng', \$selected_option.data('lng'));
                        \$google_map_container.attr('data-zoom', \$selected_option.data('zoom')).data('zoom', \$selected_option.data('zoom'));
                        \$target.closest('.form-group').show('fast', function(){
                            parameter_location_initMap();
                        });
                    }
                }
                if ('undefined' !== typeof parameter_location_map && parameter_location_map) {
                    parameter_location_setCoords(\$selected_option.data('lat'), \$selected_option.data('lng'));
                    parameter_location_map.changeView(
                        \$selected_option.data('lat'),
                        \$selected_option.data('lng'),
                        \$selected_option.data('zoom')
                    );
                }
            } else {
                \$target.selectpicker('refresh');
                parameter_location_handle_needed_levels(last_location_needed_level);
            }
        } else {
            \$target.selectpicker('refresh');
        }
    }
JS;
                    $prev_level = null;
                    $all_dropdowns = array();
                    $curr_location = new Location();
                    foreach ($this->parameter_settings as $level_i => $level) {
                        $parameter_level_id = 'parameter_location_level_' . $curr_level;
                        $parameter_level_name = 'ad_location_' . $curr_level;
                        if ($level_i < 4) {
                            if (isset($this->data)) {
                                $level_selected_value = isset($this->data[$parameter_level_name]) ? (is_numeric($this->data[$parameter_level_name]) ? intval($this->data[$parameter_level_name]) : trim($this->data[$parameter_level_name])) : null;
                                if (!$level_selected_value && 1 === $curr_level) {
                                    $level_selected_value = $level->default_value ? intval($level->default_value) : null;
                                }
                            } else {
                                $level_selected_value = $level->default_value ? intval($level->default_value) : null;
                            }

                            if (1 === $level->is_hidden) {
                                $render_data['html'] .= '<input type="hidden" name="' . $parameter_level_name . '" id="' . $parameter_level_id . '" value="' . ($level_selected_value ? $level_selected_value : '') . '" />' . PHP_EOL;
                            } else {
                                if (null === $first_location_parameter_id) {
                                    $first_location_parameter_id = $parameter_level_id;
                                }
                                $all_dropdowns[] = '#'.$parameter_level_id;
                                $required_html = '';
                                if ($level->is_required) {
                                    $required_html = ' <abbr title="' . $this->getValidationString('required_field') . '">*</abbr>';
                                }

                                if ($curr_location) {
                                    $location_level_values = Location::getCachedChildren($curr_location->id);
                                    $form_group_style = count($location_level_values) ? '' : ' style="display:none;"';
                                } else {
                                    $location_level_values = null;
                                    $form_group_style = ' style="display:none;"';
                                }

                                $div_class = $this->render_as_full_row ? 'col-lg-12' : 'col-lg-3 col-md-3';
                                $render_data['html'] .= '<div class="' . $div_class . '">' . PHP_EOL;
                                $render_data['html'] .= '    <div class="form-group' . (isset($this->errors) && $this->errors->filter($parameter_level_name) ? ' has-error' : '') . '"' . $form_group_style . '>' . PHP_EOL;
                                $render_data['html'] .= '        <label class="control-label" for="' . $parameter_level_id . '">' . trim($level->label_text) . $required_html . '</label>' . PHP_EOL;
                                $render_data['html'] .= '        <div class="icon_dropdown">' . PHP_EOL;
                                $render_data['html'] .= '            <select class="form-control show-tick" name="' . $parameter_level_name . '" id="' . $parameter_level_id . '" data-type="select" data-level-id="' . Location::getDBColumnName($curr_level) . '">' . PHP_EOL;
                                // for loop begin
                                if ($curr_location) {
                                    if ($location_level_values) {
                                        $render_data['html'] .= '                <option value=""></option>' . PHP_EOL;
                                        foreach ($location_level_values as $value) {
                                            $selected_option = $level_selected_value && $level_selected_value == intval($value->id) ? ' selected="selected"' : '';
                                            $render_data['html'] .= '                <option value="' . ((int) $value->id) . '"' . $selected_option . ' data-lat="' . $value->lat . '" data-lng="' . $value->lng . '" data-zoom="' . ((int) $value->zoom) . '">' . trim($value->name) . '</option>' . PHP_EOL;
                                        }
                                    }
                                }
                                // for loop end;
                                $render_data['html'] .= '            </select>' . PHP_EOL;
                                $render_data['html'] .= '        </div>' . PHP_EOL;

                                if (isset($this->errors) && $this->errors->filter($parameter_level_name)) {
                                    $render_data['html'] .= '        <p class="help-block">' . current($this->errors->filter($parameter_level_name))->getMessage() . '</p>' . PHP_EOL;
                                } elseif (isset($level->help_text) && trim($level->help_text)) {
                                    $render_data['html'] .= '        <p class="help-block">' . trim($level->help_text) . '</p>' . PHP_EOL;
                                }

                                $render_data['html'] .= '    </div>' . PHP_EOL;
                                $render_data['html'] .= '</div>' . PHP_EOL;
                            }
                        } else {
                            $autoInitMap = false;
                            $selected_point = array(
                                'lat' => 0,
                                'lng' => 0
                            );
                            $zoom_level = 8;

                            /**
                             * If user selects all the possible levels up to the MAP level, we have to see if the last level
                             * before the map has been selected or not. If it was, it might be that the actual POINT was set
                             * also. If the POINT was not set, we have to check if the last level was really set or not. In
                             * case it wasn't set, then we should hide the map. If it was set, then first, we have to see if
                             * POINT was set and use those coordinates for the initial map, otherwise, we have to set the
                             * coordinates of the last level.
                             */
                            if (isset($this->data) && isset($this->data['ad_location_4']) && empty($this->data['ad_location_4'])) {
                                $form_group_style = ' style="display:none;"';
                            } else {
                                if (isset($this->data)) {
                                    $level_selected_value = isset($this->data[$parameter_level_name]) && trim($this->data[$parameter_level_name]) ? trim($this->data[$parameter_level_name]) : null;
                                    if ($level_selected_value) {
                                        $tmp_point = explode(',', $level_selected_value);
                                        if (count($tmp_point) == 2) {
                                            $selected_point = array(
                                                'lat' => $tmp_point[0],
                                                'lng' => $tmp_point[1]
                                            );
                                            $zoom_level = 18;
                                            $autoInitMap = true;
                                        }
                                    }
                                }

                                // fallback if POINT was not set - we use last level's coordinates to initialize the map
                                if ($selected_point['lat'] == 0 && $selected_point['lng'] == 0 && isset($this->data['ad_location_4']) && !empty($this->data['ad_location_4'])) {
                                    $last_location = Location::findFirst($this->data['ad_location_4']);
                                    if ($last_location) {
                                        $selected_point = array(
                                            'lat' => $last_location->lat,
                                            'lng' => $last_location->lng
                                        );
                                        $zoom_level = $last_location->zoom;
                                        $autoInitMap = true;
                                    }
                                }
                            }

                            $parameter_level_map_id = $parameter_level_id . '_map';
                            $render_data['html'] .= '<div class="col-lg-12 col-md-12">' . PHP_EOL;
                            $render_data['html'] .= '    <div class="form-group' . (isset($this->errors) && $this->errors->filter($parameter_level_name) ? ' has-error' : '') . '"' . $form_group_style . '>' . PHP_EOL;
                            $render_data['html'] .= '        <label class="control-label">' . trim($level->label_text) . $required_html . '</label>' . PHP_EOL;
                            $render_data['html'] .= '        <input type="hidden" name="' . $parameter_level_name . '" id="' . $parameter_level_id . '" value="" data-type="point">' . PHP_EOL;
                            $render_data['html'] .= '        <div' . PHP_EOL;
                            $render_data['html'] .= '            id="' . $parameter_level_map_id . '"' . PHP_EOL;
                            $render_data['html'] .= '            data-zoom="' . $zoom_level . '"' . PHP_EOL;
                            $render_data['html'] .= '            data-lat="' . $selected_point['lat'] . '"' . PHP_EOL;
                            $render_data['html'] .= '            data-lng="' . $selected_point['lng'] . '"' . PHP_EOL;
                            $render_data['html'] .= '            style="width:100%;height:500px"' . PHP_EOL;
                            $render_data['html'] .= '        ><!--IE--></div>' . PHP_EOL;

                            if (isset($this->errors) && $this->errors->filter($parameter_level_name)) {
                                $render_data['html'] .= '        <p class="help-block">' . current($this->errors->filter($parameter_level_name))->getMessage() . '</p>' . PHP_EOL;
                            } elseif (isset($level->help_text) && trim($level->help_text)) {
                                $render_data['html'] .= '        <p class="help-block">' . trim($level->help_text) . '</p>' . PHP_EOL;
                            }

                            $render_data['html'] .= '    </div>' . PHP_EOL;
                            $render_data['html'] .= '</div>' . PHP_EOL;

                            // TODO/FIXME:
                            // Do we have to go through Di?
                            // Can't this be passed in or taken from some existing property/object already used somewhere?
                            $maps_api_key = '';
                            $config = Di::getDefault()->get('config');
                            if ($config) {
                                $maps_api_key = $config->google->maps->browserKey;
                            }

                            // add needed assets for google maps
                            $render_data['assets'] = array(
                                'css' => array(),
                                'js'  => array(
                                    //'/maps.google.com/maps/api/js?sensor=false',
                                    '//maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&language=hr&key=' . $maps_api_key,
                                    'assets/js/google-map-wrapper.js'
                                )
                            );
                            $render_data['js'] .= <<<JS

    var parameter_location_map = null;

    function parameter_location_setCoords(lat, lng) {
        \$('#$parameter_level_id').val(lat + ',' + lng);
    }

    function parameter_location_initMap() {
        if (!parameter_location_map) {
            var map_lat = \$('#$parameter_level_map_id').data('lat');
            var map_lng = \$('#$parameter_level_map_id').data('lng');
            if (map_lat != '0' && map_lng != '0') {
                parameter_location_setCoords(map_lat, map_lng);
            }
            parameter_location_map = \$('#$parameter_level_map_id').initGoogleMap({
                'input_field': '$parameter_level_id',
                'drag_marker': true
            });
        }
    }
JS;
                            if ($autoInitMap) {
                            $render_data['js'] .= <<<JS

    parameter_location_map = \$('#$parameter_level_map_id').initGoogleMap({
        'input_field': '$parameter_level_id',
        'drag_marker': true,
        'add_marker': true
    });
JS;
                            }
                        }

                        if ($prev_level) {
                            $prev_parameter_level_id = 'parameter_location_level_' . $prev_level;
                            $render_data['js'] .= <<<JS

    \$('#$prev_parameter_level_id').change(function(){
        parameter_location_onChange(\$(this), \$('#$parameter_level_id'));
    });
JS;
                        }

                        $prev_level = $curr_level;

                        // if we have something selected in current level, then, in next iteration we will fetch currently
                        // selected level's value children
                        $curr_location = null;
                        if ($level_selected_value && (string) intval($level_selected_value) === (string) $level_selected_value) {
                            $curr_location = Location::findFirst($level_selected_value);
                        }

                        $curr_level++;
                    }
                    if (count($all_dropdowns)) {
                        $all_dropdowns = implode(', ', $all_dropdowns);

                        $style = 'btn btn-default';
                        $noneSelectedText = $this->getValidationString('select_something');
                        $noneResultsText  = $this->getValidationString('no_values_found');

                        $render_data['js'] .= <<<JS

    \$('$all_dropdowns').selectpicker({
        style: '$style',
        size: 8,
        noneSelectedText: '$noneSelectedText',
        liveSearch: true,
        noneResultsText: '$noneResultsText',
        mobile: OGL.is_mobile_browser,
        iconBase: 'fa',
        tickIcon: 'fa-check text-primary'
    });
JS;
                    }
                }
            }
        }

        return $render_data;
    }

}
