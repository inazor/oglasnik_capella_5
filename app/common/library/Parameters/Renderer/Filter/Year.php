<?php

namespace Baseapp\Library\Parameters\Renderer\Filter;

use Baseapp\Library\Parameters\Renderer\Filter;

class Year extends Filter
{

    public function getMarkup()
    {
        $markup_data = array(
            'html'        => '',
            'js'          => '',
            'filter'      => null,
            'email_agent' => false
        );

        if (!$this->is_hidden) {
            $parameter_id   = 'parameter_' . $this->parameter->id;
            $parameter_name = 'ad_params_' . $this->parameter->id;

            $parameter_value = array(
                'from' => null,
                'to'   => null
            );
            if (isset($this->data)) {
                $tmp_filters = array();
                $from        = null;
                $to          = null;

                // first we collect data to temp variables as we don't know whether $from will really be smaller number than $to
                if (isset($this->data[$parameter_name . '_from']) && trim($this->data[$parameter_name . '_from']) && intval($this->data[$parameter_name . '_from'])) {
                    $from = intval($this->data[$parameter_name . '_from']);
                }
                if (isset($this->data[$parameter_name . '_to']) && trim($this->data[$parameter_name . '_to']) && intval($this->data[$parameter_name . '_to'])) {
                    $to = intval($this->data[$parameter_name . '_to']);
                }
                $model_alias = 'ap_' . $this->parameter->id;
                // test if we have at least one variable set... in case we have, we will will need to add first condition
                // (it will be parameter_id)
                if ($from || $to) {
                    $markup_data['email_agent'] = true;
                    $tmp_filters[] = array(
                        'condition' => '['.$model_alias.'].parameter_id = :parameter_' . $this->parameter->id .'_id:',
                        'params' => array(
                            'parameter_' . $this->parameter->id . '_id' => $this->parameter->id,
                        ),
                        'types' => array(
                            'parameter_' . $this->parameter->id . '_id' => \PDO::PARAM_INT
                        )
                    );
                    if ($from && $to) {
                        // if both variables have been set, we have to map right values to right positions...
                        // $from <= $to
                        $parameter_value['from'] = min($from, $to);
                        $parameter_value['to'] = max($from, $to);

                        if ($from == $to) {
                            $tmp_filters[] = array(
                                'condition' => '['.$model_alias . '].value = :parameter_' . $this->parameter->id . '_value:',
                                'params' => array(
                                    'parameter_' . $this->parameter->id . '_value' => $parameter_value['from'],
                                ),
                                'types' => array(
                                    'parameter_' . $this->parameter->id . '_value' => \PDO::PARAM_INT
                                )
                            );
                        } else {
                            $tmp_filters[] = array(
                                'condition' => '(['.$model_alias . '].value BETWEEN :parameter_' . $this->parameter->id . '_from_value: AND :parameter_' . $this->parameter->id . '_to_value:)',
                                'params' => array(
                                    'parameter_' . $this->parameter->id . '_from_value' => $parameter_value['from'],
                                    'parameter_' . $this->parameter->id . '_to_value' => $parameter_value['to']
                                ),
                                'types' => array(
                                    'parameter_' . $this->parameter->id . '_from_value' => \PDO::PARAM_INT,
                                    'parameter_' . $this->parameter->id . '_to_value' => \PDO::PARAM_INT
                                )
                            );
                        }
                    } else {
                        // we're not sure which value is set, so we test both of them
                        if ($from) {
                            $parameter_value['from'] = $from;
                            $tmp_filters[] = array(
                                'condition' => '['.$model_alias . '].value >= :parameter_' . $this->parameter->id . '_from_value:',
                                'params' => array(
                                    'parameter_' . $this->parameter->id . '_from_value' => $parameter_value['from']
                                ),
                                'types' => array(
                                    'parameter_' . $this->parameter->id . '_from_value' => \PDO::PARAM_INT
                                )
                            );
                        }
                        if ($to) {
                            $parameter_value['to'] = $to;
                            $tmp_filters[] = array(
                                'condition' => '['.$model_alias . '].value <= :parameter_' . $this->parameter->id . '_to_value:',
                                'params' => array(
                                    'parameter_' . $this->parameter->id . '_to_value' => $parameter_value['to']
                                ),
                                'types' => array(
                                    'parameter_' . $this->parameter->id . '_to_value' => \PDO::PARAM_INT
                                )
                            );
                        }
                    }
                }

                if ($combined_filter = $this->render_filter_combine($tmp_filters)) {
                    $markup_data['filter'] = array(
                        'model' => 'Baseapp\Models\AdsParameters',
                        'alias' => $model_alias,
                        'filter' => $combined_filter
                    );
                }

            }

            $curr_time = time();
            if ('Year' === $this->parameter_settings->interval_begin_period) {
                $starting_time = mktime(0, 0, 0, date('n', $curr_time), 1, (date('Y', $curr_time) + $this->parameter_settings->interval_begin_value));
            } elseif ('Month' === $this->parameter_settings->interval_begin_period) {
                $starting_time = mktime(0, 0, 0, (date('n', $curr_time) + $this->parameter_settings->interval_begin_value), 1, date('Y', $curr_time));
            } elseif ('Number' === $this->parameter_settings->interval_begin_period) {
                $starting_time = mktime(0, 0, 0, date('n', $curr_time), 1, $this->parameter_settings->interval_begin_value);
            }
            if ('Year' === $this->parameter_settings->interval_end_period) {
                $ending_time = mktime(0, 0, 0, date('n', $curr_time), 1, (date('Y', $curr_time) + $this->parameter_settings->interval_end_value));
            } elseif ('Month' === $this->parameter_settings->interval_end_period) {
                $ending_time = mktime(0, 0, 0, (date('n', $curr_time) + $this->parameter_settings->interval_end_value), 1, date('Y', $curr_time));
            } elseif ('Number' === $this->parameter_settings->interval_end_period) {
                $ending_time = mktime(0, 0, 0, date('n', $curr_time), 1, $this->parameter_settings->interval_end_value);
            }

            $starting_year = date('Y', $starting_time);
            $ending_year = date('Y', $ending_time);
            $all_dropdowns = array(
                '#'.$parameter_id.'_from',
                '#'.$parameter_id.'_to'
            );

            $filter_label = $this->parameter_settings->label_text;
            if (isset($this->parameter_settings->is_searchable_options->label_text) && !empty($this->parameter_settings->is_searchable_options->label_text)) {
                $filter_label = trim($this->parameter_settings->is_searchable_options->label_text);
            }

            $markup_data['html'] .= '<h3>' . $filter_label . '</h3>' . PHP_EOL;

            $markup_data['html'] .= '<div class="form-group">' . PHP_EOL;
            $markup_data['html'] .= '    <div class="input-group">' . PHP_EOL;
            $markup_data['html'] .= '        <span class="input-group-addon">Od</span>' . PHP_EOL;
            $markup_data['html'] .= '        <select class="form-control" id="' . $parameter_id .'_from" name="' . $parameter_name .'_from" data-type="select" data-default="" data-bit-name="' . $filter_label . ' od">' . PHP_EOL;
            $markup_data['html'] .= '            <option value=""></option>' . PHP_EOL;
            // for loop begin
            if ($this->parameter_settings->is_reverse) {
                for ($curr_year = $ending_year; $curr_year >= $starting_year; $curr_year--) {
                    $selected_option = $parameter_value['from'] && intval($parameter_value['from']) == intval($curr_year) ? ' selected="selected"' : '';
                    $markup_data['html'] .= '            <option value="' . ((int) $curr_year) . '"' . $selected_option . '>' . trim($curr_year) . '</option>' . PHP_EOL;
                }
            } else {
                for ($curr_year = $starting_year; $curr_year <= $ending_year; $curr_year++) {
                    $selected_option = $parameter_value['from'] && intval($parameter_value['from']) == intval($curr_year) ? ' selected="selected"' : '';
                    $markup_data['html'] .= '                    <option value="' . ((int) $curr_year) . '"' . $selected_option . '>' . trim($curr_year) . '</option>' . PHP_EOL;
                }
            }
            $markup_data['html'] .= '        </select>' . PHP_EOL;
            $markup_data['html'] .= '    </div>' . PHP_EOL;
            $markup_data['html'] .= '</div>' . PHP_EOL;
            $markup_data['html'] .= '<div class="form-group">' . PHP_EOL;
            $markup_data['html'] .= '    <div class="input-group">' . PHP_EOL;
            $markup_data['html'] .= '        <span class="input-group-addon">Do</span>' . PHP_EOL;
            $markup_data['html'] .= '        <select class="form-control" id="' . $parameter_id .'_to" name="' . $parameter_name .'_to" data-type="select" data-default="" data-bit-name="' . $filter_label . ' do">' . PHP_EOL;
            $markup_data['html'] .= '            <option value=""></option>' . PHP_EOL;
            // for loop begin
            if ($this->parameter_settings->is_reverse) {
                for ($curr_year = $ending_year; $curr_year >= $starting_year; $curr_year--) {
                    $selected_option = $parameter_value['to'] && intval($parameter_value['to']) == intval($curr_year) ? ' selected="selected"' : '';
                    $markup_data['html'] .= '            <option value="' . ((int) $curr_year) . '"' . $selected_option . '>' . trim($curr_year) . '</option>' . PHP_EOL;
                }
            } else {
                for ($curr_year = $starting_year; $curr_year <= $ending_year; $curr_year++) {
                    $selected_option = $parameter_value['to'] && intval($parameter_value['to']) == intval($curr_year) ? ' selected="selected"' : '';
                    $markup_data['html'] .= '            <option value="' . ((int) $curr_year) . '"' . $selected_option . '>' . trim($curr_year) . '</option>' . PHP_EOL;
                }
            }
            $markup_data['html'] .= '        </select>' . PHP_EOL;
            $markup_data['html'] .= '    </div>' . PHP_EOL;
            $markup_data['html'] .= '</div>' . PHP_EOL;

            if (count($all_dropdowns)) {
                $all_dropdowns = implode(', ', $all_dropdowns);
                $markup_data['js'] .= <<<JS

    \$('$all_dropdowns').selectpicker({
        style: 'btn btn-default',
        size: 8,
        noneSelectedText: '',
        liveSearch: true,
        noneResultsText: '',
        mobile: OGL.is_mobile_browser,
        iconBase: 'fa',
        tickIcon: 'fa-check text-primary',
        dropupAuto: false
    });
JS;
            }
        }

        return $markup_data;
    }

}
