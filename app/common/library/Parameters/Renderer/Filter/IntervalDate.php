<?php

namespace Baseapp\Library\Parameters\Renderer\Filter;

use Baseapp\Library\Parameters\Renderer\Filter;

class IntervalDate extends Filter
{

    public function getMarkup()
    {
        $markup_data = array(
            'html'        => '',
            'js'          => '',
            'filter'      => null,
            'email_agent' => false
        );

        if (!$this->is_hidden) {
            $markup_data['assets'] = array(
                'css' => array(
                    'assets/vendor/datepicker3.css'
                ),
                'js'  => array(
                    'assets/vendor/bootstrap-datepicker.js',
                    'assets/vendor/bootstrap-datepicker.hr.js'
                )
            );

            $parameter_id         = 'parameter_' . $this->parameter->id;
            $parameter_id_options = $parameter_id . '_options';
            $parameter_id_from    = $parameter_id . '_from';
            $parameter_id_to      = $parameter_id . '_to';
            $parameter_name       = 'ad_params_' . $this->parameter->id;

            $parameter_value = array(
                'from' => '',
                'to'   => ''
            );

            $jsdate_from = null;
            $jsdate_to = null;

            if (isset($this->data)) {
                $tmp_from = 0;
                $tmp_to = 0;
                if (isset($this->data[$parameter_name . '_from']) && strtotime($this->data[$parameter_name . '_from'])) {
                    $tmp_from = strtotime($this->data[$parameter_name . '_from']);
                }
                if (isset($this->data[$parameter_name . '_to']) && strtotime($this->data[$parameter_name . '_to'])) {
                    $tmp_to = strtotime($this->data[$parameter_name . '_to']);
                }
                if ($tmp_from || $tmp_to) {
                    if ($tmp_from && $tmp_to) {
                        $parameter_value['from'] = min($tmp_from, $tmp_to);
                        $parameter_value['to'] = max($tmp_from, $tmp_to);
                    } elseif ($tmp_from) {
                        $parameter_value['from'] = $tmp_from;
                    } elseif ($tmp_to) {
                        $parameter_value['to'] = $tmp_to;
                    }
                }
                if ($parameter_value['from']) {
                    $jsdate_from = date('Y-m-d', $parameter_value['from']);
                }
                if ($parameter_value['to']) {
                    $jsdate_to = date('Y-m-d', $parameter_value['to']);
                }

                $tmp_filters = array();

                $model_alias = 'ap_' . $this->parameter->id;
                // test if we have at least one variable set... in case we have, we will will need to add first condition
                // (it will be parameter_id)
                if ($parameter_value['from'] || $parameter_value['to']) {
                    $markup_data['email_agent'] = true;
                    $tmp_filters[] = array(
                        'condition' => '['.$model_alias.'].parameter_id = :parameter_' . $this->parameter->id .'_id:',
                        'params' => array(
                            'parameter_' . $this->parameter->id . '_id' => $this->parameter->id,
                        ),
                        'types' => array(
                            'parameter_' . $this->parameter->id . '_id' => \PDO::PARAM_INT
                        )
                    );
                    if ($parameter_value['from'] && $parameter_value['to']) {
                        if ($parameter_value['from'] == $parameter_value['to']) {
                            $tmp_filters[] = array(
                                'condition' => '(CAST(SUBSTRING_INDEX([' . $model_alias . '].value, \',\', 1) AS DATE) = :parameter_' . $this->parameter->id . '_date:)',
                                'params' => array(
                                    'parameter_' . $this->parameter->id . '_date' => date('Y-m-d', $parameter_value['from'])
                                ),
                                'types' => array(
                                    'parameter_' . $this->parameter->id . '_date' => \PDO::PARAM_STR
                                )
//                                'columns' => 'CAST(SUBSTRING_INDEX([' . $model_alias . '].value, \',\', 1) AS DATE) AS ' . $model_alias . '_date',
//                                'having' => '(' . $model_alias . '_date = \'' . date('Y-m-d', $parameter_value['from']) . '\')'
                            );
                        } else {
                            $tmp_filters[] = array(
                                'condition' => '(CAST(SUBSTRING_INDEX([' . $model_alias . '].value, \',\', 1) AS DATE) >= :parameter_' . $this->parameter->id . '_from: AND CAST(SUBSTRING_INDEX([' . $model_alias . '].value, \',\', -1) AS DATE) <= :parameter_' . $this->parameter->id . '_to:)',
                                'params' => array(
                                    'parameter_' . $this->parameter->id . '_from' => date('Y-m-d', $parameter_value['from']),
                                    'parameter_' . $this->parameter->id . '_to' => date('Y-m-d', $parameter_value['to'])
                                ),
                                'types' => array(
                                    'parameter_' . $this->parameter->id . '_from' => \PDO::PARAM_STR,
                                    'parameter_' . $this->parameter->id . '_to' => \PDO::PARAM_STR
                                )
//                                'columns' => 'CAST(SUBSTRING_INDEX([' . $model_alias . '].value, \',\', 1) AS DATE) AS ' . $model_alias . '_from, CAST(SUBSTRING_INDEX([' . $model_alias . '].value, \',\', -1) AS DATE) AS ' . $model_alias . '_to',
//                                'having' => '(' . $model_alias . '_from >= \'' . date('Y-m-d', $parameter_value['from']) . '\' AND ' . $model_alias . '_to <= \'' . date('Y-m-d', $parameter_value['to']) . '\')'
                            );
                        }
                    } else {
                        // we're not sure which value is set, so we test both of them
                        if ($parameter_value['from']) {
                            $tmp_filters[] = array(
                                'condition' => '(CAST(SUBSTRING_INDEX([' . $model_alias . '].value, \',\', 1) AS DATE) >= :parameter_' . $this->parameter->id . '_date: OR CAST(SUBSTRING_INDEX([' . $model_alias . '].value, \',\', 1) AS DATE) IS NULL)',
                                'params' => array(
                                    'parameter_' . $this->parameter->id . '_date' => date('Y-m-d', $parameter_value['from'])
                                ),
                                'types' => array(
                                    'parameter_' . $this->parameter->id . '_date' => \PDO::PARAM_STR
                                )
//                                'columns' => 'CAST(SUBSTRING_INDEX([' . $model_alias . '].value, \',\', 1) AS DATE) AS ' . $model_alias . '_date',
//                                'having' => '(' . $model_alias . '_date >= \'' . date('Y-m-d', $parameter_value['from']) . '\' OR ' . $model_alias . '_date IS NULL)'
                            );
                        } else {
                            $parameter_value['from'] = '';
                        }
                        if ($parameter_value['to']) {
                            $tmp_filters[] = array(
                                'condition' => '(CAST(SUBSTRING_INDEX([' . $model_alias . '].value, \',\', -1) AS DATE) <= :parameter_' . $this->parameter->id . '_date: OR CAST(SUBSTRING_INDEX([' . $model_alias . '].value, \',\', -1) AS DATE) IS NULL)',
                                'params' => array(
                                    'parameter_' . $this->parameter->id . '_date' => date('Y-m-d', $parameter_value['to'])
                                ),
                                'types' => array(
                                    'parameter_' . $this->parameter->id . '_date' => \PDO::PARAM_STR
                                )
//                                'columns' => 'CAST(SUBSTRING_INDEX([' . $model_alias . '].value, \',\', -1) AS DATE) AS ' . $model_alias . '_date',
//                                'having' => '(' . $model_alias . '_date <= \'' . date('Y-m-d', $parameter_value['to']) . '\' OR ' . $model_alias . '_date IS NULL)'
                            );
                        } else {
                            $parameter_value['to'] = '';
                        }
                    }
                }

                if ($combined_filter = $this->render_filter_combine($tmp_filters)) {
                    $markup_data['filter'] = array(
                        'model' => 'Baseapp\Models\AdsParameters',
                        'alias' => $model_alias,
                        'filter' => $combined_filter
                    );
                }
            }

            $suffix = $this->parameter_settings->suffix ? ' (' . $this->parameter_settings->suffix . ')' : '';
            $filter_label = $this->parameter_settings->label_text;
            if (isset($this->parameter_settings->is_searchable_options->label_text) && !empty($this->parameter_settings->is_searchable_options->label_text)) {
                $filter_label = trim($this->parameter_settings->is_searchable_options->label_text);
            }

            $markup_data['html'] .= '<h3>' . $filter_label . $suffix . '</h3>' . PHP_EOL;

            $markup_data['html'] .= '<div class="responsive-group">' . PHP_EOL;
            $markup_data['html'] .= '    <label class="control-label" for="' . $parameter_id . '_from">' . $filter_label . $suffix . '</label>' . PHP_EOL;
            $markup_data['html'] .= '    <div class="input-range display-inline-block width-full">' . PHP_EOL;
            $markup_data['html'] .= '        <input type="text" class="form-control text-center date-filter" id="' . $parameter_id . '_from" value="' . ($parameter_value['from'] ? date('d.m.Y', $parameter_value['from']) : '') . '" data-type="text" data-default="" />' . PHP_EOL;
            $markup_data['html'] .= '        <span class="vertical-middle">-</span>' . PHP_EOL;
            $markup_data['html'] .= '        <input type="text" class="form-control text-center date-filter" id="' . $parameter_id . '_to" value="' . ($parameter_value['to'] ? date('d.m.Y', $parameter_value['to']) : '') . '" data-type="text" data-default="" />' . PHP_EOL;
            $markup_data['html'] .= '        <div class="clearfix"></div>' . PHP_EOL;
            $markup_data['html'] .= '        <input type="hidden" id="' . $parameter_id . '_from_hidden" name="' . $parameter_name . '_from" value="' . ($parameter_value['from'] ? date('Y-m-d', $parameter_value['from']) : '') . '" data-type="text" data-default="" />' . PHP_EOL;
            $markup_data['html'] .= '        <input type="hidden" id="' . $parameter_id . '_to_hidden" name="' . $parameter_name . '_to" value="' . ($parameter_value['to'] ? date('Y-m-d', $parameter_value['to']) : '') . '" data-type="text" data-default="" />' . PHP_EOL;
            $markup_data['html'] .= '    </div>' . PHP_EOL;
            $markup_data['html'] .= '</div>' . PHP_EOL;

            $parameter_range_from_initial_value = ($jsdate_from ? 'new Date("' . $jsdate_from . '")' : 'null');
            $parameter_range_to_initial_value = ($jsdate_to ? 'new Date("' . $jsdate_to . '")' : 'null');

            $markup_data['js'] = <<<JS

    var $parameter_id_options = {
        id: '$parameter_id',
        from: $parameter_range_from_initial_value,
        to: $parameter_range_to_initial_value
    };
    \$('#$parameter_id_from, #$parameter_id_to').handleDateRange($parameter_id_options);
JS;
        }

        return $markup_data;
    }

}
