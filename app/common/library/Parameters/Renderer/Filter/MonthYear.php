<?php

namespace Baseapp\Library\Parameters\Renderer\Filter;

use Baseapp\Library\Parameters\Renderer\Filter;

class MonthYear extends Filter
{

    public function getMarkup()
    {
        $markup_data = array(
            'html'        => '',
            'js'          => '',
            'filter'      => null,
            'email_agent' => false
        );

        if (!$this->is_hidden) {
            $parameter_id   = 'parameter_' . $this->parameter->id;
            $parameter_name = 'ad_params_' . $this->parameter->id;

            $parameter_value = array(
                'from' => null,
                'to'   => null
            );
            if (isset($this->data)) {
                $tmp_filters = array();
                $from_str    = null;
                $from_int    = null;
                $to_str      = null;
                $to_int      = null;

                // first we collect data to temp variables as we don't know whether $from will really be smaller month/year than $to
                if (isset($this->data[$parameter_name . '_from']) && trim($this->data[$parameter_name . '_from'])) {
                    $from_str = trim($this->data[$parameter_name . '_from']);
                    $from_int = intval(str_replace('-', '', $this->data[$parameter_name . '_from']));
                }
                if (isset($this->data[$parameter_name . '_to']) && trim($this->data[$parameter_name . '_to'])) {
                    $to_str = trim($this->data[$parameter_name . '_to']);
                    $to_int = intval(str_replace('-', '', $this->data[$parameter_name . '_to']));
                }

                $model_alias = 'ap_' . $this->parameter->id;

                // test if we have at least one variable set... in case we have, we will will need to add first condition
                // (it will be parameter_id)
                if ($from_int || $to_int) {
                    $markup_data['email_agent'] = true;
                    $tmp_filters[] = array(
                        'condition' => '['.$model_alias.'].parameter_id = :parameter_' . $this->parameter->id .'_id:',
                        'params' => array(
                            'parameter_' . $this->parameter->id . '_id' => $this->parameter->id,
                        ),
                        'types' => array(
                            'parameter_' . $this->parameter->id . '_id' => \PDO::PARAM_INT
                        )
                    );
                    if ($from_int && $to_int) {
                        // if both variables have been set, we have to map right values to right positions...
                        // $from_int <= $to_int

                        if ($from_int == min($from_int, $to_int)) {
                            $parameter_value['from'] = $from_str;
                            $parameter_value['to'] = $to_str;
                        } else {
                            $parameter_value['from'] = $to_str;
                            $parameter_value['to'] = $from_str;
                        }
                    } else {
                        // we're not sure which value is set, so we only pass the values to $parameter_value array
                        $parameter_value['from'] = $from_str;
                        $parameter_value['to'] = $to_str;
                    }

                    // now, it's time to add additional filters (in case they are set)
                    if ($parameter_value['from']) {
                        $tmp_filters[] = array(
                            'condition' => '['.$model_alias.'].value >= :parameter_' . $this->parameter->id . '_from_value:',
                            'params' => array(
                                'parameter_' . $this->parameter->id . '_from_value' => $parameter_value['from']
                            ),
                            'types' => array(
                                'parameter_' . $this->parameter->id . '_from_value' => \PDO::PARAM_STR
                            )
                        );
                    }
                    if ($parameter_value['to']) {
                        $tmp_filters[] = array(
                            'condition' => '['.$model_alias.'].value <= :parameter_' . $this->parameter->id . '_to_value:',
                            'params' => array(
                                'parameter_' . $this->parameter->id . '_to_value' => $parameter_value['to']
                            ),
                            'types' => array(
                                'parameter_' . $this->parameter->id . '_to_value' => \PDO::PARAM_STR
                            )
                        );
                    }
                }

                if ($combined_filter = $this->render_filter_combine($tmp_filters)) {
                    $markup_data['filter'] = array(
                        'model' => 'Baseapp\Models\AdsParameters',
                        'alias' => $model_alias,
                        'filter' => $combined_filter
                    );
                }

            }

            $curr_time = time();
            if ('Year' === $this->parameter_settings->interval_begin_period) {
                $starting_time = mktime(0, 0, 0, date('n', $curr_time), 1, (date('Y', $curr_time) + $this->parameter_settings->interval_begin_value));
            } elseif ('Month' === $this->parameter_settings->interval_begin_period) {
                $starting_time = mktime(0, 0, 0, (date('n', $curr_time) + $this->parameter_settings->interval_begin_value), 1, date('Y', $curr_time));
            }
            if ('Year' === $this->parameter_settings->interval_end_period) {
                $ending_time = mktime(0, 0, 0, date('n', $curr_time), 1, (date('Y', $curr_time) + $this->parameter_settings->interval_end_value));
            } elseif ('Month' === $this->parameter_settings->interval_end_period) {
                $ending_time = mktime(0, 0, 0, (date('n', $curr_time) + $this->parameter_settings->interval_end_value), 1, date('Y', $curr_time));
            }
            $monthyear_length = 0;

            $all_dropdowns = array(
                '#'.$parameter_id.'_from',
                '#'.$parameter_id.'_to'
            );

            $filter_label = $this->parameter_settings->label_text;
            if (isset($this->parameter_settings->is_searchable_options->label_text) && !empty($this->parameter_settings->is_searchable_options->label_text)) {
                $filter_label = trim($this->parameter_settings->is_searchable_options->label_text);
            }

            $markup_data['html'] .= '<h3>' . $filter_label . '</h3>' . PHP_EOL;

            $markup_data['html'] .= '<div class="form-group">' . PHP_EOL;
            $markup_data['html'] .= '    <div class="input-group">' . PHP_EOL;
            $markup_data['html'] .= '        <span class="input-group-addon">Od</span>' . PHP_EOL;
            $markup_data['html'] .= '        <select class="form-control" id="' . $parameter_id . '_from" name="' . $parameter_name . '_from" data-type="select" data-default="" data-bit-name="' . $filter_label . ' od">' . PHP_EOL;
            $markup_data['html'] .= '            <option value=""></option>' . PHP_EOL;
            // for loop begin
            if ($this->parameter_settings->is_reverse) {
                $current_monthyear = $ending_time;
                $starting_monthyear = $starting_time;

                while ($current_monthyear >= $starting_monthyear) {
                    $monthyear_length++;
                    $monthyear_string_value = date('Y-m', $current_monthyear);
                    $monthyear_string = date('m/Y', $current_monthyear);
                    $selected_option = $parameter_value['from'] && trim($parameter_value['from']) == trim($$monthyear_string_value) ? ' selected="selected"' : '';
                    $markup_data['html'] .= '            <option value="' . ((string) $$monthyear_string_value) . '"' . $selected_option . '>' . trim($monthyear_string) . '</option>' . PHP_EOL;
                    $current_monthyear = mktime(0, 0, 0, (date('n', $current_monthyear) - 1), 1, date('Y', $current_monthyear));
                }
            } else {
                $current_monthyear = $starting_time;
                $ending_monthyear = $ending_time;

                while ($current_monthyear <= $ending_monthyear) {
                    $monthyear_length++;
                    $monthyear_string_value = date('Y-m', $current_monthyear);
                    $monthyear_string = date('m/Y', $current_monthyear);
                    $selected_option = $parameter_value['from'] && trim($parameter_value['from']) == trim($monthyear_string_value) ? ' selected="selected"' : '';
                    $markup_data['html'] .= '            <option value="' . ((string) $monthyear_string_value) . '"' . $selected_option . '>' . trim($monthyear_string) . '</option>' . PHP_EOL;
                    $current_monthyear = mktime(0, 0, 0, (date('n', $current_monthyear) + 1), 1, date('Y', $current_monthyear));
                }
            }
            // while loop end;
            $markup_data['html'] .= '        </select>' . PHP_EOL;
            $markup_data['html'] .= '    </div>' . PHP_EOL;
            $markup_data['html'] .= '</div>' . PHP_EOL;
            $markup_data['html'] .= '<div class="form-group">' . PHP_EOL;
            $markup_data['html'] .= '    <div class="input-group">' . PHP_EOL;
            $markup_data['html'] .= '        <span class="input-group-addon">Do</span>' . PHP_EOL;
            $markup_data['html'] .= '        <select class="form-control" id="' . $parameter_id . '_to" name="' . $parameter_name . '_to" data-type="select" data-default="" data-bit-name="' . $filter_label . ' do">' . PHP_EOL;
            $markup_data['html'] .= '            <option value=""></option>' . PHP_EOL;
            // for loop begin
            if ($this->parameter_settings->is_reverse) {
                $current_monthyear = $ending_time;
                $starting_monthyear = $starting_time;

                while ($current_monthyear >= $starting_monthyear) {
                    $monthyear_length++;
                    $monthyear_string_value = date('Y-m', $current_monthyear);
                    $monthyear_string = date('m/Y', $current_monthyear);
                    $selected_option = $parameter_value['to'] && trim($parameter_value['to']) == trim($monthyear_string_value) ? ' selected="selected"' : '';
                    $markup_data['html'] .= '            <option value="' . ((string) $monthyear_string_value) . '"' . $selected_option . '>' . trim($monthyear_string) . '</option>' . PHP_EOL;
                    $current_monthyear = mktime(0, 0, 0, (date('n', $current_monthyear) - 1), 1, date('Y', $current_monthyear));
                }
            } else {
                $current_monthyear = $starting_time;
                $ending_monthyear = $ending_time;

                while ($current_monthyear <= $ending_monthyear) {
                    $monthyear_length++;
                    $monthyear_string_value = date('Y-m', $current_monthyear);
                    $monthyear_string = date('m/Y', $current_monthyear);
                    $selected_option = $parameter_value['to'] && trim($parameter_value['to']) == trim($monthyear_string_value) ? ' selected="selected"' : '';
                    $markup_data['html'] .= '            <option value="' . ((string) $monthyear_string_value) . '"' . $selected_option . '>' . trim($monthyear_string) . '</option>' . PHP_EOL;
                    $current_monthyear = mktime(0, 0, 0, (date('n', $current_monthyear) + 1), 1, date('Y', $current_monthyear));
                }
            }
            // while loop end;
            $markup_data['html'] .= '        </select>' . PHP_EOL;
            $markup_data['html'] .= '    </div>' . PHP_EOL;
            $markup_data['html'] .= '</div>' . PHP_EOL;

            if (count($all_dropdowns)) {
                $all_dropdowns = implode(', ', $all_dropdowns);
                $markup_data['js'] .= <<<JS

    \$('$all_dropdowns').selectpicker({
        style: 'btn btn-default',
        size: 8,
        noneSelectedText: '',
        liveSearch: true,
        noneResultsText: '',
        mobile: OGL.is_mobile_browser,
        iconBase: 'fa',
        tickIcon: 'fa-check text-primary',
        dropupAuto: false
    });
JS;
            }
        }

        return $markup_data;
    }

}
