<?php

namespace Baseapp\Library\Parameters\Renderer\Filter;

use Baseapp\Library\Parameters\Renderer\Filter;
use Baseapp\Library\Utils;

class Title extends Filter
{

    public function getMarkup()
    {
        $markup_data = array(
            'html'        => '',
            'js'          => '',
            'filter'      => null,
            'email_agent' => false
        );

        if (!$this->is_hidden) {
            $parameter_id   = 'parameter_title';
            $parameter_name = 'ad_params_title';

            $filter_label = isset($this->parameter_settings->label_text) ? $this->parameter_settings->label_text : 'Traži po pojmu';
            if (isset($this->parameter_settings->is_searchable_options->label_text) && !empty($this->parameter_settings->is_searchable_options->label_text)) {
                $filter_label = trim($this->parameter_settings->is_searchable_options->label_text);
            }

            if (isset($this->data) && isset($this->data[$parameter_name]) && trim($this->data[$parameter_name])) {
                $markup_data['email_agent'] = true;
                $parameter_value = trim($this->data[$parameter_name]);

                $parsed_param_value = Utils::strip_boolean_operator_characters(Utils::remove_accents($parameter_value));
                $parsed_param_value = Utils::build_boolean_mode_operators($parsed_param_value, '+');
                $parsed_param_value = Utils::quote_unquoted_words_with_dots($parsed_param_value);

                $parsed_param_value_lc = trim(mb_strtolower($parsed_param_value, 'UTF-8'));
                $parameter_value_lc    = mb_strtolower($parameter_value, 'UTF-8');

                $model_alias = 'ast';
                $markup_data['filter'] = array(
                    'model'  => 'Baseapp\Models\AdsSearchTerms',
                    'alias'  => $model_alias,
                    'filter' => array(
                        'where_conditions' => 'FULLTEXT_MATCH_BMODE(['.$model_alias.'].search_data_unaccented, :parsed_param_value:)',
                        'where_params' => array(
                            'parsed_param_value' => $parsed_param_value_lc
                        ),
                        'where_types' => array(
                            'parsed_param_value' => \PDO::PARAM_STR
                        )
                    ),
                    'bits' => array(
                        array(
                            'id'    => $parameter_id,
                            'name'  => $filter_label,
                            'value' => $parameter_value
                        )
                    )
                );
            } else {
                $parameter_value = '';
            }

            if (!$this->get_only_sql_filter_data) {
                $markup_data['html'] .= '<h3 class="margin-top-md">Traži po pojmu</h3>';
                $markup_data['html'] .= '<div class="form-group">' . PHP_EOL;
                //$markup_data['html'] .= '    <input type="text" class="form-control" name="' . $parameter_name . '" id="' . $parameter_id . '" value="' . $this->escaper->escapeHtmlAttr($parameter_value) . '" data-type="text" data-default="" data-bit-name="' . $filter_label . '" data-bit-value="' . $parameter_value . '">' . PHP_EOL;
                $markup_data['html'] .= '
                    <div class="input-group filter-search">
                        <input type="text" class="form-control" placeholder="Upišite pojam..." name="' . $parameter_name . '" 
                            id="' . $parameter_id . '" value="' . $this->escaper->escapeHtmlAttr($parameter_value) . '" 
                            data-type="text" data-default="" data-bit-name="' . $filter_label . '" 
                            data-bit-value="' . $parameter_value . '">
                        <span class="input-group-btn">
                            <button class="btn btn-search" type="submit">
                                <span class="fa fa-fw fa-search"></span>
                            </button>
                        </span>
                    </div>';
                $markup_data['html'] .= '</div>' . PHP_EOL;
            }
        }

        return $markup_data;
    }

}
