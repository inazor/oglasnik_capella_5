<?php

namespace Baseapp\Library\Parameters\Renderer\Filter;

use Baseapp\Library\Parameters\Renderer\Filter;

class Radio extends Filter
{

    public function getMarkup()
    {
        $markup_data = array(
            'html'        => '',
            'js'          => '',
            'filter'      => null,
            'email_agent' => false
        );

        if (!$this->is_hidden) {
            $parameter_id = 'parameter_' . $this->parameter->id;
            $parameter_name = 'ad_params_' . $this->parameter->id;

            if (isset($this->data) && isset($this->data[$parameter_name]) && intval($this->data[$parameter_name])) {
                $markup_data['email_agent'] = true;
                $parameter_value = intval($this->data[$parameter_name]);

                $model_alias = 'ap_' . $this->parameter->id;
                $markup_data['filter'] = array(
                    'model' => 'Baseapp\Models\AdsParameters',
                    'alias' => $model_alias,
                    'filter' => array(
                        'where_conditions' => '(['.$model_alias.'].parameter_id = :parameter_' . $this->parameter->id .'_id: AND ['.$model_alias.'].value = :parameter_' . $this->parameter->id . '_value:)',
                        'where_params' => array(
                            'parameter_' . $this->parameter->id . '_id' => $this->parameter->id,
                            'parameter_' . $this->parameter->id . '_value' => $parameter_value
                        ),
                        'where_types' => array(
                            'parameter_' . $this->parameter->id . '_id' => \PDO::PARAM_INT,
                            'parameter_' . $this->parameter->id . '_value' => \PDO::PARAM_INT
                        )
                    )
                );
            } else {
                $parameter_value = null;
            }

            $filter_label = $this->parameter_settings->label_text;
            if (isset($this->parameter_settings->is_searchable_options->label_text) && !empty($this->parameter_settings->is_searchable_options->label_text)) {
                $filter_label = trim($this->parameter_settings->is_searchable_options->label_text);
            }

            $markup_data['html'] .= '<div class="input-group radio">' . PHP_EOL;
            $markup_data['html'] .= '    <label class="control-label">' . $filter_label . '</label>' . PHP_EOL;

            // for loop begin
            $dictionary_level_values = $this->parameter->getDictionaryValues($this->parameter->dictionary_id);
            if ($dictionary_level_values) {
                foreach ($dictionary_level_values as $value) {
                    $checkbox_selected = $parameter_value && intval($value->id) == $parameter_value ? ' checked="checked"' : '';

                    $markup_data['html'] .= '    <div>' . PHP_EOL;
                    $markup_data['html'] .= '        <input type="radio" id="' . $parameter_id . '_' . $value->id . '" name="' . $parameter_name . '" value="' . $value->id . '"' . $checkbox_selected . ' data-type="radio" data-default="false" data-bit-name="' . $filter_label . '" data-bit-value="' . trim($value->name) . '">' . PHP_EOL;
                    $markup_data['html'] .= '        <label for="' . $parameter_id . '_' . $value->id . '" title="' . trim($value->name) . '">' . trim($value->name) . '</label>' . PHP_EOL;
                    $markup_data['html'] .= '    </div>' . PHP_EOL;
                }
            }
            // for loop end
            $markup_data['html'] .= '</div>' . PHP_EOL;
        }

        return $markup_data;
    }

}
