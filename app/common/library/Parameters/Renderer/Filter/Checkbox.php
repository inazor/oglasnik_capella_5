<?php

namespace Baseapp\Library\Parameters\Renderer\Filter;

use Baseapp\Library\Parameters\Renderer\Filter;

class Checkbox extends Filter
{

    public function getMarkup()
    {
        $markup_data = array(
            'html'        => '',
            'js'          => '',
            'filter'      => null,
            'email_agent' => false
        );

        if (!$this->is_hidden) {
            $parameter_id   = 'parameter_' . $this->parameter->id;
            $parameter_name = 'ad_params_' . $this->parameter->id;

            $tmp_filters = array();
            if (isset($this->data) && isset($this->data[$parameter_name])) {
                $markup_data['email_agent'] = true;
                $parameter_values = $this->data[$parameter_name];

                $model_alias = 'ap_' . $this->parameter->id;
                $tmp_filters[] = array(
                    'condition' => '['.$model_alias.'].parameter_id = :parameter_' . $this->parameter->id .'_id:',
                    'params' => array(
                        'parameter_' . $this->parameter->id . '_id' => $this->parameter->id,
                    ),
                    'types' => array(
                        'parameter_' . $this->parameter->id . '_id' => \PDO::PARAM_INT
                    )
                );
            } else {
                $parameter_values = null;
            }

            if ($this->parameter->dictionary_id) {
                $filter_label = $this->parameter_settings->label_text;
                if (isset($this->parameter_settings->is_searchable_options->label_text) && !empty($this->parameter_settings->is_searchable_options->label_text)) {
                    $filter_label = trim($this->parameter_settings->is_searchable_options->label_text);
                }

                $markup_data['html'] .= '<h3>' . $filter_label . '</h3>' . PHP_EOL;

                // for loop begin
                $dictionary_level_values = $this->parameter->getDictionaryValues($this->parameter->dictionary_id);
                if ($dictionary_level_values) {
                    $tmp_conditions = array();
                    $tmp_conditions_params = array();
                    $tmp_conditions_types = array();

                    foreach ($dictionary_level_values as $value) {
                        $checkbox_selected = $parameter_values ? in_array($value->id, $parameter_values) : false;

                        if ($checkbox_selected) {
                            $tmp_conditions[] = '['.$model_alias.'].value = :parameter_'.$this->parameter->id.'_value_'.$value->id.':';
                            $tmp_conditions_params['parameter_'.$this->parameter->id.'_value_'.$value->id] = $value->id;
                            $tmp_conditions_types['parameter_'.$this->parameter->id.'_value_'.$value->id] = \PDO::PARAM_INT;
                        }

                        // Render the checkboxes in two or single cols depending on label text length
                        $label_text = trim($value->name);
                        if (strlen($label_text) >= 12) {
                            $classes = 'col-md-12 col-lg-12';
                        } else {
                            $classes = 'col-md-6 col-lg-6';
                        }

                        $markup_data['html'] .= '<div class="form-group checkbox checkbox-primary">' . PHP_EOL;
                        $markup_data['html'] .= '    <input type="checkbox" id="' . $parameter_name . '_' . $value->id . '_id" name="' . $parameter_name . '[]" value="' . $value->id . '"' . ($checkbox_selected ? ' checked="checked"' : '') . ' data-type="checkbox" data-default="false" data-bit-name="' . $filter_label . '" data-bit-value="' . trim($value->name) . '">' . PHP_EOL;
                        $markup_data['html'] .= '    <label for="' . $parameter_name . '_' . $value->id . '_id" title="' . trim($value->name) . '">' . $label_text . '</label>' . PHP_EOL;
                        $markup_data['html'] .= '</div>' . PHP_EOL;
                    }

                    $tmp_conditions_count = count($tmp_conditions);
                    $tmp_conditions_params_count = count($tmp_conditions_params);
                    if ($tmp_conditions_count && $tmp_conditions_params_count && $tmp_conditions_count == $tmp_conditions_params_count) {
                        $tmp_filters[] = array(
                            'condition' => '(' . implode($tmp_conditions, ' OR ') . ')',
                            'params' => $tmp_conditions_params,
                            'types' => $tmp_conditions_types
                        );
                    } else {
                        // clear filter condition
                        $tmp_filters = array();
                    }
                } else {
                    // clear filter condition
                    $tmp_filters = array();
                }
                // for loop end
            } else {
                $filter_label = $this->parameter_settings->label_text;
                if (isset($this->parameter_settings->is_searchable_options->label_text) && !empty($this->parameter_settings->is_searchable_options->label_text)) {
                    $filter_label = trim($this->parameter_settings->is_searchable_options->label_text);
                }

                $checkbox_selected = intval($parameter_values) ? true : false;
                if ($checkbox_selected) {
                    $tmp_filters[] = array(
                        'condition' => '['.$model_alias.'].value = :parameter_' . $this->parameter->id .'_value:',
                        'params' => array(
                            'parameter_' . $this->parameter->id . '_value' => 1,
                        ),
                        'types' => array(
                            'parameter_' . $this->parameter->id . '_value' => \PDO::PARAM_INT
                        )
                    );
                } else {
                    // clear filter condition
                    $tmp_filters = array();
                }

                $markup_data['html'] .= '<div class="form-group checkbox checkbox-primary">' . PHP_EOL;
                $markup_data['html'] .= '    <input type="checkbox" id="' . $parameter_name . '_id" name="' . $parameter_name . '" value="1"' . ($checkbox_selected ? ' checked="checked"' : '') . ' data-type="checkbox" data-default="false" data-bit-name="' . trim($this->parameter_settings->is_searchable_options->label_text) . '">' . PHP_EOL;
                $markup_data['html'] .= '    <label for="' . $parameter_name . '_id" title="' . $filter_label . '">' . trim($this->parameter_settings->is_searchable_options->label_text) . '</label>' . PHP_EOL;
                $markup_data['html'] .= '</div>' . PHP_EOL;
            }

            if ($markup_data_filter = $this->render_filter_combine($tmp_filters)) {
                $markup_data['filter'] = array(
                    'model' => 'Baseapp\Models\AdsParameters',
                    'alias' => $model_alias,
                    'filter' => $markup_data_filter
                );
            }
        }

        return $markup_data;
    }

}
