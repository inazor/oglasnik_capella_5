<?php

namespace Baseapp\Library\Parameters\Renderer\Filter;

use Baseapp\Library\Parameters\Renderer\Filter;

class Text extends Filter
{

    public function getMarkup()
    {
        $markup_data = array(
            'html'        => '',
            'js'          => '',
            'filter'      => null,
            'email_agent' => false
        );

        if (!$this->is_hidden) {
            $parameter_id   = 'parameter_' . $this->parameter->id;
            $parameter_name = 'ad_params_' . $this->parameter->id;

            if (isset($this->data) && isset($this->data[$parameter_name]) && trim($this->data[$parameter_name])) {
                $markup_data['email_agent'] = true;
                $parameter_value = trim($this->data[$parameter_name]);

                $model_alias = 'ap_' . $this->parameter->id;
                $markup_data['filter'] = array(
                    'model' => 'Baseapp\Models\AdsParameters',
                    'alias' => $model_alias,
                    'filter' => array(
                        'where_conditions' => '(['.$model_alias.'].parameter_id = :parameter_' . $this->parameter->id .'_id: AND ['.$model_alias.'].value LIKE CONCAT(\'%\', :parameter_' . $this->parameter->id . '_value:, \'%\'))',
                        'where_params' => array(
                            'parameter_' . $this->parameter->id . '_id' => $this->parameter->id,
                            'parameter_' . $this->parameter->id . '_value' => $parameter_value
                        ),
                        'where_types' => array(
                            'parameter_' . $this->parameter->id . '_id' => \PDO::PARAM_INT,
                            'parameter_' . $this->parameter->id . '_value' => \PDO::PARAM_STR
                        )
                    )
                );
            } else {
                $parameter_value = '';
            }

            $filter_label = $this->parameter_settings->label_text;
            if (isset($this->parameter_settings->is_searchable_options->label_text) && !empty($this->parameter_settings->is_searchable_options->label_text)) {
                $filter_label = trim($this->parameter_settings->is_searchable_options->label_text);
            }

            $markup_data['html'] .= '<h3>' . $filter_label . '</h3>' . PHP_EOL;
            $markup_data['html'] .= '<div class="form-group">' . PHP_EOL;
            $markup_data['html'] .= '    <input type="text" class="form-control" name="' . $parameter_name . '" id="' . $parameter_id . '" value="' . $this->escaper->escapeHtmlAttr($parameter_value) . '" data-type="text" data-default="" data-bit-name="' . $filter_label . '">' . PHP_EOL;
            $markup_data['html'] .= '</div>' . PHP_EOL;
        }

        return $markup_data;
    }

}
