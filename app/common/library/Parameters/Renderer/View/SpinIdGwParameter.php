<?php

namespace Baseapp\Library\Parameters\Renderer\View;

use Baseapp\Library\Parameters\Renderer\View;

class SpinIdGwParameter extends View
{
    /**
     * @return array|null
     */
    public function render()
    {
        $ret  = null;

        $parameter_name = 'ad_params_spinidgw';
        $parameter_option = isset($this->data->$parameter_name) ? $this->data->$parameter_name : null;

        if (!$this->parameter_settings->is_hidden && $parameter_option) {
            $ids = array();

            $value = trim($parameter_option->text_value);

            // Multiple 360 ids on the same ad can be comma separated
            if (false !== strpos($value, ',')) {
                $ids = preg_split('@(?:\s*,\s*|^\s*|\s*$)@', $value, null, PREG_SPLIT_NO_EMPTY);
            } else {
                // Treating single value as array too
                $ids[] = $value;
            }

            $ret = array(
                '360' => array(),
                'assets' => array(
                    'js' => array(
                        array('http://148.251.71.98/swipe/v2/snippet.js', array('data-gw' => 'true'))
                    )
                )
            );

            foreach ($ids as $id) {
                $ret['360'][] = array(
                    'spinid' => $id,
                    // TODO/FIXME: if/when we move static assets to cdn or similar, this will need updating probably
                    'spinid_thumb_url' => '/assets/img/360-square-blue.svg'
                );
            }
        }

        return $ret;
    }
}
