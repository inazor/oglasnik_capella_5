<?php

namespace Baseapp\Library\Parameters\Renderer\View;

use Baseapp\Library\Parameters\Renderer\View;
use Baseapp\Library\Utils;

class URLParameter extends View
{
    public function render()
    {
        $html = '';

        $parameter_name = 'ad_params_' . $this->parameter->parameter_id;
        $parameter_option = isset($this->data->$parameter_name) ? $this->data->$parameter_name : null;

        if (!$this->parameter_settings->is_hidden && $parameter_option) {
            $selected_value = Utils::fix_url(trim($parameter_option->text_value));
            if ($selected_value) {
                $html .= '<div class="col-sm-6">' . PHP_EOL;
                $selected_value = $this->escaper->escapeHtmlAttr($selected_value);
                $prefix = isset($this->parameter_settings->prefix) && trim($this->parameter_settings->prefix) ? trim($this->parameter_settings->prefix) . ' ' : '';
                $suffix = isset($this->parameter_settings->suffix) && trim($this->parameter_settings->suffix) ? ' ' . trim($this->parameter_settings->suffix) : '';

                if ('group' === $this->fieldset_style) {
                    $html .= '    <span class="color-light">' . trim($this->parameter_settings->label_text) . '</span>' . PHP_EOL;
                    $html .= '    <ul>' . PHP_EOL;
                    $html .= '        <li>' . $prefix . '<a target="_blank" href="' . $selected_value . '" data-url-parameter-value="true" rel="nofollow external"><span class="fa fa-link fa-fw"></span> ' . $selected_value . '</a>' . $suffix . '</li>' . PHP_EOL;
                    $html .= '    </ul>' . PHP_EOL;
                } else {
                    $html .= '    <span class="color-light">' . trim($this->parameter_settings->label_text) . ':</span> ' . $prefix . '<a target="_blank" href="' . $selected_value . '" data-url-parameter-value="true" rel="nofollow external"><span class="fa fa-link fa-fw"></span> ' . $selected_value . '</a>' . $suffix . '<br>' . PHP_EOL;
                }

                $html .= '</div>' . PHP_EOL;

                return array(
                    'html' => trim($html)
                );
            }
        }

        return null;
    }

}
