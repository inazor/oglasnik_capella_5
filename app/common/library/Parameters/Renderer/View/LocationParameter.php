<?php

namespace Baseapp\Library\Parameters\Renderer\View;

use Baseapp\Library\Parameters\Renderer\View;
use Baseapp\Models\Locations as Location;

class LocationParameter extends View
{
    public function render()
    {
        $output = array(
            'html'   => '',
            'map'    => null,
            'assets' => array(
                'css' => array(),
                'js'  => array()
            )
        );

        // This quick fix was introduced here because of some very smart ppl (https://trello.com/c/BAByUcE1)
        $show_location_in_ad_details = false;

        $map_data = array();

        if ($this->data && $this->parameter_settings) {
            $latest_location_id = null;

            if (count($this->parameter_settings)) {
                $output['html'] = '<div class="col-sm-6">' . PHP_EOL;
                $curr_level     = 1;
                if ('group' === $this->fieldset_style) {
                    $output['html'] .= '    <span class="color-light">Lokacija</span>' . PHP_EOL;
                    $output['html'] .= '    <ul>' . PHP_EOL;
                }
                foreach ($this->parameter_settings as $level) {
                    if (!$level->is_hidden) {
                        $location_level_id = Location::getDBColumnName($curr_level);

                        if (isset($this->data->location->$location_level_id)) {
                            $location_level = $this->data->location->$location_level_id;
                            if ($location_level_id !== 'latlng') {
                                $level_selected_value = null;
                                if (isset($location_level->text_value) && !is_array($location_level->text_value)) {
                                    $level_selected_value = trim($location_level->text_value);
                                    if (isset($location_level->value) && intval($location_level->value)) {
                                        $latest_location_id = intval($location_level->value);
                                    }
                                } elseif (isset($location_level->value) && intval($location_level->value)) {
                                    $location = Location::findFirst(intval($location_level->value));
                                    if ($location) {
                                        $map_data = $location->toArray();
                                        $level_selected_value = $location->name;
                                    }
                                }
                                if ($level_selected_value) {
                                    if ('group' === $this->fieldset_style) {
                                        $output['html'] .= '        <li>' . $level_selected_value . '</li>' . PHP_EOL;
                                    } else {
                                        $output['html'] .= '    <span class="color-light">' . trim($level->label_text) . ':</span> ' . $level_selected_value . '<br>' . PHP_EOL;
                                    }
                                }
                            } else {
                                $tmp_lat = null;
                                $tmp_lng = null;
                                if (isset($location_level->value) && trim($location_level->value)) {
                                    $tmp_point_lat_lng = explode(',', trim($location_level->value));
                                    if ($tmp_point_lat_lng && count($tmp_point_lat_lng) == 2) {
                                        $tmp_lat = $tmp_point_lat_lng[0];
                                        $tmp_lng = $tmp_point_lat_lng[1];

                                        $map_data = array(
                                            'zoom' => 16,
                                            'lat' => $tmp_lat,
                                            'lng' => $tmp_lng
                                        );
                                    }
                                }
                            }
                        }
                    }
                    $curr_level++;
                }
                if ('group' === $this->fieldset_style) {
                    $output['html'] .= '    </ul>' . PHP_EOL;
                }
                $output['html'] .= '</div>' . PHP_EOL;
            }

            // This was commented out in order not to display a map if specific LatLng is specified..
            /*if ($latest_location_id && empty($map_data)) {
                $location = Location::findFirst($latest_location_id);
                if ($location) {
                    $map_data = $location->toArray();
                }
            }*/
        }

        if (!$show_location_in_ad_details) {
            $output['html'] = '';
        }

        if (count($map_data)) {
            $output['map'] = $map_data;
            $output['assets']['js'] = array(
                '//maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&language=hr&key=' . $this->config->google->maps->browserKey,
                'assets/vendor/oms.min.js',
                'assets/js/google-map-wrapper.js'
            );
        }

        return $output;
    }

}
