<?php

namespace Baseapp\Library\Parameters;

use Baseapp\Frontend\Controllers\SectionsController;
use Baseapp\Models\Categories;
use Baseapp\Models\CategoriesFieldsetsParameters as FieldsetParameter;
use Baseapp\Models\Ads;
use Baseapp\Models\AdsMedia;
use Baseapp\Models\AdsParameters;
use Baseapp\Models\AdsSearchTerms;
use Baseapp\Models\Locations as Location;
use Baseapp\Models\Parameters as Parameter;
use Baseapp\Library\Utils;
use Phalcon\Di;
use Baseapp\Traits\ParametrizatorHelpers;

class Parametrizator
{

    use ParametrizatorHelpers;

    /* @var $validation \Phalcon\Validation */
    protected $validation = null;

    /* @var $request \Phalcon\Http\RequestInterface */
    protected $request = null;

    /* @var $data array */
    protected $data = array();

    /* @var $module string */
    protected $module;

    /* @var $mode string */
    protected $mode;

    /* @var $category Categories */
    protected $category = null;

    /* @var $ad Ads */
    protected $ad = null;

    /* @var $ad_parameters array */
    public $ad_parameters = array();

    public $process_description_tpl_placeholders = true;
    public $process_description_offline_placeholders = true;

    protected $prepared_data = array();
    protected $prepared_data_json = array();

    protected $errors;


    // these properties are filled with precompiled with SQL INSERT VALUES
    public $SQL_ad_parameters = array();
    public $SQL_ad_media = array();
    public $SQL_ad_search_terms = null;


    /**
     * @param AdsValidationsFrontend|AdsValidationsBackend $validation
     */
    public function setValidation(&$validation)
    {
        $this->validation = $validation;
    }

    /**
     * @param \Phalcon\Http\RequestInterface $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
        // in case we have a POST in request object, try to setData with it..
        if ($this->request->isPost()) {
            $this->setData($this->request->getPost());
        }
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @param string $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @param string $mode
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * @param Categories $category
     */
    public function setCategory(Categories $category)
    {
        $this->category = $category;
    }

    /**
     * @param int $category_id
     */
    public function setCategoryById($category_id)
    {
        if ($category_id) {
            $category = Categories::findFirst($category_id);
            if (null !== $category && null !== $category->Fieldsets) {
                $this->category = $category;
            }
        }
    }

    /**
     * @param Ads $ad
     */
    public function setAd(Ads &$ad)
    {
        $this->ad = $ad;
    }

    public function getAd()
    {
        return !empty($this->ad) ? $this->ad : null;
    }

    /**
     * @param $errors
     */
    public function setErrors(&$errors)
    {
        $this->errors = $errors;
    }

    public function prepare_and_validate()
    {
        if ($this->validation) {
            if ('backend' === $this->module) {
                if ('create' === $this->mode) {
                    $this->validation->add('user_id', new \Phalcon\Validation\Validator\PresenceOf(array(
                        'message' => 'Cannot create an ad without a user'
                    )));
                }
            } elseif ('frontend' === $this->module && (!isset($this->ad->user_id) || empty($this->ad->user_id))) {
                $this->validation->add('user_id', new \Phalcon\Validation\Validator\PresenceOf(array(
                    'message' => 'Morate se prijaviti ili registrirati'
                )));
            }
        }

        foreach ($this->category->Fieldsets as $category_fieldset) {
            foreach ($category_fieldset->Parameters as $fieldset_parameter) {
                $db_parameter = Parameter::getCached($fieldset_parameter->parameter_id);

                $parameter = $this->getParameterSubclass($db_parameter->type_id, $this->module);
                if ($parameter) {
                    $parameter->setValidation($this->validation);
                    $parameter->setFieldsetParameter($fieldset_parameter);
                    $parameter->setData($this->data);
                    $parameter->setAd($this->ad);
                    $parameter->setAdParameters($this->ad_parameters);
                    $parameter->process();

                    $this->prepared_data = array_merge($this->prepared_data, $parameter->getPreparedData());
                    $this->prepared_data_json = array_merge($this->prepared_data_json, $parameter->getPreparedDataJson());
                }
            }
        }

        $this->ad->category_id = $this->category->id;
        $this->ad->user_id     = isset($this->data['user_id']) && intval($this->data['user_id']) ? (int) $this->data['user_id'] : (isset($this->ad->user_id) && !empty($this->ad->user_id) ? (int)$this->ad->user_id : null);
        $this->ad->phone1      = isset($this->data['phone1']) && trim($this->data['phone1']) ? trim($this->data['phone1']) : null;
        $this->ad->phone2      = isset($this->data['phone2']) && trim($this->data['phone2']) ? trim($this->data['phone2']) : null;

        // we have to validate if we have any related phone numbers for this ad.
        if ($this->validation && 'backend' === $this->module) {
            if (!$this->ad->can_be_exported_to_avus()) {
                $this->validation->add('phone1', new \Phalcon\Validation\Validator\PresenceOf(array(
                    'message' => 'Cannot be exported to Avus until at least one phone number is given!'
                )));
            }
        }

        if ($this->validation) {
            return $this->validation->validate($this->data);
        }
    }

    public static function parse_placeholders_from_tpl($tpl)
    {
        // Example ad_desc_tpl
        //
        // @[Novo/Staro](parameter:17) vozilo, @[Kilometraža](parameter:35)
        // Godište automobila: @[Godište automobila](parameter:33). god.
        // Lokacija: @[Lokacija](location:2)

        preg_match_all("/\@\[([^]]+)\]\((parameter|location):(\d*|\d*_\d*)\)/uU", $tpl, $matches, PREG_PATTERN_ORDER);

        if (!isset($matches[3])) {
            $matches = null;
        }

        return $matches;
    }

    protected function process_tpl_placeholders($tpl, $data, $keep_empty_placeholders = false, $fall_back = null)
    {
        $final = '';

        if ($tpl && trim($tpl) && !empty($data)) {
            $total_placeholders  = 0;
            $missed_placeholders = 0;

            if ($placeholders = self::parse_placeholders_from_tpl($tpl)) {
                // set current tpl as final as we'll be changed that variable!
                $final = $tpl;

                foreach ($placeholders[3] as $i => $parameter_id) {
                    $total_placeholders++;
                    $parameter_level = null;
                    if (strpos($parameter_id, '_')) {
                        // dependable parameter
                        $parameter_id_array = explode('_', $parameter_id);
                        $parameter_id = $parameter_id_array[0];
                        $parameter_level = $parameter_id_array[1];
                    }

                    if ('parameter' === $placeholders[2][$i]) {
                        $parameter_key = 'ad_params_' . $parameter_id;
                        if (isset($data[$parameter_key])) {
                            $parameter_value = $data[$parameter_key];

                            $category_fieldset_parameter = $this->category->getFieldsetParameters(array(
                                'parameter_id = :parameter_id:',
                                'bind' => array(
                                    'parameter_id' => $parameter_id
                                )
                            ));

                            if ($category_fieldset_parameter) {
                                $category_parameter = $category_fieldset_parameter[0];

                                $prefix = '';
                                $suffix = '';

                                if ($parameter_level) {
                                    $category_parameter_data = json_decode($category_parameter->levels);
                                } else {
                                    $category_parameter_data = json_decode($category_parameter->data);
                                }
                                $prefix = isset($category_parameter_data->prefix) ? $category_parameter_data->prefix . ' ' : '';
                                $suffix = isset($category_parameter_data->suffix) ? ' ' . $category_parameter_data->suffix : '';

                                if ($parameter_level) {
                                    $level_id = 'level_' . $parameter_level;
                                    if (isset($parameter_value[$level_id]) && isset($parameter_value[$level_id]['text_value'])) {
                                        $parameter_text_value = $prefix . $parameter_value[$level_id]['text_value'] . $suffix;

                                        $final = str_replace($placeholders[0][$i], $parameter_text_value, $final);
                                    }
                                } else {
                                    if (isset($parameter_value['from']) || isset($parameter_value['to'])) {
                                        $parameter_range = array();
                                        $range_from = null;
                                        $range_to = null;
                                        if (isset($parameter_value['from']['value'])) {
                                            $range_from = strtotime(trim($parameter_value['from']['value']));
                                        }
                                        if (isset($parameter_value['to']['text_value'])) {
                                            $range_to = strtotime(trim($parameter_value['to']['value']));
                                        }
                                        if ($range_from || $range_to) {
                                            if ($range_from && $range_to) {
                                                // check to see if year is the same - if yes, remove it
                                                if (intval(date('Y', $range_from)) === intval(date('Y', $range_to))) {
                                                    $parameter_range[] = 'od ' . date('d.m.', $range_from);
                                                    $parameter_range[] = 'do ' . date('d.m.', $range_to);
                                                } else {
                                                    $parameter_range[] = 'od ' . date('d.m.Y', $range_from);
                                                    $parameter_range[] = 'do ' . date('d.m.Y', $range_to);
                                                }
                                            } elseif ($range_from) {
                                                $parameter_range[] = 'od ' . date('d.m.Y', $range_from);
                                            } else {
                                                $parameter_range[] = 'do ' . date('d.m.Y', $range_to);
                                            }

                                            if (count($parameter_range)) {
                                                $parameter_text_value = implode(' ', $parameter_range);
                                                $final = str_replace($placeholders[0][$i], $parameter_text_value, $final);
                                            }
                                        }
                                    } elseif (isset($parameter_value['text_value'])) {
                                        if ($categoryParameter = Parameter::getCached($category_parameter->parameter_id)) {
                                            if ('NUMBER' === $categoryParameter->type_id) {
                                                $parameter_text_value = $prefix . number_format($parameter_value['text_value'], 0, '', '.') . $suffix;
                                            } else {
                                                $parameter_text_value = $prefix . $parameter_value['text_value'] . $suffix;
                                            }
                                        }

                                        $final = str_replace($placeholders[0][$i], $parameter_text_value, $final);
                                    } elseif (count($parameter_value)) {
                                        $parameter_options = array();
                                        foreach ($parameter_value as $value_option) {
                                            if (isset($value_option['text_value'])) {
                                                $parameter_options[] = trim($value_option['text_value']);
                                            }
                                        }
                                        if (count($parameter_options)) {
                                            $parameter_text_value = implode(', ', $parameter_options);

                                            $final = str_replace($placeholders[0][$i], $parameter_text_value, $final);
                                        }
                                    }
                                }
                            }
                        } else {
                            // we couldn't find the value for this placeholder (most probably user didn't give it)
                            $missed_placeholders++;
                            if ($keep_empty_placeholders) {
                                $final = str_replace($placeholders[0][$i], '[' . $placeholders[1][$i] . ']', $final);
                            } else {
                                $final = str_replace($placeholders[0][$i], '', $final);
                            }
                        }
                    } elseif ('location' === $placeholders[2][$i] && $parameter_level) {

                        $location_key = Location::getDBColumnName($parameter_level);
                        if (isset($data['location'][$location_key])) {
                            $location_value = Location::findFirst($data['location'][$location_key]['value']);
                            if ($location_value) {
                                $final = str_replace($placeholders[0][$i], trim($location_value->name), $final);
                            }
                        } else {
                            // we couldn't find the value for this placeholder (most probably user didn't give it)
                            $missed_placeholders++;
                            if ($keep_empty_placeholders) {
                                $final = str_replace($placeholders[0][$i], '[' . $placeholders[1][$i] . ']', $final);
                            } else {
                                $final = str_replace($placeholders[0][$i], '', $final);
                            }
                        }

                    }
                }

                $final = nl2br($final);
            }

            // in case we have a lot of missed placeholders (user did not fill this info),
            // we will put ad's description instead
            if (0 === $total_placeholders || ((($missed_placeholders / $total_placeholders) * 100) > 60)) {
                if ($fall_back) {
                    $final = trim(preg_replace('/\s+/u', ' ', $fall_back));
                } else {
                    $final = '';
                }
            }
        } elseif ($fall_back) {
            $final = trim(preg_replace('/\s+/u', ' ', $fall_back));
        }

        return $final;
    }

    private static function wrap_placeholder_in_span($text, $placeholder, $placeholder_value)
    {
        preg_match_all("/(?'wrapOpen'[\{]{2}[^\}|\{]*)??(?'placeholder'" . str_replace(array('[', ']', '(', ')'), array('\[', '\]', '\(', '\)'), $placeholder) . "){1}(?'wrapClose'[^\{|\}]*[\}]{2})??/uU", $text, $matches, PREG_PATTERN_ORDER);

        if (isset($matches['wrapOpen'][0]) && trim($matches['wrapOpen'][0]) && isset($matches['wrapClose'][0]) && trim($matches['wrapClose'][0])) {
            $finalPlaceholder = str_replace(array('{{', '}}', $placeholder), array('<span class="classified-param-value">', '</span>', $placeholder_value), $matches[0][0]);
            $finalText        = str_replace($matches[0][0], $finalPlaceholder, $text);
        } else {
            $finalText = str_replace($placeholder, '<span class="classified-param-value">' . $placeholder_value . '</span>', $text);
        }

        return $finalText;
    }

    public static function process_tpl_placeholders_from_row($tpl, $row)
    {
        $finalTpl = array();
        $final    = '';

        $json = trim($row->json_data) ? json_decode($row->json_data) : null;

        if ($tpl && trim($tpl)) {
            // explode tpl by newlines
            $tplLines = explode(PHP_EOL, $tpl);

            if (count($tplLines)) {
                foreach ($tplLines as $tplLine) {
                    if ($placeholders = self::parse_placeholders_from_tpl($tplLine)) {
                        $missed_placeholders = 0;
                        $total_placeholders  = count($placeholders[0]);

                        // set current tpl as final as we'll be changed that variable!
                        $currTplLine = $tplLine;

                        foreach ($placeholders[3] as $i => $parameter_id) {
                            $parameter_level = null;
                            if (strpos($parameter_id, '_')) {
                                // dependable parameter
                                $parameter_id_array = explode('_', $parameter_id);
                                $parameter_id       = $parameter_id_array[0];
                                $parameter_level    = $parameter_id_array[1];
                            }

                            if ('parameter' === $placeholders[2][$i]) {
                                $parameter_key = 'ad_params_' . $parameter_id;
                                if (isset($json->$parameter_key)) {
                                    if ($parameter_level && isset($json->$parameter_key->{'level_' . $parameter_level}->text_value)) {
                                        $parameter_value = $json->$parameter_key->{'level_' . $parameter_level}->text_value;
                                        $currTplLine = self::wrap_placeholder_in_span($currTplLine, $placeholders[0][$i], $parameter_value);
                                    } else {
                                        $parameter_value = $json->$parameter_key;
                                        if (isset($parameter_value->from) || isset($parameter_value->to)) {
                                            $parameter_range = array();
                                            $range_from = null;
                                            $range_to = null;
                                            if (isset($parameter_value->from->value)) {
                                                $range_from = strtotime(trim($parameter_value->from->value));
                                            }
                                            if (isset($parameter_value->to->text_value)) {
                                                $range_to = strtotime(trim($parameter_value->to->value));
                                            }
                                            if ($range_from || $range_to) {
                                                if ($range_from && $range_to) {
                                                    // check to see if year is the same - if yes, remove it
                                                    if (intval(date('Y', $range_from)) === intval(date('Y', $range_to))) {
                                                        $parameter_range[] = 'od ' . date('d.m.', $range_from);
                                                        $parameter_range[] = 'do ' . date('d.m.', $range_to);
                                                    } else {
                                                        $parameter_range[] = 'od ' . date('d.m.Y', $range_from);
                                                        $parameter_range[] = 'do ' . date('d.m.Y', $range_to);
                                                    }
                                                } elseif ($range_from) {
                                                    $parameter_range[] = 'od ' . date('d.m.Y', $range_from);
                                                } else {
                                                    $parameter_range[] = 'do ' . date('d.m.Y', $range_to);
                                                }

                                                if (count($parameter_range)) {
                                                    $parameter_text_value = implode(' ', $parameter_range);
                                                    $currTplLine = self::wrap_placeholder_in_span($currTplLine, $placeholders[0][$i], $parameter_text_value);
                                                }
                                            }
                                        } elseif (isset($parameter_value->text_value)) {
                                            if ($categoryParameter = Parameter::getCached($parameter_id)) {
                                                if ('NUMBER' === $categoryParameter->type_id) {
                                                    $parameter_text_value = number_format($parameter_value->text_value, 0, '', '.');
                                                } else {
                                                    $parameter_text_value = $parameter_value->text_value;
                                                }
                                            }

                                            $currTplLine = self::wrap_placeholder_in_span($currTplLine, $placeholders[0][$i], $parameter_text_value);
                                        } elseif (count($parameter_value)) {
                                            $parameter_options = array();
                                            foreach ($parameter_value as $value_option) {
                                                if (isset($value_option->text_value)) {
                                                    $parameter_options[] = trim($value_option->text_value);
                                                }
                                            }
                                            if (count($parameter_options)) {
                                                $parameter_text_value = implode(', ', $parameter_options);

                                                $currTplLine = self::wrap_placeholder_in_span($currTplLine, $placeholders[0][$i], $parameter_text_value);
                                            }
                                        }
                                    }
                                } else {
                                    $missed_placeholders++;
                                }
                            } elseif ('location' === $placeholders[2][$i] && $parameter_level) {
                                $location_key = Location::getDBColumnName($parameter_level);
                                if (isset($json->location->$location_key)) {
                                    $location_value = $json->location->$location_key->text_value;
                                    if ($location_value) {
                                        $currTplLine = self::wrap_placeholder_in_span($currTplLine, $placeholders[0][$i], $location_value);
                                    }
                                } else {
                                    $missed_placeholders++;
                                }
                            }
                        }

                        if (0 === $missed_placeholders) {
                            $finalTpl[] = $currTplLine;
                        }

                    }
                }

                $final = !empty($finalTpl) ? '<span class="classified-param">' . implode('</span><span class="classified-param">', $finalTpl) . '</span>' : null;
            }
        } else {
            $final = null;
        }

        return $final;
    }

    /**
     * Helper method to precompile ad's description visible on the ad listing
     */
    protected function handle_ad_desc_tpl()
    {
        $changed = false;
        if ($this->process_description_tpl_placeholders) {
            if ($this->category) {
                $category_settings = $this->category->Settings;
                if ($category_settings) {
                    if ($category_settings->ad_desc_tpl && trim($category_settings->ad_desc_tpl)) {
                        $tpl = $category_settings->ad_desc_tpl;
                        $data = $this->prepared_data_json;

                        $this->ad->description_tpl = $this->process_tpl_placeholders(
                            $tpl,
                            $data,
                            $keep_empty_placeholders = false,
                            !empty($this->ad->description) ? $this->ad->description : null
                        );
                        $changed = true;
                    }
                }
            }
        }

        if (!$changed) {
            $this->ad->description_tpl = trim(preg_replace('/\s+/u', ' ', $this->ad->description));
        }
    }

    /**
     * Helper method to precompile ad's offline description
     */
    protected function handle_offline_desc_tpl()
    {
        if ($this->process_description_offline_placeholders) {
            if ($this->category) {
                $category_settings = $this->category->Settings;
                if ($category_settings) {
                    if ($category_settings->offline_desc_tpl && trim($category_settings->offline_desc_tpl)) {
                        $tpl = $category_settings->offline_desc_tpl;
                        $data = $this->prepared_data_json;

                        $this->ad->description_offline = $this->process_tpl_placeholders(
                            $tpl,
                            $data,
                            $keep_empty_placeholders = true,
                            $fall_back = null
                        );
                    }
                }
            }
        }

        if (empty($this->ad->description_offline)) {
            $this->ad->description_offline = Utils::str_truncate($this->ad->description, 157, '...');
        }
    }

    protected function prepare_search_terms()
    {
        $search_terms = array();

        // go through all the parameters
        foreach ($this->prepared_data_json as $parameter_id => $parameter_data) {
            if ('ad_media_gallery' === $parameter_id) {
                break;
            }

            if ('location' === $parameter_id) {
                // handle location parameter
                $values = array();
                foreach ($parameter_data as $row) {
                    if (!empty($row['text_value'])) {
                        $values[] = trim($row['text_value']);
                    }
                }
                if (count($values)) {
                    $search_terms[] = trim(implode(' ', $values));
                }
            } elseif ('ad_params' === substr($parameter_id, 0, 9)) {
                // see if we have a dependable param here
                if (isset($parameter_data['level_1'])) {
                    // yes
                    $values = array();
                    foreach ($parameter_data as $row) {
                        if (!empty($row['text_value'])) {
                            $values[] = trim($row['text_value']);
                        }
                    }
                    if (count($values)) {
                        $search_terms[] = trim(implode(' ', $values));
                    }
                } else {
                    if (!empty($parameter_data['text_value'])) {
                        $value = trim($parameter_data['text_value']);
                        if (!is_numeric($value)) {
                            $search_terms[] = trim($value);
                        }
                    } else {
                        // most probably, we have some multiselectable dictionary parameter
                        $values = array();
                        foreach ($parameter_data as $row) {
                            if (!empty($row['text_value'])) {
                                $values[] = trim($row['text_value']);
                            }
                        }
                        if (count($values)) {
                            $search_terms[] = trim(implode(' ', $values));
                        }
                    }
                }
            }
        }

        // we should have at least title and description here...
        if (empty($search_terms)) {
            // add the title parameter only if it current ad's description doesn't contain its title!
            //
            // if this condition doesn't fulfill, the imported ad will still have its
            // title set (we did this with $ad->title), but adding the parameter's value via addParameterBySlug
            // method adds parameter's value to the json_data field also, and in later processing, we use
            // json_data to prepare search_terms... so if we found the substring of title in
            // description, then there's no need to double information in ads_search_terms table!
            if (mb_stripos($this->ad->description, $this->ad->title, 0, 'UTF-8') === false) {
                $search_terms[] = $this->ad->title;
            }

            // add description parameter
            $search_terms[] = $this->ad->description;
        }

        $search_terms = trim(strip_tags(mb_strtolower(implode(' ', $search_terms), 'UTF-8')));

        if ('import' !== $this->module) {
            $ad_id = !empty($this->ad->id) ? $this->ad->id : null;
            $ad_search_data = AdsSearchTerms::getByPrimaryKeyOrCreateNew($ad_id);
            $ad_search_data->search_data = $search_terms;
            $ad_search_data->search_data_unaccented = Utils::remove_accents($search_terms);
            $this->ad->SearchTerm = $ad_search_data;
        }

        return $search_terms;
    }

    protected function handle_price_parameter()
    {
        // if 'Ad price' parameter exists in this category, then it was set and handled via parameter's set method so
        // we only have to check for cases if the price was not set.
        // if 'Ad price' parameter doesn't exists in this category, we have to clear the price information from the ad
        $default_currency_id = (new \Phalcon\Di())->getDefault()->getShared('config')->payment->default_currency_id;
        $price_parameter     = FieldsetParameter::findFirst(array(
            'conditions' => 'category_id = :category_id: AND parameter_id = :parameter_id:',
            'bind'       => array(
                'category_id'  => $this->ad->category_id,
                'parameter_id' => 2 // Ad price parameter id
            )
        ));

        if ($price_parameter) {
            // put default price == 0 if one is not set.
            if (!isset($this->ad->price)) {
                $this->ad->price = 0;
            }
            // put default currency_id if one is not set.
            if (!isset($this->ad->currency_id)) {
                $this->ad->currency_id = $default_currency_id;
            }
        } else {
            $this->ad->price       = 0;
            $this->ad->currency_id = $default_currency_id;
        }
    }

    /**
     * This method actually parametrizes the ad... it is still a bit dirty as we're
     * not using atomic classes to parametrize the ad.. should be fixed..
     */
    public function parametrize()
    {
        $ad_id = !empty($this->ad->id) ? $this->ad->id : null;

        $this->ad->category_id = $this->category->id;
        if (isset($this->data['user_id']) && intval($this->data['user_id'])) {
            $this->ad->user_id = (int) $this->data['user_id'];
        }
        $this->ad->phone1 = isset($this->data['phone1']) && trim($this->data['phone1']) ? trim($this->data['phone1']) : null;
        $this->ad->phone2 = isset($this->data['phone2']) && trim($this->data['phone2']) ? trim($this->data['phone2']) : null;

        $this->ad->manual_deactivation = isset($this->ad->manual_deactivation) ? $this->ad->manual_deactivation : 0;
        if (count($this->prepared_data_json)) {
            $this->ad->json_data = json_encode($this->prepared_data_json);
        }

        $this->handle_price_parameter();

        // we will track all parameters that are being used with this array. in every iteration, we'll set a unique
        // identifier that will later be used to see if we have something to delete..
        $used_parameter_ids = array();

        $this->ad_parameters = array();
        foreach ($this->prepared_data as $parameter) {
            if (isset($parameter['values']) && count($parameter['values'])) {
                $curr_index = 0;
                foreach ($parameter['values'] as $value) {
                    $used_parameter_ids[] = $parameter['id'] . '|' . 0 . '|' . $curr_index;
                    $ad_parameter = AdsParameters::getByPrimaryKeyOrCreateNew(
                        $ad_id, $parameter['id'], 0, $curr_index
                    );
                    $ad_parameter->value = $value;
                    $this->ad_parameters[] = $ad_parameter;
                    $curr_index++;
                }
            } elseif (isset($parameter['range']) && count($parameter['range'])) {
                $used_parameter_ids[] = $parameter['id'] . '|' . 0 . '|' . 0;
                $ad_parameter = AdsParameters::getByPrimaryKeyOrCreateNew(
                    $ad_id, $parameter['id'], 0, 0
                );

                $range_from = isset($parameter['range']['from']) ? $parameter['range']['from'] : null;
                $range_to   = isset($parameter['range']['to']) ? $parameter['range']['to'] : null;
                if ($range_from && $range_to) {
                    $ad_parameter->value = $range_from . ',' . $range_to;
                } elseif ($range_from) {
                    $ad_parameter->value = $range_from . ',';
                } else {
                    $ad_parameter->value = ',' . $range_to;
                }
                $this->ad_parameters[] = $ad_parameter;
            } else {
                $used_parameter_ids[] = $parameter['id'] . '|' . (isset($parameter['level']) && intval($parameter['level']) ? $parameter['level'] : 0) . '|' . 0;
                $ad_parameter = AdsParameters::getByPrimaryKeyOrCreateNew(
                    $ad_id,
                    $parameter['id'],
                    (isset($parameter['level']) && intval($parameter['level']) ? $parameter['level'] : 0),
                    0
                );
                $ad_parameter->value = $parameter['value'];
                $this->ad_parameters[] = $ad_parameter;
            }
        }
        $this->ad->AdParameters = $this->ad_parameters;

        // remove all parameteres that are not used... used parameters are saved in $used_parameter_ids array, so
        // anything that is in $ad->AdParameters and is not in $used_parameter_ids should be deleted!
        foreach ($this->ad->AdParameters as $AdParameter) {
            // parameter_id|level|item_index
            $parameter_identifier = $AdParameter->parameter_id . '|' . $AdParameter->level . '|' . $AdParameter->item_index;
            if (!in_array($parameter_identifier, $used_parameter_ids)) {
                $AdParameter->delete();
            }
        }

        // handle media files insert/update/delete
        if (!empty($this->prepared_data_json['ad_media_gallery'])) {
            $used_media_ids = array();
            $media = array();

            $curr_sort_idx = 1;
            foreach ($this->prepared_data_json['ad_media_gallery'] as $media_id) {
                $used_media_ids[] = $media_id;
                $ad_media = AdsMedia::getByAdMediaIdOrCreateNew($ad_id, $media_id);
                $ad_media->sort_idx = $curr_sort_idx++;
                $media[] = $ad_media;
            }
            $this->ad->Media = $media;
            $this->ad->setUnsavedMedia($media);
            // remove all media that are not used... simmilar thing is done with parameters
            foreach ($this->ad->Media as $Media) {
                // media_id
                $media_identifier = $Media->media_id;
                if (!in_array($media_identifier, $used_media_ids)) {
                    $Media->delete();
                }
            }
        } elseif ($this->ad->Media->valid()) {
            $this->ad->Media->delete();
        }

        $this->handle_ad_desc_tpl();

        // we should only generate template for ads that have description_offline empty
        if (empty($this->data['ad_description_offline'])) {
            $this->handle_offline_desc_tpl();
        }

        $this->prepare_search_terms();

        // check if ad's content has been changed
        if ('frontend' === $this->module && $this->ad->hasAdContentChanged()) {
            $this->ad->moderation = 'waiting';
        } elseif ('backend' === $this->module) {
            $this->ad->moderation = 'ok';
        }
    }

    /**
     * This method actually parametrizes the ad... it is still a bit dirty as we're
     * not using atomic classes to parametrize the ad.. should be fixed..
     */
    public function parametrize_imported()
    {
        $ad_id = null;

        $this->ad->category_id = $this->category->id;
        $this->ad->moderation = 'ok';
        $this->ad->manual_deactivation = 0;
        if (count($this->prepared_data_json)) {
            $this->ad->json_data = json_encode($this->prepared_data_json);
        }

        $this->handle_price_parameter();

        $this->SQL_ad_parameters = array();
        foreach ($this->prepared_data as $parameter) {
            if (isset($parameter['values']) && count($parameter['values'])) {
                $curr_index = 0;
                foreach ($parameter['values'] as $value) {
                    $this->SQL_ad_parameters[] = "([AD_ID], " . $parameter['id'] . ", 0, " . $curr_index . ", '" . $value . "')";
                    $curr_index++;
                }
            } elseif (isset($parameter['range']) && count($parameter['range'])) {
                $range_from = isset($parameter['range']['from']) ? $parameter['range']['from'] : null;
                $range_to   = isset($parameter['range']['to']) ? $parameter['range']['to'] : null;
                if ($range_from && $range_to) {
                    $value = $range_from . ',' . $range_to;
                } elseif ($range_from) {
                    $value = $range_from . ',';
                } else {
                    $value = ',' . $range_to;
                }
                $this->SQL_ad_parameters[] = "([AD_ID], " . $parameter['id'] . ", 0, 0, '" . $value . "')";
            } else {
                $param_level = (isset($parameter['level']) && intval($parameter['level']) ? $parameter['level'] : 0);
                $this->SQL_ad_parameters[] = "([AD_ID], " . $parameter['id'] . ", " . $param_level . ", 0, '" . $parameter['value'] . "')";
            }
        }

        $this->SQL_ad_media = array();
        // handle media files insert/update/delete
        if (!empty($this->prepared_data_json['ad_media_gallery'])) {
            $media = array();

            $curr_sort_idx = 1;
            foreach ($this->prepared_data_json['ad_media_gallery'] as $media_id) {
                $this->SQL_ad_media[] = "([AD_ID], " . $media_id . ", " . $curr_sort_idx++ . ")";
            }
        }

        $search_terms = $this->prepare_search_terms();
        $this->SQL_ad_search_terms = null;
        if ($search_terms) {
            $this->SQL_ad_search_terms = "([AD_ID], '" . $search_terms . "')";
        }
    }

    public function getParameterBySlug($parameter_slug = null)
    {
        $parameter_row = null;

        if (trim($parameter_slug)) {
            $db = new \Baseapp\Library\RawDB();
            $parameter_data = $db->findFirst(
                'SELECT p.id, p.type_id, p.dictionary_id FROM categories_fieldsets_parameters cfp INNER JOIN parameters p ON cfp.parameter_id = p.id WHERE cfp.category_id = :category_id AND cfp.parameter_slug = :parameter_slug',
                array(
                    'category_id' => $this->category->id,
                    'parameter_slug' => trim($parameter_slug)
                )
            );
            if ($parameter_data) {
                $parameter_row = $parameter_data;
            }
        }

        return $parameter_row;
    }

    public function getFieldsetParameterBySlug($parameter_slug = null)
    {
        $fieldset_parameter = null;

        if (trim($parameter_slug)) {
            $fieldset_parameter = FieldsetParameter::findFirst(array(
                'category_id = :category_id: AND parameter_slug = :parameter_slug:',
                'bind' => array(
                    'category_id'    => $this->category->id,
                    'parameter_slug' => trim($parameter_slug)
                )
            ));
        }

        return $fieldset_parameter;
    }

    public function addParameterBySlug($parameter_slug = null, $value)
    {
        if (trim($parameter_slug)) {
            if ($fieldset_parameter = $this->getFieldsetParameterBySlug($parameter_slug)) {
                if ($db_parameter = Parameter::getCached($fieldset_parameter->parameter_id)) {
                    if ($parameter = $this->getParameterSubclass($db_parameter->type_id, $this->module)) {
                        // we have a parameter we'll be working on...
                        $parameter->setFieldsetParameter($fieldset_parameter);
                        $parameter->setAd($this->ad);
                        $parameter->setValue($value);

                        $this->prepared_data = array_merge($this->prepared_data, $parameter->getPreparedData());
                        $this->prepared_data_json = array_merge($this->prepared_data_json, $parameter->getPreparedDataJson());
                    }
                }
            }
        }
    }

    public function addParameter($type_id, $value)
    {
        if (trim($type_id)) {
            if ($parameter = $this->getParameterSubclass($type_id, $this->module)) {
                $parameter->setParameterById($db_parameter_row['id']);
                $parameter->setAd($this->ad);
                $parameter->setValue($value);

                $this->prepared_data = array_merge($this->prepared_data, $parameter->getPreparedData());
                $this->prepared_data_json = array_merge($this->prepared_data_json, $parameter->getPreparedDataJson());
            }
        }
    }

    public function render_input()
    {
        $output = array(
            'html'   => null,
            'js'     => null,
            'assets' => array(
                'js'  => array(),
                'css' => array()
            )
        );

        $row_container = array();

        if (isset($this->category)) {
            $category_fieldsets      = $this->category->getFieldsets(array('order' => 'sort_order'));
            $category_fieldsets_html = '';
            $category_fieldsets_js   = '';
            $category_assets_css     = array();
            $category_assets_js      = array();
            foreach ($category_fieldsets as $category_fieldset) {
                $fieldset_parameters      = $category_fieldset->getParameters(array('order' => 'sort_order'));
                $fieldset_parameters_html = '';
                $fieldset_parameters_js   = '';

                foreach ($fieldset_parameters as $fieldset_parameter) {
                    $db_parameter = Parameter::getCached($fieldset_parameter->parameter_id);

                    $parameter = $this->getParameterSubclass($db_parameter->type_id, $this->module);
                    if ($parameter) {
                        $parameter->setFieldsetParameter($fieldset_parameter);
                        $parameter->setData($this->data);
                        $parameter->setAd($this->ad);
                        $parameter->setErrors($this->errors);
                        $fieldset_parameter_render_data = $parameter->render_input();

                        $row_container_count = count($row_container);

                        $fieldset_parameter_html = $fieldset_parameter_render_data['html'];
                        $fieldset_parameters_js .= $fieldset_parameter_render_data['js'];

                        if (isset($fieldset_parameter_render_data['assets']['css'])) {
                            $category_assets_css = array_merge($category_assets_css, $fieldset_parameter_render_data['assets']['css']);
                        }
                        if (isset($fieldset_parameter_render_data['assets']['js'])) {
                            $category_assets_js = array_merge($category_assets_js, $fieldset_parameter_render_data['assets']['js']);
                        }

                        if ($fieldset_parameter_render_data['row']) {
                            if ($row_container_count) {
                                $fieldset_parameters_html .= '<div class="row">' . PHP_EOL . implode(PHP_EOL, $row_container) . PHP_EOL . '</div>' . PHP_EOL;
                                $row_container = array();
                            }
                            $fieldset_parameters_html .= '<div class="row">' . PHP_EOL . $fieldset_parameter_html . PHP_EOL . '</div>' . PHP_EOL;
                        } else {
                            $row_container[] = $fieldset_parameter_html;
                            // it is 3 because, we increase the counter before adding new fieldset_parameter_html
                            // to an array
                            if ($row_container_count == 3) {
                                $fieldset_parameters_html .= '<div class="row">' . PHP_EOL . implode(PHP_EOL, $row_container) . PHP_EOL . '</div>' . PHP_EOL;
                                $row_container = array();
                            }
                        }
                    }
                }

                if (count($row_container)) {
                    $fieldset_parameters_html .= '<div class="row">' . PHP_EOL . implode(PHP_EOL, $row_container) . PHP_EOL . '</div>' . PHP_EOL;
                    $row_container = array();
                }

                if (trim($fieldset_parameters_html)) {
                    $category_fieldsets_html .= '<div class="panel panel-default">' . PHP_EOL;
                    if (trim($category_fieldset->name)) {
                        $category_fieldsets_html .= '<div class="panel-heading"><h3 class="panel-title">' . trim($category_fieldset->name) . '</h3></div>' . PHP_EOL;
                    }
                    $category_fieldsets_html .= '<div class="panel-body">' . PHP_EOL . trim($fieldset_parameters_html) . PHP_EOL . '</div>' . PHP_EOL;
                    $category_fieldsets_html .= '</div>' . PHP_EOL;
                }

                $category_fieldsets_js .= $fieldset_parameters_js;
            }

            if (trim($category_fieldsets_html)) {
                $output['html'] = $category_fieldsets_html;
                $output['js'] = trim($category_fieldsets_js) ? PHP_EOL . '$(document).ready(function(){' . $category_fieldsets_js . PHP_EOL . '});' : '';
                $output['assets']['css'] = array_unique($category_assets_css);
                $output['assets']['js'] = array_unique($category_assets_js);
            }
        }

        return $output;
    }

    /**
     * Parametrizators method to get all is_searchable fields from current category
     *
     * @return array
     */
    public function getFilters()
    {
        //global $output;
        $output = array(
            'html'            => array(),
            'js'              => array(),
            'filters'         => array(),
            'assets'          => array(
                'css' => array(),
                'js'  => array()
            ),
            'form'            => false,
            'email_agent'     => false,
            'show_more_after' => null
        );

        // Searchable fields => 'filters_sort_order' NOT NULL
        $available_filters = $this->category->getFieldsetParameters(array(
            'conditions' => 'filters_sort_order IS NOT NULL',
            'order'      => 'filters_sort_order ASC'
        ));

        if (null !== $available_filters && count($available_filters)) {
            $output['form'] = true;
            $append_to_output = function ($render_data) use (&$output) {
                if ($render_data && is_array($render_data) && isset($render_data['html']) && '' !== trim($render_data['html'])) {

                    $output['html'][] = $render_data['html'];
                    if (trim($render_data['js'])) {
                        $output['js'][] = $render_data['js'];
                    }
                    if (isset($render_data['filter']) && is_array($render_data['filter'])) {
                        $output['filters'][] = $render_data['filter'];
                    }
                    if (isset($render_data['assets']) && is_array($render_data['assets'])) {
                        if (isset($render_data['assets']['css']) && is_array($render_data['assets']['css'])) {
                            $output['assets']['css'] = array_merge($output['assets']['css'], $render_data['assets']['css']);
                        }

                        if (isset($render_data['assets']['js']) && is_array($render_data['assets']['js'])) {
                            $output['assets']['js'] = array_merge($output['assets']['js'], $render_data['assets']['js']);
                        }
                    }
                }
            };

            $lastFilterIndex = 0;
            foreach ($available_filters as $currFilterIndex => $filter_parameter) {
                if ($db_parameter = Parameter::getCached($filter_parameter->parameter_id)) {
                    $parameter = $this->getParameterSubclass($db_parameter->type_id, $this->module);
                    if ($parameter) {
                        $parameter->setModule($this->module);
                        $parameter->setFieldsetParameter($filter_parameter);
                        $parameter->setData($this->data);
                        $render_data = $parameter->getFilterMarkup();
                        if ($output['email_agent'] == false && $render_data['email_agent'] == true) {
                            $output['email_agent'] = true;
                        }
                        if ($filter_parameter->filters_sort_order_show_more) {
                            $output['show_more_after'] = $currFilterIndex;
                        }
                        $append_to_output($render_data);
                    }
                }

                $lastFilterIndex = $currFilterIndex;
            }

            if (!$output['show_more_after']) {
                $output['show_more_after'] = $lastFilterIndex;
            }
        } else {
            // as we don't have any filters in this category, we'll try to return some drill-down with subcategories...
            $output['html'][] = '<!-- remove me! -->';
            /*
            $output['html'][] = '<p>Kategorije</p>';

            $di = Di::getDefault();
            $url_service = $di->get('url');
            $tree = $di->get(Categories::MEMCACHED_KEY);
            $subtree = $tree[(int) $this->category->id];
            $category_children = $subtree->children;

            // TODO: Collect all the ids for the entire subcategory tree and get the counts grouped by category
            // in a single query somehow similar to when we get it for the search results, but this time we
            // count them all up to the root nodes we have stored in $category_children

            $total_counts = 0;
            if ($category_children && count($category_children)) {
                $menu = '<ul class="submenu">';
                foreach ($category_children as $child) {
                    $node = $tree[$child->id];

                    $ids  = SectionsController::getDescendantIdsWithTransactionTypeFromTreeArray($node);

                    $id_where = 'category_id IN (' . implode(',', $ids) . ')';
                    $conditions = $id_where . ' AND active = 1 AND is_spam = 0';
                    $conditions .= ' AND online_product_id != "pinky" AND online_product_id != "platinum"';

                    $cnt  = Ads::count($conditions);

                    $cnt = 0;
                    $menu .= '<li><a href="' . $url_service->get($child->url) . '">' . $child->name . '</a>';
                    $menu .= '<span class="ad_count text-muted">(' . $cnt . ')</span></li>';

                    $total_counts += $cnt;
                }
                $menu .= '</ul><!-- total: ' . $total_counts . ' -->';
                $output['html'][] = $menu;
            }
            */
        }

        return $output;
    }

    /**
     * Parametrizators method to get rendered ad
     *
     * @return array
     */
    public function getRenderedAd()
    {
        if (empty($this->data) && $this->ad && isset($this->ad->json_data)) {
            $this->data = json_decode($this->ad->json_data);
        }

        $output = array(
            'html'                => array(
                'other' => null
            ),
            'ad_media'            => null,
            'map'                 => null,
            '360'                 => null,
            'external_url_button' => null,
            'notabs'              => true,
            'js'                  => array(),
            'assets'              => array(
                'css' => array(),
                'js'  => array()
            )
        );

        if ($this->ad && $this->ad->id && $this->category && $this->category->id) {
            $category_fieldsets = $this->category->getFieldsets(array('order' => 'sort_order'));

            if (null !== $category_fieldsets && count($category_fieldsets)) {
                foreach ($category_fieldsets as $category_fieldset) {
                    $fieldset_renderer = new Renderer\FieldsetRenderer($category_fieldset);
                    $fieldset_renderer->setData($this->data);
                    $render_data = $fieldset_renderer->render();

                    if (isset($render_data['html']) && trim($render_data['html'])) {
                        if (!is_array($output['html']['other'])) {
                            $output['html']['other'] = array();
                        }
                        $output['html']['other'][] = trim($render_data['html']);
                    }
                    if (isset($render_data['map']) && count($render_data['map'])) {
                        $output['map'] = $render_data['map'];
                    }
                    if (isset($render_data['assets']['css']) && count($render_data['assets']['css'])) {
                        $output['assets']['css'] = array_merge($output['assets']['css'], $render_data['assets']['css']);
                    }
                    if (isset($render_data['assets']['js']) && count($render_data['assets']['js'])) {
                        $output['assets']['js'] = array_merge($output['assets']['js'], $render_data['assets']['js']);
                    }
                    if (isset($render_data['external_url_button'])) {
                        $output['external_url_button'] = $render_data['external_url_button'];
                    }
                    if (isset($render_data['360'])) {
                        $output['360'] = $render_data['360'];
                    }
                }
            }

            // check for media files (gallery), but first check to see if we have anything in _unsavedMedia
            $unsaved_media = $this->ad->getUnsavedMedia();
            if (!empty($unsaved_media)) {
                $ad_media = array();
                foreach ($unsaved_media as $adm) {
                    if ($media = \Baseapp\Models\Media::findFirst($adm->media_id)) {
                        $ad_media[] = $media;
                    }
                }
            } else {
                $ad_media = $this->ad->get_media();
            }
            if ($ad_media && count($ad_media)) {
                $output['ad_media'] = $ad_media;
                $output['assets']['css'][] = 'assets/vendor/gallery/css/blueimp-gallery.min.css';
                $output['assets']['js'][]  = 'assets/vendor/gallery/js/blueimp-gallery.min.js';
            }

            // If no media is present, but 360 is, add blueimp assets, since we depend on it for 360 display
            if (!isset($output['ad_media']) || empty($output['ad_media'])) {
                if (!empty($output['360'])) {
                    $output['assets']['css'][] = 'assets/vendor/gallery/css/blueimp-gallery.min.css';
                    $output['assets']['js'][]  = 'assets/vendor/gallery/js/blueimp-gallery.min.js';
                }
            }

            if ($output['ad_media'] || $output['map'] || $output['360']) {
                $output['notabs'] = false;
            }
        }

        return $output;
    }

    public static function get_search_terms($ad, $json_data = null)
    {
        $search_terms = array();

        $search_terms[] = $ad['title'];
        $search_terms[] = $ad['description'];

        $types_not_to_import = array('title', 'description', 'description_offline', 'description_tpl');

        if ($json_data) {
            // go through all the parameters
            foreach ($json_data as $parameter_id => $parameter_data) {
                if ('ad_media_gallery' === $parameter_id) {
                    break;
                }

                if ('location' === $parameter_id) {
                    // handle location parameter
                    $values = array();
                    foreach ($parameter_data as $row) {
                        if (!empty($row['text_value'])) {
                            $values[] = trim($row['text_value']);
                        }
                    }
                    if (count($values)) {
                        $search_terms[] = trim(implode(' ', $values));
                    }
                } elseif ('ad_params' === substr($parameter_id, 0, 9)) {
                    // see if we have a dependable param here
                    if (isset($parameter_data['level_1'])) {
                        // yes
                        $values = array();
                        foreach ($parameter_data as $row) {
                            if (!empty($row['text_value'])) {
                                $values[] = trim($row['text_value']);
                            }
                        }
                        if (count($values)) {
                            $search_terms[] = trim(implode(' ', $values));
                        }
                    } else {
                        if (!empty($parameter_data['type']) && in_array($parameter_data['type'], $types_not_to_import)) {
                            break;
                        } else {
                            if (!empty($parameter_data['text_value'])) {
                                $value = trim($parameter_data['text_value']);
                                if (!is_numeric($value)) {
                                    $search_terms[] = trim($value);
                                }
                            } else {
                                // most probably, we have some multiselectable dictionary parameter
                                $values = array();
                                foreach ($parameter_data as $row) {
                                    if (!empty($row['text_value'])) {
                                        $values[] = trim($row['text_value']);
                                    }
                                }
                                if (count($values)) {
                                    $search_terms[] = trim(implode(' ', $values));
                                }
                            }
                        }
                    }
                }
            }
        }

        return count($search_terms) ? trim(mb_strtolower(strip_tags(implode(' ', $search_terms)), 'UTF-8')) : null;
    }

}
