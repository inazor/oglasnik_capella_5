<?php

namespace Baseapp\Library\Parameters;

class NumberIntervalParameter extends BaseParameter
{
    protected $group_type = 'custom';
    protected $settings_location = 'data';
    protected $type = 'NUMBERINTERVAL';
    protected $accept_dictionary = false;
    protected $can_default = false;
    protected $can_default_type = 'text';
    protected $can_other = false;
    protected $can_prefix_suffix = false;
    protected $is_dependable = false;
    protected $is_required = false;
    public $is_searchable = false;
    public $is_searchable_type = array('INTERVAL_TEXT', 'INTERVAL_SLIDER');

    public function process()
    {
        // TODO: Implement NUMBERINTERVAL parameter parsing
    }

}
