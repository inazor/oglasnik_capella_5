<?php

namespace Baseapp\Library\Parameters;

use Baseapp\Models\Media;

class UploadableParameter extends BaseParameter
{
    protected $group_type = 'standard';
    protected $settings_location = 'data';
    protected $type = 'UPLOADABLE';
    protected $accept_dictionary = false;
    protected $can_default = false;
    protected $can_default_type = '';
    protected $can_other = false;
    protected $can_prefix_suffix = false;
    protected $is_dependable = false;
    protected $is_required = false;
    public $is_searchable = false;
    public $is_searchable_type = array('CHECKBOX');

    public function setValue($media_files)
    {
        if (isset($this->ad) && count($media_files)) {
            $this->setPreparedDataJson(
                'ad_media_gallery',
                $media_files
            );
        }
    }

    public function process()
    {
        if ($this->validation && $this->parameter_settings->is_required) {
            $this->validation->add('ad_media_gallery', new \Phalcon\Validation\Validator\PresenceOf(array(
                'message' => '\'' . $this->parameter_settings->label_text . '\' ' . $this->getValidationString('cannot_be_empty')
            )));
        }

        if (isset($this->data['ad_media_gallery'])) {
            $this->setValue($this->data['ad_media_gallery']);
        }
    }

    public function render_input()
    {
        parent::render_input();

        $render_data = array(
            'row'  => true,
            'html' => '',
            'js'   => ''
        );

        if (!$this->is_hidden) {
            $render_data['assets'] = array(
                'css' => array(
                    'assets/css/uploadable-sorter.css',
                    'assets/css/pictureSorter.css',
                    'assets/vendor/jquery-file-upload/jquery.fileupload.css'
                ),
                'js'  => array(
                    'assets/vendor/jquery-file-upload/jquery.iframe-transport.js',
                    'assets/vendor/jquery-file-upload/jquery.fileupload.js'
                )
            );

            $parameter_id           = 'parameter_uploadable';
            $parameter_id_progress  = $parameter_id . '_progress';
            $parameter_id_gallery   = $parameter_id . '_gallery';
            $parameter_name         = 'ad_media';
            $parameter_name_gallery = $parameter_name . '_gallery';

            $has_errors = false;
            if (null !== $this->errors) {
                $param_errors = $this->errors->filter($parameter_name_gallery);
                if ($param_errors) {
                    $has_errors = true;
                }
            }

            $required_html = ($this->parameter_settings->is_required ? ' <abbr title="' . $this->getValidationString('required_field') . '">*</abbr>' : '');

            $render_data['html'] .= '<div class="col-lg-12 uploadable-holder">' . PHP_EOL;
            $render_data['html'] .= '    <div class="form-group' . ($has_errors ? ' has-error' : '') . '">' . PHP_EOL;
            $render_data['html'] .= '        <label class="control-label" for="' . $parameter_id . '">' . trim($this->parameter_settings->label_text) . $required_html . '</label>' . PHP_EOL;

            if ($has_errors && isset($param_errors)) {
                foreach ($param_errors as $msg) {
                    $render_data['html'] .= '        <p class="help-block">' . $msg->getMessage() . '</p>' . PHP_EOL;
                }
            }

            if (isset($this->parameter_settings->help_text) && trim($this->parameter_settings->help_text)) {
                $render_data['html'] .= '        <p class="help-block">' . trim($this->parameter_settings->help_text) . '</p>' . PHP_EOL;
            }

            $render_data['html'] .= '        <div>';
            $render_data['html'] .= '            <span class="btn btn-success fileinput-button">' . PHP_EOL;
            $render_data['html'] .= '                <span class="glyphicon glyphicon-plus"></span>' . PHP_EOL;
            $button_text = trim($this->parameter_settings->button_text) ? trim($this->parameter_settings->button_text) : $this->getValidationString('choose_files');
            $render_data['html'] .= '                <span>' . $button_text . '</span>' . PHP_EOL;
            $render_data['html'] .= '                <input id="' . $parameter_id . '" type="file" name="' . $parameter_name . '[]" multiple accept=".jpeg,.jpg,.png,image/jpeg,image/png">' . PHP_EOL;
            $render_data['html'] .= '            </span>' . PHP_EOL;
            $render_data['html'] .= '            <br/>' . PHP_EOL;
            $render_data['html'] .= '            <br/>' . PHP_EOL;
            $render_data['html'] .= '            <div id="' . $parameter_id_progress . '" class="progress hidden">' . PHP_EOL;
            $render_data['html'] .= '                <div class="progress-bar progress-bar-success"></div>' . PHP_EOL;
            $render_data['html'] .= '            </div>' . PHP_EOL;
            $render_data['html'] .= '            <ul id="' . $parameter_id_gallery . '" class="uploadable-sorter row" data-form="' . $parameter_name_gallery . '">' . PHP_EOL;
            if (isset($this->data) && isset($this->data[$parameter_name_gallery])) {
                $mediaCount = count($this->data[$parameter_name_gallery]);
                $currMedia = 1;
                foreach ($this->data[$parameter_name_gallery] as $media) {

                    // TODO: make sure $media is a Media object earlier by passing along the
                    // Ad model instance this all is generated for, it would simplify things a lot...
                    // so that we can do what we need with it (get backend thumb etc)
                    $media_obj = Media::findFirst($media);
                    if ($media_obj) {
                        // For category-specific fallback/no-thumb images
                        $category_id = null;
                        if (isset($this->ad)) {
                            $category_id = $this->ad->category_id;
                        }
                        $mediaThumb = $media_obj->get_thumb('GridView', false, $category_id);

                        $render_data['html'] .= '                <li class="col-md-3 col-xs-6">' . PHP_EOL;
                        $render_data['html'] .= '                    <div class="uploadable-sorter-box" data-filename="' . $media . '">' . PHP_EOL;
                        $render_data['html'] .= '                        <div class="uploadable-sorter-box-item text-center">' . PHP_EOL;
                        // TODO: get default backend image size (300x300) or something...
                        $render_data['html'] .= '                            <img class="img-rounded uploadable-dragger ui-sortable-handle ' . $mediaThumb->getClass() . '" src="' . $mediaThumb->getSrc() . '">' . PHP_EOL;
                        $render_data['html'] .= '                            <input type="hidden" name="ad_media_gallery[]" value="' . $media_obj->id . '">' . PHP_EOL;
                        $render_data['html'] .= '                        </div>' . PHP_EOL;
                        $render_data['html'] .= '                        <div class="uploadable-sorter-options  btn-group btn-group-justified">' . PHP_EOL;
                        $render_data['html'] .= '                            <div class="btn-group btn-group-sm">' . PHP_EOL;
                        $render_data['html'] .= '                                <span class="btn btn-default btn-sm set-as-main' . ($currMedia == 1 ? ' disabled' : '') . '" title="Postavi kao glavnu">' . PHP_EOL;
                        $render_data['html'] .= '                                    <span class="fa fa-thumb-tack"><!--IE--></span>' . PHP_EOL;
                        $render_data['html'] .= '                                </span>' . PHP_EOL;
                        $render_data['html'] .= '                            </div>' . PHP_EOL;
                        $render_data['html'] .= '                            <div class="btn-group btn-group-sm">' . PHP_EOL;
                        $render_data['html'] .= '                                <span class="btn btn-default btn-sm move-prev' . ($currMedia == 1 ? ' disabled' : '') . '" title="Pomakni lijevo">' . PHP_EOL;
                        $render_data['html'] .= '                                    <span class="fa fa-caret-left"><!--IE--></span>' . PHP_EOL;
                        $render_data['html'] .= '                                </span>' . PHP_EOL;
                        $render_data['html'] .= '                            </div>' . PHP_EOL;
                        $render_data['html'] .= '                            <div class="btn-group btn-group-sm">' . PHP_EOL;
                        $render_data['html'] .= '                                <span class="btn btn-default btn-sm move-next' . ($currMedia == $mediaCount ? ' disabled' : '') . '" title="Pomakni desno">' . PHP_EOL;
                        $render_data['html'] .= '                                    <span class="fa fa-caret-right"><!--IE--></span>' . PHP_EOL;
                        $render_data['html'] .= '                                </span>' . PHP_EOL;
                        $render_data['html'] .= '                            </div>' . PHP_EOL;
                        $render_data['html'] .= '                            <div class="btn-group btn-group-sm">' . PHP_EOL;
                        $render_data['html'] .= '                                <span class="btn btn-danger btn-sm delete" title="Obriši">' . PHP_EOL;
                        $render_data['html'] .= '                                    <span class="fa fa-trash-o text-danger"><!--IE--></span>' . PHP_EOL;
                        $render_data['html'] .= '                                </span>' . PHP_EOL;
                        $render_data['html'] .= '                            </div>' . PHP_EOL;
                        $render_data['html'] .= '                        </div>' . PHP_EOL;
                        if ('backend' === $this->module) {
                            $render_data['html'] .= '                        <div style="padding-top:5px">' . PHP_EOL;
                            $render_data['html'] .= '                            <a href="/admin/media/edit/' . $media_obj->id . '"><span class="fa fa-picture-o fa-fw"></span>Edit</a>' . PHP_EOL;
                            $render_data['html'] .= '                            <a href="' . $media_obj->get_url() . '" target="_blank"><span class="fa fa-download fa-fw"></span>Download</a>' . PHP_EOL;
                            $render_data['html'] .= '                        </div>' . PHP_EOL;
                        }
                        $render_data['html'] .= '                    </div>' . PHP_EOL;
                        $render_data['html'] .= '                </li>' . PHP_EOL;
                    }
                    $currMedia++;
                }
            }
            $render_data['html'] .= '            </ul>' . PHP_EOL;
            $render_data['html'] .= '        </div>';

            $max_items = $this->parameter_settings->max_items ? intval($this->parameter_settings->max_items) : 0;

            // TODO: add edit link/button for each media item for admin purposes?

            $admin_edit_media_box = '';
            $admin_edit_btn_append = '';
            if ('backend' === $this->module) {
                $admin_edit_media_box = "var \$editBtn = html_builder('<div style=\"padding-top:5px\"><a href=\"/admin/media/edit/' + media.id + '\"><span class=\"fa fa-picture-o fa-fw\"></span>Edit</a></div>');";
                $admin_edit_btn_append = ".append(\$editBtn)";
            }

            $render_data['js'] .= <<<JS

    function uploadableParam_append(data, selector) {
        var html_builder = function(str) {
            var \$html = \$('<div/>').append(str);
            return \$html.children();
        };

        \$.each(data.media, function(idx, media){
            var \$remover = html_builder('<div class="btn-group btn-group-sm"><span class="btn btn-danger btn-sm delete" title="Obriši"><span class="fa fa-trash-o text-danger"><!--IE--></span></span></div>');
            \$remover.bind('click', function(){
                uploadableParam_delete(\$(this));
            });

            var \$next = html_builder('<div class="btn-group btn-group-sm"><span class="btn btn-default btn-sm move-next" title="Pomakni desno"><span class="fa fa-caret-right"><!--IE--></span></span></div>');
            \$next.bind('click', function(){
                uploadableParam_moveNext(\$(this));
            });

            var \$prev = html_builder('<div class="btn-group btn-group-sm"><span class="btn btn-default btn-sm move-prev" title="Pomakni lijevo"><span class="fa fa-caret-left"><!--IE--></span></span></div>');
            \$prev.bind('click', function(){
                uploadableParam_movePrev(\$(this));
            });

            var \$main = html_builder('<div class="btn-group btn-group-sm"><span class="btn btn-default btn-sm set-as-main" title="Postavi kao glavnu"><span class="fa fa-thumb-tack"><!--IE--></span></span></div>');
            \$main.bind('click', function(){
                uploadableParam_setAsMain(\$(this));
            });

            $admin_edit_media_box

            var \$buttons = html_builder('<div class="uploadable-sorter-options btn-group btn-group-justified"></div>');
            \$buttons.append(\$main).append(\$prev).append(\$next).append(\$remover);

            \$('<li/>')
                .addClass('col-md-3 col-xs-6')
                .append(
                    \$('<div/>')
                        .addClass('uploadable-sorter-box')
                        .attr('data-url', media.preview.src).data('url', media.preview.src)
                        .append(
                            \$('<div/>')
                                .addClass('uploadable-sorter-box-item text-center')
                                .append(
                                    \$('<img/>')
                                        .addClass('img-rounded uploadable-dragger ui-sortable-handle')
                                        .attr('src', media.preview.src)
                                )
                                .append(
                                    \$('<input/>')
                                        .attr('type', 'hidden')
                                        .attr('name', \$(selector).data('form') + '[]')
                                        .attr('value', media.id)
                                )
                        )
                        .append(\$buttons)
                        $admin_edit_btn_append
                )
                .fadeIn('fast', function(){
                    \$(this).appendTo(selector);
                    uploadableParam_modifyFirstLast();
                });
        });
    }
    function uploadableParam_setAsMain(\$sender) {
        \$sender.closest('li').prependTo(\$sender.closest('ul'));
        uploadableParam_modifyFirstLast();
    }
    function uploadableParam_movePrev(\$sender) {
        var item = \$sender.closest('li');
        if (item.length > 0) {
            var prev = item.prev();
            if (0 === prev.length) {
                return;
            }
            item.insertBefore(prev);
        }
        uploadableParam_modifyFirstLast();
    }
    function uploadableParam_moveNext(\$sender) {
        var item = \$sender.closest('li');
        if (item.length > 0) {
            var next = item.next();
            if (0 === next.length) {
                return;
            }
            item.insertAfter(next);
        }
        uploadableParam_modifyFirstLast();
    }
    function uploadableParam_delete(\$sender) {
        \$sender.closest('li').fadeOut('fast', function(){
            \$container = \$sender.closest('ul');
            \$(this).remove();
            uploadableParam_modifyFirstLast();
        });
    }
    function uploadableParam_modifyFirstLast() {
        // disable the first's .set-as-main and .move-left buttons
        \$('.uploadable-sorter li:first').find('.set-as-main, .move-prev').addClass('disabled');
        // disable .move-next too if we only have one sortable element
        var cnt = \$('.uploadable-sorter li').length;
        if (cnt <= 1) {
            \$('.uploadable-sorter li:first').find('.move-next').addClass('disabled');
        } else {
            // more than one available, enable those buttons now even on the first one
            \$('.uploadable-sorter li:first').find('.move-next').removeClass('disabled');
        }
        // enable all those potentially disabled buttons on all others except the first one
        \$('.uploadable-sorter li').not(':eq(0)').find('.set-as-main, .move-prev, .move-next').removeClass('disabled');

        // disable next button on last one
        \$('.uploadable-sorter li:last').find('.move-next').addClass('disabled');
    }
    \$('#$parameter_id').fileupload({
        url: '/ajax/upload',
        dataType: 'json',
        dropZone: null,
        pasteZone: null,
        limitMultiFileUploads: $max_items,
        // acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        acceptFileTypes: /(\.|\/)(jpe?g|png)$/i,
        done: function (e, data) {
            if ('undefined' !== data.result) {
                if (data.result.ok) {
                    uploadableParam_append(data.result, '#$parameter_id_gallery');
                    //\$('<p/>').text(data.result.picture.filename).appendTo('#$parameter_id_gallery');
                } else {
                    // TODO: handle failure in some way...
                }
            }
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#$parameter_id_progress .progress-bar').css(
                'width',
                progress + '%'
            );
        },
        start: function(e) {
            $('#$parameter_id_progress').removeClass('hidden');
        },
        stop: function(e) {
            $('#$parameter_id_progress').addClass('hidden');
        }
    }).prop('disabled', !\$.support.fileInput)
      .parent().addClass(\$.support.fileInput ? undefined : 'disabled');

    \$('#$parameter_id_gallery').sortable({
        forcePlaceholderSize: true,
        handle: '.uploadable-dragger',
        helper: 'clone',
        listType: 'ul',
        items: 'li',
        opacity: .6,
        placeholder: 'col-md-3 col-lg-3 placeholder',
        revert: 250,
        tabSize: 25,
        create: function(event, ui) {
            // initialize set-as-main buttons
            \$('.uploadable-sorter .uploadable-sorter-options .btn.set-as-main').click(function(){
                uploadableParam_setAsMain(\$(this));
            });
            // initialize move-prev buttons
            \$('.uploadable-sorter .uploadable-sorter-options .btn.move-prev').click(function(){
                uploadableParam_movePrev(\$(this));
            });
            // initialize move-next buttons
            \$('.uploadable-sorter .uploadable-sorter-options .btn.move-next').click(function(){
                uploadableParam_moveNext(\$(this));
            });
            // initialize delete buttons
            \$('.uploadable-sorter .uploadable-sorter-options .btn.delete').click(function(){
                uploadableParam_delete(\$(this));
            });
        },
        stop: function(event, ui) {
            uploadableParam_modifyFirstLast();
        }
    });
JS;

            $render_data['html'] .= '    </div>' . PHP_EOL;
            $render_data['html'] .= '</div>' . PHP_EOL;
        }

        return $render_data;
    }

}
