<?php

namespace Baseapp\Library\Parameters;

class DateIntervalParameter extends BaseParameter
{
    protected $group_type = 'custom';
    protected $settings_location = 'data';
    protected $type = 'DATEINTERVAL';
    protected $accept_dictionary = false;
    protected $can_default = false;
    protected $can_default_type = '';
    protected $can_other = false;
    protected $can_prefix_suffix = false;
    protected $is_dependable = false;
    protected $is_required = false;
    public $is_searchable = false;
    public $is_searchable_type = array('INTERVAL_DATE');

    public function setValue($values)
    {
        if (is_array($values) && count($values)) {
            $prepared_json = array(
                'type' => 'dateinterval',
                'from' => array(
                    'value' => null,
                    'text_value' => null
                ),
                'to' => array(
                    'value' => null,
                    'text_value' => null
                )
            );
            $range = array();

            $tmp_from = null;
            $tmp_to = null;
            if (isset($values['from'])) {
                $tmp_from = strtotime(trim($values['from']));
            }
            if (isset($values['to'])) {
                $tmp_to = strtotime(trim($values['to']));
            }
            if ($tmp_from || $tmp_to) {
                if ($tmp_from && $tmp_to) {
                    $prepared_json = array(
                        'from' => array(
                            'value' => date('Y-m-d', min($tmp_from, $tmp_to)),
                            'text_value' => date('d.m.Y', min($tmp_from, $tmp_to))
                        ),
                        'to' => array(
                            'value' => date('Y-m-d', max($tmp_from, $tmp_to)),
                            'text_value' => date('d.m.Y', max($tmp_from, $tmp_to))
                        )
                    );
                    $range = array(
                        'from' => date('Y-m-d', min($tmp_from, $tmp_to)),
                        'to' => date('Y-m-d', max($tmp_from, $tmp_to))
                    );
                } else {
                    if ($tmp_from) {
                        $prepared_json['from'] = array(
                            'value' => date('Y-m-d', $tmp_from),
                            'text_value' => date('d.m.Y', $tmp_from)
                        );
                        $range = array(
                            'from' => date('Y-m-d', $tmp_from)
                        );
                    }
                    if ($tmp_to) {
                        $prepared_json['to'] = array(
                            'value' => date('Y-m-d', $tmp_to),
                            'text_value' => date('d.m.Y', $tmp_to)
                        );
                        $range = array(
                            'to' => date('Y-m-d', $tmp_to)
                        );
                    }
                }

                $this->setPreparedData(array(
                    'id' => (int) $this->parameter->id,
                    'range' => $range
                ));
                $this->setPreparedDataJson(
                    'ad_params_' . $this->parameter->id,
                    $prepared_json
                );
            }
        }
    }

    public function process()
    {
        if ($this->validation && $this->parameter_settings->is_required) {
            $this->validation->add('ad_params_' . $this->parameter->id, new \Baseapp\Extension\Validator\Interval(array(
                'messageInvalidData' => $this->getValidationString('invalid_data'),
                'messageInvalidRange' => $this->getValidationString('invalid_range'),
                'message' => $this->getValidationString('at_least_one_field_required')
            )));
        }

        $values = array();
        if (isset($this->data['ad_params_' . $this->parameter->id . '_from'])) {
            $values['from'] = strtotime(trim($this->data['ad_params_' . $this->parameter->id . '_from']));
        }
        if (isset($this->data['ad_params_' . $this->parameter->id . '_to'])) {
            $values['to'] = strtotime(trim($this->data['ad_params_' . $this->parameter->id . '_to']));
        }
        $this->setValue($values);
    }

    public function render_input()
    {
        parent::render_input();

        $render_data = array(
            'row'  => false,
            'html' => '',
            'js'   => ''
        );

        if (!$this->is_hidden) {
            $parameter_id = 'parameter_' . $this->parameter->id;
            $parameter_id_options = $parameter_id . '_options';
            $parameter_id_from = $parameter_id . '_from';
            $parameter_id_to = $parameter_id . '_to';
            $parameter_name = 'ad_params_' . $this->parameter->id;
            $parameter_name_from = $parameter_name . '_from';
            $parameter_name_to = $parameter_name . '_to';
            $required_html = ($this->parameter_settings->is_required ? ' <abbr title="' . $this->getValidationString('required_field') . '">*</abbr>' : '');

            $parameter_default_value_from = '';
            $parameter_default_value_to = '';
            $jsdate_from = null;
            $jsdate_to = null;
            if (isset($this->data)) {
                $tmp_from = null;
                $tmp_to = null;
                if (isset($this->data[$parameter_name_from])) {
                    $tmp_from = strtotime(trim($this->data[$parameter_name_from]));
                }
                if (isset($this->data[$parameter_name_to])) {
                    $tmp_to = strtotime(trim($this->data[$parameter_name_to]));
                }
                if ($tmp_from && $tmp_to) {
                    $parameter_default_value_from = date('d.m.Y', min($tmp_from, $tmp_to));
                    $jsdate_from = date('Y-m-d', min($tmp_from, $tmp_to));
                    $parameter_default_value_to = date('d.m.Y', max($tmp_from, $tmp_to));
                    $jsdate_to = date('Y-m-d', max($tmp_from, $tmp_to));
                } else {
                    if ($tmp_from) {
                        $parameter_default_value_from = date('d.m.Y', $tmp_from);
                        $jsdate_from = date('Y-m-d', $tmp_from);
                    }
                    if ($tmp_to) {
                        $parameter_default_value_to = date('d.m.Y', $tmp_to);
                        $jsdate_to = date('Y-m-d', $tmp_to);
                    }
                }
            }

            $parameter_placeholder = ($this->parameter_settings->placeholder_text ? ' placeholder="' . $this->parameter_settings->placeholder_text . '"' : '');

            $div_class = $this->render_as_full_row ? 'col-lg-12' : 'col-lg-6 col-md-12';
            $render_data['html'] .= '<div class="' . $div_class . '">' . PHP_EOL;
            $render_data['html'] .= '    <div class="form-group' . (isset($this->errors) && $this->errors->filter($parameter_name) ? ' has-error' : '') . '">' . PHP_EOL;
            $render_data['html'] .= '        <label class="control-label" for="' . $parameter_id . '">' . trim($this->parameter_settings->label_text) . $required_html . '</label>' . PHP_EOL;

            $render_data['html'] .= '        <div class="row">' . PHP_EOL;
            // From
            $render_data['html'] .= '            <div class="col-sm-6 col-md-6 col-lg-6">' . PHP_EOL;
            $render_data['html'] .= '                <div class="input-group date">' . PHP_EOL;
            $render_data['html'] .= '                    <span class="input-group-addon">' . $this->getValidationString('range_from') . '</span>' . PHP_EOL;
            $render_data['html'] .= '                    <input type="hidden" name="' . $parameter_name_from . '" id="' . $parameter_id_from . '_hidden" value="' . ($jsdate_from ? $jsdate_from : '') . '" />' . PHP_EOL;
            $render_data['html'] .= '                    <input class="form-control" id="' . $parameter_id_from . '" value=""' . $parameter_placeholder . ' />' . PHP_EOL;
            $render_data['html'] .= '                </div>' . PHP_EOL;
            $render_data['html'] .= '            </div>' . PHP_EOL;

            // To
            $render_data['html'] .= '            <div class="col-sm-6 col-md-6 col-lg-6">' . PHP_EOL;
            $render_data['html'] .= '                <div class="input-group date">' . PHP_EOL;
            $render_data['html'] .= '                    <span class="input-group-addon">' . $this->getValidationString('range_to') . '</span>' . PHP_EOL;
            $render_data['html'] .= '                    <input type="hidden" name="' . $parameter_name_to . '" id="' . $parameter_id_to . '_hidden" value="' . ($jsdate_to ? $jsdate_to : '') . '" />' . PHP_EOL;
            $render_data['html'] .= '                    <input class="form-control" id="' . $parameter_id_to . '" value=""' . $parameter_placeholder . ' />' . PHP_EOL;
            $render_data['html'] .= '                </div>' . PHP_EOL;
            $render_data['html'] .= '            </div>' . PHP_EOL;
            $render_data['html'] .= '        </div>' . PHP_EOL;

            if (isset($this->errors) && $this->errors->filter($parameter_name)) {
                $render_data['html'] .= '      <p class="help-block">' . current($this->errors->filter($parameter_name))->getMessage() . '</p>' . PHP_EOL;
            } elseif (isset($this->parameter_settings->help_text) && trim($this->parameter_settings->help_text)) {
                $render_data['html'] .= '      <p class="help-block">' . trim($this->parameter_settings->help_text) . '</p>' . PHP_EOL;
            }

            $render_data['html'] .= '    </div>' . PHP_EOL;
            $render_data['html'] .= '</div>' . PHP_EOL;

            $parameter_range_from_initial_value = ($jsdate_from ? 'new Date("' . $jsdate_from . '")' : 'null');
            $parameter_range_to_initial_value = ($jsdate_to ? 'new Date("' . $jsdate_to . '")' : 'null');

            $render_data['js'] .= <<<JS

    var $parameter_id_options = {
        id: '$parameter_id',
        from: $parameter_range_from_initial_value,
        to: $parameter_range_to_initial_value,
    };
    \$('#$parameter_id_from, #$parameter_id_to').handleDateRange($parameter_id_options);
JS;
        }

        return $render_data;
    }

}
