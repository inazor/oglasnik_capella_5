<?php

namespace Baseapp\Library\Parameters;

class UrlParameter extends BaseParameter
{
    protected $group_type = 'custom';
    protected $settings_location = 'data';
    protected $type = 'URL';
    protected $accept_dictionary = false;
    protected $can_default = true;
    protected $can_default_type = 'text';
    protected $can_other = false;
    protected $can_prefix_suffix = true;
    protected $is_dependable = false;
    protected $is_required = false;
    public $is_searchable = false;
    public $is_searchable_type = array('CHECKBOX');

    public function setValue($value)
    {
        if (trim($value)) {
            $this->setPreparedData(array(
                'id' => (int) $this->parameter->id,
                'value' => (string) $value
            ));

            $this->setPreparedDataJson(
                'ad_params_' . $this->parameter->id,
                array(
                    'text_value' => (string) $value
                )
            );
        }
    }

    public function process()
    {
        $this->validate_cannot_be_empty();
        if (isset($this->data['ad_params_' . $this->parameter->id])) {
            $this->setValue(trim(strip_tags($this->data['ad_params_' . $this->parameter->id])));
        }
    }

    public function render_input()
    {
        parent::render_input();

        $render_data = array(
            'row'  => false,
            'html' => '',
            'js'   => ''
        );

        if (!$this->is_hidden) {
            $parameter_id = 'parameter_' . $this->parameter->id;
            $parameter_id_preview_btn = $parameter_id . '_preview_btn';
            $parameter_name = 'ad_params_' . $this->parameter->id;
            $required_html = ($this->parameter_settings->is_required ? ' <abbr title="' . $this->getValidationString('required_field') . '">*</abbr>' : '');

            if (isset($this->data) && isset($this->data[$parameter_name])) {
                $parameter_default_value = trim($this->data[$parameter_name]);
            } else {
                $parameter_default_value = ($this->parameter_settings->default_value ? $this->parameter_settings->default_value : '');
            }
            $parameter_default_value = strip_tags($parameter_default_value);

            $parameter_placeholder = ($this->parameter_settings->placeholder_text ? ' placeholder="' . $this->parameter_settings->placeholder_text . '"' : '');

            $div_class = $this->render_as_full_row ? 'col-lg-12' : 'col-lg-6 col-md-6';
            $render_data['html'] .= '<div class="' . $div_class . '">' . PHP_EOL;
            $render_data['html'] .= '    <div class="form-group' . (isset($this->errors) && $this->errors->filter($parameter_name) ? ' has-error' : '') . '">' . PHP_EOL;
            $render_data['html'] .= '        <label class="control-label" for="' . $parameter_id . '">' . trim($this->parameter_settings->label_text) . $required_html . '</label>' . PHP_EOL;
            $render_data['html'] .= '        <div class="input-group">';
            $render_data['html'] .= '            <input type="text" class="form-control" name="' . $parameter_name . '" id="' . $parameter_id . '" value="' . $parameter_default_value . '"' . $parameter_placeholder . ' />' . PHP_EOL;
            $render_data['html'] .= '            <span class="input-group-btn">';
            $render_data['html'] .= '                <button class="btn btn-primary" type="button" id="' . $parameter_id_preview_btn . '"><span class="fa fa-globe"></span>&nbsp;&nbsp; Pogledaj</button>';
            $render_data['html'] .= '            </span>';
            $render_data['html'] .= '        </div>';

            if (isset($this->errors) && $this->errors->filter($parameter_name)) {
                $render_data['html'] .= '      <p class="help-block">' . current($this->errors->filter($parameter_name))->getMessage() . '</p>' . PHP_EOL;
            } elseif (isset($this->parameter_settings->help_text) && trim($this->parameter_settings->help_text)) {
                $render_data['html'] .= '      <p class="help-block">' . trim($this->parameter_settings->help_text) . '</p>' . PHP_EOL;
            }

            $render_data['html'] .= '    </div>' . PHP_EOL;
            $render_data['html'] .= '</div>' . PHP_EOL;

            $render_data['js'] = <<<JS

    \$('#$parameter_id_preview_btn').click(function(){
        var preview_url = \$.trim(\$('#$parameter_id').val());
        if (preview_url) {
            $.post(
                '/ajax/check-video-url', {
                    'url': preview_url
                },
                function (json) {
                    if (json.status) {
                        preview_url = json.url;
                    }
                    $.fancybox({
                        type: 'iframe',
                        href: preview_url
                    });
                },
                'json'
            );
        }
    });
JS;
        }

        return $render_data;
    }

}
