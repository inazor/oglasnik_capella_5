<?php

namespace Baseapp\Library\Parameters;

use Baseapp\Models\Dictionaries as Dictionary;

class RadioParameter extends BaseParameter
{
    protected $group_type = 'custom';
    protected $settings_location = 'data';
    protected $type = 'RADIO';
    protected $accept_dictionary = true;
    protected $can_default = true;
    protected $can_default_type = 'dropdown';
    protected $can_other = true;
    protected $can_prefix_suffix = false;
    protected $is_dependable = false;
    protected $is_required = false;
    public $is_searchable = false;
    public $is_searchable_type = array('CHECKBOX', 'RADIO', 'DROPDOWN');

    public function setValue($value)
    {
        if (trim($value)) {
            $json_data = array();
            if (is_numeric($value)) {
                $value = (int) $value;
                $json_data['dictionary_id'] = $value;

                $dictionary = Dictionary::findFirst($value);
                if ($dictionary) {
                    $json_data['text_value'] = $dictionary->name;
                }
            }

            $this->setPreparedData(array(
                'id' => (int) $this->parameter->id,
                'value' => $value
            ));

            if (count($json_data)) {
                $this->setPreparedDataJson(
                    'ad_params_' . $this->parameter->id,
                    $json_data
                );
            }
        }
    }

    public function process()
    {
        $this->validate_cannot_be_empty();
        if (isset($this->data['ad_params_' . $this->parameter->id])) {
            $this->setValue(trim(strip_tags($this->data['ad_params_' . $this->parameter->id])));
        }
    }

    public function render_input()
    {
        parent::render_input();

        $render_data = array(
            'row'  => false,
            'html' => '',
            'js'   => ''
        );

        if (!$this->is_hidden) {
            $parameter_id = 'parameter_' . $this->parameter->id;
            $parameter_name = 'ad_params_' . $this->parameter->id;

            $selected_value = $this->parameter_settings->default_value ? intval($this->parameter_settings->default_value) : null;
            if (isset($this->data) && isset($this->data[$parameter_name])) {
                $selected_value = intval($this->data[$parameter_name]);
            }

            $required_html = ($this->parameter_settings->is_required ? ' <abbr title="' . $this->getValidationString('required_field') . '">*</abbr>' : '');

            $div_class = $this->render_as_full_row ? 'col-lg-12' : 'col-lg-3 col-md-3';
            $render_data['html'] .= '<div class="' . $div_class . '">' . PHP_EOL;
            $render_data['html'] .= '    <label>' . trim($this->parameter_settings->label_text) . $required_html . '</label>' . PHP_EOL;

            // for loop begin
            $dictionary_level_values = $this->parameter->getDictionaryValues($this->parameter->dictionary_id);
            if ($dictionary_level_values) {
                $first_radio_margin = ' style="margin-top:0"';
                $render_data['html'] .= '    <div class="main_radio_container' . (isset($this->errors) && $this->errors->filter($parameter_name) ? ' has-error' : '') . '">' . PHP_EOL;

                foreach ($dictionary_level_values as $value) {
                    $radio_checked = $selected_value && $selected_value == intval($value->id) ? ' checked="checked"' : '';

                    $render_data['html'] .= '        <div class="radio radio-primary"' . $first_radio_margin . '>' . PHP_EOL;
                    $render_data['html'] .= '            <input type="radio" name="' . $parameter_name . '" id="' . $parameter_name . '_' . $value->id . '" value="' . $value->id . '"' . $radio_checked . ' />' . PHP_EOL;
                    $render_data['html'] .= '            <label class="control-label" for="' . $parameter_name . '_' . $value->id . '">' . trim($value->name) . '</label>' . PHP_EOL;
                    $render_data['html'] .= '        </div>' . PHP_EOL;
                    $first_radio_margin = '';
                }

                if (isset($this->errors) && $this->errors->filter($parameter_name)) {
                    $render_data['html'] .= '      <p class="help-block">' . current($this->errors->filter($parameter_name))->getMessage() . '</p>' . PHP_EOL;
                } elseif (isset($this->parameter_settings->help_text) && trim($this->parameter_settings->help_text)) {
                    $render_data['html'] .= '      <p class="help-block">' . trim($this->parameter_settings->help_text) . '</p>' . PHP_EOL;
                }

                $render_data['html'] .= '    </div>' . PHP_EOL;
            }

            $render_data['html'] .= '</div>' . PHP_EOL;
        }

        return $render_data;
    }

}
