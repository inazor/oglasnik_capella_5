<?php

namespace Baseapp\Library;

use Phalcon\DispatcherInterface;
use Phalcon\Mvc\Dispatcher as PhDispatcher,
    Phalcon\Events\Event as PhEvent,
    Phalcon\Di,
    Phalcon\Mvc\Dispatcher\Exception as PhDispatcherException;
use Phalcon\Mvc\View;
use \Baseapp\Extension\PrettyExceptions;

/**
 * ErrorHandler Plugin
 *
 * Handles error logging (and displaying/forwarding to pretty error handlers)
 *
 * Extending \Phalcon\Mvc\User\Plugin since we need to implement/hook
 * methods as events of some Phalcon components (in this case the Dispatcher)
 */
class ErrorHandler extends \Phalcon\Mvc\User\Plugin
{
    /**
     * @var \Phalcon\DiInterface
     */
    protected $di;

    /**
     * @var \Phalcon\Config\Adapter\Ini
     */
    protected $config;

    /**
     * @var \Phalcon\Logger\Adapter
     */
    protected $logger;

    public function __construct(\Phalcon\DiInterface $di = null)
    {
        if (null === $di) {
            $di = Di::getDefault();
        }

        $this->di = $di;
        if ($this->di->has('config')) {
            $this->config = $this->di->get('config');
        }
        if ($this->di->has('logger')) {
            $this->logger = $this->di->get('logger');
        }
    }

    /**
     * Runs on dispatch:beforeDispatchLoop event
     * @link http://docs.phalconphp.com/en/latest/reference/dispatching.html
     *
     * @param PhEvent $event
     * @param DispatcherInterface $dispatcher
     *
     * @return bool
     */
    public function beforeDispatchLoop(PhEvent $event, $dispatcher)
    {
        // Checks for the presence of the config/.maintenance file and
        // forwards the request to our custom 503 page if the file exists
        if (file_exists(ROOT_PATH . '/config/.maintenance')) {
            $dispatcher->forward(array(
                'controller' => 'error',
                'action' => 'show503'
            ));
            // Returning false cancels the entire dispatch loop it seems,
            // and then we don't get our custom error page displayed...
            return true;
        }
    }

    /**
     * Hooked into 'dispatch:beforeException'
     * Forwards exceptions to the desired ErrorController action and logs them
     *
     * @param PhEvent $event
     * @param DispatcherInterface $dispatcher
     * @param \Exception $exception
     *
     * @return bool
     */
    public function beforeException(PhEvent $event, DispatcherInterface $dispatcher, $exception)
    {
        /*while (ob_get_level()) {
            ob_end_clean();
        }*/

        // Cli Module has slightly different dispatch parameters for some reason...
        // and we're bailing to a notFound action no matter what for now...
        if ('cli' === PHP_SAPI) {
            $dispatcher->forward(array(
                'task' => 'main',
                'action' => 'exception',
                'params' => array($exception)
            ));
            return false;
        }

        // Default to 404 for everything
        $dispatcher_params = array(
            'controller' => 'error',
            'action' => 'show404',
            // 'params' => array($exception)
        );

        // Handle explicit 404 dispatch exceptions
        if ($exception instanceof PhDispatcherException) {
            switch ($exception->getCode()) {
                case PhDispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                case PhDispatcher::EXCEPTION_ACTION_NOT_FOUND:
                    $dispatcher->forward($dispatcher_params);
                    return false;
            }
        }

        /*
        if ($exception instanceof CrudActionsException) {
            $this->flashSession->error($exception->getMessage());
            $this->redirect_back();
            return false;
        }
        */

        // Let our exception handler do it's thing
        $this->exception_handler($exception);

        // for other kinds of errors, the default is now 500
        $dispatcher_params['action'] = 'show500';

        // Handle our custom types of exceptions here
        // TODO: add more types/exceptions as they get introduced

        if ($exception instanceof \Baseapp\Extension\CsrfException) {
            $dispatcher_params['action'] = 'show400';
        }

        /*var_dump($dispatcher_params);
        exit;*/

        // Forwarding to a pretty error page
        $dispatcher->forward($dispatcher_params);
        return false;
    }

    public function error_handler($type, $message, $file, $line, $context = array())
    {
        // If the error was suppressed with the @-operator, log that too, but slightly differently
        if (0 === error_reporting()) {
            $type = '@suppressed ' . self::type_int2str($type);
        }

        $this->log($type, $message, $file, $line);

        /**
         * Returning false from a callback set via set_error_handler()
         * lets the normal error handler continue executing
         * This might not be always wanted/needed, but it makes testing error handling
         * a lot easier currently
         */
        // return false;

        /*
        // Don't execute any further error handlers (including PHP's internal one)
        return true;
        */
    }

    /**
     * Exception handler callback (Can be registered via set_exception_handler())
     *
     * @param \Exception $ex
     *
     * @return \Phalcon\Http\Response
     */
    public function exception_handler($ex)
    {
        $this->log_exception($ex);

        // TODO: ideally, this should be handled in a more testable/reusable/http-y kind of way:
        // - prepare and get the generated exception dump as a string
        // - create a new response object, set 500 response headers and set the response content
        // - send/output the response
        // But, \Phalcon\Debug currently doesn't support getting output, it always echos.
        // Also, the dispatcher has already finished or something so any 'application:beforeSendResponse' events
        // are missed if done this way... so we don't get our custom headers and shit...
        // Gonna try looking into it some more.

        if ($this->config && 'development' === $this->config->app->env) {
            $this->disable_view();

            // override whatever was already in the response with the debug output
            /* @var $response \Phalcon\Http\Response */
            $response = $this->di->getShared('response');
            $response->resetHeaders();
            $response->setStatusCode(500, 'Internal server error');

            if (!$this->request->isAjax()) {
                $dbg = $this->get_debug_output($ex);
                $response->setContent($dbg);
            } else {
                $response->setContentType('application/json', 'UTF-8');
                $response->setJsonContent(array('error' => $ex->getMessage()));
            }

            if ($this->di->has('app')) {
                $this->di->get('app')->add_extra_headers($response);
            }

            $response->send();
            exit;
        }
    }

    public function get_debug_output($ex) {
        $debug = new PrettyExceptions();
        $contents = $debug->onUncaughtException($ex);
        return $contents;
    }

    /**
     * Hopefully makes sure no unwanted output of any kind gets spit out from the view component
     */
    public function disable_view()
    {
        if ($this->di->has('view')) {
            /* @var \Phalcon\Mvc\View $view */
            $view = $this->di->get('view');
            $view->disable();
            $view->finish();
        }
    }

    /**
     * Registered via register_shutdown_function(), so it can catch fatal errors too and log them
     */
    public function shutdown_handler()
    {
        $error = error_get_last();
        // only log fatal errors, not any last error that could've occurred anywhere (including PHPUnit and such)
        if (null !== $error && $error['type'] === E_ERROR) {
            $this->disable_view();
            $this->log($error['type'], $error['message'], $error['file'], $error['line']);
        }
    }

    /**
     * @param \Exception $ex
     * @param string $type
     */
    public function log_exception($ex, $type = 'Exception'){
        $this->log($type, $ex->getMessage(), $ex->getFile(), $ex->getLine(), $ex->getTraceAsString());
    }

    /**
     * @param $type
     * @param $message
     * @param $file
     * @param $line
     * @param string $trace
     *
     * @throws \Exception
     */
    public function log($type, $message, $file, $line, $trace = '')
    {
        // [$ip $url] $type: $message in $file on line $line
        $tpl = '[%s %s] %s: %s in %s on line %s';

        if ($trace) {
            $tpl .= "\n" . $trace;
        }

        if (is_int($type)) {
            $type = static::type_int2str($type);
        }

        if ('cli' === PHP_SAPI) {
            $ip  = 'cli';
            $uri = implode(' ', $_SERVER['argv']);
        } else {
            /**
             * @var \Phalcon\Http\Request $req
             */
            $req = $this->di->getShared('request');
            $ip  = $req->getClientAddress(true);
            $uri = $req->getMethod() . ' ' . $req->getURI();
        }

        $log_msg = '[' . $ip . ' ' . $uri . '] ' . $type . ': ' . $message . ' in ' . $file . ' on line ' . $line;

        // This way it should end up somewhere at least, regardless of our custom logger existing or not
        error_log($log_msg);

        if ($this->logger) {
            $this->logger->error($log_msg);
            // Attempting to hook into Prophiler's logging too
            if ($this->di->has('prophilerLogger')) {
                $logger = $this->di->getShared('prophilerLogger');
                $logger->error($log_msg);
            }
        } else {
            throw new \Exception($log_msg);
        }
    }

    /**
     * Converts PHP's native error types/constants into a string value for logging purposes
     *
     * @param int $type PHP Error level constant/type
     *
     * @return string
     */
    public static function type_int2str($type)
    {
        switch ($type) {
            case E_NOTICE:
            case E_USER_NOTICE:
                $string = 'Notice';
                break;
            case E_WARNING:
            case E_CORE_WARNING:
            case E_USER_WARNING:
                $string = 'Warning';
                break;
            case E_STRICT:
                $string = 'Strict mode';
                break;
            case E_ERROR:
            case E_PARSE:
            case E_CORE_ERROR:
            case E_COMPILE_ERROR:
            case E_COMPILE_WARNING:
            case E_USER_ERROR:
                $string = 'Fatal Error';
                break;
            default:
                $string = 'Unknown';
        }
        return $string;
    }
}
