<?php

namespace Baseapp\Library;

/**
 * Simple DB wrapper (raw sql helper)
 */
class RawDB
{
    private $db = null;

    public function __construct($options = null)
    {
        $db_settings = array();

        if ($options && is_array($options)) {
            if (isset($options['host']) && trim($options['host'])) {
                $db_settings['host'] = trim($options['host']);
            }
            if (isset($options['username']) && trim($options['username'])) {
                $db_settings['username'] = trim($options['username']);
            }
            if (isset($options['password']) && trim($options['password'])) {
                $db_settings['password'] = trim($options['password']);
            }
            if (isset($options['dbname']) && trim($options['dbname'])) {
                $db_settings['dbname'] = trim($options['dbname']);
                if (isset($options['schema']) && trim($options['schema'])) {
                    $db_settings['schema'] = trim($options['schema']);
                } else {
                    $db_settings['schema'] = trim($options['dbname']);
                }
            }
            if (isset($options['options']) && is_array($options['options'])) {
                $db_settings['options'] = $options['options'];
            }
        }

        if (isset($db_settings['host']) && isset($db_settings['username']) && isset($db_settings['password']) && isset($db_settings['dbname'])) {
            $this->db = new \Phalcon\Db\Adapter\Pdo\Mysql($db_settings);
            $this->db->connect();
        } else {
            if ($db = \Phalcon\Di::getDefault()->getShared('db')) {
                $this->db = $db;
            }
        }
    }

    public function beginTransaction()
    {
        return $this->db->begin();
    }

    public function rollBack()
    {
        return $this->db->rollback();
    }

    public function commit()
    {
        return $this->db->commit();
    }

    public function lastInsertId()
    {
        return $this->db->lastInsertId();
    }

    public function findFirst($sql, $values_array = null, $return_value_if_one = false)
    {
        $result = null;

        if ($rows = $this->query($sql, $values_array)) {
            $result = $rows->fetch();
            if ($return_value_if_one && count($result) == 1) {
                $result = reset($result);
            }
        }

        return $result;
    }

    public function find($sql, $values_array = null)
    {
        $result = $this->query($sql, $values_array);

        if ($result) {
            return $result->fetchAll();
        }

        return null;
    }

    public function query($sql, $values_array = null)
    {
        if ($values_array) {
            $result = $this->db->query($sql, $values_array);
        } else {
            $result = $this->db->query($sql);
        }

        if ($result->numRows()) {
            $result->setFetchMode(\Phalcon\Db::FETCH_ASSOC);
        } else {
            $result = null;
        }

        return $result;
    }

    public function insert($sql, $values_array = null)
    {
        if ($values_array) {
            $result = $this->db->execute($sql, $values_array);
        } else {
            $result = $this->db->execute($sql);
        }

        if ($result) {
            return $this->db->lastInsertId();
        }

        return null;
    }

    public function update($sql, $values_array = null)
    {
        if ($values_array) {
            $result = $this->db->execute($sql, $values_array);
        } else {
            $result = $this->db->execute($sql);
        }

        return $result;
    }

    public function delete($sql, $values_array = null)
    {
        $result = null;

        if ($values_array) {
            $result = $this->db->execute($sql, $values_array);
        } else {
            $result = $this->db->execute($sql);
        }

        return $result;
    }

}
