<?php

namespace Baseapp\Traits;

use Baseapp\Library\Debug;

trait CategoryTreeHelpers
{
    public function getAllDescendantIdsFromTreeArray(&$node)
    {
        $ids = array();

        if ($node->id > 1) {
            $ids[] = $node->id;
        }

        // Loop over node's children and add up any found results
        if (!empty($node->children)) {
            foreach ($node->children as $id => &$childNode) {
                $ids = array_merge($ids, $this->getAllDescendantIdsFromTreeArray($childNode));
            }
        }

        return $ids;
    }

    public function getDescendantIdsWithTransactionTypeFromTreeArray(&$node, $includeSelf = true)
    {
        $ids = array();

        if ($node->id > 1 && $node->transaction_type_id && $includeSelf) {
            $ids[] = $node->id;
        }

        // Loop over node's children and add up any found results
        if (!empty($node->children)) {
            foreach ($node->children as $id => &$childNode) {
                // When going through children, always include themselves
                $ids = array_merge($ids, $this->getDescendantIdsWithTransactionTypeFromTreeArray($childNode, true));
            }
            unset($childNode);
        }

        return $ids;
    }

    public function getDescendantIdsWithTransactionTypeFromTreeArrayWithoutSelf(&$node)
    {
        return $this->getDescendantIdsWithTransactionTypeFromTreeArray($node, false);
    }

}
