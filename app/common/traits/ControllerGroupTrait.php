<?php

namespace Baseapp\Traits;

trait ControllerGroupTrait
{

    /**
     * Group Controllers in a virtual group
     */
    public function set_controller_group($controller_name = null)
    {
        switch ($controller_name) {
            case 'categories':
            case 'sections':
            case 'parameters':
            case 'dictionaries':
            case 'category-parameterization':
            case 'products':
            case 'products-categories':
                $controller_group = 'site-structure-management';
                break;
            case 'ads':
                $controller_group = 'ads';
                break;
            case 'cms-categories':
            case 'cms-articles':
                $controller_group = 'cms';
                break;
            case 'users':
            case 'shops':
                $controller_group = 'users';
                break;
            default:
                $controller_group = null;
        }

        $this->view->setVar('controller_group', $controller_group);
    }

}
