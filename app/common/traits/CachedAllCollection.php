<?php

namespace Baseapp\Traits;

trait CachedAllCollection
{
    /**
     * @return mixed
     */
    public static function findAll()
    {
        return static::find(array('order' => 'id'));
    }

    /**
     * @return mixed
     */
    public static function all()
    {
        return (new static())->cachedAll();
    }

    /**
     * Busts the "all cache" whenever an entity is saved (create/edit).
     *
     * Since this is a Trait, a model can provide specialized behavior by
     * overriding the method in the concrete class.
     */
    public function beforeSave()
    {
        $this->cachedAllInvalidate();
    }

    /**
     * Busts the "all cache" whenever an entity is deleted.
     *
     * Since this is a Trait, a model can provide specialized behavior by
     * overriding the method in the concrete class.
     */
    public function afterDelete()
    {
        $this->cachedAllInvalidate();
    }

    /**
     * Deletes the contents of the "all cache" key
     */
    public function cachedAllInvalidate()
    {
        $cache = $this->cachedAllGetCache();
        if ($cache) {
            $cache->delete($this->cachedAllGetCacheKeyName());
        }
    }

    /**
     * Returns the cache service from the DI container or a result
     * of a cache fetch if the key is specified.
     *
     * @param null|int|string $key Optional cache key to fetch
     *
     * @return \Phalcon\Cache\BackendInterface|mixed
     */
    public function cachedAllGetCache($key = null)
    {
        $cache = null;

        $di = $this->getDI();
        if ($di->has('cache')) {
            /* @var $cache \Phalcon\Cache\BackendInterface */
            $cache = $di->get('cache');
        }

        if (null !== $cache && null !== $key) {
            $cache = $cache->get($key);
        }

        return $cache;
    }

    /**
     * Return an array containing all the stored records keyed on their ids.
     *
     * First checks the cache and then hits the DB (and saves to cache for later).
     *
     * @return array|null|\Phalcon\Mvc\Model\ResultsetInterface
     */
    public function cachedAll()
    {
        $cache_key = $this->cachedAllGetCacheKeyName();

        $records = $this->cachedAllGetCache($cache_key);
        if (!$records) {
            // Cache missed, query the db
            $records = static::findAll();
            if ($records->valid()) {
                $records = $this->cachedAllBuildCacheData($records);
                // Store for later
                $cache = self::cachedAllGetCache();
                if ($cache) {
                    $cache->save($cache_key, $records);
                }
            }
        }

        return $records;
    }

    /**
     * Builds/prepares the data which is to be stored in the "all cache".
     *
     * @param $records \Phalcon\Mvc\Model\Resultset
     *
     * @return array
     */
    public function cachedAllBuildCacheData($records)
    {
        $data = array();

        foreach ($records as $record) {
            $data[$record->id] = $record->toArray();
        }

        return $data;
    }

    /**
     * @return string
     */
    public function cachedAllGetCacheKeyName()
    {
        $cache_key = 'all-' . $this->getSource();

        return $cache_key;
    }
}
