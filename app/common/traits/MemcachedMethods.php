<?php

namespace Baseapp\Traits;

use Baseapp\Bootstrap;
use Phalcon\Cache\Exception as PhCacheException;

trait MemcachedMethods
{
    /**
     * Instance helper method to clear any potential future caches we might add
     *
     * @return bool
     */
    public function deleteMemcachedData()
    {
        return self::deleteMemcachedKeyData();
    }

    /**
     * @return string
     */
    public static function getCachedKeyName()
    {
        return self::MEMCACHED_KEY;
    }

    /**
     * @return bool
     */
    public static function deleteMemcachedKeyData()
    {
        $cachedKeyName = self::getCachedKeyName();
        $memcache      = self::getMemcache();

        if ($cachedKeyName && $memcache) {
            return $memcache->delete($cachedKeyName);
        } else {
            return false;
        }
    }

    /**
     * @return mixed
     */
    public static function getMemcachedData()
    {
        $keyName = self::getCachedKeyName();

        if (!$keyName) {
            return null;
        }

        static $memcachedData = null;

        if (null === $memcachedData) {
            $missed   = false;
            $memcache = self::getMemcache();

            // Try getting from Memcached, if missed or unavailable, hit the db and store for later
            if ($memcache) {
                try {
                    $memcachedData = $memcache->get($keyName);

                    if (!is_array($memcachedData) || empty($memcachedData)) {
                        $memcachedData = false;
                    }
                } catch (PhCacheException $e) {
                    $memcachedData = false;
                    Bootstrap::log($e);
                }
            }

            // Either we got shit from the cache, or it's not even available, fall back to db
            if (!$memcachedData) {
                // TODO/FIXME: $memcachedData can be false or falsely stored as empty/false in memcached - do we care?
                $missed        = true;
                $model         = new self();
                $memcachedData = $model->getCachableData();
            }

            // Save for later if this was a successful db fetch and we have everything we need
            if ($missed && $memcachedData && $memcache) {
                try {
                    $memcache->save($keyName, $memcachedData);
                } catch (PhCacheException $e) {
                    Bootstrap::log($e);
                }
            }
        }

        return $memcachedData;
    }
}
