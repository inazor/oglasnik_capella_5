<?php

namespace Baseapp\Traits;

/**
 * CrudHelpers trait contains helpers for DRY-ing up some common backend controllers code
 * used for getting, checking and setting various search/filtering/pagination-related parameters and view variables
 */
trait CrudHelpers
{
    /**
     * @return int
     */
    protected function getPageParam()
    {
        $page = 1;

        if ($this->request->hasQuery('page')) {
            $page = $this->request->getQuery('page', 'int', 1);
            if ($page <= 0) {
                $page = 1;
            }
        }

        return $page;
    }

    /**
     * @return null|string
     */
    protected function processSearchParam()
    {
        $search_for = null;

        if ($this->request->hasQuery('q')) {
            $search_for = $this->request->getQuery('q', 'string', null);
        }

        $this->view->setVar('q', $search_for);

        return $search_for;
    }
	
	protected function n_processSearchStatus()
    {
        $search_for = null;

        if ($this->request->hasQuery('n_status')) {
            $search_for = $this->request->getQuery('n_status', 'int', null);
        }

        $this->view->setVar('n_search_status', $search_for);

        return $search_for;
    }

    protected function processAscDesc($default = 'desc')
    {
        $dirs = array(
            'asc'  => 'ASC',
            'desc' => 'DESC',
        );

        return $this->processOptions('dir', $dirs, $default);
    }

    protected function getPossibleSearchModes()
    {
        $modes = array(
            'exact'     => 'Exact (\'term\')',
            'fuzzy-end' => 'Fuzzy (\'term%\')',
            'fuzzy'     => 'Fuzzy (\'%term%\')',
            // 'phrase' => 'Phrase (\'term as entered\')',
        );

        return $modes;
    }

    protected function processSearchMode($selected_search_mode = 'fuzzy-end')
    {
        return $this->processOptions('mode', $this->getPossibleSearchModes(), $selected_search_mode);
    }

    /**
     * @return int
     */
    protected function processLimit()
    {
        $default_limit = $this->config->settingsBackend->pagination_items_per_page;

        $limit = $this->processOptions('limit', array(20, 50, 100), (int) $default_limit, 'int');
        if ($limit <= 1) {
            $limit = $default_limit;
        }

        return (int) $limit;
    }

    protected function getPossibleOrderByOptions()
    {
        $order_bys = array(
            'created_at'   => 'Date created',
            'modified_at'  => 'Date modified',
            'publish_date' => 'Publish date',
            'title'        => 'Title',
        );

        return $order_bys;
    }

    protected function processOrderBy($default = 'created_at')
    {
        $order_by = $this->processOptions('order_by', $this->getPossibleOrderByOptions(), $default);

        return $order_by;
    }

    /**
     * @param $name
     * @param array $options
     * @param null $default
     * @param string $filter Phalcon request filters
     *
     * @return string|int|null Value of the chosen option (or $default if chosen option is invalid)
     */
    protected function processOptions($name, $options = array(), $default = null, $filter = 'string')
    {
        $value = $default;

        if ($this->request->has($name)) {
            $value = $this->request->get($name, $filter, $default);
        }

        // Check if the value is actually a valid value
        if ($value != $default) {
            // Check both as a key and as a value, reset back to default if nothing found
            if (!array_key_exists($value, $options) && !in_array($value, $options)) {
                $value = $default;
            }
        }

        // View variables are generated based on $name (holds the chosen/selected/default value) and
        // as "{$name}_options" (holds all the $options, useful when generating radios/dropdowns/etc)
        $this->view->setVar($name, $value);
        $this->view->setVar($name . '_options', $options);

        return $value;
    }
}
