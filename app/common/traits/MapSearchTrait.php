<?php

namespace Baseapp\Traits;

use Baseapp\Library\MapSearch;
use Baseapp\Extension\Tag;
use Baseapp\Models\Categories;

trait MapSearchTrait
{
    /**
     * Helper method to check given $treeNode and return only in case it's 
     * marked as 'Search by location'
     * 
     * @param  \stdClass $treeNode
     * 
     * @return null|\stdClass
     */
    private function getMapSelectableSubTree($treeNode)
    {
        $subTree = null;

        if (!in_array($treeNode->id, MapSearch::SKIP_CATEGORIES)) {
            $json = isset($treeNode->json) ? $treeNode->json : null;
            if ($json && isset($json->search_by_location) && $json->search_by_location == 1) {
                $subTree = (new Categories())->getSubtree($treeNode);
                if (0 == count($subTree)) {
                    $subTree = null;
                }
            }
        }

        return $subTree;
    }

    /**
     * Helper method to filter out all tree nodes that are actual leaf nodes 
     * (the ones with transaction type info) and leave only those without
     * transaction type info
     * 
     * @param  \stdClass[] $subTreeArray
     * 
     * @return \stdClass[]
     */
    private function getAllExceptTransactionTypeNodes($subTreeArray)
    {
        $finalSubTreeArray = array();

        foreach ($subTreeArray as $categoryID => $categoryTreeNode) {
            if (in_array($categoryID, MapSearch::SKIP_CATEGORIES) || (isset($categoryTreeNode->transaction_type_id) && $categoryTreeNode->transaction_type_id)) {
                continue;
            }

            $finalSubTreeArray[$categoryID] = $categoryTreeNode;
        }

        return $finalSubTreeArray;
    }

    /**
     * Helper method to filter out all except those tree nodes that are actual
     * leaf nodes (the ones with transaction type info)
     * 
     * @param  \stdClass[] $subTreeArray
     * 
     * @return \stdClass[]
     */
    private function getOnlyTransactionTypeNodes($subTreeArray)
    {
        $finalSubTreeArray = array();

        $applicableTransactionTypes = array_keys(MapSearch::APPLICABLE_MORE_FILTERS);

        foreach ($subTreeArray as $categoryID => $categoryTreeNode) {
            if (!in_array($categoryTreeNode->parent_id, MapSearch::SKIP_CATEGORIES) && in_array($categoryTreeNode->id, $applicableTransactionTypes) && isset($categoryTreeNode->transaction_type_id) && $categoryTreeNode->transaction_type_id) {
                $finalSubTreeArray[$categoryID] = $categoryTreeNode;
            }
        }

        return $finalSubTreeArray;
    }

    /**
     * Enrich transaction types subtree with information about their parent so
     * we can enable/disable certain options on frontend based on currently 
     * selected category
     * 
     * @param  \stdClass[] $subTreeArray
     * 
     * @return array
     */
    private function getEnrichedSelectables($subTreeArray)
    {
        $selectables = array();

        foreach ($subTreeArray as $treeNode) {
            if ($treeNode->active) {
                $selectables[$treeNode->id] = array(
                    'text'       => $treeNode->name,
                    'attributes' => array(
                        'data-parent-id' => $treeNode->parent_id
                    )
                );
            }
        }

        return $selectables;
    }

    /**
     * Helper method to get an array with both categories and transaction types
     * for a specific categoryID, or in default behavior, all root categories
     * that are marked as MapSearchable
     * 
     * @param  null|int $specificCategoryID
     * @return array
     */
    private function getMapSearchableSelectables($specificCategoryID = null)
    {
        $categoryTree = $this->getDI()->get(Categories::MEMCACHED_KEY);
        $categoryTree[0] = null;
        unset($categoryTree[0]);

        $subTreeArray  = array();
        $foundSubTrees = 0;

        if ($specificCategoryID && isset($categoryTree[$specificCategoryID])) {
            $subTree = $this->getMapSelectableSubTree($categoryTree[$specificCategoryID]);
            if ($subTree) {
                $foundSubTrees++;
                $subTreeArray = $subTree;
            }
        } else {
            $tmpSubTreeArray = array();

            foreach ($categoryTree as $catID => $category) {
                $subTree = $this->getMapSelectableSubTree($category);
                if ($subTree) {
                    $foundSubTrees++;
                    $tmpSubTreeArray = array_merge($tmpSubTreeArray, $subTree);
                }
            }

            if ($foundSubTrees) {
                // add fake root node that will be filtered later by getSelectablesFromTreeArray() method
                $subTreeArray[0] = array();
        
                // merge $tmpSubTreeArray to $subTreeArray
                $subTreeArray = array_merge($subTreeArray, $tmpSubTreeArray);
            }
        }

        // in case we have multiple roots, then we need to display their root 
        // category so we know which category belongs where...
        $showRootNodes = ($foundSubTrees > 1);
        $nodePrefix    = '--';
        $levelDepth    = null; 
        $breadcrumbs   = false; 
        $skip_disabled = true;

        // finally, generate category selectables...
        $categorySelectables = (new Categories())->getSelectablesFromTreeArray(
            $this->getAllExceptTransactionTypeNodes($subTreeArray),
            $nodePrefix,
            $showRootNodes,
            $levelDepth,
            $breadcrumbs,
            $skip_disabled
        );

        // and transaction type selectables...
        $transactionTypeSelectables = $this->getEnrichedSelectables($this->getOnlyTransactionTypeNodes($subTreeArray));

        return array(
            'categories'       => $categorySelectables,
            'transactionTypes' => $transactionTypeSelectables
        );
    }

    /**
     * Helper method to generate category dropdown
     * @param  array    $selectables
     * @param  null|int $preselectedSelectableID
     */
    public function generateMapSearchableCategoryDropdown($selectables, $preselectedSelectableID = null)
    {
        Tag::setDefault('parent_category_id', $preselectedSelectableID);

        $parentCategoryIDDropdown = Tag::select(array(
            'parent_category_id',
            $selectables,
            'class' => 'form-control show-tick'
        ));

        $this->view->setVar('parent_category_id_dropdown', $parentCategoryIDDropdown);
    }

    /**
     * Helper method to generate transaction type dropdown
     * @param  array    $selectables
     * @param  null|int $preselectedSelectableID
     */
    public function generateMapSearchableTransactionTypesDropdown($selectables, $preselectedSelectableID = null)
    {
        Tag::setDefault('category_id', $preselectedSelectableID);

        $category_id_dropdown = Tag::select(array(
            'category_id',
            $selectables,
            'class' => 'form-control show-tick'
        ));

        $this->view->setVar('category_id_dropdown', $category_id_dropdown);
    }

    public function getParentCategoryByTransactionTypeCategoryID($transactionTypeCategoryID)
    {
        $categoryTree = $this->getDI()->get(Categories::MEMCACHED_KEY);
        if (isset($categoryTree[$transactionTypeCategoryID])) {
            return $categoryTree[$transactionTypeCategoryID]->parent_id;
        }

        return null;
    }

}
