<?php

namespace Baseapp\Controllers;

use Baseapp\Extension\Tag;
use Baseapp\Models\Ads;
use Baseapp\Models\Categories;
use Baseapp\Models\Sections;
use Baseapp\Models\MiscSettings;
use Phalcon\Di;
use Phalcon\Dispatcher;
use Phalcon\Mvc\Model;

class BaseFrontendController extends BaseController
{
    public $site_desc;

    public $submit_ad_category = null;

    /**
     * Map of Volt view variables and their values, used in views to control appearance/display
     * of certain elements (or similar). View vars gets set automatically using afterExecuteRoute().
     *
     * You can control the contents directly or using methods setLayoutFeatures(), setLayoutFeature(), hasLayoutFeature()...
     *
     * @var array
     */
    public $layout_features = array(
        // Volt view var name => value
        'full_page_width'     => false,
        'footer'              => true,
        'search_bar'          => true,

        // Default is to have the search bar hidden initially and the toggler turned on.
        // For now only the homepage is overridden (as requested)
        'mobile_search_bar_toggler'  => true,
        'mobile_search_bar_visible_initially' => false,

        'footer_links'        => true,
        'main_nav'            => true,
        'extra_sidebar_boxes' => true,
        'megamenu'            => true
    );

    protected $today = null;
    protected $today_date_sql = null;
    protected $today_date_sql_short = null;

    protected $banner_zones = array(
        'billboard'  => null,
        'mobile'     => null,
        '728x90'     => null,
        'floater'    => null,
        'background' => null,
        'megabanner' => null
    );

    protected $og_meta = '';

    protected $recaptcha_secret = '6LeXfQ4TAAAAALFmVuUSiCG4lMw2E6iPqh6oLiAB';
    protected $recaptcha_sitekey = '6LeXfQ4TAAAAAIajyA0LxhEzpw8MUHzPfE1tX6N5';

    // Controls whether the zendesk script is loaded in the footer or not (for every page view)
    protected $load_zendesk_widget_script = false;

    protected $requested_uri;
    protected $requested_uri_no_qs;
    protected $requested_path;

    public static $initialized = false;

    public function add_banner_zone($location, $zone_id, $zone_markup)
    {
        if (!isset($this->banner_zones[$location]) || null === $this->banner_zones[$location]) {
            $this->banner_zones[$location] = array();
        }

        if (is_array($this->banner_zones[$location])) {
            $this->banner_zones[$location][] = array(
                'id'     => $zone_id,
                'markup' => $zone_markup
            );
            $this->view->setVar("banner_$location", $this->get_banner_zone_markup($location));
        }
    }

    public function replace_banner_zone($location, $zone_id, $zone_markup)
    {
        $this->banner_zones[$location] = array();

        if (is_array($this->banner_zones[$location])) {
            $this->banner_zones[$location][] = array(
                'id'     => $zone_id,
                'markup' => $zone_markup
            );
            $this->view->setVar("banner_$location", $this->get_banner_zone_markup($location));
        }
    }

    public function get_banner_zone_markup($location)
    {
        $markup = '';

        if (is_array($this->banner_zones[$location])) {
            $markup_array = array();
            foreach ($this->banner_zones[$location] as $zone) {
                $markup_array[] = $zone['markup'];
            }
            if (count($markup_array) > 1) {
                $markup = '<div class="multi-banner text-center">' . implode('', $markup_array) . '</div>';
            } else {
                $markup = implode('', $markup_array);
            }
            $markup_array = null;
            unset($markup_array);
        }

        return $markup;
    }

    public function get_active_zone_ids()
    {
        $zone_ids = array();

        $priority = 1;
        foreach ($this->banner_zones as $location) {
            if (is_array($location)) {
                foreach ($location as $zone) {
                    $zone_ids[] = array(
                        'id' => intval($zone['id']),
                        'p' => $priority++
                    );
                }
            }
        }

        if (count($zone_ids)) {
            return json_encode($zone_ids);
        }

        return null;
    }

    public function remove_banner_zone($location)
    {
        $this->banner_zones[$location] = null;

        $this->view->setVar("banner_$location", false);
    }

    public function add_default_banner_zones()
    {
        // Some global banner variables
        // FIXME/TODO: find a way to manage these better/smarter
        $this->add_banner_zone('floater', '2527480', '<div id="zone2527480" class="goAdverticum"></div>');
        $this->add_banner_zone('728x90', '2285610', '<div id="zone2285610" class="goAdverticum"></div>');
        // $this->add_banner_zone('billboard', '2459080', '<script type="text/javascript" class="goa-inserterScript">goAdverticum3.insertBanner("2459080");</script>');
        $this->add_banner_zone('billboard', '2459080', '<div id="zone2459080" class="goAdverticum"></div>');
        $this->add_banner_zone('background', '4018168', '<script type="text/javascript" class="goa-inserterScript">goAdverticum3.insertBanner("4018168");</script>');
    }

    public function add_default_sidebar_banner_zones()
    {
        // Left sidebar zones...
        $this->add_banner_zone('sidebar_left_1', '2285083', '<div id="zone2285083" class="goAdverticum"></div>');
        $this->add_banner_zone('sidebar_left_2', '2527496', '<div id="zone2527496" class="goAdverticum"></div>');
        $this->add_banner_zone('sidebar_left_3', '2285608', '<div id="zone2285608" class="goAdverticum"></div>');
        $this->add_banner_zone('sidebar_left_4', '2285606', '<div id="zone2285606" class="goAdverticum"></div>');
        $this->add_banner_zone('sidebar_left_5', '2285588', '<div id="zone2285588" class="goAdverticum"></div>');

        // Right sidebar zones...
        $this->add_banner_zone('sidebar_right_1', '2285079', '<div id="zone2285079" class="goAdverticum"></div>');
        $this->add_banner_zone('sidebar_right_2', '2285024', '<div id="zone2285024" class="goAdverticum"></div>');
        $this->add_banner_zone('sidebar_right_3', '2284999', '<div id="zone2284999" class="goAdverticum"></div>');
    }

    /**
     * Triggered on Listeners/Controllers before executing the controller/action method.
     *
     * @param Dispatcher $dispatcher
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        // Setup commonly used request uri variables
        $requested_uri       = ltrim($this->request->getURI(), '/');
        $requested_uri_no_qs = strtok($requested_uri, '?');
        $requested_path      = '/' . $requested_uri_no_qs;

        $this->requested_uri       = $requested_uri;
        $this->requested_uri_no_qs = $requested_uri_no_qs;
        $this->requested_path      = $requested_path;
    }

    public function addBasicAssets()
    {
        // $this->assets->addCss('assets/css/normalize.css');
        // $this->assets->addCss('assets/css/main.css');
        $this->assets->addCss('https://fonts.googleapis.com/css?family=Source+Serif+Pro:400,700|Source+Sans+Pro:400,300,600,700&subset=latin,latin-ext', false);
        $this->assets->addCss('assets/css/base.css'); // normalize.css + main.css
        $this->assets->addCss('assets/css/bootstrap.css');
        $this->assets->addCss('assets/css/style.css');
        $this->assets->addCss('assets/css/overrides.css');
        $this->assets->addCss('assets/css/country-flags.css');
        $this->assets->addCss('assets/css/advertising.css');
    }

    /**
     * Only called if `beforeExecuteRoute` event is executed with success.
     */
    public function initialize()
    {
        /**
         * Note: Phalcon 2 calls initialize() twice even when dispatcher forwards to the
         * same controller (might change in the future), so that's why we check and bail
         * if we've already been initialized once in order to keep old behaviour.
         *
         * @link: https://forum.phalconphp.com/discussion/7551/controller-initialize-called-twice-on-forwards
         */
        if (self::$initialized) {
            parent::initialize();
            return;
        }

        // Set default title and description
        $this->tag->setTitle('Oglasnik.hr');
        $this->site_desc = 'Default';
        // $this->tag->setTitle('Plavi oglasnik: besplatni mali oglasi - Oglasnik.hr');
        // $this->site_desc = '150.000 oglasa garantira da ćete pronaći apsolutno sve što Vam treba! Jedino mjesto za Vašu ponudu, potražnju, iznajmljivanje, unajmljivanje i zamjenu.';

        // Add global fronted css and js to assets collection
        $this->addBasicAssets();

        // $this->assets->addCss('assets/vendor/bootstrap-select/bootstrap-select.min.css');
        $this->assets->addCss('assets/vendor/bootstrap-select-1.10.0-patched/bootstrap-select.min.css');

        // $this->assets->addCss('assets/vendor/awesome-bootstrap-checkbox.css');
        // $this->assets->addCss('assets/css/bootstrap.oglasnik-theme.css');
        $this->assets->addCss('assets/css/font-awesome.min.css');
        $this->assets->addCss('assets/vendor/jquery-mmenu-5.6.5/jquery.mmenu.all.css');
        $this->assets->addCss('assets/css/typeahead.css');

        $this->assets->addJs('assets/vendor/modernizr-2.8.3.min.js');
        $this->assets->addJs('assets/vendor/jquery/jquery-1.11.2.min.js');
        $this->assets->addJs('assets/vendor/bootstrap-3.3.6-dist/js/bootstrap.min.js');
        $this->assets->addJs('assets/vendor/typeahead/typeahead.bundle.min.js');

        $this->assets->addJs('assets/vendor/jquery.sticky-kit.min.js');
        $this->assets->addJs('assets/vendor/jquery.lazy.min.js');
        $this->assets->addJs('assets/vendor/jquery-mmenu-5.6.5/jquery.mmenu.all.min.js');
        $this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.widget.min.js');
        $this->assets->addJs('assets/js/jquery.oglasnik.js');
        $this->assets->addJs('assets/vendor/bootstrap-select-1.10.0-patched/bootstrap-select.min.js');
        $this->assets->addJs('assets/vendor/js-date-format.min.js');
        $this->assets->addJs('assets/vendor/jquery.mask.min.js');
        $this->assets->addJs('assets/vendor/dragend.min.js');

        $this->assets->addJs('assets/vendor/js-url/url.min.js');

        $this->assets->addJs('assets/js/carousels.js');

        // Removing banners for special controllers (error controller for now)
        $controller_name     = $this->dispatcher->getControllerName();
        $special_controllers = array('error');
        if (in_array($controller_name, $special_controllers)) {
            $this->view->setVar('banners', false);
            $this->remove_banner_zone('728x90');
            $this->remove_banner_zone('billboard');
            $this->remove_banner_zone('floater');
            $this->remove_banner_zone('background');
        }

        /**
         * Privacy policy cookie banner handling..
         */
        $this->view->setVar('show_policy_cookie_banner', false);
        if (isset($this->config->cookies) && isset($this->config->cookies->policy)) {
            // assume that policy cookie has not been set!
            $this->view->setVar('show_policy_cookie_banner', true);
            $this->view->setVar('show_policy_cookie_banner_name', $this->config->cookies->policy['name']);
            $this->view->setVar('show_policy_cookie_banner_expires', $this->config->cookies->policy['expires']);
            // test existence of policy cookie acceptance
            if ($this->cookies->has($this->config->cookies->policy['name'])) {
                $this->view->setVar('show_policy_cookie_banner', false);
            }
        }
        $this->view->setVar('default_viewType', $this->getDefaultViewType());

        // To make sure we're not called multiple times during a single request
        self::$initialized = true;

        // Make sure the parent initializes too
        parent::initialize();
    }

    public function onConstruct()
    {
        $this->today = $today = getdate();
        $this->today_date_sql_short = $today['year'] . '-' . $today['mon'] . '-' . $today['mday'];
        $this->today_date_sql = $this->today_date_sql_short . ' 23:59:59';
    }

    /**
     * N.B.
     * Called after each Action, meaning multiple times in case when the dispatcher forwards to another action!
     *
     * @param Dispatcher $dispatcher
     */
    public function afterExecuteRoute(Dispatcher $dispatcher)
    {
        static $times_called = 0;
        $times_called++;

        /**
         * No need to do all this shit if view is disabled no? (at least while everything we do below is on $this->view...)
         * Otherwise actions that end early (or think they do) still have at least 2 more SQL queries done... which
         * is silly, if you're only rendering a barcode image somewhere or something...
         */
        if ($this->view->isDisabled()) {
            return;
        }

        // Set final title and description
        // $this->tag->appendTitle(' - Oglasnik.hr');
        $this->view->setVar('site_desc', mb_substr($this->filter->sanitize($this->site_desc, 'string'), 0, 200, 'utf-8'));

        $this->view->setVar('curr_env', $this->config->app->env);
        $this->view->setVar('banners', ('development' !== $this->config->app->env));

        // in case we are in a category that can accept ads, we'll automatically
        // preselect this category and show the user a form with parameters from
        // this specific category
        $this->view->setVar('submit_ad_category', $this->submit_ad_category);

        // Set our view var for controlling various layout blocks
        $this->view->setVar('layout_features', $this->layout_features);

        // Set scripts
        $this->view->setVar('scripts', $this->scripts);

        $this->view->setVar('og_meta', $this->og_meta);

        if ($this->layout_features['megamenu']) {
            $this->view->setVar('megamenu_sections_markup', self::buildMegaMenuSectionsMarkup());
        }

        // Disable zendesk loading in development env
        if ('development' === $this->config->app->env) {
            $this->load_zendesk_widget_script = false;
        }
        $this->view->setVar('load_zendesk_script', $this->load_zendesk_widget_script);

        // mobile menu
        $categoryTree = $this->getDI()->get(Categories::MEMCACHED_KEY);
        $this->view->setVar('categories', $categoryTree[1]->children);
        $this->view->setVar('categoryTree', $categoryTree);

        // Current user's unread messages count as a global view var
        $this->view->setVar('userUnreadMsgCount', $this->getCurrentUserUnreadMsgCount());

        // Some global vars for easier url/redirection parameters building
        $this->view->setVar('requested_uri', $this->requested_uri);
        $this->view->setVar('requested_uri_no_qs', $this->requested_uri_no_qs);
        $this->view->setVar('requested_path', $this->requested_path);

        $this->view->setVar('subHeaderLinks_contents', MiscSettings::getSubHeaderLinksContents());

        // We want main.js to really be loaded among the last things, since favoriter.init()
        // is called there, and new listings do stuff that require favoriter to be called later...
        $this->assets->addJs('assets/js/main.js');
    }

    /**
     * @param string $feature
     * @param bool $value
     */
    public function setLayoutFeature($feature, $value)
    {
        $this->layout_features[$feature] = (bool) $value;
    }

    /**
     * @param string $feature
     *
     * @return bool
     */
    public function hasLayoutFeature($feature)
    {
        return (isset($this->layout_features[$feature]) && $this->layout_features[$feature]);
    }

    /**
     * @param array $features
     */
    public function setLayoutFeatures(array $features)
    {
        $this->layout_features = $features;
    }

    /**
     * @param null $default
     *
     * @return array
     */
    protected function getSortParamData($default = null)
    {
        $sort         = $this->request->get('sort', 'string', null);
        $sort_default = $this->config->adSorting->default_value;

        if (null !== $default) {
            $sort_default = $default;
        }

        if (!$sort) {
            $sort = $sort_default;
        }

        // Build a list from config.ini
        $sorts = array();
        foreach ($this->config->adSorting->toArray() as $key => $value) {
            if ('default_value' !== $key) {
                $sorts[$key] = $value['name'];
            }
        }

        return array(
            'options' => $sorts,
            'current' => $sort,
            'default' => $sort_default
        );
    }

    /**
     * Helper method for building and assigning the sort choose dropdown into the view
     *
     * @param string|null $default Named sort option which overrides the config.ini default if provided
     * @param array $options Key/value pairs passed to `Tag::select()` after being merged with default values
     */
    protected function buildSortingDropdown($default = null, $options = array())
    {
        $sort_data = $this->getSortParamData($default);
        Tag::setDefault('sort',  $sort_data['current']);

        $select_options = array(
            'sort',
            $sort_data['options'],
            'class' => 'form-control',
            'useEmpty' => false,
            'data-default' => $sort_data['default']
        );

        // Allow some override via $options
        if (!empty($options) && is_array($options)) {
            $select_options = array_merge($select_options, $options);
        }

        $sort_dropdown = Tag::select($select_options);
        $this->view->setVar('sort_dropdown', $sort_dropdown);
    }

    private function getDefaultViewType()
    {
        return isset($this->config->settingsFrontend->default_viewType) ? $this->config->settingsFrontend->default_viewType : 'list';
    }

    /**
     * @param string|null $default Set default value
     *
     * @return mixed|null|string|void
     */
    protected function getViewTypeData($default = null)
    {
        if ($viewType = $this->request->get('viewType', 'string', null)) {
            return $viewType;
        }

        if ($default) {
            return $default;
        }

        return $this->getDefaultViewType();
    }

    /**
     * Returns the ORDER BY clause to be used when constructing various listing queries that
     * support choosing different sort options.
     * If a Model instance is provided, it's used as a provider for the default sort/order by value (if it has a
     * non-empty `default_sorting_order` property).
     * Also checks for the presence of a valid named sort value in the request parameters and
     * return the corresponding columns (based on config.ini values) if they exist, null otherwise.
     *
     * @param null|Model $entity
     *
     * @return null|Model\Resultset|string
     */
    protected function buildOrderByParam(Model $entity = null)
    {
        $order_by   = null;
        $named_sort = null;

        // If an entity is specified, use it as the provider of default value
        if (null !== $entity) {
            if (isset($entity->default_sorting_order) && !empty($entity->default_sorting_order)) {
                $named_sort = $entity->default_sorting_order;
            }
        }

        /**
         * We have to find out what is the preferred sorting order.
         *
         * First, we check if user specifically selected a sorting option. If this is the case, then we validate if we
         * know what this option means (if it exists in our config array). If we have a match, everything's ok. In case
         * user didn't select anything, we look for the default preferred sorting order, or, in case there isn't one
         * (I don't know why there wouldn't be one) we fall back to some basic ordering...
         */
        $sorts = isset($this->config->adSorting) ? $this->config->adSorting->toArray() : array();

        // If a request specifies a named sort option, use it (if it's valid)
        if ($this->request->has('sort')) {
            $named_sort = trim($this->request->get('sort', 'string'));
        }

        // Check if the named sort is valid
        if (isset($sorts[$named_sort]['column'])) {
            $order_by = trim($sorts[$named_sort]['column']);
        }

        // If we haven't found anything (or a non-valid named sort was specified), try to use some sane defaults
        if (null === $order_by) {
            if (isset($sorts['default_value']) && isset($sorts[$sorts['default_value']]['column'])) {
                $order_by = trim($sorts[$sorts['default_value']]['column']);
            } else {
                $order_by = 'ad.id';
            }
        }

        return $order_by;
    }

    /**
     * Checks and forwards to age fulfillment form/action (in CategoriesController) if needed.
     *
     * @param array $restriction
     * @param array $og_meta Array of open graph data (key/value pairs) to set on the intermediate page (the one
     *                       that prompts for age verification). This is just passed along if needed to
     *                       `CategoriesController::ageRequestAction()` via $this->dispatcher->forward()
     *
     * @return bool|void
     */
    protected function ageRestrictionCheckRequest($restriction, $og_meta = array())
    {
        // TODO: find a way to let crawlers bypass this? Check user agents or maybe even safer to filter based on their known IP ranges?
        /*
        if ($is_allowed_bot) {
            $restriction = false;
        }
        */

        $check_age = false;
        $auto_deny = false;

        if ($restriction) {
            $given_approval = $this->session->get('category_' . $restriction['category_id'] . '_age_confirm', false);
            if (!$given_approval) {
                $check_age = true;

                // try getting current user (if logged in)
                $user = $this->auth->get_user();
                if ($user) {
                    $user_age = $user->getAge();
                    if ($user_age && $user_age >= $restriction['min_age']) {
                        // allow access to page
                        $check_age = false;
                    } else {
                        // user is younger than needed age so we automatically deny access to this page
                        $auto_deny = true;
                    }
                }
            }
        }

        if ($check_age) {
            $params = array(
                'controller' => 'categories',
                'action'     => 'ageRestriction',
                'params'     => array(
                    'restriction' => $restriction,
                    'auto_deny'   => $auto_deny,
                    'next_url'    => $restriction['url'],
                    'og_meta'     => $og_meta
                )
            );
            $this->dispatcher->forward($params);
        }

        return $check_age;
    }

    public function setOpenGraphMeta($data = array())
    {
        $this->og_meta = '';
        foreach ($data as $k => $v) {
            $this->og_meta .= '<meta property="' . $k . '" content="' . $this->escaper->escapeHtmlAttr($v) . '">' . "\n";
        }
    }

    /**
     * @return null|string
     */
    public static function buildMegaMenuSectionsMarkup()
    {
        $markup = null;
        $sections = Sections::getTreeStructured('homepage', true);

        if (!empty($sections)) {
            $cnt = 0;
            $markup = '';

            $url_service = Di::getDefault()->get('url');
            foreach ($sections as $section) {
                $cnt++;
                $json = $section['json'];
                $section_icon = (!empty($json->icon)) ? $json->icon : null;
                $children = (isset($section['children']) && count($section['children']) ? $section['children'] : false);
                $logo = null;
                if (isset($section['sponsorshipLogo']) && !empty($section['sponsorshipLogo'])) {
                    $logo = $section['sponsorshipLogo'];
                    $logo = $logo->getThumb();
                }

                $markup .= '<div class="col-md-3 col-sm-6">';
                $markup .= '<ul class="main-category">';
                if ($section_icon) {
                    $section_icon_url = $url_service->get('assets/img/categories/' . $section_icon);
                    $markup .= '<span class="category-icon" style="background-image:url(' . $section_icon_url . ')" alt="' . $section['name'] . '"></span>';
                }
                $markup .= '<li><a href="' . $section['frontend_url'] . '"' . (isset($section['frontend_url_target']) && $section['frontend_url_target'] ? ' target="' . $section['frontend_url_target'] . '"' : '') . '>' . $section['name']. '</a>';
                $markup .= ((isset($section['count']) && $section['count'] > 0) ? ' <small>' . $section['count'] . '</small>' : '');
                $markup .= '</li>';
                if ($children || $logo) {
                    $markup .= '<li>';
                    if ($logo) {
                        $logo_img = $logo->setClass('sponsorship-logo')->setHeight('45')->setWidth('auto');
                        $markup .= (string) $logo_img;
                    }
                    if ($children) {
                        $markup .= '<ul class="subcategory">';
                        foreach ($children as $child) {
                            $markup .= '<li><a href="' . $child['frontend_url'] . '"' . (isset($child['frontend_url_target']) && $child['frontend_url_target'] ? ' target="' . $child['frontend_url_target'] . '"' : '') . '>' . $child['name'] . '</a>';
                            $markup .= ((isset($child['count']) && $child['count'] > 0) ? ' <small>' . $child['count'] . '</small>' : '');
                            $markup .= '</li>';
                        }
                        $markup .= '</ul>';
                    }
                    $markup .= '</li>';
                }
                $markup .= '</ul>';
                $markup .= '</div>';

                if ($cnt % 4 === 0 ) {
                    $markup .= '<div class="clearfix"></div>';
                } elseif ($cnt % 2 === 0) {
                    $markup .= '<div class="clearfix visible-sm-block"></div>';
                }
            }
        }

        return $markup;
    }

    protected function getCurrentUserUnreadMsgCount()
    {
        static $count = null;

        if (null === $count) {
            $user = $this->auth->get_user();
            if ($user) {
                $count = $user->getUnreadNotificationsCount();
            }
        }

        return $count;
    }
}
