<?php

namespace Baseapp\Controllers;

use Baseapp\Extension\Tag;
use Phalcon\Dispatcher;
use Baseapp\Traits\ControllerGroupTrait;

/**
 * Base Backend Controller Class
 */
class BaseBackendController extends BaseController
{

    use ControllerGroupTrait;

    protected $next_url;

    protected $allowed_roles = array('admin', 'support', 'supersupport', 'sales', 'finance', 'moderator', 'content');
    protected $insufficient_privileges = 'Sorry, you have insufficient privileges to perform the attempted action!';

    /**
     * Runs before any route is executed.
     *
     * @param Dispatcher $dispatcher
     *
     * @return void | \Phalcon\Http\ResponseInterface
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        // Make sure we have a logged in admin
        if (!$this->auth->logged_in($this->allowed_roles)) {
            // TODO/FIXME: can we do something better/smarter here? we should be able to...
            // No allowed/required role, but could still be logged in (if so, spit out an empty 403 response)
            if ($this->request->isAjax() || $this->auth->logged_in()) {
                $this->do_403();
            } else {
                $this->redirect_signin();
            }

            // Returning false within beforeExecuteRoute tells the dispatcher to stop the current operation
            return false;
        }

        // Backend csrf checking in a single place
        $this->check_csrf();

        // Prepare 'next url' (where we want the user to end up after this action)
        $this->setup_next_url($dispatcher);

        // Global backend 'cancel' form button handler
        if ($this->request->isPost()) {
            if ($this->request->hasPost('cancel')) {
                $this->redirect_to($this->get_cancel_url($dispatcher));
                // Tells the dispatcher to stop the current operation
                return false;
            }
        }

        // Set default title
        $this->tag->setTitle('Index');

        // Add global css and js assets (built with grunt)
        // If in debug mode, load non-minified assets (concat only)
        $dbg = $this->getDI()->get('config')->app->debug;

        if ($dbg) {
            $this->assets->addCss('assets/build/backend.css');
        } else {
            $this->assets->addCss('assets/build/backend.min.css');
        }

        if ($dbg) {
            $this->assets->addJs('assets/build/backend.js');
        } else {
            $this->assets->addJs('assets/build/backend.min.js');
        }
    }

    /**
     * After every Action
     *
     * @param Dispatcher  $dispatcher
     */
    public function afterExecuteRoute(Dispatcher $dispatcher)
    {
        // Set final title
        // $this->tag->appendTitle(' ♥ Oglasnik');
        $this->tag->appendTitle(' | Admin | oglasnik.hr');

        // Set scripts
        $this->view->setVar('scripts', $this->scripts);

        // Sets some view vars (helps controlling menu display mostly for now)
        $controller_name = $this->router->getControllerName();
        $controller_action = $this->router->getActionName();
        $this->view->setVar('controller_name', $controller_name);
        $this->view->setVar('controller_action', $controller_action);
        $this->set_controller_group($controller_name);

        /**
         * TODO/FIXME: see if this can be de-duplicated, or just
         * apply the same fix so it works for both frontend and backend
         */
        // Minify css and js collection
        // \Baseapp\Library\Tool::assetsMinification();
    }

    public function set_popup_layout()
    {
        $this->view->setMainView('layouts/popup');
    }

    /**
     * Overridden BaseController::redirect_to() to automatically prefix
     * any locations with 'admin/' if not already prefixed.
     * If no location is specified we end up on admin index which is convenient.
     *
     * {@inheritdoc}
     */
/*
    public function redirect_to($location = '', $status_code = null)
    {
        $location = 'admin/' . str_replace('admin/', '', $location);

        return parent::redirect_to($location, $status_code);
    }
*/

    /**
     * Overriding BaseController's redirect_back() for backend usage since backend has slightly different
     * requirements and defaults.
     * Redirects back to where the user came from (or where he was specified to go
     * via "next" query/form param). If parsing those fails, we return him to the backend homepage.
     *
     * @return \Phalcon\Http\ResponseInterface
     */
    public function redirect_back()
    {
        $this->disable_view();
        $fallback = $this->get_default_next_url($this->dispatcher);
        $redirect_path = $this->get_redir_path_from_current_request($fallback);
        if ($redirect_path) {
            return $this->redirect_to($redirect_path);
        } else {
            return $this->redirect_to('admin/');
        }
    }

    /**
     * Returns which save button was submitted (if it was a POST)
     *
     * @return null|string
     */
    public function get_save_action()
    {
        $save_action = null;

        if ($this->request->isPost()) {
            $save_action = $this->request->hasPost('save') ? 'save' : 'saveexit';
        }

        return $save_action;
    }

    /**
     * @param \Phalcon\Mvc\DispatcherInterface $dispatcher
     *
     * @return mixed|null|string
     */
    public function determine_next_url_from_request($dispatcher)
    {
        // First check the query string
        if ($this->request->hasQuery('next')) {
            $next_url = $this->request->getQuery('next', 'trim');
        } else {
            if ($this->request->hasPost('next')) {
                // If there's an already posted next param, maintain it
                $next_url = $this->request->getPost('next', 'trim');
            } else {
                // If not, use the referer
                // TODO: what about cases when the referer already includes it's own ?next parameter?
                // TODO: what do we do when next ends up being: user/signin?next=admin%2Fimage-styles%2Fedit%2F4 ?
                $next_url = $this->request->getHTTPReferer();
                // Avoiding using the referer if it already contains a next param for now...
                if (stripos($next_url, 'next=') !== false) {
                    $next_url = null;
                }
            }
            // Fall back to default determined from the $dispatcher
            if (empty($next_url)) {
                $next_url = $this->get_default_next_url($dispatcher);
            }
        }

        // Make sure whatever we got is safe to use
        $next_url = $this->get_safe_redir_path($next_url);

        return $next_url;
    }

    /**
     * Sets up the default 'next' url form field
     *
     * @param \Phalcon\Mvc\DispatcherInterface $dispatcher
     *
     * @return null|string
     */
    public function setup_next_url($dispatcher)
    {
        if ('saveexit' === $this->get_save_action()) {
            // Modify 'next url' to point to 'cancel url' in case of 'saveexit'
            $next_url = $this->get_cancel_url($dispatcher);
        } else {
            // Set 'next url' to point to whatever was specified in the request
            $next_url = $this->determine_next_url_from_request($dispatcher);
        }

        Tag::setDefault('next', $next_url);
        $this->next_url = $next_url;

        return $this->next_url;
    }

    /**
     * @param \Phalcon\Mvc\DispatcherInterface $dispatcher
     *
     * @return string
     */
    public function get_default_next_url($dispatcher)
    {
        $next_url = 'admin/' . $dispatcher->getControllerName();
        return $next_url;
    }

    public function get_next_url()
    {
        return $this->next_url;
    }

    /**
     * @param \Phalcon\Mvc\DispatcherInterface $dispatcher
     *
     * @return string
     */
    public function get_default_cancel_url($dispatcher)
    {
        // Default cancel url is (currently at least) really the same one as the default next url
        return $this->get_default_next_url($dispatcher);
    }

    /**
     * Hitting the 'Cancel' button should generally redirect you either to where explicitly
     * specified by the current request's 'next' POST parameter or to the $dispatcher's default
     * for the current controller.
     *
     * @param \Phalcon\Mvc\DispatcherInterface $dispatcher
     *
     * @return null|string
     */
    public function get_cancel_url($dispatcher)
    {
        // Cancel should redirect you either to where explicitly specified by ?next
        // or to the $dispatcher's default
        if ($this->request->hasPost('next')) {
            $url = $this->request->getPost('next');
            $url = $this->get_safe_redir_path($url);
        } else {
            $url = $this->get_default_cancel_url($dispatcher);
        }
        return $url;
    }

    /**
     * Given a list of strings (which are considered to be sql conditions), change occurrences of
     * ` LIKE ` into ` = ` when $mode is 'exact' or 'phrase'.
     * Returns the list of strings (potentially modified, depending on $mode).
     *
     * @param array $conditions
     * @param string $mode
     *
     * @return array
     */
    protected function changeSearchParamsLikeIntoEqualsWhenExactMode(array $conditions, $mode = '')
    {
        if ('exact' === $mode || 'phrase' === $mode) {
            foreach ($conditions as $k => $condition) {
                $conditions[$k] = str_replace(' LIKE ', ' = ', $condition);
            }
        }

        return $conditions;
    }
}
