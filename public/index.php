<?php
/**
 * index.php (front controller)
 */

// could probably use auto_prepend_file for this really, and then it could easily be
// environment-specific?
// But collection needs to happen at the end (in register_shutdown_function maybe), and
// we'd need another file to include in the end maybe?

ini_set('memory_limit', '4092M');
ini_set('max_execution_time', 300); //dodano radi obrade updateTotals 
ini_set('session.gc_maxlifetime', 14400);

if (isset($_GET['_profile'])) {
    define('START_TIME', microtime(true));
    // System Start Memory
    define('START_MEMORY_USAGE', memory_get_usage());
    if (function_exists('xhprof_enable')) {
        xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);
    }
}

error_reporting(E_ALL);


// $_SESSION['_context']['zadnje_vrijeme'] = date('h:i:s');

try {

    if (!defined('ROOT_PATH')) {
        define('ROOT_PATH', dirname(__DIR__));
    }

    if (!defined('APP_DIR')) {
        define('APP_DIR', ROOT_PATH . '/app');
    }

    // Require composer autoloader
    require ROOT_PATH . '/vendor/autoload.php';

    // Init the prophiler (might not be used at all)
    $prophiler = new \Fabfuel\Prophiler\Profiler();
    $logger    = new \Fabfuel\Prophiler\Adapter\Psr\Log\Logger($prophiler);

    // Require our bootstrap
    require_once APP_DIR . '/Bootstrap.php';

    $di     = new \Phalcon\Di\FactoryDefault();
    $di->setShared('prophiler', $prophiler);
    $di->setShared('prophilerLogger', $logger);

    $app    = new \Baseapp\Bootstrap($di);

    // Dump the response
    echo $app->handle()->getContent();
 
    // Dump the prophiler toolbar if we're using it
    $config = $app->getDI()->getShared('config');
    if ($config->app->debug) {
        $view = $app->getDI()->get('view');
        // Only show it if the view is not disabled (otherwise it shits all over Ajax/JSON responses etc.)
        if (!$view->isDisabled()) {
            // FIXME: find a way to remove this hack (used to get total number of queries from our sql profiler)
            // because prophiler doesn't have anything like that (or I haven't found it at least, and I've spent too much time on it already anyway)
            if ($app->getDI()->has('prophilerLogger')) {
                $em           = $app->getEventsManager();
                $db_listeners = $em->getListeners('db');
                foreach ($db_listeners as &$listener) {
                    // Note: This is only present if config.database.debug = True in config.ini
                    if ($listener instanceof \Baseapp\Library\Profiler\QueryLogger) {
                        $baseapp_profiler = $listener->getProfiler();
                        $message = sprintf('Total queries: %s (%s seconds)', $baseapp_profiler->getNumberTotalStatements(), $baseapp_profiler->getTotalElapsedSeconds());

                        $prophiler_logger = $app->getDI()->getShared('prophilerLogger');
                        $prophiler_logger->info($message);
                        break;
                    }
                }
            }
            $toolbar = new \Fabfuel\Prophiler\Toolbar($di->get('prophiler'));
            $toolbar->addDataCollector(new \Baseapp\Library\Profiler\CustomRequestDataCollector());
            if ($app->shouldRenderProphiler()) {
                echo $toolbar->render();
            }
        }
    }

} catch (\Exception $e) {
    (new \Baseapp\Library\ErrorHandler())->exception_handler($e);
}

/**
 * TODO: don't forget to remove this in prod
 * We should probably rely on an environment variable for this, instead of $_GET
 */
if (isset($_GET['_profile'])) {
    echo 'Page rendered in <b>'
        . round((microtime(true) - START_TIME), 5) * 1000 . ' ms</b>, taking <b>'
        . round((memory_get_usage() - START_MEMORY_USAGE) / 1024, 2) ." KB</b>";
    $f = get_included_files();
    echo ', included files: ' . count($f);
    if (function_exists('xhprof_disable')) {
        if (!defined('XHPROF_LIB_ROOT')) {
            define('XHPROF_LIB_ROOT', $app->config->xhprof->lib_path);
        }
        $xhprof_data = xhprof_disable();
        include_once XHPROF_LIB_ROOT . '/config.php';
        include_once XHPROF_LIB_ROOT . '/utils/xhprof_lib.php';
        include_once XHPROF_LIB_ROOT . '/utils/xhprof_runs.php';
        $xhprof_runs = new XHProfRuns_Default();
        $run_id = $xhprof_runs->save_run($xhprof_data, 'phalcon-xhprof-testing');
        echo '<br><a href="' . $app->config->xhprof->url . '/index.php?run=' . $run_id . '&amp;source=phalcon-xhprof-testing">Profile data</a><br>';
    }
}
