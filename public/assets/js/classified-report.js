jQuery(document).ready(function() {
    $('#report_reason').selectpicker({
        style: 'btn btn-default',
        noneSelectedText: '-- Odaberite razlog prijave --',
        liveSearch: false,
        noneResultsText: '',
        mobile: is_mobile_browser(),
        dropupAuto: false
    });
});
