var OGL = OGL || {};

function is_mobile_browser() {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);
}

OGL.is_mobile_browser = is_mobile_browser();
OGL.isInt = function(n) {
    return Number(n) === n && n % 1 === 0;
};
OGL.isFloat = function(n) {
    return Number(n) === n && n % 1 !== 0;
};
OGL.scrollToFirstError = function () {
    // on page load try focusing the first error spotted
    var has_error = $('.has-error');
    if (has_error.length > 0) {
        var $el = has_error.first();
        $('html, body').animate({
            scrollTop: $el.offset().top
        }, 1000);
    }
};

OGL.isElementInViewport = function (el) {
    //special bonus for those using jQuery
    if (typeof jQuery === "function" && el instanceof jQuery) {
        el = el[0];
    }

    var rect = el.getBoundingClientRect();

    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
        rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
    );
};

OGL.calc_sticky_offset = function() {
    var offset = 0;
    var $prophiler_toolbar = jQuery('#prophiler');
    if ($prophiler_toolbar.length) {
        offset += $prophiler_toolbar.height();
    }
    return offset;
};

OGL.sticky_last_sidebar_box = function(){
    var $sidebar = jQuery('.listings-banner-col-right');
    if ($sidebar.length > 0) {
        var $last_box = $sidebar.find('.sidebar-rectangle-holder .banner:last-child');
        if ($last_box.length > 0) {
            var $to_reposition = jQuery('#filters-anchor, .sidebar-rectangle-holder, .listings-main-col, .listings-banner-col-right');
            $last_box.stick_in_parent({
                parent: $sidebar.closest('.row'),
                offset_top: OGL.calc_sticky_offset(),
                sticky_class: 'stuck'
            }).on('sticky_kit:bottom', function(e){
                $to_reposition.css({'position':'static'});
                $last_box.addClass('bottomed');
            }).on('sticky_kit:unbottom', function(e){
                $to_reposition.css({'position':'relative'});
                $last_box.removeClass('bottomed');
            });
            OGL.on_element_height_change(document.body, function(e){
                jQuery(document.body).trigger('sticky_kit:recalc');
            });
        }
    }
};
OGL.sticky_filterbits = function(){
    var $filterbits_container = jQuery('#filterBitsContainer');
    if ($filterbits_container.length > 0) {
        $filterbits_container.stick_in_parent({
            //parent: $filterbits_container.parent().parent(),
            parent:'#page-mm-container',
            offset_top: OGL.calc_sticky_offset(),
            sticky_class: 'stuck'
        });
    }
};

OGL.on_element_height_change = function(el, callback){
    var lastHeight = el.clientHeight, newHeight;
    (function run(){
        newHeight = el.clientHeight;
        if(lastHeight !== newHeight) {
            callback();
        }

        lastHeight = newHeight;

        if(el.onElementHeightChangeTimer) {
            clearTimeout(el.onElementHeightChangeTimer);
        }

        el.onElementHeightChangeTimer = setTimeout(run, 300);
    })();
};

OGL.mobile_filtering_faux_modal_thingy = function(){
    var $filters = jQuery('#filters-anchor');
    var $filters_anchor_link = jQuery('.filters-anchor-link');
    var to_hide_elements_list = '.listings-main-col, .site-footer, .featured-shops, #filterBitsContainer, ' +
        '.total-results, h1.section-title, .breadcrumbs, .header-mobile, .site-header, .banner-mobile-top, ' +
        '.banner-container-billboard, .landing-pages, .user-cp-nav, .page-header-user-dashboard, .shop-details, .user-details';
    var $to_hide = jQuery(to_hide_elements_list);
    var filters_shown = false;

    var $bs_visible_xs_elements = jQuery('.total-results, .banner-mobile-top');

    var show_filters = function() {
        $filters.removeClass('hidden-xs');
        add_close_button();
        $to_hide.hide();
        $bs_visible_xs_elements.removeClass('visible-xs');
        window.scrollTo(0, 0);
        filters_shown = true;
        $(document.body).trigger('sticky_kit:recalc');
    };
    var hide_filters = function() {
        remove_close_button();
        $filters.addClass('hidden-xs');
        $to_hide.show();
        $bs_visible_xs_elements.addClass('visible-xs');
        filters_shown = false;
        $(document.body).trigger('sticky_kit:recalc');
    };
    var toggle_filters = function() {
        if (filters_shown) {
            hide_filters();
        } else {
            show_filters();
        }
    };

    var add_close_button = function() {
        if ($filters.data('close-button-added')) {
            return;
        }
        var button_markup = '<button type="button" id="close-filters" class="pull-right margin-top-sm btn btn-sm btn-default"><span class="fa fa-fw fa-close"></span> Zatvori</button>';
        var $button = jQuery(button_markup);
        $button.on('click', function(evt){
            hide_filters();
            return false;
        });
        $filters.prepend($button);
        $filters.data('close-button-added', true);
    };
    var remove_close_button = function(){
        $filters.find('#close-filters').remove();
        $filters.data('close-button-added', false);
    };

    var window_width = jQuery(window).width();
    if (OGL.is_mobile_browser || window_width <= 1279) {
        // Check if filters are perhaps not mobile-hidden, and don't do anything on those pages,
        // in order to have the ability to turn this whole thing off when needed.
        if (!$filters.hasClass('hidden-xs')) {
            // bail
            return;
        }

        // Don't attach multiple times
        if ($filters_anchor_link.data('attached')) {
            return;
        }

        // Hide the 'h2' title
        $filters.find('h2.blue').hide();

        // Bind action to "filtriraj oglase" sticky button
        $filters_anchor_link.on('click.ogl', function(evt){
            toggle_filters();
            return false;
        });
        $filters_anchor_link.data('attached', true);
    } else {
        $filters_anchor_link.off('click.ogl');
        $filters_anchor_link.data('attached', false);
    }
};


var handleMultipleReCaptcha = function() {
    jQuery('div.g-recaptcha').each(function(){
        grecaptcha.render(jQuery(this).attr('id'), {
            'sitekey': jQuery(this).data('sitekey')
        });
    });
};

OGL.fuzzyMe = function(term, query) {
    var score = 0;
    var termLength = term.length;
    var queryLength = query.length;
    var highlighting = '';
    var ti = 0;
    // -1 would not work as this would break the calculations of bonus
    // points for subsequent character matches. Something like
    // Number.MIN_VALUE would be more appropriate, but unfortunately
    // Number.MIN_VALUE + 1 equals 1...
    var previousMatchingCharacter = -2;
    for (var qi = 0; qi < queryLength && ti < termLength; qi++) {
        var qc = query.charAt(qi);
        var lowerQc = qc.toLowerCase();

        for (; ti < termLength; ti++) {
            var tc = term.charAt(ti);

            if (lowerQc === tc.toLowerCase()) {
                score++;

                if ((previousMatchingCharacter + 1) === ti) {
                    score += 2;
                }

                highlighting += '<span class="text-primary text-bold">' + tc + '</span>';
                previousMatchingCharacter = ti;
                ti++;
                break;
            } else {
                highlighting += tc;
            }
        }
    }

    highlighting += term.substring(ti, term.length);

    return {
        score: score,
        term: term,
        query: query,
        highlightedTerm: highlighting
    };
};

Date.prototype.toYYYYMMDD = function () {
    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
    var dd  = this.getDate().toString();

    return yyyy + '-' + (mm[1] ? mm : "0"+mm[0]) + '-' + (dd[1] ? dd : "0"+dd[0]);
};

String.prototype.fromYYYYMMDD = function () {
    var date_parts = this.toString().split('-');
    return new Date(date_parts[0], date_parts[1] - 1, date_parts[2]);
};

/**
 * String.repeat() polyfill (for browsers not currently supporting it)
 * @link: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/repeat
 */
if (!String.prototype.repeat) {
    String.prototype.repeat = function(count) {
        'use strict';
        if (this == null) {
            throw new TypeError('can\'t convert ' + this + ' to object');
        }
        var str = '' + this;
        count = +count;
        if (count != count) {
            count = 0;
        }
        if (count < 0) {
            throw new RangeError('repeat count must be non-negative');
        }
        if (count == Infinity) {
            throw new RangeError('repeat count must be less than infinity');
        }
        count = Math.floor(count);
        if (str.length == 0 || count == 0) {
            return '';
        }
        // Ensuring count is a 31-bit integer allows us to heavily optimize the
        // main part. But anyway, most current (August 2014) browsers can't handle
        // strings 1 << 28 chars or longer, so:
        if (str.length * count >= 1 << 28) {
            throw new RangeError('repeat count must not overflow maximum string size');
        }
        var rpt = '';
        for (;;) {
            if ((count & 1) == 1) {
                rpt += str;
            }
            count >>>= 1;
            if (count == 0) {
                break;
            }
            str += str;
        }
        return rpt;
    };
}

Number.prototype.money_number = function(show_decimals) {
    var number = this;
    if ('undefined' === typeof show_decimals) {
        show_decimals = true;
    }
    if (show_decimals || (parseFloat(number) != parseInt(number))) {
        return number.format(2, ',', '.');
    } else {
        return number.format(0, ',', '.');
    }
};

Number.prototype.format = function (decimals, dec_point, thousands_sep) {
    var number = this;
    //number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n    = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep  = ('undefined' === typeof thousands_sep) ? '.' : thousands_sep,
        dec  = ('undefined' === typeof dec_point) ? ',' : dec_point,
        s    = "",
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return "" + Math.round(n * k) / k;
        };

    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
};

Number.prototype.formatBig = function (decimals) {
    var number = this;
    if ('undefined' === typeof decimals) {
        decimals = 0;
    }

    if (number >= 1000000) {
        // millions 1M, 1.3M, ...
        var mil = number / 1000000;
        var mil_rest = number - parseInt(mil);
        if (mil_rest > 50000) {
            return mil.format(2, ',', '.') + 'M';
        } else {
            return mil.format(0, ',', '.') + 'M';
        }
    } else if (number >= 100000) {
        // thousands 100K, 200K, ...
        return (number/100000*100).format(0, ',', '.') + 'K';
    } else {
        // everything else - 97.233
        return number.format(decimals, ',', '.');
    }
};

jQuery.is_valid_uri = function($field, options) {
    var value = $field.val();
    if (value === '') {
        return true;
    }

    var allowLocal = options.allowLocal === true || options.allowLocal === 'true',
        protocol = ('http, https, ftp').split(',').join('|').replace(/\s/g, ''),
        urlExp = new RegExp(
            "^" +
                // protocol identifier
            "(?:(?:" + protocol + ")://)" +
                // user:pass authentication
            "(?:\\S+(?::\\S*)?@)?" +
            "(?:" +
                // IP address exclusion
                // private & local networks
            (allowLocal
                ? ''
                : ("(?!(?:10|127)(?:\\.\\d{1,3}){3})" +
            "(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})" +
            "(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})")) +
                // IP address dotted notation octets
                // excludes loopback network 0.0.0.0
                // excludes reserved space >= 224.0.0.0
                // excludes network & broadcast addresses
                // (first & last IP address of each class)
            "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" +
            "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" +
            "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" +
            "|" +
                // host name
            "(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)" +
                // domain name
            "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*" +
                // TLD identifier
            "(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))" +
                // Allow intranet sites (no TLD) if `allowLocal` is true
            (allowLocal ? '?' : '') +
            ")" +
                // port number
            "(?::\\d{2,5})?" +
                // resource path
            "(?:/[^\\s]*)?" +
            "$", "i"
        );

    return urlExp.test(value);
};

jQuery.slug = function(str) {
    var r = str + '';
    r = jQuery.trim(r);
    r = r.toLowerCase();
    r = r.replace (/(ǎ|â|ă|å|ą|ȧ|à|á|ä|ã)/g, "a");
    r = r.replace (/(ë|é|ĕ|ê|ě)/g, "e");
    r = r.replace (/(ǐ|î|ĭ|į|ı|í|ï)/g, "i");
    r = r.replace (/(ǒ|ô|ŏ|ǫ|ȯ|ó|ő|ö|õ)/g, "o");
    r = r.replace (/(ǔ|û|ŭ|ů|ų|ú|ű|ü)/g, "u");
    r = r.replace (/(ŷ|ẙ|ẏ|ý|ÿ)/g, "y");
    r = r.replace (/(ç|č|ć|ĉ|ċ)/g, "c");
    r = r.replace (/(ž|ź|ẑ)/g, "z");
    r = r.replace (/(š|ś|ŝ)/g, "s");
    r = r.replace (/đ/g, "d");
    r = r.replace(/ /g, "-");
    r = r.replace(/\./g, "-");
    r = r.replace(/,/g, "-");
    // r = r.replace(/\//g, "-");
    r = r.replace(/[^a-zA-Z0-9\-]/g, "");
    r = r.replace(/-+/g, "-");
    r = r.replace(/-$/, "");
    return r;
};

jQuery.fn.generateSlug = function(source) {
    return this.each(function(index, elem){
        jQuery(elem).wrap('<div class="input-group"></div>');

        var createSlug = function (e) {
            jQuery(e).val(jQuery.slug(jQuery(source).val()));
            if (jQuery.effect) {
                jQuery(e).effect('highlight', {}, 1000);
            }
        };

        jQuery(elem).after(
            jQuery('<span/>')
                .addClass('input-group-btn')
                .append(
                jQuery('<button/>')
                    .addClass('btn btn-default')
                    .attr('type', 'button')
                    .append(
                    jQuery('<i/>')
                        .addClass('fa fa-refresh')
                )
                    .click(function(){
                        createSlug(elem);
                        jQuery(elem).focus();
                    })
            )
        );

        jQuery(elem).on('focus', function () {
            if ('' === jQuery(elem).val()) {
                createSlug(elem);
            }
        });
    });
};
jQuery.fn.handleDateRange = function(options) {
    var parameter_id = options.id;
    var parameter_range_from = options.from;
    var parameter_range_to = options.to;

    if (!Modernizr.touch) {
        // Non-touch enabled devices get a jQuery plugin since it looks nicer and they can handle it

        if (parameter_range_from) {
            jQuery('#' + parameter_id + '_from').val(parameter_range_from.format('DD.MM.YYYY'));
        }
        if (parameter_range_to) {
            jQuery('#' + parameter_id + '_to').val(parameter_range_to.format('DD.MM.YYYY'));
        }

        this.datepicker({
            format: 'dd.mm.yyyy',
            weekStart: 1,
            language: 'hr',
            keyboardNavigation: false,
            disableTouchKeyboard: true,
            todayHighlight: true,
            clearBtn: true,
            beforeShowDay: function(date) {
                if (parameter_range_from && parameter_range_to) {
                    if (date >= parameter_range_from && date <= parameter_range_to) {
                        return {
                            classes: 'range'
                        };
                    }
                } else {
                    if (parameter_range_from && date >= parameter_range_from) {
                        return {
                            classes: 'range'
                        };
                    }
                    if (parameter_range_to && date <= parameter_range_to) {
                        return {
                            classes: 'range'
                        };
                    }
                }
            }
        });
        jQuery('#' + parameter_id + '_from').on('changeDate', function(e){
            if (e.date) {
                parameter_range_from = e.date;
                jQuery('#' + parameter_id + '_to').data('datepicker').setStartDate(parameter_range_from);
                jQuery('#' + parameter_id + '_from').data("datepicker").fill();
                jQuery('#' + parameter_id + '_from_hidden').val(e.date.format('YYYY-MM-DD'));
            } else {
                parameter_range_from = null;
                jQuery('#' + parameter_id + '_from_hidden').val('');
            }
        });
        jQuery('#' + parameter_id + '_to').on('changeDate',function(e){
            if (e.date) {
                parameter_range_to = e.date;
                jQuery('#' + parameter_id + '_from').data('datepicker').setEndDate(parameter_range_to);
                jQuery('#' + parameter_id + '_to').data("datepicker").fill();
                jQuery('#' + parameter_id + '_to_hidden').val(e.date.format('YYYY-MM-DD'));
            } else {
                parameter_range_to = null;
                jQuery('#' + parameter_id + '_to_hidden').val('');
            }
        });
        if (parameter_range_from) {
            jQuery('#' + parameter_id + '_to').data('datepicker').setStartDate(parameter_range_from);
        }
        if (parameter_range_to) {
            jQuery('#' + parameter_id + '_from').data('datepicker').setEndDate(parameter_range_to);
        }
    } else {
        if (Modernizr.inputtypes.date) {
            // Using native HTML5 input type=date since it appears to be supported
            document.getElementById(parameter_id + '_from').setAttribute('type', 'date');
            document.getElementById(parameter_id + '_to').setAttribute('type', 'date');
            if (parameter_range_from) {
                jQuery('#' + parameter_id + '_from').val(parameter_range_from.format('YYYY-MM-DD'));
                jQuery('#' + parameter_id + '_to').attr('min', parameter_range_from.format('YYYY-MM-DD'));
            }
            if (parameter_range_to) {
                jQuery('#' + parameter_id + '_to').val(parameter_range_to.format('YYYY-MM-DD'));
                jQuery('#' + parameter_id + '_from').attr('max', parameter_range_to.format('YYYY-MM-DD'));
            }
            jQuery('#' + parameter_id + '_from').change(function(){
                parameter_range_from = new Date(jQuery(this).val());
                jQuery('#' + parameter_id + '_to').attr('min', parameter_range_from.format('YYYY-MM-DD'));
                jQuery('#' + parameter_id + '_from_hidden').val(parameter_range_from.format('YYYY-MM-DD'));
            });
            jQuery('#' + parameter_id + '_to').change(function(){
                parameter_range_to = new Date(jQuery(this).val());
                jQuery('#' + parameter_id + '_from').attr('max', parameter_range_to.format('YYYY-MM-DD'));
                jQuery('#' + parameter_id + '_to_hidden').val(parameter_range_to.format('YYYY-MM-DD'));
            });
        } else {
            // could this be possible?
            jQuery('#' + parameter_id + '_from, #' + parameter_id + '_to').attr('placeholder', 'DD.MM.YYYY');
            if (parameter_range_from) {
                jQuery('#' + parameter_id + '_from').val(parameter_range_from.format('DD.MM.YYYY'));
            }
            if (parameter_range_to) {
                jQuery('#' + parameter_id + '_to').val(parameter_range_to.format('DD.MM.YYYY'));
            }
            jQuery('#' + parameter_id + '_from').change(function(){
                var temp_date_array = jQuery(this).val().split('.');
                if (3 === temp_date_array.length) {
                    var temp_date = temp_date_array[2] + '-' + temp_date_array[1] + '-' + temp_date_array[0];
                    parameter_range_from = new Date(temp_date);
                } else {
                    parameter_range_from = null;
                }
                if (parameter_range_from) {
                    jQuery('#' + parameter_id + '_from_hidden').val(parameter_range_from.format('YYYY-MM-DD'));
                } else {
                    jQuery(this).val('');
                    jQuery('#' + parameter_id + '_from_hidden').val('');
                }
            });
            jQuery('#' + parameter_id + '_to').change(function(){
                var temp_date_array = jQuery(this).val().split('.');
                if (3 === temp_date_array.length) {
                    var temp_date = temp_date_array[2] + '-' + temp_date_array[1] + '-' + temp_date_array[0];
                    parameter_range_to = new Date(temp_date);
                } else {
                    parameter_range_to = null;
                }
                if (parameter_range_to) {
                    jQuery('#' + parameter_id + '_to_hidden').val(parameter_range_to.format('YYYY-MM-DD'));
                } else {
                    jQuery(this).val('');
                    jQuery('#' + parameter_id + '_to_hidden').val('');
                }
            });
        }
    }
};

/**
 * Helper method to open external links in new window
 */
jQuery.fn.openLinksInNewWindow = function() {
    this.find('a').each(function () {
        if ('#' !== this.href && this.href !== '') {
            if (this.hostname != document.location.hostname) {
                this.target = "_blank";
            }
        }
    });
};

jQuery.fn.moreless = function(lang) {
    return this.each(function(index, elem){
        var lang_strings = {
            'hr' : {
                'more' : 'Više',
                'less' : 'Manje'
            },
            'en' : {
                'more' : 'More',
                'less' : 'Less'
            }
        };

        if ('undefined' === typeof lang) {
            lang = 'en';
        }

        jQuery(elem).wrap('<div class="moreless_container" style="position:relative;padding-bottom:15px;"></div>');
        jQuery(elem).children(':gt(3)').hide();

        var toggleMoreLess = function (e) {
            $moreLessBtn = jQuery(e).parent().find('span.moreless-btn');
            if ('more' == $moreLessBtn.data('next')) {
                $moreLessBtn.html('<span class="fa fa-caret-up fa-fw"><!--IE--></span> ' + lang_strings[lang].less);
                jQuery(elem).children(':gt(3)').fadeIn('fast', function(){
                    $moreLessBtn.attr('data-next', 'less').data('next', 'less');
                });
            } else if ('less' == $moreLessBtn.data('next')) {
                jQuery(elem).children(':gt(3)').fadeOut('fast', function(){
                    $moreLessBtn.attr('data-next', 'more').data('next', 'more').html('<span class="fa fa-caret-down fa-fw"><!--IE--></span> ' + lang_strings[lang].more);
                });
            }
        };

        jQuery(elem).after(
            jQuery('<span/>')
                .addClass('moreless-btn')
                .css({
                    "position": "absolute",
                    "bottom": "5px",
                    "left": "2px",
                    "font-size": "10px",
                    "text-transform": "uppercase",
                    "color": "#737373",
                    "cursor": "pointer"
                })
                .attr('data-next', 'more').data('next', 'more')
                .html('<span class="fa fa-caret-down fa-fw"><!--IE--></span> ' + lang_strings[lang].more)
                .click(function(){
                    toggleMoreLess(elem);
                })
        );
    });
};

jQuery.fn.disableEnterSubmit = function() {
    return this.each(function(index, elem){
        jQuery(elem).keypress(function(e) {
            var node = (e.target) ? e.target : ((e.srcElement) ? e.srcElement : null);
            if ((e.keyCode == 13) && (node.type=="text"))  { return false; }
        });
    });
};

jQuery.fn.random = function () {
    var randomIndex = Math.floor(Math.random() * this.length);
    return jQuery(this[randomIndex]);
};

jQuery.fn.clickOnEnter = function($element_to_click) {
    return this.each(function(index, elem){
        jQuery(this).keypress(function(e) {
            var node = (e.target) ? e.target : ((e.srcElement) ? e.srcElement : null);
            if ((e.keyCode == 13) && (node.type=="text"))  { $element_to_click.click(); }
        });
    });
};

jQuery.fn.fnOnEnter = function(funkcija) {
    return this.each(function(index, elem){
        jQuery(this).keypress(function(e) {
            var node = (e.target) ? e.target : ((e.srcElement) ? e.srcElement : null);
            if ((e.keyCode == 13) && (node.type=="text")) { funkcija (); }
        });
    });
};

jQuery.fn.cleanURL = function() {
    return this.each(function(index, elem){
        var fix_button = jQuery('<span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-wrench"></i></button></span>');
        var fixUrl = function (e) {
            var oldVal = jQuery(e).val();
            var newVal = oldVal.replace(/^(https?|ftp):\/\//, '');
            if (oldVal !== newVal) {
                jQuery(e).val(newVal);
                jQuery(e).effect("highlight", {}, 1000);
            }
        };
        fix_button.find('button').click(function(){fixUrl(elem);});
        jQuery(elem).after(fix_button);
        jQuery(elem).blur(function() {fixUrl(elem);});
    });
};

/**
 * Parser for URI Parameters
 * @param {String} uri_type Return only certain type of parameter (all|video|non-video)
 */
jQuery.fn.getAllURIParameters = function(uri_type) {
    var acceptable_uri_types = ['all', 'video', 'non-video'];
    var acceptable_video_types = ['youtube', 'vimeo'];

    if ('undefined' === typeof uri_type) {
        uri_type = 'all';
    } else {
        uri_type = uri_type.toLowerCase();
    }
    if (-1 === acceptable_uri_types.indexOf(uri_type)) {
        uri_type = 'all';
    }

    var allURIParameters = [];
    var foundURIParameters = this.find('a[data-url-parameter-value="true"]');

    if ('undefined' !== foundURIParameters && foundURIParameters.length > 0) {

        var parseURI = function($elem) {
            var href = 'undefined' !== $elem.attr('href') ? $elem.attr('href') : null;
            if (href && href.toString().substring(0, 4) == 'http') {
                if (href.toString().match(/(youtube\.com|youtu\.be)/gi)) {
                    var YTID_array = href.toString().match(/(\?v=|\/\d\/|\/embed\/|\/v\/|\.be\/)([a-zA-Z0-9\-\_]+)/);
                    if (YTID_array && typeof YTID_array !== 'undefined' && YTID_array.length == 3 && YTID_array[2] !== undefined) {
                        return {
                            'elem'    : $elem,
                            'type'    : 'youtube',
                            'video_id': YTID_array[2],
                            'url'     : href,
                            'poster'  : '//img.youtube.com/vi/' + YTID_array[2] + '/0.jpg'
                        };
                    }
                } else if (href.toString().match(/(www\.)?vimeo\.com/gi)) {
                    var VID_array = href.toString().match(/(www\.)?vimeo\.com\/(\w*\/)*(([a-z]{0,2}-)?\d+)/);
                    if (VID_array && typeof VID_array !== 'undefined' && VID_array.length >= 4 && VID_array[3] !== undefined) {
                        $.get('http://vimeo.com/api/v2/video/' + VID_array[3] + '.json', function(data) {
                            return {
                                'elem'    : $elem,
                                'type'    : 'vimeo',
                                'video_id': VID_array[3],
                                'url'     : href,
                                'poster'  : data[0].thumbnail_large
                            };
                        });
                    }
                } else {
                    return {
                        'elem': $elem,
                        'type': 'normal',
                        'url' : href
                    };
                }
            } else {
                return null;
            }
        };

        if ('video' === uri_type) {
            foundURIParameters.each(function(i, elem){
                var parsedURI = parseURI($(elem));
                if (parsedURI && -1 !== acceptable_video_types.indexOf(parsedURI.type)) {
                    allURIParameters.push(parsedURI);
                }
            });
        } else if ('non-video' === uri_type) {
            foundURIParameters.each(function(i, elem){
                var parsedURI = parseURI($(elem));
                if (parsedURI && -1 === acceptable_video_types.indexOf(parsedURI.type)) {
                    allURIParameters.push(parsedURI);
                }
            });
        } else {
            foundURIParameters.each(function(i, elem){
                var parsedURI = parseURI($(elem));
                if (parsedURI) {
                    allURIParameters.push(parsedURI);
                }
            });
        }
    }
    return allURIParameters;
};

jQuery.fn.sidebardCategoriesDrillDown = function() {
    if (this.length == 1) {
        var sidebardCategoriesNodeClick = function($sender) {
            var $children = $sender.closest('li').find('> ul');
            if ($children.length == 1) {
                if ($children.data('hidden') == 1) {
                    $children.slideDown('fast', function(){
                        $children.data('hidden', '0');
                    });
                } else {
                    $children.slideUp('fast', function(){
                        $children.data('hidden', '1');
                    });
                }
            }
        };

        var $ul = this;
        if ($ul.prop('tagName') != 'UL') {
            $ul = this.find('> ul');
            if ($ul.length != 1) {
                $ul = null;
            }
        }
        if ($ul) {
            $ul.find('a').click(function(event){
                var $clickedLink = jQuery(this);
                if (!$clickedLink.closest('li').hasClass('leaf-node')) {
                    event.preventDefault();
                    sidebardCategoriesNodeClick($clickedLink);
                }
            });
            if ($ul.hasClass('expanded')) {
                $ul.find('li ul').data('hidden', '0').show();
            } else {
                $ul.find('li ul').data('hidden', '1').hide();

                var $currentlySelectedNode = $ul.find('a.current');
                if ($currentlySelectedNode.length) {
                    var $ulParents = $currentlySelectedNode.parentsUntil('.depth-0', 'ul');
                    $ulParents.data('hidden', '0').show();
                }
            }
        }
    }
    return this;
};

/**
 * Generic form change watcher. Provides a hasChanges() method on the form.
 *
 * Example usage:
 *
 * var $form = $('#some-form');
 * $form.changeWatcher();
 *
 * // now you can use $form.hasChanges() whenever you need it
 *
 * // Or you can implement something like this more easily:
 *
 * // Alert on navigating away if there are any unsaved changes (unless a submit happens)
 * $(window).on('beforeunload', function(e){
 *     if ($form.hasChanges()) {
 *          return 'Unsaved changes detected, continue anyway?';
 *     }
 * });
 * // Turn off beforeunload handler if form submit is happening
 * $form.on('submit', function(e){
 *     $(window).off('beforeunload');
 * });
 */
(function($){
    $.fn.changeWatcher = function(){
        return this.each(function(){
            $.data(this, 'form-hash', $(this).serialize());
        });
    };
    $.fn.hasChanges = function () {
        var changed = false;

        this.each(function(){
            var hash = $.data(this, 'form-hash');

            if (null != hash && hash !== $(this).serialize()) {
                changed = true;
                return false;
            }
        });

        return changed;
    };
}).call(window, jQuery);

/**
 * Handles ajax-based favorites UI
 */
var Favoriter = {
    controls: [],
    init: function (){
        var ids = [];
        this.controls = $('button.toggle-favorite, span.toggle-favorite');
        this.controls.each(function(i,el){
            ids.push($(el).data('id'))
        });

        // Goes through the returned results and marks ads accordingly
        var self = this;
        var favs_callback = function(response) {
            if (response.favs) {
                for (var k in response.favs) {
                    if (response.favs.hasOwnProperty(k)) {
                        var is_fav = response.favs[k];
                        var $control = self.controls.filter('[data-id=' + k + ']');
                        if ($control.length > 0) {
                            self.control_state_toggle($control, is_fav);
                        }
                    }
                }
                // Hook up clicks
                self.bind_clicks();
            }
        };

        // Build the favorites UI based on ajax results
        if (ids.length > 0) {
            var loc = '/ajax/favorites/' + ids.join(',');
            $.get(
                loc,
                favs_callback,
                'json'
            );
        }
    },
    control_state_toggle: function($control, active) {
        if (active) {
            $control.addClass('active').attr('title', 'Obriši iz spremljenih oglasa');
        } else {
            $control.removeClass('active').attr('title', 'Spremi oglas');
        }
    },
    bind_clicks: function(){
        var self = this;
        this.controls.removeAttr('disabled').removeClass('disabled');

        var click_handler = function($control, ad_id, action){
            jQuery.post(
                '/ajax/favorite-ad/' + ad_id + '/' + action,
                function (json) {
                    if (json && json.status) {
                        self.control_state_toggle($control, ('set' === action));
                    }
                    /*if (json && json.data && !json.data.logged_in) {
                        alert("Ooops!\nMolimo vas da se prijavite u vaš račun kako bi mogli spremiti ovaj oglas u svoj račun...");
                    }*/
                }
            );
        };

        this.controls.on('click', function(e){
            e.preventDefault();
            var $ctrl = jQuery(this);
            var action = $ctrl.hasClass('active') ? 'remove' : 'set';
            click_handler($ctrl, $ctrl.data('id'), action);
        });
    }
};


/**
 * Plugin to ajaxify modal forms in order to display validations inside the modal dialog without leaving the page
 */
jQuery.fn.ajaxifyModalForm = function() {
    return this.each(function(i, form){
        var $form  = jQuery(form);
        var $alert = $form.find('.alert');
        var $modal = $form.closest('.modal');
        if ($modal.length == 0) {
            $modal = null;
        }

        var setValidationErrors = function(json){
            // clear old validation errors
            $alert.addClass('hidden').removeClass('alert-succes alert-danger').html('');
            $form.find('.form-group.has-error').removeClass('has-error');
            $form.find('.form-group p.help-block').html('');

            if ('undefined' !== typeof json.msg) {
                $alert.addClass('alert-danger').removeClass('alert-success hidden').html(json.msg);
            }

            if ('undefined' !== typeof json.errors) {
                jQuery(json.errors).each(function(i, error){
                    $errorField = $form.find('#' + error.field);
                    if ('undefined' !== typeof $errorField && 1 === $errorField.length) {
                        $errorFormGroup = $errorField.closest('.form-group');
                        if ('undefined' !== typeof $errorFormGroup && 1 === $errorFormGroup.length) {
                            $errorFormGroup.addClass('has-error');
                            $errorFormGroup.find('p.help-block').html(error.value);
                        }
                    }
                });
            }
        };

        var submitFormData = function(){
            jQuery.post(
                $form.attr('action'),
                $form.serializeArray(),
                function(json) {
                    if (json.status) {
                        if ('undefined' !== typeof json.msg) {
                            $alert.addClass('alert-success').html(json.msg).removeClass('alert-danger hidden');
                            if ($modal) {
                                setTimeout(function(){
                                    $modal.modal('hide');
                                    $form.trigger('reset');
                                    $alert.addClass('hidden').removeClass('alert-succes alert-danger').html('');
                                }, 1000);
                            }
                        }
                    } else {
                        setValidationErrors(json);
                    }
                },
                'json'
            );
        };

        $form.submit(function(event) {
            event.preventDefault();
            submitFormData();
        });
    });
};

(function($) {
    $.widget('ogl.phoneNumberHandler', {
        options: {
            'pluginName'            : 'phoneNumberHandler',
            'carret'                : '<span class="caret"></span>',
            'type'                  : 'auto',
            'wrapper_class'         : 'phoneNumberHandler',
            'preselectedCountryID'  : null,
            'domesticCountryID'     : 1,
            'countryBtnPrefixTitle' : 'Odaberite zemlju',
            'countryPrefixes'       : [
                {'code': '', 'id': 0, 'prefix': 'Odaberite zemlju', 'title': 'Bez broja', 'type': 'prefix'},
                {'code': 'hr', 'id': 1, 'prefix': '+385', 'title': 'Hrvatska', 'type': 'prefix'},
                {'code': 'ba', 'id': 2, 'prefix': '+387', 'title': 'Bosna i Hercegovina', 'type': 'prefix'},
                {'code': 'me', 'id': 3, 'prefix': '+382', 'title': 'Crna Gora', 'type': 'prefix'},
                {'code': 'ks', 'id': 4, 'prefix': '+381', 'title': 'Kosovo', 'type': 'prefix'},
                {'code': 'mk', 'id': 5, 'prefix': '+389', 'title': 'Makedonija', 'type': 'prefix'},
                {'code': 'si', 'id': 6, 'prefix': '+386', 'title': 'Slovenija', 'type': 'prefix'},
                {'code': 'rs', 'id': 7, 'prefix': '+381', 'title': 'Srbija', 'type': 'prefix'},
                {'type': 'separator'},
                {'code': 'af', 'id': 8, 'prefix': '+93', 'title': 'Afganistan', 'type': 'prefix'},
                {'code': 'al', 'id': 9, 'prefix': '+355', 'title': 'Albanija', 'type': 'prefix'},
                {'code': 'dz', 'id': 10, 'prefix': '+213', 'title': 'Alžir', 'type': 'prefix'},
                {'code': 'as', 'id': 12, 'prefix': '+1', 'title': 'Američka Samoa', 'type': 'prefix'},
                {'code': 'ad', 'id': 13, 'prefix': '+376', 'title': 'Andora', 'type': 'prefix'},
                {'code': 'ai', 'id': 14, 'prefix': '+1', 'title': 'Angila', 'type': 'prefix'},
                {'code': 'ao', 'id': 15, 'prefix': '+244', 'title': 'Angola', 'type': 'prefix'},
                {'code': 'aq', 'id': 22, 'prefix': '+672', 'title': 'Antarktika', 'type': 'prefix'},
                {'code': 'ag', 'id': 16, 'prefix': '+1', 'title': 'Antigva i Barbuda', 'type': 'prefix'},
                {'code': 'ar', 'id': 17, 'prefix': '+54', 'title': 'Argentina', 'type': 'prefix'},
                {'code': 'am', 'id': 18, 'prefix': '+374', 'title': 'Armenija', 'type': 'prefix'},
                {'code': 'aw', 'id': 19, 'prefix': '+297', 'title': 'Aruba', 'type': 'prefix'},
                {'code': 'au', 'id': 21, 'prefix': '+61', 'title': 'Australija', 'type': 'prefix'},
                {'code': 'at', 'id': 23, 'prefix': '+43', 'title': 'Austrija', 'type': 'prefix'},
                {'code': 'az', 'id': 24, 'prefix': '+994', 'title': 'Azerbajdžan', 'type': 'prefix'},
                {'code': 'bs', 'id': 26, 'prefix': '+1', 'title': 'Bahama', 'type': 'prefix'},
                {'code': 'bh', 'id': 27, 'prefix': '+973', 'title': 'Bahrein', 'type': 'prefix'},
                {'code': 'bd', 'id': 28, 'prefix': '+880', 'title': 'Bangladeš', 'type': 'prefix'},
                {'code': 'bb', 'id': 29, 'prefix': '+1', 'title': 'Barbados', 'type': 'prefix'},
                {'code': 'be', 'id': 30, 'prefix': '+32', 'title': 'Belgija', 'type': 'prefix'},
                {'code': 'bz', 'id': 31, 'prefix': '+501', 'title': 'Belize', 'type': 'prefix'},
                {'code': 'bj', 'id': 32, 'prefix': '+229', 'title': 'Benin', 'type': 'prefix'},
                {'code': 'bm', 'id': 33, 'prefix': '+1', 'title': 'Bermuda', 'type': 'prefix'},
                {'code': 'by', 'id': 34, 'prefix': '+375', 'title': 'Bjelorusija', 'type': 'prefix'},
                {'code': 'bw', 'id': 35, 'prefix': '+267', 'title': 'Bocvana', 'type': 'prefix'},
                {'code': 'bo', 'id': 36, 'prefix': '+591', 'title': 'Bolivija', 'type': 'prefix'},
                {'code': 'br', 'id': 37, 'prefix': '+55', 'title': 'Brazil', 'type': 'prefix'},
                {'code': 'bn', 'id': 38, 'prefix': '+673', 'title': 'Brunej', 'type': 'prefix'},
                {'code': 'bg', 'id': 39, 'prefix': '+359', 'title': 'Bugarska', 'type': 'prefix'},
                {'code': 'bf', 'id': 40, 'prefix': '+226', 'title': 'Burkina Faso', 'type': 'prefix'},
                {'code': 'bi', 'id': 41, 'prefix': '+257', 'title': 'Burundi', 'type': 'prefix'},
                {'code': 'bt', 'id': 42, 'prefix': '+975', 'title': 'Butan', 'type': 'prefix'},
                {'code': 'cy', 'id': 44, 'prefix': '+357', 'title': 'Cipar', 'type': 'prefix'},
                {'code': 'ck', 'id': 45, 'prefix': '+682', 'title': 'Cookovo otocje', 'type': 'prefix'},
                {'code': 'td', 'id': 46, 'prefix': '+235', 'title': 'Čad', 'type': 'prefix'},
                {'code': 'cz', 'id': 47, 'prefix': '+420', 'title': 'Češka Republika', 'type': 'prefix'},
                {'code': 'cl', 'id': 11, 'prefix': '+56', 'title': 'Čile', 'type': 'prefix'},
                {'code': 'dk', 'id': 48, 'prefix': '+45', 'title': 'Danska', 'type': 'prefix'},
                {'code': 'vi', 'id': 50, 'prefix': '+1', 'title': 'Djevičansko otočje', 'type': 'prefix'},
                {'code': 'dm', 'id': 51, 'prefix': '+1', 'title': 'Dominika', 'type': 'prefix'},
                {'code': 'do', 'id': 52, 'prefix': '+1', 'title': 'Dominikanska Republika', 'type': 'prefix'},
                {'code': 'dj', 'id': 53, 'prefix': '+253', 'title': 'Džibuti', 'type': 'prefix'},
                {'code': 'eg', 'id': 54, 'prefix': '+20', 'title': 'Egipat', 'type': 'prefix'},
                {'code': 'ec', 'id': 55, 'prefix': '+593', 'title': 'Ekvador', 'type': 'prefix'},
                {'code': 'gq', 'id': 56, 'prefix': '+240', 'title': 'Ekvatorska Gvineja', 'type': 'prefix'},
                {'code': 'sv', 'id': 57, 'prefix': '+503', 'title': 'El Salvador', 'type': 'prefix'},
                {'code': 'er', 'id': 58, 'prefix': '+291', 'title': 'Eritreja', 'type': 'prefix'},
                {'code': 'ee', 'id': 59, 'prefix': '+372', 'title': 'Estonija', 'type': 'prefix'},
                {'code': 'et', 'id': 60, 'prefix': '+251', 'title': 'Etiopija', 'type': 'prefix'},
                {'code': 'fk', 'id': 61, 'prefix': '+500', 'title': 'Falklandsko otočje', 'type': 'prefix'},
                {'code': 'fo', 'id': 62, 'prefix': '+298', 'title': 'Farski otoci', 'type': 'prefix'},
                {'code': 'fj', 'id': 63, 'prefix': '+679', 'title': 'Fidži', 'type': 'prefix'},
                {'code': 'ph', 'id': 64, 'prefix': '+63', 'title': 'Filipini', 'type': 'prefix'},
                {'code': 'fi', 'id': 65, 'prefix': '+358', 'title': 'Finska', 'type': 'prefix'},
                {'code': 'fr', 'id': 66, 'prefix': '+33', 'title': 'Francuska', 'type': 'prefix'},
                {'code': 'gf', 'id': 67, 'prefix': '+594', 'title': 'Francuska Gijana', 'type': 'prefix'},
                {'code': 'pf', 'id': 68, 'prefix': '+689', 'title': 'Francuska Polinezija', 'type': 'prefix'},
                {'code': 'ga', 'id': 69, 'prefix': '+241', 'title': 'Gabon', 'type': 'prefix'},
                {'code': 'gm', 'id': 70, 'prefix': '+220', 'title': 'Gambija', 'type': 'prefix'},
                {'code': 'gh', 'id': 71, 'prefix': '+233', 'title': 'Gana', 'type': 'prefix'},
                {'code': 'gi', 'id': 72, 'prefix': '+350', 'title': 'Gibraltar', 'type': 'prefix'},
                {'code': 'gr', 'id': 73, 'prefix': '+30', 'title': 'Grčka', 'type': 'prefix'},
                {'code': 'gd', 'id': 74, 'prefix': '+1', 'title': 'Grenada', 'type': 'prefix'},
                {'code': 'gl', 'id': 75, 'prefix': '+299', 'title': 'Grenland', 'type': 'prefix'},
                {'code': 'ge', 'id': 76, 'prefix': '+995', 'title': 'Gruzija', 'type': 'prefix'},
                {'code': 'gu', 'id': 77, 'prefix': '+1', 'title': 'Guam', 'type': 'prefix'},
                {'code': 'gp', 'id': 78, 'prefix': '+590', 'title': 'Gvadalupa', 'type': 'prefix'},
                {'code': 'gy', 'id': 79, 'prefix': '+592', 'title': 'Gvajana', 'type': 'prefix'},
                {'code': 'gt', 'id': 80, 'prefix': '+502', 'title': 'Gvatemala', 'type': 'prefix'},
                {'code': 'gn', 'id': 81, 'prefix': '+224', 'title': 'Gvineja', 'type': 'prefix'},
                {'code': 'gw', 'id': 82, 'prefix': '+245', 'title': 'Gvineja Bisau', 'type': 'prefix'},
                {'code': 'ht', 'id': 83, 'prefix': '+509', 'title': 'Haiti', 'type': 'prefix'},
                {'code': 'hn', 'id': 85, 'prefix': '+504', 'title': 'Honduras', 'type': 'prefix'},
                {'code': 'hk', 'id': 86, 'prefix': '+852', 'title': 'HongKong', 'type': 'prefix'},
                {'code': 'in', 'id': 87, 'prefix': '+91', 'title': 'Indija', 'type': 'prefix'},
                {'code': 'id', 'id': 88, 'prefix': '+62', 'title': 'Indonezija', 'type': 'prefix'},
                {'code': 'iq', 'id': 89, 'prefix': '+964', 'title': 'Irak', 'type': 'prefix'},
                {'code': 'ir', 'id': 90, 'prefix': '+98', 'title': 'Iran', 'type': 'prefix'},
                {'code': 'ie', 'id': 91, 'prefix': '+353', 'title': 'Irska', 'type': 'prefix'},
                {'code': 'is', 'id': 92, 'prefix': '+354', 'title': 'Island', 'type': 'prefix'},
                {'code': 'tl', 'id': 49, 'prefix': '+670', 'title': 'Istočni Timor', 'type': 'prefix'},
                {'code': 'it', 'id': 93, 'prefix': '+39', 'title': 'Italija', 'type': 'prefix'},
                {'code': 'il', 'id': 94, 'prefix': '+972', 'title': 'Izrael', 'type': 'prefix'},
                {'code': 'jm', 'id': 95, 'prefix': '+1', 'title': 'Jamajka', 'type': 'prefix'},
                {'code': 'jp', 'id': 96, 'prefix': '+81', 'title': 'Japan', 'type': 'prefix'},
                {'code': 'ye', 'id': 97, 'prefix': '+967', 'title': 'Jemen', 'type': 'prefix'},
                {'code': 'jo', 'id': 98, 'prefix': '+962', 'title': 'Jordan', 'type': 'prefix'},
                {'code': 'za', 'id': 99, 'prefix': '+27', 'title': 'Južnoafricka Republika', 'type': 'prefix'},
                {'code': 'ky', 'id': 43, 'prefix': '+1', 'title': 'Kajmanski otoci', 'type': 'prefix'},
                {'code': 'kh', 'id': 100, 'prefix': '+855', 'title': 'Kambodža', 'type': 'prefix'},
                {'code': 'cm', 'id': 101, 'prefix': '+237', 'title': 'Kamerun', 'type': 'prefix'},
                {'code': 'ca', 'id': 102, 'prefix': '+1', 'title': 'Kanada', 'type': 'prefix'},
                {'code': 'qa', 'id': 103, 'prefix': '+974', 'title': 'Katar', 'type': 'prefix'},
                {'code': 'kz', 'id': 104, 'prefix': '+7', 'title': 'Kazahstan', 'type': 'prefix'},
                {'code': 'ke', 'id': 105, 'prefix': '+254', 'title': 'Kenija', 'type': 'prefix'},
                {'code': 'cn', 'id': 106, 'prefix': '+86', 'title': 'Kina', 'type': 'prefix'},
                {'code': 'kg', 'id': 107, 'prefix': '+996', 'title': 'Kirgistan', 'type': 'prefix'},
                {'code': 'ki', 'id': 108, 'prefix': '+686', 'title': 'Kiribati', 'type': 'prefix'},
                {'code': 'co', 'id': 109, 'prefix': '+57', 'title': 'Kolumbija', 'type': 'prefix'},
                {'code': 'km', 'id': 110, 'prefix': '+269', 'title': 'Komori', 'type': 'prefix'},
                {'code': 'cg', 'id': 111, 'prefix': '+242', 'title': 'Kongo', 'type': 'prefix'},
                {'code': 'cd', 'id': 112, 'prefix': '+243', 'title': 'Kongo Demokratska Republika', 'type': 'prefix'},
                {'code': 'kp', 'id': 113, 'prefix': '+850', 'title': 'Koreja, Južna', 'type': 'prefix'},
                {'code': 'kr', 'id': 114, 'prefix': '+82', 'title': 'Koreja, Sjeverna', 'type': 'prefix'},
                {'code': 'cr', 'id': 115, 'prefix': '+506', 'title': 'Kostarika', 'type': 'prefix'},
                {'code': 'cu', 'id': 116, 'prefix': '+53', 'title': 'Kuba', 'type': 'prefix'},
                {'code': 'kw', 'id': 117, 'prefix': '+965', 'title': 'Kuvajt', 'type': 'prefix'},
                {'code': 'la', 'id': 118, 'prefix': '+856', 'title': 'Laos', 'type': 'prefix'},
                {'code': 'ls', 'id': 119, 'prefix': '+266', 'title': 'Lesoto', 'type': 'prefix'},
                {'code': 'lv', 'id': 120, 'prefix': '+371', 'title': 'Letonija-Latvija', 'type': 'prefix'},
                {'code': 'lb', 'id': 121, 'prefix': '+961', 'title': 'Libanon', 'type': 'prefix'},
                {'code': 'lr', 'id': 122, 'prefix': '+231', 'title': 'Liberija', 'type': 'prefix'},
                {'code': 'ly', 'id': 123, 'prefix': '+218', 'title': 'Libija', 'type': 'prefix'},
                {'code': 'li', 'id': 124, 'prefix': '+423', 'title': 'Lihtenštajn', 'type': 'prefix'},
                {'code': 'lt', 'id': 125, 'prefix': '+370', 'title': 'Litva', 'type': 'prefix'},
                {'code': 'lu', 'id': 126, 'prefix': '+352', 'title': 'Luksemburg', 'type': 'prefix'},
                {'code': 'mg', 'id': 127, 'prefix': '+261', 'title': 'Madagaskar', 'type': 'prefix'},
                {'code': 'hu', 'id': 128, 'prefix': '+36', 'title': 'Mađarska', 'type': 'prefix'},
                {'code': 'mo', 'id': 129, 'prefix': '+853', 'title': 'Makao', 'type': 'prefix'},
                {'code': 'mw', 'id': 130, 'prefix': '+265', 'title': 'Malavi', 'type': 'prefix'},
                {'code': 'mv', 'id': 131, 'prefix': '+960', 'title': 'Maldivi', 'type': 'prefix'},
                {'code': 'my', 'id': 132, 'prefix': '+60', 'title': 'Malezija', 'type': 'prefix'},
                {'code': 'ml', 'id': 133, 'prefix': '+223', 'title': 'Mali', 'type': 'prefix'},
                {'code': 'mt', 'id': 134, 'prefix': '+356', 'title': 'Malta', 'type': 'prefix'},
                {'code': 'mp', 'id': 135, 'prefix': '+1', 'title': 'Marijansko otočje', 'type': 'prefix'},
                {'code': 'ma', 'id': 136, 'prefix': '+212', 'title': 'Maroko', 'type': 'prefix'},
                {'code': 'mh', 'id': 137, 'prefix': '+692', 'title': 'Marshallovi otoci', 'type': 'prefix'},
                {'code': 'mq', 'id': 138, 'prefix': '+596', 'title': 'Martinik', 'type': 'prefix'},
                {'code': 'mr', 'id': 139, 'prefix': '+222', 'title': 'Mauretanija', 'type': 'prefix'},
                {'code': 'mu', 'id': 140, 'prefix': '+230', 'title': 'Mauricius', 'type': 'prefix'},
                {'code': 'yt', 'id': 141, 'prefix': '+262', 'title': 'Mayotte', 'type': 'prefix'},
                {'code': 'mx', 'id': 142, 'prefix': '+52', 'title': 'Meksiko', 'type': 'prefix'},
                {'code': 'mm', 'id': 143, 'prefix': '+95', 'title': 'Mianma-Myanma', 'type': 'prefix'},
                {'code': 'fm', 'id': 144, 'prefix': '+691', 'title': 'Mikronezija', 'type': 'prefix'},
                {'code': 'md', 'id': 145, 'prefix': '+373', 'title': 'Moldavija', 'type': 'prefix'},
                {'code': 'mc', 'id': 146, 'prefix': '+377', 'title': 'Monako', 'type': 'prefix'},
                {'code': 'mn', 'id': 147, 'prefix': '+976', 'title': 'Mongolija', 'type': 'prefix'},
                {'code': 'ms', 'id': 148, 'prefix': '+1', 'title': 'Montserrat', 'type': 'prefix'},
                {'code': 'mz', 'id': 149, 'prefix': '+258', 'title': 'Mozambik', 'type': 'prefix'},
                {'code': 'na', 'id': 150, 'prefix': '+264', 'title': 'Namibija', 'type': 'prefix'},
                {'code': 'nr', 'id': 151, 'prefix': '+674', 'title': 'Nauru', 'type': 'prefix'},
                {'code': 'np', 'id': 152, 'prefix': '+977', 'title': 'Nepal', 'type': 'prefix'},
                {'code': 'ne', 'id': 153, 'prefix': '+227', 'title': 'Niger', 'type': 'prefix'},
                {'code': 'ng', 'id': 154, 'prefix': '+234', 'title': 'Nigerija', 'type': 'prefix'},
                {'code': 'ni', 'id': 155, 'prefix': '+505', 'title': 'Nikaragva', 'type': 'prefix'},
                {'code': 'nu', 'id': 156, 'prefix': '+683', 'title': 'Niue', 'type': 'prefix'},
                {'code': 'nl', 'id': 157, 'prefix': '+31', 'title': 'Nizozemska', 'type': 'prefix'},
                {'code': 'an', 'id': 158, 'prefix': '+599', 'title': 'Nizozemski Antili', 'type': 'prefix'},
                {'code': 'nf', 'id': 159, 'prefix': '+672', 'title': 'Norfolk', 'type': 'prefix'},
                {'code': 'no', 'id': 160, 'prefix': '+47', 'title': 'Norveška', 'type': 'prefix'},
                {'code': 'nc', 'id': 161, 'prefix': '+687', 'title': 'Nova Kaledonija', 'type': 'prefix'},
                {'code': 'nz', 'id': 162, 'prefix': '+64', 'title': 'Novi Zeland', 'type': 'prefix'},
                {'code': 'de', 'id': 163, 'prefix': '+49', 'title': 'Njemacka', 'type': 'prefix'},
                {'code': 'ci', 'id': 164, 'prefix': '+225', 'title': 'Obala Bjelokosti', 'type': 'prefix'},
                {'code': 'om', 'id': 165, 'prefix': '+968', 'title': 'Oman', 'type': 'prefix'},
                {'code': 'pk', 'id': 166, 'prefix': '+92', 'title': 'Pakistan', 'type': 'prefix'},
                {'code': 'pw', 'id': 167, 'prefix': '+680', 'title': 'Palau', 'type': 'prefix'},
                {'code': 'ps', 'id': 20, 'prefix': '+970', 'title': 'Palestina', 'type': 'prefix'},
                {'code': 'pa', 'id': 168, 'prefix': '+507', 'title': 'Panama', 'type': 'prefix'},
                {'code': 'pg', 'id': 169, 'prefix': '+675', 'title': 'Papua Nova Gvineja', 'type': 'prefix'},
                {'code': 'py', 'id': 170, 'prefix': '+595', 'title': 'Paragvaj', 'type': 'prefix'},
                {'code': 'pe', 'id': 171, 'prefix': '+51', 'title': 'Peru', 'type': 'prefix'},
                {'code': 'pn', 'id': 25, 'prefix': '+64', 'title': 'Pitcairnovo Otočje', 'type': 'prefix'},
                {'code': 'pl', 'id': 172, 'prefix': '+48', 'title': 'Poljska', 'type': 'prefix'},
                {'code': 'pr', 'id': 173, 'prefix': '+1', 'title': 'Portoriko', 'type': 'prefix'},
                {'code': 'pt', 'id': 174, 'prefix': '+351', 'title': 'Portugal', 'type': 'prefix'},
                {'code': 're', 'id': 175, 'prefix': '+262', 'title': 'Réunion', 'type': 'prefix'},
                {'code': 'rw', 'id': 176, 'prefix': '+250', 'title': 'Ruanda', 'type': 'prefix'},
                {'code': 'ro', 'id': 177, 'prefix': '+40', 'title': 'Rumunjska', 'type': 'prefix'},
                {'code': 'ru', 'id': 178, 'prefix': '+7', 'title': 'Ruska Federacija', 'type': 'prefix'},
                {'code': 'sb', 'id': 179, 'prefix': '+677', 'title': 'Salamunski otoci', 'type': 'prefix'},
                {'code': 'sm', 'id': 180, 'prefix': '+378', 'title': 'San Marino', 'type': 'prefix'},
                {'code': 'st', 'id': 181, 'prefix': '+239', 'title': 'San Tome i Prinsipe', 'type': 'prefix'},
                {'code': 'sa', 'id': 182, 'prefix': '+966', 'title': 'Saudijska Arabija', 'type': 'prefix'},
                {'code': 'sc', 'id': 183, 'prefix': '+248', 'title': 'Sejšeli', 'type': 'prefix'},
                {'code': 'sn', 'id': 184, 'prefix': '+221', 'title': 'Senegal', 'type': 'prefix'},
                {'code': 'sl', 'id': 185, 'prefix': '+232', 'title': 'Sijera Leone', 'type': 'prefix'},
                {'code': 'sg', 'id': 186, 'prefix': '+65', 'title': 'Singapur', 'type': 'prefix'},
                {'code': 'sy', 'id': 187, 'prefix': '+963', 'title': 'Sirija', 'type': 'prefix'},
                {'code': 'us', 'id': 188, 'prefix': '+1', 'title': 'Sjedinjene Američke Države', 'type': 'prefix'},
                {'code': 'sk', 'id': 189, 'prefix': '+421', 'title': 'Slovačka', 'type': 'prefix'},
                {'code': 'so', 'id': 190, 'prefix': '+252', 'title': 'Somalija', 'type': 'prefix'},
                {'code': 'cf', 'id': 191, 'prefix': '+236', 'title': 'Srednjoafricka Rep.', 'type': 'prefix'},
                {'code': 'sd', 'id': 192, 'prefix': '+249', 'title': 'Sudan', 'type': 'prefix'},
                {'code': 'sr', 'id': 193, 'prefix': '+597', 'title': 'Surinam', 'type': 'prefix'},
                {'code': 'sz', 'id': 194, 'prefix': '+268', 'title': 'Svazi', 'type': 'prefix'},
                {'code': 'sh', 'id': 195, 'prefix': '+290', 'title': 'Sveta Helena', 'type': 'prefix'},
                {'code': 'lc', 'id': 196, 'prefix': '+1', 'title': 'Sveta Lucija', 'type': 'prefix'},
                {'code': 'kn', 'id': 197, 'prefix': '+1', 'title': 'Sveti Kristofer i Nevis', 'type': 'prefix'},
                {'code': 'pm', 'id': 198, 'prefix': '+508', 'title': 'Sveti Pierre i Miquelon', 'type': 'prefix'},
                {'code': 'vc', 'id': 199, 'prefix': '+1', 'title': 'Sveti Vincent i Grenadini', 'type': 'prefix'},
                {'code': 'es', 'id': 200, 'prefix': '+34', 'title': 'Španjolska', 'type': 'prefix'},
                {'code': 'lk', 'id': 201, 'prefix': '+94', 'title': 'Šrilanka', 'type': 'prefix'},
                {'code': 'se', 'id': 202, 'prefix': '+46', 'title': 'Švedska', 'type': 'prefix'},
                {'code': 'ch', 'id': 203, 'prefix': '+41', 'title': 'Švicarska', 'type': 'prefix'},
                {'code': 'tj', 'id': 204, 'prefix': '+992', 'title': 'Tadžikistan', 'type': 'prefix'},
                {'code': 'th', 'id': 205, 'prefix': '+66', 'title': 'Tajland', 'type': 'prefix'},
                {'code': 'tw', 'id': 206, 'prefix': '+886', 'title': 'Tajvan', 'type': 'prefix'},
                {'code': 'tz', 'id': 207, 'prefix': '+255', 'title': 'Tanzanija', 'type': 'prefix'},
                {'code': 'tg', 'id': 208, 'prefix': '+228', 'title': 'Togo', 'type': 'prefix'},
                {'code': 'tk', 'id': 209, 'prefix': '+690', 'title': 'Tokelausko otocje', 'type': 'prefix'},
                {'code': 'to', 'id': 210, 'prefix': '+676', 'title': 'Tonga', 'type': 'prefix'},
                {'code': 'tt', 'id': 211, 'prefix': '+1', 'title': 'Trinidad i Tobago', 'type': 'prefix'},
                {'code': 'tn', 'id': 212, 'prefix': '+216', 'title': 'Tunis', 'type': 'prefix'},
                {'code': 'tm', 'id': 213, 'prefix': '+993', 'title': 'Turkmenistan', 'type': 'prefix'},
                {'code': 'tc', 'id': 214, 'prefix': '+1', 'title': 'Turksi Caicos', 'type': 'prefix'},
                {'code': 'tr', 'id': 215, 'prefix': '+90', 'title': 'Turska', 'type': 'prefix'},
                {'code': 'tv', 'id': 216, 'prefix': '+688', 'title': 'Tuvalu', 'type': 'prefix'},
                {'code': 'ae', 'id': 217, 'prefix': '+971', 'title': 'UAE', 'type': 'prefix'},
                {'code': 'ug', 'id': 218, 'prefix': '+256', 'title': 'Uganda', 'type': 'prefix'},
                {'code': 'ua', 'id': 219, 'prefix': '+380', 'title': 'Ukrajina', 'type': 'prefix'},
                {'code': 'uy', 'id': 220, 'prefix': '+598', 'title': 'Urugvaj', 'type': 'prefix'},
                {'code': 'uz', 'id': 221, 'prefix': '+998', 'title': 'Uzbekistan', 'type': 'prefix'},
                {'code': 'vu', 'id': 222, 'prefix': '+678', 'title': 'Vanuatu', 'type': 'prefix'},
                {'code': 'va', 'id': 223, 'prefix': '+39', 'title': 'Vatikan', 'type': 'prefix'},
                {'code': 'gb', 'id': 224, 'prefix': '+44', 'title': 'Velika Britanija', 'type': 'prefix'},
                {'code': 've', 'id': 225, 'prefix': '+58', 'title': 'Venecuela', 'type': 'prefix'},
                {'code': 'vn', 'id': 226, 'prefix': '+84', 'title': 'Vijetnam', 'type': 'prefix'},
                {'code': 'wf', 'id': 227, 'prefix': '+681', 'title': 'Wallis i Futuna', 'type': 'prefix'},
                {'code': 'zm', 'id': 228, 'prefix': '+260', 'title': 'Zambija', 'type': 'prefix'},
                {'code': 'eh', 'id': 84, 'prefix': '+212', 'title': 'Zapadna Sahara', 'type': 'prefix'},
                {'code': 'ws', 'id': 229, 'prefix': '+685', 'title': 'Zapadna Samoa', 'type': 'prefix'},
                {'code': 'cv', 'id': 230, 'prefix': '+238', 'title': 'Zelenortski otoci', 'type': 'prefix'},
                {'code': 'zw', 'id': 231, 'prefix': '+263', 'title': 'Zimbabve', 'type': 'prefix'}
            ],
            'domesticBtnPrefixTitle': '---',
            'domesticPrefixes'      : [
                {'type': 'prefix', 'prefix': '+38591', 'title': '091','btntext': '(0)91'},
                {'type': 'prefix', 'prefix': '+38592', 'title': '092','btntext': '(0)92'},
                {'type': 'prefix', 'prefix': '+38595', 'title': '095','btntext': '(0)95'},
                {'type': 'prefix', 'prefix': '+38597', 'title': '097','btntext': '(0)97'},
                {'type': 'prefix', 'prefix': '+38598', 'title': '098','btntext': '(0)98'},
                {'type': 'prefix', 'prefix': '+38599', 'title': '099','btntext': '(0)99'},
                {'type': 'prefix', 'prefix': '+3851', 'title': '01','btntext': '(0)1'},
                {'type': 'prefix', 'prefix': '+38520', 'title': '020','btntext': '(0)20'},
                {'type': 'prefix', 'prefix': '+38521', 'title': '021','btntext': '(0)21'},
                {'type': 'prefix', 'prefix': '+38522', 'title': '022','btntext': '(0)22'},
                {'type': 'prefix', 'prefix': '+38523', 'title': '023','btntext': '(0)23'},
                {'type': 'prefix', 'prefix': '+38531', 'title': '031','btntext': '(0)31'},
                {'type': 'prefix', 'prefix': '+38532', 'title': '032','btntext': '(0)32'},
                {'type': 'prefix', 'prefix': '+38533', 'title': '033','btntext': '(0)33'},
                {'type': 'prefix', 'prefix': '+38534', 'title': '034','btntext': '(0)34'},
                {'type': 'prefix', 'prefix': '+38535', 'title': '035','btntext': '(0)35'},
                {'type': 'prefix', 'prefix': '+38540', 'title': '040','btntext': '(0)40'},
                {'type': 'prefix', 'prefix': '+38542', 'title': '042','btntext': '(0)42'},
                {'type': 'prefix', 'prefix': '+38543', 'title': '043','btntext': '(0)43'},
                {'type': 'prefix', 'prefix': '+38544', 'title': '044','btntext': '(0)44'},
                {'type': 'prefix', 'prefix': '+38547', 'title': '047','btntext': '(0)47'},
                {'type': 'prefix', 'prefix': '+38548', 'title': '048','btntext': '(0)48'},
                {'type': 'prefix', 'prefix': '+38549', 'title': '049','btntext': '(0)49'},
                {'type': 'prefix', 'prefix': '+38551', 'title': '051','btntext': '(0)51'},
                {'type': 'prefix', 'prefix': '+38552', 'title': '052','btntext': '(0)52'},
                {'type': 'prefix', 'prefix': '+38553', 'title': '053','btntext': '(0)53'},
                {'type': 'prefix', 'prefix': '+38572', 'title': '072','btntext': '(0)72'},
                {'type': 'prefix', 'prefix': '+385800', 'title': '0800','btntext': '(0)800'}
            ]
        },

        _create: function() {
            var _self = this;
            this.elems = {};
            this.elems.$fullPhoneNumber = this.element;
            this.elems.fullPhoneNumber = this.elems.$fullPhoneNumber.val() + '';
            this.elems.$phoneNumberFormGroupElement = this.elems.$fullPhoneNumber.closest('.form-group');
            this.elems.$fullPhoneNumberWrapper = null;
            this.elems.$fullPhoneNumberWrapperInputGroup = null;
            if ('undefined' !== typeof this.elems.$fullPhoneNumber.data('type')) {
                this.options.type = this.elems.$fullPhoneNumber.data('type');
            }

            var $fullPhoneNumberWrapper = this.elems.$fullPhoneNumber.closest('.' + this.options.wrapper_class);
            if ('undefined' === typeof $fullPhoneNumberWrapper || $fullPhoneNumberWrapper.length == 0) {
                this.elems.$fullPhoneNumber.wrap('<div class="' + this.options.wrapper_class + '">');
                $fullPhoneNumberWrapper = this.elems.$fullPhoneNumber.parent();
            }
            this.elems.$fullPhoneNumberWrapper = $fullPhoneNumberWrapper;

            $fullPhoneNumberWrapperInputGroup = this.elems.$fullPhoneNumberWrapper.find('> .input-group');
            if ('undefined' === typeof $fullPhoneNumberWrapperInputGroup || $fullPhoneNumberWrapperInputGroup.length == 0) {
                this.elems.$fullPhoneNumberWrapper.append(
                    $('<div/>').addClass('input-group')
                );
                $fullPhoneNumberWrapperInputGroup = this.elems.$fullPhoneNumberWrapper.find('> .input-group');

                if ($fullPhoneNumberWrapperInputGroup.find('.countryPrefixSelector').length == 0) {
                    $fullPhoneNumberWrapperInputGroup.append(
                        $('<div/>')
                            .addClass('input-group-btn countryPrefixSelector')
                            .append(
                                $('<button/>')
                                    .addClass('btn btn-default dropdown-toggle')
                                    .attr('type', 'button')
                                    .attr('data-toggle', 'dropdown').data('toogle', 'dropdown')
                                    .attr('aria-haspopup', 'true')
                                    .attr('aria-expanded', 'false')
                                    .html(_self.options.countryBtnPrefixTitle + ' ' + _self.options.carret)
                            )
                            .append(_self._getCountryPrefixesDropdown())
                    );
                }
            }
            this.elems.$fullPhoneNumberWrapperInputGroup = $fullPhoneNumberWrapperInputGroup;

            // setup domestic elements
            this.elems.$fullPhoneNumberWrapperInputGroup.append(
                $('<div/>')
                    .addClass('input-group-btn domesticPrefixSelector domesticInput')
                    .append(
                        $('<button/>')
                            .addClass('btn btn-default dropdown-toggle')
                            .attr('type', 'button')
                            .attr('data-toggle', 'dropdown').data('toogle', 'dropdown')
                            .attr('aria-haspopup', 'true')
                            .attr('aria-expanded', 'false')
                            .html(_self.options.domesticBtnPrefixTitle + ' ' + _self.options.carret)
                    )
                    .append(_self._getDomesticPrefixesDropdown())
            ).append(
                $('<input/>')
                    .addClass('form-control domesticInput')
                    .attr('type', 'text')
                    .attr('id', _self.elems.$fullPhoneNumber.attr('name') + '_domestic_input')
                    .attr('placeholder', 'Pretplatnički broj')
                    .attr('value', '')
            );
            // try to see if we already have a selected domesticPrefix
            var $alreadySelectedDomesticPrefix = this.elems.$fullPhoneNumberWrapperInputGroup.find('.domesticPrefixSelector .dropdown-menu li.active a');
            if ('undefined' !== typeof $alreadySelectedDomesticPrefix && $alreadySelectedDomesticPrefix.length) {
                $alreadySelectedDomesticPrefix.closest('ul').find('li').removeClass('active');
                $alreadySelectedDomesticPrefix.closest('li').addClass('active');
                this.elems.$fullPhoneNumberWrapperInputGroup.find('.domesticPrefixSelector button.dropdown-toggle').html($alreadySelectedDomesticPrefix.data('btntext') + ' ' + this.options.carret);
            }

            var $constructedDomesticInputField = this.elems.$fullPhoneNumberWrapperInputGroup.find('input.form-control.domesticInput');
            if ('undefined' !== typeof $constructedDomesticInputField && $constructedDomesticInputField.length == 1) {
                $constructedDomesticInputField.change(function(){
                    _self._setFullPhoneNumberWithPrefix();
                });
                if ($().mask) {
                    $constructedDomesticInputField.mask('9000000');
                }
            }

            // setup foreign elements
            this.elems.$fullPhoneNumberWrapperInputGroup.append(
                $('<input/>')
                    .addClass('form-control foreignInput')
                    .attr('type', 'text')
                    .attr('id', _self.elems.$fullPhoneNumber.attr('name') + '_foreign_input')
                    .attr('placeholder', 'Pretplatnički broj')
                    .attr('value', '')
            );
            var $constructedForeignInputField = this.elems.$fullPhoneNumberWrapperInputGroup.find('input.form-control.foreignInput');
            if ('undefined' !== typeof $constructedForeignInputField && $constructedForeignInputField.length == 1) {
                $constructedForeignInputField.change(function(){
                    _self._setFullPhoneNumberWithPrefix();
                });
                if ($().mask) {
                    $constructedForeignInputField.mask('0#');
                }
            }

            // initialize selected country
            var reSetSelectedCountryPrefix = true;
            if (this.options.preselectedCountryID && this.elems.fullPhoneNumber) {
                var $possibleCountryItem = this.elems.$phoneNumberFormGroupElement.find('.countryPrefixSelector ul li a[data-id="' + this.options.preselectedCountryID + '"]');
                if ('undefined' !== typeof $possibleCountryItem && $possibleCountryItem.length == 1) {
                    reSetSelectedCountryPrefix = false;
                    this.setSelectedCountryPrefix($possibleCountryItem, false);
                }
            }
            if (reSetSelectedCountryPrefix) {
                this.setSelectedCountryPrefix(null, false);
            }

            // parse and set currently entered phone number
            this._initialSetupPhoneNumber();
        },

        /*
        Returns currently selected prefix based on the current phoneHandler type (domestic/foreign)
         - if type is 'domestic' we return currently selected value of DomesticPrefix dropdown
         - if type if 'foreign' we return currently selected value of CountryPrefix dropdown
         */
        _getSelectedPrefix: function(){
            if (this.elems.$fullPhoneNumberWrapper.hasClass('domestic')) {
                return this._getSelectedDomesticPrefix();
            } else {
                var selectedCountryPrefix = this._getSelectedCountryPrefix();
                if (trimmedSelectedCountryPrefix = $.trim(selectedCountryPrefix)) {
                    return trimmedSelectedCountryPrefix;
                }
            }

            return null;
        },

        /*
        Returns entered phone number
         */
        _getEnteredNumber: function(){
            var $phoneNumberInput = null;

            if (this.elems.$fullPhoneNumberWrapper.hasClass('domestic')) {
                $phoneNumberInput = this.elems.$fullPhoneNumberWrapper.find('input.form-control.domesticInput');
            } else {
                $phoneNumberInput = this.elems.$fullPhoneNumberWrapper.find('input.form-control.foreignInput');
            }
            if ('undefined' !== typeof $phoneNumberInput && $phoneNumberInput && $phoneNumberInput.length) {
                return $.trim($phoneNumberInput.val());
            }

            return '';
        },

        /*
        Returns currently selected CountryPrefix
         */
        _getSelectedCountryPrefix: function() {
            var $selectedCountryPrefix = this.elems.$fullPhoneNumberWrapper.find('.countryPrefixSelector .dropdown-menu li.active a');
            if ('undefined' !== typeof $selectedCountryPrefix && $selectedCountryPrefix.length) {
                if ($selectedCountryPrefix.data('id')) {
                    return $.trim($selectedCountryPrefix.data('prefix'));
                }
            }

            return '';
        },

        /*
        Returns currently selected Domestic Prefix value
         */
        _getSelectedDomesticPrefix: function() {
            var $selectedDomesticPrefix = this.elems.$fullPhoneNumberWrapper.find('.domesticPrefixSelector .dropdown-menu li.active a');
            if ('undefined' !== typeof $selectedDomesticPrefix && $selectedDomesticPrefix.length) {
                return $.trim($selectedDomesticPrefix.data('prefix'));
            }

            return '';
        },

        /*
        Sets Country prefix to one that user selected
         */
        setSelectedCountryPrefix: function($sender, setFullPhoneNumber) {
            if ('undefined' === typeof setFullPhoneNumber) {
                setFullPhoneNumber = true;
            }

            if ('undefined' !== typeof $sender && $sender && $sender.length) {
                $sender.closest('ul').find('li').removeClass('active');
                $sender.closest('li').addClass('active');
                this.elems.$fullPhoneNumberWrapper.find('.countryPrefixSelector button.dropdown-toggle').html($sender.data('prefix'));

                if ($sender.data('id')) {
                    if (this.options.domesticCountryID == $sender.data('id')) {
                        this.elems.$fullPhoneNumberWrapper.removeClass('foreign').addClass('domestic');
                    } else {
                        this.elems.$fullPhoneNumberWrapper.removeClass('domestic').addClass('foreign');
                    }
                } else {
                    this.elems.$fullPhoneNumberWrapper.removeClass('domestic foreign');
                }
                if (setFullPhoneNumber) {
                    this._setFullPhoneNumberWithPrefix();
                }
            } else {
                var $possibleCountryItem = this.elems.$fullPhoneNumberWrapper.find('.countryPrefixSelector ul li a[data-id="0"]');
                if ('undefined' !== typeof $possibleCountryItem && $possibleCountryItem.length == 1) {
                    this.setSelectedCountryPrefix($possibleCountryItem, false);
                } else {
                    this.elems.$fullPhoneNumberWrapper.find('.countryPrefixSelector button.dropdown-toggle').html(this.options.countryBtnPrefixTitle);
                }
            }
        },

        /*
        Sets Domestic Prefix to one that user selected
         - if setFullPhoneNumber is true or not set at all, procedure will try to parse and set phonenumber in intl format
         */
        setSelectedDomesticPrefix: function($sender, setFullPhoneNumber) {
            if ('undefined' === typeof setFullPhoneNumber) {
                setFullPhoneNumber = true;
            }
            if ('undefined' !== typeof $sender && $sender && $sender.length) {
                $sender.closest('ul').find('li').removeClass('active');
                $sender.closest('li').addClass('active');
                this.elems.$fullPhoneNumberWrapper.find('.domesticPrefixSelector button.dropdown-toggle').html($sender.data('btntext') + ' ' + this.options.carret);
                if (setFullPhoneNumber) {
                    this._setFullPhoneNumberWithPrefix();
                }
            } else {
                this.elems.$fullPhoneNumberWrapper.find('.domesticPrefixSelector button.dropdown-toggle').html(this.options.domesticBtnPrefixTitle + ' ' + this.options.carret);
            }
        },

        /*
        Parse and set phonenumber in international format (+xxxxxxxxxxxx)
         */
        _setFullPhoneNumberWithPrefix: function() {
            var fullPhoneNumberWithPrefix = '';
            var enteredNumber = this._getEnteredNumber();
            if (enteredNumber) {
                if (currentlySelectedPrefix = this._getSelectedPrefix()) {
                    fullPhoneNumberWithPrefix = currentlySelectedPrefix + enteredNumber;
                } else {
                    fullPhoneNumberWithPrefix = '';
                    this._clearPhoneNumber();
                }
            }
            this.elems.$fullPhoneNumber.val(fullPhoneNumberWithPrefix);
        },

        /*
        Clears entered phone number
         */
        _clearPhoneNumber: function() {
            var $phoneNumberInput = null;
            if (this.elems.$fullPhoneNumberWrapper.hasClass('domestic')) {
                $phoneNumberInput = this.elems.$fullPhoneNumberWrapper.find('input.form-control.domesticInput');
            } else {
                $phoneNumberInput = this.elems.$fullPhoneNumberWrapper.find('input.form-control.foreignInput');
            }
            $phoneNumberInput.val('');
        },

        /*
        Returns existing Country Prefixes Dropdown or creates a new one (and returns that one)
         */
        _getCountryPrefixesDropdown: function(selectedPrefix) {
            var $countryPrefixesDropdown = this.elems.$fullPhoneNumberWrapper.find('.countryPrefixSelector ul.dropdown-menu');
            if ('undefined' !== typeof $countryPrefixesDropdown && $countryPrefixesDropdown.length == 1) {
                return $countryPrefixesDropdown;
            }
            if ('undefined' === typeof selectedPrefix) {
                selectedPrefix = null;
            }

            $countryPrefixesDropdown = $('<ul/>').addClass('dropdown-menu scrollable-menu');
            var _self = this;
            $(this.options.countryPrefixes).each(function(i, data) {
                switch(data.type) {
                    case 'prefix':
                        var $countryPrefixLI = $('<li/>').append(
                            $('<a/>')
                                .attr('href', '#')
                                .attr('data-id', data.id).data('id', data.id)
                                .attr('data-prefix', data.prefix).data('prefix', data.prefix)
                                .html('<span class="' + (data.code ? 'country-flag ' + data.code : 'fa fa-ban fa-fw') + '"><!--IE--></span> ' + data.title)
                                .on('click.' + _self.options.pluginName, function(event){
                                    event.preventDefault();
                                    var $elem = $(this).closest('.phoneNumberHandler').find('> input.phone-number-handler');
                                    if ('undefined' !== typeof $elem && $elem.length == 1) {
                                        $elem.phoneNumberHandler('setSelectedCountryPrefix', $(this));
                                    }
                                })
                        );
                        if ((selectedPrefix && selectedPrefix == data.prefix || selectedPrefix == data.title) || (('+' == _self.elems.fullPhoneNumber[0] && data.prefix == _self.elems.fullPhoneNumber.substring(0, data.prefix.length)) || ('0' == _self.elems.fullPhoneNumber[0] && data.title == _self.elems.fullPhoneNumber.substring(0, data.title.length)))) {
                            $countryPrefixLI.addClass('active');
                        }

                        $countryPrefixesDropdown.append($countryPrefixLI);
                        break;

                    case 'separator':
                        $countryPrefixesDropdown.append(
                            $('<li/>')
                                .addClass('divider')
                                .attr('role', 'separator')
                        );
                        break;
                }
            });

            return $countryPrefixesDropdown;
        },

        /*
        Returns existing Domestic Prefixes Dropdown or creates a new one (and returns that one)
         */
        _getDomesticPrefixesDropdown: function(selectedPrefix) {
            var $domesticPrefixesDropdown = this.elems.$fullPhoneNumberWrapper.find('.domesticInput ul.dropdown-menu');
            if ('undefined' !== typeof $domesticPrefixesDropdown && $domesticPrefixesDropdown.length == 1) {
                return $domesticPrefixesDropdown;
            }
            if ('undefined' === typeof selectedPrefix) {
                selectedPrefix = null;
            }

            $domesticPrefixesDropdown = $('<ul/>').addClass('dropdown-menu scrollable-menu');
            var _self = this;
            $(this.options.domesticPrefixes).each(function(i, data) {
                switch(data.type) {
                    case 'prefix':
                        var $domesticPrefixLI = $('<li/>').append(
                            $('<a/>')
                                .attr('href', '#')
                                .attr('data-prefix', data.prefix).data('prefix', data.prefix)
                                .attr('data-btntext', data.btntext).data('btntext', data.btntext)
                                .text(data.title)
                                .on('click.' + _self.options.pluginName, function(event){
                                    event.preventDefault();
                                    var $elem = $(this).closest('.phoneNumberHandler').find('> input.phone-number-handler');
                                    if ('undefined' !== typeof $elem && $elem.length == 1) {
                                        $elem.phoneNumberHandler('setSelectedDomesticPrefix', $(this));
                                    }
                                })
                        );
                        if ((selectedPrefix && selectedPrefix == data.prefix || selectedPrefix == data.title) || (('+' == _self.elems.fullPhoneNumber[0] && data.prefix == _self.elems.fullPhoneNumber.substring(0, data.prefix.length)) || ('0' == _self.elems.fullPhoneNumber[0] && data.title == _self.elems.fullPhoneNumber.substring(0, data.title.length)))) {
                            $domesticPrefixLI.addClass('active');
                        }

                        $domesticPrefixesDropdown.append($domesticPrefixLI);
                        break;

                    case 'separator':
                        $domesticPrefixesDropdown.append(
                            $('<li/>')
                                .addClass('divider')
                                .attr('role', 'separator')
                        );
                        break;
                }
            });

            return $domesticPrefixesDropdown
        },

        _initialParsePhoneNumber: function(number) {
            var _self = this;
            var initiallyParsedPhoneNumber = null;
            number = $.trim(number);
            if (number) {
                // first we have to see if we have this.options.preselectedCountryID set and if we do, we'll try to get
                // parsed data from there
                if (this.options.preselectedCountryID) {
                    // find the country in dropdown
                    var $preselectedCountry = this.elems.$fullPhoneNumberWrapper.find('.countryPrefixSelector .dropdown-menu li a[data-id="' + this.options.preselectedCountryID + '"]');
                    if ('undefined' !== typeof $preselectedCountry && $preselectedCountry.length == 1) {
                        // see if country's prefix match number's
                        if ($preselectedCountry.data('prefix') == number.substring(0, $preselectedCountry.data('prefix'))) {
                            // we have a match, so we can now utilize _parsePhoneNumber method to do the parsing
                            if (this.options.preselectedCountryID == this.options.domesticCountryID) {
                                initiallyParsedPhoneNumber = this._parsePhoneNumber('domestic');
                            } else {
                                initiallyParsedPhoneNumber = this._parsePhoneNumber('foreign');
                            }
                        }
                    }
                }

                // if we still don't have a match, we'll have to iterate through all the countries to find one (if it exists!)
                if (null === initiallyParsedPhoneNumber) {
                    // go through all available countries and see if we have a match
                    $(this.options.countryPrefixes).each(function(i, data){
                        if (data.type == 'prefix' && data.prefix == number.substring(0, data.prefix.length)) {
                            // we have a match
                            initiallyParsedPhoneNumber = {
                                'type': (data.id == _self.options.domesticCountryID ? 'domestic' : 'foreign'),
                                'countryCode': data.prefix
                            };
                            if (initiallyParsedPhoneNumber.type == 'domestic') {
                                // try finding the area code
                                var areaCode = null;
                                $(_self.options.domesticPrefixes).each(function(i, data2) {
                                    if (data2.type == 'prefix' && data2.prefix == number.substring(0, data2.prefix.length)) {
                                        // again, we have a match
                                        areaCode = data2.prefix;
                                        return;
                                    }
                                });
                                if (areaCode) {
                                    initiallyParsedPhoneNumber.areaCode = areaCode;
                                }
                                initiallyParsedPhoneNumber.customerNumber = '' + number.slice(initiallyParsedPhoneNumber.areaCode.length);
                                initiallyParsedPhoneNumber.fullNumber = initiallyParsedPhoneNumber.areaCode + initiallyParsedPhoneNumber.customerNumber;
                            } else {
                                initiallyParsedPhoneNumber.customerNumber = '' + number.slice(initiallyParsedPhoneNumber.countryCode.length);
                                initiallyParsedPhoneNumber.fullNumber = initiallyParsedPhoneNumber.countryCode + initiallyParsedPhoneNumber.customerNumber;
                            }

                            return;
                        }
                    });
                }
            }

            return initiallyParsedPhoneNumber;
        },

        /*
        Parse entered phone number and return an object with 'callingCode' and 'customerNumber' properties. In case
        'field' parameter is given, then only that property will be returned.
         */
        _parsePhoneNumber: function(type, field) {
            if ('undefined' === typeof type || null === type) {
                type = this.elems.$fullPhoneNumberWrapper.hasClass('domestic') ? 'domestic' : (this.elems.$fullPhoneNumberWrapper.hasClass('foreign') ? 'foreign' : null);
            }
            if ('undefined' === typeof field) {
                field = null;
            }

            var parsedPhoneNumber = null;
            switch(type) {
                case 'domestic':
                    var selectedDomesticPrefix = this._getSelectedDomesticPrefix();
                    parsedPhoneNumber = {
                        'type': type,
                        'countryCode': this._getSelectedCountryPrefix(),
                        'areaCode': selectedDomesticPrefix
                    };
                    if (selectedDomesticPrefix) {
                        parsedPhoneNumber.customerNumber = '' + this.elems.$fullPhoneNumber.val().slice(selectedDomesticPrefix.length);
                    } else {
                        parsedPhoneNumber.customerNumber = this.elems.$fullPhoneNumber.val();
                    }
                    parsedPhoneNumber.fullNumber = parsedPhoneNumber.areaCode + parsedPhoneNumber.customerNumber;
                    break;

                case 'foreign':
                    parsedPhoneNumber = {
                        'type': type,
                        'countryCode': this._getSelectedCountryPrefix()
                    };
                    parsedPhoneNumber.customerNumber = '' + this.elems.$fullPhoneNumber.val().slice(parsedPhoneNumber.countryCode.length);
                    parsedPhoneNumber.fullNumber = parsedPhoneNumber.countryCode + parsedPhoneNumber.customerNumber;
                    break;
            }
            if (field) {
                if (parsedPhoneNumber && 'undefined' !== typeof parsedPhoneNumber[field]) {
                    return parsedPhoneNumber[field];
                }
                return '';
            }
            return parsedPhoneNumber;
        },

        /*
        Initial setup and parsing of phone number
         */
        _initialSetupPhoneNumber: function() {
            var initiallyParsedPhoneNumberDetails = this._initialParsePhoneNumber(this.elems.fullPhoneNumber);
            if (initiallyParsedPhoneNumberDetails) {
                switch(initiallyParsedPhoneNumberDetails.type) {
                    case 'domestic':
                        // find needed country and set it up
                        var $neededDomesticCountry = this.elems.$fullPhoneNumberWrapper.find('.countryPrefixSelector .dropdown-menu li a[data-prefix="' + initiallyParsedPhoneNumberDetails.countryCode + '"]:eq(0)');
                        if ('undefined' !== typeof $neededDomesticCountry && $neededDomesticCountry.length == 1) {
                            this.setSelectedCountryPrefix($neededDomesticCountry, false);

                            // find needed domestic prefix and set it up
                            var $neededDomesticPrefix = this.elems.$fullPhoneNumberWrapper.find('.domesticPrefixSelector .dropdown-menu li a[data-prefix="' + initiallyParsedPhoneNumberDetails.areaCode + '"]');
                            if ('undefined' !== typeof $neededDomesticPrefix && $neededDomesticPrefix.length == 1) {
                                this.setSelectedDomesticPrefix($neededDomesticPrefix, false);
                            }
                        }
                        $initialPhoneNumberInput = this.elems.$fullPhoneNumberWrapper.find('input.form-control.domesticInput');
                        $initialPhoneNumberInput.val(initiallyParsedPhoneNumberDetails.customerNumber);
                        break;

                    case 'foreign':
                        // find country with .prefix and trigger click event
                        var $neededForeignCountry = this.elems.$fullPhoneNumberWrapper.find('.countryPrefixSelector .dropdown-menu li a[data-prefix="' + initiallyParsedPhoneNumberDetails.countryCode + '"]:eq(0)');
                        if ('undefined' !== typeof $neededForeignCountry && $neededForeignCountry.length == 1) {
                            this.setSelectedCountryPrefix($neededForeignCountry, false);
                        }
                        $initialPhoneNumberInput = this.elems.$fullPhoneNumberWrapper.find('input.form-control.foreignInput');
                        $initialPhoneNumberInput.val(initiallyParsedPhoneNumberDetails.customerNumber);
                        break;
                }
            }
        }
    });

    // filterizeForm
    $.widget('ogl.filterizeForm', {
        options: {
            'sortDropdown': $('#sort'),
            'viewTypeID': 'viewType',
            'refreshBitsButtonText': 'Pretraži!'
        },

        _create: function() {
            var _self = this;
            this.elems = {};
            this.elems.$form = this.element;
            this.elems.$resetBtn = this.elems.$form.find('button[type="reset"]');
            this.elems.$viewType = $('#' + this.options.viewTypeID);
            this.elems.viewTypeDefaultValue = this.elems.$viewType.data('default-value');
            this.elems.$toggleFiltersBtn = this.elems.$form.find('.hidden-filters-more .more-btn');
            this.elems.$filterBits = this.elems.$form.find('.filterBits');

            this.elems.$viewTypeButtons = $('button[data-name="' + this.options.viewTypeID + '"]');
            if (this.elems.$viewTypeButtons.length) {
                this.elems.$viewTypeButtons.click(function() {
                    $(this).closest('form').filterizeForm('changeViewType', $(this).data('value'));
                });
            }

            // Change the reset button style as requested
            this.elems.$form.changeWatcher();
            this.elems.$form.on('change', function(evt){
                if (_self.elems.$form.hasChanges()) {
                    _self.elems.$resetBtn.removeClass('btn-grey btn-reset-inactive').addClass('btn-primary');
                } else {
                    _self.elems.$resetBtn.removeClass('btn-primary').addClass('btn-grey btn-reset-inactive');
                }
            });

            this.options.sortDropdown.on('change', function(){
                // as we don't have any information (except the one in URL's query), this event will loose this info
                // so we need to append it anywhere in the form...
                _self.addCurrentPageParameter();
                _self.elems.$form.trigger('submit');
            });
            if ($().selectpicker) {
                this.options.sortDropdown.selectpicker({
                    style: 'btn btn-default',
                    noneSelectedText: '',
                    liveSearch: false,
                    noneResultsText: '',
                    dropdownAlignRight: 'auto',
                    mobile: OGL.is_mobile_browser,
                    dropupAuto:false
                });
            }

            if (this.elems.$resetBtn.length) {
                this.elems.$resetBtn.click(function(event){
                    event.preventDefault();
                    _self.resetFormFilters();
                });
            }

            if (this.elems.$toggleFiltersBtn.length) {
                this.elems.$toggleFiltersBtn.click(function(){
                    _self.toggleFilters();
                });
            }

            this.elems.$form.on('submit', function(event){
                var preparedQueryString = _self.prepareQueryString();
            });

            // When filterbits update, check if there's anything inside them and set some classes
            // on the container so that we can hide some crap via css.
            jQuery(document.body).on('filterBitsUpdated', function(){
                var $filterBitsContainer = jQuery('#filterBitsContainer');
                var has_bits = ($filterBitsContainer.find('.filterBits span').length > 0);
                if (has_bits) {
                    $filterBitsContainer.addClass('has-bits');
                    $filterBitsContainer.removeClass('no-bits');
                } else {
                    $filterBitsContainer.addClass('no-bits');
                    $filterBitsContainer.removeClass('has-bits');
                }
            });

            this.updateFilterBits();
            this.element.data('f1lt3r1z3d', true);

            this.setFormFieldsCallbacks();
            //this.scrollPageToClassifiedsIfFiltersPreset();

            return this;
        },

        scrollPageToClassifiedsIfFiltersPreset: function() {
            var queryStringParams = url('?');
            if ('undefined' !== typeof queryStringParams && queryStringParams) {
                window.location = window.location + '#classifieds';
            }
        },

        setFormFieldsCallbacks: function() {
            var _self = this;
            this.elems.$form.find(':input[data-default]:not([data-type="slider"])').attr('tabindex', function(i){
                $(this).on('change.filterize', function(){
                    _self.updateFilterBits();
                });
            });

            var sliders = [];
            this.elems.$form.find('[data-default][data-type="slider"]').each(function(i, sliderInput){
                var formFieldName = $(sliderInput).attr('id').split('_');
                formFieldName     = formFieldName.splice(0, (formFieldName.length - 1)).join('_');
                if (-1 === sliders.indexOf(formFieldName)) {
                    sliders.push(formFieldName);
                    var $noUiSlider   = window['$' + formFieldName + '_noUiSlider'];
                    if ('undefined' !== typeof $noUiSlider && $noUiSlider.length == 1) {
                        $noUiSlider.on('set', function(){
                            _self.updateFilterBits();
                        });
                    }
                }
            });
        },

        changeViewType: function(viewType) {
            // Don't even bother if/when the requested viewType is already shown
            // (this can only happen via inspector fiddling or similar, since the
            // server-side implemented disabled attribute on current viewType button)
            var current = this.elems.$viewType.val();
            if (current == viewType) {
                return;
            }
            this.elems.$viewType.val(viewType);
            // as we don't have any information (except the one in URL's query), this event will loose this info
            // so we need to append it anywhere in the form...
            this.addCurrentPageParameter();
            var current_action = this.elems.$form.attr('action');
            if ('undefined' !== typeof current_action) {
                var anchor = current_action.split('#')[1];
                // If the current action doesn't already have an anchor, append it
                if (!anchor) {
                    //this.elems.$form.attr('action', current_action + '#classifieds');
                }
            }
            this.elems.$form.trigger('submit');
        },

        addFilterBit: function(parameterID, bitName, bitValue) {
            var _self = this;
            if (this.elems.$filterBits.length) {
                var bitNameValue = '<b>' + bitName + '</b>' + (bitValue ? ': ' + bitValue.name : '');
                var $filterBit;
                if (bitValue) {
                    $filterBit = this.elems.$filterBits.find('[data-parameter-id="' + parameterID + '"][data-bit-value="' + bitValue.value + '"]');
                } else {
                    $filterBit = this.elems.$filterBits.find('[data-parameter-id="' + parameterID + '"]');
                }
                if (1 === $filterBit.length) {
                    // update existing
                    $filterBit.find('span.bitName').html(bitNameValue);
                } else {
                    // append new
                    var $newFilterBit = $('<span/>')
                        .addClass('btn tag')
                        .attr('data-parameter-id', parameterID).data('parameter-id', parameterID)
                        .append(
                            $('<span/>')
                                .addClass('bitName')
                                .html(bitNameValue)
                        )
                        .append(
                            $('<span/>')
                                .addClass('fa fa-close remove')
                                .click(function(){
                                    _self.removeFilterBit($(this));
                                })
                        );
                    if (bitValue) {
                        $newFilterBit.attr('data-bit-value', bitValue.value).data('bit-value', bitValue.value);
                    }
                    this.elems.$filterBits.append($newFilterBit);
                }
            }
        },

        addFilterRefreshBit: function() {
            if (this.elems.$filterBits.length) {
                var _self = this;
                var $filterBit = this.elems.$filterBits.find('[data-type="refresh"]');
                if (!$filterBit.length) {
                    this.elems.$filterBits.append(
                        $('<span/>')
                            .addClass('btn tag apply text-bold')
                            .attr('data-type', 'refresh').data('type', 'refresh')
                            .text(this.options.refreshBitsButtonText)
                            .click(function(){
                                _self.elems.$form.trigger('submit');
                            })
                    );
                }
            }
        },

        removeFilterBit: function($sender) {
            var $senderTagBtn = $sender.closest('.btn.tag');
            var $formField = this.elems.$form.find('#' + $senderTagBtn.data('parameter-id'));
            var _self = this;
            $senderTagBtn.fadeOut('fast', function(){
                if ($formField.length) {
                    if ($formField.data('type') !== 'slider') {
                        if ($formField.prop('multiple')) {
                            _self.unsetFormFieldValue($formField, $senderTagBtn.data('bit-value'));
                        } else {
                            _self.setFormFieldDefaultValue($formField);
                        }
                    } else {
                        _self.setFormSliderFieldDefaultValue($formField);
                    }
                    // Refresh global state of filters here just in case, since some
                    // ui elements/plugins/widgets don't propagate all their changes properly yet...
                    _self.updateFilterBits();
                }
                $senderTagBtn.remove();
                // Trigger 'change' event on the entire form so hopefully everything updates
                _self.elems.$form.trigger('change');
            });
        },

        updateFilterBits: function() {
            if (this.elems.$filterBits.length) {
                this.elems.$filterBits.find('.btn.tag').remove();
                var _self = this;
                this.elems.$form.find(':input[data-bit-name]').each(function(){
                    var $field = $(this);
                    var fieldParameterID = $field.attr('id');
                    var fieldDefaultValue = $field.data('default');
                    var fieldType = this.type || this.tagName.toLowerCase();
                    var bitName = $field.data('bit-name');
                    var bitValues = [];
                    switch (fieldType) {
                        case 'text':
                        case 'hidden':
                            var bitValue = $.trim($field.val());
                            if (bitValue != fieldDefaultValue) {
                                bitValues.push({
                                    'name' : bitValue,
                                    'value' : bitValue
                                });
                            }
                            break;
                        case 'checkbox':
                            if ($field.prop('checked')) {
                                var bitValue = true;
                                if ('undefined' !== typeof $field.data('bit-value')) {
                                    bitValue = $field.data('bit-value');
                                }
                                bitValues.push({
                                    'name' : bitValue,
                                    'value' : bitValue
                                });
                            }
                            break;
                        case 'radio':
                            if ($field.prop('checked')) {
                                var bitValue = true;
                                if ('undefined' !== typeof $field.data('bit-value')) {
                                    bitValue = $field.data('bit-value');
                                }
                                bitValues.push({
                                    'name' : bitValue,
                                    'value' : bitValue
                                });
                            }
                            break;
                        case 'select-one':
                            var bitValue = $field.find(':selected').text();
                            var bitCustomTitle = 'undefined' !== typeof $field.attr('title') ? $.trim($field.attr('title')) : '';
                            if (bitValue != fieldDefaultValue && bitValue !== bitName && bitValue !== bitCustomTitle) {
                                bitValues.push({
                                    'name' : bitValue,
                                    'value' : bitValue
                                });
                            }
                            break;
                        case 'select-multiple':
                            $field.find(':selected').each(function(){
                                var currBitName = $(this).text();
                                var currBitValue = $(this).val();
                                var bitCustomTitle = 'undefined' !== typeof $field.attr('title') ? $.trim($field.attr('title')) : '';
                                if (currBitValue != fieldDefaultValue && currBitValue !== bitCustomTitle) {
                                    bitValues.push({
                                        'name' : currBitName,
                                        'value' : currBitValue
                                    });
                                }
                            });
                            break;
                    }
                    if (bitValues.length) {
                        $(bitValues).each(function(i, bitValue){
                            if (true === bitValue.value) {
                                bitValue = null;
                            }
                            _self.addFilterBit(fieldParameterID, bitName, bitValue);
                        });
                    }
                });
                if ('undefined' === typeof this.elems.initialBits) {
                    this.elems.initialBits = this.elems.$filterBits.html();
                }

                if (this.elems.initialBits !== this.elems.$filterBits.html()) {
                    // add refresh filter button
                    _self.addFilterRefreshBit();
                }

                if (this.elems.$filterBits.find('.btn.tag').length) {
                    this.elems.$filterBits.removeClass('hidden');
                } else {
                    this.elems.$filterBits.addClass('hidden');
                }
            }
            jQuery(document.body).trigger('filterBitsUpdated');
        },

        disableDefaultSort: function() {
            var sortDefaultValue = this.options.sortDropdown.data('default');
            if ('undefined' !== typeof sortDefaultValue && sortDefaultValue === this.options.sortDropdown.val()) {
                this.options.sortDropdown.prop('disabled', true);
            }
        },

        disableDefaultViewType: function() {
            if ('undefined' !== typeof this.elems.viewTypeDefaultValue && this.elems.viewTypeDefaultValue === this.elems.$viewType.val()) {
                this.elems.$viewType.prop('disabled', true);
            }
        },

        addCurrentPageParameter: function() {
            var currentPageFromURLQueryString = url('?page');
            if ('undefined' !== typeof currentPageFromURLQueryString) {
                this.elems.$form.append(
                    $('<input/>')
                        .attr('type', 'hidden')
                        .attr('name', 'page')
                        .val(currentPageFromURLQueryString)
                );
            }
        },

        toggleFilters: function() {
            var $hidden_filters = this.elems.$form.find('.hidden-filters');
            var $show_hide_btn = this.elems.$form.find('.hidden-filters-more .more-btn');
            if ($hidden_filters.is(':visible')) {
                $hidden_filters.slideUp(function(){
                    $show_hide_btn.html('Prikaži više filtera <span class="fa fa-chevron-down fa-fw"></span>');
                    $(document.body).trigger('sticky_kit:recalc');
                });
            } else {
                $hidden_filters.slideDown(function(){
                    $show_hide_btn.html('Prikaži manje filtera <span class="fa fa-chevron-up fa-fw"></span>');
                    $(document.body).trigger('sticky_kit:recalc');
                });
            }
        },

        prepareQueryString: function() {
            this.disableDefaultSort();
            this.disableDefaultViewType();
            // Disable all inputs that don't have a value set so they don't get submitted
            this.elems.$form.find(':input').not('button').filter(function(){ return $.trim($(this).val()) == ''; }).prop('disabled', true);

            var fullQueryString    = $.trim(this.elems.$form.serialize());
            var cleanedQueryString = $.trim(fullQueryString.replace(/[^&]+=\.?(?:&|$)/g, ''));
            // Removing default filter values in order to reduce url length
            var queryStringArray     = cleanedQueryString.split('&');
            var finalQueryStringJson = {};
            for (var i = 0, il = queryStringArray.length; i < il; i++) {
                var queryStringSegment = $.trim(queryStringArray[i]);
                if (queryStringSegment) {
                    var filterParts = queryStringSegment.split('=');
                    var filterName  = $.trim(filterParts[0]);
                    var filterValue = $.trim(filterParts[1]);
                    if (OGL.isInt(filterValue)) {
                        filterValue = parseInt(filterValue);
                    } else if (OGL.isFloat(filterValue)) {
                        filterValue = parseFloat(filterValue);
                    }

                    var $formFilter = this.elems.$form.find('[name="' + filterName + '"]');
                    if ($formFilter.length) {
                        if (filterValue) {
                            var pushFilter             = true;
                            var formFilterDefaultValue = $formFilter.data('default');

                            if ('undefined' !== typeof formFilterDefaultValue && $.trim(formFilterDefaultValue) == filterValue) {
                                pushFilter = false;
                            }

                            if (pushFilter) {
                                finalQueryStringJson[filterName] = filterValue;
                            } else {
                                // disable the filter so it doesn't even get submited
                                $formFilter.prop('disabled', true);
                            }
                        }
                    }
                }
            }

            if (!$.isEmptyObject(finalQueryStringJson)) {
                var finalQueryString = Object.keys(finalQueryStringJson).map(function(k) { return k + '=' + finalQueryStringJson[k] }).join('&');
                if (finalQueryString.length > 1000) {
                    this.elems.$form.attr('method', 'post');
                }
            }
            return finalQueryString;
        },

        setFormFieldDefaultValue: function($formField) {
            switch ($formField.data('type')) {
                case 'text':
                    $formField.val($formField.data('default'));
                    $formField.prop('disabled', false);
                    break;
                case 'number':
                    var $autoNumericField = window['$' + $formField.attr('id') + '_autoNumeric'];
                    if ('undefined' !== typeof $autoNumericField && $autoNumericField.length == 1) {
                        $autoNumericField.autoNumeric('set', $formField.data('default'));
                        $autoNumericField.prop('disabled', false);
                    } else {
                        $formField.prop('disabled', false);
                        if ($formField.data('autoNumeric')) {
                            $formField.autoNumeric('set', $formField.data('default'));
                        } else {
                            $formField.val($formField.data('default'));
                        }
                    }
                    break;
                case 'select':
                    $formField.prop('disabled', false);
                    $formField.find('option').prop('selected', false);
                    $formField.find('option[value="' + $formField.data('default') + '"]').prop('selected', true);
                    $formField.change();
                    if ($().selectpicker) {
                        $formField.selectpicker('refresh');
                    }
                    break;
                case 'checkbox':
                case 'radio':
                    $formField.prop('disabled', false);
                    $formField.prop('checked', $formField.data('default'));
                    break;
            }
        },

        unsetFormFieldValue: function($formField, value) {
            $formField.find('option[value='+value+']').prop('selected', false);
            $formField.change();
            if ($().selectpicker) {
                $formField.selectpicker('refresh');
            }
        },

        setFormSliderFieldDefaultValue: function($formField) {
            var formFieldName = $formField.attr('id').split('_');
            var formFieldSubName = formFieldName[(formFieldName.length - 1)];
            formFieldName = formFieldName.splice(0, (formFieldName.length - 1)).join('_');
            var $noUiSlider   = window['$' + formFieldName + '_noUiSlider'];
            if ('undefined' !== typeof $noUiSlider && $noUiSlider.length == 1) {
                var newSliderDefaults = window[formFieldName + '_options'];
                if ('from' == formFieldSubName) {
                    newSliderDefaults.start[0] = newSliderDefaults.range.min;
                    newSliderDefaults.start[1] = parseInt($('#' + formFieldName + '_to').val());
                } else if ('to' == formFieldSubName) {
                    newSliderDefaults.start[0] = parseInt($('#' + formFieldName + '_from').val());
                    newSliderDefaults.start[1] = newSliderDefaults.range.max;
                }
                $noUiSlider.val(newSliderDefaults.start);
            }
        },

        resetFormFilters: function() {
            var _self = this;
            // reset form input data
            this.elems.$form.find(':input[data-default]:not([data-type="slider"])').each(function(i, elem){
                _self.setFormFieldDefaultValue($(elem));
            });

            // reset sliders (if they exist)
            this.elems.$form.find('.noUi-slider-box[data-defaults]').each(function(i){
                var $currSlider = $(this).find('> div.noUi-target');
                var $noUiSlider = window['$' + $currSlider.attr('id') + '_noUiSlider'];
                if ('undefined' !== typeof $noUiSlider && $noUiSlider.length == 1) {
                    $noUiSlider.val($(this).data('defaults'));
                }
            });
            this.updateFilterBits();
        }
    });


    // socialShare
    $.widget('ogl.socialShare', {
        options: {
            'pluginName': 'socialShare',
            'instanceName': 's0c1al1z3d',
            'classes': {
                'button': 'social-btn',
                'facebook': 'facebook',
                'twitter': 'twitter',
                'googleplus': 'googleplus',
                'pinterest': 'pinterest',
                'linkedin': 'linkedin'
            },
            'service': null,
            'services': {
                'useNativeIfAvailabe': false,
                'usePopup': true,
                'facebook': {
                    'url': 'https://www.facebook.com/sharer/sharer.php?u={url}',
                    'width': 600,
                    'height': 500,
                    'data': ['url']
                },
                'twitter': {
                    'url': 'https://twitter.com/intent/tweet?url={url}&text={title}',
                    'width': 600,
                    'height': 450,
                    'data': ['url', 'title']
                },
                'googleplus': {
                    'url': 'https://plus.google.com/share?url={url}',
                    'width': 500,
                    'height': 500,
                    'data': ['url']
                },
                'pinterest': {
                    'url': 'https://www.pinterest.com/pin/create/button/?url={url}&description={title}',
                    'width': 750,
                    'height': 550,
                    'data': ['url', 'title']
                },
                'linkedin': {
                    'url': 'https://www.linkedin.com/shareArticle?url={url}',
                    'width': 600,
                    'height': 500,
                    'data': ['url']
                },
                'email': {
                    'url': "mailto:{recipients}?subject=Zanimljiv oglas na Oglasnik.hr&body=Bok, ovaj mi se oglas čini zanimljivim:%0D%0A %0D%0A {title}%0D%0A {url}",
                    'data': ['recipients', 'title', 'url'],
                    'modal': 'modal-email-share'
                }
            }
        },

        _create: function() {
            var _self   = this;
            this.elems = {
                socialButtons: []
            };
            if (($buttons = $(this.element).find('.' + this.options.classes.button)).length > 0) {
                // we're dealing with container with multiple buttons
                $buttons.each(function(i, button){
                    _self.elems.socialButtons.push($(button));
                });
            } else {
                // we're dealing with single button
                this.elems.socialButtons.push($(this.element));
                this.service = this.options.service;
            }
            if (this.elems.socialButtons.length) {
                $(this.elems.socialButtons).each(function(i, btn){
                    var $btn = jQuery(btn);
                    var btnHref = null;
                    if (_self._isService($btn, 'facebook')) {
                        if (_self.options.services.useNativeIfAvailabe && 'undefined' !== typeof window.fbAsyncInit || 'undefined' !== typeof FB) {
                            $btn.on('click.' + _self.options.pluginName, function(event){
                                event.preventDefault();
                                FB.ui({
                                    method: 'share',
                                    href: $(location).attr('href')
                                }, function(response){});
                            });
                        } else {
                            if (_self.options.services.usePopup) {
                                $btn.on('click.' + _self.options.pluginName, function(event) {
                                    event.preventDefault();
                                    if ('undefined' !== typeof $(this).data(_self.options.instanceName)) {
                                        $(this).socialShare('popupShare', 'facebook');
                                    } else {
                                        var $socialShareParent = $(this).closest('[data-' + _self.options.instanceName + ']');
                                        if ('undefined' !== typeof $socialShareParent && $socialShareParent.length == 1) {
                                            $socialShareParent.socialShare('popupShare', 'facebook');
                                        }
                                    }
                                });
                            } else {
                                $btn.attr('href', _self._getServiceURL('facebook')).attr('target', '_blank');
                            }
                        }
                    } else if (_self._isService($btn, 'twitter')) {
                        if (_self.options.services.usePopup) {
                            $btn.on('click.' + _self.options.pluginName, function(event) {
                                event.preventDefault();
                                if ('undefined' !== typeof $(this).data(_self.options.instanceName)) {
                                    $(this).socialShare('popupShare', 'twitter');
                                } else {
                                    var $socialShareParent = $(this).closest('[data-' + _self.options.instanceName + ']');
                                    if ('undefined' !== typeof $socialShareParent && $socialShareParent.length == 1) {
                                        $socialShareParent.socialShare('popupShare', 'twitter');
                                    }
                                }
                            });
                        } else {
                            $btn.attr('href', _self._getServiceURL('twitter')).attr('target', '_blank');
                        }
                    } else if (_self._isService($btn, 'googleplus')) {
                        if (_self.options.services.usePopup) {
                            $btn.on('click.' + _self.options.pluginName, function(event) {
                                event.preventDefault();
                                if ('undefined' !== typeof $(this).data(_self.options.instanceName)) {
                                    $(this).socialShare('popupShare', 'googleplus');
                                } else {
                                    var $socialShareParent = $(this).closest('[data-' + _self.options.instanceName + ']');
                                    if ('undefined' !== typeof $socialShareParent && $socialShareParent.length == 1) {
                                        $socialShareParent.socialShare('popupShare', 'googleplus');
                                    }
                                }
                            });
                        } else {
                            $btn.attr('href', _self._getServiceURL('googleplus')).attr('target', '_blank');
                        }
                    } else if (_self._isService($btn, 'linkedin')) {
                        if (_self.options.services.usePopup) {
                            $btn.on('click.' + _self.options.pluginName, function(event) {
                                event.preventDefault();
                                if ('undefined' !== typeof $(this).data(_self.options.instanceName)) {
                                    $(this).socialShare('popupShare', 'linkedin');
                                } else {
                                    var $socialShareParent = $(this).closest('[data-' + _self.options.instanceName + ']');
                                    if ('undefined' !== typeof $socialShareParent && $socialShareParent.length == 1) {
                                        $socialShareParent.socialShare('popupShare', 'linkedin');
                                    }
                                }
                            });
                        } else {
                            $btn.attr('href', _self._getServiceURL('linkedin')).attr('target', '_blank');
                        }
                    } else if (_self._isService($btn, 'pinterest')) {
                        if (_self.options.services.usePopup) {
                            $btn.on('click.' + _self.options.pluginName, function(event) {
                                event.preventDefault();
                                if ('undefined' !== typeof $(this).data(_self.options.instanceName)) {
                                    $(this).socialShare('popupShare', 'pinterest');
                                } else {
                                    var $socialShareParent = $(this).closest('[data-' + _self.options.instanceName + ']');
                                    if ('undefined' !== typeof $socialShareParent && $socialShareParent.length == 1) {
                                        $socialShareParent.socialShare('popupShare', 'pinterest');
                                    }
                                }
                            });
                        } else {
                            $btn.attr('href', _self._getServiceURL('pinterest')).attr('target', '_blank');
                        }
                    } else if (_self._isService($btn, 'email')) {
                        // share to email
                        if (_self.options.services.usePopup) {
                            // see if we have a modal already on the page
                            _self._ajaxifyEmailShareModalForm();
                            $btn.on('click.' + _self.options.pluginName, function(event) {
                                event.preventDefault();
                                if ('undefined' !== typeof $(this).data(_self.options.instanceName)) {
                                    $(this).socialShare('emailModalShare');
                                } else {
                                    var $socialShareParent = $(this).closest('[data-' + _self.options.instanceName + ']');
                                    if ('undefined' !== typeof $socialShareParent && $socialShareParent.length == 1) {
                                        $socialShareParent.socialShare('emailModalShare');
                                    }
                                }
                            });
                        } else {
                            $btn.attr('href', _self._getServiceURL('email')).attr('target', '_blank');
                        }
                    }
                });
            }
            this.element.attr('data-' + this.options.instanceName, true).data(this.options.instanceName, true);
            return this;
        },

        _validateEmailModal: function() {
            var modalID   = this.options.services.email.modal;
            $('#' + modalID + ' .has-error').removeClass('has-error');
            var tmpRecipients = $.trim($('#' + modalID + '-recipients').val()).split(/\n/);
            var recipients = [];
            if (tmpRecipients.length) {
                $.each(tmpRecipients, function(i, recipient){
                    if ($.trim(recipient)) {
                        recipients.push($.trim(recipient));
                    }
                });
            }
            var isValid = true;

            if (recipients.length == 0) {
                isValid = false;
                $('#' + modalID + '-recipients').closest('.form-group').addClass('has-error');
            }

            return {
                'valid': isValid,
                'url': this._getCurrentURL(),
                'title': this._getCurrentTitle(),
                'recipients': recipients
            };
        },

        sendEmailFromModal: function() {
            var validation = this._validateEmailModal();
            if (validation.valid) {
                $('#' + this.options.services.email.modal + '-recipients').val('');
                $('#' + this.options.services.email.modal).modal('hide');
                window.location.href = this._generateServiceURLFromData(
                    this.options.services.email.url,
                    validation,
                    false
                );
            }
        },

        _constuctEmailShareModal: function(modalID){
            var _self = this;
            var $modal = $('<div/>')
                .addClass('modal fade ' + modalID)
                .attr('id', modalID)
                .attr('tabindex', '-1')
                .attr('role', 'dialog')
                .attr('aria-labelledby', modalID)
                .append(
                    $('<div/>')
                        .addClass('modal-dialog')
                        .attr('role', 'document')
                        .append(
                            $('<div/>')
                                .addClass('modal-content')
                                .append(
                                    $('<div/>')
                                        .addClass('modal-header')
                                        .append(
                                            $('<button/>')
                                                .addClass('close')
                                                .attr('type', 'button')
                                                .attr('data-dismiss', 'modal').data('dismiss', 'modal')
                                                .attr('arial-label', 'Zatvori')
                                                .append(
                                                    $('<span/>')
                                                        .attr('aria-hidden', 'true')
                                                        .html('&times;')
                                                )
                                        )
                                        .append(
                                            $('<h4/>')
                                                .addClass('modal-title')
                                                .attr('id', modalID + '-title')
                                                .text('Podijeli putem emaila')
                                        )
                                )
                                .append(
                                    $('<div/>')
                                        .addClass('alert text-center hidden')
                                )
                                .append(
                                    $('<div/>')
                                        .addClass('modal-body')
                                        .append(
                                            $('<div/>')
                                                .addClass('form-group')
                                                .append(
                                                    $('<label/>')
                                                        .addClass('control-label')
                                                        .attr('for', modalID + '-recipients')
                                                        .append(
                                                            document.createTextNode('Primatelji')
                                                        )
                                                        .append(
                                                            $('<abbr/>')
                                                                .attr('title', 'Obavezno polje')
                                                        )
                                                )
                                                .append(
                                                    $('<textarea/>')
                                                        .addClass('form-control')
                                                        .attr('type', 'text')
                                                        .attr('id', modalID + '-recipients')
                                                        .attr('rows', '8')
                                                        .val('')
                                                )
                                                .append(
                                                    $('<p/>')
                                                        .addClass('help-block text-small')
                                                        .text('Možete dodati više email adresa (svaka email adresa mora biti u novom redu!)')
                                                )
                                        )
                                )
                                .append(
                                    $('<div/>')
                                        .addClass('modal-footer')
                                        .append(
                                            $('<button/>')
                                                .addClass('btn btn-primary width-full modal-btn-send text-center light-blue')
                                                .attr('type', 'submit')
                                                .text('Pošalji')
                                                .click(function(event){
                                                    event.preventDefault();
                                                    $(_self.element).socialShare('sendEmailFromModal');
                                                })
                                        )
                                )
                        )
                );

            return $modal;
        },

        // TODO: prevent multiple click events on share button...

        _getEmailShareModal: function() {
            var modalID = this.options.services.email.modal;
            var $modal  = $(document).find('#' + modalID);
            if ('undefined' === typeof $modal || $modal.length === 0) {
                $modal = this._constuctEmailShareModal(modalID);
                $(document).find('body').append($modal);
            }
            return $modal;
        },

        _ajaxifyEmailShareModalForm: function() {
            var $modal = this._getEmailShareModal();
            $modal.find('form').ajaxifyModalForm();
        },

        emailModalShare: function() {
            var $modal = this._getEmailShareModal();
            $modal.modal('show');
        },

        _isService: function($btn, service) {
            if (this.service) {
                return this.service == service;
            } else {
                return $btn.hasClass(this.options.classes[service])
            }
        },

        _getCurrentURL: function() {
            return $(location).attr('href');
        },

        _getCurrentTitle: function() {
            return $(document).find('title').text();
        },

        _getServiceData: function(service) {
            var serviceData = {};
            if ('undefined' !== typeof this.options.services[service].data) {
                _self = this;
                $(this.options.services[service].data).each(function(i, k){
                    switch(k) {
                        case 'url':
                            serviceData.url = _self._getCurrentURL();
                            break;
                        case 'title':
                            serviceData.title = _self._getCurrentTitle();
                            break;
                    }
                });
            } else {
                serviceData.url = this._getCurrentURL();
            }
            return serviceData;
        },

        _encodeValue: function(val) {
            if ('undefined' !== typeof val && '' !== $.trim(val)) {
                return encodeURIComponent($.trim(val));
            }
            return '';
        },

        _generateServiceURLFromData: function(serviceURL, serviceData, encodeURI) {
            var serviceURL = serviceURL;
            if ('undefined' !== typeof serviceData) {
                _self = this;
                $.each(serviceData, function(k, v){
                    serviceURL = serviceURL.replace('{' + k + '}', (encodeURI ? _self._encodeValue(v) : v));
                });
            }
            return serviceURL;
        },

        _openPopup: function(url, options, service) {
            var left = Math.round(screen.width / 2 - options.width / 2),
                top = 0;
            screen.height > options.height && (top = Math.round(screen.height / 3 - options.height / 2));
            var win = window.open(url, this.options.pluginName + '_' + service, "left=" + left + ",top=" + top + ",width=" + options.width + ",height=" + options.height + ",personalbar=0,toolbar=0,scrollbars=1,resizable=1");
            if (win) {
                win.focus();
            } else {
                location.href = url;
            }
        },

        _getServiceURL: function(service, encodeURI) {
            if ('undefined' === typeof encodeURI) {
                encodeURI = true;
            }
            return this._generateServiceURLFromData(
                this.options.services[service].url,
                this._getServiceData(service),
                encodeURI
            );
        },

        popupShare: function(service) {
            if ('undefined' !== typeof this.options.services[service]) {
                this._openPopup(this._getServiceURL(service), this.options.services[service], service);
            }
        }
    });

    // tabbifyClassifiedsDetails
    $.widget('ogl.tabbifyClassifiedsDetails', {
        options: {
            instanceName : 'tabb1f13d',
            closedCount: 3,
            mobileBreakPoint: 767,
            selectors : {
                header : '.convert-to-tab-xs',
                body : '.col-sm-10',
                bodyP : '> p'
            }
        },

        _create: function() {
            var _self = this;

            this.elems = {
                header : $(this.element).find(this.options.selectors.header),
                body : $(this.element).find(this.options.selectors.body),
                state : 'open'
            };

            if ('h3' === this.elems.header.prop('tagName').toLowerCase()) {
                this.elems.body = $(this.element).find(this.options.selectors.bodyP);
            }

            var state = this._getStateBasedOnBodyRows();
            _self = this;
            $(this.element).addClass(state);
            if ('open' === state) {
                $(this.element).find('.caret').addClass('active');
            }
            this.elems.header.click(function(){
                $(_self.element).tabbifyClassifiedsDetails('changeState');
            });

            this.element.attr('data-' + this.options.instanceName, true).data(this.options.instanceName, true);
            return this;
        },

        _getBodyRows: function() {
            var bodyHeight = this.elems.body.outerHeight();
            var lineHeight = parseInt(this.elems.body.css('line-height'), 10);
            return bodyHeight / lineHeight;
        },

        _getStateBasedOnBodyRows: function() {
            if (this.elems.header.hasClass('initial-open')) {
                this.elems.header.removeClass('initial-open');
                return 'open';
            }
            var bodyRows = this._getBodyRows();
            var state = 'open';
            if (bodyRows > this.options.closedCount) {
                state = 'closed';
            }

            return state;
        },

        changeState: function() {
            if (this.options.mobileBreakPoint >= $(window).width()) {
                if ($(this.element).hasClass('closed')) {
                    $(this.element).removeClass('closed').addClass('open');
                    $(this.element).find('.caret').addClass('active');
                } else {
                    $(this.element).removeClass('open').addClass('closed');
                    $(this.element).find('.caret').removeClass('active');
                }
            }
        }
    });

    // genericCarousel
    $.widget('ogl.genericCarousel', {
        options: {
            selectors: {
                slider: '.slider',
                slide: '.slide',
                pager: '.pager',
                pageNumber: '.number'
            },
            sliderOptions: {
                autoplay: true,
                autoplaySpeed: 5000,
                arrows: false,
                dots: false,
                slide: '.slide',
                slidesToShow: 4,
                slidesToScroll: 4,
                adaptiveHeight: false
            }
        },

        _create: function() {
            var _self = this;
            this.elems = {
                container : $(this.element).find(this.options.selectors.slider),
                slides    : $(this.element).find(this.options.selectors.slider).find(this.options.sliderOptions.slide),
                paginator : null
            };
            var $pager = $(this.element).find(this.options.selectors.pager);
            if ($pager.length > 0) {
                this.elems.paginator = {
                    pager: $pager,
                    pageNumber: $pager.find(this.options.selectors.pageNumber)
                };
                $pager.find('a[data-dir]').click(function(e) {
                    e.preventDefault();
                    if ('prev' === $(this).data('dir')) {
                        $(_self.element).genericCarousel('prevSlide');
                    } else {
                        $(_self.element).genericCarousel('nextSlide');
                    }
                });
            }

            var sliderOptionsFunctions = {
                onInit: function(e){
                    if (_self.elems.paginator && _self.elems.paginator.pageNumber) {
                        var paginationText = parseInt(e.currentSlide + 1, 10) + ' / ' + e.slideCount;
                        _self.elems.paginator.pageNumber.html(paginationText);
                    }
                },
                onAfterChange: function(e){
                    if (_self.elems.paginator && _self.elems.paginator.pageNumber) {
                        var paginationText = e.currentSlide + 1 + ' / ' + e.slideCount;
                        _self.elems.paginator.pageNumber.html(paginationText);
                    }
                }
            };

            jQuery.extend(sliderOptionsFunctions, this.options.sliderOptions);
            this.elems.container.slick(sliderOptionsFunctions);

            return this;
        },

        nextSlide: function() {
            this.elems.container.slickNext();
        },

        prevSlide: function() {
            this.elems.container.slickPrev();
        },

        pause: function() {
            this.elems.container.slickPause();
        }
    });
}(jQuery));
