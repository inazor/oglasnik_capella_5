var currentLevel = 1;
var currentCategoryID = null;

function initCategorySelector(initialCategoryID) {
    $('.new-ad-steps .category-select a').click(function(event){
        event.preventDefault();
        selectCategoryByClick($(this).parent());
    });

    if (initialCategoryID) {
        selectCategoryByID(initialCategoryID);
    } else {
        showMobileCategorySelectorsLevel();
    }
}

function recalculateAndSetCategorySelectorHeight() {
    // get all visible ul's and get the height of the most tallest
    var maxHeight = 0;
    if ($('.new-ad-steps .mobile-category-selector-breadcrumbs').is(':visible')) {
        maxHeight = $('.new-ad-steps ul[data-level="' + currentLevel + '"]:visible').outerHeight();
    } else {
        $('ul.category-select:visible').each(function(i, ul){
            if ($(ul).outerHeight() > maxHeight) {
                maxHeight = $(ul).outerHeight();
            }
        });
    }
    $('.new-ad-steps .mobile-overflow-hidden').height(maxHeight);
}

function getCategoryParents(categoryID, includeSelf) {
    var categoryParents = [];
    if ('undefined' === typeof includeSelf) {
        includeSelf = false;
    }
    if (categoryID) {
        var $category = $('.new-ad-steps li[data-category-id="' + categoryID + '"]');
        if ($category.length) {
            if (includeSelf) {
                categoryParents.push($category);
            }
            var $categoryParents = $category.parents('li[data-category-id]');
            if ($categoryParents.length) {
                $categoryParents.each(function(){
                    categoryParents.push($(this));
                })
            }
        }
    }
    if (categoryParents.length) {
        categoryParents.reverse();
    }

    return categoryParents;
}

function updateMobileBreadCrumbs() {
    var $bcContainer = $('.mobile-category-selector-breadcrumbs .bc-container');
    if ($bcContainer.length) {
        var currentCategoryParents = getCategoryParents(currentCategoryID, true);

        var $category_id = $('#category_id');
        var $submitContainer = $('#submit-button-container');

        $bcContainer.html('');
        $bcContainer.append(
            $('<span/>')
                .text('Sve kategorije')
                .click(function(){
                    currentLevel = 1;
                    currentCategoryID = null;
                    $('.new-ad-steps li.active').removeClass('active');
                    $category_id.val('');
                    $submitContainer.hide();
                    showMobileCategorySelectorsLevel();
                    hideUnusedLevels(true);
                })
        );
        if (currentCategoryParents.length) {
            $(currentCategoryParents).each(function(){
                var $parentLI = $(this);

                $bcContainer.append(
                    $('<span/>')
                        .text($parentLI.data('name'))
                        .click(function(){
                            $category_id.val('');
                            $submitContainer.hide();
                            selectCategoryByID(parseInt($parentLI.data('category-id')));
                        })
                );
            });
        }
    }
}

function selectCategoryByID(categoryID) {
    currentCategoryID = categoryID;
    var $selectedCategory = $('.new-ad-steps li[data-category-id="' + categoryID + '"]');
    if ($selectedCategory.length) {
        currentLevel = parseInt($selectedCategory.parent().data('level'));
        hideUnusedLevels();
        var enabledCategories = [];
        enabledCategories.push($selectedCategory);
        var $selectedCategoryParents = $selectedCategory.parents('li[data-category-id]');
        if ($selectedCategoryParents.length) {
            $selectedCategoryParents.each(function(){
                enabledCategories.push($(this));
            })
        }
        $(enabledCategories).each(function(){
            $(this).addClass('active').find('> ul').show();
        });
        showMobileCategorySelectorsLevel();
        recalculateAndSetCategorySelectorHeight();
    }
}

function hideUnusedLevels(skipLevelBump) {
    // Default to false
    skipLevelBump = skipLevelBump || false;

    var $levelsToClear = null;
    if (currentLevel == 1) {
        $levelsToClear = $('ul.category-level-2, ul.category-level-3, ul.category-level-4');
    } else if (currentLevel == 2) {
        $levelsToClear = $('ul.category-level-3, ul.category-level-4');
    } else if (currentLevel == 3) {
        $levelsToClear = $('ul.category-level-4');
    }
    if ($levelsToClear) {
        $levelsToClear.hide();
        $levelsToClear.find('.active').removeClass('active');
    }
    if (!skipLevelBump) {
        currentLevel++;
    }
}

function selectCategoryByClick($sender) {
    currentCategoryID = parseInt($sender.data('category-id'));
    var sendersLevel = parseInt($sender.parent().data('level'));
    $sender.parent().children().removeClass('active');
    $sender.addClass('active');
    currentLevel = sendersLevel;

    // we have to clear all visible levels greater than current one
    hideUnusedLevels();

    var $submitContainer = $('#submit-button-container');
    var $categoryId = $('#category_id');

    if ($sender.data('leaf-node')) {
        $submitContainer.fadeIn('fast', function(){
            $categoryId.val($sender.data('category-id'));
            if (!OGL.isElementInViewport($(this))) {
                $('html, body').animate({
                    scrollTop: ($submitContainer.offset().top - $submitContainer.outerHeight())
                }, 250);
            }
        });
    } else {
        $submitContainer.hide(function(){
            $categoryId.val('');
        });
        var $sendersUL = $sender.find('> ul');
        if ($sendersUL.length) {
            $sendersUL.fadeIn('fast', function(){
                if (!OGL.isElementInViewport($sendersUL)) {
                    $('html, body').animate({
                        scrollTop: $('.choose-category-heading').offset().top
                    }, 250);
                }
                showMobileCategorySelectorsLevel();
            });
        }
    }
}

function showMobileCategorySelectorsLevel() {
    if ($('.new-ad-steps .mobile-category-selector-breadcrumbs').is(':visible')) {
        var $firstLevelUL = $('.new-ad-steps ul[data-level="1"]');
        if ($firstLevelUL.length) {
            var leftPosition = 0 - ((currentLevel - 1) * 100);
            var animateSpeed = (currentLevel - 1) * 150;
            $firstLevelUL.animate({
                left: leftPosition + '%'
            }, animateSpeed, function() {
                updateMobileBreadCrumbs();
                recalculateAndSetCategorySelectorHeight();
            });
        }
    } else {
        $('.new-ad-steps ul[data-level="1"]').css({'left':0});
        recalculateAndSetCategorySelectorHeight();
    }
}

$(document).ready(function(){
    var $category_id_hidden = $('#category_id');
    currentLevel = parseInt($('.new-ad-steps .category-selector-container').data('current-level'));
    if (currentLevel <= 0) {
        currentLevel = 1;
    }
    var catId = parseInt($.trim($category_id_hidden.val())) || null;
    initCategorySelector(catId);
});

$(window).resize(function(){
    // delay execution of showMobileCategorySelectorsLevel fix
    clearTimeout(this.categorySelectorResizeTimer);
    this.categorySelectorResizeTimer = setTimeout(function(){showMobileCategorySelectorsLevel()}, 250);
});
