var Carouseler = {
    options: {
        pageClass: 'col-xs-12',
        itemsInPage: 1,
        scribe: 50
    },
    instances: [],
    elements: [],
    running: false,
    init: function($window){
        var self_ = this;
        this.elements = jQuery('.slideshow-on-mobile');

        // Bail early if there aren't any carousels to handle
        if (this.elements.length <= 0) {
            return;
        }

        if (this.shouldUseCarousels()) {
            this.checkNumShownItems();
            this.startCarousels();
        }

        // Make sure we adapt when window resizes
        $window.on('resize', function(){
            self_.resizeCallback();
        });
    },
    resizeCallback: function() {
        if (this.shouldUseCarousels()) {
            var changed = this.checkNumShownItems();
            if ( ( ! this.running ) || ( changed && this.running ) ) {
                this.startCarousels();
            }
        } else {
            if (this.running) {
                this.destroyCarousels();
            }
        }
    },
    checkNumShownItems: function(){
        var changed = false;

        var window_width = this.$win.width();
        if (window_width > 500) {
            if (this.options.itemsInPage != 2) {
                this.options.itemsInPage = 2;
                changed = true;
            }
        } else {
            if (this.options.itemsInPage != 1) {
                this.options.itemsInPage = 1;
                changed = true;
            }
        }

        return changed;
    },
    shouldUseCarousels: function(){
        return (matchMedia("(max-width: 768px)").matches);
    },
    startCarousels: function(){
        this.destroyCarousels();
        var self = this;
        this.elements.each(function(idx, el){
            self.instances[idx] = new Dragend(el, self.options);
        });

        this.running = true;
    },
    destroyCarousels: function () {
        for (var k in this.instances) {
            if (this.instances.hasOwnProperty(k)) {
                var instance = this.instances[k];
                if (instance) {
                    instance.destroy();
                }
            }
        }

        this.running = false;
    }
};
