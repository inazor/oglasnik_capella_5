jQuery(function(){
    var duration = function(x, y) {
        return Math.floor(Math.random() * ((y-x)+1) + x);
    };
    var $button = jQuery('#load-more-latest');

    var $spinner = $button.find('.loading');
    var block_idx = $button.data('start');

    var load = function(){
        block_idx++;

        $spinner.removeClass('hidden');
        var delay = duration(350, 1000);

        var timer = setTimeout(function(){
            $spinner.addClass('hidden');

            // Show another block if it's there
            var $block = jQuery('#latest-ads-block-' + block_idx);
            if ($block.length > 0) {
                $block.removeClass('hidden');
                // console.log('block found and shown');
            } else {
                // Make up a new block? Reload the page? Actually load ajax data?
                // Just show an error?
                // console.log('block not found');
            }

            clearTimeout(timer);
        }, delay);
    };

    $button.on('click', function(e){
        load();
    });
});
