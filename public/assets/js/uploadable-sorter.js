function uploadable_item_append(media_data, selector) {
    var html_builder = function(str) {
        var $html = $('<div/>').append(str);
        return $html.children();
    };

    $.each(media_data, function(idx, media){
        var $remover = html_builder('<div class="btn-group btn-group-sm"><span class="btn btn-danger delete"><span class="fa fa-trash-o"><!--IE--></span></span></div>');
        $remover.bind('click', function(){
            uploadable_item_delete($(this));
        });

        var $next = html_builder('<div class="btn-group btn-group-sm"><span class="btn btn-default move-next"><span class="fa fa-arrow-circle-o-right"><!--IE--></span></span></div>');
        $next.bind('click', function(){
            uploadable_item_moveNext($(this));
        });

        var $prev = html_builder('<div class="btn-group btn-group-sm"><span class="btn btn-default move-prev"><span class="fa fa-arrow-circle-o-left"><!--IE--></span></span></div>');
        $prev.bind('click', function(){
            uploadable_item_movePrev($(this));
        });

        var $main = html_builder('<div class="btn-group btn-group-sm"><span class="btn btn-default set-as-main"><span class="fa fa-eye"><!--IE--></span></span></div>');
        $main.bind('click', function(){
            uploadable_item_setAsMain($(this));
        });

        var $mover = html_builder('<div class="hidden-sm btn-group btn-group-sm"><span class="btn btn-default drag-me ui-sortable-handle" style="cursor:move"><span class="fa fa-arrows"><!--IE--></span></span></div>');
        var $buttons = html_builder('<div class="uploadable-sorter-options btn-group btn-group-justified"></div>');

        $buttons.append($mover).append($main).append($prev).append($next).append($remover);

        $('<div/>')
            .addClass('uploadable-sorter-item col-lg-4 col-md-4 col-sm-4 col-xs-6')
            .append(
                $('<div/>')
                    .addClass('attachment')
                    .attr('data-id', media.id).data('id', media.id)
                    .append(
                        $('<div/>')
                            .addClass('attachment-preview')
                            .append(
                                $('<div/>')
                                    .addClass('thumb')
                                    .append(
                                        $('<div/>')
                                            .addClass('centered')
                                            .append(
                                                $('<img/>')
                                                    .attr('src', media.src)
                                            )
                                            .append(
                                                $('<input/>')
                                                .attr('type', 'hidden')
                                                .attr('name', $(selector).data('form') + '[]')
                                                .attr('value', media.id)
                                            )
                                    )
                            )
                    )
                    .append($buttons)
                    .append(
                        $('<div/>')
                            .addClass('text-left')
                            .attr('style', 'padding-top:5px')
                            .append(
                                $('<a/>')
                                    .attr('href', '/admin/media/edit/' + media.id)
                                    .attr('target', '_blank')
                                    .html('<span class="fa fa-picture-o fa-fw"></span>Edit')
                            )
                    )
            )
            .fadeIn('fast', function(){
                $(this).appendTo(selector);
            });
    });
}

function uploadable_item_setAsMain($sender) {
    $sender.closest('.uploadable-sorter-item').prependTo($('#uploadable_sorter'));
}

function uploadable_item_movePrev($sender) {
    var item = $sender.closest('.uploadable-sorter-item');
    if (item.length > 0) {
        var prev = item.prev();
        if (0 === prev.length) {
            return;
        }
        item.insertBefore(prev);
    }
}

function uploadable_item_moveNext($sender) {
    var item = $sender.closest('.uploadable-sorter-item');
    if (item.length > 0) {
        var next = item.next();
        if (0 === next.length) {
            return;
        }
        item.insertAfter(next);
    }
}

function uploadable_item_delete($sender) {
    $sender.closest('.uploadable-sorter-item').fadeOut('fast', function(){
        $container = $sender.closest('#uploadable_sorter');
        $(this).remove();
    });
}

$(document).ready(function() {
    $('#uploadable_sorter').sortable({
        forcePlaceholderSize: true,
        handle: '.drag-me',
        helper: 'clone',
        listType: 'div',
        items: '.uploadable-sorter-item',
        opacity: .6,
        placeholder: 'col-lg-4 col-md-4 col-sm-4 col-xs-6 placeholder',
        revert: 250,
        tabSize: 25,
        create: function(event, ui) {
            // initialize set-as-main button
            $('#uploadable_sorter .uploadable-sorter-options .btn.set-as-main').click(function(){
                uploadable_item_setAsMain($(this));
            });
            // initialize move-prev button
            $('#uploadable_sorter .uploadable-sorter-options .btn.move-prev').click(function(){
                uploadable_item_movePrev($(this));
            });
            // initialize move-next button
            $('#uploadable_sorter .uploadable-sorter-options .btn.move-next').click(function(){
                uploadable_item_moveNext($(this));
            });
            // initialize delete button
            $('#uploadable_sorter .uploadable-sorter-options .btn.delete').click(function(){
                uploadable_item_delete($(this));
            });
        },
        stop: function(event, ui) {}
    });
});
