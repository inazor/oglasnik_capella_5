var purchaseSummaryProducts = {
    'online': null,
    'offline': null
};

function display_purchase_summary() {
    if ($('.purchase-summary').length) {
        var $onlinePurchaseBox = $('.purchase-summary .pregled-kupnje-box[data-type="online"]');
        var $offlinePurchaseBox = $('.purchase-summary .pregled-kupnje-box[data-type="offline"]');

        if (purchaseSummaryProducts.online) {
            $onlinePurchaseBox.find('.name').text(purchaseSummaryProducts.online.name);
            $onlinePurchaseBox.find('.description').html(purchaseSummaryProducts.online.description);
            $onlinePurchaseBox.find('.icon').attr('src', '/assets/img/product-icons/online-' + purchaseSummaryProducts.online.id + '.png');
            $onlinePurchaseBox.show();
        }
        if (purchaseSummaryProducts.offline) {
            $offlinePurchaseBox.find('.name').text(purchaseSummaryProducts.offline.name);
            $offlinePurchaseBox.find('.description').html(purchaseSummaryProducts.offline.description);
            $offlinePurchaseBox.find('.icon').attr('src', '/assets/img/product-icons/offline-' + purchaseSummaryProducts.offline.id + '.png');
            $offlinePurchaseBox.show();
        } else {
            $offlinePurchaseBox.hide();
        }
    }
}

function disable_controls_within($container) {
    $container.find('input[type="checkbox"]').prop('disabled', true);
    $container.find('select').prop('disabled', true).selectpicker('refresh');
    // hide any extras dropdowns which are shown only when their corresponding checkbox is checked
    $container.find('select.product-extras-options').hide();
}

function enable_controls_within($container) {
    $container.find('input[type="checkbox"]').prop('disabled', false);
    $container.find('select').prop('disabled', false).selectpicker('refresh');
}

function product_box_changes_listener(e, $box) {
    // Disable all the controls within this group of products
    var $group = $box.parents('.products-group');
    disable_controls_within($group);
    // Now only enable the current box's controls
    enable_controls_within($box);

    var purchasedProductId = $box.data('id');
    var purchasedProductName = $box.find('.form-group.radio label').text();
    var purchasedProductDescription = '';
    var purchasedProductExtraDescription = '';

    // Ensuring extras longer than the chosen product option duration are disabled/unavailable
    var $selected_option = $box.find('.product-options select :selected');
    if ($selected_option.length > 0) {
        var selected_duration = $selected_option.data('duration');
        purchasedProductDescription = '<b>' + $.trim($selected_option.text().split('/')[0]) + '</b>' + ($box.data('type') == 'offline' ? ' u tiskanom izdanju Oglasnika' : ' u odabranoj kategoriji');

        // Going over $extras_options and disabling any that are bigger than the selected option duration
        var $extras_options = $box.find('.extras-prices select option');
        if ($extras_options.length > 0) {
            $extras_options.each(function(){
                var $opt = $(this);

                var extra_duration = $opt.data('duration');
                if (extra_duration > selected_duration) {
                    // When disabling, make sure it's also unselected in case it was selected earlier
                    if ($opt.prop('selected')) {
                        $opt.prop('selected', false);
                    }
                    $opt.prop('disabled', true).attr('disabled', 'disabled');
                } else {
                    $opt.prop('disabled', false).removeAttr('disabled');
                }

                if ($opt.prop('selected')) {
                    purchasedProductExtraDescription = ($box.data('type') == 'online' ? 'u trajanju od ' : '') + '<b>' + $.trim($opt.text().split('/')[0]) + '</b>';
                }

            });
            $extras_options.selectpicker('refresh');
        }
    }

    // Go through all the .extras-dropdown-toggle checkboxes and make
    // appropriate changes to the dropdowns based on the checkbox state
    var $checkboxes = $box.find('.extras-dropdown-toggle');
    $checkboxes.each(function(){
        var $checkbox = $(this);
        if ($checkbox.prop('checked')) {
            purchasedProductExtraDescription = $checkbox.closest('.checkbox').find('label .text-primary').text() + ' ' + purchasedProductExtraDescription;
        } else {
            purchasedProductExtraDescription = '';
        }
        // Grab the checkbox target
        var $prices_container = $checkbox.parents('.product-extra').find('.extras-prices');
        var $dropdown = $prices_container.find('select');
        if ($checkbox.prop('checked')) {
            $dropdown.prop('disabled', false).selectpicker('refresh');
            $prices_container.show();
        } else {
            $dropdown.prop('disabled', true).selectpicker('refresh');
            $prices_container.hide();
        }
    });

    purchaseSummaryProducts[$box.data('type')] = {
        'id': purchasedProductId,
        'name': purchasedProductName,
        'description': (purchasedProductDescription + (purchasedProductExtraDescription ? ' + ' + purchasedProductExtraDescription : ''))
    };

    // Remove any existing 'on' classes within the group
    $group.find('.product-box').removeClass('on');
    // Add 'on' to this specific box
    $box.addClass('on');

    // Recalculate pricing
    recalculate_and_show_product_box_pricing($box);

    // Recalculate full total
    calc_full_total();
}

/**
 * Goes through the .product-box's available options and extras
 * and sums up the total price for checked/selected options/extras and
 * displays the product box's total price (if the product is chosen)
 *
 * @param $box
 * @returns {number}
 */
function recalculate_and_show_product_box_pricing($box) {
    var price = 0;

    var $radio = $box.find('input[type="radio"]');
    if ($radio.length > 0) {
        if ($radio.is(':checked')) {
            var $selects = $box.find('select');
            // Add-up the prices only for non-disabled dropdowns
            $selects.each(function(){
                var $select = $(this);
                if (!$select.prop('disabled')) {
                    var $selected_options = $select.children(':selected');
                    if ($selected_options.length > 0) {
                        $selected_options.each(function () {
                            price += $(this).data('price');
                        });
                    }
                }
            });
            // Go through checked non-dropdown extras and tally up their prices too
            var $checkbox_only_prices = $box.find('input[data-price]:checked');
            if ($checkbox_only_prices.length > 0) {
                $checkbox_only_prices.each(function(){
                    price += $(this).data('price');
                });
            }
        }
    }

    // Converting lp to kn
    price = price / 100;

    var $price_holder = $box.find('.total-product-price');
    $price_holder.find('span.price').text(price.money_number());
    if (price > 0) {
        $price_holder.find('.no-price').hide();
        $price_holder.find('.has-price').show();
    } else {
        $price_holder.find('.no-price').show();
        $price_holder.find('.has-price').hide();
    }

    return price;
}

function calc_full_total() {
    var $full_total_holder = $('#full-total');

    /**
     * @link https://github.com/openexchangerates/accounting.js
     *
     * Takes a string/array of strings, removes all formatting/cruft and returns the raw float value
     *
     * Decimal must be included in the regular expression to match floats (defaults to `.`), so if the number
     * uses a non-standard decimal separator, provide it as the second argument.
     *
     * Also matches bracketed negatives (eg. "$ (1.99)" => -1.99)
     *
     * Doesn't throw any errors (`NaN`s become 0) but this may change in future
     */
    function unformat(value, decimal) {
        value = value || 0;

        if ('number' === typeof value) {
            return value;
        }

        decimal = decimal || '.';
        var regex = new RegExp("[^0-9-" + decimal + "]", 'g'),
            unformatted = parseFloat(
                ("" + value)
                    .replace(/\((.*)\)/, "-$1") // replace bracketed values with negatives
                    .replace(regex, '')         // strip out any cruft
                    .replace(decimal, '.')      // make sure decimal point is standard
            );

        return !isNaN(unformatted) ? unformatted : 0;
    }

    // Find checked radio buttons and add up their subtotals
    var $radios = $('.product-box').find('input[type=radio]:checked').not(':disabled');
    var total = 0;
    $radios.each(function(idx, radio){
        var $radio_box = $(radio).closest('.product-box');
        var $radio_price = $radio_box.find('.total-product-price .has-price .price');
        if ($radio_price.length > 0) {
            total += unformat($radio_price.text(), ',');
        }
    });

    // Slightly different mode when upgrading vs submitting a new entity
    var is_upgrade_mode = false;
    var $upgrade_form = $('#frm_upgrade_submit');
    if ($upgrade_form.length > 0) {
        is_upgrade_mode = true;
    }

    var total_txt, button_txt;
    if (total > 0) {
        total_txt = total.money_number() + ' kn';
        button_txt = 'Plaćanje';
    } else {
        total_txt = 'besplatno';
        button_txt = 'Predaj besplatan oglas';
        if (is_upgrade_mode) {
            button_txt = 'Odaberite barem jednu opciju izdvajanja';
        }
    }
    $full_total_holder.text(total_txt);

    var $button = $('#submitter');
    var $button_text = $button.find('.button-text');
    $button_text.text(button_txt);

    if (is_upgrade_mode) {
        if (total <= 0) {
            $button.attr('disabled', 'disabled');
        } else {
            $button.removeAttr('disabled');
        }
    }
    display_purchase_summary();
}

function offline_products_toggle($checkbox) {
    var $container = $('.products-group-offline');
    var $radios = $container.find('input[name="products-offline"]');
    var $printNoPhone = $('div.print-no-phone');
    if ($checkbox.prop('checked')) {
        // see if we have div.print-no-phone and if yes, show it as we noticed that
        // user wants to publish the ad in print but gave us no phone number
        if ('undefined' !== typeof $printNoPhone && $printNoPhone.size()) {
            $printNoPhone.show();
        }

        // Enable all the group radios
        $radios.prop('disabled', false);

        // Assume there's a checked radio present, if not, set first one as checked
        var $checked = $radios.filter(':checked');
        var $first_radio = $radios.first();
        var $which;

        // If no checked, check the first one
        if ($checked.length <= 0) {
            $which = $first_radio;
            $which.trigger('click');
        } else {
            $which = $checked;
        }

        // Emit our custom event on the checked box
        var $box = $which.closest('.product-box');
        $box.trigger('changes.oglasnik', [$box]);
        $container.removeClass('hidden');
    } else {
        purchaseSummaryProducts.offline = null;
        // see if we have div.print-no-phone and if yes, hide it
        if ('undefined' !== typeof $printNoPhone && $printNoPhone.size()) {
            $printNoPhone.hide();
        }

        // Disable all the standard controls for this group
        disable_controls_within($container);
        // Disable all the radios additionally (since they're not handled by disable_controls_within())
        $radios.prop('disabled', true);
        $container.addClass('hidden');
        // Since the offline products checkbox is no longer "checked", we need to recalc
        calc_full_total();
    }
}

function products_chooser_init() {
    // Disable all the checkboxes and dropdowns inside each group by default
    var $groups = $('.products-group');
    disable_controls_within($groups);

    var $product_boxes = $('.product-box');

    // Set up our custom event listener on all the product boxes
    $product_boxes.on('changes.oglasnik', product_box_changes_listener);

    // Listen to changes on various elements within $groups and emit our custom event
    var watch_changes_on = [
        '.extras-dropdown-toggle',
        '.product-box select',
        'input[type="radio"]'
    ];
    $.each(watch_changes_on, function(idx, selector){
        $groups.find(selector).on('change', function(){
            var $box = $(this).closest('.product-box');
            $box.trigger('changes.oglasnik', [$box]);
        });
    });

    // If no radios are checked (aka products chosen), check the first one
    var $checked_radios = $product_boxes.find('input[type=radio]:checked');
    if (0 === $checked_radios.length) {
        $product_boxes.first().find('input[type=radio]').prop('checked', true);
    }

    // Simulate a click on any pre-selected products
    $groups.find('input[type="radio"]:checked').trigger('change');

    // Setup offline products toggler checkbox handler
    var $offline_checkbox = $('#offline-products');
    $offline_checkbox.on('change', function(){
        offline_products_toggle($(this));
    });
    // Call it with whatever is the current state of it (if it exists)
    if ($offline_checkbox.length > 0) {
        offline_products_toggle($offline_checkbox);
    }
}

$(document).ready(function($){
    $('.products-group .icon_dropdown select').selectpicker({
        style: 'btn btn-default',
        noneSelectedText: 'Odaberite trajanje',
        liveSearch: false,
        noneResultsText: '',
        mobile: OGL.is_mobile_browser,
        dropupAuto: false
    });

    products_chooser_init();
});
