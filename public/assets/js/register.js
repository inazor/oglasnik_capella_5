jQuery(document).ready(function(){
    jQuery('#birth_date').dateDropdowns({
        daySuffixes: false,
        minAge: parseInt(jQuery('#birth_date').data('minage')),
        defaultValue: jQuery('#birth_date').val(),
        dropdownClass: 'form-control date-select',
        wrapperClass: 'date-dropdowns',
        submitFieldName: 'birth_date'
    });

    jQuery('#frm_register select').selectpicker({
        style: 'btn btn-default',
        noneSelectedText: '',
        liveSearch: true,
        noneResultsText: '',
        mobile: OGL.is_mobile_browser,
        dropupAuto: false
    });
    // bind country dropdown change
    jQuery('#country_id').change(function(){
        jQuery('#county_id').find('option').remove();
        var county_div = jQuery('#county_id').closest('.form-group');
        if (jQuery.trim(jQuery(this).val()) !== '') {
            jQuery.get(
                '/ajax/counties/' + jQuery.trim($(this).val()),
                function(data) {
                    if (data.length) {
                        jQuery.each(data, function(idx, data){
                            jQuery('#county_id').append(jQuery('<option/>').attr('value', data.id).text(data.name));
                        });
                        if (!county_div.is(':visible')) {
                            county_div.fadeIn();
                        }
                    } else {
                        if (county_div.is(':visible')) {
                            county_div.fadeOut();
                        }
                    }
                    jQuery('#county_id').selectpicker('refresh');
                },
                'json'
            );
        } else {
            county_div.fadeOut();
        }
    });
    $('input.phone-number-handler').phoneNumberHandler({
        'triggerSelect'       : $('#country_id'),
        'triggerDomesticValue': 1
    });
    jQuery('#submitBtn').click(function(){
        jQuery('#frm_register').submit();
    });
});
