$(document).ready(function(){
    var $masonry_articles = $('.masonry-articles');
    var $masonry_items = $masonry_articles.find('.masonry-item');

    if ($.fn.masonry && $masonry_items.length) {
        $masonry_articles.masonry({
            itemSelector: '.masonry-item'
        });
        $masonry_articles.imagesLoaded().progress(function(){
            $masonry_articles.masonry('layout');
        });
    }

    $('.featured-classifieds').genericCarousel({
        selectors: {
            slider: '.grid-layout'
        },
        sliderOptions: {
            slide: '.featured-classified',
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [{
                breakpoint: 1280,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }]
        }
    });
});
