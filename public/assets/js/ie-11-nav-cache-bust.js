/**
 * dummy handler not doing anything extra really, but hopefully prevents
 * IE11 back navigation caching: https://msdn.microsoft.com/library/dn265017(v=vs.85).aspx
 */
window.onbeforeunload = function(e){
    // stare into the void...
};
