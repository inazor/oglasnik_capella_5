/*! jquery.pagevisibility v0.2.2 | (c) 2013 Daniel Herman | opensource.org/licenses/MIT */
(function(i){"function"==typeof define&&define.amd?define(["jquery"],i):i(jQuery)})(function(i){"use strict";var e,t,n,d,r,o,u,c;for(c=window.document,u=Object.defineProperty||i.noop,r=!1,t=["ms","moz","webkit",""];t.length;)if(e=t.pop(),o=e?e+"Hidden":"hidden",o in c){r=!0;break}if(r)e&&(i.event.special.visibilitychange={bindType:e+"visibilitychange"},u(c,"hidden",{get:function(){return c[o]}}),u(c,"visibilityState",{get:function(){return c[e+"VisibilityState"]}}));else{n=c.hidden=!1,d=c.visibilityState="visible";try{u(c,"hidden",{get:function(){return n}}),u(c,"visibilityState",{get:function(){return d}})}catch(b){}i(window).bind("focus blur",function(e){var t="blur"===e.type;n!==t&&(c.hidden=n=t,c.visibilityState=d=t?"hidden":"visible",i(c).trigger("visibilitychange"))})}});

/*! matchMedia() polyfill - Test a CSS media type/query in JS. Authors & copyright (c) 2012: Scott Jehl, Paul Irish, Nicholas Zakas, David Knight. Dual MIT/BSD license */
window.matchMedia || (window.matchMedia = function() {
    "use strict";

    // For browsers that support matchMedium api such as IE 9 and webkit
    var styleMedia = (window.styleMedia || window.media);

    // For those that don't support matchMedium
    if (!styleMedia) {
        var style       = document.createElement('style'),
            script      = document.getElementsByTagName('script')[0],
            info        = null;

        style.type  = 'text/css';
        style.id    = 'matchmediajs-test';

        script.parentNode.insertBefore(style, script);

        // 'style.currentStyle' is used by IE <= 8 and 'window.getComputedStyle' for all other browsers
        info = ('getComputedStyle' in window) && window.getComputedStyle(style, null) || style.currentStyle;

        styleMedia = {
            matchMedium: function(media) {
                var text = '@media ' + media + '{ #matchmediajs-test { width: 1px; } }';

                // 'style.styleSheet' is used by IE <= 8 and 'style.textContent' for all other browsers
                if (style.styleSheet) {
                    style.styleSheet.cssText = text;
                } else {
                    style.textContent = text;
                }

                // Test if media query is true or false
                return info.width === '1px';
            }
        };
    }

    return function(media) {
        return {
            matches: styleMedia.matchMedium(media || 'all'),
            media: media || 'all'
        };
    };
}());

/**
 * Maintain bs3 tabs/pills state across page reloads (via localStorage).
 * Make sure the parent <ul> has an id attribute!
 */
jQuery(function(){
    if (jQuery.tab) {
        var json, state;
        jQuery('a[data-toggle="pill"], a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var href, json, parent_id, state;

            state = localStorage.getItem('tabs-state');
            json = JSON.parse(state || '{}');
            parent_id = jQuery(e.target).parents('ul.nav.nav-pills, ul.nav.nav-tabs').attr('id');
            href = jQuery(e.target).attr('href');
            json[parent_id] = href;

            return localStorage.setItem('tabs-state', JSON.stringify(json));
        });

        state = localStorage.getItem('tabs-state');
        json = JSON.parse(state || '{}');

        $.each(json, function (container_id, href) {
            return jQuery('#' + container_id + ' a[href=' + href + ']').tab('show');
        });

        jQuery('ul.nav.nav-pills, ul.nav.nav-tabs').each(function () {
            var $this = jQuery(this);
            if (!json[$this.attr('id')]) {
                return $this.find('a[data-toggle=tab]:first, a[data-toggle=pill]:first').tab('show');
            }
        });
    }
});
var $window = jQuery(window);

jQuery(document).ready(function(){
    /*
    var come_back_msg = 'Hej, vrati nam se! :)';
    var orig_title = document.title;
    jQuery(document).on('visibilitychange', function(){
        if (document.hidden) {
            document.title = come_back_msg;
        } else {
            if (come_back_msg === document.title) {
                document.title = orig_title;
            }
        }
    });
    */

    // Lazyload images
    jQuery('.lazy').lazy();

    var $bg_banner = jQuery('.banner-background div > *');
    if ($bg_banner.length > 0) {
        jQuery('#page-mm-container').addClass('container-with-background-banner');
        jQuery('.banner-background').addClass('banner-background-zindex-auto');
    }

    // Clean up any ad zone divs that didn't get anything returned in them
    // (have to wait for domready or similar since some elements don't exist
    // if this is called directly from <head>
    function removeEmptyAdverticumZones(requested, returned) {
        for (var i = 0, l = requested.length; i < l; i++) {
            var requestedZone = requested[i];
            var returnedZone = returned[requestedZone.id] ? returned[requestedZone.id] : false;
            if ((returnedZone && returnedZone.empty) || !returnedZone) {
                //console.log('requested Zone ' + requestedZone.id + ' is empty');
                var elementId = 'zone' + requestedZone.id;
                var $placeholder = jQuery('#' + elementId);
                if ($placeholder.length > 0) {
                    var $parent = $placeholder.parent();
                    if ($parent.hasClass('banner')) {
                        $parent.remove();
                    } else {
                        $placeholder.remove();
                    }
                }
            }
        }
    }
    // This way we delete everything even if adblocking prevents any ads from loading etc...
    var goAdZonesReturned = {};
    if (window.goAdverticum3) {
        goAdZonesReturned = window.goAdverticum3.getZones();
    }
    var requestedZones = [];
    if (window.goAdZonesRequested) {
        requestedZones = window.goAdZonesRequested;
    }
    removeEmptyAdverticumZones(requestedZones, goAdZonesReturned);

    var $mobile_nav = jQuery("#mobile-nav");
    $mobile_nav.mmenu({
        'slidingSubmenus': false,
        'extensions': [
            'pagedim-black'
        ],
        'navbars': [
            {
               'position': 'top',
               'content': [
                    '<a href="/" class="homer mmenu-logo"></a>'
                ]
            }
        ]
    }, {
        classNames: {
            selected: 'active',
            spacer: 'spacer'
        },
        offCanvas: {
            pageSelector: '.page-mm-container'
        }
    });
    var mmAPI = $mobile_nav.data('mmenu');
    jQuery('.mobile-nav-trigger').on('click', function(){
        mmAPI.open();
    });

    // TODO: this could be made more reusable probably
    jQuery('.target-toggler').on('change', function(){
        var $el = jQuery(this);
        var checked = $el.prop('checked');
        var $toggle_target = $el.data('toggle-target');
        if ($toggle_target) {
            jQuery($toggle_target).toggle(checked);
        }
    }).trigger('change');

    // Search forms handling
    var $search_forms = jQuery('.searchform');
    if ($search_forms.length) {
        var $search_term_inputs   = $search_forms.find('.search-term');
        var $main_search_category = $search_forms.find('.search-category-input');
        var $dropdown_button_name = $search_forms.find('.dropdown-button-name');

        // Add generic submit handler for empty submit prevention
        $search_forms.on('submit', function(e) {
            var $form = jQuery(this);
            var $input = $form.find('.tt-input');
            var term = $.trim($input.val());
            if ('' == term) {
                e.preventDefault();
                $input.focus();
                // If term is empty but a category has been chosen, go to that category
                var chosen_cat = $.trim($main_search_category.val());
                if ('' !== chosen_cat) {
                    var href = $main_search_categories.filter('[data-category-id="' + chosen_cat + '"]').data('category-href');
                    if (href) {
                        window.location = href;
                    }
                }
            }
        });

        // Add ENTER key submit triggers
        $search_term_inputs.keyup(function(e){
            var code = e.which; // recommended to use e.which, it's normalized across browsers
            if (code === 13) { // ENTER
                // Find the term's form and submit it
                var $form = jQuery(this).closest('form');
                if ($form.length > 0) {
                    $form.trigger('submit');
                }
            }
        });

        // Setup categories dropdown
        var $main_search_categories = $search_forms.find('.category-chooser li a');
        $main_search_categories.each(function(i, elem){
            var $elem = jQuery(elem);
            $elem.click(function(e){
                e.preventDefault();
                var $this = jQuery(this);
                $main_search_categories.removeClass('active');
                $this.addClass('active');
                $main_search_category.val($this.data('category-id'));
                if ('' === $.trim($main_search_category.val())) {
                    $main_search_category.prop('disabled', true);
                } else {
                    $main_search_category.prop('disabled', false);
                }
                $dropdown_button_name.text($this.text());
            });
            // Trigger click on pre-selected/active item
            if ($elem.hasClass('active') && $elem.data('category-id') > 0) {
                $elem.trigger('click');
            }
        });

        // Setup Bloodhound sources
        var searchAutocomplete = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('term'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            dupDetector: function(remoteMatch, localMatch) {
                return remoteMatch.term === localMatch.term;
            },
            prefetch: '/ajax/search-autocomplete',
            remote: {
                url: '/ajax/search-autocomplete/%QUERY',
                wildcard: '%QUERY'
            }
        });
        var searchShopsAutocomplete = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            dupDetector: function(remoteMatch, localMatch) {
                return remoteMatch.id === localMatch.id;
            },
            prefetch: '/ajax/search-shops-autocomplete',
            remote: {
                url: '/ajax/search-shops-autocomplete/%QUERY',
                wildcard: '%QUERY'
            }
        });

        // Setup typeaheads for all inputs
        $search_term_inputs.each(function(){
            var $input = jQuery(this);
            $input.typeahead(null, {
                name: 'search-autocomplete',
                display: 'term',
                source: searchAutocomplete.ttAdapter(),
                templates: {
                    suggestion: function(data) {
                        return '<div class="tt-suggestion tt-selectable">' + OGL.fuzzyMe(data.term, $input.val()).highlightedTerm + '</div>';
                    }
                }
            }, {
                name: 'search-shops-autocomplete',
                display: 'name',
                source: searchShopsAutocomplete.ttAdapter(),
                templates: {
                    header: '<div class="search-header">Pronađene trgovine</div>',
                    suggestion: function(data) {
                        return '<div class="tt-suggestion tt-selectable"><a class="suggestion" href="' + data.frontend_url + '">' + OGL.fuzzyMe(data.name, $input.val()).highlightedTerm + '</a></div>';
                    }
                }
            }).on('typeahead:selected', function(e, datum){
                // Submit auto-completed search form only when a term is selected
                // (that way shop links still work as they used to)
                if ('undefined' !== typeof datum.term) {
                    e.target.form.submit();
                }
            });
        });
    }

    // Mobile search icon toggle
    jQuery('.js-search-toggle').on('click', function(e){
        e.preventDefault();
        var $el = jQuery(this);
        var $form = jQuery('#search-xs');
        $el.toggleClass('is-active');
        var active = $el.hasClass('is-active');
        // $form.toggle(active);
        $form.toggleClass('hidden-xs', !active);
        if (active) {
            jQuery('#search-xs-term').focus();
        }
    });

    OGL.sticky_last_sidebar_box();
    OGL.sticky_filterbits();
    OGL.mobile_filtering_faux_modal_thingy();
    $window.on('resize', function(){
        OGL.mobile_filtering_faux_modal_thingy();
    });

    // triggering sticky-kit recalc on window.load and on doc ready since
    // banners come in later and change the heights of the page, which messes up stickies...
    jQuery(document.body).trigger('sticky_kit:recalc');
    $window.load(function(){
        jQuery(document.body).trigger('sticky_kit:recalc');
    });

    // Initialize the favorites thing
    Favoriter.init();

    // Carousels dynamic handler (depends on Dragend)
    Carouseler.init($window);

    /* Turn this on to automatically scroll to firs error
    if ('function' === typeof OGL.scrollToFirstError) {
        OGL.scrollToFirstError();
    }
    */

    jQuery('#mobile-search-category').selectpicker({
        style: 'category-select',
        liveSearch: false,
        mobile: OGL.is_mobile_browser,
        dropupAuto: false
    });

    // initialize featured shops carousel
    var $featuredShopsContainer = jQuery('.featured-shops');
    if ($featuredShopsContainer.length) {
        var shoppingWindowsOptions = {
            selectors: {
                slider: '.featured-shops-windows'
            },
            sliderOptions: {
                slide: '.shop',
                slidesToShow: 3,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 1280,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            }
        };
        $featuredShopsContainer.genericCarousel(shoppingWindowsOptions);
    }
});
