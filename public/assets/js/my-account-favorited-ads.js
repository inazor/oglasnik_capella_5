function favoritedClassifiedSaveNote(classifiedId, note) {
    var $favoritedClassifiedRow           = $('.row[data-favorite-classified-id="' + classifiedId + '"]');
    var $favoritedClassifiedNoteContainer = null;
    var $savingIndicator                  = null;
    if ($favoritedClassifiedRow.length == 1) {
        $favoritedClassifiedNoteContainer = $favoritedClassifiedRow.find('.saved-ad-note');
    }
    if ($favoritedClassifiedNoteContainer) {
        $savingIndicator = $favoritedClassifiedNoteContainer.find('.save-note .fa-spin');
    }

    if ($savingIndicator) {
        $savingIndicator.removeClass('hidden');
    }

    jQuery.post(
        '/moj-kutak/spremi-biljesku/' + classifiedId, {
            'note': note
        },
        function (json) {
            if ($savingIndicator) {
                $savingIndicator.addClass('hidden');
            }

            if (!json.status && 'undefined' !== typeof json.msg) {
                alert(json.msg);
            }

            if ($favoritedClassifiedNoteContainer) {
                $favoritedClassifiedNoteContainer.removeClass('bg-color-white');
            }
        }
    );
}

$(document).ready(function(){
    $('#deleteModal').on('show.bs.modal', function (event) {
        var $sender = $(event.relatedTarget);
        var $modal = $(this);
        var form_url = $sender.data('formurl');
        if ($sender.hasClass('bulk-delete')) {
            form_url = form_url + $sender.data('classified-ids');
            $modal.find('.modal-body p').html('Jeste li sigurni da želite obrisati <b class="text-danger">sve označene oglase</b> iz popisa spremljenih oglasa?');
        } else {
            $modal.find('.modal-body p').html('Jeste li sigurni da želite obrisati ovaj oglas iz popisa spremljenih oglasa?');
        }
        $modal.find('#frm_ads_delete').attr('action', form_url);
    });
    $('.actions button[data-href]').click(function(e){
        e.preventDefault();
        window.location.href = $(this).data('href');
    });
    $('.ad-box-wide input.checkbox').change(function(){
        if ($(this).prop('checked')) {
            $(this).closest('.ad-box-wide').addClass('selected');
        } else {
            $(this).closest('.ad-box-wide').removeClass('selected');
        }

        var selected_classified_ids = [];
        $('.ad-box-wide.selected').each(function(i, elem){
            selected_classified_ids.push($(elem).data('classified-id'));
        });
        selected_classified_ids = selected_classified_ids.join(',');

        $('.bulk-actions button').prop('disabled', (selected_classified_ids == ''));
        $('.bulk-actions button.bulk-delete').attr('data-classified-ids', selected_classified_ids).data('classified-ids', selected_classified_ids);
    });
    $('.bulk-actions input.select-all').change(function(){
        $('.ad-box-wide input.checkbox').prop('checked', $(this).prop('checked')).trigger('change');
    });
    $('.saved-ad-note textarea').focus(function(){
        $(this).closest('.saved-ad-note').addClass('bg-color-white');
    }).blur(function(e){
        e.preventDefault();
        var $favoritedClassifiedNoteTextarea = $(this);
        var favoritedClassifiedId = $favoritedClassifiedNoteTextarea.closest('.row').data('favorite-classified-id');
        // wait 1 tick so we can detect what element triggered the blur event
        setTimeout(function(){
            var $blurTrigger = $(document.activeElement);
            var isCallbackBlured = false;
            if ($blurTrigger.hasClass('save-note')) {
                var blurTriggerClassifiedId = $blurTrigger.closest('.row').data('favorite-classified-id');
                if (favoritedClassifiedId == blurTriggerClassifiedId) {
                    isCallbackBlured = true;
                    favoritedClassifiedSaveNote(favoritedClassifiedId, $.trim($favoritedClassifiedNoteTextarea.val()));
                }
            }
            if (!isCallbackBlured) {
                $favoritedClassifiedNoteTextarea.closest('.saved-ad-note').removeClass('bg-color-white');
            }
        }, 10);
    });
});
