var unreadMessageScrollTimer = null;

function getVisibleUnreadMessagesID() {
    var visibleUnreadMessagesID = [];
    $('.msg-box[data-message-read="0"]').each(function(i, msg){
        var $msg = $(msg);
        if (OGL.isElementInViewport($msg)) {
            visibleUnreadMessagesID.push($msg.data('message-id'));
        }
    });

    return visibleUnreadMessagesID;
}

function markVisibleUnreadMessagesAsRead() {
    var visibleUnreadMessagesID = getVisibleUnreadMessagesID();
    if (visibleUnreadMessagesID.length) {
        jQuery.post(
            '/ajax/read-message/' + visibleUnreadMessagesID.join(','),
            function (json) {
                if (json && json.status) {
                    $notificationCount = $('#top a.notification .count');
                    if ($notificationCount.length) {
                        if ('undefined' !== typeof json.count && json.count > 0) {
                            $notificationCount.text(json.count);
                        } else {
                            $notificationCount.hide('fast', function(){
                                $notificationCount.remove();
                            });
                        }
                    }
                    $(visibleUnreadMessagesID).each(function(i, msgID){
                        $msgBox = $('.msg-box[data-message-id="' + msgID + '"]');
                        if ($msgBox.length) {
                            $msgBox
                                .attr('data-message-read', '1').data('message-read', '1')
                                .removeClass('msg-new').addClass('msg-read');
                        }
                    });
                }
            }
        );
    }
}

$(document).ready(function(){
    markVisibleUnreadMessagesAsRead();
    $('#frm_modal_reply').ajaxifyModalForm();

    $('#modal-send-reply').on('shown.bs.modal', function(event){
        var $modal         = $(this);
        var $button        = $(event.relatedTarget);
        var recipientName  = $button.data('recipient-name');
        var recipientEmail = null;
        var formAction     = $button.data('reply-url');
        if ('undefined' !== typeof $button.data('recipient-email')) {
            recipientEmail = $button.data('recipient-email');
        }
        $modal.find('.recipient').text(recipientName);
        $modal.find('form').attr('action', formAction);
        $modal.find('textarea').focus();
    });
    $('#deleteModal').on('shown.bs.modal', function(event){
        var $modal         = $(this);
        var $button        = $(event.relatedTarget);
        var formAction     = $button.data('form-url');
        $modal.find('form').attr('action', formAction);
    });
});

$(window).scroll(function(){
    clearTimeout(this.unreadMessageScrollTimer);
    this.unreadMessageScrollTimer = setTimeout(function(){markVisibleUnreadMessagesAsRead()}, 250);
});
