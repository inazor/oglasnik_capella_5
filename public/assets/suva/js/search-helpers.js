$(document).ready(function(){

    function get_param($el){
        return $el.attr('href').replace('#', '');
    }

    function get_field_id($el) {
        var $ddown = get_links_dropdown($el);
        return $ddown.data('param');
    }

    function get_links_dropdown($el) {
        return $el.parent().parent();
    }

    function update_dropdown_label($dropdown, label, is_html) {
        is_html = is_html || false;
        var label_el = $dropdown.prev().find('.dropdown-label');
        if (label_el.length > 0) {
            if (is_html) {
                label_el.html(label);
            } else {
                label_el.text(label);
            }
        }
    }

    function set_active_dropdown_item($link) {
        $link.parent().parent().find('li').removeClass('active');
        $link.parent().addClass('active');
        // if $link contains an icon, grab that too instead of just text
        var label;
        var is_html = false;
        if ($link.find('.fa').length > 0) {
            is_html = true;
            label = $link.html();
        } else {
            label = $link.text();
        }
        update_dropdown_label(get_links_dropdown($link), label, is_html);
    }

    var $dropdowns = $('.search-panel .dropdown-menu');
    $dropdowns.each(function(idx){
        var $el = $(this);
        var links = $el.find('a');
        links.on('click', function(e){
            e.preventDefault();
            var $this = $(this);
            var param = get_param($this);
            var hidden_field_id = get_field_id($this);
            $(hidden_field_id).val(param);
            set_active_dropdown_item($this);
        });

        // check if current link's param matches the hidden value,
        // and if it does, update the 'label' to indicate so and
        // set the 'active' class to the dropdown li
        links.each(function(idx){
            var $elem = $(this);
            var param = get_param($elem);
            var current_value = $(get_field_id($elem)).val();
            if (param === current_value) {
                set_active_dropdown_item($elem);
            }
        });
    });

});
