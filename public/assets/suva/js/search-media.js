$(document).ready(function(){

    var is_popup = $('#media_library_container').data('is-popup');
    var bulk_mode_active = false;
    var $bulk_mode_button = $('#bulk-select-mode-toggle');
    var $bulk_button_html = $bulk_mode_button.html();
    var $bulk_delete_button = $('#bulk-delete');
    var $bulk_import_button = $('#bulk-import');
    var $media_search_results = $('.media-search-results');
    var $bulk_select_helpers = $('.bulk-select-helpers');

    function bulk_click(e) {
        e.preventDefault();
        var $this = $(this);

        if (!$this.hasClass('selected')) {
            $this.addClass('selected');
            $this.attr('aria-checked', true);
        } else {
            $this.removeClass('selected');
            $this.attr('aria-checked', false);
        }

        update_bulk_action_button_state();
    }

    function bulk_uncheck_click(e) {
        e.preventDefault();
        e.stopPropagation();
        var $parent = $(this).parent();
        if ($parent.hasClass('selected')) {
            $parent.removeClass('selected');
        }

        update_bulk_action_button_state();
    }

    function has_bulk_selected_elements() {
        return $('.attachment.selected').length > 0;
    }

    function bind_bulk_click_handlers() {
        $media_search_results.on('click.bulk', '.attachment', bulk_click);
    }

    function remove_bulk_click_handlers() {
        $media_search_results.off('click.bulk', '.attachment');
    }

    function update_bulk_action_button_state() {
        var has_selection = has_bulk_selected_elements();
        var $bulk_action_button = is_popup ? $bulk_import_button : $bulk_delete_button;
        if (bulk_mode_active || has_selection) {
            // show bulk action button if needed
            $bulk_action_button.removeClass('hidden');
            if (has_selection) {
                $bulk_action_button.removeAttr('disabled');
            } else {
                // nothing for bulk action, so disable the button
                $bulk_action_button.attr('disabled', 'disabled');
            }
        } else {
            // bulk mode not active or no selection, button should be disabled
            $bulk_action_button.attr('disabled', 'disabled');
            // hide the button if not hidden already
            if (!$bulk_action_button.hasClass('hidden')) {
                $bulk_action_button.addClass('hidden');
            }
        }
    }

    function bulk_mode_toggle() {
        bulk_mode_active = !bulk_mode_active;

        if (!bulk_mode_active) {
            // Clear/cancel selection
            $('.attachment.selected').trigger('click.bulk');
            $media_search_results.removeClass('bulk-selection-mode');
            $bulk_mode_button.html($bulk_button_html);
            $bulk_select_helpers.addClass('hidden');
            remove_bulk_click_handlers();
        } else {
            $media_search_results.addClass('bulk-selection-mode');
            $bulk_mode_button.html('<span class="glyphicon glyphicon-remove"></span> Cancel Selection');
            $bulk_select_helpers.removeClass('hidden');
            bind_bulk_click_handlers();
        }

        update_bulk_action_button_state();
    }

    function import_button_click($btn) {
        var media_id = $btn.data('id');
        var media_src = $btn.data('src');
        if ('undefined' !== typeof media_id && parseInt(media_id) && 'undefined' !== typeof media_src && $.trim(media_src)) {
            var ids = [];
            ids.push({id: media_id, src: media_src});
            import_media_from_popup(ids);
        }
    }

    function import_media_from_popup(ids) {
        try {
            window.opener.handle_popup_return(ids);
        }
        catch (err) {}
        window.close();
        return false;
    }

    // bind emulated checkbox handler
    $media_search_results.on('click', '.attachment .check', bulk_uncheck_click);

    $bulk_mode_button.on('click', function(){
        bulk_mode_toggle();
    });

    // Bind numpad '*' key to quickly toggle bulk select mode
    $(document).on('keyup', function(e){
        if (e.which === 106) {
            bulk_mode_toggle();
        }
    });

    // Handle bulk delete button click
    $bulk_delete_button.on('click', function(){
        var ids = [];
        $('.attachment.selected').each(function(){
            ids.push($(this).data('id'));
        });
        ids = ids.join(',');
        if (ids) {
            window.location.href = '/admin/media/delete/' + ids;
        }
    });

    // Handle bulk import button click
    $bulk_import_button.on('click', function(){
        var ids = [];
        $('.attachment.selected').each(function(){
            var media_id = $(this).data('id');
            var media_src = $(this).find('.import').data('src');

            if ('undefined' !== typeof media_id && parseInt(media_id) && 'undefined' !== typeof media_src && $.trim(media_src)) {
                ids.push({id: media_id, src: media_src});
            }
        });
        if (ids.length) {
            import_media_from_popup(ids);
        }
    });

    // Handle single import button click
    $('.attachment .import').click(function(e){
        e.preventDefault();
        import_button_click($(this));
    });

    // bulk selection helper buttons
    $('#bulk-select-all').on('click', function(){
        // deselect all, and then trigger our custom click handler on all of them
        $('#bulk-select-none').trigger('click');
        $('.attachment').trigger('click.bulk');
    });
    $('#bulk-select-none').on('click', function(){
        $('.attachment .check').trigger('click')
    });
    $('#bulk-select-toggle').on('click', function(){
        $('.attachment').trigger('click.bulk');
    });

    // Heavily customized dropzone.js thing, making the whole body act as a media upload dropzone
    function init_media_dropzone() {
        // Get the template HTML and remove it from the document
        var node = $('#dropzone-template')[0];
        node.id = '';
        var template = node.parentNode.innerHTML;
        node.parentNode.removeChild(node);

        // Holds our custom errors
        var $errors = $('#dropzone-errors');

        var edit_link_tpl = '/admin/media/edit/';
        var delete_link_tpl = '/admin/media/delete/';

        // Create the dropzone instance
        var dropzone = new Dropzone(document.body, {
            url: '/ajax/upload', // Set the url
            createImageThumbnails: false, // We're creating our own, much better looking and with proper resizing
            parallelUploads: 20,
            previewTemplate: template,
            // Where the previews are inserted
            previewsContainer: '#dropzone-previews',
            // Click trigger to select files
            clickable: '#media-upload-btn',
            // Re-written addedfile method in order to prepend new files to the beginning instead of appending
            addedfile: function(file) {
                var node, removeFileEvent, removeLink, _i, _j, _k, _len, _len1, _len2, _ref, _ref1, _ref2, _results;
                if (this.element === this.previewsContainer) {
                    this.element.classList.add('dz-started');
                }
                if (this.previewsContainer) {
                    file.previewElement = Dropzone.createElement(this.options.previewTemplate.trim());
                    file.previewTemplate = file.previewElement;
                    // this.previewsContainer.appendChild(file.previewElement);
                    this.previewsContainer.insertBefore(file.previewElement, this.previewsContainer.firstChild);
                    _ref = file.previewElement.querySelectorAll('[data-dz-name]');
                    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                        node = _ref[_i];
                        node.textContent = file.name;
                    }
                    _ref1 = file.previewElement.querySelectorAll('[data-dz-size]');
                    for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
                        node = _ref1[_j];
                        node.innerHTML = this.filesize(file.size);
                    }
                    if (this.options.addRemoveLinks) {
                        file._removeLink = Dropzone.createElement('<a class="dz-remove" href="javascript:undefined;" data-dz-remove>' + this.options.dictRemoveFile + '</a>');
                        file.previewElement.appendChild(file._removeLink);
                    }
                    removeFileEvent = (function(_this) {
                        return function(e) {
                            e.preventDefault();
                            e.stopPropagation();
                            if (Dropzone.UPLOADING === file.status) {
                                return Dropzone.confirm(_this.options.dictCancelUploadConfirmation, function() {
                                    return _this.removeFile(file);
                                });
                            } else {
                                if (_this.options.dictRemoveFileConfirmation) {
                                    return Dropzone.confirm(_this.options.dictRemoveFileConfirmation, function() {
                                        return _this.removeFile(file);
                                    });
                                } else {
                                    return _this.removeFile(file);
                                }
                            }
                        };
                    })(this);
                    _ref2 = file.previewElement.querySelectorAll('[data-dz-remove]');
                    _results = [];
                    for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
                        removeLink = _ref2[_k];
                        _results.push(removeLink.addEventListener('click', removeFileEvent));
                    }
                    return _results;
                }
            }
        });

        // Setting up various dropzone event listeners

        // Update total progress bar
        dropzone.on('totaluploadprogress', function(progress) {
            document.querySelector('#total-progress .progress-bar').style.width = progress + '%';
        });

        // dropzone.on('sending', function(file, xhr, form_data) {
        dropzone.on('sending', function() {
            // Show the total progress bar when upload starts
            document.querySelector('#total-progress').style.opacity = '1';
        });

        dropzone.on('error', function(file, msg, xhr) {
            var error_markup = '<div class="alert alert-danger alert-dismissible" role="alert">';
            error_markup += '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
            error_markup += '<strong>' + file.name + '</strong> Upload failed: ' + msg.error;
            error_markup += '</div>';
            $errors.append($(error_markup));
            dropzone.removeFile(file);
        });

        dropzone.on('success', function(file, response) {
            // Emit our own thumb and attach the response to file for further use
            file.response = response;

            if (response.ok) {
                if (response.media) {
                    dropzone.emit('thumbnail', file, response.media[0].preview.src);
                    return true;
                }
            }

            var err = { error: 'Unspecified server error / failed request' };
            if (response.msg) {
                err.error = response.msg;
            }
            dropzone.emit('error', file, err );
            return false;
        });

        dropzone.on('complete', function(file) {
            // Hide this element's progress bar
            file.previewElement.querySelector('.progress').style.opacity = '0';
        });

        // This is called on errors, on non-image uploads too
        dropzone.on('thumbnail', function(file, dataUrl) {
            if (file.response) {
                // Add class="icon" to the preview img if server returned an icon
                var icon = file.response.media[0].preview.icon || false;
                if (icon) {
                    var img = file.previewElement.querySelector('[data-dz-thumbnail]');
                    if (img) {
                        img.classList.remove('icon');
                        img.classList.add('icon');
                    }
                }
                // Setup proper type-* and subtype-* classnames as the server sees it
                var type = 'type-' + file.response.media[0].preview.type;
                var subtype = 'subtype-' + file.response.media[0].preview.subtype;
                var orientation = 'subtype-' + file.response.media[0].preview.orientation;
                var preview_el = file.previewElement.querySelector('.attachment-preview');
                var id = file.response.media[0].id;
                var src = file.response.media[0].preview.src;
                if (preview_el) {
                    preview_el.classList.add(type);
                    preview_el.classList.add(subtype);
                    preview_el.classList.add(orientation);
                    // Using jQuery here since dataset is still not universally supported... yay standards!
                    $(preview_el.parentNode).attr('data-id', id);
                    if (is_popup == 0) {
                        // Set the edit href
                        var edit_link = preview_el.querySelector('.edit-link');
                        if (edit_link) {
                            edit_link.href = edit_link_tpl + id;
                        }
                        // Set the delete href
                        var delete_link = preview_el.parentNode.querySelector('.delete');
                        if (delete_link) {
                            delete_link.href = delete_link_tpl + id;
                        }
                    } else {
                        // Set the import data-id and data-src properties
                        var import_link = preview_el.parentNode.querySelector('.import');
                        if (import_link) {
                            $(import_link).attr('data-id', id).data('id', id);
                            $(import_link).attr('data-src', src).data('src', src);
                            $(import_link).on('click', function(e){
                                e.preventDefault();
                                import_button_click($(this));
                            });
                        }
                    }
                }
            }
        });

        // Hide total progress when all done
        dropzone.on('queuecomplete', function(progress) {
            document.querySelector('#total-progress').style.opacity = '0';
            document.querySelector('#total-progress .progress-bar').style.width = 0;
        });
    }

    // Call dropzone setup
    init_media_dropzone();

});
