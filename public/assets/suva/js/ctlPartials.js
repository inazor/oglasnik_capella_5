function ajaxUpdateField( id_string, ajax_table, ajax_id, field, element_to_blink = null ) {
	//console.log('onchange'); 
	//alert('onchange');
	
	if (! element_to_blink ) element_to_blink = id_string;
	
	var value = $( '#'+ id_string ).val();
	value = value.replace('/','__SLASH__');
	urlEncodedValue = encodeURIComponent(value);
	
	//alert(value);
	
	var txtUrl = '/suva/ajax/updateField' 
		 + '/' + ajax_table
		 + '/' + ajax_id 
		 + '/' + field  
		 + '/' + urlEncodedValue;
	console.log( txtUrl );
	
	
	$.ajax({
		async: false,
		url: txtUrl,
		cache: false,
		dataType: 'json'
	}).done(function(json_data) {
		//alert('received');
		console.log('received');
		if (json_data){
			console.log(json_data.length);

			for(var i = 0; i < json_data.length; i++) {
				
				var obj = json_data[i];
				var elem_prev_border = $( '#' + element_to_blink).css("border");
				// alert(elem_prev_border);
				if (obj == 'OK')
					$( '#' + element_to_blink).css('border', '3px solid green');
				else {
					$( '#' + element_to_blink ).css('border', '3px solid red');
					alert(obj + ', request: ' + txtUrl);
					console.log(obj);
				}
				setTimeout(function(){
					$( '#' + element_to_blink ).css('border', elem_prev_border);
					}, 300);
			}; 
		}  
		else{
			$( '#' + element_to_blink ).css('border', '3px solid red');
			setTimeout(function(){
				$( '#' + element_to_blink ).css('border', '1px solid gray');
				}, 1000);
			alert('nothing received');
			console.log('received null');
		}
	});
}

function ctlTextUpdateField( id_string, ajax_table, ajax_id, field ) {
	ajaxUpdateField( id_string, ajax_table, ajax_id, field );
}

function ctlNumericUpdateField( id_string, ajax_table, ajax_id, field, element_to_blink = null ) {
	console.log('ctlNumericUpdateField');
	ajaxUpdateField( id_string, ajax_table, ajax_id, field, element_to_blink );
}

function ctlDateUpdateField( id_string, ajax_table, ajax_id, field ) {
	ajaxUpdateField( id_string, ajax_table, ajax_id, field );
}

function ctlDropdownUpdateField( id_string, ajax_table, ajax_id, field ) {
	ajaxUpdateField( id_string, ajax_table, ajax_id, field );
}
function ctlCheckboxUpdateField( id_string, ajax_table, ajax_id, field, element_to_blink = null ) {
	ajaxUpdateField( id_string, ajax_table, ajax_id, field, element_to_blink );
}


function testCtlPartial() {
	alert('test partials.js');
}

var gTimestamp = Date.now();  //koristi se u ctlTextTypeV3 za praćenje koliko se često poziva onClick


function callV3_1(params){
	
	ctlTextTypeV3_1(params);
	
}


function ctlTextTypeV3_1( p ) {
	
	console.log('ctlTextTypeV3_1');
	//TRENUTNO AKTIVNA
	// s jednim associative array parametrom p
	//oninput - Izvršva se kad se klikne nova tipka u polju za unos
		 
	/*
		ajaxCOntroller izvršava slijedeći upit

		select 
			id, 
			$pReturnField as _return_value, 
			$pSearchField as _search_field_value  
		from 
			$pTableName 
		where 
			cast($pSearchField as char) like '%$pSearchValue%' 
			$pAdditionalWhere 
		limit 15
	*/	

	//defaultne vrijednosti
	if (!"modelName" in p ) p['modelName'] = null;
	if (!"exprSearch" in p ) p['exprSearch'] = null;
	if (!"exprValue" in p ) p['exprValue'] = "''";
	if (!"exprShowList" in p ) p['exprShowList'] = "''";
	if (!"exprShowInput" in p ) p['exprShowInput'] = "''";
	if (!"additionalWhere" in p ) p['additionalWhere'] = "";
	if (!"inputIdPrefix" in p ) p['inputIdPrefix'] = '';
	if (!"keepTypedText" in p ) p['keepTypedText'] = false;
	if (!"nrReturnedRows" in p ) p['nrReturnedRows'] = 15;
	if (!"debug" in p ) p['debug'] = false;
	if (!"input_P1" in p ) p['input_P1'] = null;
	
	console.log ("keeptypedtext: " + p['keepTypedText'] );
	if ("input_P1" in p ){
		/* ako se navede input_P1 - to je vrijednost nekog inputa na stranici
			Primjer:  za: 
				'input_P1' : 'country_id'
				'additional_where' : ' and parent_id = __P1__ and level = 2 order by name '
			__P1__ se parsira u trenutnu vrijednost inputa country_id
		*/
		var inputP1Name = p['input_P1'];
		//replace zamjenjuje samo prvi occurence, treba ih više da zamijene sve occurence, TODO naći bolji način
		var inputP1Id = "#" + inputP1Name.replace('[','_').replace(']','_').replace('[','_').replace(']','_').replace('[','_').replace(']','_');
		if ( p['debug'] ) alert(inputP1Id);
		valP1 = $(inputP1Id).val();
		//alert (valP1);
		p['additionalWhere'] = p['additionalWhere'].replace("__P1__", valP1);
		
		//alert (p['additionalWhere']);
	}
	
	
	//console.log('oninput'); 
	var txtFieldName = p['inputIdPrefix'];
	var hiddenInput = $( '#' + txtFieldName );  //input (skriveni) koji ima pravu vrijednost koja se šalje u bazu (npr. id)
	var textInput = $( '#' + txtFieldName + '-input'); //input u koji se ukucava tekst
	var valueList = $( '#' + txtFieldName + '_value_list'); // DIV u kojem se generira lista pronađenih vrijednosti
	//var $listItems = valueList.children(); -ne može ovdje jer je statična, u početku i ma 0 elemenata i poslije se ne refresha sama
	var $current;

	hiddenInput.val('');
	valueList.empty();
	function fixWhich(e) {

  if (!e.which && e.button) {

    if (e.button & 1) e.which = 1      // Left

    else if (e.button & 4) e.which = 2 // Middle

    else if (e.button & 2) e.which = 3 // Right

  }
	
}
//dodavanje mouse button
	// textInput.mousedown( function(e) {
			// console.log("ASFDFSFD SDF SDF SDFSDDS FSFSDF DSF");
			// if (!e.which && e.button) {
			
			// if( e.button & 1 ) {
				
				// e.which = 1;
				// alert("LEFT MOUSE CLICK");
				// }
			// }
		
	// });
	
	
// 	micanje strelicama gore-dolje - ovo treba stavit kao opciju ukucavanju
	textInput.keydown( function(e){
		
		// gTimestamp - zadnji put kad je pozvana funkcija. 
		// Ako je razmak među pozivima manji od 100 ms onda se poziva automatski pa se to ignorira
		// Ovo je zato jer je npr. nakon 3 ukucana znaka funkcija pozvana 3 puta
		if ( ( Date.now() - gTimestamp ) > 100 ) {
			//console.log (Date.now());  

			var key = e.keyCode;
			var listItems = valueList.children();
			//console.log(listItems);
			var selectedListItem = listItems.filter('.selected'); //selectani je onaj s klasom "selected"
			//console.log(selectedListItem);
			var selectedListItem = selectedListItem.eq(0);
			
			
			if ( key == 40 ) // Down key
			{
				listItems.removeClass('selected');
				listItems.removeAttr('style');

				if ( ! selectedListItem.length || selectedListItem.is(':last-child') ) {
					$current = listItems.eq(2); // skače na treći red (prvi je X, drugi je prazan))
				}
				else {
					$current = selectedListItem.next();
				}
				$current.addClass('selected');
				$current.css('background', '#ccc');
			}
			else if ( key == 38 ) // Up key
			{
				listItems.removeClass('selected');
				listItems.removeAttr('style');
				
				//alert(listItems.index(selectedListItem));
				
				if ( ! selectedListItem.length || selectedListItem.is(':first-child') ) {
					$current = listItems.last();
				}
				
				//ako je na trećem elementu i stisnut je keyUp, vraća se na dno
				else if ( listItems.index(selectedListItem) == 2 ) {
					$current = listItems.last();
				}

				else {
					$current = selectedListItem.prev();
				}
				
				$current.addClass('selected');
				$current.css('background', '#ccc');
			} 
			else if ( key == 9 ) // Tab
			{
				//TAB služi za biranje tekućeg elementa	 IGOR OVO VATA
				
				//ako ne postoji niti jedna vrijednost, pritiskom taba
				//uzima vrijednost napisanu u inputu
                console.log("key = tab");
				if (p['keepTypedText'] == true){
                    if(!selectedListItem.attr('_value')){
					//alert('nema vrijednosti')
					hiddenInput.val($(this).val());
					textInput.val($(this).val());
					console.log(hiddenInput.val($(this).val()));
					}
				else
				{
					// alert('ima vrijednosti')
					hiddenInput.val(selectedListItem.attr('_value'));
					textInput.val(selectedListItem.attr('_show_input'));
					console.log(selectedListItem.attr('_value'));					
				}
				
				valueList.hide()
				
				textInput.trigger('change'); // radi quick save, jer se nakon oninput ignorira onchange
				//textInput.fireEvent("onchange"); // ovo ne radi
                }else{
                    hiddenInput.val(selectedListItem.attr('_value'));
					textInput.val(selectedListItem.attr('_show_input'));
                    valueList.hide();				
				    textInput.trigger('change');
                } 
				
			}
			else if ( key == 13 ) // Enter
			{
				//TAB služi za biranje tekućeg elementa	 IGOR OVO VATA
				
				//ako ne postoji niti jedna vrijednost, pritiskom taba
				//uzima vrijednost napisanu u inputu
                console.log("key = tab");
				if (p['keepTypedText'] == true){
                    if(!selectedListItem.attr('_value')){
					//alert('nema vrijednosti')
					hiddenInput.val($(this).val());
					textInput.val($(this).val());
					console.log(hiddenInput.val($(this).val()));
					}
				else
				{
					// alert('ima vrijednosti')
					hiddenInput.val(selectedListItem.attr('_value'));
					textInput.val(selectedListItem.attr('_show_input'));
					console.log(selectedListItem.attr('_value'));					
				}
				
				valueList.hide()
				
				textInput.trigger('change'); // radi quick save, jer se nakon oninput ignorira onchange
				//textInput.fireEvent("onchange"); // ovo ne radi
                }else{
                    hiddenInput.val(selectedListItem.attr('_value'));
					textInput.val(selectedListItem.attr('_show_input'));
                    valueList.hide();				
				    textInput.trigger('change');
                } 
				
			}
			
			
			else return; 
		}
		
		//nakon pozivanja onClick eventa bilježi se kad je pozvan (da se eliminira automatsko pozivanje)
		gTimestamp = Date.now(); 
	});
	
	
	if (textInput.val()) {
		var txtUrl = '/suva/ajax/ctlTextTypeV3' 
			+ '/' + p['modelName']  
			+ '/' + textInput.val()
			+ '/' + p['exprSearch']
			+ '/' + p['exprValue']
			+ '/' + p['exprShowList']
			+ '/' + p['exprShowInput']
			+ '/' + p['additionalWhere']
			+ '/' + p['nrReturnedRows'];
		//console.log( txtUrl );
 		if ( p['debug'] ) alert(txtUrl);
		$.ajax({
			async: false,
			url: txtUrl,
			cache: false,
			dataType: 'json'
		}).done(function(json_data) {
			console.log('received');
			//console.log('length:'.json_data.length);
			console.log(json_data);
			//console.log(JSON.stringify(json_data));
			if ( p['debug'] ) alert ('received ' + JSON.stringify(json_data));
			
			if (json_data){
				console.log(json_data.length);
				
				//DODAVANJE X-A NA VRHU
				var e = $('<div id=' + txtFieldName + '_close_list' + '><b>' + 'X' + '</b>&nbsp;&nbsp;' + textInput.val() + '</div>');
				e.css('float','right');
				e.click( function(){ valueList.hide(); } )
				valueList.append(e);
				
				var e = $('<br></br>'); //prazan red
				valueList.append(e);
				
				for ( var i = 0; i < json_data.length; i++ ) {
					var obj = json_data[i];
					
					//REDAK NA LISTI
					var listItem = $( "<div style=' display:block; width:auto;' id=" + txtFieldName + '__' + obj._value + '>' + obj._value + ' - ' + obj._show_list + '</div>' );
					
					//CLICK - dodavanje onclick eventa na list iteme  IGOR OVO NE VATA
					listItem.click(
						function() { 
							
							console.log(this.getAttribute('_value'));
							
							//hiddenInput.val(this.getAttribute('_value')); //IGOR OVDE BI TREBALO PREBACIT this.getAttribute('_value')
							textInput.val(this.getAttribute('_show_input'));
							
							//console.log($(this));
							console.log('listItem.onclick');
							//$('#' + this.getAttribute('id') ).onchange();
							//this.fireEvent("onchange");
							//ctlTextTypeV3_1(p);
							
							document.getElementById( hiddenInput.attr('id') ).value = this.getAttribute('_value') ;
							
							// console.log(hiddenInput.attr('id'))
							// ctlNumericUpdateField(hiddenInput.attr('id'));
							// console.log(hiddenInput)
							//alert($(this).html());
							//alert ( this.getAttribute('_value') );
							// console.log("hidden input:" + hiddenInput.val() );
							//debugger;
							
							console.log(p);
							valueList.empty();//IGOR ODVE SE BRIŠE LISTA NA CLICK
							//console.log('p onclick2 escfield' + p['pOnClick2']['escField']);
							var pEscField = p['pOnClick2']['escField']; 
							var pAjaxTable = p['pOnClick2']['ajax_table']; 
							var pAjaxId = p['pOnClick2']['ajax_id'];
							var pField = p['pOnClick2']['field'];
							var pEscFieldInput = p['pOnClick2']['escFieldInput'];
							console.log (pEscField);
							//ctlTextTypeV3_1(p['onClick2']['escField']['ajax_table']['ajax_id']['field']['escFieldInput']);
							//var params = p['onClick2']['escField']['ajax_table']['ajax_id']['field']['escFieldInput'];
							ctlNumericUpdateField( 
								pEscField, 
								pAjaxTable,
								pAjaxId,
								pField,
								pEscFieldInput
								); 
							
							
							
						}
					);
					
					listItem.attr( '_value' , obj._value);
					listItem.attr( '_show_input' , obj._show_input);
					listItem.attr( '_show_list' , obj._show_list);
					valueList.append(listItem);
					
				};
				valueList.show();
				 
				//Automatski selectanje prvog itema na listi
				var listItems = valueList.children();
				if (listItems.length > 2) //prvi red je X, drugi je prazan, u trećem redu je prvi tekst
				{
					listItems.removeClass('selected');
					listItems.removeAttr('style');
					var ctlFirstLI = listItems.eq(2);
					ctlFirstLI.addClass('selected');
					ctlFirstLI.css('background', '#ccc');
				}
			} else console.log('received null');
		}).fail(function (jqXHR, textStatus) {
			alert('jqXHR:' + jqXHR  + '  typeof:' + typeof(jqXHR) + '  textstatus:' + textStatus);
			console.log(jqXHR);
		});
	}
	else {
		//ako je vrijednost prazna (izbrisana), sve se postavlja na ''. 
		// U modelu se odlučuje što znači '' (null, 0 ili prazan string)
		hiddenInput.val('');
		textInput.val('');	
	}
};


function ctlFileUploadAjax(){
	//alert ('ctlFileUploadAjax');
	console.log("ctlPartials.ctlFileUploadAjax");
    $('#parameter_description_offline').maxlength({
        counterContainer: $('#parameter_description_offline_maxlength_label')
    });
    $('#refresh_ad_description_offline_btn').click(function(){
        $.post(
            '/admin/ads/refreshOfflineDescription',
            $('#parameter_description_offline').closest('form').serializeArray(),
            function(json) {
                if (json.status) {
                    $('#parameter_description_offline_avus_id').html(json.data.avus);
                    $('#parameter_description_offline').val(json.data.content).trigger('change');
                } else if ('undefined' !== typeof json.msg) {
                    alert(json.msg);
                }
            },
            'json'
        );
    });
    function uploadableParam_append(data, selector) {
        var html_builder = function(str) {
            var $html = $('<div/>').append(str);
            return $html.children();
        };

        $.each(data.media, function(idx, media){
            var $remover = html_builder('<div class="btn-group btn-group-sm"><span class="btn btn-danger btn-sm delete" title="Obriši"><span class="fa fa-trash-o text-danger"><!--IE--></span></span></div>');
            $remover.bind('click', function(){
                uploadableParam_delete($(this));
            });

            var $next = html_builder('<div class="btn-group btn-group-sm"><span class="btn btn-default btn-sm move-next" title="Pomakni desno"><span class="fa fa-caret-right"><!--IE--></span></span></div>');
            $next.bind('click', function(){
                uploadableParam_moveNext($(this));
            });

            var $prev = html_builder('<div class="btn-group btn-group-sm"><span class="btn btn-default btn-sm move-prev" title="Pomakni lijevo"><span class="fa fa-caret-left"><!--IE--></span></span></div>');
            $prev.bind('click', function(){
                uploadableParam_movePrev($(this));
            });

            var $main = html_builder('<div class="btn-group btn-group-sm"><span class="btn btn-default btn-sm set-as-main" title="Postavi kao glavnu"><span class="fa fa-thumb-tack"><!--IE--></span></span></div>');
            $main.bind('click', function(){
                uploadableParam_setAsMain($(this));
            });

            

            var $buttons = html_builder('<div class="uploadable-sorter-options btn-group btn-group-justified"></div>');
            $buttons.append($main).append($prev).append($next).append($remover);

            $('<li/>')
                .addClass('col-md-3 col-xs-6')
                .append(
                    $('<div/>')
                        .addClass('uploadable-sorter-box')
                        .attr('data-url', media.preview.src).data('url', media.preview.src)
                        .append(
                            $('<div/>')
                                .addClass('uploadable-sorter-box-item text-center')
                                .append(
                                    $('<img/>')
                                        .addClass('img-rounded uploadable-dragger ui-sortable-handle')
                                        .attr('src', media.preview.src)
                                )
                                .append(
                                    $('<input/>')
                                        .attr('type', 'hidden')
                                        .attr('name', $(selector).data('form') + '[]')
                                        .attr('value', media.id)
                                )
                        )
                        .append($buttons)
                        
                )
                .fadeIn('fast', function(){
                    $(this).appendTo(selector);
                    uploadableParam_modifyFirstLast();
                });
        });
    }
    function uploadableParam_setAsMain($sender) {
        $sender.closest('li').prependTo($sender.closest('ul'));
        uploadableParam_modifyFirstLast();
    }
    function uploadableParam_movePrev($sender) {
        var item = $sender.closest('li');
        if (item.length > 0) {
            var prev = item.prev();
            if (0 === prev.length) {
                return;
            }
            item.insertBefore(prev);
        }
        uploadableParam_modifyFirstLast();
    }
    function uploadableParam_moveNext($sender) {
        var item = $sender.closest('li');
        if (item.length > 0) {
            var next = item.next();
            if (0 === next.length) {
                return;
            }
            item.insertAfter(next);
        }
        uploadableParam_modifyFirstLast();
    }
    function uploadableParam_delete($sender) {
        $sender.closest('li').fadeOut('fast', function(){
            $container = $sender.closest('ul');
            $(this).remove();
            uploadableParam_modifyFirstLast();
        });
    }
    function uploadableParam_modifyFirstLast() {
        // disable the first's .set-as-main and .move-left buttons
        $('.uploadable-sorter li:first').find('.set-as-main, .move-prev').addClass('disabled');
        // disable .move-next too if we only have one sortable element
        var cnt = $('.uploadable-sorter li').length;
        if (cnt <= 1) {
            $('.uploadable-sorter li:first').find('.move-next').addClass('disabled');
        } else {
            // more than one available, enable those buttons now even on the first one
            $('.uploadable-sorter li:first').find('.move-next').removeClass('disabled');
        }
        // enable all those potentially disabled buttons on all others except the first one
        $('.uploadable-sorter li').not(':eq(0)').find('.set-as-main, .move-prev, .move-next').removeClass('disabled');

        // disable next button on last one
        $('.uploadable-sorter li:last').find('.move-next').addClass('disabled');
    }
    $('#parameter_uploadable').fileupload({
        url: '/ajax/upload',
        dataType: 'json',
        dropZone: null,
        pasteZone: null,
        limitMultiFileUploads: 5,
        // acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        acceptFileTypes: /(\.|\/)(jpe?g|png)$/i,
        done: function (e, data) {
            if ('undefined' !== data.result) {
                if (data.result.ok) {
                    uploadableParam_append(data.result, '#parameter_uploadable_gallery');
                    //$('<p/>').text(data.result.picture.filename).appendTo('#parameter_uploadable_gallery');
                } else {
                    // TODO: handle failure in some way...
                }
            }
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#parameter_uploadable_progress .progress-bar').css(
                'width',
                progress + '%'
            );
        },
        start: function(e) {
            $('#parameter_uploadable_progress').removeClass('hidden');
        },
        stop: function(e) {
            $('#parameter_uploadable_progress').addClass('hidden');
        }
    }).prop('disabled', !$.support.fileInput)
      .parent().addClass($.support.fileInput ? undefined : 'disabled');

    $('#parameter_uploadable_gallery').sortable({
        forcePlaceholderSize: true,
        handle: '.uploadable-dragger',
        helper: 'clone',
        listType: 'ul',
        items: 'li',
        opacity: .6,
        placeholder: 'col-md-3 col-lg-3 placeholder',
        revert: 250,
        tabSize: 25,
        create: function(event, ui) {
            // initialize set-as-main buttons
            $('.uploadable-sorter .uploadable-sorter-options .btn.set-as-main').click(function(){
                uploadableParam_setAsMain($(this));
            });
            // initialize move-prev buttons
            $('.uploadable-sorter .uploadable-sorter-options .btn.move-prev').click(function(){
                uploadableParam_movePrev($(this));
            });
            // initialize move-next buttons
            $('.uploadable-sorter .uploadable-sorter-options .btn.move-next').click(function(){
                uploadableParam_moveNext($(this));
            });
            // initialize delete buttons
            $('.uploadable-sorter .uploadable-sorter-options .btn.delete').click(function(){
                uploadableParam_delete($(this));
            });
        },
        stop: function(event, ui) {
            uploadableParam_modifyFirstLast();
        }
    });    promise_parameter_location = [];

    function parameter_location_handle_needed_levels(last_needed_level) {
        var max_location_level = 4;
        for (var i = 1; i <= max_location_level; i++) {
            var $current_level_select = $('#parameter_location_level_' + i);
            if ('undefined' !== typeof $current_level_select && 1 === $current_level_select.length) {
                var current_level_type = $current_level_select.data('type');
                var $current_level_form_group = $current_level_select.closest('.form-group');
                if (i <= last_needed_level) {
                    $current_level_form_group.show();
                    $current_level_select.prop('disabled', false)
                    if (current_level_type !== 'point') {
                        $current_level_select.selectpicker('refresh');
                    }
                } else {
                    $current_level_form_group.hide();
                    if (current_level_type !== 'point') {
                        $current_level_select.prop('disabled', true).selectpicker('refresh');
                    }
                }
            }
        }
    }

    function parameter_location_onChange($source, $target) {
        if ('undefined' != typeof $source.val()) {
            var last_location_needed_level = parseInt($source.attr('id').replace('parameter_location_level_', ''));
            if ('' != $.trim($source.val())) {
                var $selected_option = $source.find(':selected');
                if ($target.data('type') !== 'point') {
                    promise_parameter_location[$source.attr('id')] = $.ajax({
                        url: '/ajax/location/' + $source.val() + '/values',
                        dataType: 'json',
                        success: function(data) {
                            $target.find('option').remove();
                            if (data.length) {
                                last_location_needed_level++;
                                $target.append(
                                    $('<option/>').attr('value', '').text('')
                                );
                                $.each(data, function(idx, data){
                                    $target.append(
                                        $('<option/>').attr('value', data.id).text(data.name).data('lat', data.lat).data('lng', data.lng).data('zoom', data.zoom)
                                    );
                                });
                                $target.selectpicker('refresh');
                            }
                            parameter_location_handle_needed_levels(last_location_needed_level);
                        }
                    });
                } else {
                    last_location_needed_level++;
                    parameter_location_handle_needed_levels(last_location_needed_level);
                    var $google_map_container = $('#' + $target.attr('id') + '_map');
                    if ('undefined' !== typeof $google_map_container) {
                        $google_map_container.attr('data-lat', $selected_option.data('lat')).data('lat', $selected_option.data('lat'));
                        $google_map_container.attr('data-lng', $selected_option.data('lng')).data('lng', $selected_option.data('lng'));
                        $google_map_container.attr('data-zoom', $selected_option.data('zoom')).data('zoom', $selected_option.data('zoom'));
                        $target.closest('.form-group').show('fast', function(){
                            parameter_location_initMap();
                        });
                    }
                }
                if ('undefined' !== typeof parameter_location_map && parameter_location_map) {
                    parameter_location_setCoords($selected_option.data('lat'), $selected_option.data('lng'));
                    parameter_location_map.changeView(
                        $selected_option.data('lat'),
                        $selected_option.data('lng'),
                        $selected_option.data('zoom')
                    );
                }
            } else {
                $target.selectpicker('refresh');
                parameter_location_handle_needed_levels(last_location_needed_level);
            }
        } else {
            $target.selectpicker('refresh');
        }
    }
    $('#parameter_location_level_1').change(function(){
        parameter_location_onChange($(this), $('#parameter_location_level_2'));
    });
    $('#parameter_location_level_2').change(function(){
        parameter_location_onChange($(this), $('#parameter_location_level_3'));
    });
    $('#parameter_location_level_3').change(function(){
        parameter_location_onChange($(this), $('#parameter_location_level_4'));
    });
    $('#parameter_location_level_2, #parameter_location_level_3, #parameter_location_level_4').selectpicker({
        style: 'btn btn-default',
        size: 8,
        noneSelectedText: '',
        liveSearch: true,
        noneResultsText: '',
        mobile: OGL.is_mobile_browser,
        iconBase: 'fa',
        tickIcon: 'fa-check text-primary'
    });
    $('#parameter_price_input').autoNumeric({aSep:'.', aDec:',', mDec:'0', lZero:'deny', vMax: '99999999999999.99'});
    $('#parameter_price_input').change(function(){
        $('#parameter_price').val($('#parameter_price_input').autoNumeric('get'));
    });
    $('#parameter_price').val($('#parameter_price_input').autoNumeric('get'));
    $('#parameter_6_preview_btn').click(function(){
        var preview_url = $.trim($('#parameter_6').val());
        if (preview_url) {
            $.post(
                '/ajax/check-video-url', {
                    'url': preview_url
                },
                function (json) {
                    if (json.status) {
                        preview_url = json.url;
                    }
                    $.fancybox({
                        type: 'iframe',
                        href: preview_url
                    });
                },
                'json'
            );
        }
    });
}



