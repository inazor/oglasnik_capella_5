$(function () {
    // Prevent double form submissions
    jQuery('form:not(".js-allow-double-submit")').preventDoubleSubmit();
    // Show tooltips
    jQuery('[data-toggle="tooltip"][data-type!="html"]').tooltip();
    jQuery('[data-toggle="tooltip"][data-type="html"]').tooltip({html:true});
});
