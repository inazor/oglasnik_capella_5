var hasChanges = false;
$(document).ready(function(){
    $('#url').generateSlug('#name');

    $('#category_ids').selectpicker({
        style: 'btn btn-default',
        size: 8,
        liveSearch: true,
        selectedTextFormat: 'count>3',
        title: 'Select categories',
        noneSelectedText: 'Select categories',
        noneResultsText: 'No categories match',
        mobile: OGL.is_mobile_browser
    });

    $('#location').selectpicker({
        style: 'btn btn-default',
        mobile: OGL.is_mobile_browser
    });

    $('input[type=text],select,textarea').change(function(){
        hasChanges = true;
    });
});
