// TODO: create some sort of console logger (in case console does not exist, all errors/info/logs should be alerted somehow)
//----------------------------------------------------------------------------------------------------------------------
// TODO: implement geolocation for location parameter (visible only if all levels are added)
// TODO: remember last used fieldset (when adding new parameter!)

var category_fieldsets_and_parameters_initial = null;
var category_fieldsets_and_parameters_final = {
    settings: {
        ad_desc_tpl: '',
        offline_desc_tpl: ''
    },
    filters: [],
    fieldsets: []
};
var category_fieldsets_and_parameters_has_changes = false;
var parametrize_multiple_categories = false;
var currently_selected_parameter_type = null;
var default_title_max_length = 80;

//----------------------------------------------------------------------------------------------------------------------
// COMMON METHODS - START
//----------------------------------------------------------------------------------------------------------------------

/**
 * Drag'n'Drop reorder of fieldsets
 * @param {object} $sender Main fieldset container
 */
function drag_n_drop_reorder_fields($sender) {
    $sender.sortable({
        forcePlaceholderSize: true,
        handle: '.panel-heading-handle',
        helper: 'clone',
        containment: '#fieldset_container',
        axis: 'y',
        items: '> li',
        opacity: .6,
        placeholder: 'placeholder',
        revert: 250,
        tabSize: 25,
        stop: function(event, ui) {
            refresh_category_fieldsets_and_parameters();
        }
    });
}

/**
 * Drag'n'Drop reorder of parameters within fieldset
 * @param {object} $sender Main parameters list container
 */
function drag_n_drop_reorder_parameters($sender) {
    $sender.sortable({
        forcePlaceholderSize: true,
        handle: '.drag-me',
        helper: 'clone',
        containment: '#fieldset_sorter',
        axis: 'y',
        items: '> li',
        opacity: .6,
        placeholder: 'placeholder',
        connectWith: '#fieldset_sorter .list-sortable ul.sortable',
        revert: 250,
        tabSize: 25,
        stop: function(event, ui) {
            refresh_category_fieldsets_and_parameters();
        }
    });
}

/**
 * Drag'n'Drop reorder of filter
 */
function drag_n_drop_reorder_filters($sender) {
    $sender.sortable({
        forcePlaceholderSize: true,
        handle: '.drag-me',
        helper: 'clone',
        containment: '#reorderFiltersModal .modal-body',
        axis: 'y',
        items: '> li',
        opacity: .6,
        placeholder: 'placeholder',
        revert: 250,
        tabSize: 25
    });
}

function is_parameters_slug_unique(parameter_slug, selected_parameter_id) {
    // there are two possibilities that parameter slug is unique:
    // 1. if parameter_slug is found, selected_parameter_id should be the same to the one that is found next to parameter_slug
    // 2. Obviously, if parameter_slug is not found :)

    var validation_result = true;

    $.each(category_fieldsets_and_parameters_final.fieldsets, function(i, fieldset) {
        if ('undefined' !== typeof fieldset.parameters[parameter_slug]) {
            // match found.. we need to make sure that selected_parameter_id is the same as the one found here...
            var found_parameter = fieldset.parameters[parameter_slug];
            if (parseInt(found_parameter.id) != parseInt(selected_parameter_id)) {
                validation_result = false;
            }
        }
    });

    return validation_result;
}

/**
 * Rebuild category_fieldsets_and_parameters_final object with latest changes
 * @return {bool|string} Is everything's ok? - In case string is returned, it's a message to display in alert.
 */
function refresh_category_fieldsets_and_parameters() {
    var return_value = true;
    category_fieldsets_and_parameters_final.fieldsets = [];

    var curr_fieldset_id = 1;
    var curr_parameter_index = 1;
    $('#fieldset_sorter > li').each(function(f){
        var $curr_fielset = $(this);
        $(this).attr('data-fieldset-id', curr_fieldset_id).data('fieldset-id', curr_fieldset_id);
        if ($(this).find('.list-sortable li').length > 0) {
            var current_fieldset_parameters = {};
            $(this).find('.list-sortable li').each(function(p){
                $(this).data('parameter-attributes', $(this).attr('data-parameter-attributes'));
                var current_parameter_attributes = JSON.parse($(this).data('parameter-attributes'));
                if ('undefined' !== typeof $(this).data('parameter-slug') && '' !== $.trim($(this).data('parameter-slug'))) {
                    parameter_slug = $.trim($(this).data('parameter-slug'));
                } else {
                    parameter_slug = 'parameter_' + curr_parameter_index;
                }
                current_fieldset_parameters[parameter_slug] = current_parameter_attributes;
                //current_fieldset_parameters.push(current_parameter_attributes);

                curr_parameter_index++;
            });
            var current_fieldset = {
                sort_order: (f+1),
                label: $.trim($(this).find('.panel-heading input[type=text]').val()),
                parameters: current_fieldset_parameters
            };
            if ('undefined' !== typeof $curr_fielset.data('settings') && $.trim($curr_fielset.data('settings'))) {
                if ('object' === typeof $curr_fielset.data('settings')) {
                    current_fieldset.settings = $curr_fielset.data('settings');
                } else {
                    current_fieldset.settings = JSON.parse($curr_fielset.data('settings'));
                }
            }
            category_fieldsets_and_parameters_final.fieldsets.push(current_fieldset);
        }

        curr_fieldset_id++;
    });

    if ($('#fieldset_sorter > li').length == 1) {
        $('#fieldset_sorter .panel-heading .btn-fieldset-delete').addClass('disabled');
    } else {
        $('#fieldset_sorter .panel-heading .btn-fieldset-delete').removeClass('disabled');
    }

    if (null === category_fieldsets_and_parameters_initial) {
        // adDescTpl_value is a global var in category-parameterization-edit-adDescTpl.js file
        category_fieldsets_and_parameters_final.settings.ad_desc_tpl = adDescTpl_value;
        if (adDescTpl_getSelectedParameters(true)) {
            return_value = '[Ad Description Template]\n\n' + adDescTpl_errorMsg;
        }
        // offlineDescTpl_value is a global var in category-parameterization-edit-adDescTpl.js file
        category_fieldsets_and_parameters_final.settings.offline_desc_tpl = offlineDescTpl_value;
        if (offlineDescTpl_getSelectedParameters(true)) {
            return_value = '[Offline Description Template]\n\n' + offlineDescTpl_errorMsg;
        }
        refresh_category_filters(false, false);
        category_fieldsets_and_parameters_initial = JSON.parse(JSON.stringify(category_fieldsets_and_parameters_final));
        category_fieldsets_and_parameters_has_changes = false;

        // also, disable all used parameters...
        refresh_available_parameters_dropdown();
    }

    check_category_fieldsets_and_parameters_changes();

    return return_value;
}

/**
 * Helper method to refresh available parameters (hide those that are used)
 */
function refresh_available_parameters_dropdown() {
    $('.list-sortable li.sortable_li').each(function(index){
        var parameter_id_to_disable = $(this).data('parameter-id');
        if (parameter_id_to_disable) {
            $('#parameter_id option[value="' + parameter_id_to_disable + '"]').prop('disabled', true).hide();
        }
    });

    // return number of parameters that are still available (enabled)...
    var still_available_count = $('#parameter_id option:enabled').size();
    return still_available_count;
}

/**
 * Check if there are any changes that can be saved
 * @return {boolean}
 */
function check_category_fieldsets_and_parameters_changes() {
    category_fieldsets_and_parameters_has_changes = (JSON.stringify(category_fieldsets_and_parameters_initial) != JSON.stringify(category_fieldsets_and_parameters_final));
    parametrize_multiple_categories = $.trim($('#current_category_id').val()) !== $.trim($('#chosen_categories').val());

    var $btn_save_cat_params = $('#btnSaveCategoryParameters');
    if (category_fieldsets_and_parameters_has_changes || parametrize_multiple_categories) {
        if (!$btn_save_cat_params.is(':visible')) {
            $btn_save_cat_params.fadeIn('fast');
        }
    } else {
        if ($btn_save_cat_params.is(':visible')) {
            $btn_save_cat_params.fadeOut('fast');
        }
    }
}

/**
 * Decide whether we're working with dependable type of parameter
 * @param {object} $chosen_param  Selected <option> from Parameters dropdown menu
 * @return {boolean}
 */
function selected_parameter_is_dependable($chosen_param) {
    if (
        parseInt($chosen_param.data('is-dependable')) &&
        parseInt($chosen_param.data('accept-dictionary')) &&
        parseInt($chosen_param.data('dictionary-id')) &&
        parseInt($chosen_param.data('max-level'))
    ) {
        return true;
    }
    return false;
}

/**
 * Decide whether we're working with location (which is dependable) type of parameter
 * @param {object} $chosen_param  Selected <option> from Parameters dropdown menu
 * @return {boolean}
 */
function selected_parameter_is_location($chosen_param) {
    if (
        parseInt($chosen_param.data('is-dependable')) &&
        $.trim($chosen_param.data('type-id')) == 'LOCATION' &&
        parseInt($chosen_param.data('max-level'))
    ) {
        return true;
    }
    return false;
}

/**
 * Handle toggling of is_searchable container
 * @param  {object} $sender
 */
function is_searchable_toggle($sender) {
    var $is_searchable_container = $('#' + $sender.data('container'));
    if ($is_searchable_container.length > 0) {

        var is_protected = $sender.data('protected');

        // If protected and not checked, set back to checked state
        if (is_protected && !$sender.prop('checked')) {
            $sender.prop('checked', true);
        }

        // simple show/hide based on checked state
        if ($sender.prop('checked')) {
            $is_searchable_container.removeClass('hidden');
        } else {
            $is_searchable_container.addClass('hidden');
        }
    }
}

/**
 * Helper method to copy and preset available is_searchable types
 * @param {object} $sender         Dropdown where to build options
 * @param {object} $chosen_param   Currently selected parameter from where to see available options
 * @param {object} is_searchable_options
 */
function set_available_is_searchable_types($sender, $chosen_param, is_searchable_options) {
    if ('undefined' !== typeof $sender) {
        if ('undefined' === typeof $chosen_param) {
            $chosen_param = get_selected_parameter_dropdown(':selected');
        }

        if ('attribute_is_searchable_type' !== $sender.attr('id')) {
            // copy options from '#attribute_is_searchable_type' to this sender
            if ($sender.find('option').length > 0) {
                $sender.find('option').remove();
            }
            $sender.append($('#attribute_is_searchable_type option').clone());
        }

        var $sender_options = $sender.find('option');
        $sender_options.addClass('hidden');
        var available_types = $.trim($chosen_param.data('is-searchable-type')) ? $.trim($chosen_param.data('is-searchable-type')) : null;
        if (available_types) {
            available_types = available_types.split(',');
            for (var type_index in available_types) {
                if (available_types.hasOwnProperty(type_index)) {
                    $sender.find('option[value="' + $.trim(available_types[type_index]) + '"]').removeClass('hidden');
                }
            }
        }
        // preselect is_searchable details
        set_is_searchable_type($sender, is_searchable_options);
    }
}

function set_is_searchable_type($sender, is_searchable_options) {
    if ($sender.length > 0) {
        is_searchable_settings = {
            'label_text': '',
            'type'      : $sender.find('option:not(.hidden):eq(0)').val()
        };

        if ('undefined' !== typeof is_searchable_options && is_searchable_options) {
            if ('undefined' !== typeof is_searchable_options.label_text) {
                is_searchable_settings.label_text = is_searchable_options.label_text;
            }
            if ('undefined' !== typeof is_searchable_options.type) {
                is_searchable_settings.type = is_searchable_options.type;
            } else {
                is_searchable_settings.type = is_searchable_options;
            }
        }

        $sender.closest('.row').find('input').val(is_searchable_settings.label_text);
        $sender.find('option[value="' + is_searchable_settings.type + '"]').prop('selected', true);
        $sender.selectpicker('val', is_searchable_settings.type);
        $sender.selectpicker('refresh');
    }
}

function get_selected_parameter_dropdown(selector) {
    var $dropdown = $('#parameter_id');

    if ('undefined' !== typeof selector) {
        $dropdown = $dropdown.find(selector);
    }

    return $dropdown;
}


//----------------------------------------------------------------------------------------------------------------------
// COMMON METHODS - END
//----------------------------------------------------------------------------------------------------------------------
// REORDER FILTERS RELATED METHODS - START
//----------------------------------------------------------------------------------------------------------------------

function show_reorder_filters_dialog() {
    $('#filter_sorter li').remove();
    $('#fieldset_sorter .list-sortable li span.fa-search').sort(function(a, b){
        var $a_li = $(a).closest('li.sortable_li');
        var $b_li = $(b).closest('li.sortable_li');
        if ($a_li.length > 0 && $b_li.length > 0) {
            return parseInt($a_li.data('filters-sort-order')) - parseInt($b_li.data('filters-sort-order'));
        }
    }).each(function(i, elem){
        var $li = $(elem).closest('li.sortable_li');
        if ($li.length > 0) {
            // construct and add html chunk to modal dialog
            $('#filter_sorter').append(
                $('<li/>')
                    .addClass('sortable_li')
                    .attr('id', 'filter_' + (i + 1))
                    .attr('data-parameter-id', $li.data('parameter-id')).data('parameter-id', $li.data('parameter-id'))
                    .append(
                        $('<div/>')
                            .addClass('sortable_div')
                            .append(
                                $('<div/>')
                                    .addClass('sortable_row')
                                    .append(
                                        $('<div/>')
                                            .addClass('drag-me')
                                            .append(
                                                $('<span/>')
                                                    .addClass('fa fa-arrows fa-fw')
                                                    .html('<!--IE-->')
                                            )
                                    )
                                    .append(
                                        $('<div/>')
                                            .addClass('name-cat')
                                            .append(
                                                $('<span/>')
                                                    .addClass('name')
                                                    .text($li.data('parameter-name'))
                                            )
                                    )
                            )
                    )
            );
        }
    });
    $('#reorderFiltersModal').modal({
        backdrop: 'static',
        keyboard: true
    });
}

function refresh_category_filters(click_from_dialog, check_for_changes) {
    if ('undefined' === typeof click_from_dialog) {
        click_from_dialog = false;
    }
    if ('undefined' === typeof check_for_changes) {
        check_for_changes = true;
    }

    category_fieldsets_and_parameters_final.filters = [];
    if (click_from_dialog) {
        $('#filter_sorter > li').each(function(f){
            var $curr_parameter_filter = $(this);
            var $fieldset_parameter = $('#fieldset_sorter .list-sortable li[data-parameter-id="' + $curr_parameter_filter.data('parameter-id') + '"]');
            if ($fieldset_parameter.length > 0) {
                $fieldset_parameter.attr('data-filters-sort-order', (f + 1)).data('filters-sort-order', (f + 1));
                category_fieldsets_and_parameters_final.filters.push($curr_parameter_filter.data('parameter-id'));
            }
        });
    } else {
        $('#fieldset_sorter .list-sortable li span.fa-search').sort(function(a, b){
            var $a_li = $(a).closest('li.sortable_li');
            var $b_li = $(b).closest('li.sortable_li');
            if ($a_li.length > 0 && $b_li.length > 0) {
                return parseInt($a_li.data('filters-sort-order')) - parseInt($b_li.data('filters-sort-order'));
            }
        }).each(function(i, elem){
            var $li = $(elem).closest('li.sortable_li');
            if ($li.length > 0) {
                $li.attr('data-filters-sort-order', (i + 1)).data('filters-sort-order', (i + 1));
                // add parameter to filters sort
                category_fieldsets_and_parameters_final.filters.push($li.data('parameter-id'));
            }
        });
    }
    if (check_for_changes) {
        check_category_fieldsets_and_parameters_changes();
    }
}

//----------------------------------------------------------------------------------------------------------------------
// REORDER FILTERS RELATED METHODS - END
//----------------------------------------------------------------------------------------------------------------------
// FIELDSET RELATED METHODS - START
//----------------------------------------------------------------------------------------------------------------------

/**
 * Populates fieldset select on the parameter attributes dialog
 * @param {undefined|int} curr_fieldset_index Current fieldset's index or none
 */
function populate_fieldset_id_select(curr_fieldset_index) {
    // clear current select options - we'll generate updated list...
    var $fieldset = $('#fieldset_id');
    $fieldset.find('option').remove();

    var empty_fieldset_exists = true;
    var $fieldset_sorter_items = $('#fieldset_sorter').find('> li');
    if ($fieldset_sorter_items.length > 0) {
        empty_fieldset_exists = false;
        $fieldset_sorter_items.each(function(f){
            var curr_fieldset_label = $.trim($(this).find('.panel-heading input.form-control').val());
            if ($(this).find('li').length == 0) {
                empty_fieldset_exists = true;
            }
            $fieldset.append(
                $('<option/>')
                    .attr('value', (f+1))
                    .text((f+1) + '. - ' + (curr_fieldset_label != '' ? curr_fieldset_label : 'Unnamed fieldset'))
            );
        });
    }

    if (false === empty_fieldset_exists) {
        $fieldset.append($('<option/>').attr('value', 'createNew').text('Create new fieldset'));
    }

    // preselect something (curr_fieldset_index or first item)
    if ('undefined' === typeof curr_fieldset_index) {
        curr_fieldset_index = 1;
    }
    // refresh the selectpicker
    $fieldset.selectpicker('refresh');
    $fieldset.selectpicker('val', curr_fieldset_index);
}

/**
 * Add new fieldset
 * @param {undefined|function} callback_function CallBack function that should be invoked upon successful fieldset creation
 */
function add_new_fieldset(callback_function) {

    var new_fieldset_id = $('#fieldset_sorter > li').length + 1;

    var $li = $('<li/>')
        .addClass('fieldset')
        .attr('data-fieldset-id', new_fieldset_id).data('fieldset-id', new_fieldset_id)
        .append(
            $('<div/>')
                .addClass('panel panel-info')
                .append(
                    $('<div/>')
                        .addClass('panel-heading')
                        .append(
                            $('<div/>')
                                .addClass('row')
                                .append(
                                    $('<div/>')
                                        .addClass('col-md-12 col-lg-12')
                                        .append(
                                            $('<div/>')
                                                .addClass('panel-heading-handle')
                                                .append(
                                                    $('<span/>')
                                                        .addClass('fa fa-arrows fa-fw')
                                                )
                                        )
                                        .append(
                                            $('<div/>')
                                                .append(
                                                    $('<div/>')
                                                        .addClass('input-group')
                                                        .append(
                                                            $('<input/>')
                                                                .addClass('form-control')
                                                                .attr('type', 'text')
                                                                .attr('placeholder', 'Fieldset label')
                                                                .on('change', function(){
                                                                    refresh_category_fieldsets_and_parameters();
                                                                })
                                                        )
                                                        .append(
                                                            $('<span/>')
                                                                .addClass('input-group-btn')
                                                                .append(
                                                                    $('<button/>')
                                                                        .addClass('btn btn-primary btn-fieldset-settings')
                                                                        .attr('type', 'button')
                                                                        .append($('<span/>').addClass('fa fa-cog fa-fw'))
                                                                        .append(document.createTextNode(' Settings'))
                                                                        .on('click', function(){
                                                                            fieldset_settings_button_click($(this));
                                                                        })
                                                                )
                                                                .append(
                                                                    $('<button/>')
                                                                        .addClass('btn btn-danger btn-fieldset-delete')
                                                                        .attr('type', 'button')
                                                                        .append($('<span/>').addClass('fa fa-trash-o fa-fw'))
                                                                        .on('click', function(){
                                                                            delete_fieldset_button_click($(this));
                                                                        })
                                                                )
                                                        )
                                                )
                                        )
                                )
                        )
                )
                .append(
                    $('<div/>')
                        .addClass('panel-body')
                        .append(
                            $('<div/>')
                                .addClass('row')
                                .append(
                                    $('<div/>')
                                        .addClass('col-md-12 col-lg-12')
                                        .append(
                                            $('<div/>')
                                                .addClass('list-sortable')
                                                .append(
                                                    $('<ul/>')
                                                        .addClass('sortable ui-sortable')

                                                )
                                        )
                                )
                        )
                )
        );

    drag_n_drop_reorder_parameters($li.find('.list-sortable ul'));
    $('#fieldset_sorter').append(
        $li.fadeIn('fast', function(){
            drag_n_drop_reorder_parameters($(this));
            if ('function' === typeof callback_function) {
                callback_function();
            }
        })
    );
}

/**
 * Delete fieldset button click
 * @param {object} $sender Delete button of fieldset we want to delete
 */
function delete_fieldset_button_click($sender) {
    $('#deleteFieldsetModalYesBtn').attr('data-fieldset-id', '').data('fieldset-id', '');

    var $current_fieldset = $sender.closest('li.fieldset');

    if ($current_fieldset.length > 0) {
        $('#deleteFieldsetModalYesBtn').attr('data-fieldset-id', $current_fieldset.data('fieldset-id')).data('fieldset-id', $current_fieldset.data('fieldset-id'));

        // count how many parameters are currently in this field.
        var $found_parameters_in_current_fieldset = $current_fieldset.find('.list-sortable ul.sortable li');
        var number_of_parameters = $found_parameters_in_current_fieldset.length;

        if ($found_parameters_in_current_fieldset && number_of_parameters > 0) {
            var fieldset_name = $.trim($current_fieldset.find('.panel-heading .input-group input.form-control').val());
            $('#deleteFieldsetModalLabel span').text(fieldset_name);

            if (number_of_parameters == 1) {
                var fieldset_parameters_string = $found_parameters_in_current_fieldset.data('parameter-name');
                var fieldset_delete_warning_string = '<p>You are about to delete a fieldset with parameter "' + fieldset_parameters_string + '". Do you want to continue?</p>';
            } else {
                var fieldset_parameters_array = [];
                $found_parameters_in_current_fieldset.each(function(i){
                    fieldset_parameters_array.push($(this).data('parameter-name'));
                });
                var fieldset_parameters_string = fieldset_parameters_array.join('</li><li>');

                var fieldset_delete_warning_string = '<p>You are about to delete a fieldset with ' + number_of_parameters + ' parameters.</p> \
                <ul><li>' + fieldset_parameters_string + '</li></ul> \
                <p>Do you want to continue?</p>';
            }

            $('#deleteFieldsetModalBody').html(fieldset_delete_warning_string);

            $('#deleteFieldsetModal').modal({
                backdrop: 'static',
                keyboard: true
            });
        } else {
            // there are no parameters in this section so we can delete it without any user confirmation...
            delete_fieldset($current_fieldset.data('fieldset-id'));
        }
    }
}

/**
 * Delete fieldset method
 * @param  {int} delete_fieldset_id fieldset_id to delete
 */
function delete_fieldset(delete_fieldset_id) {
    if ('undefined' === typeof delete_fieldset_id) {
        delete_fieldset_id = parseInt($('#deleteFieldsetModalYesBtn').data('fieldset-id'));
    } else {
        delete_fieldset_id = parseInt(delete_fieldset_id);
    }

    if (delete_fieldset_id) {
        var $fieldset_container = $('#fieldset_sorter li.fieldset[data-fieldset-id="' + delete_fieldset_id + '"]')
        if ($fieldset_container.length > 0) {
            $fieldset_container.fadeOut('fast', function(){
                // enable all parameters that were in this fieldset so we can select them again
                $fieldset_container.find('.list-sortable li.sortable_li').each(function(index){
                    var parameter_id_to_enable = $(this).data('parameter-id');
                    if ('undefined' !== typeof parameter_id_to_enable) {
                        $('#parameter_id option[value="' + parameter_id_to_enable + '"]').prop('disabled', false).show();
                    }
                });

                // finally, delete the fieldset
                $fieldset_container.remove();

                // and refresh js object where we keep current category parameterization data
                refresh_category_fieldsets_and_parameters();
            });
        }
    }
}

/**
 * Change various fieldset settings
 * @param  {object} $sender Fieldset object
 */
function fieldset_settings_button_click($sender) {
    fieldset_load_settings_to_dialog($sender.closest('li.fieldset'));

    $('#fieldsetSettingsModal').modal({
        backdrop: 'static',
        keyboard: true
    });
}

function fieldset_load_settings_to_dialog($selected_fieldset) {
    $('#fieldsetSettingsModal')
        .attr('data-fieldset-id', parseInt($selected_fieldset.data('fieldset-id')))
        .data('fieldset-id', parseInt($selected_fieldset.data('fieldset-id')));

    var fieldset_settings;
    if ('undefined' !== typeof $selected_fieldset.data('settings') && $selected_fieldset.data('settings')) {
        fieldset_settings = $selected_fieldset.data('settings');
    } else {
        fieldset_settings = {
            style: 'group'
        };
    }

    var $settings_style = $('#fieldset_settings_style_' + fieldset_settings.style);
    if ($settings_style.length > 0) {
        $settings_style.prop('checked', true);
        var $zebra = $('#fieldset_settings_style_zebra');
        var $group = $('#fieldset_settings_style_group');
        if (1 == parseInt($selected_fieldset.data('fieldset-id'))) {
            $zebra.prop('disabled', false);
        } else {
            if ($zebra.prop('checked')) {
                $zebra.prop('checked', false);
                $group.prop('checked', true);
            }
            $zebra.prop('disabled', true);
        }
    }
}

function fieldset_save_settings_from_dialog() {
    var selected_fieldset_id = null;
    var $fieldset_settings_modal = $('#fieldsetSettingsModal');
    if ('undefined' !== typeof $fieldset_settings_modal.data('fieldset-id') && parseInt($fieldset_settings_modal.data('fieldset-id'))) {
        selected_fieldset_id = parseInt($fieldset_settings_modal.data('fieldset-id'));
    }

    if (selected_fieldset_id) {
        var $selected_fieldset = $('li.fieldset[data-fieldset-id="' + selected_fieldset_id + '"]');
        if ($selected_fieldset.length > 0) {
            var fieldset_settings = {};
            var fieldset_settings_size = 0;

            var settings_style_selected = $fieldset_settings_modal.find('input[name="fieldset_settings_style"]:checked');
            if (settings_style_selected.length > 0 && $.trim(settings_style_selected.val())) {
                fieldset_settings.style = settings_style_selected.val();
                fieldset_settings_size++;
            }

            if (fieldset_settings_size) {
                $selected_fieldset
                    .attr('data-settings', JSON.stringify(fieldset_settings))
                    .data('settings', JSON.stringify(fieldset_settings));
            } else {
                $selected_fieldset.attr('data-settings', '').data('settings', '');
            }
            refresh_category_fieldsets_and_parameters();
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------
// FIELDSET RELATED METHODS - END
//----------------------------------------------------------------------------------------------------------------------
// PARAMETER DIALOG & REORDER RELATED METHODS - START
//----------------------------------------------------------------------------------------------------------------------

var parameter_dialog_is_visible = false;

/**
 * Handle changes on the 'Parameter Dialog' based on the currently selected parameter type
 * @param {object} $chosen_parameter Currently selected parameter
 */
function handle_parameter_select_change($chosen_parameter) {
    reset_parameter_attributes_dialog();

    // we have two group of parameters - standard and custom. Standard parameters are those parameters that every ad has
    // (title, price, description, location) while custom are specific to every category. As parameters are grouped and
    // they have different rules how they behave and how they are presented, first thing we have to find out is their group.

    var parameter_group = $chosen_parameter.closest('optgroup').data('group');
    var parameter_type_id = $chosen_parameter.data('type-id');
    currently_selected_parameter_type = parameter_type_id;

    // here we should collect all the info about this specific parameter and display/hide (enable/disable) parameter's
    // specific fields that can be controlled by administrator/moderator

    var $panel_title = $('#parameterOptionsModalBody h5.text-info');

    // Hiding all parameter-specific panels by default if they're not hidden already
    $('.parameter-options-panel').each(function(idx){
        var panel = $(this);
        if (!panel.hasClass('hidden')) {
            panel.addClass('hidden');
        }
    });

    var $classicParameter = $('#classicParameter');
    var $dependableParameter = $('#dependableParameter');
    var $uploadableParameter = $('#uploadableParameter');
    var $intervalParameter = $('#intervalParameter');

    // is this dependable type of parameter?
    if (selected_parameter_is_dependable($chosen_parameter)) {
        $panel_title.text('Dependable parameter options');
        $dependableParameter
            .removeClass('location').addClass('dictionary')
            .attr('data-parameter-id', parseInt($chosen_parameter.val()))
            .data('parameter-id', parseInt($chosen_parameter.val()))
            .attr('data-dictionary-id', parseInt($chosen_parameter.data('dictionary-id')))
            .data('dictionary-id', parseInt($chosen_parameter.data('dictionary-id')))
            .attr('data-max-level', parseInt($chosen_parameter.data('max-level')))
            .data('max-level', parseInt($chosen_parameter.data('max-level')))
            .attr('data-source', 'dictionary')
            .data('source', 'dictionary');

        if (parameter_dialog_is_visible) {
            dependable_parameter_init();
        }
        // show our parameter modal
        if ($dependableParameter.hasClass('hidden')) {
            $dependableParameter.removeClass('hidden');
        }
    // is it location type of parameter?
    } else if (selected_parameter_is_location($chosen_parameter)) {
        $panel_title.text('Location parameter options');
        $dependableParameter
            .removeClass('dictionary').addClass('location')
            .attr('data-parameter-id', '')
            .data('parameter-id', '')
            .attr('data-dictionary-id', '')
            .data('dictionary-id', '')
            .attr('data-max-level', 5)
            .data('max-level', 5)
            .attr('data-source', 'location')
            .data('source', 'location');

        if (parameter_dialog_is_visible) {
            dependable_parameter_init();
        }
        // show our parameter modal
        if ($dependableParameter.hasClass('hidden')) {
            $dependableParameter.removeClass('hidden');
        }
    // is it uploadable parameter type?
    } else if ('UPLOADABLE' === parameter_type_id) {
        $panel_title.text('Uploadable parameter options');
        // show our parameter modal
        if ($uploadableParameter.hasClass('hidden')) {
            $uploadableParameter.removeClass('hidden');
        }
    // is it some kind of interval parameter (year, monthyear, ...)?
    } else if ('YEAR' === parameter_type_id || 'MONTHYEAR' === parameter_type_id) {
        $panel_title.text('Parameter options');
        if (parameter_dialog_is_visible) {
            set_available_is_searchable_types($('#interval_is_searchable_type'), $chosen_parameter, null);
            is_searchable_toggle($('#interval_is_searchable'));
        }
        // show our parameter modal
        if ($intervalParameter.hasClass('hidden')) {
            $intervalParameter.removeClass('hidden');
        }
    } else {
        $panel_title.text('Parameter options');

        $('.form-group.default-value').removeClass('hidden');
        $('.form-group.max-length').addClass('hidden');
        // handle 'standard' group of parameters
        if ('standard' == parameter_group) {
            // none of the 'standard' parameters can have default values
            $('#default_value_text').removeClass('hidden');
            $('#default_value_text').prop('readonly', true);
            $('#default_value_date').addClass('hidden');
            $('#default_value_date').prop('readonly', true);
            $('#default_value_checkbox').addClass('hidden');
            $('#default_value_select').selectpicker('hide');

            // none of the 'standard' parameters can have prefix/suffix values
            $('#attribute_prefix, #attribute_suffix').prop('readonly', true);

            if ('CURRENCY' == parameter_type_id) {
                $('#classicParameter div.help_text_div').removeClass('col-md-4 col-lg-4').addClass('col-md-8 col-lg-8');
                $('#classicParameter div.prefix_div').addClass('hidden');
                $('#classicParameter div.suffix_div').addClass('hidden');
                $('#classicParameter div.currency_div').removeClass('hidden');

                $('#attribute_prefix, #attribute_suffix').prop('readonly', true);
            } else {
                $('#classicParameter div.help_text_div').removeClass('col-md-8 col-lg-8').addClass('col-md-4 col-lg-4');
                $('#classicParameter div.prefix_div').removeClass('hidden');
                $('#classicParameter div.suffix_div').removeClass('hidden');
                $('#classicParameter div.currency_div').addClass('hidden');
            }

            // TITLE parameter can have a max length for input, so we'll hide the default
            if ('TITLE' == parameter_type_id) {
                $('.form-group.default-value').addClass('hidden');
                $('.form-group.max-length').removeClass('hidden');
            }
        } else {
            $('#classicParameter div.help_text_div').removeClass('col-md-8 col-lg-8').addClass('col-md-4 col-lg-4');
            $('#classicParameter div.prefix_div').removeClass('hidden');
            $('#classicParameter div.suffix_div').removeClass('hidden');
            $('#classicParameter div.currency_div').addClass('hidden');

            if ('NUMBER' == parameter_type_id) {
                $('#classicParameter div.prefix_div').removeClass('col-md-4 col-lg-4').addClass('col-md-2 col-lg-2');
                $('#classicParameter div.suffix_div').removeClass('col-md-4 col-lg-4').addClass('col-md-2 col-lg-2');
                $('#classicParameter div.number_decimals_div').removeClass('hidden');
            } else {
                $('#classicParameter div.prefix_div').removeClass('col-md-2 col-lg-2').addClass('col-md-4 col-lg-4');
                $('#classicParameter div.suffix_div').removeClass('col-md-2 col-lg-2').addClass('col-md-4 col-lg-4');
                $('#classicParameter div.number_decimals_div').addClass('hidden');
            }

            // enable/disable 'default value' if parameter accepts it
            if (parseInt($chosen_parameter.data('can-default'))) {
                // based on the 'can_default_value' default_value is presented as text or checkbox input
                if ($chosen_parameter.data('can-default-type') == 'text') {
                    $('#default_value_text').removeClass('hidden');
                    $('#default_value_text').prop('readonly', false);
                    $('#default_value_date').addClass('hidden');
                    $('#default_value_date').prop('disabled', true);
                    $('#default_value_select').selectpicker('hide');
                    $('#default_value_select').prop('disabled', true);
                    $('#default_value_checkbox').addClass('hidden');
                } else if ($chosen_parameter.data('can-default-type') == 'dropdown') {
                    $('#default_value_text').addClass('hidden');
                    $('#default_value_text').prop('readonly', true);
                    $('#default_value_date').addClass('hidden');
                    $('#default_value_date').prop('readonly', true);
                    $('#default_value_select').prop('disabled', false);
                    $('#default_value_select').selectpicker('show');
                    $('#default_value_checkbox').addClass('hidden');
                    if (parseInt($chosen_parameter.data('dictionary-id'))) {
                        classic_parameter_populate_default_value_select($chosen_parameter.data('dictionary-id'));
                    }
                } else if ($chosen_parameter.data('can-default-type') == 'date') {
                    $('#default_value_text').addClass('hidden');
                    $('#default_value_text').prop('readonly', true);
                    $('#default_value_date').removeClass('hidden');
                    $('#default_value_date').prop('readonly', false);
                    $('#default_value_select').selectpicker('hide');
                    $('#default_value_select').prop('disabled', true);
                    $('#default_value_checkbox').addClass('hidden');
                    if (parseInt($chosen_parameter.data('dictionary-id'))) {
                        classic_parameter_populate_default_value_select($chosen_parameter.data('dictionary-id'));
                    }
                } else {
                    $('#default_value_text').addClass('hidden');
                    $('#default_value_text').prop('readonly', true);
                    $('#default_value_date').addClass('hidden');
                    $('#default_value_date').prop('readonly', true);
                    $('#default_value_select').prop('disabled', true);
                    $('#default_value_select').selectpicker('hide');
                    $('#default_value_checkbox').removeClass('hidden');
                }
            } else {
                $('#default_value_text').removeClass('hidden');
                $('#default_value_text').prop('readonly', true);
                $('#default_value_date').addClass('hidden');
                $('#default_value_date').prop('readonly', true);
                $('#default_value_checkbox').addClass('hidden');
                $('#default_value_select').selectpicker('hide');
            }

            // enable/disable 'Prefix' and 'Suffix' values
            if (parseInt($chosen_parameter.data('can-prefix-suffix'))) {
                $('#attribute_prefix, #attribute_suffix').prop('readonly', false);
            } else {
                $('#attribute_prefix, #attribute_suffix').prop('readonly', true);
            }
        }

        if (parameter_dialog_is_visible) {
            set_available_is_searchable_types($('#attribute_is_searchable_type'), $chosen_parameter, null);
            is_searchable_toggle($('#attribute_is_searchable'));
        }
        // show our parameter modal
        if ($classicParameter.hasClass('hidden')) {
            $classicParameter.removeClass('hidden');
        }
    }
}

function reset_parameter_attributes_dialog() {
    dependable_parameter_init('reset');
    uploadable_parameter_set_attributes_to_dialog('reset');
    interval_parameter_set_attributes_to_dialog('reset');
    classic_parameter_set_attributes_to_dialog('reset');
}

/**
 * Populate and display 'Parameter Dialog' window with attribute values
 * @param {none|string} parameter_slug  Parameter's slug
 * @param {none|object} parameter_attributes  Parameter's attributes object
 * @param {none|int} target_fieldset  Target fieldset
 * @param {none|string} dialog_type  Can be 'add' or 'edit'
 */
function show_parameter_attributes_dialog(parameter_slug, parameter_attributes, target_fieldset, dialog_type) {
    // if all parameters are undefined, 'Add new parameter' was clicked...
    if (
        'undefined' == typeof parameter_slug &&
        'undefined' == typeof parameter_attributes &&
        'undefined' == typeof target_fieldset &&
        'undefined' == typeof dialog_type
    ) {
        // preselect first available (enabled) parameter
        $('#parameter_id').val($('#parameter_id option:enabled:eq(0)').val());
    }
    // reset all dialog options as it was never used :)
    reset_parameter_attributes_dialog();

    // based on 'dialog_type', which can be 'new' or 'edit', set appropriate action button and dialog title
    //  - default value is 'new'
    var $dropdown = get_selected_parameter_dropdown();
    $('#parameter_attribute_slug').val('');

    if ('undefined' === typeof dialog_type || 'edit' !== dialog_type) {
        dialog_type = 'new';
    }

    if ('undefined' === typeof target_fieldset) {
        target_fieldset = null;
    }

    $('#parameter_slug').closest('div.form-group').removeClass('has-error');

    if ('undefined' === typeof parameter_attributes) {
        var first_enabled_option = $dropdown.find('option:enabled:eq(0)');

        if ('undefined' === typeof parameter_slug) {
            parameter_slug = first_enabled_option.data('slug');
        }

        parameter_attributes = {
            id: first_enabled_option.val(),
            type_id: first_enabled_option.data('type-id')
        };
    } else {
        if ('undefined' === typeof parameter_slug) {
            parameter_slug = 'parameter_slug';
        }
    }

    var still_available_count = refresh_available_parameters_dropdown();

    if (still_available_count) {
        // reset the dependable parameter container so we can initialize it automatically in case we will need it...
        $dependableParameter_ul = $('ul.dependableParameter');
        $dependableParameterContainer = $dependableParameter_ul.parent();

        $dependableParameterContainer
            .attr('data-max-level', '').data('max-level', '')
            .attr('data-dictionary-id', '').data('dictionary-id', '')
            .attr('data-parameter-id', '').data('parameter-id', '')
            .attr('data-source', '').data('source', '');
        $dependableParameterContainer.find('p.level_dictionary_values').text('- - -');

        if ('new' == dialog_type) {
            $dropdown.prop('disabled', false).selectpicker('refresh');

            $('#addParameterModalLabel').text('Add new parameter');
            $('#btnSaveParameterModal').attr('data-type', 'add').data('type', 'add').text('Add');
        } else {
            $dropdown.prop('disabled', true).selectpicker('refresh');

            $('#addParameterModalLabel').text('Edit parameter settings');
            $('#btnSaveParameterModal').attr('data-type', 'save').data('type', 'save').text('Save');
        }

        // populate available fieldsets (and preselect one if it should be preselected)
        if ('undefined' !== typeof target_fieldset && target_fieldset) {
            populate_fieldset_id_select(target_fieldset);
        } else {
            populate_fieldset_id_select();
        }

        // preselect parameter and apply it's specific attributes
        $dropdown.find('option[value="' + parameter_attributes.id + '"]').show().prop('selected', true);
        $dropdown.selectpicker('val', parameter_attributes.id);
        $dropdown.trigger('change');

        if ('undefined' !== typeof parameter_attributes.type_id && ('LOCATION' === parameter_attributes.type_id || 'DEPENDABLE_DROPDOWN' === parameter_attributes.type_id)) {
            // dependable parameter
            if ('undefined' === typeof parameter_attributes.levels) {
                dependable_parameter_init();
            } else {
                dependable_parameter_init(parameter_attributes);
            }
        } else if ('undefined' !== typeof parameter_attributes.type_id && 'UPLOADABLE' === parameter_attributes.type_id) {
            // uploadable parameter
            if ('new' !== dialog_type) {
                uploadable_parameter_set_attributes_to_dialog(parameter_attributes);
            }
        } else if ('undefined' !== typeof parameter_attributes.type_id && ('YEAR' === parameter_attributes.type_id || 'MONTHYEAR' === parameter_attributes.type_id)) {
            // interval parameter
            if ('new' !== dialog_type) {
                interval_parameter_set_attributes_to_dialog(parameter_attributes);
            }
        } else {
            // classic parameter
            if ('new' !== dialog_type) {
                classic_parameter_set_attributes_to_dialog(parameter_attributes);
            }
        }

        $('#parameter_slug').val(parameter_slug);

        // show the dialog
        $('#addNewParameterModal').modal({
            backdrop: 'static',
            keyboard: true
        });
    } else {
        alert('You have used all available parameters... :/');
    }
}

/**
 * This method validates administrator's/moderator's input and if everything is ok it returns
 * an object containing parameter_attributes and target_fieldset values. Otherwise, it will
 * return false (which indicates that some validation failed).
 *
 * @param {string} button_type  Button type (can be 'add' or 'edit')
 * @return {false|object}       'false' if validation failed; otherwise, an object is returned
 */
function collect_data_from_parameter_dialog(button_type) {
    // we need to collect all attribute values from the dialog and validate them
    // first step is to remove all 'has-error' classes as we are starting validation from scratch
    $('#parameterOptionsModalBody .has-error').removeClass('has-error');

    var $dropdown = $('#parameter_id');
    var selected_parameter_id;
    var parameter_slug = $('#parameter_slug').val();
    if ('add' === button_type) {
        selected_parameter_id = parseInt($dropdown.selectpicker('val'));
    } else {
        selected_parameter_id = parseInt($dropdown.find('option:selected').attr('value'));
    }
    var $selected_parameter = $dropdown.find('option[value="' + selected_parameter_id + '"]');

    if ($selected_parameter.length > 0) {
        var parameter_slug_is_unique = is_parameters_slug_unique(parameter_slug, selected_parameter_id);
        if (parameter_slug_is_unique) {
            $('#parameter_slug').closest('div.form-group').removeClass('has-error');

            var parameter_price = null;
            var parameter_price_currency_id = null;
            if ($('#attribute_has_price').prop('checked') && parseFloat($('#attribute_price').autoNumeric('get')) > 0) {
                parameter_price = parseFloat($('#attribute_price').autoNumeric('get'));
                parameter_price_currency_id = parseInt($('#attribute_price').data('currency-id'));
            }

            if (!isNaN($('#fieldset_id').selectpicker('val'))) {
                var parameter_target_fieldset = parseInt($('#fieldset_id').selectpicker('val'));
            } else {
                var parameter_target_fieldset = 'createNew';
            }

            var parameter_is_dependable = selected_parameter_is_dependable($selected_parameter);
            var parameter_is_location = false;
            if (!parameter_is_dependable) {
                parameter_is_location = selected_parameter_is_location($selected_parameter);
            }

            if (parameter_is_dependable || parameter_is_location) {
                // selected parameter is dependable so we have to loop through its values
                var dependable_parameter_attributes = {
                    id: selected_parameter_id,
                    type_id: $selected_parameter.data('type-id'),
                    price: parameter_price,
                    price_currency_id: parameter_price_currency_id,
                    levels: []
                };

                var current_parameters_level = 1;
                // iterate through added levels and put them in the dependable_parameter_attributes array...
                $dependableParameterContainer.find('.tab-content .tab-pane').each(function(tab_pane_index){
                    var $current_tab_pane = $(this);
                    var curr_tab_level = parseInt($current_tab_pane.data('level'));

                    var parameter_attribute_label_text = $.trim($('#dp_level_' + curr_tab_level + '_attribute_label_text').val());
                    if (parameter_attribute_label_text) {
                        $('#dp_level_' + curr_tab_level + '_attribute_label_text').closest('div.form-group').removeClass('has-error');
                    } else {
                        $('#dp_level_' + curr_tab_level + '_attribute_label_text').closest('div.form-group').addClass('has-error');
                    }

                    var parameter_attribute_default_value = null;
                    // location parameter can have default value set for first level
                    if (current_parameters_level == 1 && parameter_is_location && '' !== $.trim($('#location_default_value_select').val())) {
                        parameter_attribute_default_value = parseInt($.trim($('#location_default_value_select').val()));
                    }

                    var parameter_attribute_help_text = null;
                    if ($.trim($('#dp_level_' + curr_tab_level + '_attribute_help_text').val())) {
                        parameter_attribute_help_text = $.trim($('#dp_level_' + curr_tab_level + '_attribute_help_text').val());
                    }

                    var parameter_attribute_is_searchable = $('#dp_level_' + curr_tab_level + '_attribute_is_searchable').prop('checked') ? 1 : 0;
                    var parameter_attribute_is_searchable_options = null;
                    if (parameter_attribute_is_searchable) {
                        parameter_attribute_is_searchable_options = {};
                        if ($.trim($('#dp_level_' + curr_tab_level + '_attribute_is_searchable_label_text').val())) {
                            parameter_attribute_is_searchable_options.label_text = $.trim($('#dp_level_' + curr_tab_level + '_attribute_is_searchable_label_text').val());
                            $('#dp_level_' + curr_tab_level + '_attribute_is_searchable_label_text').closest('div.form-group').removeClass('has-error');
                        } else {
                            $('#dp_level_' + curr_tab_level + '_attribute_is_searchable_label_text').closest('div.form-group').addClass('has-error');
                        }
                        parameter_attribute_is_searchable_options.type = $('#dp_level_' + curr_tab_level + '_attribute_is_searchable_type').val();
                    }

                    // construct parameter_attributes object with collected data
                    var parameter_attributes = {
                        label_text: parameter_attribute_label_text,
                        default_value: parameter_attribute_default_value,
                        help_text: parameter_attribute_help_text,
                        is_searchable: parameter_attribute_is_searchable,
                        is_searchable_options: parameter_attribute_is_searchable_options,
                        is_required: $('#dp_level_' + curr_tab_level + '_attribute_is_required').prop('checked') ? 1 : 0,
                        is_hidden: $('#dp_level_' + curr_tab_level + '_attribute_is_hidden').prop('checked') ? 1 : 0
                    };

                    dependable_parameter_attributes.levels.push(parameter_attributes);
                    current_parameters_level++;
                });

                // validation is done.... if there are no 'has-error' classes, we can proceed
                if ($('#parameterOptionsModalBody .has-error').length == 0 && dependable_parameter_attributes.levels.length >= dependable_parameter_min_number_of_levels) {
                    return {
                        'slug': parameter_slug,
                        'parameter_attributes': dependable_parameter_attributes,
                        'target_fieldset': parameter_target_fieldset
                    };
                } else {
                    var first_errorer_level = parseInt($('#parameterOptionsModalBody .has-error:eq(0)').closest('.tab-pane').data('level'));
                    $dependableParameter_ul.find('a[href="#dp_level_' + first_errorer_level + '"]').trigger('click');
                }
            } else if ($.trim($selected_parameter.data('type-id')) == 'UPLOADABLE') {
                // uploadable parameter type

                var $label_text = $('#uploadable_label_text');
                parameter_attribute_label_text = $.trim($label_text.val());
                if (parameter_attribute_label_text) {
                    $label_text.closest('div.form-group').removeClass('has-error');
                } else {
                    $label_text.closest('div.form-group').addClass('has-error');
                }

                var $button_text = $('#uploadable_button_text');
                var parameter_attribute_button_text = $.trim($button_text.val());
                if (parameter_attribute_button_text) {
                    $button_text.closest('div.form-group').removeClass('has-error');
                } else {
                    $button_text.closest('div.form-group').addClass('has-error');
                }

                var $max_items = $('#uploadable_attribute_max_items');
                var parameter_attribute_max_items = $.trim($max_items.val());
                if (parameter_attribute_max_items && !isNaN(parameter_attribute_max_items)) {
                    $max_items.closest('div.form-group').removeClass('has-error');
                } else {
                    $max_items.closest('div.form-group').addClass('has-error');
                }

                var help_text = null;
                var $help_text = $('#uploadable_attribute_help_text');
                var help_text_trimmed = $.trim($help_text.val());
                if ('' !== help_text_trimmed) {
                    help_text = help_text_trimmed;
                }

                var parameter_attribute_is_searchable = $('#uploadable_attribute_is_searchable').prop('checked') ? 1 : 0;
                var parameter_attribute_is_searchable_options = null;
                if (parameter_attribute_is_searchable) {
                    parameter_attribute_is_searchable_options = {};
                    if ($.trim($('#uploadable_attribute_is_searchable_label_text').val())) {
                        parameter_attribute_is_searchable_options.label_text = $.trim($('#uploadable_attribute_is_searchable_label_text').val());
                        $('#uploadable_attribute_is_searchable_label_text').closest('div.form-group').removeClass('has-error');
                        parameter_attribute_is_searchable_options.type = 'CHECKBOX';
                        parameter_attribute_is_searchable_options.default_value = $('#uploadable_attribute_is_searchable_default_value').prop('checked');
                    } else {
                        $('#uploadable_attribute_is_searchable_label_text').closest('div.form-group').addClass('has-error');
                    }
                }
                // If there are no 'has-error' classes we proceed
                if ($('#parameterOptionsModalBody .has-error').length == 0) {
                    // construct parameter_attributes object with collected data
                    parameter_attributes = {
                        id: selected_parameter_id,
                        type_id: $selected_parameter.data('type-id'),
                        price: parameter_price,
                        price_currency_id: parameter_price_currency_id,
                        label_text: parameter_attribute_label_text,
                        button_text: parameter_attribute_button_text,
                        max_items: parameter_attribute_max_items,
                        help_text: help_text,
                        //deletable: true,
                        is_searchable: parameter_attribute_is_searchable,
                        is_searchable_options: parameter_attribute_is_searchable_options,
                        is_required: $('#uploadable_attribute_is_required').prop('checked') ? 1 : 0
                    };
                    return {
                        'slug': parameter_slug,
                        'parameter_attributes' : parameter_attributes,
                        'target_fieldset' : parameter_target_fieldset
                    };
                }
            } else if ('MONTHYEAR' === currently_selected_parameter_type || 'YEAR' === currently_selected_parameter_type) {
                // selected parameter is interval parameter

                var parameter_attribute_label_text = $.trim($('#interval_label_text').val());
                if (parameter_attribute_label_text) {
                    $('#interval_label_text').closest('div.form-group').removeClass('has-error');
                } else {
                    $('#interval_label_text').closest('div.form-group').addClass('has-error');
                }

                var parameter_attribute_placeholder_text = null;
                if ($.trim($('#interval_placeholder_text').val())) {
                    parameter_attribute_placeholder_text = $.trim($('#interval_placeholder_text').val());
                }

                var parameter_attribute_help_text = null;
                if ($.trim($('#interval_help_text').val())) {
                    parameter_attribute_help_text = $.trim($('#interval_help_text').val());
                }

                var parameter_attribute_begin_value = 0;
                var parameter_attribute_begin_period = 'Year';
                if ($.trim($('#interval_begin').val())) {
                    parameter_attribute_begin_value = parseInt($.trim($('#interval_begin').val()));
                    parameter_attribute_begin_period = $.trim($('#interval_begin').data('type'));
                }

                var parameter_attribute_end_value = 0;
                var parameter_attribute_end_period = 'Year';
                if ($.trim($('#interval_end').val())) {
                    parameter_attribute_end_value = parseInt($.trim($('#interval_end').val()));
                    parameter_attribute_end_period = $.trim($('#interval_end').data('type'));
                }

                var parameter_attribute_is_searchable = $('#interval_is_searchable').prop('checked') ? 1 : 0;
                var parameter_attribute_is_searchable_options = null;
                if (parameter_attribute_is_searchable) {
                    parameter_attribute_is_searchable_options = {};
                    if ($.trim($('#interval_is_searchable_label_text').val())) {
                        parameter_attribute_is_searchable_options.label_text = $.trim($('#interval_is_searchable_label_text').val());
                        $('#interval_is_searchable_label_text').closest('div.form-group').removeClass('has-error');
                    } else {
                        $('#interval_is_searchable_label_text').closest('div.form-group').addClass('has-error');
                    }
                    parameter_attribute_is_searchable_options.type = $('#interval_is_searchable_type').val();
                }

                // validation is done.... if there are no 'has-error' classes, we can proceed
                if ($('#parameterOptionsModalBody .has-error').length == 0) {
                    // set proper parameter's default value attribute
                    var parameter_attribute_default_value = null;
                    if ($.trim($('#interval_default_value').selectpicker('val'))) {
                        parameter_attribute_default_value = $.trim($('#interval_default_value').selectpicker('val'));
                    }

                    // construct parameter_attributes object with collected data
                    var parameter_attributes = {
                        id: selected_parameter_id,
                        type_id: $selected_parameter.data('type-id'),
                        price: parameter_price,
                        price_currency_id: parameter_price_currency_id,
                        label_text: parameter_attribute_label_text,
                        placeholder_text: parameter_attribute_placeholder_text,
                        help_text: parameter_attribute_help_text,
                        interval_begin_value: parameter_attribute_begin_value,
                        interval_begin_period: parameter_attribute_begin_period,
                        interval_end_value: parameter_attribute_end_value,
                        interval_end_period: parameter_attribute_end_period,
                        is_reverse: $('#interval_is_reverse').prop('checked') ? 1 : 0,
                        default_value: parameter_attribute_default_value,
                        is_searchable: parameter_attribute_is_searchable,
                        is_searchable_options: parameter_attribute_is_searchable_options,
                        is_required: $('#interval_is_required').prop('checked') ? 1 : 0,
                        is_hidden: $('#interval_is_hidden').prop('checked') ? 1 : 0
                    };

                    return {
                        'slug': parameter_slug,
                        'parameter_attributes' : parameter_attributes,
                        'target_fieldset' : parameter_target_fieldset
                    };
                }
            } else {
                // selected parameter is classic (not dependable)

                var parameter_attribute_label_text = $.trim($('#attribute_label_text').val());
                if (parameter_attribute_label_text) {
                    $('#attribute_label_text').closest('div.form-group').removeClass('has-error');
                } else {
                    $('#attribute_label_text').closest('div.form-group').addClass('has-error');
                }

                var parameter_attribute_max_length = null;
                if ('TITLE' === $selected_parameter.data('type-id')) {
                    parameter_attribute_max_length = parseInt($.trim($('#default_max_length').val()));
                    if (isNaN(parameter_attribute_max_length)) {
                        parameter_attribute_max_length = default_title_max_length;
                    }
                }

                var parameter_attribute_help_text = null;
                if ($.trim($('#attribute_help_text').val())) {
                    parameter_attribute_help_text = $.trim($('#attribute_help_text').val());
                }

                var parameter_attribute_prefix = null;
                if ($.trim($('#attribute_prefix').val())) {
                    parameter_attribute_prefix = $.trim($('#attribute_prefix').val());
                }

                var parameter_attribute_suffix = null;
                if ($.trim($('#attribute_suffix').val())) {
                    parameter_attribute_suffix = $.trim($('#attribute_suffix').val());
                }

                var parameter_attribute_number_decimals = 0;
                if ('NUMBER' === $selected_parameter.data('type-id')) {
                    parameter_attribute_number_decimals = parseInt($.trim($('#attribute_number_decimals').val()));
                    if (isNaN(parameter_attribute_number_decimals)) {
                        parameter_attribute_number_decimals = 0;
                    }
                }

                var parameter_currency_id = null;
                if ('CURRENCY' == $selected_parameter.data('type-id')) {
                    if (parseInt($("#attribute_currency").selectpicker('val'))) {
                        parameter_currency_id = parseInt($("#attribute_currency").selectpicker('val'));
                    }

                    if (parameter_currency_id) {
                        $('#attribute_currency').closest('div.form-group').removeClass('has-error');
                    } else {
                        $('#attribute_currency').closest('div.form-group').addClass('has-error');
                    }
                } else {
                    $('#attribute_currency').closest('div.form-group').removeClass('has-error');
                }

                var parameter_attribute_is_searchable = $('#attribute_is_searchable').prop('checked') ? 1 : 0;
                var parameter_attribute_is_searchable_options = null;
                if (parameter_attribute_is_searchable) {
                    parameter_attribute_is_searchable_options = {};
                    if ($.trim($('#attribute_is_searchable_label_text').val())) {
                        parameter_attribute_is_searchable_options.label_text = $.trim($('#attribute_is_searchable_label_text').val());
                        $('#attribute_is_searchable_label_text').closest('div.form-group').removeClass('has-error');
                    } else {
                        $('#attribute_is_searchable_label_text').closest('div.form-group').addClass('has-error');
                    }
                    parameter_attribute_is_searchable_options.type = $('#attribute_is_searchable_type').val();
                }

                // validation is done.... if there are no 'has-error' classes, we can proceed
                if ($('#parameterOptionsModalBody .has-error').length == 0) {
                    // set proper parameter's default value attribute
                    var parameter_attribute_default_value = null;
                    if (parseInt($selected_parameter.data('can-default'))) {
                        if ($selected_parameter.data('can-default-type') == 'text' && $.trim($('#default_value_text').val())) {
                            parameter_attribute_default_value = $.trim($('#default_value_text').val());
                        } else if ($selected_parameter.data('can-default-type') == 'date' && $.trim($('#default_value_date').val())) {
                            parameter_attribute_default_value = $('#default_value_date').datepicker('getDate').toYYYYMMDD();
                        } else if ($selected_parameter.data('can-default-type') == 'checkbox') {
                            parameter_attribute_default_value = $('#default_value_checkbox').prop('checked');
                        } else if ($selected_parameter.data('can-default-type') == 'dropdown' && parseInt($('#default_value_select').selectpicker('val'))) {
                            parameter_attribute_default_value = parseInt($('#default_value_select').selectpicker('val'));
                        }
                    }

                    // construct parameter_attributes object with collected data
                    var parameter_attributes = {
                        id: selected_parameter_id,
                        type_id: $selected_parameter.data('type-id'),
                        price: parameter_price,
                        price_currency_id: parameter_price_currency_id,
                        label_text: parameter_attribute_label_text,
                        placeholder_text: $.trim($('#attribute_placeholder_text').val()),
                        default_value: parameter_attribute_default_value,
                        max_length: parameter_attribute_max_length,
                        help_text: parameter_attribute_help_text,
                        prefix: parameter_attribute_prefix,
                        suffix: parameter_attribute_suffix,
                        number_decimals: parameter_attribute_number_decimals,
                        currency_id: parameter_currency_id,
                        is_searchable: parameter_attribute_is_searchable,
                        is_searchable_options: parameter_attribute_is_searchable_options,
                        is_required: $('#attribute_is_required').prop('checked') ? 1 : 0,
                        is_hidden: $('#attribute_is_hidden').prop('checked') ? 1 : 0
                    };

                    return {
                        'slug': parameter_slug,
                        'parameter_attributes' : parameter_attributes,
                        'target_fieldset' : parameter_target_fieldset
                    };
                }
            }
        } else {
            $('#parameter_slug').closest('div.form-group').addClass('has-error');
        }
    }
    return false;
}

/**
 * Adds new parameter to target fieldset
 * @param {object} parameter_attributes Parameter's attributes
 */
function add_parameter_to_fieldset(parameter_slug, parameter_attributes, target_fieldset) {
    var parameter_attributes_JSON = JSON.stringify(parameter_attributes);

    var parameter_is_required = '';
    var parameter_is_searchable = '';

    if ('undefined' !== typeof parameter_attributes.levels) {
        // dependable parameter
        var parameter_is_hidden = false;
        var parameter_name_text = [];
        var parameter_name_html = [];
        $.each(parameter_attributes.levels, function(index, level_attributes) {
            parameter_name_text.push(level_attributes.label_text);
            if (level_attributes.is_hidden) {
                parameter_is_hidden = true;
                parameter_name_html.push('<span class="text-danger">' + level_attributes.label_text + '</span>');
            } else {
                parameter_name_html.push(level_attributes.label_text);
            }
            if (level_attributes.is_required) {
                parameter_is_required = ' <span class="fa fa-asterisk fa-fw text-muted" title="Required"><!--IE--></span>';
            }
            if (level_attributes.is_searchable) {
                parameter_is_searchable = ' <span class="fa fa-search fa-fw text-muted" title="Searchable"><!--IE--></span>';
            }
        });
        parameter_name_text = parameter_name_text.join(' › ');
        parameter_name_html = parameter_name_html.join(' <span class="fa fa-long-arrow-right fa-fw"><!--IE--></span> ');
    } else {
        // classic parameter
        var parameter_is_hidden = parameter_attributes.is_hidden;
        var parameter_name_text = parameter_attributes.label_text;
        var parameter_name_html = parameter_name_text;
        if (parameter_attributes.is_required) {
            parameter_is_required = ' <span class="fa fa-asterisk fa-fw text-muted" title="Required"><!--IE--></span>';
        }
        if (parameter_attributes.is_searchable) {
            parameter_is_searchable = ' <span class="fa fa-search fa-fw text-muted" title="Searchable"><!--IE--></span>';
        }
    }

    if (parameter_attributes.price) {
        parameter_name_html = parameter_name_html + ' <span class="fa fa-money fa-fw text-muted" title="Price"><!--IE--></span>';
    }

    parameter_name_html += parameter_is_required;
    parameter_name_html += parameter_is_searchable;

    var filters_sort_order = 0;

    var $li = $('<li/>')
        .addClass('sortable_li' + (parameter_is_hidden ? ' disabled' : ''))
        .attr('id', 'item_' + parameter_attributes.id)
        .attr('data-parameter-slug', parameter_slug).data('parameter-slug', parameter_slug)
        .attr('data-parameter-name', parameter_name_text).data('parameter-name', parameter_name_text)
        .attr('data-parameter-id', parameter_attributes.id).data('parameter-id', parameter_attributes.id)
        .attr('data-filters-sort-order', filters_sort_order).data('filters-sort-order', filters_sort_order)
        .attr('data-parameter-price', parameter_attributes.price).data('parameter-price', parameter_attributes.price)
        .attr('data-parameter-attributes', parameter_attributes_JSON).data('parameter-attributes', parameter_attributes_JSON)
        .append(
            $('<div/>')
                .addClass('sortable_div')
                .append(
                    $('<div/>')
                        .addClass('sortable_row')
                        .append(
                            $('<div/>')
                                .addClass('drag-me ui-sortable-handle')
                                .append(
                                    $('<span/>')
                                        .addClass('fa fa-arrows fa-fw')
                                        .html('<!-- IE -->')
                                )
                        )
                        .append(
                            $('<div/>')
                                .addClass('name-cat')
                                .append(
                                    $('<span/>')
                                        .addClass('name')
                                        .html(parameter_name_html)
                                )
                        )
                        .append(
                            $('<div/>')
                                .addClass('actions-cat labels')
                                .append(
                                    $('<span/>')
                                        .addClass('btn btn-info btn-xs')
                                        .attr('data-action', 'options').data('action', 'options')
                                        .append(
                                            $('<span/>')
                                                .addClass('fa fa-cog fa-fw')
                                                .html('<!-- IE -->')
                                        )
                                        .append(
                                            document.createTextNode(' Options')
                                        )
                                        .on('click', function(){
                                            edit_parameter_options($(this));
                                        })
                                )
                                .append(
                                    document.createTextNode(' · ')
                                )
                                .append(
                                    $('<span/>')
                                        .addClass('btn btn-danger btn-xs')
                                        .attr('data-action', 'delete').data('action', 'delete')
                                        .append(
                                            $('<span/>')
                                                .addClass('fa fa-trash-o fa-fw')
                                                .html('<!-- IE -->')
                                        )
                                        .on('click', function(){
                                            delete_parameter_from_fieldset($(this));
                                        })
                                )
                        )
                )
        );

    if ('undefined' === typeof target_fieldset) {
        target_fieldset = 1;
    } else {
        if ('createNew' === target_fieldset) {
            target_fieldset = $('#fieldset_sorter > li').length;
        } else {
            if (parseInt(target_fieldset) < 1) {
                target_fieldset = 1;
            } else if (parseInt(target_fieldset) > $('#fieldset_sorter > li').length) {
                target_fieldset = $('#fieldset_sorter > li').length;
            }
        }
    }

    var $target_fieldset_params_ul = $('#fieldset_sorter > li[data-fieldset-id="' + target_fieldset + '"] ul.sortable');
    if ($target_fieldset_params_ul.length > 0) {
        $target_fieldset_params_ul.append(
            $li.fadeIn('fast', function(){
                $('#parameter_id option[value="' + parameter_attributes.id + '"]').prop('disabled', true).hide();
                refresh_category_fieldsets_and_parameters();
            })
        )
    } else if ('undefined' !== typeof write_to_console && write_to_console) {
        console.error('Target Fieldset (ID: ' + target_fieldset + ') not found or UL within it not found!');
    }
}

/**
 * Save currently edited parameter
 * @param {object} parameter_attributes  Parameter's attributes object
 * @param {int} target_fieldset  Target fieldset
 */
function save_parameter_to_fieldset(parameter_slug, parameter_attributes, target_fieldset) {
    // locate current parameter
    var $current_parameter = $('#item_'+parameter_attributes.id);
    if ($current_parameter.length > 0) {

        // find current parameter's fieldset
        var $current_fieldset = $current_parameter.closest('li.fieldset');
        if ($current_fieldset.length > 0) {
            // we have successfully located exact position of current parameter...

            // first we have to see what has changed... in case that target_fieldset is different, then the best option
            // is to delete current parameter and create a new one in a target_fieldset.
            if (parseInt($current_fieldset.data('fieldset-id')) !== parseInt(target_fieldset)) {
                // fade out current parameter
                $current_parameter.fadeOut('fast', function(){
                    // totally remove it from the fieldset
                    $current_parameter.remove();

                    // and create a new one, just like we would create a new parameter
                    add_parameter_to_fieldset(parameter_slug, parameter_attributes, target_fieldset);

                    // job done! (we don't have to rebuild final array as it will be rebuilt automatically from within
                    // add_parameter_to_fieldset() function...)
                });
            } else {
                // it seems like the fieldset hasn't changed, so we have to change only parameter's values
                var parameter_attributes_JSON = JSON.stringify(parameter_attributes);

                var parameter_is_required = '';
                var parameter_is_searchable = '';

                if ('undefined' !== typeof parameter_attributes.levels) {
                    // dependable parameter
                    var parameter_is_hidden = false;
                    var parameter_name_text = [];
                    var parameter_name_html = [];
                    $.each(parameter_attributes.levels, function(index, level_attributes) {
                        parameter_name_text.push(level_attributes.label_text);
                        if (level_attributes.is_hidden) {
                            parameter_is_hidden = true;
                            parameter_name_html.push('<span class="text-danger">' + level_attributes.label_text + '</span>');
                        } else {
                            parameter_name_html.push(level_attributes.label_text);
                        }
                        if (level_attributes.is_required) {
                            parameter_is_required = ' <span class="fa fa-asterisk fa-fw text-muted" title="Required"><!--IE--></span>';
                        }
                        if (level_attributes.is_searchable) {
                            parameter_is_searchable = ' <span class="fa fa-search fa-fw text-muted" title="Searchable"><!--IE--></span>';
                        }
                    });
                    parameter_name_text = parameter_name_text.join(' › ');
                    parameter_name_html = parameter_name_html.join(' <span class="fa fa-long-arrow-right fa-fw"><!--IE--></span> ');
                } else {
                    // classic parameter
                    var parameter_is_hidden = parameter_attributes.is_hidden;
                    var parameter_name_text = parameter_attributes.label_text;
                    var parameter_name_html = parameter_name_text;

                    if (parameter_attributes.is_required) {
                        parameter_is_required = ' <span class="fa fa-asterisk fa-fw text-muted" title="Required"><!--IE--></span>';
                    }
                    if (parameter_attributes.is_searchable) {
                        parameter_is_searchable = ' <span class="fa fa-search fa-fw text-muted" title="Searchable"><!--IE--></span>';
                    }
                }

                if (parameter_attributes.price) {
                    parameter_name_html = parameter_name_html + ' <span class="fa fa-money fa-fw text-muted"><!--IE--></span>';
                }

                parameter_name_html += parameter_is_required;
                parameter_name_html += parameter_is_searchable;

                $current_parameter
                    .attr('data-parameter-slug', parameter_slug).data('parameter-slug', parameter_slug)
                    .attr('data-parameter-name', parameter_name_text).data('parameter-name', parameter_name_text)
                    .attr('data-parameter-price', parameter_attributes.price).data('parameter-price', parameter_attributes.price)
                    .attr('data-parameter-attributes', parameter_attributes_JSON).data('parameter-attributes', parameter_attributes_JSON)
                    .find('div.name-cat span.name').html(parameter_name_html);

                if (parameter_is_hidden) {
                    $current_parameter.addClass('disabled');
                } else {
                    $current_parameter.removeClass('disabled');
                }

                // again, job done, but now, we have to rebuild our final array again
                refresh_category_fieldsets_and_parameters();
            }
        } else if ('undefined' !== typeof write_to_console && write_to_console) {
            console.error('Unexpected behavior: We were not able to find current parameter\'s fieldset.', $current_parameter);
        }
    } else if ('undefined' !== typeof write_to_console && write_to_console) {
        console.error('Unexpected behavior: We were not able to locate current parameter.', $current_parameter);
    }
}

/**
 * Add/Edit action on 'Parameter Dialog' window
 */
function btnSaveParameterModalClick() {
    var button_type = $('#btnSaveParameterModal').data('type');
    var dialog_data = collect_data_from_parameter_dialog(button_type);
    if (dialog_data) {
        if ('undefined' !== typeof dialog_data.target_fieldset) {
            if ('createNew' === dialog_data.target_fieldset) {
                if (button_type == 'add') {
                    add_new_fieldset(function(){
                        add_parameter_to_fieldset(dialog_data.slug, dialog_data.parameter_attributes, dialog_data.target_fieldset);
                    });
                } else {
                    add_new_fieldset(function(){
                        save_parameter_to_fieldset(dialog_data.slug, dialog_data.parameter_attributes, dialog_data.target_fieldset);
                    });
                }
            } else {
                if (button_type == 'add') {
                    add_parameter_to_fieldset(dialog_data.slug, dialog_data.parameter_attributes, dialog_data.target_fieldset);
                } else {
                    save_parameter_to_fieldset(dialog_data.slug, dialog_data.parameter_attributes, dialog_data.target_fieldset);
                }
            }

            $('#parameter_id').prop('disabled', false).selectpicker('refresh');
            $('#addNewParameterModal').modal('hide');
        }
    }
}

/**
 * Deletes parameter from current fieldset (there are cases when parameter is protected from deleteion - ie. price, etc...
 * those parameters cannot be deleted and this function will not be called in those situations)
 * @param {object} $sender Delete button of parameter that needs to be deleted
 */
function delete_parameter_from_fieldset($sender) {
    var $current_parameter = $sender.closest('li.sortable_li');
    var $dropdown = get_selected_parameter_dropdown();
    if ($current_parameter.length == 1) {
        $current_parameter.fadeOut('fast', function(){
            $dropdown.find('option[value="' + $current_parameter.data('parameter-id') + '"]').prop('disabled', false).show();
            $current_parameter.remove();
            refresh_category_fieldsets_and_parameters();
        });
    } else if ('undefined' !== typeof write_to_console && write_to_console) {
        console.error('Unexpected behavior: We were not able to locate current parameter.', $sender);
    }
}

/**
 * Edit parameter
 * @param {object} $sender Options button of parameter that needs to be edited
 */
function edit_parameter_options($sender) {
    var $current_fieldset = $sender.closest('li.fieldset');
    if ($current_fieldset.length > 0) {
        var target_fieldset_id = parseInt($current_fieldset.data('fieldset-id'));
        if (target_fieldset_id) {
            var $current_parameter = $sender.closest('li.sortable_li');
            if ($current_parameter.length > 0) {
                if ('undefined' !== typeof $current_parameter.data('parameter-attributes') && $.trim($current_parameter.data('parameter-attributes'))) {
                    var parameter_slug = 'parameter_slug';
                    if ('undefined' !== typeof $current_parameter.data('parameter-slug') && '' !== $.trim($current_parameter.data('parameter-slug'))) {
                        parameter_slug = $.trim($current_parameter.data('parameter-slug'));
                    }
                    var parameter_attributes = JSON.parse($current_parameter.data('parameter-attributes'));
                    show_parameter_attributes_dialog(parameter_slug, parameter_attributes, target_fieldset_id, 'edit');
                }
            } else if ('undefined' !== typeof write_to_console && write_to_console) {
                console.error('Unexpected behavior: We were not able to locate current parameter.', $sender);
            }
        } else if ('undefined' !== typeof write_to_console && write_to_console) {
            console.error('Unexpected behavior: We were not able to find current parameter\'s fieldset ID.', $sender);
        }
    } else if ('undefined' !== typeof write_to_console && write_to_console) {
        console.error('Unexpected behavior: We were not able to find current parameter\'s fieldset.', $sender);
    }
}



//----------------------------------------------------------------------------------------------------------------------
// PARAMETER DIALOG & REORDER RELATED METHODS - END
//----------------------------------------------------------------------------------------------------------------------
// CLASSIC PARAMETER METHODS - START
//----------------------------------------------------------------------------------------------------------------------

var classic_parameter_dictionary_loading = false;

/**
 * Populate dialog fields with sent classic parameter's attributes
 * @param  {none|object} parameter_attributes
 */
function classic_parameter_set_attributes_to_dialog(parameter_attributes) {
    var $dropdown = get_selected_parameter_dropdown();
    var $dropdown_selected = $dropdown.find(':selected') || null;

    if ('undefined' === typeof parameter_attributes || 'reset' === parameter_attributes) {
        var param_is_searchable_options = null;
        var param_is_searchable = ('undefined' !== typeof $dropdown_selected.data('is-searchable') ? parseInt($dropdown_selected.data('is-searchable')) : 0);
        var param_is_required = ('undefined' !== typeof $dropdown_selected.data('is-required') ? parseInt($dropdown_selected.data('is-required')) : 0);
        var param_is_hidden = ('undefined' !== typeof $dropdown_selected.data('is-hidden') ? parseInt($dropdown_selected.data('is-hidden')) : 0);
        if (param_is_searchable && 'undefined' !== typeof $dropdown_selected.data('is-searchable-type')) {
            param_is_searchable_options = $.trim($dropdown_selected.data('is-searchable-type'));
        }

        var first_enabled_option = $dropdown.find('option:enabled:eq(0)');
        parameter_attributes = {
            id: first_enabled_option.val(),
            type_id: first_enabled_option.data('type-id'),
            price: null,
            price_currency_id: null,
            label_text: '',
            placeholder_text: null,
            default_value: null,
            max_length: null,
            help_text: null,
            prefix: null,
            suffix: null,
            number_decimals: null,
            currency_id: null,
            is_searchable: param_is_searchable,
            is_searchable_options: param_is_searchable_options,
            is_required: param_is_required,
            is_hidden: param_is_hidden
        };
    }

    $('#attribute_label_text').val(parameter_attributes.label_text);
    $('#attribute_placeholder_text').val((parameter_attributes.placeholder_text ? parameter_attributes.placeholder_text : ''));

    // handle default value
    // appropriate text/checkbox/select field is already displayed/hidden during #parameter_id onChange event so now
    // we only look for the currently visible element (default_value / default_value_checkbox)
    if (!$('#default_value_text').hasClass('hidden') && $('#default_value_date').hasClass('hidden') && $('#default_value_checkbox').hasClass('hidden') && $('#default_value_select').prop('disabled')) {
        // we have a text field type
        $('#default_value_text').val((parameter_attributes.default_value ? parameter_attributes.default_value : ''));
    } else if ($('#default_value_text').hasClass('hidden') && !$('#default_value_date').hasClass('hidden') && $('#default_value_checkbox').hasClass('hidden') && $('#default_value_select').prop('disabled')) {
        // we have a date field type
        $('#default_value_date').datepicker('setDate', (parameter_attributes.default_value ? parameter_attributes.default_value.fromYYYYMMDD() : ''));
    } else if ($('#default_value_text').hasClass('hidden') && $('#default_value_date').hasClass('hidden') && !$('#default_value_checkbox').hasClass('hidden') && $('#default_value_select').prop('disabled')) {
        // we have a checkbox field type
        $('#default_value_checkbox').prop('checked', (parameter_attributes.default_value ? true : false));
    } else if ($('#default_value_text').hasClass('hidden') && $('#default_value_date').hasClass('hidden') && $('#default_value_checkbox').hasClass('hidden') && !$('#default_value_select').prop('disabled')) {
        // we have a select field type
        classic_parameter_set_default_value_select((parameter_attributes.default_value ? parameter_attributes.default_value : '0'));
    } else if (parameter_attributes.default_value && ('undefined' !== typeof write_to_console && write_to_console)) {
        console.error('Unexpected behavior: Unknown default value field type?!');
    }

    if (!$('.form-group.max-length').hasClass('hidden')) {
        $('#default_max_length').val('undefined' !== typeof parameter_attributes.max_length && !isNaN(parameter_attributes.max_length) ? parseInt(parameter_attributes.max_length) : default_title_max_length);
    }

    $('#attribute_help_text').val((parameter_attributes.help_text ? parameter_attributes.help_text : ''));
    $('#attribute_prefix').val((parameter_attributes.prefix ? parameter_attributes.prefix : ''));
    $('#attribute_suffix').val((parameter_attributes.suffix ? parameter_attributes.suffix : ''));

    if (!$('#attribute_number_decimals').hasClass('hidden')) {
        var number_decimals = 0;
        if (parameter_attributes.number_decimals !== null) {
            number_decimals = parseInt(parameter_attributes.number_decimals);
            if (isNaN(number_decimals)) {
                number_decimals = 0;
            }
        }
        $('#attribute_number_decimals').val(number_decimals);
    }

    $('#attribute_currency').selectpicker('val', (parameter_attributes.currency_id ? parameter_attributes.currency_id : ''));

    var $attr_is_searchable = $('#attribute_is_searchable');
    $attr_is_searchable.prop('checked', parameter_attributes.is_searchable);
    set_available_is_searchable_types($('#attribute_is_searchable_type'), $dropdown_selected, parameter_attributes.is_searchable_options);
    is_searchable_toggle($attr_is_searchable);
    $('#attribute_is_required').prop('checked', parameter_attributes.is_required);
    $('#attribute_is_hidden').prop('checked', parameter_attributes.is_hidden);

    // handle 'price' part
    handle_price_part(parameter_attributes);
}

/**
 * populate default value dropdown with dictionary values
 * @param {int} dictionary_id Dictionary id to get values from
 */
function classic_parameter_populate_default_value_select(dictionary_id) {
    classic_parameter_dictionary_loading = true;
    if ('undefined' !== typeof dictionary_id && parseInt(dictionary_id)) {
        $.get(
            '/ajax/dictionary/' + parseInt(dictionary_id) + '/values',
            function(data) {
                var $default_value_select = $('#default_value_select');
                $default_value_select.find('option').remove();
                if (data && data.length) {
                    $default_value_select.append($('<option/>').attr('value', '0').text('No default value'));
                    $.each(data, function(index, value) {
                        $default_value_select.append($('<option/>').attr('value', value.id).text(value.name));
                    });
                }
                $default_value_select.selectpicker('refresh');
                classic_parameter_dictionary_loading = false;
            },
            'json'
        );
    }
}

/**
 * helper method for setting default value of the dropdown tag. In case tag is still loading data, the method will call
 * itself with a delay of 250ms
 * @param {int} value
 */
function classic_parameter_set_default_value_select(value) {
    if (classic_parameter_dictionary_loading) {
        setTimeout(function(){
            classic_parameter_set_default_value_select(value);
        }, 250);
    } else {
        $('#default_value_select').selectpicker('val', value);
    }
}

//----------------------------------------------------------------------------------------------------------------------
// CLASSIC PARAMETER METHODS - END
//----------------------------------------------------------------------------------------------------------------------
// DEPENDABLE PARAMETER METHODS - START
//----------------------------------------------------------------------------------------------------------------------

var dependable_parameter_min_number_of_levels = 2;
var $dependableParameter_ul = null;
var $dependableParameterContainer = null;
var dependable_parameter_dictionary_level_values = null;

/**
 * Initialize new dependable parameter (load its example values...)
 * @param {object}  dependable_parameter_attributes_json
 */
function dependable_parameter_init(dependable_parameter_attributes_json) {
    $dependableParameter_ul = $('ul.dependableParameter');
    $dependableParameterContainer = $dependableParameter_ul.parent();

    var dependable_parameter_dictionary_set_example_values = function(data) {
        dependable_parameter_dictionary_level_values = data;

        var currLevel = 1;
        if (dependable_parameter_dictionary_level_values) {
            for (var i = 0, l = dependable_parameter_dictionary_level_values.length; i < l; i++) {
                var $exampleTreeValues = $('#dp_level_' + currLevel + '_dictionary_values');
                if ($exampleTreeValues.length > 0) {
                    $exampleTreeValues.text(dependable_parameter_get_example_values(i));
                }
                currLevel++;
            }
        } else if ('undefined' !== typeof $dependableParameterContainer && parseInt($dependableParameterContainer.data('max-level'))) {
            for (var i = 0, l = parseInt($dependableParameterContainer.data('max-level')); i < l; i++) {
                var $exampleTreeValues = $('#dp_level_' + currLevel + '_dictionary_values');
                $exampleTreeValues.text('-');
                currLevel++;
            }
        }
    };

    // A single callback handles both types of ajax responses (dict/location)
    var dependable_dropdown_ajax_callback = function(data) {
        if (data.length >= dependable_parameter_min_number_of_levels) {
            dependable_parameter_dictionary_set_example_values(data);
        }
        if ('undefined' !== typeof dependable_parameter_attributes_json) {
            dependable_parameter_set_attributes_to_dialog(dependable_parameter_attributes_json);
        } else {
            dependable_parameter_set_attributes_to_dialog();
        }
    };

    if ('reset' !== dependable_parameter_attributes_json) {
        // first thing to do is to find out what kind of dependable parameter we're working with (dictionary or location)
        // handle location type of dependable
        if ($.trim($dependableParameterContainer.data('source')) == 'location' && parseInt($dependableParameterContainer.data('max-level'))) {
            // show all default values as we're using them here
            $dependableParameterContainer.find('div.dict-values')
                .removeClass('col-md-8').removeClass('col-lg-8')
                .addClass('col-md-4').addClass('col-lg-4');
            $dependableParameterContainer.find('div.default-value').removeClass('hidden');

            // ajax call and save data
            $.get(
                '/ajax/location/getExampleTreeValues',
                dependable_dropdown_ajax_callback,
                'json'
            );
        // find out if everything is setup (we know the dictionary-id, ... so we can prepopulate dictionary values as
        // helpers, so admins/moderators know what this level is about...)
        } else if (parseInt($dependableParameterContainer.data('dictionary-id')) && parseInt($dependableParameterContainer.data('max-level'))) {
            // hide all default values as we're not using them here
            $dependableParameterContainer.find('div.dict-values')
                .removeClass('col-md-4').removeClass('col-lg-4')
                .addClass('col-md-8').addClass('col-lg-8');
            $dependableParameterContainer.find('div.default-value').removeClass('hidden').addClass('hidden');

            // ajax call and save data
            $.get(
                '/ajax/dictionary/' + parseInt($dependableParameterContainer.data('dictionary-id')) + '/getExampleTreeValues',
                dependable_dropdown_ajax_callback,
                'json'
            );
        } else if ('undefined' !== typeof write_to_console && write_to_console) {
            console.error('dependable_parameter_init - not initialized', $dependableParameterContainer);
        }
    } else {
        dependable_parameter_dictionary_set_example_values(null);
        dependable_parameter_set_attributes_to_dialog();
    }
}

/**
 * Helper function to preset example values for specific level
 * @param {int} level_index
 * @return {string}
 */
function dependable_parameter_get_example_values(level_index) {
    if (dependable_parameter_dictionary_level_values && dependable_parameter_dictionary_level_values[level_index]) {
        return dependable_parameter_dictionary_level_values[level_index].join(', ');
    }
    return '';
}

/**
 * Preset dependable parameter's level attributes to dialog fields
 * @param {object} dependable_parameter_attributes_json
 */
function dependable_parameter_set_attributes_to_dialog(dependable_parameter_attributes_json) {
    if ('undefined' === typeof dependable_parameter_attributes_json) {
        dependable_parameter_attributes_json = {
            id: $('#parameter_id option:enabled:eq(0)').val(),
            price: null,
            price_currency_id: null,
            levels: [{
                label_text: '',
                default_value: null,
                help_text: null,
                is_searchable: 0,
                is_searchable_options: null,
                is_required: 0,
                is_hidden: 0
            },{
                label_text: '',
                default_value: null,
                help_text: null,
                is_searchable: 0,
                is_searchable_options: null,
                is_required: 0,
                is_hidden: 0
            }]
        };
    }
    if ('undefined' !== typeof dependable_parameter_attributes_json.levels) {
        $.each(dependable_parameter_attributes_json.levels, function(index, level_attributes) {
            var curr_parameter_level = index + 1;

            // find out if the tab already exists
            var $level_tab = $('#dp_level_' + curr_parameter_level);
            if ($level_tab.length > 0) {
                // tab exists so we will only pre-fill its settings
                dependable_parameter_edit_existing_level($level_tab, level_attributes);
            } else {
                // tab doesn't exist so we will add a new one and pre-fill it with this level's settings
                dependable_parameter_add_new_level($('#add_level_btn'), null, level_attributes, false);
            }
        });

        // check if maybe we have created more tabs than we have levels (tabs from some other dependable
        // parameter created before this call) - if so, delete them!

        var $all_tabs = $dependableParameterContainer.find('.tab-content .tab-pane');
        if ($all_tabs.length > 0) {
            if (dependable_parameter_attributes_json.levels.length < $all_tabs.length) {
                var starting_index = dependable_parameter_attributes_json.levels.length;
                var ending_index = $all_tabs.length;
                for (var ti = starting_index; ti < ending_index; ti++) {
                    $dependableParameterContainer.find('.tab-content .tab-pane:eq('+ti+')').hide().remove();
                    $dependableParameter_ul.find('li:eq('+ti+')').hide().remove();
                }
            }
        }

        // handle 'price' part
        handle_price_part(dependable_parameter_attributes_json);

        var $firstTab = $dependableParameter_ul.find('a[href="#dp_level_1"]');
        $firstTab.trigger('click');
        dependable_parameter_refresh();
    }
}

/**
 * This function will refresh how dependable parameter's level tabs are presented. It takes into account how many levels
 * are currently visible, whether to disable/enable 'add new level' button, puts 'delete tab' button on the last
 * deletable tab and so on...
 */
function dependable_parameter_refresh() {
    // enable/disable 'add new level' button
    var $add_new_level_button = $('#add_level_btn').parent();
    if ($add_new_level_button.length > 0) {
        if (($dependableParameter_ul.children().length - 1) >= parseInt($dependableParameterContainer.data('max-level'))) {
            // disable the 'add new level' button
            $add_new_level_button.addClass('disabled');
        } else {
            // enable the 'add new level' button
            $add_new_level_button.removeClass('disabled');
        }
    }

    // put delete button only on the last deletable level (level cannot be lower or equal than the defined minimun
    // number od levels -> dependable_parameter_min_number_of_levels)
    $dependableParameter_ul.find('span[data-type="delete"]').remove();
    if (($dependableParameter_ul.children().length - 1) > dependable_parameter_min_number_of_levels) {
        // we have at least one level above minimum number of levels so we place the remove button on the last
        // level that can be deleted

        $dependableParameter_ul.children('li:eq(' + ($dependableParameter_ul.children().length - 2) + ')').append(
            $('<span/>')
                .addClass('fa fa-times fa-fw text-danger')
                .attr('data-type', 'delete').data('type', 'delete')
                .html('<!--IE-->')
                .on('click', function(){
                    dependable_parameter_delete_level($(this));
                })
        );
    }
}

/**
 * Show dependable parameter's level settings tab
 * @param {object} $sender Tab which was clicked
 */
function dependable_parameter_show_level_tab($sender) {
    $sender.on('shown.bs.tab', function (e) {
        if ('#' !== $(e.target).attr('href')) {
            $($(e.target).attr('href') + ' input:eq(0)').focus();
        }
    }).on('show.bs.tab', function (e) {
        if (e.relatedTarget && $(e.currentTarget).hasClass('add-level')) {
            e.preventDefault();
            dependable_parameter_show_level_tab($(e.relatedTarget));
        }
    }).tab('show');
}

/**
 * Delete dependable parameter's level (tab and content). This method takes into account that there is at most only one
 * delete button and it is located only on the last level. In case a limit is reached (2), then, no delete buttons will
 * be rendered as we need at least 2 levels so we can link them (so they can depend on each-other's values)
 *
 * @param {object} $sender Delete button from the level tab that initiated the action
 */
function dependable_parameter_delete_level($sender) {
    var curr_number_of_levels = $dependableParameter_ul.children().length - 1;

    if (curr_number_of_levels > dependable_parameter_min_number_of_levels) {
        // delete tab and tab_content for $sender's tab
        var anchor = $sender.siblings('a');
        $(anchor.attr('href')).remove();
        $sender.parent().remove();

        // select last level's tab
        var $last_current_level = $dependableParameter_ul.children('li:eq(' + (curr_number_of_levels - 2) + ')').find('a');
        if ($last_current_level.length > 0) {
            $last_current_level.trigger('click');
        }
    }
    dependable_parameter_refresh();
}

/**
 * Add new dependable parameter's level
 *
 * @param {object} $sender                          'Add new level' button always initiates this call
 * @param {none|function} callback_function         Callback function we want when everything is done
 * @param {none|object} parameters_level_attributes Level attributes we want to pre-fill
 * @param {bool} preselect_tab                      Whether the tab should be selected
 */
function dependable_parameter_add_new_level($sender, callback_function, parameters_level_attributes, preselect_tab) {
    if ('undefined' === typeof preselect_tab) {
        preselect_tab = true;
    }

    var $dropdown = get_selected_parameter_dropdown();
    var $dropdown_selected = $dropdown.find(':selected');

    var max_number_of_levels = parseInt($dependableParameterContainer.data('max-level'));

    // this actually counts the 'add new level' also, but, we use 'add new level' as a 'placeholder' for next level ;)
    var next_number_of_levels = $dependableParameter_ul.children().length;

    if (next_number_of_levels <= max_number_of_levels) {
        // we have 'free' dictionary levels to add

        // as we have to construct a lot more elements in new tab-content, we will first built the tab-content
        // and after that, we will construct and preselect it's tab, so, let's start!
        var $tabContentContainer = $dependableParameter_ul.siblings('.tab-content');
        if ($tabContentContainer.length > 0) {

            if ('undefined' === typeof parameters_level_attributes) {
                parameters_level_attributes = {
                    label_text: '',
                    default_value: null,
                    help_text: null,
                    is_searchable: 0,
                    is_searchable_options: null,
                    is_required: 0,
                    is_hidden: 0
                };
            }

            // continue with building process...
            $tabContentContainer.append(
                $('<div/>')
                    .addClass('tab-pane fade' + (next_number_of_levels == 1 ? ' in' : ''))
                    .attr('id', 'dp_level_' + next_number_of_levels)
                    .attr('data-level', next_number_of_levels).data('level', next_number_of_levels)
                    .append(
                        $('<div/>')
                            .addClass('row')
                            .append(
                                $('<div/>')
                                    .addClass('col-md-4 col-lg-4')
                                    .append(
                                        $('<div/>')
                                            .addClass('form-group')
                                            .append(
                                                $('<label/>')
                                                    .addClass('control-label')
                                                    .attr('for', 'dp_level_' + next_number_of_levels + '_attribute_label_text')
                                                    .text('Label text')
                                            )
                                            .append(
                                                $('<input/>')
                                                    .addClass('form-control')
                                                    .attr('id', 'dp_level_' + next_number_of_levels + '_attribute_label_text')
                                                    .attr('maxlength', 64)
                                                    .attr('value', parameters_level_attributes.label_text).val(parameters_level_attributes.label_text)
                                            )
                                    )
                            )
                            .append(
                                $('<div/>')
                                    .addClass('col-md-8 col-lg-8')
                                    .append(
                                        $('<div/>')
                                            .addClass('form-group')
                                            .append(
                                                $('<label/>')
                                                    .addClass('control-label text-muted')
                                                    .text('Dictionary values')
                                            )
                                            .append(
                                                $('<p/>')
                                                    .addClass('form-control-static text-muted')
                                                    .attr('id', 'dp_level_' + next_number_of_levels + '_dictionary_values')
                                                    .text(dependable_parameter_get_example_values((next_number_of_levels - 1)))
                                            )
                                    )
                            )
                    )
                    .append(
                        $('<div/>')
                            .addClass('row')
                            .append(
                                $('<div/>')
                                    .addClass('col-md-12 col-lg-12')
                                    .append(
                                        $('<div/>')
                                            .addClass('form-group')
                                            .append(
                                                $('<label/>')
                                                    .addClass('control-label')
                                                    .attr('for', 'dp_level_' + next_number_of_levels + '_attribute_help_text')
                                                    .text('Help text')
                                            )
                                            .append(
                                                $('<input/>')
                                                    .addClass('form-control')
                                                    .attr('id', 'dp_level_' + next_number_of_levels + '_attribute_help_text')
                                                    .attr('maxlength', 256)
                                                    .attr('value', (parameters_level_attributes.help_text ? parameters_level_attributes.help_text : '')).val((parameters_level_attributes.help_text ? parameters_level_attributes.help_text : ''))
                                            )
                                    )
                            )
                    )
                    .append(
                        $('<div/>')
                            .addClass('row')
                            .append(
                                $('<div/>')
                                    .addClass('col-md-4 col-lg-4')
                                    .append(
                                        $('<div/>')
                                            .addClass('checkbox')
                                            .append(
                                                $('<label/>')
                                                    .append(
                                                        $('<input/>')
                                                            .attr('type', 'checkbox')
                                                            .attr('id', 'dp_level_' + next_number_of_levels + '_attribute_is_searchable')
                                                            .attr('data-container', 'dp_level_' + next_number_of_levels + '_is_searchable_container')
                                                            .data('container', 'dp_level_' + next_number_of_levels + '_is_searchable_container')
                                                            .prop('checked', (parameters_level_attributes.is_searchable ? true : false))
                                                            .on('change', function(){
                                                                is_searchable_toggle($(this));
                                                            })
                                                    )
                                                    .append(
                                                        document.createTextNode(' Is searchable')
                                                    )
                                            )
                                    )
                            )
                            .append(
                                $('<div/>')
                                    .addClass('col-md-4 col-lg-4')
                                    .append(
                                        $('<div/>')
                                            .addClass('checkbox')
                                            .append(
                                                $('<label/>')
                                                    .append(
                                                        $('<input/>')
                                                            .attr('type', 'checkbox')
                                                            .attr('id', 'dp_level_' + next_number_of_levels + '_attribute_is_required')
                                                            .prop('checked', (parameters_level_attributes.is_required ? true : false))
                                                    )
                                                    .append(
                                                        document.createTextNode(' Is required')
                                                    )
                                            )
                                    )
                            )
                            .append(
                                $('<div/>')
                                    .addClass('col-md-4 col-lg-4')
                                    .append(
                                        $('<div/>')
                                            .addClass('checkbox')
                                            .append(
                                                $('<label/>')
                                                    .append(
                                                        $('<input/>')
                                                            .attr('type', 'checkbox')
                                                            .attr('id', 'dp_level_' + next_number_of_levels + '_attribute_is_hidden')
                                                            .prop('checked', (parameters_level_attributes.is_hidden ? true : false))
                                                    )
                                                    .append(
                                                        document.createTextNode(' Is hidden')
                                                    )
                                            )
                                    )
                            )
                    )
                    .append(
                        $('<div/>')
                            .addClass('row' + (parameters_level_attributes.is_searchable ? '' : ' hidden'))
                            .attr('id', 'dp_level_' + next_number_of_levels + '_is_searchable_container')
                            .append(
                                $('<div/>')
                                    .addClass('is_searchable_options')
                                    .append(
                                        $('<div/>')
                                            .addClass('col-md-6 col-lg-6')
                                            .append(
                                                $('<div/>')
                                                    .addClass('form-group')
                                                    .append(
                                                        $('<label/>')
                                                            .addClass('control-label')
                                                            .attr('for', 'dp_level_' + next_number_of_levels + '_attribute_is_searchable_label_text')
                                                            .text('Search field label text')
                                                    )
                                                    .append(
                                                        $('<input/>')
                                                            .addClass('form-control')
                                                            .attr('id', 'dp_level_' + next_number_of_levels + '_attribute_is_searchable_label_text')
                                                            .val('')
                                                            .attr('maxlength', 32)
                                                    )
                                            )
                                    )
                                    .append(
                                        $('<div/>')
                                            .addClass('col-md-6 col-lg-6')
                                            .append(
                                                $('<div/>')
                                                    .addClass('form-group')
                                                    .append(
                                                        $('<label/>')
                                                            .addClass('control-label')
                                                            .attr('for', 'dp_level_' + next_number_of_levels + '_attribute_is_searchable_type')
                                                            .text('Search field type')
                                                    )
                                                    .append(
                                                        $('<select/>')
                                                            .addClass('form-control show-tick')
                                                            .attr('id', 'dp_level_' + next_number_of_levels + '_attribute_is_searchable_type')
                                                            .append(
                                                                $('#attribute_is_searchable_type option').clone()
                                                            )
                                                    )
                                            )
                                    )
                            )
                    )
            );
            // initialize selectpicker on the search filter's dropdown
            $('#dp_level_' + next_number_of_levels + '_attribute_is_searchable_type').selectpicker({
                style: 'btn btn-default',
                title: 'Filter type',
                noneSelectedText: 'Filter type',
                mobile: OGL.is_mobile_browser
            });
            // finish the building of the new tab
            $sender.closest('li').before(
                $('<li/>')
                    .append(
                        $('<a/>')
                            .attr('href', '#dp_level_' + next_number_of_levels)
                            .text('Level #' + next_number_of_levels)
                    )
                    .fadeIn('fast', function(){

                        set_available_is_searchable_types($('#dp_level_' + next_number_of_levels + '_attribute_is_searchable_type'), $dropdown_selected, parameters_level_attributes.is_searchable_options);
                        is_searchable_toggle($('#dp_level_' + next_number_of_levels + '_attribute_is_searchable'));

                        if (preselect_tab) {
                            $(this).find('a').trigger('click');
                            dependable_parameter_refresh();
                        }
                        if ('function' == typeof callback_function) {
                            callback_function();
                        }
                    })
            );
        }
    } else {
        // we exhausted all dictionary levels so we shouldn't reach this point at all... to fix this, we will (try) to
        // hide the 'add new level' button now so we won't be able to reach this state again.
        dependable_parameter_refresh();
    }
}

/**
 * Edit existing dependable parameter's level
 *
 * @param {object}        $level_tab                   Level tab we will work on..
 * @param {object}        parameters_level_attributes  Level attributes we want to prefill
 * @param {none|function} callback_function            Callback function we want when everything is done
 */
function dependable_parameter_edit_existing_level($level_tab, parameters_level_attributes, callback_function) {
    if ($level_tab.length > 0) {

        var $dropdown = get_selected_parameter_dropdown();
        var $dropdown_selected = $dropdown.find(':selected');
        var curr_tab_level = $level_tab.data('level');

        $('#dp_level_' + curr_tab_level + '_attribute_label_text').val(parameters_level_attributes.label_text);
        if (1 === parseInt($level_tab.data('level'))) {
            $('#location_default_value_select').selectpicker('val', (parameters_level_attributes.default_value ? parameters_level_attributes.default_value : ''));
        }
        $('#dp_level_' + curr_tab_level + '_attribute_help_text').val((parameters_level_attributes.help_text ? parameters_level_attributes.help_text : ''));
        $('#dp_level_' + curr_tab_level + '_attribute_is_searchable').prop('checked', (parameters_level_attributes.is_searchable ? true : false));
        set_available_is_searchable_types($('#dp_level_' + curr_tab_level + '_attribute_is_searchable_type'), $dropdown_selected, parameters_level_attributes.is_searchable_options);
        is_searchable_toggle($('#dp_level_' + curr_tab_level + '_attribute_is_searchable'));
        $('#dp_level_' + curr_tab_level + '_attribute_is_required').prop('checked', (parameters_level_attributes.is_required ? true : false));
        $('#dp_level_' + curr_tab_level + '_attribute_is_hidden').prop('checked', (parameters_level_attributes.is_hidden ? true : false));

        dependable_parameter_refresh();

        if ('function' == typeof callback_function) {
            callback_function();
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------
// DEPENDABLE PARAMETER - END
//----------------------------------------------------------------------------------------------------------------------
// UPLOADABLE PARAMETER METHODS - START
//----------------------------------------------------------------------------------------------------------------------

/**
 * Populate dialog fields with sent uploadable parameter's attributes
 * @param  {none|object} parameter_attributes
 */
function uploadable_parameter_set_attributes_to_dialog(parameter_attributes) {
    if ('undefined' === typeof parameter_attributes || 'reset' === parameter_attributes) {
        var param_is_searchable = 0;
        var param_is_required = 0;
        var param_is_searchable_options = null;
        var $dropdown = get_selected_parameter_dropdown();
        if ($dropdown.length > 0) {
            var $dropdown_selected = $dropdown.find(':selected');
            param_is_searchable = ('undefined' !== typeof $dropdown_selected.data('is-searchable') ? parseInt($dropdown_selected.data('is-searchable')) : 0);
            param_is_required = ('undefined' !== typeof $dropdown_selected.data('is-required') ? parseInt($dropdown_selected.data('is-required')) : 0);
            if (param_is_searchable && 'undefined' !== typeof $dropdown_selected.data('is-searchable-type')) {
                param_is_searchable_options = $.trim($dropdown_selected.data('is-searchable-type'));
            }
        }

        var first_enabled_option = $dropdown.find('option:enabled:eq(0)');
        parameter_attributes = {
            id: first_enabled_option.val(),
            type_id: first_enabled_option.data('type-id'),
            price: null,
            price_currency_id: null,
            label_text: '',
            button_text: '',
            help_text: null,
            max_items: null,
            is_searchable: param_is_searchable,
            is_searchable_options: param_is_searchable_options,
            is_required: param_is_required
        };
    }

    $('#uploadable_label_text').val(parameter_attributes.label_text);
    $('#uploadable_button_text').val(parameter_attributes.button_text);
    $('#uploadable_attribute_max_items').val((parameter_attributes.max_items ? parameter_attributes.max_items : '20'));
    $('#uploadable_attribute_help_text').val((parameter_attributes.help_text ? parameter_attributes.help_text : ''));

    var $uploadable_attr_is_searchable = $('#uploadable_attribute_is_searchable');
    $uploadable_attr_is_searchable.prop('checked', parameter_attributes.is_searchable);
    if (parameter_attributes.is_searchable_options) {
        $('#uploadable_attribute_is_searchable_label_text').val(parameter_attributes.is_searchable_options.label_text);
        var uploadable_is_searchable_default_value = parameter_attributes.is_searchable_options.default_value;
        $('#uploadable_attribute_is_searchable_default_value').prop('checked', ('undefined' !== typeof uploadable_is_searchable_default_value ? uploadable_is_searchable_default_value : false));
    }
    is_searchable_toggle($uploadable_attr_is_searchable);

    $('#uploadable_attribute_is_required').prop('checked', parameter_attributes.is_required);

    // handle 'price' part
    handle_price_part(parameter_attributes);
}

//----------------------------------------------------------------------------------------------------------------------
// UPLOADABLE PARAMETER METHODS - END
//----------------------------------------------------------------------------------------------------------------------
// INTERVAL PARAMETER METHODS - START
//----------------------------------------------------------------------------------------------------------------------

var interval_parameter_range = null;

/**
 * Handle change of interval value
 * @param {object} $sender
 */
function change_interval_value($sender) {
    if ($sender.length > 0) {
        var selectedIntervalValue = $sender.closest('li').data('type');
        var $intervalValueButton = $sender.closest('ul').siblings('button');
        if ($intervalValueButton.length > 0) {
            $intervalValueButton.find('span.name').text(selectedIntervalValue.slice(0, 1));
            var $inputElement = $sender.closest('.input-group-btn').siblings('input');
            if ($inputElement.length > 0) {
                $inputElement.attr('data-type', selectedIntervalValue).data('type', selectedIntervalValue);
            }
        }
    }
}

/**
 * Recalculate interval default value select box
 */
function interval_parameter_recalculate_default_value() {
    var interval_begin_value = parseInt($('#interval_begin').autoNumeric('get'));
    var interval_begin_period = $.trim($('#interval_begin').data('type'));
    var interval_end_value = parseInt($('#interval_end').autoNumeric('get'));
    var interval_end_period = $.trim($('#interval_end').data('type'));
    var interval_is_reverse = $('#interval_is_reverse').prop('checked');
    var tmp_interval_begin_date = interval_parameter_get_relative_date(interval_begin_value, interval_begin_period);
    var tmp_interval_end_date = interval_parameter_get_relative_date(interval_end_value, interval_end_period);
    if (tmp_interval_begin_date >= tmp_interval_end_date) {
        var interval_begin_date = tmp_interval_end_date;
        var interval_end_date = tmp_interval_begin_date;
    } else {
        var interval_begin_date = tmp_interval_begin_date;
        var interval_end_date = tmp_interval_end_date;
    }

    var interval_parameter_curr_range = interval_begin_date + '_' + interval_end_date + '_' + (interval_is_reverse ? 1 : 0);

    if (!interval_parameter_range || interval_parameter_range !== interval_parameter_curr_range) {
        interval_parameter_range = interval_parameter_curr_range;

        $('#interval_default_value option').remove();
        $('#interval_default_value').append($('<option/>').attr('value', '').text(''));
        if ('YEAR' === currently_selected_parameter_type) {
            var beginYear = interval_begin_date.getFullYear();
            var endYear = interval_end_date.getFullYear();

            if (endYear - beginYear > 250) {
                alert('Too big range');
            } else {
                if (interval_is_reverse) {
                    for (var currYear = endYear; currYear >= beginYear; currYear--) {
                        $('#interval_default_value').append(
                            $('<option/>').attr('value', currYear).text(currYear)
                        );
                    }
                } else {
                    for (var currYear = beginYear; currYear <= endYear; currYear++) {
                        $('#interval_default_value').append(
                            $('<option/>').attr('value', currYear).text(currYear)
                        );
                    }
                }
            }
        } else if ('MONTHYEAR' === currently_selected_parameter_type) {
            var months = (interval_end_date.getFullYear() - interval_begin_date.getFullYear()) * 12;
            months -= interval_begin_date.getMonth() + 1;
            months += interval_end_date.getMonth();

            if (months > 250) {
                alert('Too big range');
            } else {
                if (interval_is_reverse) {
                    var interval_curr_date = new Date(interval_end_date);
                    while (interval_curr_date >= interval_begin_date) {
                        var curr_month_string = ('0' + (interval_curr_date.getMonth() + 1)).slice(-2);
                        var curr_year_string = interval_curr_date.getFullYear().toString();
                        $('#interval_default_value').append(
                            $('<option/>')
                                .attr('value', curr_month_string + '/' + curr_year_string)
                                .text(curr_month_string + '/' + curr_year_string)
                        );
                        interval_curr_date.setMonth(interval_curr_date.getMonth() - 1);
                    }
                } else {
                    var interval_curr_date = new Date(interval_begin_date);
                    while (interval_curr_date <= interval_end_date) {
                        var curr_month_string = ('0' + (interval_curr_date.getMonth() + 1)).slice(-2);
                        var curr_year_string = interval_curr_date.getFullYear().toString();
                        $('#interval_default_value').append(
                            $('<option/>')
                                .attr('value', curr_month_string + '/' + curr_year_string)
                                .text(curr_month_string + '/' + curr_year_string)
                        );
                        interval_curr_date.setMonth(interval_curr_date.getMonth() + 1);
                    }
                }
            }
        }
        $('#interval_default_value').selectpicker('refresh');
    }
}

/**
 * Helper method for easy conversion of relative dates to absolute ones
 * @param {int} relative_number
 * @param {string} period
 * @return {date}
 */
function interval_parameter_get_relative_date(relative_number, period){
    var currDate = new Date();
    var currYear = currDate.getFullYear();
    var currMonth = currDate.getMonth();

    var targetDate;
    if ('Year' === period) {
        targetDate = new Date((currYear + parseInt(relative_number)), currMonth, 1, 0, 0, 0, 0);
    } else if ('Month' === period) {
        targetDate = new Date(currYear, (currMonth + parseInt(relative_number)), 1, 0, 0, 0, 0);
    } else if ('Number' === period) {
        targetDate = new Date(parseInt(relative_number), 0, 1, 0, 0, 0, 0);
    }

    return targetDate;
}

/**
 * Populate dialog fields with sent interval parameter's attributes
 * @param  {none|object} parameter_attributes
 */
function interval_parameter_set_attributes_to_dialog(parameter_attributes) {
    if ('undefined' === typeof parameter_attributes || 'reset' === parameter_attributes) {
        var param_is_searchable = 0;
        var param_is_searchable_options = null;
        var param_is_required = 0;
        var param_is_hidden = 0;
        var $dropdown = get_selected_parameter_dropdown();
        if ($dropdown.length > 0) {
            var $dropdown_selected = $dropdown.find(':selected');
            param_is_searchable = ('undefined' !== typeof $dropdown_selected.data('is-searchable') ? parseInt($dropdown_selected.data('is-searchable')) : 0);
            param_is_required = ('undefined' !== typeof $dropdown_selected.data('is-required') ? parseInt($dropdown_selected.data('is-required')) : 0);
            param_is_hidden = ('undefined' !== typeof $dropdown_selected.data('is-hidden') ? parseInt($dropdown_selected.data('is-hidden')) : 0);
            if (param_is_searchable && 'undefined' !== typeof $dropdown_selected.data('is-searchable-type')) {
                param_is_searchable_options = $.trim($dropdown_selected.data('is-searchable-type'));
            }
        }

        var first_enabled_option = $dropdown.find('option:enabled:eq(0)');
        parameter_attributes = {
            id: first_enabled_option.val(),
            type_id: first_enabled_option.data('type-id'),
            price: null,
            price_currency_id: null,
            label_text: '',
            placeholder_text: null,
            help_text: null,
            interval_begin_value: 0,
            interval_begin_period: 'Year',
            interval_end_value: 0,
            interval_end_period: 'Year',
            is_reverse: false,
            default_value: null,
            is_searchable: param_is_searchable,
            is_searchable_options: param_is_searchable_options,
            is_required: param_is_required,
            is_hidden: param_is_hidden
        };
    }

    var $number = $('ul.dropdown-menu li[data-type="Number"]');
    switch (parameter_attributes.type_id) {
        case 'MONTHYEAR':
            // hide 'Number' from dropdown
            $number.addClass('hidden');
            break;
        default:
            // show 'Number' from dropdown
            $number.removeClass('hidden');
    }

    $('#interval_label_text').val(parameter_attributes.label_text);
    $('#interval_placeholder_text').val((parameter_attributes.placeholder_text ? parameter_attributes.placeholder_text : ''));
    $('#interval_help_text').val((parameter_attributes.help_text ? parameter_attributes.help_text : ''));

    $('#interval_begin')
        .attr('data-type', parameter_attributes.interval_begin_period)
        .data('type', parameter_attributes.interval_begin_period)
        .val((parameter_attributes.interval_begin_value ? parameter_attributes.interval_begin_value : 0));
    $('#interval_begin_btn span.name').text(parameter_attributes.interval_begin_period.slice(0, 1));
    $('#interval_end')
        .attr('data-type', parameter_attributes.interval_end_period)
        .data('type', parameter_attributes.interval_end_period)
        .val((parameter_attributes.interval_end_value ? parameter_attributes.interval_end_value : 0));
    $('#interval_end_btn span.name').text(parameter_attributes.interval_end_period.slice(0, 1));

    $('#interval_is_reverse').prop('checked', parameter_attributes.is_reverse);

    var $is_searchable = $('#interval_is_searchable');
    $is_searchable.prop('checked', parameter_attributes.is_searchable);
    set_available_is_searchable_types($('#interval_is_searchable_type'), $dropdown_selected, parameter_attributes.is_searchable_options);
    is_searchable_toggle($is_searchable);

    $('#interval_is_required').prop('checked', parameter_attributes.is_required);
    $('#interval_is_hidden').prop('checked', parameter_attributes.is_hidden);

    // handle 'price' part
    handle_price_part(parameter_attributes);
}

//----------------------------------------------------------------------------------------------------------------------
// INTERVAL PARAMETER METHODS - END
//----------------------------------------------------------------------------------------------------------------------

function handle_price_part(param_attrs) {
    $('#attribute_has_price').prop('checked', (param_attrs.price && parseFloat(param_attrs.price) > 0 ? true : false));
    var $price_opts = $('#has_price_container').find('.has_price_options');
    if (param_attrs.price && parseFloat(param_attrs.price) > 0) {
        $('#attribute_price').val(param_attrs.price).autoNumeric('set', param_attrs.price);
        $price_opts.removeClass('hidden');
    } else {
        $('#attribute_price').val('0').autoNumeric('set', 0);
        $price_opts.addClass('hidden');
    }
}

$(document).ready(function(){
    $('#addNewParameter').on('click', function(){show_parameter_attributes_dialog();});
    $('#addNewFieldset').on('click', function(){add_new_fieldset();});
    $('#reorderFilters').on('click', function(){show_reorder_filters_dialog();});
    $('button.btn-fieldset-settings').on('click', function(){fieldset_settings_button_click($(this));});
    $('#fieldsetSettingsModalSaveBtn').on('click', function(){fieldset_save_settings_from_dialog();});
    $('#reorderFiltersModalSaveBtn').on('click', function(){refresh_category_filters(true);});
    $('.btn-fieldset-delete').on('click', function(){delete_fieldset_button_click($(this));});
    $('.list-sortable .actions-cat span[data-action=options]').on('click', function(){edit_parameter_options($(this));});
    $('.list-sortable .actions-cat span[data-action=delete]').on('click', function(){delete_parameter_from_fieldset($(this));});
    $('li.fieldset .panel-heading input[type="text"]').on('change', function(){
        refresh_category_fieldsets_and_parameters();
    });

    $('#parameter_slug_refresh').click(function(){
        var $parameter_dropdown = get_selected_parameter_dropdown();
        if ('undefined' !== typeof $parameter_dropdown && $parameter_dropdown.length) {
            var selected_parameter_option = $parameter_dropdown.find('option:selected');
            if ('undefined' !== typeof selected_parameter_option && 'undefined' !== typeof selected_parameter_option.data('slug')) {
                $('#parameter_slug').val(selected_parameter_option.data('slug'));
            }
        }
    });

    $('#choosen_categories_id').selectpicker({
        style: 'btn btn-default',
        size: 8,
        liveSearch: true,
        title: 'Select categories',
        mobile: OGL.is_mobile_browser
    });
    // choose additional categories that should be parametrized with same structure
    $('#choosen_categories_id').change(function(){
        var chosen_categories = [];
        chosen_categories.push(parseInt($('#current_category_id').val()));
        var additional_categories = $('#choosen_categories_id').val();

        $.each(additional_categories, function(i, id){
            chosen_categories.push(parseInt(id));
        });
        $('#chosen_categories').val(chosen_categories.join(','));
        check_category_fieldsets_and_parameters_changes();
    });

    var $parameter_id = get_selected_parameter_dropdown();
    $parameter_id.selectpicker({
        style: 'btn btn-default',
        size: 8,
        liveSearch: true,
        title: 'Select parameter',
        mobile: OGL.is_mobile_browser
    });

    $('#default_value_date').datepicker({
        format: "dd.mm.yyyy",
        weekStart: 1,
        language: "hr",
        autoclose: true,
        todayHighlight: true
    });

    $('#default_value_select, #interval_default_value').selectpicker({
        style: 'btn btn-default',
        size: 8,
        liveSearch: true,
        title: 'Set default value',
        noneSelectedText: 'Set default value',
        mobile: OGL.is_mobile_browser
    });

    $('#attribute_currency').selectpicker({
        style: 'btn btn-default',
        title: 'Currency',
        noneSelectedText: 'Currency',
        mobile: OGL.is_mobile_browser
    });

    $('#location_default_value_select').selectpicker({
        style: 'btn btn-default',
        size: 8,
        liveSearch: true,
        title: 'Country',
        noneSelectedText: 'Select country',
        mobile: OGL.is_mobile_browser
    });

    $('#addNewParameterModal').on('shown.bs.modal', function (e) {
        parameter_dialog_is_visible = true;
    })
    .on('hidden.bs.modal', function (e) {
        parameter_dialog_is_visible = false;
    });

    $('#deleteFieldsetModalYesBtn').on('click', function(){
        delete_fieldset();
    });

    $('input.is_searchable_toggle').on('change', function(){
        is_searchable_toggle($(this));
    });

    $('select.filter-select-type').selectpicker({
        style: 'btn btn-default',
        title: 'Filter type',
        noneSelectedText: 'Filter type',
        mobile: OGL.is_mobile_browser
    });

    // change parameter's options based on its type
    $parameter_id.on('change', function(){
        var $selected_parameter = $(this[this.selectedIndex]);
        handle_parameter_select_change($selected_parameter);
    });

    $('#fieldset_id').selectpicker({
        style: 'btn btn-default',
        size: 8,
        title: 'Select fieldset',
        mobile: OGL.is_mobile_browser
    });

    drag_n_drop_reorder_fields($('#fieldset_sorter'));
    drag_n_drop_reorder_parameters($('#fieldset_sorter .list-sortable ul.sortable'));
    drag_n_drop_reorder_filters($('#filter_sorter'));

    // create first fieldset (empty one) if there are no fieldsets currently
    if ($('#fieldset_sorter').find('li').length == 0) {
        add_new_fieldset();
    }

    $('#attribute_has_price').on('change', function(){
        var $price_options = $('#has_price_container').find('.has_price_options');
        if ($(this).prop('checked')) {
            $price_options.removeClass('hidden');
        } else {
            $price_options.addClass('hidden');
        }
    });
    $('#attribute_price').autoNumeric('init', {aSep: '.', aDec: ',', wEmpty: 'zero', lZero: 'deny'});

    $('#btnSaveParameterModal').on('click', function(){
        btnSaveParameterModalClick();
    });

    $('#btnSaveCategoryParameters').on('click', function(){
        refresh_category_fieldsets_and_parameters();
        if (category_fieldsets_and_parameters_has_changes || parametrize_multiple_categories) {
            $('#category_fieldsets_and_parameters').val(JSON.stringify(category_fieldsets_and_parameters_final));
            $('#frm_category_parameterization').submit();
        }
    });

//--[Dependable parameter specific setup ]--------------------------------------
    $('#add_level_btn').on('click', function(e) {
        e.preventDefault();
        if (!$(this).parent().hasClass('disabled')) {
            dependable_parameter_add_new_level($(this));
        }
    });
    $('#dependableParameter').find('.nav-tabs').on('click', 'a', function(e){
        e.preventDefault();
        dependable_parameter_show_level_tab($(this));
    });
//------------------------------------------------------------------------------
    $('#intervalParameter').find('ul.dropdown-menu li a').on('click', function(e){
        e.preventDefault();
        change_interval_value($(this));
    });
    $('#interval_begin, #interval_end').autoNumeric('init', {aSep: '.', aDec: ',', vMin: '-10000', vMax: '10000', mDec: '0', wEmpty: 'zero', lZero: 'deny'});
    // as bootstrap-select currently doesn't have any events like onClick or
    // similar, we have to attach onClick event on its created element...
    // This is not a good approach but it works for now...
    $('button[data-id="interval_default_value"]').on('click', function(){
        interval_parameter_recalculate_default_value();
    });

    var initial_refresh_status = refresh_category_fieldsets_and_parameters();
});
