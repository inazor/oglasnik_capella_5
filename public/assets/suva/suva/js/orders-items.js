 $(document).ready(function(){
    
    // ctl - kontrola
    // val - vrijednost kontrole
          
    var ctlPublications = $('#n_publications_id')
    var valPublications = ctlPublications.val()  //ne može ovdje jer je konstanta koja se ne mijenja
    
    var ctlProducts = $('#n_products_id')
    var valProducts = ctlProducts.val()   //ne može ovdje jer je konstanta koja se ne mijenja
    
    var ctlFirstPublishedAt = $("#n_first_published_at")
    
    var ctlSelSpotDiscounts = $('#sel-spot-discounts')
    var ctlSelSpotIssues = $('#sel-spot-issues')
    
    
    var valAdId = $("#ad_id").val()
    var valIsDisplayAd = $("#is_display_ad").val()
    
    var ctlPublicationIsOnline = $("#publication_is_online")
    var valPublicationIsOnline = ctlPublicationIsOnline.val()

    var txtPostPublications = '/admin/ads/publications/'  + valPublications  + '/'+ valAdId  + '/'+ $('#n_categories_id').val() // treba refreshat ne može ovdje jer se treba mijenjat dinamički
    var txtPostProducts = '/admin/discounts-products/product/' + valProducts                                         //         --//--
     
    
    function checked_issues(){
      $("input:checkbox").change(function(){
        var  numberOfChecked = $('input:checkbox.issues_checkbox:checked').length;
//         console.log($(".issues_checkbox"))
//         console.log(numberOfChecked)
        $(".issues_count").html("<b>Checked: </b>"+numberOfChecked)
        
        })
      
      $('select').on('change', function (e) {
          var  check = $('input:checkbox.issues_checkbox:checked').length;
          $(".issues_count").html("<b>Checked: </b>"+check)
      });
        
        
    }
    
    function emptyForm(){
        ctlSelSpotIssues.empty();
        ctlSelSpotDiscounts.empty();
        ctlProducts.empty();
        ctlProducts.append($("<option></option>").attr("value", '0').text("--")); 
        
    }
    
    function makeProductsDropDown(pArrProducts){
        
        ctlProducts.empty();
        
        ctlProducts.append($("<option></option>").attr("value", '0').text("-- Select Product --")); 
        for (var i = 0; i < pArrProducts.length; i = i+1) {
            var lProd = pArrProducts[i]
            if(valProducts == lProd.id){
                ctlProducts.append($("<option></option>").attr({ value : lProd.id, selected : true }).text(lProd.id + ' - ' + lProd.name + ' - ' + lProd.unit_price + ' kn')); 
            } else {
                ctlProducts.append($("<option></option>").attr({ value : lProd.id }).text(lProd.id + ' - ' + lProd.name + ' - ' + lProd.unit_price + ' kn')); 
            }
        }
        
    } 
    
    function makeListOfDiscounts (pArray, pParent){
        selItems = "<div id='select-discounts'>";
        for (var j = 0; j < pArray.length; j++) {
            var lItem = pArray[j]
            selItems +='<div><input type="checkbox" name="offline-discounts[]" value="' + lItem.id + '"><label for="offline-discounts"><span class="text-primary">' + lItem.name + '</span></label></div>';
        };
        selItems += "</div>";
        pParent.append(selItems);
    }
     
    function makeListOfIssues (pArray, pParent){
        console.log("loaded issues")
        selItems = "<div id='select-issues'>";
        for (var j = 0; j < pArray.length; j++) {
            var lItem = pArray[j]
            selItems +='<div><input class="issues_checkbox" type="checkbox" name="offline-issues[]" value="' + lItem.id + '"><label for="offline-issues"><span class="text-primary">' + lItem.name + '</span></label></div>';
            console.log(pArray[j])
        };
        selItems += "</div>";
        pParent.append(selItems);
        
        checked_issues()
      
        
    }
     
    function enablePubDateEntry ( yesNo ){
//          console.log("1 data.publication_rec" +yesNo)
//         if(yesNo == 1 ){
//             console.log("2 data.publication_rec.is_online readonly")
//             $(".dropdown-menu").show()
//             ctlFirstPublishedAt.removeAttr("readonly"); 
// //              ctlFirstPublishedAt.datepicker({
// //                                   format: 'dd.mm.yyyy',
// //                                   weekStart: 1,
// //                                   language: 'hr',
// //                                   autoclose: true
// //              });
//         } else {
//             console.log("3 else")
//             ctlFirstPublishedAt.attr("readonly", true)
// //             ctlFirstPublishedAt.datepicker("destroy");datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-bottom
// //             console.log(ctlFirstPublishedAt.attr('class'))
// //             ctlFirstPublishedAt.removeClass('form-control')
// //             ctlFirstPublishedAt.datepicker("disable");
//             console.log($(".datepicker"))
//             $(".datepicker").removeClass("")
            
//         }   
    }
     
  
    //dodavanje metode datepicker na datumsko polje
     if (ctlFirstPublishedAt.val() == '01.01.1970') {
         
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
        dd='0'+dd
        } 

        if(mm<10) {
        mm='0'+mm
        } 

        today = dd+'.'+mm+'.'+yyyy;
        ctlFirstPublishedAt.val(today)
         
     }
    ctlFirstPublishedAt.datepicker({
                                  format: 'dd.mm.yyyy',
                                  weekStart: 1,
                                  language: 'hr',
                                  autoclose: true
             });
     
    ctlFirstPublishedAt.click(function(){
        if (valPublicationIsOnline == 0 ) {$(".datepicker-dropdown").hide()}
        
        
    }) 
    //$(".datepicker").hide();  //ne radi
    
    //ako nije odabran publications, briše popis proizvoda, popis izdanja, popis popusta
    if (valPublications == "0"){
        emptyForm();
    } 
     
    //na početku osvježiti editabilnost prozora za datum
    enablePubDateEntry(valPublicationIsOnline);
     
    //broj checkiranih nakon loadanja
    numberOfChecked = $('input:checkbox:checked').length;
    
    
    
    //kad se promijeni publikacija, treba ponovo učitati proizvode
    ctlPublications.change(function(){ 
        //alert('publicationChange');
        
        valPublications = ctlPublications.val()  //treba osvježit jer se ne mijenja sama
        txtPostPublications = '/admin/ads/publications/'  + ctlPublications.val()  + '/'+ valAdId  + '/'+ valIsDisplayAd
//         alert('change');
        //alert(valPublications) // ovo se  ne mijenja dinamički
        //alert (ctlPublications.val())  // ovo se mijenaj dinamčki
        
        var selItems = "";
         
        ctlSelSpotDiscounts.empty();
        ctlSelSpotIssues.empty();

//         products_select($(this).val()); //ne radi alert
       
        
         $.post( txtPostPublications, 
                { '_csrftoken': $('#_csrftoken').val() },
                function (data) {
                    
                    //omogućavanje /onemogućavanje editiranja datuma objave 
                    if(data.publications_rec){
                        ctlPublicationIsOnline.attr("value",data.publications_rec.is_online)
                        valPublicationIsOnline = ctlPublicationIsOnline.val()
                        //alert(valPublicationIsOnline)
                        enablePubDateEntry(valPublicationIsOnline) 
                    }
             
                    if(data.issues){
                        makeListOfIssues(data.issues,ctlSelSpotIssues)
                        console.log("loaded")
                    }else{
                        ctlSelSpotIssues.append("<p>Nema dostupnih datuma.</p>");
                    }
             
                    if(data.products){ 
                        //alert('makeproductdropdown');
                        makeProductsDropDown (data.products) 
                    }  
             
             
                },
                'json'
            );
    })
     
    //kad se odabere ptoizvod učitava listu popusta
    ctlProducts.change(function(){
       
       valProducts = ctlProducts.val() //treba osvježit jer se nemijenja dinamički
       ctlSelSpotDiscounts.empty();
       //alert(valProducts) //radi kad se mijenja proizvod
       
        if (valProducts == "0"){  exit;  } 
       
        //traži listu discounta za odabrani proizvod
        valPublications = ctlPublications.val()
        var txtPostProducts = '/admin/discounts-products/product/' + valProducts + '/' + $('#n_publications_id').val()
        //alert(txtPostProducts)
        
        $.post( 
            txtPostProducts, 
            {'_csrftoken': $('#_csrftoken').val() },
            function (data) {
                //alert ("stigao odgovor")

                if(data.discounts) 
                {
                    makeListOfDiscounts(data.discounts, ctlSelSpotDiscounts)
                } else {
                    ctlSelSpotDiscounts.append("<p>No discounts.</p>");
                }
            },
            'json'
        );
    });
     
     

if($(".checked_issues")){
    var  checked = $('input.checked_issues:checked').length;
    
    $(".issues_count").html("<b>Checked: </b>"+checked)
    
       $("input:checkbox").change(function(){

            var  checked2 = $('input.checked_issues:checked').length;
            $(".issues_count").html("<b>Checked: </b>"+checked2)
        
        })   
}

      function date_picker_hours(){
            var hours = Number($( "#first_published_hours" ).val())
            var minutes = Number($( "#first_published_minutes" ).val())
			var seconds = Number($( "#first_published_seconds" ).val())
            var date_picker = $('#n_first_published_at')
            
            if(hours == '') hours = 0
            if(minutes == '') minutes = 0
			if(seconds == '') seconds = 0
			
			
			if(seconds > '' || seconds >= 0){
                if(seconds > 59){
                    $( "#first_published_seconds" ).val(0)
                        seconds = 0
                }
                if(seconds < 0){
                    $( "#first_published_seconds" ).val(0)
                    seconds = 0  
                } 
            }
            
            if(minutes > '' || minutes >= 0){
                if(minutes > 59){
                    $( "#first_published_minutes" ).val(0)
                        minutes = 0
                }
                if(minutes < 0){
                    $( "#first_published_minutes" ).val(0)
                    minutes = 0  
                } 
            }

            if(hours > '' || hours >= 0){
                if(hours > 23){
                    $( "#first_published_hours" ).val(0)
                    hours = 0
                } 
                if(hours < 0){
                    $( "#first_published_hours" ).val(0)
                    hours = 0  
                } 
                hours.toString()
                minutes.toString()
				seconds.toString()
                if(hours < 10){
                    if(minutes < 10){
						minutes = '0'+minutes
					} 
					if(seconds < 10){
						seconds = '0'+seconds
					} 
                    hours = '0'+hours+':'+minutes+':'+seconds
                }else if(hours >= 10 && hours <= 24){
                    if(minutes < 10){
						minutes = '0'+minutes
					} 
					if(seconds < 10){
						seconds = '0'+seconds
					} 
                    hours = hours+':'+minutes+':'+seconds
                }
				console.log(hours)
                var date_picker_split = date_picker.val().split(" ");
                date_picker.val(date_picker_split[0] + ' ' +hours) 
                date_picker.attr('value',date_picker_split[0] + ' ' +hours)
				// console.log(date_picker.text())

            }
        }

     $( "#first_published_hours" ).change(function() {
        date_picker_hours()
    });
    
     $( "#first_published_minutes" ).change(function() {
        date_picker_hours()
    });
	
	$( "#first_published_seconds" ).change(function() {
        date_picker_hours()
    });
        
        date_picker_hours()
       
            
 });

 
