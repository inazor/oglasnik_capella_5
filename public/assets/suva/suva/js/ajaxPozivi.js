	// UČITAVA JE WRAPPER
	//alert("prije");
	function testAjaxPozivi(){
		alert('test');
	}

	function ajaxPushToContext ( pParam , pValue = null ){
		//alert("tu sam 1");
		if ( pValue === null ){
			var lUrl = '/suva/ajax/pushToContext/' + pParam;
		}
		else {
			var lUrl = '/suva/ajax/pushToContext/' + pParam + '/' + pValue;
		}
		console.log("slanje " + lUrl);
		$.ajax({
			url     : lUrl,
			type    : 'POST',
			data	: null,

			success	: function( data, status, jqXHR ){
				//alert("tu sam 2");
				// console.log ("prošlo je");
				// console.log(data._context);
			},
			
			error	: function( jqXHR, textStatus, errorThrown ){
				//ako je Forbidden baca na root stranicu di će se ponovo logirati
				alert("tu sam 3");
				
				
				if (errorThrown == 'Forbidden'){
					//alert ('treba logirat se ponovo');
					//substring(errorthrown, )
					if (gServerUrl )window.location.replace(gServerUrl);
					else window.location.replace( window.location.hostname );
				}
				else {
					alert(
						'ajaxPozivi V2:\n 4 SERVER RETURNED ERROR: ' + textStatus  + '\n'
						+ 'URL: ' + lUrl + '\n'
						+ 'ERROR TEXT: ' + errorThrown + '\n'
						
						);
					alert('jqXHR:' + jqXHR  + ' typeof:' + typeof(jqXHR));
				}
				console.log(jqXHR);
			}
		});		
	}
	
	function ajaxSubmit( 
		pUrl
		, pWindowId = null
		, pFormId = null
		, pCallerUrl = null
		, pDebug = false
		, pBlinkBorder = true
		, pFunctionToExecuteAfterLoad = null 
		, pFillHtmlElementWithResponse = true 

		) {	
			
		ajaxSubmitP ({
			url : pUrl
			,windowId : pWindowId
			,formId : pFormId
			,callerUrl : pCallerUrl
			,isDebug : pDebug
			,blinkBorder : pBlinkBorder
			,functionToExecuteAfterLoad : pFunctionToExecuteAfterLoad
			,isFillHtmlElementWithResponse : pFillHtmlElementWithResponse
		});
	
	}
	
/*	
	KORIŠTENJE
	ajaxSubmitP ( { 
			url: '{{_url}}'
			,windowId: '{{ _window_id }}'
			,formId: '{{ _form_id }}'
			,callerUrl : {{ ( _caller_url is defined ? "'" ~ _url ~ "'" : 'null' ) }} 
			,functionToExecuteAfterLoad: {{ ( _function_to_execute_after_load is defined ? "'" ~ _function_to_execute_after_load ~ "'" : 'null' ) }}
			,functionsToExecuteAfterSuccess : [ { name: 'alert', p:'blabla'},{ name: 'ads2RefreshAd', p: { adId: {{ad.id}}, userId: {{ad.user_id}} , isDisplayAd : {{ ( ad.n_is_display_ad ? 'true' : 'false' ) }} , isRefreshAd : false , isRefreshUsersContacts : false, isRefreshPublicationsAndIssues : false }, }, ]
			});
*/	
	function ajaxSubmitP( p ){

		if (!("url" in p)) return ;
		if (!("windowId" in p)) p.windowId = null;
		if (!("formId" in p)) p.formId = null;
		if (!("callerUrl" in p)) p.callerUrl = null;
		if (!("isDebug" in p)) p.isDebug = false;
		if (!("isBlinkBorder" in p)) p.isBlinkBorder = true;
		if (!("functionToExecuteAfterLoad" in p)) p.functionToExecuteAfterLoad = null;
		if (!("isFillHtmlElementWithResponse" in p)) p.isFillHtmlElementWithResponse = true;
		if (!("functionsToExecuteAfterSuccess" in p)) p.functionsToExecuteAfterSuccess = null;
		
		
		
//, pEntityId = null , pUserId = null
		/*
			pUrl - adresa kontrolera na koji se šalje poziv
			pWindowId - ID wrappera u kojem će se ubaciti primljeni sadržaj
			pFormId - ID forme čiji će se sadržaj poslati kontrolleru
			pCallerUrl - ????

			ako se ne navede pWindowId - funkcija ne čeka odgovor.

		*/
        // console.log(pUrl);
		//alert ('blinkborder ' + pBlinkBorder);
		var data_to_send;
		var request_type;
		var wrapper_id = '_wrapper_' + p.windowId;
		if ( p.formId === null){
			lUrl = p.url + '?_window_id=' + p.windowId;
			if (p.callerUrl) lUrl = lUrl + '&_caller_url=' + p.callerUrl;
			request_type = 'GET';
			data_to_send = null;
			if (p.isDebug) alert('ajaxSubmit:NO FORM, reqiest= GET');
		}
		else {
			lUrl = p.url;
			request_type = 'POST';
			data_to_send = $('#' + p.formId).serialize();
			if (p.debug) alert("ajaxSubmit:FORM DATA: for form: " + p.formId + ":" + data_to_send);
		}
		if (p.isDebug) 
			alert(
				'ajaxSubmit: purl:' + p.url 
				+ ' ajaxSubmit:: pFormId:' + p.formId 
				+ ', lUrl: ' + lUrl 
				+ ', pCallerUrl:' + p.callerUrl 
				+ ', wrapper_id: ' + wrapper_id 
				+ ', data_to_send:' 
				+ data_to_send 
				+ ', request_type:' + request_type
			);

		$.ajax({
			url     : lUrl,
			type    : request_type,
			data	: data_to_send,


			success	: function( data, status, jqXHR ){
					if (typeof(data) == 'object'){
						if ( typeof(data.html) == 'string' && p.windowId ){
							if (p.isDebug)  alert ("RECEIVED HTML(1): " + data.html);
							if (p.isDebug)  alert ("WRAPPER: " + wrapper_id);
							
							if (p.isFillHtmlElementWithResponse){
								//ako je false, ne očekuje html stranicu kao odgovor, samo poruke i sl.	
								$('#' + wrapper_id ).html(data.html);
								
								if ( p.isBlinkBorder == true ){
								var borderPrije = $('#' + wrapper_id ).css('border');
								$( '#' + wrapper_id ).css('border', '2px solid green');
									setTimeout(function(){
										$( '#' + wrapper_id ).css('border', borderPrije);
										}, 200);		
								}
							} 

							
							//funkciije koje se izvršavaju nakon što stigne odgovor sa servera
							var lFunctions = p.functionsToExecuteAfterSuccess;
							// console.log (lFunctions);
							if ( $.isArray( lFunctions ) ){
								for ( i = 0; i < lFunctions.length ; i++ ){
									var lFunction = p.functionsToExecuteAfterSuccess[ i ];
									if ( typeof ( lFunction ) == 'object' ){
										if ( 'name' in lFunction ){
											if ( 'p' in lFunction ){
												window[ lFunction.name ]( lFunction.p );
											}
											else{
												window[ lFunction.name ]();
											}
										}
									}
								}
							}
							
							//Ako je poslano ime funkcije koja će se izvršiti nakon loadanja - stari način
							if ( p.functionToExecuteAfterLoad != null){
								//alert('p.functionToExecuteAfterLoad = ' + p.functionToExecuteAfterLoad );
								window[ p.functionToExecuteAfterLoad ]();
								
								/*treba poslat i parametre funkcije*/
								
							}
							
							//ako ima u _context.messages_system - dodat u div - bijeli
							//ako ima u _context.messages_info - dodat u div - zeleni...
							// console.log(data._context)
							if (data._context){
								
								/*
								
								ić po _context.messages
									za svaki vidit koji mu je key. Ako je je error onda crveno, inače zeleno
								
								*/
								var msgs = data._context._msg_flashsession;
								if( typeof( msgs ) == 'object' ){
									// console.log('object')
									for(i = 0; i < msgs.length; i++){
										if(msgs[i]['error'] != undefined ){
											// console.log('error')
											var error_message = $('<div>'+msgs[i]['error']+'</div>')
											error_message.attr( 'class' , 'status status-error alert alert-danger');
											$('.ajax_flash_session').append(error_message)
											$('.flash_sessions_box').show()
											$('.flash_session_resize').show()
											$('.flash_session_remove').show()
											$('.flash_sessions_box').scrollTop($('.flash_sessions_box')[0].scrollHeight);
										}
										else if(msgs[i]['system'] != undefined){
											// console.log('system or info')
											var system_message = $('<div>'+msgs[i]['system']+'</div>')
											system_message.attr( 'class' , 'status status-success alert alert-success');
											$('.ajax_flash_session').append(system_message)
											$('.flash_sessions_box').show()
											$('.flash_session_resize').show()
											$('.flash_session_remove').show()
											$('.flash_sessions_box').scrollTop($('.flash_sessions_box')[0].scrollHeight);
										}
										else if(msgs[i]['info'] != undefined){
											// console.log('system or info')
											var info_message = $('<div>'+msgs[i]['info']+'</div>')
											info_message.attr( 'class' , 'status status-success alert alert-success');
											$('.ajax_flash_session').append(info_message)
											$('.flash_sessions_box').show()
											$('.flash_session_resize').show()
											$('.flash_session_remove').show()
											$('.flash_sessions_box').scrollTop($('.flash_sessions_box')[0].scrollHeight);
										}	
									}
								}
							}
						}
					}
					else {
						if (data.substring(1,4) == 'PDF'){
							//pdf SE NE MOŽE POSLATI PREKO JSONA KAO DIO KEY-VALUE ARRAY-A, JAVLJA GREŠKU 
							//baci ga na novi ekran
							alert('PDF ');

							 var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/='
							  var o1
							  var o2
							  var o3
							  var h1
							  var h2
							  var h3
							  var h4
							  var bits
							  var i = 0
							  var ac = 0
							  var enc = ''
							  var tmpArr = []

							 var stringToEncode = data

							  stringToEncode = unescape(encodeURIComponent(stringToEncode))

							  do {
								// pack three octets into four hexets
								o1 = stringToEncode.charCodeAt(i++)
								o2 = stringToEncode.charCodeAt(i++)
								o3 = stringToEncode.charCodeAt(i++)

								bits = o1 << 16 | o2 << 8 | o3

								h1 = bits >> 18 & 0x3f
								h2 = bits >> 12 & 0x3f
								h3 = bits >> 6 & 0x3f
								h4 = bits & 0x3f

								// use hexets to index into b64, and append result to encoded string
								tmpArr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4)
							  } while (i < stringToEncode.length)

							  enc = tmpArr.join('')

							  var r = stringToEncode.length % 3

							  var dataBase64 = (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3)

							
							
							var datauri = 'data:application/pdf;base64,' + dataBase64;
							var win = window.open("", "Your PDF", "width=1024,height=768,resizable=yes,scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no");
							win.document.location.href = datauri;
						}
						else {
							$('#' + wrapper_id ).html(data);
							if ( p.blinkBorder == true ){ 
								var borderPrije = $('#' + wrapper_id ).css('border');
								$( '#' + wrapper_id ).css('border', '2px solid green');
									setTimeout(function(){
										$( '#' + wrapper_id ).css('border', borderPrije);
										}, 200);
							}
							if ( p.functionToExecuteAfterLoad != null){
								//alert('pFunctionToExecuteAfterLoad = ' + pFunctionToExecuteAfterLoad );
								window[ p.functionToExecuteAfterLoad ]();
							}
							
							if (data._context){
								
								/* 
								
								ić po _context.messages
									za svaki vidit koji mu je key. Ako je je error onda crveno, inače zeleno
								
								*/
								var msgs = data._context._msg_flashsession;
								if(typeof( msgs ) == 'object'){
									// console.log('object')
									for(i = 0; i < msgs.length; i++){
										if(msgs[i]['error'] != undefined ){
											// console.log('error')
											var error_message = $('<div>'+msgs[i]['error']+'</div>')
											error_message.attr( 'class' , 'status status-error alert alert-danger');
											$('.ajax_flash_session').append(error_message)
											$('.flash_sessions_box').show()
											$('.flash_session_resize').show()
											$('.flash_session_remove').show()
											$('.flash_sessions_box').scrollTop($('.flash_sessions_box')[0].scrollHeight);
										}else if(msgs[i]['system'] != undefined){
											// console.log('system or info')
											var system_message = $('<div>'+msgs[i]['system']+'</div>')
											system_message.attr( 'class' , 'status status-success alert alert-success');
											$('.ajax_flash_session').append(system_message)
											$('.flash_sessions_box').show()
											$('.flash_session_resize').show()
											$('.flash_session_remove').show()
											$('.flash_sessions_box').scrollTop($('.flash_sessions_box')[0].scrollHeight);
										}else if(msgs[i]['info'] != undefined){
											// console.log('system or info')
											var info_message = $('<div>'+msgs[i]['info']+'</div>')
											info_message.attr( 'class' , 'status status-success alert alert-success');
											$('.ajax_flash_session').append(info_message)
											$('.flash_sessions_box').show()
											$('.flash_session_resize').show()
											$('.flash_session_remove').show()
											$('.flash_sessions_box').scrollTop($('.flash_sessions_box')[0].scrollHeight);
										}	
									}
								}
							}

							// dataArr = JSON.parse(data);
							if (p.isDebug)  alert ("RECEIVED: " + data);
							
							// console.log(data);
							// console.log( JSON.parse(data) );
						}
					}
				},
			
			error: function( jqXHR, textStatus, errorThrown ){
						 
					//ako je Forbidden baca na root stranicu di će se ponovo logirati
					if (errorThrown == 'Forbidden'){
						//alert ('treba logirat se ponovo');
						if ( gServerUrl ) window.location.replace( gServerUrl );
						else window.location.replace( window.location.hostname );
					}
					else {
						alert(
						'ajaxPozivi V2:\n 3 SERVER RETURNED ERROR: ' + textStatus  + '\n'
						+ 'URL: ' + lUrl + '\n'
						+ 'ERROR TEXT: ' + errorThrown + '\n'
						
						);
						alert('jqXHR:' + jqXHR  + ' typeof:' + typeof(jqXHR));
					}
					console.log(jqXHR);
				}
		});		
	}
	
	function ajaxSubmitOLD( 
		pUrl
		, pWindowId
		, pFormId = null
		, pCallerUrl = null
		, pDebug = false
		, pBlinkBorder = true
		, pFunctionToExecuteAfterLoad = null 
		, pFillHtmlElementWithResponse = true 

		) {
//, pEntityId = null , pUserId = null
		/*
			pUrl - adresa kontrolera na koji se šalje poziv
			pWindowId - ID wrappera u kojem će se ubaciti primljeni sadržaj
			pFormId - ID forme čiji će se sadržaj poslati kontrolleru
			pCallerUrl - ????

			ako se ne navede pWindowId - funkcija ne čeka odgovor.

		*/
        // console.log(pUrl);
		//alert ('blinkborder ' + pBlinkBorder);
		var data_to_send;
		var request_type;
		var wrapper_id = '_wrapper_' + pWindowId
		if ( pFormId === null){


			lUrl = pUrl + '?_window_id=' + pWindowId
			if (pCallerUrl) lUrl = lUrl + '&_caller_url=' + pCallerUrl;
			request_type = 'GET';
			data_to_send = null;
			if (pDebug) alert('ajaxSubmit:NO FORM, reqiest= GET');
		}
		else {
			lUrl = pUrl;
			request_type = 'POST';
			data_to_send = $('#' + pFormId).serialize();
			if (pDebug) alert("ajaxSubmit:FORM DATA: for form: " + pFormId + ":" + data_to_send);
		}
		
		if (pDebug) alert('ajaxSubmit: purl:' + pUrl + ' ajaxSubmit:: pFormId:' + pFormId + ', lUrl: ' + lUrl + ', pCallerUrl:' + pCallerUrl + ', wrapper_id: ' + wrapper_id + ', data_to_send:' + data_to_send + ', request_type:' + request_type);

		$.ajax({
			url     : lUrl,
			type    : request_type,
			data	: data_to_send,


			success	: function( data, status, jqXHR ){
						//alert("success");
						//alert (gServerUrl);
						if (typeof(data) == 'object'){
							if ( typeof(data.html) == 'string' && pWindowId ){
								if (pDebug)  alert ("RECEIVED HTML(1): " + data.html);
								if (pDebug)  alert ("WRAPPER: " + wrapper_id);
								
								if (pFillHtmlElementWithResponse){
									//ako je false, ne očekuje html stranicu kao odgovor, samo poruke i sl.	
									$('#' + wrapper_id ).html(data.html);
									
									if ( pBlinkBorder == true ){
									var borderPrije = $('#' + wrapper_id ).css('border');
									$( '#' + wrapper_id ).css('border', '2px solid green');
										setTimeout(function(){
											$( '#' + wrapper_id ).css('border', borderPrije);
											}, 200);		
									}
								} 
								//Ako je poslano ime funkcije koja će se izvršiti nakon loadanja
								if ( pFunctionToExecuteAfterLoad != null){
									//alert('pFunctionToExecuteAfterLoad = ' + pFunctionToExecuteAfterLoad );
									window[ pFunctionToExecuteAfterLoad ]();
									
									/*treba poslat i parametre funkcije*/
									
								}
								//ako ima u _context.messages_system - dodat u div - bijeli
								//ako ima u _context.messages_info - dodat u div - zeleni...
								// console.log(data._context)
								if (data._context){
									
									/*
									
									ić po _context.messages
										za svaki vidit koji mu je key. Ako je je error onda crveno, inače zeleno
									
									*/
									var msgs = data._context._msg_flashsession;
									if(typeof( msgs ) == 'object'){
										// console.log('object')
										for(i = 0; i < msgs.length; i++){
											if(msgs[i]['error'] != undefined ){
												// console.log('error')
												var error_message = $('<div>'+msgs[i]['error']+'</div>')
												error_message.attr( 'class' , 'status status-error alert alert-danger');
												$('.ajax_flash_session').append(error_message)
												$('.flash_sessions_box').show()
												$('.flash_session_resize').show()
												$('.flash_session_remove').show()
												//$('.flash_sessions_box').scrollTop($('.flash_sessions_box')[0].scrollHeight);
											}else if(msgs[i]['system'] != undefined){
												// console.log('system or info')
												var system_message = $('<div>'+msgs[i]['system']+'</div>')
												system_message.attr( 'class' , 'status status-success alert alert-success');
												$('.ajax_flash_session').append(system_message)
												$('.flash_sessions_box').show()
												$('.flash_session_resize').show()
												$('.flash_session_remove').show()
												//$('.flash_sessions_box').scrollTop($('.flash_sessions_box')[0].scrollHeight);
											}else if(msgs[i]['info'] != undefined){
												// console.log('system or info')
												var info_message = $('<div>'+msgs[i]['info']+'</div>')
												info_message.attr( 'class' , 'status status-success alert alert-success');
												$('.ajax_flash_session').append(info_message)
												$('.flash_sessions_box').show()
												$('.flash_session_resize').show()
												$('.flash_session_remove').show()
												//$('.flash_sessions_box').scrollTop($('.flash_sessions_box')[0].scrollHeight);
											}	
										}
									}
									

								}
								
								

								
							}
							//alert (typeof(data.messages));

							// for(i = 0; i < data.messages.length; i++){
								// alert(data.messages[i]);
								// var succes_message = $('<div><pre>'+data.messages[i]+'</pre></div>')
								// succes_message.attr( 'class' , 'status status-success alert alert-success');
								// $('.flash_session').append(succes_message)

							// }
							// for(j = 0; j < data.errors.length; j++){
								// alert(data.errors[j]);
								// var error_message = $('<div><pre>'+data.errors[j]+'</pre></div>')
								// error_message.attr( 'class' , 'status status-error alert alert-danger');
								// $('.flash_session').append(error_message)

							// }
							// if(data.messages.length > 0 || data.errors.length > 0) $('.flash_session').show()

						}
						else {
							if (data.substring(1,4) == 'PDF'){
								//pdf SE NE MOŽE POSLATI PREKO JSONA KAO DIO KEY-VALUE ARRAY-A, JAVLJA GREŠKU 
								//baci ga na novi ekran
								alert('PDF ');

								
								
								
								 var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/='
								  var o1
								  var o2
								  var o3
								  var h1
								  var h2
								  var h3
								  var h4
								  var bits
								  var i = 0
								  var ac = 0
								  var enc = ''
								  var tmpArr = []

								 var stringToEncode = data

								  stringToEncode = unescape(encodeURIComponent(stringToEncode))

								  do {
									// pack three octets into four hexets
									o1 = stringToEncode.charCodeAt(i++)
									o2 = stringToEncode.charCodeAt(i++)
									o3 = stringToEncode.charCodeAt(i++)

									bits = o1 << 16 | o2 << 8 | o3

									h1 = bits >> 18 & 0x3f
									h2 = bits >> 12 & 0x3f
									h3 = bits >> 6 & 0x3f
									h4 = bits & 0x3f

									// use hexets to index into b64, and append result to encoded string
									tmpArr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4)
								  } while (i < stringToEncode.length)

								  enc = tmpArr.join('')

								  var r = stringToEncode.length % 3

								  var dataBase64 = (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3)

								
								
								var datauri = 'data:application/pdf;base64,' + dataBase64;
								var win = window.open("", "Your PDF", "width=1024,height=768,resizable=yes,scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no");
								win.document.location.href = datauri;
								

							}

							else {

								$('#' + wrapper_id ).html(data);
								if ( pBlinkBorder == true ){ 
									var borderPrije = $('#' + wrapper_id ).css('border');
									//console.log(borderPrije);
									$( '#' + wrapper_id ).css('border', '2px solid green');
										setTimeout(function(){
											$( '#' + wrapper_id ).css('border', borderPrije);
											//$( '#' + wrapper_id ).css('border', borderPrije );
											}, 200);
								}
								if ( pFunctionToExecuteAfterLoad != null){
									//alert('pFunctionToExecuteAfterLoad = ' + pFunctionToExecuteAfterLoad );
									window[ pFunctionToExecuteAfterLoad ]();
									
								}
								
								if (data._context){
									
									/*
									
									ić po _context.messages
										za svaki vidit koji mu je key. Ako je je error onda crveno, inače zeleno
									
									*/
									var msgs = data._context._msg_flashsession;
									if(typeof( msgs ) == 'object'){
										// console.log('object')
										for(i = 0; i < msgs.length; i++){
											if(msgs[i]['error'] != undefined ){
												// console.log('error')
												var error_message = $('<div>'+msgs[i]['error']+'</div>')
												error_message.attr( 'class' , 'status status-error alert alert-danger');
												$('.ajax_flash_session').append(error_message)
												$('.flash_sessions_box').show()
												$('.flash_session_resize').show()
												$('.flash_session_remove').show()
												$('.flash_sessions_box').scrollTop($('.flash_sessions_box')[0].scrollHeight);
											}else if(msgs[i]['system'] != undefined){
												// console.log('system or info')
												var system_message = $('<div>'+msgs[i]['system']+'</div>')
												system_message.attr( 'class' , 'status status-success alert alert-success');
												$('.ajax_flash_session').append(system_message)
												$('.flash_sessions_box').show()
												$('.flash_session_resize').show()
												$('.flash_session_remove').show()
												$('.flash_sessions_box').scrollTop($('.flash_sessions_box')[0].scrollHeight);
											}else if(msgs[i]['info'] != undefined){
												// console.log('system or info')
												var info_message = $('<div>'+msgs[i]['info']+'</div>')
												info_message.attr( 'class' , 'status status-success alert alert-success');
												$('.ajax_flash_session').append(info_message)
												$('.flash_sessions_box').show()
												$('.flash_session_resize').show()
												$('.flash_session_remove').show()
												$('.flash_sessions_box').scrollTop($('.flash_sessions_box')[0].scrollHeight);
											}	
										}
									}
									

								}

								// dataArr = JSON.parse(data);
								if (pDebug)  alert ("RECEIVED: " + data);

								// console.log(data);
								// console.log( JSON.parse(data) );
							}
						}

					},
			error	: function( jqXHR, textStatus, errorThrown ){
						
						 
						//ako je Forbidden baca na root stranicu di će se ponovo logirati
						if (errorThrown == 'Forbidden'){
							//alert ('treba logirat se ponovo');
							
							if (gServerUrl )window.location.replace(gServerUrl);
							else window.location.replace( window.location.hostname );
						}
						else {
							alert(
								'ajaxPozivi V2:\n 2 SERVER RETURNED ERROR: ' + textStatus  + '\n'
								+ 'URL: ' + lUrl + '\n'
								+ 'ERROR TEXT: ' + errorThrown + '\n'
								);
							alert('jqXHR:' + jqXHR  + ' typeof:' + typeof(jqXHR));
						}
						console.log(jqXHR);
					}
		});
	}

	function ajaxSubmitV2( pUrl, pWindowId = null, pFormId = null, pCallerUrl = null ) {
		
		/*
			pUrl - adresa kontrolera na koji se šalje poziv
			pWindowId - ID wrappera u kojem će se ubaciti primljeni sadržaj
			pFormId - ID forme čiji će se sadržaj poslati kontrolleru
			pCallerUrl - ????

			ako se ne navede pWindowId - funkcija ne čeka odgovor.
		*/
		
		
		var data_to_send;
		var request_type;
		var wrapper_id = '_wrapper_' + pWindowId
		if ( pFormId === null){
			lUrl = pUrl + '?_window_id=' + pWindowId
			if (pCallerUrl) lUrl = lUrl + '&_caller_url=' + pCallerUrl;
			request_type = 'GET';
			data_to_send = null;
		}
		else {
			lUrl = pUrl;
			request_type = 'POST';
			data_to_send = $('#' + pFormId).serialize();
		}

		alert('purl:' + pUrl + 'ajaxSubmit: pFormId:' + pFormId + ', lUrl: ' + lUrl + ', pCallerUrl:' + pCallerUrl + ', wrapper_id: ' + wrapper_id + ', data_to_send:' + data_to_send + ', request_type:' + request_type);




		$.ajax({
			url     : lUrl,
			type    : request_type,
			data	: data_to_send,


			success	: function( data, status, jqXHR ){
						//alert("success");
						//alert('success, data: ' + data);
						//alert("success: status:" + status);
						//dataArr = JSON.parse(data);
						//console.log( dataArr );
						//alert(data.substring(1,3) == 'PDF');
						if (data.substring(1,4) == 'PDF'){
							//baci ga na novi ekran
							alert('PDF ');

						}

						else {
							if (pWindowId){
								$('#' + wrapper_id ).html(data);
								var borderPrije = $('#' + wrapper_id ).css('border');
								//console.log(borderPrije);
								$( '#' + wrapper_id ).css('border', '2px solid green');
									setTimeout(function(){
										$( '#' + wrapper_id ).css('border', borderPrije);
										//$( '#' + wrapper_id ).css('border', borderPrije );
										}, 200);
								// dataArr = JSON.parse(data);
								// console.log(data);
								// console.log( JSON.parse(data) );
							}
						}
					},
			error	: function( jqXHR, textStatus, errorThrown ){
				
						//ako je Forbidden baca na root stranicu di će se ponovo logirati
						if (errorThrown == 'Forbidden'){
							alert ('treba logirat se ponovo');
							
							if (gServerUrl )window.location.replace(gServerUrl);
							else window.location.replace( window.location.hostname );
						}
						else {
							alert(
								'ajaxPozivi V2:\n 1 SERVER RETURNED ERROR: ' + textStatus  + '\n'
								+ 'URL: ' + lUrl + '\n'
								+ 'ERROR TEXT: ' + errorThrown + '\n'
								+ 'jqXHR:' + jqXHR  + ', type:' + typeof(jqXHR)
								);
						}
						console.log(jqXHR);
				
					}
		});
	}
	

/*
ads2RefreshAd({
	adId : {{ad.id}}
	, userId : {{ad.user_id}}
	, isDisplayAd : '{{ ( ad.n_is_display_ad ? 'true' : 'false' ) }}'
	, submitForm : 'form_subPublicationsAndIssuesAjax'
	, isRefreshAd : true
	, isRefreshUsersContacts : true
	})


*/


function ads2RefreshAd( p  ){
	//alert ("ad id" + adId);
	if ( !("adId" in p)) p.adId = null;
	if ( !("userId" in p)) p.userId = null;
	if ( !("isDisplayAd" in p)) p.isDisplayAd = 0;
	if ( !("submitForm" in p)) p.submitForm = null;
	if ( !("isDebug" in p)) p.isDebug = false;
	if ( !("isRefreshAd" in p)) p.isRefreshAd = true;
	if ( !("isRefreshUsersContacts" in p)) p.isRefreshUsersContacts = true;
	if ( !("isRefreshInsertions" in p)) p.isRefreshInsertions = true;
	if ( !("isRefreshModeration" in p)) p.isRefreshModeration = true;
	if ( !("isRefreshPublicationsAndIssues" in p)) p.isRefreshPublicationsAndIssues = true;

	
	// console.log ("ads2RefreshAd:");
	// console.log (p);
	if (p.isDisplayAd == "0") p.isDisplayAd = 0;
	if (p.isDisplayAd == "1") p.isDisplayAd = 1;
	
	//Namještanje "varijable" za tekući ad;
	$('#var_current_ad_id').attr('value', p.adId );
	$('#var_current_ad_is_display').attr('value', (p.isDisplayAd ? 1 : 0) );
	if (p.userId > 0){
		$('#var_current_user_id').attr('value', p.userId );
	}
	
	//Namještanje tekućih tabova (jel ovo čemu služi?)
	$('#_context_active_tab').val('single_ad');
	$('#_context_active_tab_subInsertions').val('single_ad');
	$('#_context_active_tab_subPublicationsAndIssues').val('single_ad');
	$('#_context_active_tab_subAdBasic').val('single_ad');
	
	// console.log (p);
	//var adText = ( p.isDisplayAd ? ' Display Ad ' : ' Ad ' ) + p.adId;
	if (p.isDisplayAd) {
		var adText = 'Display Ad';
		//console.log("AD TEXT JE :"+ adText);
	} else {
		var adText = 'Ad';
		//console.log("AD TEXT JE :"+ adText);
	}
	adText = adText + ' ' + p.adId;
	//alert(adText );
	$('#single_ad_link').text( adText );
	// console.log(adText);
	var htmlLoading = "<h1><br/><br/><br/>Loading..</h1>";

	if ( p.isRefreshAd == true ){
		//$('#_wrapper_crudSingleAd').html( htmlLoading );

		if ( p.isDisplayAd ){
			ajaxSubmit( 
				'/suva/ads2/crudDisplay/' + p.adId 
				,'crudSingleAd'
				, ( p.submitForm == 'frmCrudDisplay1' ? 'frmCrudDisplay1' : null)
				, null
				, p.isDebug
				, true
				, 'ctlFileUploadAjax'
				);
		}
		else{
			ajaxSubmit( 
				'/suva/ads2/crudBasic/' + p.adId 
				, 'crudSingleAd'
				, ( p.submitForm == 'frmCrudBasic' ? 'frmCrudBasic' : null)
				, null
				, p.isDebug
				, true
				, 'ctlFileUploadAjax'
				);
		}
	}
	
	if (p.isRefreshInsertions){
		//$('#_wrapper_subInsertionsSingle').html( htmlLoading );

		/*
		ajaxSubmit( 
			'/suva/ads2/subInsertions/' + p.adId
			,'subInsertionsSingle'
			, ( p.submitForm == 'frm_subInsertions' ? 'frm_subInsertions' : null)
			, null
			, p.isDebug
			, true
			, null
			);
		*/
		ajaxSubmitP ({
			url : '/suva/ads2/subInsertions/' + p.adId
			,windowId : 'subInsertionsSingle'
			,formId : ( p.submitForm == 'frm_subInsertions' ? 'frm_subInsertions' : null)
			,callerUrl : null
			,isDebug : p.isDebug
			,blinkBorder : true
			,functionToExecuteAfterLoad : null
			,isFillHtmlElementWithResponse : true
		});
	}
	
	if (p.isRefreshPublicationsAndIssues){
		//$('#_wrapper_subPublicationsAndIssuesSingle').html( htmlLoading );
		ajaxSubmit( 
			'/suva/ads2/subPublicationsAndIssuesAjax/' + p.adId
			,'subPublicationsAndIssuesSingle'
			, ( p.submitForm == 'form_subPublicationsAndIssuesAjax' ? 'form_subPublicationsAndIssuesAjax' : null)
			, null
			, p.isDebug
			, true
			, null
			);
		}
	
	if (p.isRefreshModeration){
		//$('#_wrapper_subModerationSingle').html( htmlLoading );

		ajaxSubmit( 
			'/suva/ads2/subModeration/' + p.adId
			,'subModerationSingle'
			, ( p.submitForm == 'frm_subModeration' ? 'frm_subModeration' : null)
			, null
			, p.isDebug
			, true
			, null
			);
	} 
	
	if ( p.isRefreshUsersContacts ){
		//$('#_wrapper_subUsersContactsSingle').html( htmlLoading );
		ajaxSubmit( 
			'/suva/users-contacts/index/' + p.userId
			,'subUsersContactsSingle'
			, ( p.submitForm == 'subUsersContactsSingle' ? 'subUsersContactsSingle' : null)
			, null
			, p.isDebug
			, true
			, null
			);
	}
}

function resetPublicationsAndIssuesForm( p ){
	document.getElementById('form_subPublicationsAndIssuesAjax').reset();
}

	


