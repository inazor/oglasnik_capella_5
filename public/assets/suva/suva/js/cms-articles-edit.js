function openMediaBrowser(popup_url) {
    var mediaLibPopup = window.open(popup_url, 'mediaLibPopup', 'location=no,menubar=no,status=no,scrollbars=yes,titlebar=no,toolbar=no');
}

function handle_popup_return(data) {
    uploadable_item_append(data, '#uploadable_sorter');
}

$(document).ready(function(){
    $('#slug').generateSlug('#title');

    $('#category_id').selectpicker({
        style: 'btn btn-default',
        size: 8,
        liveSearch: true,
        title: 'Select category',
        noneResultsText: 'No categories match',
        mobile: OGL.is_mobile_browser
    });

    var $publish_date = $('#publish_date');

    if ($.trim($publish_date.val())) {
        $publish_date.val(moment($publish_date.val()).format('DD.MM.YYYY HH:mm'));
    }
    $publish_date.datetimepicker();
    $('textarea#content').ckeditor({
        customConfig: '/assets/backend/js/ckeditor_config.js',
        realPreview_Url: '/preview/magazine',
        realPreview_FieldName: 'content',
        realPreview_AdditionalFields: 'category_id;title;excerpt;meta_title;meta_description;meta_keywords;publish_date;article_media_gallery[]',
        imageBrowser_custom_source: {
            name: 'Article media',
            field: 'article_media_gallery[]'
        }
    });

    var articleTags = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        dupDetector: function(remoteMatch, localMatch) {
            return remoteMatch.name === localMatch.name;
        },
        prefetch: '/ajax/availableTags',
        remote: '/ajax/availableTags/%QUERY'
    });
    $('#tags').tagsinput({
        confirmKeys: [9, 13, 188],
        allowDuplicates: false,
        freeInput: true,
        typeaheadjs: {
            hint: false,
            name: 'article-tags',
            displayKey: 'name',
            valueKey: 'name',
            source: articleTags
        }
    });

    $('#importMedia').click(function(){
        openMediaBrowser($(this).data('url'));
    });
});
