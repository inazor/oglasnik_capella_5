/**
 * This is basic helper nested items set sorter file.
 *
 * With every change of the tree, this helper writes current order of the items. It checks to see if there are any
 * changes withing the tree and if so it can even show/hide specific $element (Save changes button, etc.).
 *
 * If you want to get current tree structure, you need the 'nested_tree_final_ids' variable which is actually an object
 * with nested structure. You can easily convert it to JSON with 'JSON.stringify(nested_tree_final_ids)' and use it with
 * POST or AJAX...
 *
 * If you would like this helper to handle $element automatic showing/hiding then, you should include your page specific
 * javascript file AFTER this helper and in onDocumentReady set the 'nested_tree_controllable_button' variable with
 * selector of your element that you want to control.
 */

var nested_tree_initial_ids = [];
var nested_tree_final_ids = [];
var nested_tree_initial_serialization = '';
var nested_tree_last_serialization = '';
var nested_tree_has_changes = false;
var nested_tree_controllable_button = '';

function nested_tree_check_changes() {
    if (nested_tree_last_serialization != nested_tree_initial_serialization) {
        nested_tree_rebuild_indexes();
        nested_tree_has_changes = true;
    } else {
        nested_tree_final_ids = nested_tree_initial_ids;
        nested_tree_has_changes = false;
    }
    nested_tree_controllable_button_handle();
}

function nested_tree_controllable_button_handle() {
    if ($.trim(nested_tree_controllable_button)) {
        var $button = $(nested_tree_controllable_button);
        if ($button.length) {
            if (nested_tree_has_changes && !$button.is(':visible')) {
                $button.fadeIn('fast');
            } else if (!nested_tree_has_changes && $button.is(':visible')) {
                $button.fadeOut('fast');
            }
        }
    }
}

function nested_tree_rebuild_indexes() {
    nested_tree_final_ids = $('.sortable').nestedSortable('toArray', {startDepthCount: 1});
}

$(document).ready(function(){
    $('.children').click(function(){
        var $children = $(this);
        var $subcategory = $(this).closest('.nested-item-li').find('.sublist').filter(':first');
        if ($subcategory.is(':visible')) {
            $subcategory.slideUp('fast', function(){
                $children.find('.toggle').removeClass('fa-minus-square-o').addClass('fa-plus-square-o');
                $children.closest('.nested-item-li').addClass('no-nest');
            });
        } else {
            $subcategory.slideDown('fast', function(){
                $children.find('.toggle').removeClass('fa-plus-square-o').addClass('fa-minus-square-o');
                $children.closest('.nested-item-li').removeClass('no-nest');
            });
        }
    });

    // check if a hidden field exists and use that value as rootID nestedSortable config
    var tree_root_id = null;
    var $tree_root_id_hidden_field = $('#tree_root_id');
    if ($tree_root_id_hidden_field.length > 0) {
        tree_root_id = $tree_root_id_hidden_field.val();
    }

    $('.sortable').nestedSortable({
        disableNesting: 'no-nest',
        forcePlaceholderSize: true,
        handle: '.drag-me',
        helper: 'clone',
        listType: 'ul',
        items: 'li',
        opacity: .6,
        placeholder: 'placeholder',
        revert: 250,
        tabSize: 25,
        rootID: tree_root_id,
        tolerance: 'pointer',
        toleranceElement: '> div',
        create: function(event, ui) {
            nested_tree_rebuild_indexes();
            nested_tree_initial_serialization = $('.sortable').nestedSortable('serialize');
            nested_tree_initial_ids = nested_tree_final_ids;
        },
        stop: function(event, ui) {
            nested_tree_last_serialization = $('.sortable').nestedSortable('serialize');
            nested_tree_check_changes();
        }
    });

    // Adding a quick UI helper (toggle all expand/collapse buttons)
    // TODO: should we keep state for each user in a cookie/localStorage/something-else?
    var $sortable = $('.nested-list-sortable');
    var $sortable_expandables = $sortable.find('.children.clickable');
    var $expand_collapse_button = $('#toggle-all');
    var click_all_pluses = function () {
        $sortable_expandables.trigger('click');
    };
    if ($sortable_expandables.length > 0) {
        $expand_collapse_button.prop('disabled', false);
        $expand_collapse_button.on('click', function (e) {
            $(this).data('clicked', !$(this).data('clicked'));
            e.preventDefault();
            click_all_pluses();
        });
    } else {
        $expand_collapse_button.prop('disabled', true);
    }
});
