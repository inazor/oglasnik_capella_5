var offlineDescTpl_value = '';
var offlineDescTpl_data = [];
var offlineDescTpl_available_parameters = [];
var offlineDescTpl_selected_parameters = [];
var offlineDescTpl_errorMsg = "Please check all used parameters as it seems that some \nof them are not attached to current category anymore!";

function offlineDescTpl_refreshParameters() {
    offlineDescTpl_data = [];
    offlineDescTpl_available_parameters = [];
    $('.list-sortable li').each(function(p){
        if ('undefined' !== typeof $(this).attr('data-parameter-attributes')) {
            $(this).data('parameter-attributes', $(this).attr('data-parameter-attributes'));
            var parameter_attributes = JSON.parse($(this).data('parameter-attributes'));

            var tpl_type = 'parameter';
            var tpl_param_id = parseInt(parameter_attributes.id);
            if ('undefined' !== typeof parameter_attributes.levels) {
                if ('LOCATION' === parameter_attributes.type_id) {
                    tpl_type = 'location';
                }
                $.each(parameter_attributes.levels, function(index, level_attributes) {
                    var current_row = {
                        value: level_attributes.label_text,
                        uid: tpl_type + ':' + tpl_param_id + '_' + (index + 1)
                    };
                    offlineDescTpl_data.push(current_row);
                    offlineDescTpl_available_parameters.push(tpl_param_id);
                });
            } else {
                var current_row = {
                    value: parameter_attributes.label_text,
                    uid: tpl_type + ':' + tpl_param_id
                };
                offlineDescTpl_data.push(current_row);
                offlineDescTpl_available_parameters.push(tpl_param_id);
            }
        }
    });
}

function offlineDescTpl_modalShow() {
    $('#offlineDescTplModal').modal({
        backdrop: 'static',
        keyboard: true
    });
}

function offlineDescTpl_getTplMatches() {
    var main_regex = /\@\[([^\]]+)\]\((parameter|location):([\d_]*)+\)/gim;
    var inner_regex = /\@\[([^\]]+)\]\((parameter|location):([\d_]*)+\)/i;

    var globalMatch = offlineDescTpl_value.match(main_regex);
    var matchArray = new Array();
    for (var i in globalMatch) {
        if (nonGlobalMatch = inner_regex.exec(globalMatch[i])) {
            matchArray.push(nonGlobalMatch);
        }
    }
    return matchArray;
}

function offlineDescTpl_getSelectedParameters(show_warning) {
    if ('undefined' === typeof show_warning) {
        show_warning = false;
    }
    var has_warnings = false;

    var matchArray = offlineDescTpl_getTplMatches();
    if (matchArray.length) {
        offlineDescTpl_selected_parameters = [];
        for (var i in matchArray) {
            var matched_parameter_id = parseInt(matchArray[i][3].split('_')[0]);
            if (-1 === offlineDescTpl_available_parameters.indexOf(matched_parameter_id)) {
                has_warnings = true;
                var $warning_elem = $('.mentions-input .highlighter-content strong:contains("' + matchArray[i][1] + '")');
                if ('undefined' !== typeof $warning_elem) {
                    $warning_elem.addClass('error');
                }
                offlineDescTpl_selected_parameters.push(matched_parameter_id);
            }
        }
    }

    return has_warnings;
}

$(document).ready(function(){
    offlineDescTpl_value = $('#offlineDescTplModal textarea').val();
    $('#offlineDescTplModal').on('show.bs.modal', function (e) {
        $('#offlineDescTplModal textarea').mentionsInput('destroy');
        offlineDescTpl_refreshParameters();
        $('#offlineDescTplModal textarea').mentionsInput({source: offlineDescTpl_data});
        $('#offlineDescTplModal textarea').mentionsInput('setValue', offlineDescTpl_value);
        offlineDescTpl_getSelectedParameters();
    });
    $('#offlineDescTplModal').on('shown.bs.modal', function (e) {
        $('#offlineDescTplModal textarea').focus();
    });

    $('#offlineDescTpl').click(function(){
        offlineDescTpl_modalShow();
    });

    $('#offlineDescTplModal textarea').mentionsInput({source: offlineDescTpl_data});

    $('#offlineDescTplModalSaveBtn').click(function(){
        offlineDescTpl_value = $('#offlineDescTplModal textarea').mentionsInput('getValue');
        var has_warnings = offlineDescTpl_getSelectedParameters(true);
        if (has_warnings) {
            alert(offlineDescTpl_errorMsg);
        } else {
            // following two lines are from category-parameterization-edit.js file
            category_fieldsets_and_parameters_final.settings.offline_desc_tpl = offlineDescTpl_value;
            check_category_fieldsets_and_parameters_changes();
            $('#offlineDescTplModal').modal('hide');
        }
    });

    offlineDescTpl_refreshParameters();
    offlineDescTpl_getSelectedParameters();
});
