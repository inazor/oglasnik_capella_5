
$(document).ready(function(){
    $(".btn-group.bootstrap-select.new_contact_dropdown").hide()
    
    $('span[action=edit]').click(function() {
        var edit_lastClass = $(this).attr('class').split(' ').pop()
        var edit_input = $("input."+edit_lastClass)
        var contact_value = $("#_contact_value")
        var contact_action = $("#_contact_action")
        var contact_id = $("#_contact_id")
        
        
        edit_input.prop('readonly',false)        
        $('span[action=save]').show()             
               
        edit_input.change(function() {
            contact_value.val($(this).val())
            contact_action.val("edit")
            contact_id.val($(this).attr('id'))
        })       
       
    })
    
    $('span[action=delete]').click(function() {        
        var contact_action = $("#_contact_action")
        var contact_id = $("#_contact_id")
        var delete_input = $("input#"+$(this).attr('id'))
        
        delete_input.css('background-color','red')

        contact_action.val("delete")
        contact_id.val($(this).attr('id'))
    })
    
    $('span[action=create]').click(function() { 
        $('span[action=save]').show()  
        $(".new_contact_text").show()
        var contact_action = $("#_contact_action")
        var contact_value = $("#_contact_value")
        var contact_type = $("#_contact_type")
        var contact_user_id = $("#_contact_user_id")
        

        
        $(".new_contact_text").change(function() {
            contact_value.val($(".new_contact_text").val())
        }) 
        
        contact_action.val("create")
        contact_user_id.val($(".hidden_id").attr('id'))
        contact_type.val("number")
        
        
        
    })

    
    
});



function uncheck_radio_before_click(radio) {
    if(radio.prop('checked'))
        radio.one('click', function(){ radio.prop('checked', false) } )
}

$('body').on('mouseup', 'input[type="radio"]', function(){
    var radio=$(this)
    uncheck_radio_before_click(radio)
})

$('body').on('mouseup', 'label', function(){
    var label=$(this);
    var radio
    if(label.attr('for'))
        radio=$('#'+label.attr('for')).filter('input[type="radio"]')
    else
        radio=label.children('input[type="radio"]')
    if(radio.length)
        uncheck_radio_before_click(radio)
})
