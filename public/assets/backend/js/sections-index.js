$(document).ready(function(){
    reorder_controllable_button = '#btnSaveSectionsOrder';
    $(reorder_controllable_button).click(function(){
        $('#sections_order').val(JSON.stringify(reorder_final_ids));
        $('#frm_sections').submit();
    }).hide();

    $('#full-width-section-layout').change(function(){
        var $chkBox = $(this);
        $.post(
            '/admin/sections/triggerFullWith', {
                '_csrftoken': $('#_csrftoken').val(),
                'location'  : $chkBox.data('section-location'),
                'value'     : $chkBox.prop('checked') ? '1' : '0'
            },
            function (json) {
                if (!json.status) {
                    $chkBox.prop('checked', !$chkBox.prop('checked'));
                    if ($.trim(json.msg)) {
                        alert(json.msg);
                    }
                }
            },
            'json'
        );
    });
});
