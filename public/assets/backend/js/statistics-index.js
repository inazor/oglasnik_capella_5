$(function() {
    Morris.Bar({
        element: 'registered-users-chart',
        data: registered_users_by_month,
        xkey: 'y',
        ykeys: ['a', 'b'],
        labels: ['Personal', 'Company'],
        hideHover: 'auto',
        resize: true
    });
});
