$(document).ready(function(){
    $('#url').generateSlug('#name');

    var $categoryIcon = $('#category_icon');
    $categoryIcon.selectpicker({
        style: 'btn btn-default',
        size: 8,
        title: 'Select category\'s icon',
        mobile: OGL.is_mobile_browser
    });

    var $category_fallback_image = $('#category_fallback_image');
    $category_fallback_image.selectpicker({
        style: 'btn btn-default',
        size: 8,
        title: 'Category NoImage graphic',
        mobile: OGL.is_mobile_browser
    });

    var $parent_id = $('#parent_id');
    // TODO: this is a quick fix to apply title attribute on options so we can have clean text when option is selected
    $parent_id.find('option').each(function(i, elem){
        $(elem).attr('title', $.trim($(elem).text().replace(/^\-*/,'')));
    });

    $parent_id.selectpicker({
        style: 'btn btn-default',
        size: 8,
        liveSearch: true,
        title: 'Select categories',
        noneResultsText: 'No categories match',
        mobile: OGL.is_mobile_browser
    });

    $('#transaction_type_id, #moderation_type').selectpicker({
        style: 'btn btn-default',
        mobile: OGL.is_mobile_browser
    });

    $('#transaction_type_id').change(function(){
        var $ad_expire_time_div = $('#ad_expire_time_div');
        var val = parseInt($(this).val());

        if (val) {
            $ad_expire_time_div.removeClass('hidden');
        } else {
            $ad_expire_time_div.addClass('hidden');
        }
    });

    $('#default_ad_expire_time').autoNumeric({aSep:'', mDec:'0', wEmpty:'zero', lZero:'deny'});
    $('#min_age_allowed').autoNumeric({aSep:'', mDec:'0', wEmpty:'zero', lZero:'deny'});

    $('textarea#min_age_html').ckeditor({
        customConfig: '/assets/backend/js/ckeditor_basic_config.js'
    });

    $('#age_restriction').change(function(){
        if ($(this).prop('checked')) {
            $('#age_restriction_div').show();
        } else {
            $('#age_restriction_div').hide();
        }
    });

});
