/*global category_tree_items:false */
var $published_at_datetimepicker,
    $expires_at_datetimepicker;
var searchFilterInitialized = false;

function import_children(children, initial_level) {
    $.each(children, function(i, elem){
        var prefix = '-'.repeat((elem.level - initial_level) - 1) + ' ';
        var $option = $('<option/>').val(elem.id).text(prefix + elem.name);
        var subtext = elem.path.text.split(' › ').slice(0, -1).join(' › ');
        if (subtext !== elem.name) {
            $option.attr('data-subtext', elem.path.text).data('subtext', subtext);
        }
        $('#additional_category_ids').append($option);
        if ('undefined' !== typeof elem['children']) {
            import_children(elem['children'], initial_level);
        }
    });
}

function populate_additional_category_dropdown(parent_id) {
    var $additional_cat_select = $('#additional_category_ids');
    var $additional_cat_options = $additional_cat_select.find('option');
    $additional_cat_options.remove();
    $additional_cat_select.selectpicker('refresh');
    if (parseInt(parent_id) && 'undefined' !== typeof category_tree_items[parent_id]) {
        var initial_level = parseInt(category_tree_items[parent_id]['level']);
        var parents_children = category_tree_items[parent_id]['children'];
        if ('undefined' !== typeof parents_children) {
            import_children(parents_children, initial_level);
        }
    }
    // Number of options might've changed above, we need new state here
    var $new_options = $additional_cat_select.find('option');
    if ($new_options.size() == 0) {
        $additional_cat_select.prop('disabled', true);
        $('#additional_cat_div').hide();
    } else {
        $additional_cat_select.prop('disabled', false);
        $('#additional_cat_div').show();
    }
    $additional_cat_select.selectpicker('refresh');
}

function filterActiveClassifieds() {
    var $buttonIcon = $('#filter-classifieds-btn > .fa');
    $buttonIcon.removeClass('fa-search').addClass('fa-spinner fa-spin');
    var $container  = $('#all_ads_container');
    var term        = $.trim($('#filter-classifieds').val());
    var termURL     = '/admin/shops/usersActiveAds/' + $('#user_id').val();
    if (term) {
        termURL += '/' + term;
    }
    $.get(
        termURL,
        function(json) {
            $buttonIcon.removeClass('fa-spinner fa-spin').addClass('fa-search');
            $container.html(json.data);
            if (json.status) {
                ajaxify_pagination($container);
            } else {
                $container.html('No ads found for given search term!');
            }
            shopping_windows_editor.init();
        },
        'json'
    );
}

function load_paginated_results($container, $sender) {
    $.get(
        $sender.data('page-url'),
        function(json) {
            if (json.status) {
                $container.html(json.data);
                ajaxify_pagination($container);
            }
            shopping_windows_editor.init();
        },
        'json'
    );
}

function ajaxify_pagination($container) {
    $container.find('ul.pagination a').each(function(i, elem){
        $(elem)
            .attr('data-page-url', $(elem).attr('href'))
            .data('page-url', $(elem).attr('href'))
            .click(function(){
                load_paginated_results($container, $(this));
                return false;
            });
    });
}

var shopping_windows_editor = {
    init: function () {
        // Build the collection of ads already present in the loaded shopping window
        var collection = {};
        $('.featured-shops-box-preview .box-classifieds div[data-classified-id]').each(function (i, el) {
            var $el = $(el);
            var id = parseInt($el.data('classified-id'));
            if (id) {
                // (key is prefixed with an underscore to avoid Chrome's optimization of not
                // maintaining key order for numeric indexes - see http://stackoverflow.com/a/3550355)
                collection['_' + id] = id;
            }
        });
        this.collection = collection;

        // bind clicks
        var self_ = this;
        $('.add-to-shopping-window').on('click', function (e) {
            e.preventDefault();
            var ad_id = $(this).parent().prev().attr('data-classified-id');
            self_.add(ad_id);
        });
        $('.remove-from-shopping-window').on('click', function (e) {
            e.preventDefault();
            var ad_id;
            if (parseInt($(this).data('classified-id'))) {
                ad_id = parseInt($(this).data('classified-id'));
            } else {
                ad_id = $(this).parent().prev().attr('data-classified-id');
            }
            self_.remove(ad_id);
        });
        if (!searchFilterInitialized) {
            var $search_input = $('#filter-classifieds');
            var $search_button = $('#filter-classifieds-btn');
            // Prevent enter from submitting the entire form, just use it to search
            $search_input.on('keydown keypress', function(e){
                var code = e.keyCode ? e.keyCode : e.which;
                if (13 === code) {
                    e.preventDefault();
                    $search_button.trigger('click');
                }
            });
            $search_button.on('click', function(e){
                filterActiveClassifieds();
            });
            searchFilterInitialized = true;
        }

        // refresh ui state
        this.refresh_ui();

        // make sure links within the shopping window open in new windows to not loose changes
        this.change_links_target();
    },
    change_links_target: function () {
        $('.featured-shops-box-preview').find('a').attr('target', '_blank');
    },
    refresh_ui: function () {
        // go through the list of available ads and enable/disable their buttons accordingly
        var $boxes = $('.selectable-ads').find('.post-box');
        var self_ = this;
        $boxes.each(function (i, box) {
            var $box = $(box);
            var $box_actions = $box.next();
            var box_ad_id = $box.attr('data-classified-id');

            // Ads without an image can no longer be added to the shopping window. For some reason...
            var no_img = $box.find('img.no-thumb');
            if (no_img.length > 0) {
                $box_actions.find('.add-to-shopping-window').attr('disabled', 'disabled').addClass('disabled');
                $box_actions.find('.no-image-warning').removeClass('hidden');
            }

            if (self_.is_added(box_ad_id)) {
                // hide add link, show delete link
                $box_actions.find('.add-to-shopping-window').removeClass('hidden').addClass('hidden');
                $box_actions.find('.remove-from-shopping-window').removeClass('hidden');
            } else {
                // hide delete link, show add link
                $box_actions.find('.remove-from-shopping-window').removeClass('hidden').addClass('hidden');
                $box_actions.find('.add-to-shopping-window').removeClass('hidden');
            }
        });

        // Hide shopping window's .row.featured if it's empty
        var $row = $('.featured-shops-box-preview .box-classifieds');
        var $ads = $row.find('div[data-classified-id]');
        if ($ads.length <= 0) {
            // hide if empty
            $row.addClass('hidden');
        } else {
            // show if it was hidden (until we added a new first item)
            $row.removeClass('hidden');
        }

        // Show/hide .shop-about when needed
        var current_total = this.num_keys(this.collection);
        var $about = $('.featured-shops-box-preview .shop-about');
        if (current_total <= 0) {
            $about.removeClass('hidden');
            // hide the .featured row if it's empty
        } else {
            if (!($about.hasClass('hidden'))) {
                $about.addClass('hidden');
            }
        }

        // Refresh the hidden input containing the ids
        $('#ads').val(this.get_collection_ids_string());
    },
    add: function (id) {
        if (!id || id <= 0) {
            return;
        }

        if (this.is_added(id)) {
            return;
        }

        // check if max reached
        var current_total = this.num_keys(this.collection);
        if (current_total >= this.max_items) {
            // TODO: show modal warning about max items
            alert('Max shopping window items reached. Remove an existing one before adding a new one');
            return;
        }

        // add to collection
        this.collection['_' + id] = id;

        // create a clone and append it to the shopping window
        var $clone = this.create_ad_clone(id);
        if ($clone) {
            $('.featured-shops-box-preview .box-classifieds').append($clone);
        }

        this.refresh_ui();
    },
    remove: function (id) {
        if (!id || id <= 0) {
            return;
        }

        if (this.is_added(id)) {
            // remove from collection
            delete this.collection['_' + id];

            // delete box
            var $box = $('.featured-shops-box-preview div[data-classified-id="' + id + '"]');
            if ($box.length > 0) {
                $box.remove();
            }

            this.refresh_ui();
        }
    },
    create_ad_clone: function (id) {
        var $box = $('.selectable-ads div[data-classified-id="' + id + '"]');
        var $clone;
        if ($box.length > 0) {
            var src = $box.find('.img-wrapper img').attr('src');
            var title = $box.find('h3').text();
            var href = $box.find('h3').attr('data-href');

            $clone = $('<div/>').addClass('col-xs-6 classified-details').attr('data-classified-id', id).data('classified-id', id);
            var $editorActions = $('<div/>').addClass('editor-actions')
                .append(
                    $('<span/>')
                        .addClass('action-btn remove-from-shopping-window fa fa-close')
                        .attr('data-classified-id', id).data('classified-id', id)
                        .click(function(e){
                            e.preventDefault();
                            var classifiedID = $(this).closest('div[data-classified-id]').data('classified-id');
                            shopping_windows_editor.remove(classifiedID);
                        })
                )
                .append(
                    $('<a/>')
                        .addClass('action-btn preview-classifieds fa fa-globe')
                        .attr('href', href)
                        .attr('target', '_blank')
                );
            var $img = ($('<img/>').attr('src', src).attr('alt', title));

            var $img_wrap = $('<div/>').addClass('border-radius overflow-hidden').append($img);
            $clone.append($editorActions).append($img_wrap);
        }

        return $clone;
    },
    is_added: function (id) {
        return (this.collection['_' + id] || false);
    },
    collection: {},
    max_items: 2,
    num_keys: function (o) {
        var num = 0;
        for (var k in o) {
            if (o.hasOwnProperty(k)) {
                num++;
            }
        }
        return num;
    },
    get_collection_ids_string: function () {
        var values = [];
        for (var k in this.collection) {
            if (this.collection.hasOwnProperty(k)) {
                values.push(this.collection[k]);
            }
        }

        return values.join(',');
    }
};

$(document).ready(function(){
    $('select').selectpicker({
        style: 'btn btn-default',
        size: 10,
        noneSelectedText: '',
        noneResultsText: '',
        mobile: OGL.is_mobile_browser
    });

    var $main_cat_select       = $('#main_category_id');
    var $additional_cat_select = $('#additional_category_ids');

    $main_cat_select.on('change', function(){
        populate_additional_category_dropdown($(this).val());
    });
    $main_cat_select.trigger('change');

    var preselect_data = $additional_cat_select.data('preselect');
    if ('undefined' !== typeof preselect_data) {
        var preselect = $.trim(preselect_data).split(',');
        $additional_cat_select.selectpicker('val', preselect);
    }

    var dpicker_options = {
        format: 'DD.MM.YYYY HH:mm',
        locale: 'hr'
    };
    $('#published_at_datetimepicker').datetimepicker(dpicker_options);
    $published_at_datetimepicker = $('#published_at_datetimepicker').data('DateTimePicker');
    $('#expires_at_datetimepicker').datetimepicker(dpicker_options);
    $expires_at_datetimepicker = $('#expires_at_datetimepicker').data('DateTimePicker');
    $('#published_at_datetimepicker').on('dp.change', function(e) {
        $expires_at_datetimepicker.minDate(e.date);
    });
    $('#expires_at_datetimepicker').on('dp.change', function(e) {
        $published_at_datetimepicker.maxDate(e.date);
    });

    /*$('#type_tab a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $('#type').val($(e.target).data('type'));
    });*/

    var $all_ads_container = $('#all_ads_container');
    ajaxify_pagination($all_ads_container);

    shopping_windows_editor.init();
});
