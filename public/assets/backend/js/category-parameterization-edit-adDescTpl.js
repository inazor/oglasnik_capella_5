var adDescTpl_value = '';
var adDescTpl_data = [];
var adDescTpl_available_parameters = [];
var adDescTpl_selected_parameters = [];
var adDescTpl_errorMsg = "Please check all used parameters as it seems that some \nof them are not attached to current category anymore!";

function adDescTpl_refreshParameters() {
    adDescTpl_data = [];
    adDescTpl_available_parameters = [];
    $('.list-sortable li').each(function(p){
        if ('undefined' !== typeof $(this).attr('data-parameter-attributes')) {
            $(this).data('parameter-attributes', $(this).attr('data-parameter-attributes'));
            var parameter_attributes = JSON.parse($(this).data('parameter-attributes'));

            var tpl_type = 'parameter';
            var tpl_param_id = parseInt(parameter_attributes.id);
            if ('undefined' !== typeof parameter_attributes.levels) {
                if ('LOCATION' === parameter_attributes.type_id) {
                    tpl_type = 'location';
                }
                $.each(parameter_attributes.levels, function(index, level_attributes) {
                    var current_row = {
                        value: level_attributes.label_text,
                        uid: tpl_type + ':' + tpl_param_id + '_' + (index + 1)
                    };
                    adDescTpl_data.push(current_row);
                    adDescTpl_available_parameters.push(tpl_param_id);
                });
            } else {
                var current_row = {
                    value: parameter_attributes.label_text,
                    uid: tpl_type + ':' + tpl_param_id
                };
                adDescTpl_data.push(current_row);
                adDescTpl_available_parameters.push(tpl_param_id);
            }
        }
    });
}

function adDescTpl_modalShow() {
    $('#adDescTplModal').modal({
        backdrop: 'static',
        keyboard: true
    });
}

function adDescTpl_getTplMatches() {
    var main_regex = /\@\[([^\]]+)\]\((parameter|location):([\d_]*)+\)/gim;
    var inner_regex = /\@\[([^\]]+)\]\((parameter|location):([\d_]*)+\)/i;

    var globalMatch = adDescTpl_value.match(main_regex);
    var matchArray = new Array();
    for (var i in globalMatch) {
        if (nonGlobalMatch = inner_regex.exec(globalMatch[i])) {
            matchArray.push(nonGlobalMatch);
        }
    }
    return matchArray;
}

function adDescTpl_getSelectedParameters(show_warning) {
    if ('undefined' === typeof show_warning) {
        show_warning = false;
    }
    var has_warnings = false;

    var matchArray = adDescTpl_getTplMatches();
    if (matchArray.length) {
        adDescTpl_selected_parameters = [];
        for (var i in matchArray) {
            var matched_parameter_id = parseInt(matchArray[i][3].split('_')[0]);
            if (-1 === adDescTpl_available_parameters.indexOf(matched_parameter_id)) {
                has_warnings = true;
                var $warning_elem = $('.mentions-input .highlighter-content strong:contains("' + matchArray[i][1] + '")');
                if ('undefined' !== typeof $warning_elem) {
                    $warning_elem.addClass('error');
                }
                adDescTpl_selected_parameters.push(matched_parameter_id);
            }
        }
    }

    return has_warnings;
}

$(document).ready(function(){
    adDescTpl_value = $('#adDescTplModal textarea').val();
    $('#adDescTplModal').on('show.bs.modal', function (e) {
        $('#adDescTplModal textarea').mentionsInput('destroy');
        adDescTpl_refreshParameters();
        $('#adDescTplModal textarea').mentionsInput({source: adDescTpl_data});
        $('#adDescTplModal textarea').mentionsInput('setValue', adDescTpl_value);
        adDescTpl_getSelectedParameters();
    });
    $('#adDescTplModal').on('shown.bs.modal', function (e) {
        $('#adDescTplModal textarea').focus();
    });

    $('#adDescTpl').click(function(){
        adDescTpl_modalShow();
    });

    $('#adDescTplModal textarea').mentionsInput({source: adDescTpl_data});

    $('#adDescTplModalSaveBtn').click(function(){
        adDescTpl_value = $('#adDescTplModal textarea').mentionsInput('getValue');
        var has_warnings = adDescTpl_getSelectedParameters(true);
        if (has_warnings) {
            alert(adDescTpl_errorMsg);
        } else {
            // following two lines are from category-parameterization-edit.js file
            category_fieldsets_and_parameters_final.settings.ad_desc_tpl = adDescTpl_value;
            check_category_fieldsets_and_parameters_changes();
            $('#adDescTplModal').modal('hide');
        }
    });

    adDescTpl_refreshParameters();
    adDescTpl_getSelectedParameters();
});
