$(document).ready(function(){
    $('#parent_id').selectpicker({
        style: 'btn btn-default',
        size: 8,
        liveSearch: true,
        noneResultsText: 'No dictionaries match',
        mobile: OGL.is_mobile_browser
    });
});
