$(function () {
    // Show tooltips
    jQuery('[data-toggle="tooltip"][data-type!="html"]').tooltip();
    jQuery('[data-toggle="tooltip"][data-type="html"]').tooltip({html:true});

    jQuery.fn.handleInfractionReports = function() {
        return this.each(function(index, elem){
            var $reportContainer             = jQuery(elem);
            var $reportedInfractions         = $reportContainer.find('.report-row');
            var $unresolvedReportedInfractions = $reportedInfractions.filter('.unresolved');
            var $form = $reportContainer.closest('form');
            var formType = null;
            if ($form.length) {
                formType = $form.data('type');
                if ('undefined' === typeof formType) {
                    formType = null;
                }
            }

            if ('ads' !== formType) {
                var unresolvedIDs = [];
                $unresolvedReportedInfractions.each(function(i, row){
                    if ('undefined' !== typeof $(row).data('id') && parseInt($(row).data('id'))) {
                        unresolvedIDs.push(parseInt($(row).data('id')));
                    }
                });

                if (unresolvedIDs.length) {
                    $.post(
                        '/admin/infraction-reports/resolve', {
                            '_csrftoken': $('#_csrftoken').val(),
                            'ids'       : unresolvedIDs.join(',')
                        },
                        function (json) {
                            if (!json.status) {
                                if ('undefined' !== typeof json.msg && $.trim(json.msg)) {
                                    alert("Ooops!\n" + json.msg);
                                }
                            }
                        },
                        'json'
                    );
                }
            }

            var hasGroupedStatuses = $reportContainer.find('tbody.all').length == 0;
            if (hasGroupedStatuses) {
                // attach click events on tbody > tr > th in order to show/hide table rows

                $reportContainer.find('tbody tr th').each(function(i, header){
                    var $header    = $(header);
                    $header.click(function(){
                        var $tbody     = $(this).closest('tbody');
                        var $tableRows = $tbody.find('tr.report-row');
                        if ('visible' == $tbody.data('visibility')) {
                            $tableRows.hide();
                            $tbody.data('visibility', 'hidden');
                        } else {
                            $tableRows.show();
                            $tbody.data('visibility', 'visible');
                        }
                    });

                });
            }
        });
    };
});
