var avus_category_mapping_initial = null;
var avus_category_mapping_final = {
    type: $('input[name="map_type"]').val(),
    value: null
};
var avus_category_mapping_has_changes = false;

/**
 * Refresh avus category mapping data. At the same time data is being validated and upon errors, user's browser window
 * is being scrolled to the position of first visible error
 *
 * @return {boolean} Is everything ok?
 */
function refresh_avus_category_mapping() {
    var return_value = true;
    var avus_ids = [];

    avus_category_mapping_final.type = $('input[name="map_type"]:checked').val();
    if ('entire' == avus_category_mapping_final.type) {
        avus_category_mapping_final.value = $.trim($('#avus_category').val());
        if (!avus_category_mapping_final.value) {
            $('#avus_category_box').addClass('has-error');
            return_value = false;
        } else {
            $('#avus_category_box').removeClass('has-error');
        }
    } else {
        avus_category_mapping_final.value = [];

        var curr_rule_index = 1;
        $('.avus-rules').each(function(avus_rule){
            var curr_rule_item = 1;
            var $curr_avus_rule = $(this);
            $curr_avus_rule.attr('data-rule-index', curr_rule_index).data('rule-index', curr_rule_index);

            var avus_id = $.trim($curr_avus_rule.find('input.avus-category-id').val());
            if (avus_id == '' || $.inArray(avus_id, avus_ids) > -1) {
                // avus_id is not set or it already exists in some earlier rule
                $curr_avus_rule.find('input.avus-category-id').closest('.form-group').addClass('has-error');
                return_value = false;
            } else {
                avus_ids.push(avus_id);
                $curr_avus_rule.find('input.avus-category-id').closest('.form-group').removeClass('has-error');
                var avus_rule_parameters = [];
                $curr_avus_rule.find('.panel-body > .panel').each(function(parameter_rule){
                    $(this).attr('data-rule-item', curr_rule_item).data('rule-item', curr_rule_item);
                    var current_parameter_rule = $(this).data('parameter-rule');
                    if ('object' !== typeof current_parameter_rule) {
                        var current_parameter_rule = JSON.parse($(this).data('parameter-rule'));
                    }
                    current_parameter_rule.required = $(this).find('.parameter-required input[type="checkbox"]:eq(0)').prop('checked');
                    avus_rule_parameters.push(current_parameter_rule);
                    curr_rule_item++;
                });

                if (avus_rule_parameters.length > 0) {
                    var current_avus_rule = {
                        avus_id: $.trim($curr_avus_rule.find('input.avus-category-id').val()),
                        rules: avus_rule_parameters
                    };

                    avus_category_mapping_final.value.push(current_avus_rule);

                    $curr_avus_rule.find('.panel-body').show();
                } else {
                    $curr_avus_rule.find('.panel-body').hide();
                }
            }
            curr_rule_index++;
        });
    }

    if (null === avus_category_mapping_initial) {
        avus_category_mapping_initial = JSON.parse(JSON.stringify(avus_category_mapping_final));
        avus_category_mapping_has_changes = false;
    }

    if (return_value) {
        check_avus_category_mapping_changes();
    } else {
        $('html, body').animate({
            scrollTop: $('#frm_category_mapping .has-error:visible').first().offset().top
        }, 500);
    }

    return return_value;
}

/**
 * Check if there are any changes that can be savedž
 *
 * @return {boolean}
 */
function check_avus_category_mapping_changes() {
    avus_category_mapping_has_changes = (JSON.stringify(avus_category_mapping_initial) != JSON.stringify(avus_category_mapping_final));

    var $btn_save_ajax_mapping = $('#btnSaveCategoryMapping');
    if (avus_category_mapping_has_changes) {
        if (!$btn_save_ajax_mapping.is(':visible')) {
            $btn_save_ajax_mapping.fadeIn('fast');
        }
    } else {
        if ($btn_save_ajax_mapping.is(':visible')) {
            $btn_save_ajax_mapping.fadeOut('fast');
        }
    }
}

/**
 * Helper method to get parameter_id dropdown/option
 *
 * @param  {string} selector Additional selector to be applied on the object's find method
 *
 * @return {jQuery object}
 */
function get_selected_parameter_dropdown(selector) {
    var $dropdown = $('#parameter_id');

    if ('undefined' !== typeof selector) {
        $dropdown = $dropdown.find(selector);
    }

    return $dropdown;
}

/**
 * Add new Avus Category container
 */
function addAvusCategory(){
    $('#rules_container').append(
        $('<div/>')
            .addClass('panel panel-default avus-rules')
            .append(
                $('<div/>')
                    .addClass('panel-heading')
                    .append(
                        $('<div/>')
                            .addClass('form-group')
                            .css({'margin-bottom':'0'})
                            .append(
                                $('<div/>')
                                    .addClass('input-group')
                                    .append(
                                        $('<input/>')
                                            .addClass('form-control avus-category-id')
                                            .attr('type', 'text')
                                            .attr('placeholder', 'Avus category ID')
                                            .attr('value', '')
                                            .change(function(){
                                                refresh_avus_category_mapping();
                                            })
                                    )
                                    .append(
                                        $('<span/>')
                                            .addClass('input-group-btn')
                                            .append(
                                                $('<button/>')
                                                    .addClass('btn btn-primary add-avus-rule')
                                                    .attr('type', 'button')
                                                    .html('<span class="fa fa-plus fa-fw"></span> Add rule')
                                                    .click(function(){
                                                        addParameterDialog($(this));
                                                    })
                                            )
                                            .append(
                                                $('<button/>')
                                                    .addClass('btn btn-danger del-avus-rules')
                                                    .attr('type', 'button')
                                                    .html('<span class="fa fa-trash fa-fw"></span>')
                                                    .click(function(){
                                                        removeAvusCategory($(this));
                                                    })
                                            )
                                    )
                            )
                    )
            )
            .append(
                $('<div/>')
                    .addClass('panel-body parameter-dict-values')
                    .css({'display':'none'})
            )
            .fadeIn('fast', function(){
                refresh_avus_category_mapping();
                $('html, body').animate({
                    scrollTop: $('#rules_container .avus-rules').last().offset().top
                }, 500);
            })
    );
}

/**
 * Remove Avus Category container
 *
 * @param  {jQuery object} $sender
 */
function removeAvusCategory($sender){
    var delete_avus_rules = true;

    var parameter_count = $sender.closest('.avus-rules').find('.parameter-dict-values .panel').length;
    if (parameter_count) {
        delete_avus_rules = confirm('You have ' + parameter_count + ' parameter' + (parameter_count > 1 ? 's' : '') + ' in this avus rule. Do you want to delete this rule?');
    }

    if (delete_avus_rules) {
        // see if we have some rules here so we can prevent not intended deltetion
        $sender.closest('.avus-rules').fadeOut('fast', function(){
            $sender.closest('.avus-rules').remove();
            refresh_avus_category_mapping();
        });
    }
}

/**
 * Reset all form fields in modal windows
 */
function reset_all_dialog_settings() {
    $('.parameter-value input[type="radio"]:eq(0)').prop('checked', true);
    $('.parameter-value input[type="checkbox"]').prop('checked', false);
}

/**
 * Helper method to set all values to a specific dependable (even location is dependable) parameters based on promises.
 * When someone wants to set all values to all sibling parameters, this method should be called!
 *
 * @param {string} promise_name
 * @param {string} parameter_id_segment
 * @param {JSON array} values
 */
function set_parameter_promise_values(promise_name, parameter_id_segment, values) {
    var total_levels = values.length;
    set_parameter_promise_level_value(promise_name, parameter_id_segment, total_levels, 1, values);
}

/**
 * Helper method to set a specific sibling of a dependable parameter. This method should never be called manually - use
 * the set_parameter_promise_values instead!
 *
 * @param {string} promise_name
 * @param {string} parameter_id_segment
 * @param {int} total_levels
 * @param {int} curr_level
 * @param {JSON array} values
 */
function set_parameter_promise_level_value(promise_name, parameter_id_segment, total_levels, curr_level, values) {
    var level_index = (curr_level - 1);
    var level_id = parameter_id_segment + '_level_' + curr_level;
    $('#' + level_id).selectpicker('val', values[level_index].value);
    $('#' + level_id).trigger('change');
    var parameter_promise = window[promise_name];
    $.when(parameter_promise[level_id]).done(function(promise) {
        if (curr_level < total_levels) {
            set_parameter_promise_level_value(promise_name, parameter_id_segment, total_levels, (curr_level + 1), values);
        }
    });
}

/**
 * Popullate rule settings to the dialog
 *
 * @param {JSON object} parameter_rule
 */
function set_rule_to_dialog(parameter_rule) {
    $parameter_select = get_selected_parameter_dropdown();
    if (parameter_rule) {
        $parameter_select.selectpicker('val', parameter_rule.id);
        $selected_parameter = get_selected_parameter_dropdown(':selected');

        $parameter_select.selectpicker('val', parameter_rule.id);
        $parameter_select.prop('disabled', true);
        $parameter_select.selectpicker('refresh');

        if ('LOCATION' === $selected_parameter.data('type-id')) {
            set_parameter_promise_values('promise_parameter_location', 'parameter_location', parameter_rule.rule);
        } else if ('DEPENDABLE_DROPDOWN' === $selected_parameter.data('type-id')) {
            set_parameter_promise_values('promise_parameter_' + parameter_rule.id, 'parameter_' + parameter_rule.id, parameter_rule.rule);
        } else if ('CHECKBOXGROUP' === $selected_parameter.data('type-id')) {
            $.each(parameter_rule.rule, function(i, elem){
                $('#ad_params_' + parameter_rule.id + '_' + elem.value).prop('checked', true);
            });
        } else if ('DROPDOWN' === $selected_parameter.data('type-id')) {
            $('#parameter_' + parameter_rule.id).selectpicker('val', parameter_rule.rule.value);
        } else if ('RADIO' === $selected_parameter.data('type-id')) {
            $('#ad_params_' + parameter_rule.id + '_' + parameter_rule.rule.value).prop('checked', true);
        }
    } else {
        $parameter_select.selectpicker('val', get_selected_parameter_dropdown('option:first').attr('value'));
        $parameter_select.prop('disabled', false);
        $parameter_select.selectpicker('refresh');
    }
}

/**
 * Show the add parameter dialog
 *
 * @param {jQuery object} $sender
 */
function addParameterDialog($sender) {
    $rule_box = $sender.closest('.avus-rules');
    var avus_category_id = $.trim($rule_box.find('input.avus-category-id').val());
    if (avus_category_id) {
        if (parseInt(avus_category_id)) {
            var rule_index = $rule_box.attr('data-rule-index');

            var parameter_rule = null;
            var $add_save_btn = $('#btnSaveParameterModal');
            var btn_type = 'add';
            var btn_label = 'Add';
            var rule_item = '';

            if ($sender.hasClass('edit-rule')) {
                btn_type = 'edit';
                btn_label = 'Save';
                rule_item = $sender.closest('.panel').data('rule-item');

                parameter_rule = $sender.closest('.panel').data('parameter-rule');
                if ('object' !== typeof parameter_rule) {
                    parameter_rule = JSON.parse($sender.closest('.panel').data('parameter-rule'));
                }
            }

            $add_save_btn.attr('data-type', btn_type).data('type', btn_type);
            $add_save_btn.text(btn_label);
            $add_save_btn.attr('data-rule-item', btn_type).data('rule-item', rule_item);
            $add_save_btn.attr('data-rule-index', rule_index).data('rule-index', rule_index);

            set_rule_to_dialog(parameter_rule);

            // show the dialog
            $('#addParameterModal').modal({
                backdrop: 'static',
                keyboard: true
            });
        } else {
            alert('Incorrect AVUS category ID!');
        }
    } else {
        alert('First, you have to decide in which AVUS category ID should parameters be mapped!');
    }
}

/**
 * SaveParameterModal button helper method
 */
function btnSaveParameterModalClick(){
    var button_type = $('#btnSaveParameterModal').data('type');
    var rule_index = $('#btnSaveParameterModal').data('rule-index');
    var parameter_rule = collectParameterRule();
    if (parameter_rule) {
        if (button_type == 'add') {
            addParameterRule(parameter_rule, rule_index);
        } else {
            editParameterRule($('#btnSaveParameterModal').data('rule-item'), parameter_rule, rule_index);
        }

        $('#addParameterModal').modal('hide');
    } else {
        alert('No parameter value chosen for rule!');
    }
}

/**
 * Helper method to collect chosen parameter settings from the dialog
 *
 * @return {null|JSON object}
 */
function collectParameterRule(){
    $chosen_param = get_selected_parameter_dropdown(':selected');
    var param_id = parseInt($chosen_param.data('id'));

    var parameter_rule = {
        id: param_id,
        slug: 'ad_params_' + param_id,
        text: $chosen_param.text(),
        value: '',
        rule: null,
        required: true
    };

    // dependable dropdowns
    if ('undefined' !== typeof $chosen_param.data('max-level') && parseInt($chosen_param.data('max-level'))) {
        var levels = [];
        var joined_value = [];
        if ('LOCATION' === $chosen_param.data('type-id')) {
            parameter_rule.slug = 'location';

            for(var i=1; i<=parseInt($chosen_param.data('max-level')); i++) {
                var param_value      = parseInt($('select[name="ad_location_' + i + '"]').val());
                var param_level_slug = $.trim($('select[name="ad_location_' + i + '"]').data('level-id'));
                var param_text       = $('select[name="ad_location_' + i + '"] option[value="' + param_value + '"]').text();
                if (param_value) {
                    var level = {
                        slug: param_level_slug,
                        value: param_value,
                        text:  param_text
                    };
                    joined_value.push(param_text);
                    levels.push(level);
                }
            }
        } else {
            for(var i=1; i<=parseInt($chosen_param.data('max-level')); i++) {
                var param_value      = parseInt($('select[name="ad_params_' + param_id + '_' + i + '"]').val());
                var param_level_slug = $.trim($('select[name="ad_params_' + param_id + '_' + i + '"]').data('level-id'));
                var param_text       = $('select[name="ad_params_' + param_id + '_' + i + '"] option[value="' + param_value + '"]').text();
                if (param_value) {
                    var level = {
                        slug: param_level_slug,
                        value: param_value,
                        text:  param_text
                    };
                    joined_value.push(param_text);
                    levels.push(level);
                }
            }
        }
        if (levels.length) {
            parameter_rule.rule  = levels;
            parameter_rule.value = joined_value.join(' › ');
        }
    // checkboggroup
    } else if ('CHECKBOXGROUP' == $chosen_param.data('type-id')) {
        var chosen = [];
        var joined_value = [];
        $('input[name="ad_params_' + param_id + '[]"]:checked').each(function(i, elem){
            var param_value = parseInt($(elem).val());
            var param_text  = $(elem).parent().find('label').text()
            if (param_value) {
                var option = {
                    value: param_value,
                    text:  param_text
                };
                joined_value.push(param_text);
                chosen.push(option);
            }
        });
        if (chosen.length) {
            parameter_rule.rule  = chosen;
            parameter_rule.value = joined_value.join(', ');
        }
    // classic dropdown
    } else if ('DROPDOWN' == $chosen_param.data('type-id')) {
        var param_value = parseInt($('[name="ad_params_' + param_id + '"]').val());
        var param_text  = $('select[name="ad_params_' + param_id + '"] option[value="' + param_value + '"]').text()
        if (param_value) {
            parameter_rule.rule = {
                value: param_value,
                text:  param_text
            };
            parameter_rule.value = param_text;
        }
    // radio parameter
    } else if ('RADIO' == $chosen_param.data('type-id')) {
        var param_value = parseInt($('input[name="ad_params_' + param_id + '"]:checked').val());
        var param_text  = $('label[for="ad_params_' + param_id + '_' + param_value + '"]').text()
        if (param_value) {
            parameter_rule.rule = {
                value: param_value,
                text:  param_text
            };
            parameter_rule.value = param_text;
        }
    }

    if (null === parameter_rule.rule) {
        return null;
    } else {
        var $required_checkbox = $('input[data-required-parameter-id="' + param_id + '"]');
        if ($required_checkbox !== typeof 'undefined' && $required_checkbox.length == 1) {
            parameter_rule.required = $required_checkbox.prop('checked');
        }
        return parameter_rule;
    }
}

/**
 * Add parameter rule to target rule container
 *
 * @param {JSON object} parameter_rule
 * @param {int} target_rule_index Index of target container
 */
function addParameterRule(parameter_rule, target_rule_index) {
    var parameter_rule_JSON = JSON.stringify(parameter_rule);

    $('.avus-rules[data-rule-index="' + target_rule_index + '"] .panel-body').append(
        $('<div/>')
            .addClass('panel panel-default')
            .attr('data-parameter-rule', parameter_rule_JSON).data('parameter-rule', parameter_rule_JSON)
            .append(
                $('<div/>')
                    .addClass('panel-heading')
                    .append(
                        $('<div/>')
                            .addClass('pull-right')
                            .append(
                                $('<span/>')
                                    .addClass('btn btn-info btn-xs edit-rule')
                                    .html('<span class="fa fa-edit"></span> Edit')
                                    .click(function(){
                                        addParameterDialog($(this));
                                    })
                            )
                            .append(
                                document.createTextNode(' ')
                            )
                            .append(
                                $('<span/>')
                                    .addClass('btn btn-danger btn-xs del-rule')
                                    .html('<span class="fa fa-trash"></span>')
                                    .click(function(){
                                        deleteParameterRule($(this));
                                    })
                            )
                    )
                    .append(
                        $('<div/>')
                            .addClass('checkbox-inline parameter-required')
                            .append(
                                $('<label/>')
                                    .addClass('no-margin')
                                    .append(
                                        $('<input/>')
                                            .attr('data-required-parameter-id', parameter_rule.id)
                                            .attr('type', 'checkbox')
                                            .prop('checked', parameter_rule.required)
                                            .click(function(){
                                                change_parameters_required_state($(this));
                                            })
                                    )
                                    .append(document.createTextNode(' '))
                                    .append(
                                        $('<span/>')
                                            .addClass((parameter_rule.required ? 'text-success' : 'text-muted'))
                                            .text((parameter_rule.required ? 'Required' : 'Optional'))
                                    )
                            )
                    )
                    .append(
                        $('<span>')
                            .append(
                                $('<span/>')
                                    .text(parameter_rule.text + ': ')
                            )
                            .append(
                                $('<strong/>')
                                    .addClass('text-success')
                                    .text(parameter_rule.value)
                            )
                    )
            )
            .fadeIn('fast', function(){
                refresh_avus_category_mapping();
            })
    );
}

/**
 * Update existing rule with new data
 *
 * @param  {int} rule_item Index of target rule inside a container
 * @param  {JSON object} parameter_rule
 * @param  {int} target_rule_index Index of target rule container
 */
function editParameterRule(rule_item, parameter_rule, target_rule_index) {
    var parameter_rule_JSON = JSON.stringify(parameter_rule);

    var $rule_item = $('.avus-rules[data-rule-index="' + target_rule_index + '"] .panel[data-rule-item="' + rule_item + '"]');
    $rule_item.attr('data-parameter-rule', parameter_rule_JSON).data('parameter-rule', parameter_rule_JSON)
    $rule_item.find('.panel-heading > span').html('');
    $rule_item.find('.panel-heading > span')
        .append(
            $('<span/>')
                .text(parameter_rule.text + ': ')
        )
        .append(
            $('<strong/>')
                .addClass('text-success')
                .text(parameter_rule.value)
        )
        .fadeIn('fast', function(){
            refresh_avus_category_mapping();
        });
}

/**
 * Delete the rule from container
 *
 * @param  {jQuery object} $sender
 */
function deleteParameterRule($sender) {
    var $current_parameter_rule = $sender.closest('.panel');
    var target_rule_index = $current_parameter_rule.closest('.avus-rules').data('rule-index');
    $current_parameter_rule.fadeOut('fast', function(){
        $current_parameter_rule.remove();
        refresh_avus_category_mapping();
    });
}

function change_parameters_required_state($parameter) {
    if ($parameter.prop('checked')) {
        $parameter.siblings('span').attr('class', 'text-success').text('Required');
    } else {
        $parameter.siblings('span').attr('class', 'text-muted').text('Optional');
    }
    refresh_avus_category_mapping();
}

$(document).ready(function(){
    $('input[name="map_type"]').change(function(){
        if ('entire' == $(this).val()) {
            $('#avus_category_box').show();
            $('#avus_parameter_box').hide();
            $('#category_dictionary_parameters').hide();
        } else {
            $('#avus_category_box').hide();
            $('#avus_parameter_box').show();
            $('#category_dictionary_parameters').show();
        }
        refresh_avus_category_mapping();
    });
    $('#avus_category, input.avus-category-id').change(function(){
        refresh_avus_category_mapping();
    });
    $('.add-avus-rule').click(function(){
        addParameterDialog($(this));
    });
    $('.del-avus-rules').click(function(){
        removeAvusCategory($(this));
    });

    $('.parameter-required input').click(function(){
        change_parameters_required_state($(this));
    });

    $('#btnSaveCategoryMapping').on('click', function(){
        if (refresh_avus_category_mapping()) {
            if (avus_category_mapping_has_changes) {
                $('#category_mapping').val(JSON.stringify(avus_category_mapping_final));
                $('#frm_category_mapping').submit();
            }
        }
    });

    $('#parameter_id').selectpicker({
        style: 'btn btn-default',
        size: 8,
        liveSearch: true,
        title: 'Select parameter',
        mobile: OGL.is_mobile_browser
    });
    $('#parameter_id').change(function(){
        $('.parameter-value[data-parameter-id!="' + $(this).val() + '"]').hide();
        $('.parameter-value[data-parameter-id="' + $(this).val() + '"]').show();
    });
    $('#addParameterModal').on('hidden.bs.modal', function (e) {
        reset_all_dialog_settings();
    }).on('shown.bs.modal', function(e){
        $('#parameter_id').trigger('change');
    });

    $('#btnAddAvusCategory').click(function(){
        addAvusCategory();
    });

    $('#btnSaveParameterModal').click(function(){
        btnSaveParameterModalClick();
    });

    $('.edit-rule').click(function(){
        addParameterDialog($(this));
    });
    $('.del-rule').click(function(){
        deleteParameterRule($(this));
    });

    reset_all_dialog_settings();
    refresh_avus_category_mapping();
});
