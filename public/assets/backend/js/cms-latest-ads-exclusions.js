$(function(){
    var versioner = '0002';

    // Setup users tagsinput field and bloodhound
    var users = new Bloodhound({
        datumTokenizer: function(datum) {
            return Bloodhound.tokenizers.whitespace(datum.text);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        dupDetector: function(remoteMatch, localMatch) {
            return remoteMatch.name === localMatch.name;
        },
        remote: {
            url: '/admin/users/search/%QUERY',
            wildcard: '%QUERY'
        }
    });
    users.initialize();
    var $element = $('#users');
    $element.tagsinput({
        confirmKeys: [9, 13, 44, 188],
        allowDuplicates: false,
        freeInput: false, // has no effect anyway since we're using objects and not basic arrays
        itemValue: function(item) {
            return item.value;
        },
        itemText: function(item) {
            return item.text;
        },
        typeaheadjs: {
            hint: false,
            highlight: true,
            name: 'exclude-user-ids-' + versioner,
            displayKey: 'text',
            source: users.ttAdapter(),
            limit: 20
        }
    });
    // Collect already excluded users printed during page load
    if (window.excluded_users) {
        $.each(window.excluded_users, function (idx, u) {
            $element.tagsinput('add', u);
        });
    }

    // Setup selectpicker for ads categories
    var $cat_id_dropdown = $('#categories');
    $cat_id_dropdown.selectpicker({
        style: 'btn btn-default',
        size: 8,
        liveSearch: true,
        title: 'Choose categories to exclude',
        noneSelectedText: 'Choose excluded categories',
        noneResultsText: 'No categories matched',
        mobile: OGL.is_mobile_browser,
        showSubtext: true
    });
    var preselect_data = $cat_id_dropdown.data('preselect');
    if ('undefined' !== typeof preselect_data) {
        var preselect = $.trim(preselect_data).split(',');
        $cat_id_dropdown.selectpicker('val', preselect);
    }


    // Handle tree-like propagation when a category is clicked/changed
    $cat_id_dropdown.on('changed.bs.select', function(event, clickedIndex, newValue, oldValue){
        function trim_option_text(text) {
            return text.replace(/^(\-|\s)+/, '');
        }

        var $select = $(this);
        var $options = $select.find('option');
        var $option = $options.eq(clickedIndex);
        var option_text_clean = trim_option_text($option.text());
        var changed_option_subtext = $option.data('subtext');
        var data_level = $option.data('level');
        if (!changed_option_subtext) {
            changed_option_subtext = option_text_clean;
        }
        var selector;
        if (!data_level) {
            data_level = 1;
            selector = 'option[data-subtext^="' + changed_option_subtext + '"]';
        } else {
            selector = 'option[data-subtext^="' + changed_option_subtext + ' › ' + option_text_clean + '"]';
        }

        // Find all other options matching changed_option_subtext and propagate the newValue to them as well
        var $child_options = $select.find(selector);
        // Only get those with the level larger than the clicked one
        var $child_options = $child_options.filter(function(){
            var level = $(this).data('level');
            if (!level || level > data_level) {
                return true;
            } else {
                return false;
            }
        });
        $child_options.prop('selected', newValue);
        // Refresh the selectpicker
        $cat_id_dropdown.selectpicker('refresh');
    });
});
