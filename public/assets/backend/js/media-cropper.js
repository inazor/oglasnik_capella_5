$(document).ready(function(){
    /* global cropper_configs */
    var $modals = $('.modal');
    $modals.each(function(){
        var $modal = $(this),
            $image = $modal.find('.modal-cropper > img'),
            cropper_cfg = window.cropper_configs[$modal.data('slug')];

        $modal.on('shown.bs.modal', function(){
            var image_w = $image.width();
            var modal_w = $modal.find('.modal-body').width();
            if (image_w < modal_w) {
                $modal.find('.modal-cropper-container').css({'max-width': image_w + 'px'});
            }
            $image.cropper(cropper_cfg);
        });

        $modal.on('hidden.bs.modal', function(){
            cropper_cfg.data = $image.cropper('getData');
            $image.cropper('destroy');
        });

        $modal.find('.cropper-save-close').on('click', function(){
            var $status = $modal.find('.ajax-status');
            var $spinner = $modal.find('.spinner');
            $status.removeClass('text-danger text-success').text('');
            $spinner.removeClass('hidden');

            var media_id = $modal.data('media-id');
            var style_id = $modal.data('slug');
            var data = $image.cropper('getData');
            data._csrftoken = $('#_csrftoken').val();

            $.post('/admin/media/crop/' + media_id + '/' + style_id, data, function(response, textStatus, xhr) {
                $spinner.addClass('hidden');
                $status.removeClass('text-danger').addClass('text-success').text(response.text);
                if (response.ok) {
                    var $preview_container = $('#preview-' + response.preview.style.slug);
                    // Make sure the proper orientation is set on the container
                    $preview_container.find('.attachment-preview').removeClass('portrait landscape').addClass(response.preview.orientation);
                    // TODO: maybe fade/flash/do-something with the updated image...
                    $preview_container.find('img').attr('src', response.preview.src + '?' + Math.random());
                    $modal.modal('hide');
                }
            }).fail(function(jqXHR, txt_status, error_thrown){
                $spinner.addClass('hidden');
                $status.removeClass('text-success').addClass('text-danger').text('Failed: ' + error_thrown);
            });
        });
    });

});
