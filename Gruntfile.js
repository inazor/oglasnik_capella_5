module.exports = function(grunt) {

    // measures the time each task takes
    require('time-grunt')(grunt);

    var banner = '/*! <%= pkg.name %> v<%= pkg.version %> built: <%= grunt.template.today("yyyy-mm-dd HH:MM") %> */\n\n';

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                banner: banner,
                mangle: true
            },
            frontend: {
                files: {
                    'public/assets/build/frontend.min.js': 'public/assets/build/frontend.js'
                }
            },
            backend: {
                files: {
                    'public/assets/build/backend.min.js': 'public/assets/build/backend.js'
                }
            }
        },
        concat: {
/*
            js_frontend: {
                options: {
                    separator: ';',
                    stripBanners: { block: true, line: true },
                    banner: banner
                },
                src: [
                    'public/assets/js/*.js' // Our site's scripts
                ],
                dest: 'public/assets/build/frontend.js'
            },
            css_frontend: {
                src: [
                    'public/assets/css/*.css'
                ],
                // dest: 'public/assets/css/oglasnik.css'
                dest: 'public/assets/build/frontend.css',
                options: {
                    separator: '\n\n',
                    stripBanners: { block: true, line: true },
                    banner: banner
                }
            },
*/
            js_backend: {
                options: {
                    separator: ';',
                    stripBanners: { block: true, line: true },
                    banner: banner
                },
                src: [
                    'public/assets/vendor/modernizr-2.8.3.min.js',
                    'public/assets/vendor/jquery/jquery-2.1.3.min.js',
                    'public/assets/vendor/bootstrap-3.3.2-dist/js/bootstrap.min.js',
                    'public/assets/vendor/bootstrap-hover-dropdown.min.js',
                    // 'public/assets/vendor/bootstrap-select/bootstrap-select.min.js',
                    'public/assets/vendor/bootstrap-select/bootstrap-select.custom.min.js',
                    'public/assets/vendor/sb-admin-2/js/plugins/metisMenu/metisMenu.min.js',
                    'public/assets/vendor/sb-admin-2/js/sb-admin-2.js',
                    'public/assets/vendor/bootstrap-datepicker.js',
                    'public/assets/vendor/js-date-format.min.js',
                    'public/assets/vendor/jquery.autoNumeric.js',
                    'public/assets/vendor/jquery.maxlength.min.js',
                    'public/assets/vendor/fancyBox/source/jquery.fancybox.pack.js',
                    // jquery.oglasnik.js depends on jquery-ui widget now because agim used $.widget in there
                    'public/assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.widget.min.js',
                    'public/assets/js/jquery.oglasnik.js',
                    'public/assets/backend/js/table-master-detail-rows.js',
                    'public/assets/backend/js/search-helpers.js',
                    'public/assets/backend/js/main.js',
                ],
                dest: 'public/assets/build/backend.js'
            },
            css_backend: {
                options: {
                    separator: '\n\n',
                    stripBanners: { block: true, line: true },
                    banner: banner
                },
                src: [
                    'public/assets/vendor/bootstrap-3.3.2-dist/css/bootstrap.min.css',
                    'public/assets/vendor/sb-admin-2/css/sb-admin-2.css',
                    'public/assets/vendor/bootstrap-select/bootstrap-select.min.css',
                    'public/assets/vendor/datepicker3.css',
                    'public/assets/vendor/fancyBox/source/jquery.fancybox.css',
                    'public/assets/css/font-awesome.min.css',
                    'public/assets/css/shopping-windows.css',
                    'public/assets/backend/css/backend.css',
                    // we might not want all of these bundled and only include each when needed separately?
                    'public/assets/backend/css/categories.css',
                    'public/assets/backend/css/category-parameterization.css',
                    'public/assets/backend/css/deletable-tabs.css',
                    'public/assets/backend/css/list-sortable.css',
                    'public/assets/backend/css/nested-list-sortable.css',
                    'public/assets/backend/css/parameters.css',
                    'public/assets/backend/css/users-edit.css'
                ],
                dest: 'public/assets/build/backend.css'
            }
        },
        cssmin: {
            options: {
                keepSpecialComments: 1
            },
            target: {
                files: [{
                    expand: true,
                    cwd: 'public/assets/build',
                    src: ['*.css', '!*.min.css'],
                    dest: 'public/assets/build',
                    ext: '.min.css'
                }]
            }
        }
/*
        ,csslint: {
            options: {
                csslintrc: '.csslintrc'
            },
            strict: {
                options: {
                    import: 2
                },
                src: [
                    'public/assets/build/frontend.css',
                    'public/assets/build/frontend.min.css',
                    'public/assets/build/backend.css',
                    'public/assets/build/backend.min.css',
                ]
            },
            lax: {
                options: {
                    import: false
                },
                src: [
                    'public/assets/build/*.css'
                ]
            }
        }
*/
    });

    // Load the plugins
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    // grunt.loadNpmTasks('grunt-contrib-csslint');
    // grunt.loadNpmTasks('grunt-contrib-imagemin');

    // Default task(s)
    // grunt.registerTask('default', ['backend', 'frontend']);
    grunt.registerTask('backend', ['concat:css_backend', 'concat:js_backend', 'uglify:backend', 'cssmin']);
    grunt.registerTask('frontend', ['concat:css_frontend', 'concat:js_frontend', 'uglify:frontend', 'cssmin']);
    // grunt.registerTask('build', ['backend', 'frontend']);
    grunt.registerTask('build', ['backend']);

};
